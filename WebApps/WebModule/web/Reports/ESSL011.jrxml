<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ESSL011" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="802" leftMargin="28" rightMargin="12" topMargin="20" bottomMargin="20" uuid="49d9ce06-d65a-4f88-a8ed-bf42b504c3b1">
	<property name="ireport.zoom" value="1.464100000000018"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="COMPANY_ID" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["21111"]]></defaultValueExpression>
	</parameter>
	<parameter name="REPORT_LOGO" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["../images/CompanyLogo.png"]]></defaultValueExpression>
	</parameter>
	<parameter name="EMPLOYEE_LIST" class="java.util.List" isForPrompting="false">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="TO_DATE" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["2014/08/04"]]></defaultValueExpression>
	</parameter>
	<parameter name="REPORT_NAME" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["ESSL011"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	(SELECT NAME FROM C_MASTER WHERE COMPANY_ID = vwl.COMPANY_ID) AS C_NAME,
	EMPLOYEE_ID,
	COMPANY_EMPLOYEE_NUMBER,
	FIRSTNAME,
	SURNAME,
	ACTIVE_YN,
	PROFILE_NAME,
	LEAVE_CYCLE_START,
	LEAVE_CYCLE_END,
	ACCUMULATED_BALANCE,
	LEAVE_AMENDMENTS,
	LEAVE_TAKEN,
	LEAVE_NOT_FORFEIT,
	BALANCE,
	FORFEIT_DATE,
	MONTHS_BEFORE_FORFEIT,
	ESTIMATED_FORFEIT
FROM
	DBA.vw_L_AnnualProjectedForfeit vwl
WHERE $X{IN,COMPANY_EMPLOYEE_NUMBER, EMPLOYEE_LIST}
AND ACTIVE_YN = 'Y'
AND ESTIMATED_FORFEIT > 0

ORDER BY COMPANY_EMPLOYEE_NUMBER]]>
	</queryString>
	<field name="C_NAME" class="java.lang.String"/>
	<field name="EMPLOYEE_ID" class="java.math.BigDecimal"/>
	<field name="COMPANY_EMPLOYEE_NUMBER" class="java.lang.String"/>
	<field name="FIRSTNAME" class="java.lang.String"/>
	<field name="SURNAME" class="java.lang.String"/>
	<field name="ACTIVE_YN" class="java.lang.String"/>
	<field name="PROFILE_NAME" class="java.lang.String"/>
	<field name="LEAVE_CYCLE_START" class="java.sql.Date"/>
	<field name="LEAVE_CYCLE_END" class="java.sql.Date"/>
	<field name="ACCUMULATED_BALANCE" class="java.math.BigDecimal"/>
	<field name="LEAVE_AMENDMENTS" class="java.math.BigDecimal"/>
	<field name="LEAVE_TAKEN" class="java.math.BigDecimal"/>
	<field name="LEAVE_NOT_FORFEIT" class="java.lang.String"/>
	<field name="BALANCE" class="java.math.BigDecimal"/>
	<field name="FORFEIT_DATE" class="java.sql.Date"/>
	<field name="MONTHS_BEFORE_FORFEIT" class="java.math.BigDecimal"/>
	<field name="ESTIMATED_FORFEIT" class="java.math.BigDecimal"/>
	<group name="NAME">
		<groupExpression><![CDATA[$F{PROFILE_NAME}]]></groupExpression>
		<groupHeader>
			<band height="27">
				<textField>
					<reportElement mode="Opaque" x="0" y="0" width="802" height="27" forecolor="#404040" backcolor="#E6E6E6" uuid="eafd9aa1-eb13-441f-80f4-4d75e513df6a"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14" isBold="false"/>
						<paragraph leftIndent="1"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{PROFILE_NAME} == null ? "" : $F{PROFILE_NAME}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<group name="ID">
		<groupExpression><![CDATA[$F{COMPANY_EMPLOYEE_NUMBER}]]></groupExpression>
		<groupHeader>
			<band/>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="72">
			<frame>
				<reportElement mode="Opaque" x="-20" y="-20" width="385" height="92" backcolor="#FFFFFF" uuid="c9b6bbf3-1eec-42a9-b078-c37b60acba0a"/>
				<staticText>
					<reportElement x="20" y="20" width="365" height="43" forecolor="#000000" uuid="4dc47214-e576-4fe6-b436-65df87d8dbd1"/>
					<textElement>
						<font fontName="SansSerif" size="28" isBold="false"/>
					</textElement>
					<text><![CDATA[Annual Projected Forfeit]]></text>
				</staticText>
				<staticText>
					<reportElement x="18" y="67" width="42" height="20" forecolor="#000000" uuid="bbc9aa4b-bce3-477f-9a37-7a7c39160192"/>
					<textElement textAlignment="Right">
						<font fontName="SansSerif" size="14" isBold="false"/>
					</textElement>
					<text><![CDATA[As at ]]></text>
				</staticText>
				<textField pattern="dd MMMMM yyyy">
					<reportElement x="63" y="67" width="258" height="20" forecolor="#000000" uuid="5e047d03-b038-4939-bb2c-b78091908baa"/>
					<textElement>
						<font fontName="SansSerif" size="14" isUnderline="false"/>
						<paragraph leftIndent="1"/>
					</textElement>
					<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
				</textField>
			</frame>
			<image scaleImage="RetainShape" hAlign="Center" vAlign="Middle" onErrorType="Blank">
				<reportElement x="628" y="-7" width="164" height="75" uuid="a40d6541-ad09-4393-b30a-33b7629d117e"/>
				<imageExpression><![CDATA[$P{REPORT_LOGO}]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band height="35">
			<textField>
				<reportElement x="0" y="0" width="443" height="35" forecolor="#333333" uuid="f1b405cd-0246-4e42-8a42-0e93553ab8ec"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="14" isBold="false"/>
					<paragraph leftIndent="1"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{C_NAME} == null? "Company : " : "Company : " + $F{C_NAME}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="32">
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="173" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="b871ca77-1394-474e-9184-4399932e0e36"/>
				<textElement textAlignment="Left">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph leftIndent="1"/>
				</textElement>
				<text><![CDATA[EMPLOYEE DETAILS]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="172" y="0" width="113" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="9f49e6c3-e1d3-4771-8137-aca053daab77"/>
				<textElement textAlignment="Left">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph leftIndent="10"/>
				</textElement>
				<text><![CDATA[PROFILE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="543" y="0" width="65" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="aa20f47e-7277-467b-ab4b-6bff031c8db4"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[AMENDMENTS ]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="451" y="0" width="41" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="7edddab6-c8fd-4149-92a8-f3a2b4cfd000"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[LEAVE TAKEN ]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="608" y="0" width="40" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="4c883713-e95c-4396-a43a-4e8694c3ca4d"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[LEAVE NOT FORFEIT]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="31" width="802" height="1" uuid="d3758cee-a6de-47a5-a24b-fb757464ee15"/>
			</line>
			<staticText>
				<reportElement mode="Opaque" x="285" y="0" width="44" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="44a4a6d3-985b-45a2-a004-aaae50815d41"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
				</textElement>
				<text><![CDATA[CYCLE
 START
 DATE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="329" y="0" width="49" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="198fd70f-d7f9-49ae-846c-9028c4645ec7"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
				</textElement>
				<text><![CDATA[CYCLE
END
DATE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="698" y="0" width="44" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="f52316bc-0544-4f0c-b0bd-976c158bc115"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[MONTHS BEFORE FORFEIT]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="742" y="0" width="60" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="a6293bf2-0aa3-4296-beae-4d36311ae840"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[PROJECTED FORFEIT]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="648" y="0" width="50" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="e692017f-c404-44e7-8568-ca3aaab7751e"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[FORFEIT DATE]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="378" y="0" width="73" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="10e475e7-b046-49d0-bc18-1624c7b677b5"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[ACCUMULATED BALANCE ]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="492" y="0" width="51" height="30" forecolor="#404040" backcolor="#E6E6E6" uuid="708a40c5-76b7-4c66-94c9-a421ba77cb01"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="8" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[CURRENT CYCLE BALANCE ]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="14">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="0" y="0" width="64" height="14" uuid="baed8bdc-ff4e-4281-b4d8-c150e6ea4e96"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{COMPANY_EMPLOYEE_NUMBER} == null ? "" : " " + $F{COMPANY_EMPLOYEE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="172" y="0" width="113" height="14" uuid="26b2121f-323a-4bf0-b7f4-e923317959a7"/>
				<textElement textAlignment="Left">
					<font size="8"/>
					<paragraph leftIndent="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{PROFILE_NAME} == null ? "" : $F{PROFILE_NAME}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="545" y="0" width="63" height="14" uuid="cd83a5c6-a6c7-426d-944d-159eb8eb7cbf"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LEAVE_AMENDMENTS}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="329" y="0" width="49" height="14" uuid="18a47b6e-dc66-4d7e-9d4e-cd5d91bdc012"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LEAVE_CYCLE_END}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="451" y="0" width="41" height="14" uuid="0341f494-960c-492d-865d-c86f5ff651ae"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LEAVE_TAKEN}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="492" y="0" width="53" height="14" uuid="dd0dcc19-f5a6-4023-b800-d1a82f759108"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BALANCE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="378" y="0" width="73" height="14" uuid="29aec2d5-2b9f-40c4-9083-c0959a9297f1"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ACCUMULATED_BALANCE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="64" y="0" width="108" height="14" uuid="d98541a4-8d67-48d4-8088-f08f7470bcb3"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SURNAME} == null || $F{FIRSTNAME} == null ? "" : " " + $F{SURNAME} + " " + $F{FIRSTNAME}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="285" y="0" width="44" height="14" uuid="d53da576-bcf9-4c92-b093-8e8a5e7b961b"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LEAVE_CYCLE_START}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="608" y="0" width="40" height="14" uuid="2d721a77-84d4-4205-a975-6f39ce119b83"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LEAVE_NOT_FORFEIT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="648" y="0" width="50" height="14" uuid="26a09fd5-4448-42ca-8fc6-301588d84cdf"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FORFEIT_DATE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#0" isBlankWhenNull="false">
				<reportElement x="698" y="0" width="44" height="14" uuid="e9324006-1d34-4714-9afd-70379a102ad0"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{MONTHS_BEFORE_FORFEIT}.equals(null) ? "0" : $F{MONTHS_BEFORE_FORFEIT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.00" isBlankWhenNull="true">
				<reportElement x="742" y="0" width="60" height="14" uuid="2187c2e9-db69-4626-8b7a-11a549393280"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ESTIMATED_FORFEIT}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band/>
	</columnFooter>
	<pageFooter>
		<band height="21">
			<textField>
				<reportElement mode="Opaque" x="172" y="4" width="229" height="13" backcolor="#E6E6E6" uuid="0f7c36ee-0c6d-4339-812b-d3320972ee92"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy h:mm a">
				<reportElement mode="Opaque" x="693" y="4" width="109" height="13" backcolor="#E6E6E6" uuid="d93abed3-766a-4bec-804f-bd89e03bb5c6"/>
				<textElement textAlignment="Right">
					<font size="9"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="0" y="4" width="173" height="13" backcolor="#E6E6E6" uuid="86f94e73-f2ae-4e36-bafc-0d4e189700f7"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
					<paragraph leftIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA["Accsys PeopleWare ESSL011"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement mode="Opaque" x="401" y="4" width="292" height="13" backcolor="#E6E6E6" uuid="b7e8f272-f0eb-4f1c-947d-3c75a9ce985c"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
