select ta.company_id,ta.person_id,ta.company_employee_number,surname,firstname,s.day,
    // Employees linked to shift profiles
    COALESCE(
    (select min(ta_clockingrecord."timestamp") from DBA.ta_clockingrecord where ta_clockingrecord.company_id = ta.company_id and ta_clockingrecord.person_id = ta.person_id
      and "date"(ta_clockingrecord."timestamp") between dateadd(day,-2,s.day) and dateadd(day,2,s.day) and ta_clockingrecord.direction = 1
      and ta_clockingrecord.period_id = any(select ta_24HourPeriod.period_id from DBA.ta_24HourPeriod where ta_24HourPeriod.dayofweek+1 = datepart(dw,s.day))),
    // Clockings not recorded against shift profiles
    (select min(ta_clockingrecord."timestamp") from DBA.ta_clockingrecord where ta_clockingrecord.company_id = ta.company_id and ta_clockingrecord.person_id = ta.person_id
      and "date"(ta_clockingrecord."timestamp") = s.day and ta_clockingrecord.direction = 1 and ta_clockingrecord.shift_id is null)) as FirstIn,
    COALESCE(
    (select max(ta_clockingrecord."timestamp") from DBA.ta_clockingrecord where ta_clockingrecord.company_id = ta.company_id and ta_clockingrecord.person_id = ta.person_id
      and "date"(ta_clockingrecord."timestamp") between dateadd(day,-2,s.day) and dateadd(day,2,s.day) and ta_clockingrecord.direction = 0
      and ta_clockingrecord.period_id = any(select ta_24HourPeriod.period_id from DBA.ta_24HourPeriod where ta_24HourPeriod.dayofweek+1 = datepart(dw,s.day))),
    (select max(ta_clockingrecord."timestamp") from DBA.ta_clockingrecord where ta_clockingrecord.company_id = ta.company_id and ta_clockingrecord.person_id = ta.person_id
      and "date"(ta_clockingrecord."timestamp") = s.day and ta_clockingrecord.direction = 0 and ta_clockingrecord.shift_id is null)) as LastOut,
    minutes(FirstIn,LastOut) as TimeAtWork_ActualMinutes,
    "right"('00' || (datediff(minute,FirstIn,LastOut)/60),2) || ':'
     || "right"('00' || (datediff(minute,FirstIn,LastOut)-(60*(datediff(minute,FirstIn,LastOut)/60))),2) as TimeAtWork_DisplayValue,
    (select Name FROM C_MASTER where COMPANY_ID = $P{Comp_ID_Param}),
    (select TA_ROSTERPROFILE.Name from TA_ROSTERPROFILE where TA_ROSTERPROFILE.SHIFTPROFILE_ID = ta.SHIFTPROFILE_ID) as PROFILE_NAME
    from DBA.ta_e_master as ta,DBA.s_calendar_days as s
where  day >= $P{FromDate} and day <= $P{ToDate} and ta.COMPANY_ID = $P{Comp_ID_Param} and ta.EMPLOYEE_ID in(select E_MASTER."employee_id" from "DBA"."E_MASTER" where E_MASTER."COMPANY_ID" = ta."company_id" and ACTIVE_YN = 'Y') and FirstIn IS NOT NULL and $X{IN,ta.company_employee_number, Employee_List}
order by company_employee_number, day