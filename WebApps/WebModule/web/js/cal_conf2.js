
//Define calendar(s): addCalendar ("Unique Calendar Name", "Window title", "Form element's name", Form name")
addCalendar("Calendar1", "Select From Date", "fromDate", "req_leave");
addCalendar("Calendar2", "Select To Date", "toDate", "req_leave");
addCalendar("Calendar3", "Select From Date", "fromDate", "tnastats_dates");
addCalendar("Calendar4", "Select To Date", "toDate", "tnastats_dates");
addCalendar("Calendar7", "Select From Date", "fromDate", "ToDoList");
addCalendar("Calendar8", "Select To Date", "toDate", "ToDoList");
addCalendar("Calendar10", "Select From Date", "fromDate", "reportFilter");
addCalendar("Calendar11", "Select To Date", "toDate", "reportFilter");


// default settings for English
// Uncomment desired lines and modify its values
// setFont("verdana", 9);
 setWidth(95, 1, 15, 1);
// setColor("#cccccc", "#cccccc", "#ffffff", "#ffffff", "#333333", "#cccccc", "#333333");
// setFontColor("#333333", "#333333", "#333333", "#ffffff", "#333333");
// setFormat("yyyy/mm/dd");
// setSize(200, 200, -200, 16);

// setWeekDay(0);
// setMonthNames("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
// setDayNames("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
// setLinkNames("[Close]", "[Clear]");
