<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!--  Now we need to verify that there is a LoginSessionObject   -->

<%

    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Changes to Personal Information")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
    } else {
// Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {
            // Page Top

            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            if (user.equals(employee)) {
                out.write(gen.buildHTMLPageHeader("Change Personal Information", employee.toString()));
            } else {
                out.write(gen.buildHTMLPageHeader("Change Personal Information", "On Behalf Of: " + employee.toString()));
            }


            // Page Detail
            PageProducer_EmployeeContactRequest producer = new PageProducer_EmployeeContactRequest(employee);
            out.write("<center>");
            out.write(producer.generateInformationContent());
            out.write("</center>");
        }
    }
%>

<%@include file="html_bottom.jsp"%>