<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    loginSessionObject.setEmployee(user);
    employee = new Employee(user.getCompany(), user.getEmployeeID());

    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Payslips")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
    } else {
        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {

            // Employee
            FileContainer fc = FileContainer.getInstance();
            // Payroll
            Payroll payroll = employee.getPayroll();
            String periodID = request.getParameter("periodID");
            // Previous archived payslips?
            String fromYear = request.getParameter("fromYear");
            String toYear = request.getParameter("toYear");
            // Check hash to make sure this is the correct person
            String urlHash = request.getParameter("hash");
            String empHash = String.valueOf(employee.hashCode());
            
            if ((urlHash != null) && (urlHash.compareToIgnoreCase(empHash)!=0)) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Possible fraudulent attempt.  Payslips will not be displayed.",
                        false, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            } else {

                if (fromYear != null) {
                // ******
                    // ****** PAYSLIPS OF PREVIOUS TAX YEARS ******
                    // ******
                    ArchivedPayrollDetail archivedPayrollDetail = null;
                    //System.out.println("Generating Archived Payslips");
                    if (payroll == null) {
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                "You are not currently linked to a Payroll.",
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                        out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                    } else {
                        // Period Detail
                        if (periodID == null) {
                            // Get the current open period
                            periodID = "1";
                        }

                        // Now we create a tab selection for the different groups
                        java.util.TreeMap periodList = new java.util.TreeMap();  // use a TreeMap to sort the dates chronologically
                        for (int runtype = 1; runtype <= 5; runtype++) {
                            ArchivedPayslipInfo info = new ArchivedPayslipInfo(employee, new Integer(periodID).intValue(), runtype, fromYear);
                            java.util.LinkedList periods = info.getCalculatedPeriods();
                            for (int i = 0; i < periods.size(); i++) {
                                String endDate = za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(((ArchivedPayrollDetail) periods.get(i)).getEndDate());
                                if (!periodList.containsKey(endDate)) {
                                    periodList.put(endDate, (ArchivedPayrollDetail) periods.get(i));
                                }
                            }
                        }
                    // Once all the periods are loaded, we can construct the tab buttons

                        // Page Header
                        PageSectionGenerator gen = PageSectionGenerator.getInstance();
                        out.write(gen.buildHTMLPageHeader("Payslip Preview - " + fromYear + "/" + toYear, employee.toString()));
                        out.write("<center><h3 class=\"shaddow\">Payslip Period: ");
                        StringBuilder selection = new StringBuilder();
                        selection.append("<SELECT name=\"balanceDate\" ONCHANGE=\"location = this.options[this.selectedIndex].value;\">");

                        // Build a list of payslip periods
                        int cnt;
                        java.util.Iterator iter = periodList.values().iterator();
                        while (iter.hasNext()) {
                            ArchivedPayrollDetail listPayrollDetail = (ArchivedPayrollDetail) iter.next();
                            String endDate = za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(listPayrollDetail.getEndDate());
                            if (listPayrollDetail.getPayrolldetailID().toString().trim().compareTo(periodID) == 0) {
                                selection.append(" <option value=\"payslip_prev.jsp?hash=" + employee.hashCode() + "&periodID=" + listPayrollDetail.getPayrolldetailID() + "&fromYear=" + fromYear + "&toYear=" + toYear + " \" selected  >" + endDate + "</option>");
                            } else {
                                selection.append(" <option value=\"payslip_prev.jsp?hash=" + employee.hashCode() + "&periodID=" + listPayrollDetail.getPayrolldetailID() + "&fromYear=" + fromYear + "&toYear=" + toYear + " \" >" + endDate + "</option>");
                            }
                        }
                        selection.append("</select></h3>");
                        out.write(selection.toString());

                        // Page Detail
                        archivedPayrollDetail = new ArchivedPayrollDetail(employee.getPayroll(), new Integer(periodID), fromYear);
                        if (archivedPayrollDetail == null) {
                        } else {
                            PageProducer_PayslipPreview producer = new PageProducer_PayslipPreview(employee, archivedPayrollDetail);
                            out.write(producer.generateInformationContent());
                        }

                    }
                } else {
                // ******
                    // ****** PAYSLIPS OF CURRENT TAX YEAR ******
                    // ******

                    PayrollDetail payrollDetail = null;

                    if (payroll == null) {
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                "You are not currently linked to a Payroll.",
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                        out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                    } else {
                        // Period Detail
                        if (periodID == null) {
                            // Get the current open period
                            periodID = employee.getLatestCalculatedPeriod();
                            //System.out.println("periodID = " + periodID);
                            if (periodID == null) {
                                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                        "Unable to find a calculated payslip for this tax year.",
                                        false, request.getSession());
                                producer.setMessageInSessionObject();
                                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                                return;
                            }
                            payrollDetail = fc.getPayrollDetailFromContainer(employee.getPayroll(), new Integer(periodID).intValue());
                            // If, for whatever reason, the PayrollDetail is null, select the first available period you can find.
                            // This is not that big a problem, as the user will still have the option of choosing different periods
                            // to view payslips on.
                            if (payrollDetail == null) {
                                payrollDetail = fc.getPayrollDetailFromContainer(employee.getPayroll(), 1);
                            }
                        } else {
                            payrollDetail = fc.getPayrollDetailFromContainer(employee.getPayroll(), new Integer(periodID).intValue());
                        }

                        // Page Header
                        PageSectionGenerator gen = PageSectionGenerator.getInstance();
                        out.write(gen.buildHTMLPageHeader("Payslip Preview", employee.toString()));
                        out.write("<center><h3 class=\"shaddow\">Payslip Period: ");

                        // Now we create a tab selection for the different groups
                        java.util.TreeMap periodList = new java.util.TreeMap();  // use a TreeMap to sort the dates chronologically
                        for (int runtype = 1; runtype <= 5; runtype++) {
                            PayslipInfo info = new PayslipInfo(employee, new Integer(periodID).intValue(), runtype);
                            java.util.LinkedList periods = info.getCalculatedPeriods(true);
                            for (int i = 0; i < periods.size(); i++) {
                                String endDate = za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(((PayrollDetail) periods.get(i)).getEndDate());
                                if (!periodList.containsKey(endDate)) {
                                    periodList.put(endDate, (PayrollDetail) periods.get(i));
                                }
                            }
                        }

                        StringBuilder selection = new StringBuilder();
                        selection.append("<SELECT name=\"balanceDate\" ONCHANGE=\"location = this.options[this.selectedIndex].value;\">");

                        // Build a list of periods
                        java.util.Iterator iter = periodList.values().iterator();
                        while (iter.hasNext()) {
                            PayrollDetail listPayrollDetail = (PayrollDetail) iter.next();
                            String endDate = za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(listPayrollDetail.getEndDate());
                            if (listPayrollDetail.getPayrolldetailID().toString().trim().compareTo(periodID) == 0) {
                                selection.append(" <option value=\"payslip_prev.jsp?hash=" + employee.hashCode() + "&periodID=" + listPayrollDetail.getPayrolldetailID() + " \" selected  >" + endDate + "</option>");
                            } else {
                                selection.append(" <option value=\"payslip_prev.jsp?hash=" + employee.hashCode() + "&periodID=" + listPayrollDetail.getPayrolldetailID() + " \" >" + endDate + "</option>");
                            }
                        }
                        selection.append("</select></h3>");
                        out.write(selection.toString());

                        // Page Detail
                        if (payrollDetail == null) {
                        } else {
                            PageProducer_PayslipPreview producer = new PageProducer_PayslipPreview(employee, payrollDetail);
                            out.write(producer.generateInformationContent());
                        }

                    }
                }
            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>