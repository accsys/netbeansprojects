<%-- 
    Document   : onlineQuote
    Created on : 26 Aug 2014, 1:57:13 PM
    Author     : fkleynhans
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.IOException"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.ss.usermodel.Workbook"%>
<%@page import="java.io.FileInputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/style.css" />
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        <title>PeoleWare Cloud Pricing- Quick Quote</title>
        <script type="text/javascript">
            // Goes back
            function back() {
                act = 'Back';
                history.back();
            }
        </script>
    </head>
    <body>
        <div id="container">
            <h1 class="title">PeopleWare Cloud Pricing - Quick quote</h1>
            <form action="onlineContactSales.jsp" method="GET">
                <%
                    int numberOfEmployees = 0;
                    String numberOfEmps = request.getParameter("numberOfEmployees");
                    if (numberOfEmps == null || numberOfEmps.equalsIgnoreCase("")) {
                        numberOfEmps = "0";
                    }
                    numberOfEmployees = Integer.parseInt(numberOfEmps);
                    session.setAttribute("numberOfEmployees", numberOfEmployees);

                    int PWOperators = 0;
                    String PWOps = request.getParameter("PWOperators");
                    if (PWOps == null || PWOps.equalsIgnoreCase("")) {
                        PWOps = "0";
                    }
                    PWOperators = Integer.parseInt(PWOps);
                    session.setAttribute("PWOperators", PWOperators);

                    String payrollSwitch = request.getParameter("payrollSwitch");
                    if (payrollSwitch == null || payrollSwitch.equalsIgnoreCase("")) {
                        payrollSwitch = "N";
                    }
                    session.setAttribute("payrollSwitch", payrollSwitch);
                    
                    String HrSwitch = request.getParameter("HrSwitch");
                    if (HrSwitch == null || HrSwitch.equalsIgnoreCase("")) {
                        HrSwitch = "N";
                    }
                    session.setAttribute("HrSwitch", HrSwitch);
                    
                    String TnaSwitch = request.getParameter("TnaSwitch");
                    if (TnaSwitch == null || TnaSwitch.equalsIgnoreCase("")) {
                        TnaSwitch = "N";
                    }
                    session.setAttribute("TnaSwitch", TnaSwitch);
                    
                    String EssSwitch = request.getParameter("EssSwitch");
                    if (EssSwitch == null || EssSwitch.equalsIgnoreCase("")) {
                        EssSwitch = "N";
                    }
                    session.setAttribute("EssSwitch", EssSwitch);

                    int NoOfCredits = 0;
                    String NoOfCreds = request.getParameter("NoOfCredits");
                    if (NoOfCreds == null || NoOfCreds.equalsIgnoreCase("")) {
                        NoOfCreds = "1000";
                    }
                    NoOfCredits = Integer.parseInt(NoOfCreds);
                    session.setAttribute("NoOfCredits", NoOfCredits);

                    String ExcelSwitch = request.getParameter("ExcelSwitch");
                    if (ExcelSwitch == null || ExcelSwitch.equalsIgnoreCase("")) {
                        ExcelSwitch = "N";
                    }
                    session.setAttribute("ExcelSwitch", ExcelSwitch);
                    
                    session.setAttribute("emailSent", "N");

                    double payrollCost = 0.00;
                    double hrCost = 0.00;
                    double tnaCost = 0.00;
                    double essUserCost = 0.00;
                    double excelCost = 0.00;
                    double essServerCost = 0.00;
                    double essCreditsCost = 0.00;
                    int essCreditsRow = 0;
                    double crystalCost = 0.00;
                    double exchangeRate = 0.00;

                    try {

                        //Create the input stream from the xlsx/xls file
                        String fileName = "PriceList.xls";
                        FileInputStream fis = new FileInputStream(fileName);

                        //Create Workbook instance for xlsx/xls file input stream
                        Workbook workbook = new HSSFWorkbook();
                        //if(fileName.toLowerCase().endsWith("xlsx")){
                        //    workbook = new Workbook(fis);
                        //}else if(fileName.toLowerCase().endsWith("xls")){
                        try {
                            workbook = new HSSFWorkbook(fis);
                        } catch (IOException exc) {
                            exc.printStackTrace();
                        }
                            //}

                        // Get all the costs from the workbook
                        payrollCost = workbook.getSheetAt(3).getRow(14).getCell(0).getNumericCellValue();
                        hrCost = workbook.getSheetAt(3).getRow(14).getCell(1).getNumericCellValue();
                        tnaCost = workbook.getSheetAt(3).getRow(14).getCell(2).getNumericCellValue();
                        essUserCost = workbook.getSheetAt(3).getRow(14).getCell(4).getNumericCellValue();
                        excelCost = workbook.getSheetAt(3).getRow(15).getCell(4).getNumericCellValue();
                        essServerCost = workbook.getSheetAt(3).getRow(16).getCell(4).getNumericCellValue();
                        crystalCost = workbook.getSheetAt(2).getRow(14).getCell(2).getNumericCellValue();
                        exchangeRate = workbook.getSheetAt(3).getRow(28).getCell(2).getNumericCellValue();

                        switch (NoOfCredits) {
                            case 1000:
                                essCreditsRow = 5;
                                NoOfCreds = "1 000";
                                break;
                            case 2500:
                                essCreditsRow = 6;
                                NoOfCreds = "2 500";
                                break;
                            case 5000:
                                essCreditsRow = 7;
                                NoOfCreds = "5 000";
                                break;
                            case 10000:
                                essCreditsRow = 8;
                                NoOfCreds = "10 000";
                                break;
                            default:
                                essCreditsRow = 5;
                        }

                        essCreditsCost = workbook.getSheetAt(4).getRow(essCreditsRow).getCell(9).getNumericCellValue();

                        //                out.write("payrollCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(payrollCost) + "</b>");
                        //                out.write("hrCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(hrCost) + "</b>");
                        //                out.write("tnaCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(tnaCost) + "</b>");
                        //                out.write("essUserCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(essUserCost) + "</b>");
                        //                out.write("excelCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(excelCost) + "</b>");
                        //                out.write("essServerCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(essServerCost) + "</b>");
                        //                out.write("essCreditsCost:<b>&nbsp;" + new DecimalFormat("###,##0.00").format(essCreditsCost) + "</b>");
                        //close file input stream
                        fis.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                %>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Monthly Cost</h2>
                    <div class="grouping">
                        <table id="quotetable">
                            <thead>
                                <tr>
                                    <th style="width: 58px;">Code</th>
                                    <th style="width: 33px;">Qty</th>
                                    <th>Description</th>
                                    <th style="width: 61px;">Unit Price</th>
                                    <th style="width: 85px;">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="nohover">
                                    <td></td>
                                    <td></td>
                                    <td style="padding-top: 15px;"><b>Software: PeopleWare Corporate - Cloud</b></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <%
                                    double payrollTotal = 0.00;
                                    if (payrollSwitch.contains("on")) {
                                        payrollTotal = payrollCost * numberOfEmployees;
                                %>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;"><%out.write(new DecimalFormat("0").format(numberOfEmployees));%></td>
                                    <td>Per employee, per month - Payroll (both wages and salaries)</td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(payrollCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(payrollTotal));%></td>
                                </tr>
                                <%
                                    }
                                    double hrTotal = 0.00;
                                    if (HrSwitch.contains("on")) {
                                        hrTotal = hrCost * numberOfEmployees;
                                %>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;"><%out.write(new DecimalFormat("0").format(numberOfEmployees));%></td>
                                    <td>Per employee, per month - HR </td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(hrCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(hrTotal));%></td>
                                </tr>
                                <%
                                    }
                                    double tnaTotal = 0.00;
                                    if (TnaSwitch.contains("on")) {
                                        tnaTotal = tnaCost * numberOfEmployees;
                                %>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;"><%out.write(new DecimalFormat("0").format(numberOfEmployees));%></td>
                                    <td>Per employee, per month - T&A</td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(tnaCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(tnaTotal));%></td>
                                </tr>
                                <%
                                    }
                                    double operatorsTotal = essUserCost * PWOperators;
                                %>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;"><%out.write(new DecimalFormat("0").format(PWOperators));%></td>
                                    <td>PeopleWare system users per month </td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(essUserCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(operatorsTotal));%></td>
                                </tr>
                                <%
                                    double microsoftTotal = 0.00;
                                    if (ExcelSwitch.contains("on")) {
                                        microsoftTotal = excelCost * PWOperators;
                                %>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;"><%out.write(new DecimalFormat("0").format(PWOperators));%></td>
                                    <td>Microsoft users per month<br>(allows Excel extract onto Cloud desktop - optional)</td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(excelCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(microsoftTotal));%></td>
                                </tr>
                                <%
                                    }
                                    double essServerTotal = 0.00;
                                    double essTransactionsTotal = 0.00;
                                    if (EssSwitch.contains("on")) {
                                        essServerTotal = essServerCost;
                                        essTransactionsTotal = essCreditsCost;
                                %>
                                <tr id="nohover">
                                    <td></td>
                                    <td></td>
                                    <td style="padding-top: 25px;"><b>Software: PeopleWare Employee Self Service</b></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">PWP-CL</td>
                                    <td style="text-align: center;">1</td>
                                    <td>ESS Server monthly rental</td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(essServerCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(essServerTotal));%></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>Mobile Application</td>
                                    <td style="text-align: right;  margin-right: 0px;">--</td>
                                    <td style="text-align: right;  margin-right: 0px;">--</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;"></td>
                                    <td style="text-align: center;">1</td>
                                    <td><%out.write("Transactions - " + NoOfCreds);%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(essCreditsCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(essTransactionsTotal));%></td>
                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <td style="border-right: none; border-left: none; border-top: 1px solid black"></td>
                                    <td style="border-right: none; border-top: 1px solid black"></td>
                                    <td style="border-right: none; border-top: 1px solid black"></td>
                                    <td style="text-align: right;  margin-right: 0px; padding-bottom: 0px; border-top: 1px solid black; border-right: 1px solid black"><b>Subtotal</b></td>
                                    <td style="border-top: 1px solid black; padding-bottom: 0px;">
                                        <%
                                            double TotalExl = 0.00;
                                            double VAT = 0.00;
                                            double GrandTotal = 0.00;

                                            TotalExl = payrollTotal + hrTotal + tnaTotal + operatorsTotal + microsoftTotal + essServerTotal + essTransactionsTotal;
                                            VAT = TotalExl * 0.14;
                                            GrandTotal = TotalExl + VAT;
                                        %>
                                        <span style="text-align: left; margin-left: 0px; float: left;">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("#,###,##0.00").format(TotalExl) + "</p>");%></span></td>
                                </tr>
                                <tr>
                                    <td style="border-right: none; border-left: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="text-align: right;  margin-right: 0px; padding-top: 0px; border-right: 1px solid black">VAT</td>
                                    <td style="padding-top: 0px;"><span style="text-align: left; margin-left: 0px; float: left;">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("###,##0.00").format(VAT) + "</p>");%></span></td>
                                </tr>
                                <tr>
                                    <td style="border-right: none; border-left: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="text-align: right;  margin-right: 0px; border-right: 1px solid black"><b>Total</b></td>
                                    <td style="border: 1px solid black"><span style="text-align: left; margin-left: 0px; float: left">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("#,###,##0.00").format(GrandTotal) + "</p>");%></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <h2 class="title-nomargin">Once-Off Cost</h2>
                    <div class="grouping">
                        <%
                            double crystalTotal = crystalCost * exchangeRate;
                        %>
                        <table id="quotetable"> 
                            <thead>
                                <tr>
                                    <th style="width: 58px;">Code</th>
                                    <th style="width: 33px;">Qty</th>
                                    <th>Description</th>
                                    <th style="width: 65px;">Unit Price</th>
                                    <th style="width: 85px;">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="nohover">
                                    <td></td>
                                    <td></td>
                                    <td style="padding-top: 15px;"><b>3rd Party Software</b></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">CL-2082</td>
                                    <td style="text-align: center;">1</td>
                                    <td>SAP Crystal Reports XI (exchange rate on day of invoice to be used)</td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(crystalCost));%></td>
                                    <td style="text-align: right;  margin-right: 0px;"><%out.write(new DecimalFormat("###,##0.00").format(crystalTotal));%></td>
                                </tr>
                                <tr>
                                    <td style="border-right: none; border-left: none; border-top: 1px solid black"></td>
                                    <td style="border-right: none; border-top: 1px solid black"></td>
                                    <td style="border-right: none; border-top: 1px solid black"></td>
                                    <td style="text-align: right;  margin-right: 0px; padding-bottom: 0px; border-top: 1px solid black; border-right: 1px solid black"><b>Subtotal</b></td>
                                    <td style="border-top: 1px solid black; padding-bottom: 0px;">
                                        <%
                                            TotalExl = crystalTotal;
                                            VAT = TotalExl * 0.14;
                                            GrandTotal = TotalExl + VAT;
                                        %>
                                        <span style="text-align: left; margin-left: 0px; float: left;">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("###,##0.00").format(TotalExl) + "</p>");%></span></td>
                                </tr>
                                <tr>
                                    <td style="border-right: none; border-left: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="text-align: right;  margin-right: 0px; padding-top: 0px; border-right: 1px solid black">VAT</td>
                                    <td style="padding-top: 0px;"><span style="text-align: left; margin-left: 0px; float: left;">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("###,##0.00").format(VAT) + "</p>");%></span></td>
                                </tr>
                                <tr>
                                    <td style="border-right: none; border-left: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="border-right: none;"></td>
                                    <td style="text-align: right;  margin-right: 0px; border-right: 1px solid black"><b>Total</b></td>
                                    <td style="border: 1px solid black"><span style="text-align: left; margin-left: 0px; float: left">R</span><span style="text-align: right;  margin-right: 0px; float: right;"><%out.write(new DecimalFormat("#,###,##0.00").format(GrandTotal) + "</p>");%></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button class="gradient-button">Get a formal quote</button>
                <input class="gradient-button" value="Back" onclick="back();" style="width: 40px;">
            </form>
        </div><!-- Container end -->
    </body>
</html>
