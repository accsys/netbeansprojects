<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>
<!-- Now we need to verify that there is a LoginSessionObject -->

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
// Page Top
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("System Setup", ""));

// Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {

// Get LoggedIn Employee
            if (!za.co.ucs.accsys.webmodule.FileContainer.getInstance().isEmployeeWebAdministrator(user)) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "You must belong to the 'Web Administrator' group in Peopleware in <br>order to "
                        + "modify the WebModule properties", true, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;url=http://localhost:8084/WebModule/processes/message_prev.jsp\">");  //http://store.apple.com/1-800-MY-APPLE/WebObjects/AppleStore.woa\">"); //http://172.31.160.23:8084/WebModule/processes/message_prev.jsp\"> ");

            } else {

                java.util.prefs.Preferences webPreferences = za.co.ucs.accsys.webmodule.WebModulePreferences.getPreferences();
%>

<FORM METHOD=POST ACTION=../Controller.jsp >
    <center>
    <div style="width: 1480px">
    <div class="grouping">
    <TABLE id="admintable">
    
<% //Get the number of preferences
            try {
                String keys[] = webPreferences.keys();
                StringBuffer output = new StringBuffer();
                String key;
                String value;
                // Column Headers
                output.append("<tr><th><h3 class=\"shaddow\">Property</h3></th>");
                output.append("    <th style=\"border-right: 1px solid #b0b0b0;\"><h3 class=\"shaddow\">Value</h3></th>");
                output.append("    <th><h3 class=\"shaddow\">Property</h3></th>");
                output.append("    <th><h3 class=\"shaddow\">Value</h3></th></tr>");


                System.out.println("Number Of Keys:" + keys.length);
                for (int i = 0; i < java.lang.Math.ceil((keys.length + 1) / 2); i++) {

                    // First Column
                    if ((i * 2) < keys.length) {
                        key = webPreferences.keys()[i * 2];
                        value = webPreferences.get(key, "");
                        // Make sure you can handle the """
                        value = value.replaceAll("\"", "'");
                        key = key.replaceAll("_", " ");

                        output.append("<tr>");

                        output.append("    <td style=\"padding-top: 10px; padding-bottom: 10px;\">" + key + "</td><td style=\"border-right: 1px solid #b0b0b0;\"><INPUT TYPE=text  maxlength=50 size=\"50\" NAME=\"" + key + "\" value=\"" + value + "\" ></td>");


                        // Second Column
                        if ((i * 2 + 1) < keys.length) {
                            key = webPreferences.keys()[i * 2 + 1];
                            value = webPreferences.get(key, "");
                            // Make sure you can handle the """
                            value = value.replaceAll("\"", "'");
                            key = key.replaceAll("_", " ");

                            output.append("    <td>" + key + "</td><td><INPUT TYPE=text  maxlength=50 size=\"50\" NAME=\"" + key + "\" value=\"" + value + "\" ></td>");
                        } else {
                            output.append("   <td></td><td></td><td></td>");
                            output.append("</tr>");
                        }
                    }
                }
                out.write(output.toString());
            } catch (java.util.prefs.BackingStoreException e) {
                out.write(e.toString());
            }
%>
      </table></div>
<%
            // SUBMIT
            {
                out.write("<div style=\"height: 40px;\"><input class=\"gradient-button\" style=\"margin-right: 35px;\" type=\"submit\" value=\"Apply Changes\"></div><br><hr style=\"width: 65%;\">");
            }
            
            {
                out.write("<div>Your current ESS credit balance is <b>"+za.co.ucs.lwt.db.DatabaseObject.getAvailableESSCredits() +"</b></div><br>");
            }
            // ESS Credits
            
                   %>
    <input type="hidden" name="action" value="admin">
</div></center>
</FORM>
<%
            }
        }
    }
%>

</CENTER>

<%@include file="html_bottom.jsp"%>