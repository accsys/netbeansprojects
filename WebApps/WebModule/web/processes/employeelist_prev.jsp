<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Page Top
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        boolean multiEmployeeSelection = false; // Should we allow for the user to select more than one employee?

        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        String hashValue = request.getParameter("hashValue"); // the HashValue of the Reporting Structure in use
        String orderBy = request.getParameter("orderBy");
        String selectAllAction = request.getParameter("action");
        String selectionID = request.getParameter("selSelection");
        ReportingStructure reportingStructure = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getReportingStructure(hashValue);
        if (request.getParameter("MultiEmployee") != null) {
            multiEmployeeSelection = (request.getParameter("MultiEmployee").compareToIgnoreCase("true") == 0);
        }
        if (multiEmployeeSelection) {
            out.write(gen.buildHTMLPageHeader("Multiple Employee Selection", employee.toString()));
        } else {
            out.write(gen.buildHTMLPageHeader("Employee Selection", employee.toString()));
        }


        // Page Detail
        String refDate = request.getParameter("referenceDate"); // for which period?
        String selectedSelectionString = request.getParameter("selSelection"); // Which Selection was selected?
        String webProcessClassName = request.getParameter("webProcessClassName"); // Which WebProcess is being used?

        //
        // Group of selections 'under' this one in the form of a TreeList
        //
        EmployeeSelection displaySelection = null;  // The displaySelection is the class used to generate the detailed information
        // of the specific page.

        // We also need to traverse the sub-groups to instantiate Get a list of all the selections underneath the current user.
        // We need this to forward the appropriate selected selection
        int selectedSelectionCode = 0;
        if (selectedSelectionString != null) {
            selectedSelectionCode = new Integer(selectedSelectionString).intValue();
            displaySelection = reportingStructure.getEmployeeSelection(selectedSelectionCode);
        }
        // Page Detail
        WebProcessDefinition webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);

        if (displaySelection != null) {
            if (webProcessDefinition != null) {
                PageProducer_EmployeeSelection producer = null;
                out.write("<div id=\"container\">");
                out.write("<div class=\"item\">");

                producer = new PageProducer_EmployeeSelection(user, webProcessClassName, displaySelection, request.getSession(), orderBy, hashValue, selectedSelectionString, multiEmployeeSelection, selectAllAction, selectionID);
                out.write(producer.generateInformationContent());
                out.write("</div>");
                out.write("</div>");
            } else {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "No Web Process Definition available.", true, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>