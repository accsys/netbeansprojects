<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%//@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        if (!user.equals(employee)) {
            loginSessionObject.setEmployee(user);
        } else {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Personal Information", employee.toString()));

            // Page Detail
            out.write("<table width=\"80%\"><tr><td>");
            out.write("<div id=\"container\">");
            PageProducer_PersonalInfo producer = new PageProducer_PersonalInfo(employee);
            out.write(producer.generateInformationContent());
            out.write("</div>");
            out.write("</td></tr></table>");
                }
            }

        %>

        <%@include file="html_bottom.jsp"%>