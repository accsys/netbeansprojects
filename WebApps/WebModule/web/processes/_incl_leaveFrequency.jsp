

<%

            // Define Chart
            strBXML = new StringBuffer();
            sqlStatement = new StringBuffer();

            strBXML.append("<");
            strBXML.append("chart caption='Leave Frequency: Per day of week' ");
            strBXML.append("xAxisName='Day' ");
            strBXML.append("yAxisName='x Times'");
            strBXML.append("showValues='0' ");
            strBXML.append("formatNumberScale='0' ");
            strBXML.append("showAboutMenuItem='0' ");
            strBXML.append("bgColor='D8E6E9,FFFFFF' ");
            strBXML.append("bgAlpha='100, 100' ");
            strBXML.append("showBorder='1' ");
            strBXML.append("canvasbgAlpha='30' ");
            strBXML.append("labelDisplay='ROTATE' ");
            strBXML.append("outCnvBaseFont='Verdana' ");
            strBXML.append("numVisiblePlot='12' ");
            strBXML.append("useRoundEdges='1' ");
            strBXML.append(">");


            za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));
            try {
                con = object.getNewConnectionFromPool();
                sqlStatement.append("select dayname(start_date) theDay, datepart(dw,start_date) as theDatePart, ");
                sqlStatement.append("   count(*) as CNT from l_history where ");
                sqlStatement.append("   Company_id=" + employee.getCompany().getCompanyID());
                sqlStatement.append("   and Employee_id=" + employee.getEmployeeID());
                sqlStatement.append("   group by theDay, theDatePart ");
                sqlStatement.append("   order by theDatePart ");


                java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);

                // Build dataset - Leave taken per day of week
                while (rs.next()) {
                    strBXML.append("<set label='" + rs.getString("theDay") + "' value='" + rs.getString("CNT")
                            + "' toolText='" + rs.getString("CNT") + " times leave was taken on this day' />");
                }

                strBXML.append("</chart>");
                strXML = strBXML.toString();
                rs.close();
            } finally {
                object.releaseConnection(con);
            }

%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/Column2D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="550" />
        <jsp:param name="chartHeight" value="350" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>


