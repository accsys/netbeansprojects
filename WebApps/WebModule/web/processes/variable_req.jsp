<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
            if (loginSessionObject == null) {
                response.sendRedirect(response.encodeURL("Login.jsp"));
            } else {  // Page Top
                String indicatorCode = request.getParameter("indCode");

                // Variable Info
                // Did the calling jsp pass an indicator code?
                if (indicatorCode == null) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "Unable to extract the appropriate Indicator Code", false, request.getSession());
                    producer.setMessageInSessionObject();
                    out.write("<meta http-equiv=\"refresh\" content=\"5;message_prev.jsp\"> ");
                    loginSessionObject.setEmployee(user);
                } else {
                    // Determine which variable linked to this employee, uses that indicator code
                    za.co.ucs.accsys.peopleware.Variable variable = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(employee, indicatorCode);
                    if (variable == null) {
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                employee.toString() + " is either not linked to a variable with Indicator Code - " + indicatorCode + ", or has more than one variable with said Indicator Code linked to his/her package. <br>Please consult your payroll administrator.",
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                        out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
                        loginSessionObject.setEmployee(user);
                    } else {
                        // Generate the String containing the processClassName
                        String processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges";
                        if (indicatorCode.compareToIgnoreCase("55001") == 0) {
                            processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001";
                        }
                        if (indicatorCode.compareToIgnoreCase("55002") == 0) {
                            processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002";
                        }
                        if (indicatorCode.compareToIgnoreCase("55003") == 0) {
                            processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003";
                        }
                        if (indicatorCode.compareToIgnoreCase("55004") == 0) {
                            processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004";
                        }
                        if (indicatorCode.compareToIgnoreCase("55005") == 0) {
                            processClassName = "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005";
                        }

                        PageSectionGenerator gen = PageSectionGenerator.getInstance();
                        // Get the appropriate header:
                        if (user.equals(employee)) {
                            out.write(gen.buildHTMLPageHeader("Variable Change Request", employee.toString()));
                        } else {
                            out.write(gen.buildHTMLPageHeader("Variable Change Request", "On Behalf Of: " + employee.toString()));
                        }

                        // Page Detail
                        String selectionID = request.getParameter("selSelection");
                        PageProducer_VariableChangesRequest producer =
                                new PageProducer_VariableChangesRequest(employee, processClassName, indicatorCode, selectionID);
                        out.write(producer.generateInformationContent());
                    }
                }
            }
%>

<%@include file="html_bottom.jsp"%>