<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%  // Can I connect to the database?
    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Time and Attendance")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
    } else {

        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {
            // If this request was sent in to request one's own hours, the getAttribute("selectedEmployeeHashCodes") should be cleared.
            if ((request.getParameter("self") !=null) && (request.getParameter("self").compareToIgnoreCase("true") ==0)){
                request.getSession().removeAttribute("selectedEmployeeHashCodes");
            }
            //
            // Do we have anything from Pageproducer_EmployeeSelection in the form of multiple-selected employees?
            // If so, I need to convert those request parameters to session objects so that it can be passed on to PageProducer_TnADailySignoff
            //

            // But first, how about those already stored in a session?
            java.util.LinkedList<String> selectedEmployeeHashCodes = new java.util.LinkedList<String>();
            if (request.getSession().getAttribute("selectedEmployeeHashCodes") != null) {
                selectedEmployeeHashCodes = (java.util.LinkedList<String>)request.getSession().getAttribute("selectedEmployeeHashCodes");
                //System.out.println("Session's selectedEmployeeHashCodes already exists");
            }

            for (int i = 0; i < 1000; i++) {
                if (request.getParameter("multiEmp_" + i) != null) {
                    String selectedEmp = request.getParameter("multiEmp_" + i);
                    //System.out.println("request.getParameter(\"multiEmp_" + i + "\")" + request.getParameter("multiEmp_" + i) + "=" + selectedEmp);
                    selectedEmployeeHashCodes.add(selectedEmp);
                }
            }
            request.getSession().setAttribute("selectedEmployeeHashCodes", selectedEmployeeHashCodes);



            // T&A Header Information
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            if (selectedEmployeeHashCodes.size() > 0) {
                //
                // Multiple Employees
                //
                out.write(gen.buildHTMLPageHeader("T&A Daily Signoff Request (Multiple Employees)", "By "+user.toString()));
            } else if (user.equals(employee)) {
                //
                // Single employee - for himself / herself
                //
                out.write(gen.buildHTMLPageHeader("T&A Daily Signoff Request", employee.toString()));
            } else {
                //
                // Single manager - for one employee
                //
                out.write(gen.buildHTMLPageHeader("T&A Daily Signoff Request", "On Behalf Of: " + employee.toString()));
            }

            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String selectionID = request.getParameter("selSelection");

            //
            // Page Producer
            //
            PageProducer_TnADailySignoff producer;
            if (selectedEmployeeHashCodes.size() > 0) {
                //
                // Multiple Employees
                //
                //System.out.println("MultipleEmployees");
                producer = new PageProducer_TnADailySignoff(user, fromDate, toDate, request.getSession(), selectionID);
            } else {
                //
                // Single employee
                //                
                //System.out.println("SingleEmployee");
                producer = new PageProducer_TnADailySignoff(user, employee, fromDate, toDate, request.getSession(), selectionID);
            }
            out.write(producer.generateInformationContent());


        }
    }
%>

<%@include file="html_bottom.jsp"%>