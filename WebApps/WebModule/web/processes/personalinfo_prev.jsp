<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Page Top
        // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
        if (!user.equals(employee)) {
            loginSessionObject.setEmployee(user);
        } else {

            if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Personal Information")) {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
            } else {

                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Personal Information", employee.toString()));

                // Page Detail
                PageProducer_PersonalInfo producer = new PageProducer_PersonalInfo(employee);
                out.write(producer.generateInformationContent());
            }
        }
%>

<%@include file="html_bottom.jsp"%>