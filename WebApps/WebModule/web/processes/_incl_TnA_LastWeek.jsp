

<%

        // Define Chart
        strBXML = new StringBuffer();
        sqlStatement = new StringBuffer();
        //String fromDate = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -7));
        //String toDate = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -1));

        strBXML.append("<");
        strBXML.append("chart caption='First In - Last Out ("+fromDate+" - "+toDate+") ' ");
        strBXML.append("subCaption ='Last 7 days' ");
        strBXML.append("xAxisName='Day' ");
        strBXML.append("yAxisName='Time'");
        strBXML.append("startAngX='20' startAngY='-30' ");
        strBXML.append("endAngX='10' endAngY='-6' ");
        strBXML.append("showValues='0' ");
        strBXML.append("showYAxisValues='1' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append(">");


        za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));


        try {
            con = object.getNewConnectionFromPool();
            sqlStatement.append("select dayname(day) as DayName, day, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(FirstIn), FirstIn),0) as  FirstIn_Num, ");
            sqlStatement.append("right('00'||datepart(hh,FirstIn),2)||':'||right('00'||datepart(mi,FirstIn),2) as FirstIn_Displ, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(LastOut), LastOut),0) as  LastOut_Num, ");
            sqlStatement.append("right('00'||datepart(hh,LastOut),2)||':'||right('00'||datepart(mi,LastOut),2) as LastOut_Displ, ");
            sqlStatement.append("COALESCE(TimeAtWork_ActualMinutes,0) as TimeAtWork_ActualMinutes, TimeAtWork_DisplayValue ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (nolock) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=" + employee.getEmployeeID() + ") ");
            sqlStatement.append("and day between dateadd(dd, -8, now()) and dateadd(dd, -1, now()) ");
            sqlStatement.append("order by day ");

            java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);

            // Build CATEGORIES
            strBXML.append("<categories>");
            while (rs.next()) {
                strBXML.append("<category label='" + rs.getString("DayName") + "'/>");
            }
            strBXML.append("</categories>");

            // Build Series: Last Out
            rs.first();
            strBXML.append("<dataset seriesName='Last Out' renderAs='COLUMN' showValues='0'>");
            strBXML.append("<set value='" + new Float(rs.getInt("LastOut_Num") / 60).toString() + "' toolText='Clocked Out{br}" + rs.getString("LastOut_Displ") + "' displayValue='" + rs.getString("LastOut_Displ") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + new Float(rs.getInt("LastOut_Num") / 60).toString() + "' toolText='Clocked Out{br}" + rs.getString("LastOut_Displ") + "' displayValue='" + rs.getString("LastOut_Displ") + "' />");
            }
            strBXML.append("</dataset>");


            // Build Series: Time At Work
            rs.first();
            strBXML.append("<dataset seriesName='Time At Work' renderAs='LINE' showValues='0' >");
            strBXML.append("<set value='" + new Float(rs.getInt("TimeAtWork_ActualMinutes") / 60).toString() + "' toolText='Time in office{br}" + rs.getString("TimeAtWork_DisplayValue") + "' displayValue='" + rs.getString("TimeAtWork_DisplayValue") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + new Float(rs.getInt("TimeAtWork_ActualMinutes") / 60).toString() + "' toolText='Time in office{br}" + rs.getString("TimeAtWork_DisplayValue") + "' displayValue='" + rs.getString("TimeAtWork_DisplayValue") + "' />");
            }
            strBXML.append("</dataset>");

            // Build Series: First In
            rs.first();
            strBXML.append("<dataset seriesName='First In' renderAs='COLUMN' showValues='0'>");
            strBXML.append("<set value='" + new Float(rs.getInt("FirstIn_Num") / 60).toString() + "' toolText='Clocked In{br}" + rs.getString("FirstIn_Displ") + "' displayValue='" + rs.getString("FirstIn_Displ") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + new Float(rs.getInt("FirstIn_Num") / 60).toString() + "' toolText='Clocked In{br}" + rs.getString("FirstIn_Displ") + "' displayValue='" + rs.getString("FirstIn_Displ") + "' />");
            }
            strBXML.append("</dataset>");

            strBXML.append("<styles><definition>");
            strBXML.append("   <style name='bgAnim' type='animation' param='_xScale' start='0' duration='1'/>");
            strBXML.append("</definition><application><apply toObject='BACKGROUND' styles='bgAnim'/></application></styles>");


            strBXML.append("</chart>");
            strXML = strBXML.toString();
            rs.close();
        } finally {
            object.releaseConnection(con);
        }

%>
<DIV ALIGN="CENTER"><jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/MSCombi3D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="750" />
        <jsp:param name="chartHeight" value="300" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>
</DIV>

