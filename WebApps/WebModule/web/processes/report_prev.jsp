<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html"%>

<%@page import="java.util.*"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.ParseException"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Am I allowed connect to the database?
    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Leave")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
    } else {

        boolean showReport = true;

        String reportNb = request.getParameter("reportNum");
        if (reportNb == null) {
            reportNb = "0";
        }

        String fromDate = request.getParameter("fromDate");
        if (reportNb.compareTo("ESSL010") != 0) {
            if (fromDate == null) {
                fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(new java.util.Date(), -30));
                showReport = false;
            }
        }

        String toDate = request.getParameter("toDate");
        if (toDate == null) {
            toDate = DatabaseObject.getCurrentDate();
            showReport = false;
        }

        String processName = request.getParameter("processName");
        if (processName == null) {
            processName = "Leave Request";
        }

        String activeYN = request.getParameter("activeYN");
        if (activeYN == null) {
            activeYN = "Y";
        }

        String rsSelection = request.getParameter("rsSelection");
        if (rsSelection == null) {
            rsSelection = "All";
        }

        //
        // Remember the selections
        //
        out.write("<script>$(document).ready(function() {");
        out.write("var item = window.localStorage.getItem('processName');");
        out.write("$('select[name=processName]').val(item);");
        out.write("$('select[name=processName]').change(function() {");
        out.write("window.localStorage.setItem('processName', $(this).val());");
        out.write("});");
        out.write("});</script>");

        out.write("<script>$(document).ready(function() {");
        out.write("var item = window.localStorage.getItem('activeYN');");
        out.write("$('select[name=activeYN]').val(item);");
        out.write("$('select[name=activeYN]').change(function() {");
        out.write("window.localStorage.setItem('activeYN', $(this).val());");
        out.write("});");
        out.write("});</script>");

        out.write("<script>$(document).ready(function() {");
        out.write("var item = window.localStorage.getItem('rsSelection');");
        out.write("$('select[name=rsSelection]').val(item);");
        out.write("$('select[name=rsSelection]').change(function() {");
        out.write("window.localStorage.setItem('rsSelection', $(this).val());");
        out.write("});");
        out.write("});</script>");

        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            String submissionPeriod = "";

            if (reportNb.contains("ESS_IRP5")) {
                ////////////////////////////////////////////////////////////////
                ///////////               IRP5                     /////////////
                ////////////////////////////////////////////////////////////////

                // Filecontainer
                FileContainer fc = FileContainer.getInstance();
                // Payroll
                Payroll payroll = employee.getPayroll();

                if (payroll == null) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "You are not currently linked to a Payroll.",
                            false, request.getSession());
                    producer.setMessageInSessionObject();
                    out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                } else {

                    // Submission Period selected through URL
                    submissionPeriod = request.getParameter("submissionPeriod");

                    // Submission Periods in database
                    java.util.LinkedList submissionPeriods = fc.getTaxCertificateYears(employee);

                    // Period Detail
                    if (submissionPeriod == null) {
                        // Get the first submission period
                        if (submissionPeriods.size() > 0) {
                            submissionPeriod = (String) submissionPeriods.getFirst();
                        } else {
                            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                    "Unable to find tax return submissions for " + employee.getPreferredName() + " " + employee.getSurname(),
                                    false, request.getSession());
                            producer.setMessageInSessionObject();
                            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
                            return;
                        }
                    }
                    submissionPeriod = submissionPeriod.replace("/", "_");
                    //System.out.println("submissionPeriod :" + submissionPeriod);

                    // Page Header
                    //PageSectionGenerator gen = PageSectionGenerator.getInstance();
                    out.write(gen.buildHTMLPageHeader("Tax Certificates", employee.toString()));

                    // Now we create a tab selection for the different groups
                    java.util.TreeMap periodList = new java.util.TreeMap();  // use a TreeMap to sort the dates chronologically
                    java.util.Iterator iter = submissionPeriods.iterator();
                    while (iter.hasNext()) {
                        String aPeriod = (String) iter.next();
                        periodList.put(aPeriod, aPeriod);
                    }

                    //out.write("<h3 class=\"shaddow\">Certificate Period: </h3><CENTER class=\"taxCertificatePeriod\">");
                    out.write("<CENTER><h3 class=\"shaddow\" style=\"display: inline-block;\">Certificate Period: </h3>");

                    StringBuilder selection = new StringBuilder();
                    selection.append("<SELECT  style=\"display: inline-block; margin-left: 5px;\" name=\"balanceDate\" ONCHANGE=\"location = this.options[this.selectedIndex].value;\">");

                    // Once all the periods are loaded, we can construct the tab buttons
                    // We will list a maximum of 6 periods in a line
                    java.util.Iterator periodIter = periodList.values().iterator();
                    while (periodIter.hasNext()) {
                        String periodDate = (String) periodIter.next();
                        // For fear of messing up the URL string, we'll replace '2010/08' with '2010_08'

                        if (submissionPeriod.compareTo(periodDate.replace("/", "_")) == 0) {
                            selection.append(" <option value=\"report_prev.jsp?reportNum=ESS_IRP5&submissionPeriod=" + periodDate.replace("/", "_") + " \" selected  >" + periodDate + "</option>");
                        } else {
                            selection.append(" <option value=\"report_prev.jsp?reportNum=ESS_IRP5&submissionPeriod=" + periodDate.replace("/", "_") + " \" >" + periodDate + "</option>");
                        }
                    }
                    selection.append("</select></CENTER>");
                    out.write(selection.toString());

                    // Page Detail
                    if (submissionPeriod == null) {
                        System.out.println("Null submissionPeriod");
                    } else {
                        out.write("<br><hr width=\"80%\" size=\"1\" color=\"#c0c0c0\"><br>");
                        showReport = true;
                    }

                }
            // End of IRP5
            // Applies to all other reports except IRP5
            } else {
                // Display page header with logo
                out.write(gen.buildHTMLPageHeader("Report Preview", employee.toString()));

                out.write("<center>");
                if (reportNb.contains("ESSL011")) {

                    out.write("<h3 class=\"shaddow\">This report calculates the Annual Projected Forfeit on the current date</h3>");
                    //
                    // Date Selection
                    //
                    out.write("<FORM name=\"reportFilter\" METHOD=POST action=\"report_prev.jsp?reportNum=");
                    out.write(reportNb);
                    out.write("\"> ");
                    //out.write("<table align='center'><tr align='center'>");
                    out.write("<div style=\"display: inline-block; padding:10px;\"><b>Analyse Annual Leave Projected Forfeit as on &nbsp;&nbsp;" + DatabaseObject.getCurrentDate() + "</b></div>");
                    out.write("<div style=\"display: hidden;\"><input type=\"hidden\" name=\"fromDate\" value=\"ESSL011\"></div>");
                    //
                    // Reporting Structure Selections 
                    //
                    out.write("<div style=\"display: inline-block; padding:10px 20px 10px 10px;\"><b>Employee Selection Rule&nbsp;&nbsp;</b>");
                    out.write("<select name=\"rsSelection\" id=\"rsSelection\" style=\"max-width:950px;\">");
                    out.write("<option value=\"All\" selected>All</option>");

                    WebProcessDefinition webProcessDefinition = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
                    ReportingStructure rs = webProcessDefinition.getReportingStructure();
                    ArrayList<String> esList = new ArrayList();
                    ArrayList<EmployeeSelection> topNodes = rs.getTopNodes();
                    for (EmployeeSelection topNode : topNodes) {
                        LinkedList<EmployeeSelectionRule> childEmployeeSelectionRules = rs.getAllChildEmployeeSelections(topNode);
                        for (EmployeeSelectionRule es : childEmployeeSelectionRules) {
                            if (rs.isUserParentOfSelection(user, es)) {
                                if (!esList.contains(es.getName())) {
                                    out.write("<option value=\"" + es.getName() + "\">" + es.getName() + "</option>");
                                }
                                esList.add(es.getName());
                            }
                        }
                    }

                    out.write("</select>");
                    out.write("</div>");
                // End of ESSL011
                // All other reports except ESSL011
                } else {
                    out.write("<h3 class=\"shaddow\">Please select report filters</h3>");
                    //
                    // Date Selection
                    //
                    out.write("<FORM name=\"reportFilter\" METHOD=POST action=\"report_prev.jsp?reportNum=");
                    out.write(reportNb);
                    out.write("\"> ");
                    //
                    // From Date and To Date Picker (START)
                    //
                    if (reportNb.contains("ESSTM")) {
                        //
                        // Reporting Structure Selections - Time reports
                        //
                        out.write("<div style=\"display: inline-block; padding:10px 20px 10px 10px;\"><b>Employee Selection Rule&nbsp;&nbsp;</b>");
                        out.write("<select name=\"rsSelection\" id=\"rsSelection\" style=\"max-width:950px;\">");
                        out.write("<option value=\"All\" selected>All</option>");

                        WebProcessDefinition webProcessDefinition = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
                        ReportingStructure rs = webProcessDefinition.getReportingStructure();
                        ArrayList<String> esList = new ArrayList();
                        ArrayList<EmployeeSelection> topNodes = rs.getTopNodes();
                        for (EmployeeSelection topNode : topNodes) {
                            LinkedList<EmployeeSelectionRule> childEmployeeSelectionRules = rs.getAllChildEmployeeSelections(topNode);
                            for (EmployeeSelectionRule es : childEmployeeSelectionRules) {
                                if (rs.isUserParentOfSelection(user, es)) {
                                    if (!esList.contains(es.getName())) {
                                        out.write("<option value=\"" + es.getName() + "\">" + es.getName() + "</option>");
                                    }
                                    esList.add(es.getName());
                                }
                            }
                        }

                        out.write("</select>");
                        out.write("</div>");
                    }// End time reports employee selection filter
                    
                    if (reportNb.contains("ESSL010")) {
                        //
                        // Reporting Structure Selections - Leave Reports
                        //
                        out.write("<div style=\"display: inline-block; padding:10px 20px 10px 10px;\"><b>Employee Selection Rule&nbsp;&nbsp;</b>");
                        out.write("<select name=\"rsSelection\" id=\"rsSelection\" style=\"max-width:950px;\">");
                        out.write("<option value=\"All\" selected>All</option>");

                        WebProcessDefinition webProcessDefinition = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
                        ReportingStructure rs = webProcessDefinition.getReportingStructure();
                        ArrayList<String> esList = new ArrayList();
                        ArrayList<EmployeeSelection> topNodes = rs.getTopNodes();
                        for (EmployeeSelection topNode : topNodes) {
                            LinkedList<EmployeeSelectionRule> childEmployeeSelectionRules = rs.getAllChildEmployeeSelections(topNode);

                            for (EmployeeSelectionRule es : childEmployeeSelectionRules) {
                                if (rs.isUserParentOfSelection(user, es)) {
                                    if (!esList.contains(es.getName())) {
                                        out.write("<option value=\"" + es.getName() + "\">" + es.getName() + "</option>");
                                    }
                                    esList.add(es.getName());
                                }
                            }
                        }

                        out.write("</select>");
                        out.write("</div>");

                        out.write("<div style=\"display: inline-block; padding:10px;\" width=\"100\"  align=\"right\" ><b>Analysis date</b></div>");

                    } else {
                        if (reportNb.contains("ESSP1190")) {
                            //
                            // Reporting Structure Selections - All
                            //
                            out.write("<div style=\"display: inline-block; padding:10px;\"><b>Employee Selection Rule&nbsp;&nbsp;</b>");
                            out.write("<select name=\"rsSelection\" id=\"rsSelection\" style=\"max-width:950px;\">");
                            out.write("<option value=\"All\" selected>All</option>");

                            ArrayList<ReportingStructure> reportingStructures = fileContainer.getReportingStructures();
                            ArrayList<String> esList = new ArrayList();
                            for (ReportingStructure rs : reportingStructures) {
                                ArrayList<EmployeeSelectionRule> employeeSelectionRules = fileContainer.getEmployeeSelections();
                                for (EmployeeSelectionRule es : employeeSelectionRules) {
                                    if (rs.isUserParentOfSelection(user, es)) {
                                        if (!esList.contains(es.getName())) {
                                            out.write("<option value=\"" + es.getName() + "\">" + es.getName() + "</option>");
                                        }
                                        esList.add(es.getName());
                                    }
                                }
                            }

                            out.write("</select>");
                            out.write("</div>");
                            // End employee selections filter

                            out.write("<div style=\"display: inline-block; padding:10px;\"><b>Process&nbsp;&nbsp;</b>");
                            out.write("<select name=\"processName\" id=\"processName\" style=\"width:145px;\">");
                            out.write("<option value=\"All\" selected>All</option>");

                            DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));
                            Connection con = null;
                            try {
                                con = DatabaseObject.getNewConnectionFromPool();
                                ResultSet rs = DatabaseObject.openSQL("SELECT DISTINCT PROCESS_NAME FROM ESS_PROCESS ORDER BY PROCESS_NAME", con);
                                while (rs.next()) {
                                    out.write("<option value=\"" + rs.getString(1) + "\">" + rs.getString(1) + "</option>");
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            } finally {
                                DatabaseObject.releaseConnection(con);
                            }
                            out.write("</select>");
                            out.write("</div>");
                            out.write("<div style=\"display: inline-block; padding:10px;\"><b>Active/Inactive&nbsp;&nbsp;</b>");
                            out.write("<select name=\"activeYN\" id=\"activeYN\" style=\"width:145px;\">");
                            out.write("<option value=\"All\" selected>All</option>");
                            out.write("<option value=\"Y\">Active</option>");
                            out.write("<option value=\"N\">Inactive</option>");
                            out.write("</select>");
                            out.write("</div>");
                        }
                        //
                        // From Date
                        //
                        out.write("<div style=\"display: inline-block; padding:10px;\" width=\"50\" align=\"right\" ><span><b>From</b></span>");

                        out.write("<span style=\"display: inline-block; padding:10px;\" width=\"160\"><input type=\"text\" maxlength=10 name=\"fromDate\" value=\"");
                        out.write(fromDate);
                        out.write("\" size=\"10\" ");
                        //
                        // Seems that javascript is missing
                        //
                        out.write(" onFocus=\"javascript:vDateType='2'\"");
                        out.write(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                        out.write(" placeholder=\"yyyy/mm/dd\">");

                        out.write(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar10')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></span></div>");
                        //
                        // To Date
                        //
                        out.write("<div style=\"display: inline-block; padding:10px;\" width=\"25\"  align=\"right\" ><span><b>To</b></span>");
                    }
                    out.write("<span style=\"display: inline-block; padding:10px;\" width=\"180\" ><input type=\"text\" maxlength=10 name=\"toDate\" value=\"");
                    out.write(toDate);
                    out.write("\" size=\"10\" ");
                    //
                    // Seems that javascript is missing
                    //
                    out.write(" onFocus=\"javascript:vDateType='2'\"");
                    out.write(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                    out.write(" placeholder=\"yyyy/mm/dd\">");

                    out.write(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date.', this, event, '130px')\" href=\"javascript:showCal('Calendar11')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a></span></div>");
                }
                //            
                //Button
                //
                out.write("<div  style=\"display: inline-block; padding-bottom:5px; vertical-align:middle;\"><input class=\"gradient-button\" type=\"submit\" value=\"Generate Report\" ></div>");

                //
                // From Date and To Date Picker (END)
                //
                out.write("</form><hr width=\"80%\" size=\"1\" color=\"#c0c0c0\"><br>");
                out.write("</center>");

                if (!reportNb.contains("ESSL011")) {
                    //Perform simple date validation
                    if (reportNb.compareTo("ESSL010") == 0) {
                        //Perform simple date validation
                        if ((toDate == null) || (toDate.trim().length() == 0)) {
                            out.write("<center><h3>You must enter a <b>To Date</b>.</h3></center>");
                            return;
                        }
                    } else {

                        if ((fromDate == null) || (toDate == null) || (fromDate.trim().length() == 0) || (toDate.trim().length() == 0)) {
                            out.write("<center><h3>You must enter a <b>From Date</b> and a <b>To Date</b>.</h3></center>");
                            return;
                        }
                    }
                    // Perform date format validation
                    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                    if (reportNb.compareTo("ESSL010") == 0) {
                        // To Date
                        try {
                            df.parse(toDate);
                        } catch (ParseException e) {
                            out.write("<center><h3>You must enter a valid <b>To Date</b><br>The required format is:<b>yyyy/mm/dd</b>.</h3></center>");
                            System.out.println(e);
                            return;
                        }
                    } else {

                        // From Date
                        try {
                            df.parse(fromDate);
                        } catch (ParseException e) {
                            out.write("<center><h3>You must enter a valid <b>From Date</b><br>The required format is:<b>yyyy/mm/dd</b>.</h3></center>");
                            System.out.println(e);
                            return;
                        }

                        // To Date
                        try {
                            df.parse(toDate);
                        } catch (ParseException e) {
                            out.write("<center><h3>You must enter a valid <b>To Date</b><br>The required format is:<b>yyyy/mm/dd</b>.</h3></center>");
                            System.out.println(e);
                            return;
                        }
                    }
                    if (reportNb.compareTo("ESSL010") != 0) {
                        // fromDate before toDate
                        if (DatabaseObject.formatDate(fromDate).after(DatabaseObject.formatDate(toDate))) {
                            out.write("<center><h3>From Date cannot be later than To Date.</h3></center>");
                            return;
                        }
                    }
                }
                if (reportNb.contains("ESSL011") && fromDate.contains("ESSL011")) {
                    showReport = true;
                }
            } // End of all reports

            // Print a warning for duplicate employee numbers only to top level managers
            if (fileContainer.duplicateEmployeeNumbers().length() > 2 && (!reportNb.contains("ESS_IRP5") || !reportNb.contains("ESSP1102_OLD") || !reportNb.contains("ESSP1102"))) {
                // Get Web Process Definitions
                ArrayList<WebProcessDefinition> webProcessDefinitions_collection = fileContainer.getWebProcessDefinitions();
                boolean isAtTop = false;
                for (WebProcessDefinition def : webProcessDefinitions_collection) {
                    if (fileContainer.isEmployeeInTopLevel(employee, def)) {
                        isAtTop = true;
                    }
                }
                if (isAtTop) {
                    out.write("<center><h3><span style=\"color: red;\">There are duplicate employee numbers on the database.<br>Please investigate employee number(s): "
                            + fileContainer.duplicateEmployeeNumbers() + "</span></h3></center>");
                }
            }

            //
            // Generate report if button is clicked
            //
            if (showReport) {
                if (reportNb.compareTo("ESSL010") == 0) {
                    out.write("<center><iframe id='iframe' style=\"width: 80%; height: 1170px;\" src=\"report_app.jsp?reportNum="
                            + reportNb
                            + "&toDate="
                            + toDate
                            + "&rsSelection="
                            + rsSelection
                            + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");
                } else if (reportNb.contains("ESSL011")) {
                    out.write("<center><iframe id='iframe' style=\"width: 80%; height: 1170px;\" src=\"report_app.jsp?reportNum="
                            + reportNb
                            + "&rsSelection="
                            + rsSelection
                            + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");
                } else if (reportNb.contains("ESS_IRP5")) {
                    out.write("<center><iframe id='iframe' style=\"width: 80%; height: 1170px;\" src=\"report_app.jsp?reportNum="
                            + reportNb
                            + "&submissionPeriod="
                            + submissionPeriod
                            + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");
                } else {
                    out.write("<center><iframe id='iframe' style=\"width: 80%; height: 1170px;\" src=\"report_app.jsp?reportNum="
                            + reportNb
                            + "&fromDate="
                            + fromDate
                            + "&toDate="
                            + toDate
                            + "&processName="
                            + processName
                            + "&activeYN="
                            + activeYN
                            + "&rsSelection="
                            + rsSelection
                            + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");
                }
            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>