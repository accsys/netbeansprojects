

<%

            {
                // Define Chart
                strBXML = new StringBuffer();
                sqlStatement = new StringBuffer();

                strBXML.append("<");
                strBXML.append("chart caption='Monthly Leave Overview: Annual Leave' ");
                strBXML.append("xAxisName='Month' ");
                strBXML.append("PYAxisName='Days Taken'");
                if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideLeaveBalanceFor("Annual")) {
                    strBXML.append("SYAxisName='Days Available'");
                } else {
                    strBXML.append("SYAxisName=''");
                }
                strBXML.append("showValues='0' ");
                strBXML.append("showAboutMenuItem='0' ");
                strBXML.append("formatNumberScale='0' ");
                strBXML.append("bgColor='D8E6E9,FFFFFF' ");
                strBXML.append("bgAlpha='100, 100' ");
                strBXML.append("showBorder='1' ");
                strBXML.append("canvasbgAlpha='30' ");
                strBXML.append("labelDisplay='ROTATE' ");
                strBXML.append("outCnvBaseFont='Verdana' ");
                strBXML.append("numVisiblePlot='12' ");
                strBXML.append("useRoundEdges='1' ");
                strBXML.append(">");


                za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));
                try {
                    con = object.getNewConnectionFromPool();
                    sqlStatement.append("select distinct ( MonthName(DAY)||' '||DatePart(yy,DAY)) as MonthTaken, ");
                    sqlStatement.append("\"date\"(DAY-DatePart(dd,DAY)+1) as DateOrdered, ");
                    // Annual Leave
                    sqlStatement.append(" (select COALESCE(sum(nb_unit),0) from l_history where start_date between DateOrdered and "
                            + "dateadd(mm, 1, DateOrdered) and company_id=e.company_id and employee_id=e.employee_id "
                            + "and leave_type_id=1) as AnnualLeave,");
                    // Balance
                    sqlStatement.append(" fn_L_EstimateBalance(company_id, employee_id, 1, DateOrdered) as Balance ");

                    sqlStatement.append(" from s_calendar_days s, e_master e where ");
                    sqlStatement.append("   Company_id=" + employee.getCompany().getCompanyID());
                    sqlStatement.append("   and Employee_id=" + employee.getEmployeeID());
                    sqlStatement.append(" and DateOrdered between dateadd(mm,-18,now()) and dateadd(mm,6,now()) ");
                    sqlStatement.append(" order by DateOrdered ");


                    java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);

                    // Build the CATEGORIES
                    strBXML.append("<categories>");
                    while (rs.next()) {
                        String monthTaken = rs.getString("MonthTaken");
                        strBXML.append("<category label='" + monthTaken + "' />");
                    }
                    strBXML.append("</categories>");

                    // Build dataset-ANNUAL LEAVE
                    if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideLeaveBalanceFor("Annual")) {
                        strBXML.append("<dataset seriesName='Days Taken'>");
                        rs.first();
                        strBXML.append("<set value='" + new Float(rs.getFloat("AnnualLeave")).toString() + "' />");
                        while (rs.next()) {
                            strBXML.append("<set value='" + new Float(rs.getFloat("AnnualLeave")).toString()
                                    + "' toolText='" + new Float(rs.getFloat("AnnualLeave")).toString() + " Days{br}Annual Leave{br}taken' />");
                        }
                        strBXML.append("</dataset>");
                    }

                    // Build dataset-Balance
                    strBXML.append("<dataset seriesName='Days Available' parentYAxis='S'>");
                    rs.first();
                    strBXML.append("<set value='" + new Float(rs.getFloat("Balance")).toString() + "' />");
                    while (rs.next()) {
                        strBXML.append("<set value='" + new Float(rs.getFloat("Balance")).toString() + "' />");
                    }
                    strBXML.append("</dataset>");


                    strBXML.append("</chart>");
                    rs.close();
                    strXML = strBXML.toString();
                } finally {
                    object.releaseConnection(con);
                }
            }

%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/ScrollCombiDY2D.swf" />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="550" />
        <jsp:param name="chartHeight" value="350" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>


