<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<!--  ******************** 
        A default screen to simply display any messages 
      ******************** 
-->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (loginSessionObject == null) {
            response.sendRedirect(response.encodeURL("Login.jsp"));
        }
    }
    // Page Header
    PageSectionGenerator gen = PageSectionGenerator.getInstance();
    out.write(gen.buildHTMLPageHeader("Message", employee.toString()));
%>

<%@include file="html_messages.jsp"%> <!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%@include file="html_bottom.jsp"%>