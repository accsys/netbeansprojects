<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>
<!-- Now we need to verify that there is a LoginSessionObject -->

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%  // Can I connect to the database?
        if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Company Statistics")) {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
        } else {

            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Company Statistics", employee.getCompany().getName()));

            StringBuffer sqlStatement = new StringBuffer();
            String strXML = new String();
            StringBuffer strBXML = new StringBuffer();
            za.co.ucs.lwt.db.DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();
            java.sql.Connection con = null;


%>
<center>
    <table width="90%">
        <tr>
            <td>
                <table align="center">
                    <tr>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">Staff complement over the last 2 years</h3>
                                            <br>
                                            <%@include file="_incl_employeeStrength_chart.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="4%"></td>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">Current Race and Gender distribution</h3>
                                            <br>
                                            <%@include file="_incl_employeeStrength_PerRaceGender.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
<br><div style="text-align: center;"><small>Note: <a href="http://www.adobe.com/products/flashplayer/">Adobe Flash Player 8 </a>or later required</small></div><br>
<%
        }
%>

<%@include file="html_bottom.jsp"%>