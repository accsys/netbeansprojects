<%@page contentType="text/html"%>
<%@include file="html5_top.jsp"%>
<!-- Now we need to verify that there is a LoginSessionObject -->
<%    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        out.write("<center>");
// Page Top
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("ESS Reporting Structure Setup", ""));

// Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
        } else {

// Get LoggedIn Employee
            try {
                if (!za.co.ucs.accsys.webmodule.FileContainer.getInstance().isEmployeeWebAdministrator(employee)) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "You must belong to the 'Web Administrator' group in Peopleware in <br>order to "
                            + "view this page", true, request.getSession());
                    producer.setMessageInSessionObject();
                    out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
                } else {
                    PageProducer_WebSetup producer = new PageProducer_WebSetup(fileContainer);
                    out.write(producer.generateInformationContent());
                }
            } catch (NullPointerException e) {
                System.out.println("******************************************");
                System.out.println("******************************************");
                System.out.println("            WEB ADMIN DISCHARGED          ");
                System.out.println("******************************************");
                System.out.println("******************************************");
                System.out.println("Exception: " + e);
            }
        }
    }
%>
</CENTER>
<%@include file="html_messages.jsp"%> <!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->
<%@include file="html_bottom.jsp"%>