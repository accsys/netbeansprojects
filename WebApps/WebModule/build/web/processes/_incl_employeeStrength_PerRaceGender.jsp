

<%

        // Define Chart
        strBXML = new StringBuffer();
        sqlStatement = new StringBuffer();

        strBXML.append("<");
        strBXML.append("chart caption='Employee Strength: By Type' ");
        strBXML.append("useRoundEdges='1' ");
        strBXML.append("showValues='1' ");
        strBXML.append("showSum='1' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("formatNumberScale='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("showZeroPies ='0' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("xAxisName='Month'");
        strBXML.append("yAxisName='Number of Employees'");
        strBXML.append("numVisiblePlot='18' ");
        strBXML.append(">");


        object.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));

        try {
            con = object.getNewConnectionFromPool();

            // Start by loading the employee groups
            sqlStatement.append("select ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='African' and GENDER='Male') as AfricanMale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='African' and GENDER='Female') as AfricanFemale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Coloured' and GENDER='Male') as ColouredMale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Coloured' and GENDER='Female') as ColouredFemale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='White' and GENDER='Male') as WhiteMale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='White' and GENDER='Female') as WhiteFemale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Indian' and GENDER='Male') as IndianMale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Indian' and GENDER='Female') as IndianFemale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Unconfirmed race' and GENDER='Male') as UnknownMale, ");
            sqlStatement.append("(select count(*) from vw_CUSTOM_Employee_Demographics where  DATE_OF_TERMINATION is null and company_id=c.company_id and RACE='Unconfirmed race' and GENDER='Female') as UnknownFemale ");
            sqlStatement.append("from c_master c where Company_id=" + employee.getCompany().getCompanyID());

            java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);
            // Build the SETS
            rs.first();

            strBXML.append("<set label='African-Male'   value='" + rs.getString("AfricanMale") + "'/>");
            strBXML.append("<set label='African-Female' value='" + rs.getString("AfricanFemale") + "'/>");
            strBXML.append("<set label='Coloured-Male' value='" + rs.getString("ColouredMale") + "'/>");
            strBXML.append("<set label='Coloured-Female' value='" + rs.getString("ColouredFemale") + "'/>");
            strBXML.append("<set label='White-Male' value='" + rs.getString("WhiteMale") + "'/>");
            strBXML.append("<set label='White-Female' value='" + rs.getString("WhiteFemale") + "'/>");
            strBXML.append("<set label='Indian-Male' value='" + rs.getString("IndianMale") + "'/>");
            strBXML.append("<set label='Indian-Female' value='" + rs.getString("IndianFemale") + "'/>");
            strBXML.append("<set label='Unknown-Male' value='" + rs.getString("UnknownMale") + "'/>");
            strBXML.append("<set label='Unknown-Female' value='" + rs.getString("UnknownFemale") + "'/>");

            rs.close();
        } finally {
            object.releaseConnection(con);
        }
        strBXML.append("</chart>");
        strXML = strBXML.toString();
%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/Doughnut2D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="WMode" value="Transparent" />
        <jsp:param name="chartWidth" value="580" />
        <jsp:param name="chartHeight" value="300" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>
 

