<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%  // Can I connect to the database?
    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Time and Attendance")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
    } else {

        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {
            // If this request was sent in to request one's own hours, the getAttribute("selectedEmployeeHashCodes") should be cleared.
            if ((request.getParameter("self") !=null) && (request.getParameter("self").compareToIgnoreCase("true") ==0)){
                request.getSession().removeAttribute("selectedEmployeeHashCodes");
            }

            // T&A Header Information
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            if (user.equals(employee)) {
                //
                // Single employee - for himself / herself
                //
                out.write(gen.buildHTMLPageHeader("T&A Daily Signoff Request", employee.toString()));
            } else {
                //
                // Single manager - for one employee
                //
                out.write(gen.buildHTMLPageHeader("T&A Daily Signoff Request", "On Behalf Of: " + employee.toString()));
            }

            String detailDate = request.getParameter("detailDate");
            
            //
            // Page Producer
            //
            PageProducer_TnADailySignoffDetail producer;
                //
                // Single employee
                //                
                //System.out.println("SingleEmployee");
                producer = new PageProducer_TnADailySignoffDetail(user, employee, detailDate, request.getSession());
            out.write(producer.generateInformationContent());


        }
    }
%>

<%@include file="html_bottom.jsp"%>