<%@page import="java.awt.Color"%>
<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%>
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!--  ******************** 
        Preview Leave Balances for all employees in the accompanied
      ******************** 
-->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Page Top
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        // Get parameters
        String shiftID = request.getParameter("shiftID");
        String originalURI = request.getRequestURI();
        String refDate = request.getParameter("referenceDate"); // for which week?
        String rosterDay = request.getParameter("rosterDay"); // for spesific day?

        // clear the session objects if the rosterDay changed
        String referer = request.getHeader("Referer");
        int index = referer.indexOf("rosterDay=");
        if (index > 0) {
            String prevRosterDay = referer.substring(index + 10, index + 20);
            if (rosterDay != null && !rosterDay.contains(prevRosterDay)) {
                request.getSession().removeAttribute("selectedEmployeeHashCodesToLink");
                request.getSession().removeAttribute("selectedEmployeeHashCodesToUnlink");
            }
        } else {
            request.getSession().removeAttribute("selectedEmployeeHashCodesToLink");
            request.getSession().removeAttribute("selectedEmployeeHashCodesToUnlink");
        }

        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Shift Roster Overview", employee.toString()));

        out.write("<center>");

        // Selection dropdown list begin
        out.write("<nav id=\"menu-wrap\"><ul id=\"menu\" style=\"width: 245px; float: none;\">");
        if (shiftID != null) {
            //out.write("<li><a href=\"" + baseURL + "processes/tna_roster.jsp\">Change shift</a><ul>");
            Shift selectedShift = new Shift(shiftID);
            String webprocessClassName = "webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition";
            out.write("<li><table><tr><td><div id=\"shiftButton\" style=\"background: " + selectedShift.getColor() + ";\"></div></td>");
            out.write("<td><a href=" + baseURL + "processes/tna_roster.jsp?" + webprocessClassName + "&shiftID=" + selectedShift.getShiftID() + ">");
            out.write(selectedShift.getName() + "</a></td></tr></table><ul>");
        } else {
            out.write("<li><a href=\"" + baseURL + "processes/tna_roster.jsp\">Choose a shift</a><ul>");
        }

        // Populate list with shifts
        ShiftRosterInfo sri = fileContainer.getShiftRosterInfoFromContainer(employee);
        for (Shift shift : sri.getAvailableShifts()) {
            if ((shiftID != null) && (shift.getShiftID().contains(shiftID))) {
                continue;
            } else {
                out.write("<li><table><tr><td style=\"background: " + shift.getColor() + "; min-width: 35px; border-radius: 20px;\"></td><td><a href=\"" + baseURL + "processes/tna_roster.jsp?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_DailySignoff&MultiEmployee=true&shiftID=" + shift.getShiftID() + "\">" + shift.getName() + "</a></td></tr></table></li>");
            }
        }
        // Selection dropdown list end
        out.write("</ul></li></ul></nav>");

        out.write("</center>");
        // Contiue only if a shift was selected
        if (shiftID != null) {
            String webProcessClassName = request.getParameter("webProcessClassName"); // Which WebProcess is being used?
            //
            // Group of selections 'under' this one in the form of a TreeList
            //

            // We also need to traverse the sub-groups to instantiate Get a list of all the selections underneath the current user.
            // We need this to forward the appropriate selected selection
            // Page Detail
            WebProcessDefinition webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);

            if (webProcessDefinition != null) {
                //////////////////////////////////////////////////////////////////////////////
                availableProcesses = new java.util.ArrayList(fileContainer.getWebProcessDefinitionsWithSubordinates(loginSessionObject.getUser()).values());
                // Sort by name
                Collections.sort(availableProcesses);
                LinkedList<Employee> allChildEmployees = new LinkedList();
                for (int j = 0; j < availableProcesses.size(); j++) { // Get the next process
                    webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                    if (webProcessDefinition.getProcessClassName().contains(za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName).getProcessClassName())) {
                        if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {

                            boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                            boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));
                            if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                                System.out.println("*** Process outside valid period ***:" + webProcessDefinition + " valid between " + webProcessDefinition.getActiveFromDayOfMonth() + " and " + webProcessDefinition.getActiveToDayOfMonth() + " of the month.");
                                continue;
                            }

                            // Add all child emplyees to the list
                            Collection<Employee> childEmployeesInStructure = fileContainer.getSubordinates(user, webProcessDefinition).values();
                            // Only add each emplyee once
                            for (Employee emp : childEmployeesInStructure) {
                                if (!allChildEmployees.contains(emp)) {
                                    allChildEmployees.add(emp);
                                }
                            }
                        }
                    }
                }

                if (rosterDay != null) {
                    ////////////////////////////////////////////////////////////////////
                    // Employees already ticked to LINK
                    ////////////////////////////////////////////////////////////////////
                    // But first, how about those already stored in a session?
                    java.util.LinkedList<String> selectedEmployeeHashCodesToLink = new java.util.LinkedList();
                    if (request.getSession().getAttribute("selectedEmployeeHashCodesToLink") != null) {
                        selectedEmployeeHashCodesToLink = (java.util.LinkedList<String>) request.getSession().getAttribute("selectedEmployeeHashCodesToLink");
                        //System.out.println("Session's selectedEmployeeHashCodes already exists");
                    }

                    for (int i = 0; i < 1000; i++) {
                        if (request.getParameter("multiEmpToLink_" + i) != null) {
                            String selectedEmp = request.getParameter("multiEmpToLink_" + i);
                            //System.out.println("request.getParameter(\"multiEmp_" + i + "\")" + request.getParameter("multiEmp_" + i) + "=" + selectedEmp);
                            selectedEmployeeHashCodesToLink.add(selectedEmp);
                        }
                    }

                    ////////////////////////////////////////////////////////////////////
                    // Employees already ticked to UNLINK
                    ////////////////////////////////////////////////////////////////////
                    // But first, how about those already stored in a session?
                    java.util.LinkedList<String> selectedEmployeeHashCodesToUnlink = new java.util.LinkedList();
                    if (request.getSession().getAttribute("selectedEmployeeHashCodesToUnlink") != null) {
                        selectedEmployeeHashCodesToUnlink = (java.util.LinkedList<String>) request.getSession().getAttribute("selectedEmployeeHashCodesToUnlink");
                        //System.out.println("Session's selectedEmployeeHashCodes already exists");
                    }

                    for (int i = 0; i < 1000; i++) {
                        if (request.getParameter("multiEmpToUnlink_" + i) != null) {
                            String selectedEmp = request.getParameter("multiEmpToUnlink_" + i);
                            //System.out.println("request.getParameter(\"multiEmp_" + i + "\")" + request.getParameter("multiEmp_" + i) + "=" + selectedEmp);
                            selectedEmployeeHashCodesToUnlink.add(selectedEmp);
                        }
                    }

                    //////////////////////////////////////////////////////////////////////////////
                    // Employees in both
                    /////////////////////////////////////////////////////////////////////////////
                    LinkedList<String> employeesInBoth = new LinkedList();
                    for (String longHash : selectedEmployeeHashCodesToLink) {
                        if (selectedEmployeeHashCodesToUnlink.contains(longHash)) {
                            employeesInBoth.add(longHash);
                        }
                    }
                    for (String toRemove : employeesInBoth) {
                            // Link
                            selectedEmployeeHashCodesToLink.remove(toRemove);
                            // Unlink
                            selectedEmployeeHashCodesToUnlink.remove(toRemove);
                    }
                    // Set the session attributes
                    request.getSession().setAttribute("selectedEmployeeHashCodesToLink", selectedEmployeeHashCodesToLink);
                    request.getSession().setAttribute("selectedEmployeeHashCodesToUnlink", selectedEmployeeHashCodesToUnlink);
                    ////////////////////////////////////////////////////////////////////
                }

                // Generate the page content
                DatabaseObject dbo = za.co.ucs.lwt.db.DatabaseObject.getInstance();
                PageProducer_ShiftRoster producer;
                if (rosterDay == null) {
                    producer = new PageProducer_ShiftRoster(allChildEmployees, shiftID, dbo.formatDate(refDate), null, originalURI, request.getSession());
                } else {
                    producer = new PageProducer_ShiftRoster(allChildEmployees, shiftID, dbo.formatDate(refDate), dbo.formatDate(rosterDay), originalURI, request.getSession());
                }

                out.write(producer.generateInformationContent());
            }
        }
    }

%>

<%@include file="html_bottom.jsp"%>