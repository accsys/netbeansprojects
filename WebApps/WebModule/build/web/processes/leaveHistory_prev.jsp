<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Page Top
        // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
        if (!user.equals(employee)) {
            loginSessionObject.setEmployee(user);
        } else {
            if (loginSessionObject == null) {
                response.sendRedirect(response.encodeURL("Login.jsp"));
            } else {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Leave History Overview", employee.toString()));



                // Page Detail
                String hashValue = request.getParameter("hashValue"); // the HashValue of the Reporting Structure in use
                String refDate = request.getParameter("referenceDate"); // for which period?
                String selectedSelectionString = request.getParameter("selSelection"); // Which Selection was selected?

                //
                // Group of selections 'under' this one in the for of a TreeList
                //
                EmployeeSelection displaySelection = null;  // The displaySelection is the class used to generate the detailed information
                // of the specific page.
                ReportingStructure reportingStructure = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getReportingStructure(hashValue);

                // We also need to traverse the sub-groups to get a list of all the selections underneath the current user.
                // We need this to forward the appropriate selected selection
                int selectedSelectionCode = 0;
                if (selectedSelectionString != null) {
                    selectedSelectionCode = new Integer(selectedSelectionString).intValue();
                    displaySelection = reportingStructure.getEmployeeSelection(selectedSelectionCode);
                }

                // If the request passed on a parameter with a referencedate value, rather use that
                String originalURI = request.getRequestURI();

                if (displaySelection != null) {
                    PageProducer_LeaveSummaryGrid_SelectionGroup groupProducer;
                    if ((refDate == null) || (refDate.compareToIgnoreCase("null") == 0)) {
                        groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(displaySelection, new java.util.Date(), originalURI, hashValue);
                    } else {
                        groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(displaySelection, za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(refDate), originalURI, hashValue);
                    }
                    out.write(groupProducer.generateInformationContent());
                }
            }
        }
%>

<%@include file="html_bottom.jsp"%>