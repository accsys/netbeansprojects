<%@page contentType="text/html"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<% // If the File Container is busy loading data from the database, do not allow LOGINS to occur.
    if (!za.co.ucs.accsys.webmodule.FileContainer.getInstance().isESSRegistered()) {
        out.write("<br><span  class=\"standardHeading\">The PeopleWare ESS Module has not been registered.</span>");
        out.write("<br><span  class=\"standardHeading\">Please contact your Payroll/HR administrator to assist.</h5>");

    } else {
        if (za.co.ucs.accsys.webmodule.FileContainer.getInstance().isCachingFromDB()) {
            out.write("<br><h4>The system is in the process of reloading data from the database.  Please try again later.</h4>");
            out.write("<br><h5>" + za.co.ucs.accsys.webmodule.FileContainer.getInstance().getCachePercentage() + "% complete.</h5>");
        } else {
%>
<form METHOD=POST id="login" ACTION=../Controller.jsp>
    <h1>New Password</h1>
    <fieldset id="inputs">
        <input id="pwd1" NAME="newPass" type="password" placeholder="New password" maxlength="30" autofocus required>   
        <input id="pwd2" name="confirmNewPass" type="password" placeholder="Re-type new password" maxlength="30" required>
    </fieldset>
    <fieldset id="actions">
        <input type="submit" id="submit" value="Change Password">

    </fieldset>
    <input type="hidden" name="action" value="changepassword"/>
</form>
<% 
        }
    }
%>

<%@include file="html_bottom.jsp"%>
