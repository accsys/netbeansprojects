<%-- 
    Document   : tax_result
    Created on : 06 Nov 2013, 10:28:46 AM
    Author     : fkleynhans
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>South African Tax Calculation Utility</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/style.css" />
        <script type="text/javascript">
            $(document).ready(function () {
                console.log('ready!');

                function showIfChecked($mySelector) {
                    console.log($mySelector);
                    console.log($mySelector.children("input[type='checkbox']"));
                    if ($mySelector.children("input[type='checkbox']").is(':checked'))
                        $mySelector.parent().children("div.other_text").slideDown("slow");
                    else
                        $mySelector.parent().children("div.other_text").slideUp("slow");
                }

                showIfChecked($(".onoffswitch").eq(0)); // first question
                showIfChecked($(".onoffswitch").eq(1)); // second question
                showIfChecked($(".onoffswitch").eq(2));
                showIfChecked($(".onoffswitch").eq(3));
                showIfChecked($(".onoffswitch").eq(4));
                showIfChecked($(".onoffswitch").eq(5));
                showIfChecked($(".onoffswitch").eq(6));

                function changeIfChecked($mySelector1) {
                    $mySelector1.children("input[type='checkbox']").change(function () {
                        console.log($(this).is(':checked'));  // $(this) represents whatever the selector brought back - eg. $("div.question > input[type='checkbox']")
                        if ($(this).is(':checked'))
                            $mySelector1.parent().children("div.other_text").slideDown("slow");
                        else
                            $mySelector1.parent().children("div.other_text").slideUp("slow");
                        $mySelector1.parent().children("div.other_text").children().children("input[type='number']").val('');
                        $mySelector1.parent().children("div.other_text").children().children().children("input[type='number']").val('');
                    });
                }

                changeIfChecked($(".onoffswitch").eq(0));
                changeIfChecked($(".onoffswitch").eq(1));
                changeIfChecked($(".onoffswitch").eq(2));
                changeIfChecked($(".onoffswitch").eq(3));
                changeIfChecked($(".onoffswitch").eq(4));
                changeIfChecked($(".onoffswitch").eq(5));
                changeIfChecked($(".onoffswitch").eq(6));

            });

            function copyTo(obj)
            {
                document.getElementById("RetirementFundIncomeInput").value = obj.value;
            }

            function compute() {
                var a = +$('input[id=TravelAllowanceInput]').val();
                var b = +$('input[id=OtherAllowanceInput]').val();
                var c = +$('input[id=AnnualBonusInput]').val();
                var total = a + b + c;
                $('input[id=NonRetirementFundIncomeInput]').val(total);
            }
        </script>
    </head>
    <body>
        <div id="container">
            <h1 class="title">South African Tax Calculation Utility</h1>
            <form action="tax_result.jsp" method="GET">
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Income details</h2>
                    <div class="grouping">
                        <p style="display: inline;">
                            <label for="GrossIncome">Gross Income:&nbsp;R </label>
                            <input type="number" style="width: 120px;" name="GrossIncome" min="1" max="999999999999" size="12" onchange="copyTo(this)" onkeyup="copyTo(this, RetirementFundIncomeInput)" required>
                        </p>
                        <p style="display: inline;">
                            <label for="RetirementFundIncome">RFI Income:&nbsp;R </label>
                            <input id="RetirementFundIncomeInput" type="number" style="width: 120px;" name="RetirementFundIncome" min="0" max="999999999999" size="12" required>
                        </p>
                        <p style="display: inline;">
                            <label for="NonRetirementFundIncome">NRFI Income:&nbsp;R </label>
                            <input id="NonRetirementFundIncomeInput" type="number" style="width: 120px;" name="NonRetirementFundIncome" min="0" max="999999999999" size="12">
                        </p>
                        <p>
                            <label for="NoOfPayPeriodsInYear">Payment frequency: </label>
                            <input type="radio" name="NoOfPayPeriodsInYear" value="12" checked>Monthly
                            <input type="radio" name="NoOfPayPeriodsInYear" value="26">Every 2 weeks
                            <input style="margin-top: 10px; margin-bottom: 10px;" type="radio" name="NoOfPayPeriodsInYear" value="52">Weekly                            
                        </p>
                        <p>
                            <label for="EmployeeAge">Age: </label>
                            <input type="radio" name="EmployeeAge" value="25" checked>Younger than 65
                            <input type="radio" name="EmployeeAge" value="66">Older than 65
                        </p>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Allowances</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="travelAllowanceInput">Travel Allowance?</label>
                                </p>
                            </div> 
                            <div id="travelAllowance" class="other_text" style="margin-top: 5px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the Travel Allowance: &nbsp; R <input type="number" id="TravelAllowanceInput" name="TravelAllowance" min="0" max="999999999999" size="15" onchange="compute()">
                                    </p>
                                    <p>
                                        <label for="travelAllowance">What percentage of the allowance is taxable? </label>
                                        <input type="radio" name="taxPercentage" value="1">100%
                                        <input style="margin-top: 10px; margin-bottom: 10px;" type="radio" name="taxPercentage" value="0.8" checked>80%
                                        <input type="radio" name="taxPercentage" value="0.2">20%                                        
                                    </p>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="otherAllowanceInput" style="margin-bottom: 10px;">Other Taxable Allowances?</label>
                                </p>
                            </div> 
                            <div id="otherAllowance" class="other_text" style="margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the Other Taxable Allowances: &nbsp; R <input type="number" id="OtherAllowanceInput" name="OtherAllowance" min="0" max="999999999999" size="15" onchange="compute()">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Bonus</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="bonusInput">Do you receive an Annual Bonus?</label>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="bonusSwitch" class="onoffswitch-checkbox" id="bonusSwitch">
                                <label class="onoffswitch-label" for="bonusSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="bonus" class="other_text" style="display: none; margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the Annual Bonus: &nbsp; R <input type="number" id="AnnualBonusInput" name="AnnualBonus" min="0" max="999999999999" size="15" onchange="compute()">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Monthly deductions</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="pensionFundInput">Pension Fund contribution?</label>
                                    <br>&nbsp;
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="pensionFundSwitch" class="onoffswitch-checkbox" id="pensionFundSwitch">
                                <label class="onoffswitch-label" for="pensionFundSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="pensionFund" class="other_text" style="display: none; margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the personal Pension Fund contribution: &nbsp; R <input type="number" id="Pension" name="Pension" min="0" max="999999999999" size="15" >
                                    </p>
                                    <p style="margin-top: 15px; margin-bottom: 10px;">
                                        Please enter the amount of the company Pension Fund benefit: &nbsp; R <input type="number" id="PensionCompany" name="PensionCompany" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="retirementAnnuityInput" style="margin-bottom: 10px;">Provident Fund contribution?</label>
                                    <br>&nbsp;
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="providentFundSwitch" class="onoffswitch-checkbox" id="providentFundSwitch">
                                <label class="onoffswitch-label" for="providentFundSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="providentFund" class="other_text" style=" display: none; margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the personal Provident Fund contribution: &nbsp; R <input type="number" id="Provident" name="Provident" min="0" max="999999999999" size="15" >
                                    </p>
                                    <p style="margin-top: 15px; margin-bottom: 10px;">
                                        Please enter the amount of the company Provident Fund benefit: &nbsp; R <input type="number" id="ProvidentCompany" name="ProvidentCompany" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="retirementAnnuityInput" style="margin-bottom: 10px;">Retirement Annuity contribution?</label>
                                    <br>&nbsp;
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="retirementAnnuitySwitch" class="onoffswitch-checkbox" id="retirementAnnuitySwitch">
                                <label class="onoffswitch-label" for="retirementAnnuitySwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="retirementAnnuity" class="other_text" style="display: none; margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the personal Retirement Annuity contribution: &nbsp; R <input type="number" id="Retirement" name="Retirement" min="0" max="999999999999" size="15" >
                                    </p>
                                    <p style="margin-top: 15px; margin-bottom: 10px;">
                                        Please enter the amount of the company Retirement Annuity benefit: &nbsp; R <input type="number" id="RetirementCompany" name="RetirementCompany" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="incomeReplacementInput" style="margin-bottom: 10px;">Income Replacement Policy contribution?</label>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="incomeReplacementSwitch" class="onoffswitch-checkbox" id="incomeReplacementSwitch">
                                <label class="onoffswitch-label" for="incomeReplacementSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="incomeReplacement" class="other_text" style="display: none; margin-top: 10px;">
                                <div class="inputDiv">
                                    <p>
                                        Please enter the amount of the personal Income Replacement Policy contribution: &nbsp; R <input type="number" id="incomeReplacement" name="incomeReplacement" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">MEDICAL AID</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="medicalAidInput" style="margin-bottom: 10px;">Do you have a Medical Aid?</label>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="medicalAidSwitch" class="onoffswitch-checkbox" id="medicalAidSwitch">
                                <label class="onoffswitch-label" for="medicalAidSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="medicalAid" class="other_text" style=" display: none; margin-left: 50px; margin-top: 10px;">
                                <div>
                                    <p>Please enter the number of dependents: &nbsp; 
                                        <input type="number" id="NumberOfDependants" name="NumberOfDependants" min="0" max="10" >
                                        <br><span class="comment">(excluding main member)</span>
                                    </p>
                                </div>
                                <div style="margin-top: 20px">
                                    <p>Please enter the amount of the personal contribution: &nbsp; R 
                                        <input type="number" id="PersonalContribution" name="PersonalContribution" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                                <div style="margin-top: 10px">
                                    <p>Please enter the amount of the company contribution: &nbsp; R 
                                        <input type="number" id="CompanyContribution" name="CompanyContribution" min="0" max="999999999999" size="15" >
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input class="gradient-button" type="submit" value="Calculate tax">
                <br>&nbsp;
                <br>&nbsp;
                <br>&nbsp;
            </form>
        </div><!-- Container end -->
    </body>
</html>
