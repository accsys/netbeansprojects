<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!--  ******************** 
        Initiates a Leave Cancellation Request 
      ******************** 
-->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
            if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Leave Cancellation")) {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
            } else {
                // Can I connect to the database?
                if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "Unable to connect to database.  Please contact your network administrator.",
                            false, request.getSession());
                    producer.setMessageInSessionObject();
                    out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                } else {
                    // Leave Info
                    LeaveInfo leaveInfo = fileContainer.getLeaveInfoFromContainer(employee);
                    if (leaveInfo.getNumberOfLeaveTypes() == 0) {
                        // Page Header
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                employee.toString() + " is not linked to any Leave Profiles.  Leave Cancellations cannot be processed.",
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                        out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                    } else {
                        PageSectionGenerator gen = PageSectionGenerator.getInstance();
                        // Hive the appropriate header:
                        if (user.equals(employee)) {
                            out.write(gen.buildHTMLPageHeader("Leave Cancellation Request", employee.toString()));
                        } else {
                            out.write(gen.buildHTMLPageHeader("Leave Cancellation Request", "On Behalf Of: " + employee.toString()));
                        }

                        // Page Detail
                        String selectedSelection = request.getParameter("selSelection");
                        out.write("<div id=\"container\">");
                        PageProducer_LeaveCancellationRequest producer = new PageProducer_LeaveCancellationRequest(employee, request.getSession(), selectedSelection);
                        out.write(producer.generateInformationContent());
                        out.write("</div>");


                    }
                }
            }
%>

<%@include file="html_bottom.jsp"%>