<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("T&A Status")) {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
        } else {


            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            // If no dates were passed, look at the last 7 days
            if ((fromDate == null) || (toDate == null)) {
                toDate = DatabaseObject.getCurrentDate();
                fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(toDate), -30));
            }
            // Make sure that the toDate is after the fromDate
            if (DatabaseObject.formatDate(fromDate).after(DatabaseObject.formatDate(toDate))) {
                fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(toDate), -30));
            }


            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("T&A History", employee.toString()));


            //
            // Page Producer
            //
            PageProducer_TnAFirstInLastOutHistory producer = new PageProducer_TnAFirstInLastOutHistory(employee, fromDate, toDate, true);
            out.write(producer.generateInformationContent());
        }
    }
%>

<%@include file="html_bottom.jsp"%>