<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {

        if (!user.equals(employee)) {
            loginSessionObject.setEmployee(user);
        }

        // Process In Question
        String hashValue = request.getParameter("hashValue");
        WebProcess process = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcess(hashValue);

        // Page Detail
        if (process == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Web Process available with given hashCode.",
                    true, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        }

        if ((process != null) && (!process.isActive())) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "This Web Process is no longer active.", false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        }
        // The logged-in user is not the owner of the current stage
        if ((process != null) && (process.isActive()) && (!process.getCurrentStage().getOwner().equals(loginSessionObject.getUser()))) {
            String message = "You are not the current owner of this Web Process.";
            // Did this user belong to the previous process owners?
            java.util.LinkedList previousOwners = process.getProcessHistory().getPreviousProcessOwners();
            if (previousOwners.contains(loginSessionObject.getUser())) {
                message = "You have already replied to this Web Process.  It is no longer assigned to you.";
            }
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    message, false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        }

        // This is an active, valid process
        if ((process != null) && (process.isActive())
                && (process.getCurrentStage().getOwner().equals(loginSessionObject.getUser()))) {

            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Web Process Action", loginSessionObject.getUser().toString()));

            // ******************************************************************
            // WebProcess-Specific detail sections
            //
            // 1. Leave Request
            // If this is a LeaveRequest Process, show additional information
            if (process.getClass().getName().compareTo("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
            //
            // **** Grid with leave summary of other employees in same selection group ****
                {
                    ReportingStructure reportingStructure = process.getProcessDefinition().getReportingStructure();
                    EmployeeSelection employeeSelection = process.getProcessHistory().getOriginProcessStage().getEmployeeSelection();
                    // If the request passed on a parameter with a referencedate value, rather use that
                    String originalURI = request.getRequestURI();
                    String refDate = request.getParameter("referenceDate");
                    //System.out.print("referenceDate:" + refDate);
                    PageProducer_LeaveSummaryGrid_SelectionGroup groupProducer;
                    if (refDate == null) {
                        groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(process, originalURI, hashValue);
                    } else {
                        groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(process, za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(refDate), originalURI, hashValue);
                    }
                    out.write(groupProducer.generateInformationContent());
                }
                out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");
                //
                // **** Grid with current leave balances ****
                {
                    String balanceDate = request.getParameter("balanceDate"); // for which period are we to analyse the balances?
                    if (balanceDate == null) {
                        balanceDate = "Today";
                    }
                    PageProducer_LeaveBalances balancesProducer = new PageProducer_LeaveBalances(process.getEmployee(), balanceDate, request.getRequestURL().toString());
                    out.write(balancesProducer.generateInformationContent());
                }
                out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");

                //
                // **** Grid with leave summary over the last two years ****
                {
                    PageProducer_LeaveSummaryGrid producer = new PageProducer_LeaveSummaryGrid(process.getEmployee());
                    out.write(producer.generateInformationContent());
                }
                out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");

            }
            // ******************************************************************

            // This is where the standard handling of any web process continues.
            // Page Top
            //out.write("<div id=\"container\">");
            out.write("<center><div class=\"container\" style=\"width: 1050px;\">");
            PageProducer_ProcessAction producer = new PageProducer_ProcessAction(process);
            out.write(producer.generateInformationContent());
            //out.write("</div>");
            out.write("</div></center><br>&nbsp;<br>");

        }
    }

%>

<%@include file="html_bottom.jsp"%>