<%-- 
    Document   : html5_top
    Created on : 26 Mar 2013, 11:07:24 AM
    Author     : lterblanche
--%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.Collections"%>
<%@page language="java" session="true" %>
<%@page import="za.co.ucs.accsys.webmodule.*" %>
<%@page import="za.co.ucs.ess.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="za.co.ucs.accsys.peopleware.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Expires" content="0">
        <link rel="shortcut icon" href="../images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/EssCss.css" />

        <link rel="stylesheet" href="../CSS/jquery-ui-1.8.20.custom.css" type="text/css">

        <!--<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>-->
        <script type="text/javascript" src="../js/jquery-ui-1.8.20.custom.min.js"></script>
        <script type="text/javascript" src="../js/jquery.masonry.min.js"></script>
        <script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
        

        <script>
            $(document).ready(function () {

                document.onmouseover = function () {
                    //User's mouse is inside the page.
                    sessionStorage.setItem('inOut', 'in');
                };

                document.onmouseleave = function () {
                    //User's mouse has left the page.
                    sessionStorage.setItem('inOut', 'out');
                };

                $(window).on('hashchange', function () {
                    if (window.location.search === "?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=true&leaveView=History" ||
                            window.location.search === "?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=true&leaveView=Balances") {
                        if (sessionStorage.getItem('inOut') === 'out') {
                            sessionStorage.setItem('inOut', 'in');
                            window.location = window.location;
                        }
                    }
                }).trigger('hashchange'); // bind event to the same selector as event-listener

                var processNameItem = window.localStorage.getItem('processName');
                $('select[name=processName]').val(processNameItem);
                if ($(this).val() === '') {
                    window.localStorage.setItem('processName', 'All');
                }
                ;
                $('select[name=processName]').change(function () {
                    window.localStorage.setItem('processName', $(this).val());
                });

                var activeYNItem = window.localStorage.getItem('activeYN');
                $('select[name=activeYN]').val(activeYNItem);
                if ($(this).val() === '') {
                    window.localStorage.setItem('activeYN', 'All');
                }
                ;
                $('select[name=activeYN]').change(function () {
                    window.localStorage.setItem('activeYN', $(this).val());
                });

                var rsSelectionItem = window.localStorage.getItem('rsSelection');
                $('select[name=rsSelection]').val(rsSelectionItem);
                if ($(this).val() === '') {
                    window.localStorage.setItem('rsSelection', 'All');
                }
                ;
                $('select[name=rsSelection]').change(function () {
                    window.localStorage.setItem('rsSelection', $(this).val());
                });

            });

            $(function () {
                $('#employeeTable').DataTable({
                    "paging": false,
                    "columnDefs": [
                        {"orderable": false, "targets": 0}
                    ]
                });
            });

            $(function () {
                $('#employeeTable2').DataTable({
                    "paging": false,
                    "columnDefs": [
                        {"orderable": false, "targets": 0}
                    ]
                });
            });

            // Goes back
            function back() {
                act = 'Back';
                history.back();
            }
            ;

            $(function () {
                $("#tabs").tabs();
            });

            $(function () {
                $('#container').masonry({
                    // options
                    itemSelector: '.item',
                    isFitWidth: true,
                    isResizable: true,
                    isAnimated: true,
                    animationOptions: {
                        duration: 400,
                        easing: 'linear',
                        queue: false
                    },
                    // set columnWidth a fraction of the container width
                    columnWidth: function (containerWidth) {
                        return containerWidth / 10;
                    },
                    gutterWidth: 10
                });
            });

        </script>

        <%            LoginSessionObject loginSessionObject = (LoginSessionObject) session.getAttribute("loginsessionobject");
            String sPasswordExpired = (String) session.getAttribute("expiredPassword");
            // If the password expired, reset the login session
            if (sPasswordExpired != null) {
                if (sPasswordExpired.trim().compareToIgnoreCase("TRUE") == 0) {
                    loginSessionObject = null;
                }
            }

            if ((loginSessionObject == null)
                    && (!request.getRequestURI().contains("Login.jsp"))) {
                session.setAttribute("originalURI", request.getRequestURI());
                session.setAttribute("originalQuery", request.getQueryString());
            } else {
                String timeOutPref = WebModulePreferences.getInstance().getPreference(WebModulePreferences.HTML_Timeout, "600");
                out.write("<META HTTP-EQUIV=\"Refresh\" CONTENT=\"" + timeOutPref + "; URL=Login.jsp\" />");
            }

        %>

        <TITLE>Employee Self Service (
            <%                out.write("   " + za.co.ucs.accsys.webmodule.ESSVersion.getInstance().getJarVersion());
            %>)
        </TITLE>
        <meta http-equiv="content-type" 
              content="text/html;charset=utf-8" />

        <style  type="text/css"> 
            /*
            Hint Box
            */
            #hintbox{ /*CSS for pop up hint box */
                position:absolute;
                top: 0;
                background-color: lightyellow;
                width: 150px; /*Default width of hint.*/
                padding: 3px;
                border:1px solid black;
                font:normal 11px Verdana;
                line-height:18px;
                z-index:100;
                border-right: 3px solid black;
                border-bottom: 3px solid black;
                visibility: hidden;
            }

            .hintanchor{ /*CSS for link that shows hint onmouseover*/
                font-weight: bold;
                color: maroon;
                margin: 3px 8px;
            }


        </style>




        <script language="javascript" src="../js/cal2.js"></script>
        <script language="javascript" src="../js/cal_conf2.js"></script>

        <script type="text/javascript">

            /***********************************************
             * Show Hint script- ï¿½ Dynamic Drive (www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit http://www.dynamicdrive.com/ for this script and 100s more.
             ***********************************************/

            var horizontal_offset = "9px" //horizontal offset of hint box from anchor link

            /////No further editting needed

            var vertical_offset = "0" //horizontal offset of hint box from anchor link. No need to change.
            var ie = document.all
            var ns6 = document.getElementById && !document.all

            function getposOffset(what, offsettype) {
                var totaloffset = (offsettype == "left") ? what.offsetLeft : what.offsetTop;
                var parentEl = what.offsetParent;
                while (parentEl != null) {
                    totaloffset = (offsettype == "left") ? totaloffset + parentEl.offsetLeft : totaloffset + parentEl.offsetTop;
                    parentEl = parentEl.offsetParent;
                }
                return totaloffset;
            }

            function iecompattest() {
                return (document.compatMode && document.compatMode !== "BackCompat") ? document.documentElement : document.body;
            }

            function clearbrowseredge(obj, whichedge) {
                var edgeoffset = (whichedge === "rightedge") ? parseInt(horizontal_offset) * -1 : parseInt(vertical_offset) * -1
                if (whichedge === "rightedge") {
                    var windowedge = ie && !window.opera ? iecompattest().scrollLeft + iecompattest().clientWidth - 30 : window.pageXOffset + window.innerWidth - 40
                    dropmenuobj.contentmeasure = dropmenuobj.offsetWidth
                    if (windowedge - dropmenuobj.x < dropmenuobj.contentmeasure)
                        edgeoffset = dropmenuobj.contentmeasure + obj.offsetWidth + parseInt(horizontal_offset)
                } else {
                    var windowedge = ie && !window.opera ? iecompattest().scrollTop + iecompattest().clientHeight - 15 : window.pageYOffset + window.innerHeight - 18
                    dropmenuobj.contentmeasure = dropmenuobj.offsetHeight
                    if (windowedge - dropmenuobj.y < dropmenuobj.contentmeasure)
                        edgeoffset = dropmenuobj.contentmeasure - obj.offsetHeight
                }
                return edgeoffset
            }

            function showhint(menucontents, obj, e, tipwidth) {
                if ((ie || ns6) && document.getElementById("hintbox")) {
                    dropmenuobj = document.getElementById("hintbox")
                    dropmenuobj.innerHTML = menucontents
                    dropmenuobj.style.left = dropmenuobj.style.top = -500
                    if (tipwidth != "") {
                        dropmenuobj.widthobj = dropmenuobj.style
                        dropmenuobj.widthobj.width = tipwidth
                    }
                    dropmenuobj.x = getposOffset(obj, "left")
                    dropmenuobj.y = getposOffset(obj, "top")
                    dropmenuobj.style.left = dropmenuobj.x - clearbrowseredge(obj, "rightedge") + obj.offsetWidth + "px"
                    dropmenuobj.style.top = dropmenuobj.y - clearbrowseredge(obj, "bottomedge") + "px"
                    dropmenuobj.style.visibility = "visible"
                    obj.onmouseout = hidetip
                }
            }

            function hidetip(e) {
                dropmenuobj.style.visibility = "hidden"
                dropmenuobj.style.left = "-500px"
            }

            function createhintbox() {
                var divblock = document.createElement("div")
                divblock.setAttribute("id", "hintbox")
                document.body.appendChild(divblock)
            }

            if (window.addEventListener)
                window.addEventListener("load", createhintbox, false)
            else if (window.attachEvent)
                window.attachEvent("onload", createhintbox)
            else if (document.getElementById)
                window.onload = createhintbox

        </script>



        <script type="text/javascript" src="../js/js.js">
        </script>
    </head>
    <BODY>

        <%
            Employee user = null;
            Employee employee = null;
            FileContainer fileContainer = null;
            java.util.ArrayList availableProcesses;

            // If nobody is logged in, hide most of the menu items
            fileContainer = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
            java.util.prefs.Preferences preferences = za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreferences();
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            if (loginSessionObject != null) {
                user = loginSessionObject.getUser();
                employee = loginSessionObject.getEmployee();

                // Were these overwritten by request parameters?
                // If the request is logged by a manager on behalf of an employee, a parameter will
                // be passed with the Employee's hashCode.  This is initiated from the PageProducer_EmployeeSelection class
                String hashCode = (String) request.getParameter("forEmployee");
                if ((hashCode != null) && (hashCode != "")) {
                    employee = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getEmployeeFromContainer(hashCode);
                    if (employee != null) {
                        loginSessionObject.setEmployee(employee);
                    }
                } else {
                    loginSessionObject.setEmployee(user);
                    employee = user;
                }

                availableProcesses = new java.util.ArrayList(fileContainer.getWebProcessDefinitionsWithSubordinates(loginSessionObject.getUser()).values());
                // Sort by name
                Collections.sort(availableProcesses);

                boolean isManager = false;
                for (int j = 0; j < availableProcesses.size(); j++) {
                    WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                    if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {
                        isManager = true;
                    }
                }


        %>

        <script type="text/javascript" src="../menu_js/stmenu.js"></script>


        <nav id="menu-wrap" style="posistion: relative; z-index: 900">    
            <ul id="menu"><li>
                    <!--
    
                    VIEW MENU 
                    
                    -->

                    <%                    out.write("<a href=\"" + baseURL + "processes/Welcome.jsp\">View</a><ul>");
                        if (user != null) {
                            // Personal Information
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Personal Information")) {
                                out.write("<li><a href=\"" + baseURL + "processes/personal_prev.jsp?hash=" + employee.hashCode() + "\">Personal Information</a></li>");
                            }
                            // Leave Status
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Leave Status")) {
                                out.write("<li><a href=\"" + baseURL + "processes/leave_prev.jsp?hash=" + employee.hashCode() + "\">Leave Status</a></li>");
                            }
                            // T&A Status
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("T&A Status")) {
                                out.write("<li><a href=\"" + baseURL + "processes/tna_prev.jsp?hash=" + employee.hashCode() + "\">T&A Status</a></li>");
                            }
                            // Training History
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Training History")) {
                                out.write("<li><a href=\"" + baseURL + "processes/training_prev.jsp?hash=" + employee.hashCode() + "\">Training History</a></li>");
                            }
                            // Tax Certificates
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Tax Certificates")) {
                                // Use new Jasper report for IRP5
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESS_IRP5\">Tax Certificates</a></li>");
                            }

                            if (user.getPayroll() != null && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Payslips"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/Welcome.jsp\">Payslips</a><ul>");
                                // ----- Payslips -----
                                // This Year
                                out.write("<li><a href=\"" + baseURL + "processes/payslip_prev.jsp?hash=" + employee.hashCode() + "\">Payslips - This Tax Year</a></li>");

                                // Now, let us add a line for all the archived tax years
                                java.util.LinkedList taxYears = fileContainer.getArchivedPayslipTaxYears(user);
                                java.util.Iterator iter = taxYears.iterator();
                                String fromAndToDate;
                                String fromYear;
                                String toYear;
                                while (iter.hasNext()) {
                                    fromAndToDate = (String) iter.next();
                                    fromYear = fromAndToDate.substring(0, 4);
                                    toYear = fromAndToDate.substring(5, 9);
                                    out.write("<li><a href=\"" + baseURL + "processes/payslip_prev.jsp?hash=" + employee.hashCode() + "&fromYear=" + fromYear + "&toYear=" + toYear + " \">Payslips - " + fromYear + " - " + toYear + "</a></li>");
                                }

                                out.write("</ul></li>");
                            }

                            // Statistics
                            if (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Statistics")) {
                                out.write("<li><a href=\"" + baseURL + "processes/Welcome.jsp\">Statistics</a><ul>");

                                out.write("<li><a href=\"" + baseURL + "processes/leave_stats.jsp?hash=" + employee.hashCode() + "\">Personal Leave Statistics</a></li>");
                                out.write("<li><a href=\"" + baseURL + "processes/time_stats.jsp?hash=" + employee.hashCode() + "\">Personal T&A Statistics</a></li>");
                                if (availableProcesses.size() > 0) {
                                    out.write("<li><a href=\"" + baseURL + "processes/company_stats.jsp?hash=" + employee.hashCode() + "\">Company Statistics</a></li>");
                                }
                                out.write("</ul></li>");
                            }
                        }
                    %>
            </ul>
        </li>
        <!--

        ACTIONS MENU 
        
        -->
        <li>

            <%
                out.write("<a href=\"" + baseURL + "processes/Welcome.jsp\">Actions</a><ul>");
                int creditBalance = DatabaseObject.getAvailableESSCredits();
                // Warn the user to buy credits
                if (creditBalance == 0) {
                    session.setAttribute("essMessage", "<table id=\"htmlPageHeader\" style=\"width: 90%;\"><tr><td><i>Your ESS Credits have been depleted.  All workflow processes are disabled.</i></td></tr></table>");
                }

                if ((user != null) && (creditBalance > 0)) {

                    java.util.ArrayList allProcesses = fileContainer.getWebProcessDefinitions();
                    Collections.sort(allProcesses);
                    if (allProcesses.size() > 0) {
                        for (int a = 0; a < allProcesses.size(); a++) {
                            WebProcessDefinition webProcessDefinition = (WebProcessDefinition) allProcesses.get(a);

                            // Only show those processes than can be instantiated by the employee him/herself 
                            boolean cannotInstantiateSelf = webProcessDefinition.getCannotInstantiateSelf();
                            boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                            boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));

                            if (cannotInstantiateSelf) {
                                continue;
                            }

                            if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                                continue;
                            }

                            // Contact Information
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Change personal information"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/contact_req.jsp\">Change personal information</a></li>");
                            }

                            // Leave
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for leave")) && webProcessDefinition.employeeInReportingStructure(user)) {
                                out.write("<li><a href=\"" + baseURL + "processes/leave_req.jsp\">Apply for leave</a></li>");
                            }

                            // Leave Cancellation
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Cancel future-dated leave"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/leavecancel_req.jsp\">Cancel future-dated leave</a></li>");
                            }

                            // Family
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Change family information"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/family_req.jsp\">Change family information</a></li>");
                            }

                            // Training
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for training"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/training_req.jsp\">Apply for training</a></li>");
                            }

                            // TnA Clocking Authorisation
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Submit working hours"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/tna_signoff_req.jsp?self=true\">Submit working hours</a></li>");
                            }

                            //
                            // Actions based on Variables (Indicator codes 55001 ... 55005
                            //
                            za.co.ucs.accsys.peopleware.Variable variableRequested;
                            // 55001
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                                variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55001");

                                if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for - " + variableRequested.getDescription().trim()))) {
                                    out.write("<li><a href=\"" + baseURL + "processes/variable_req.jsp?indCode=55001 \">Apply for - " + variableRequested.getDescription() + "</a></li>");
                                }
                            }
                            // 55002
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                                variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55002");

                                if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for - " + variableRequested.getDescription().trim()))) {
                                    out.write("<li><a href=\"" + baseURL + "processes/variable_req.jsp?indCode=55002 \">Apply for - " + variableRequested.getDescription() + "</a></li>");
                                }
                            }
                            // 55003
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                                variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55003");

                                if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for - " + variableRequested.getDescription().trim()))) {
                                    out.write("<li><a href=\"" + baseURL + "processes/variable_req.jsp?indCode=55003 \">Apply for - " + variableRequested.getDescription() + "</a></li>");
                                }
                            }
                            // 55004
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                                variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55004");

                                if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for - " + variableRequested.getDescription().trim()))) {
                                    out.write("<li><a href=\"" + baseURL + "processes/variable_req.jsp?indCode=55004 \">Apply for - " + variableRequested.getDescription() + "</a></li>");
                                }
                            }
                            // 55005
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                                variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55005");

                                if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Apply for - " + variableRequested.getDescription().trim()))) {
                                    out.write("<li><a href=\"" + baseURL + "processes/variable_req.jsp?indCode=55005 \">Apply for - " + variableRequested.getDescription() + "</a></li>");
                                }
                            }

                        }
                    }
                }

            %>                  
            </ul>		
        </li>
        <!--

        PROCESSES MENU 
        
        -->                
        <li>

            <%                out.write("<a href=\"" + baseURL + "processes/Welcome.jsp\">Inbox</a><ul>");
                if (user != null) {

                    // --- Inbox
                    if (fileContainer.getWebProcessesForActiveList(user).size() > 0) {
                        out.write("<li><a href=\"" + baseURL + "processes/proc_todo.jsp\">Authorise requests</a></li>");
                    }
                    // --- List processes
                    out.write("<li><a href=\"" + baseURL + "processes/Welcome.jsp\">Show me a list of...</a>");
                    out.write("<ul>");

                    // --- My Active Requests
                    out.write("   <li><a href=\"" + baseURL + "processes/proc_activelist.jsp\">All active processes</a></li>");
                    // --- Handled Processes (Do we show all, or just the active ones?)
                    boolean onlyActive = ((WebModulePreferences.getInstance().getPreference(WebModulePreferences.MEMORY_OnlyLoadActiveProcesses, "false")).compareToIgnoreCase("true") == 0);
                    if (!onlyActive) {
                        out.write("   <li><a href=\"" + baseURL + "processes/proc_diddo.jsp\">All finalised processes</a></li>");
                        // --- My Active/Closed Requests
                        out.write("   <li><a href=\"" + baseURL + "processes/proc_list.jsp\">My requests (active and closed)</a></li>");
                    } else {
                        // --- My Active Requests
                        out.write("   <li><a href=\"" + baseURL + "processes/proc_list.jsp\">My requests (active only)</a></li>");
                    }
                    out.write("</ul></li>");

                    //
                    //
                    // If this user is on top of the hierarchy with a given web process, he/she will be able to see detailed information on these
                    // processes 
                    //
                    boolean didLeave = false;
                    for (int j = 0; j < availableProcesses.size(); j++) {
                        WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                        //ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
                        if (fileContainer.isEmployeeInTopLevel(loginSessionObject.getUser(), webProcessDefinition)) {
                            String description = webProcessDefinition.getName();
                            String webProcessClassName = webProcessDefinition.getProcessClassName();
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) {
                                description = "Contact Detail";
                                webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact";
                            }
                            if (webProcessDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") && !didLeave) {
                                description = "Leave";
                                webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition";
                                didLeave = true;
                                // Need to write out at this point to only write out once for leave
                                out.write("   <li><a href=\"" + baseURL + "processes/proc_list_toplevel.jsp?webProcessClassName=" + webProcessClassName + "\">" + description + "</a></li>");
                            }
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) {
                                description = "Leave Cancellation";
                                webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation";
                            }
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) {
                                description = "Training";
                                webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition";
                            }

                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) {
                                description = "Family Detail";
                                webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition";
                            }

                            if (!webProcessDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                                out.write("   <li><a href=\"" + baseURL + "processes/proc_list_toplevel.jsp?webProcessClassName=" + webProcessClassName + "\">" + description + "</a></li>");
                            }
                        }
                    }

                }
            %>
            </ul>
        </li>
        <%
            if ((user != null) && (isManager)) {
                out.write("<li> <a href=\"" + baseURL + "processes/Welcome.jsp\">Managers</a><ul>");

                boolean didLeave = false;

                for (int j = 0; j < availableProcesses.size(); j++) {
                    WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                    if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {

                        boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                        boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));
                        if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                            System.out.println("*** Process outside valid period ***:" + webProcessDefinition + " valid between " + webProcessDefinition.getActiveFromDayOfMonth() + " and " + webProcessDefinition.getActiveToDayOfMonth() + " of the month.");
                            continue;
                        }

                        //
                        // Standard Manager menu options for web processes
                        //
                        // BUT - Not for T&A Clocking Authorisation, as we have created a special form for that
                        ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
                        PageProducer_GroupSelector gen = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null); // Scan the existing list of process definitions
                        if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                            // Authorise T&A hours for multiple employees
                            //PageProducer_GroupSelector genMultiEmpTnAAuth = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "employeelist_prev.jsp", "Clocking Authorisation", true); // Scan the existing list of process definitions
                            //out.write(genMultiEmpTnAAuth.generateWindowsMenuContent(baseURL));
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/employeelist_prev.jsp?webProcessClassName=" + webProcessDefinition.getProcessClassName() + "&MultiEmployee=true\">");
                            out.write(gen.getMenuCaption());
                            out.write("</a>");
                            out.write("</li>");
                        } else if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition") == 0) {
                            // Roster screen
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/tna_roster.jsp?webProcessClassName=" + webProcessDefinition.getProcessClassName() + "\">");
                            out.write(gen.getMenuCaption());
                            out.write("</a>");
                            out.write("</li>");
                        } else if (!webProcessDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                            //out.write(gen.generateWindowsMenuContent(baseURL));
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/employeelist_prev.jsp?webProcessClassName=" + webProcessDefinition.getProcessClassName() + "&MultiEmployee=false\">");
                            out.write(gen.getMenuCaption());
                            out.write("</a>");
                            out.write("</li>");
                        } else if (!didLeave) { // All leave
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/employeelist_prev.jsp?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=false\">");
                            out.write("Request Leave");
                            out.write("</a>");
                            out.write("</li>");
                            didLeave = true;
                        }
                        //
                        // Extra options for Leave related items
                        //
                        if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                            // View Leave History
                            //PageProducer_GroupSelector genLeaveHistory = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "leaveHistory_prev.jsp", "View Leave History", false); // Scan the existing list of process definitions
                            //out.write(genLeaveHistory.generateWindowsMenuContent(baseURL));
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/employeelist_prev.jsp?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=true&leaveView=History\">");
                            out.write("View Leave History");
                            out.write("</a>");
                            out.write("</li>");
                            // View Leave Balances
                            //PageProducer_GroupSelector genLeaveBalances = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "leaveBalances_prev.jsp", "View Leave Balances", false); // Scan the existing list of process definitions
                            //out.write(genLeaveBalances.generateWindowsMenuContent(baseURL));
                            out.write("<li>");
                            out.write("<a href=\"" + baseURL + "processes/employeelist_prev.jsp?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=true&leaveView=Balances\">");
                            out.write("View Leave Balances");
                            out.write("</a>");
                            out.write("</li>");
                        }
                    }
                }
                out.write("</ul></li>");
            }
        %>

        <!--

       REPORTS MENU 
       
        -->                
        <%
            if (FileContainer.isESSReportingRegistered) {
                if ((user != null) && (isManager)) {
                    out.write("<li> <a href=\"" + baseURL + "processes/Welcome.jsp\">Reports</a><ul>");
                    //
                    // Sub menu for Human Resources Reports
                    //
                    out.write("<li> <a href=\"" + baseURL + "processes/Welcome.jsp\">Human Resources</a><ul>");
                    for (int j = 0; j < availableProcesses.size(); j++) {
                        WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                        if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {
                            // Employee Leave Balance Report
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Leave"))) {
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSL010\">Leave Balance Report</a></li>");
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSL011\">Leave Forfeit Report</a></li>");
                            }
                        }
                    }
                    out.write("</ul>");
                    //
                    // Sub menu for Time and Attendance Reports
                    //
                    out.write("<li> <a href=\"" + baseURL + "processes/Welcome.jsp\">Time and Attendance</a><ul>");
                    for (int j = 0; j < availableProcesses.size(); j++) {
                        WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                        if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {
                            if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Clocking Authorisation"))) {
                                // First In / Last Out Report
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSTM1207\">First In / Last Out Report</a></li>");
                                // Late In / Early Out Report
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSTM1115\">Late In / Early Out Report</a></li>");
                                // Clocking Times
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSTM1206\">Clocking Times Report</a></li>");
                                // Clocking Times
                                out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSTM112\">Absentees Report</a></li>");
                            }
                        }
                    }
                    out.write("</ul>");
                    //
                    // Sub menu for ESS Processes Report
                    //
                    out.write("<li> <a href=\"" + baseURL + "processes/Welcome.jsp\">ESS Processes</a><ul>");
                    out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESSP1190\">Process History Report</a></li>");
                    out.write("</ul></ul></li>");
                }
            }
        %>                


        <li>

            <%
                try {
                    if (fileContainer.isEmployeeWebAdministrator(user)) {
                        out.write("<a href=\"" + baseURL + "processes/Welcome.jsp\">Admin</a>");
                        // --- Admin Screen
                        out.write("<ul>");
                        out.write("<li><a href=\"" + baseURL + "processes/admin.jsp\">Settings</a></li>");
                        // --- Reporting Structures
                        out.write("<li><a href=\"" + baseURL + "processes/websetup.jsp\">View Reporting Structures</a></li>");
                        // --- Login Audit Report
                        out.write("<li><a href=\"" + baseURL + "processes/report_prev.jsp?reportNum=ESS_AUDIT\">Login Audit Report</a></li>");
                        out.write("</ul>");
                    }
                } catch (NullPointerException ex) {
                    if ((user != null) && (isManager)) {
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                "PeopleWare ESS requires a nominated Web Administrator.\nPlease appoint a new employee in the 'Web Administrator' user group in PeopleWare.",
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                    }
                    System.out.println("******************************************");
                    System.out.println("******************************************");
                    System.out.println("            WEB ADMIN DISCHARGED          ");
                    System.out.println("******************************************");
                    System.out.println("******************************************");
                    System.out.println("Exception: " + ex);
                }
            %>
        </li>
        <li>
            <% out.write("<a href=\"" + baseURL + "Controller.jsp?action=logout \"> Sign Out</a>");%>
        </li>
        <li>
            <% out.write("<a href=\"" + baseURL + "processes/ChangePassword.jsp \"> Change Password</a>");%>
        </li>
    </ul>
</nav>
<%        } else {
%>    
<nav id="menu-wrap">    
    <ul id="menu">

        <li><% out.write("<a href=\"" + baseURL + "processes/Login.jsp \" >Log In</a>");%> </li>


    </ul>
</nav>
<% }%>
<br>