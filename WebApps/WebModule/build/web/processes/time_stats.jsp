<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%>
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?        // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Personal T&A Statistics")) {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
        } else {

            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Time & Attendance Statistics", employee.toString()));

            StringBuffer sqlStatement = new StringBuffer();
            String strXML = new String();
            StringBuffer strBXML = new StringBuffer();
            za.co.ucs.lwt.db.DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();
            java.sql.Connection con = null;
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            // If no dates were passed, look at the last 7 days
            if ((fromDate == null) || (toDate == null)) {
                //fromDate =  DatabaseObject.addNDays( new java.util.Date(), -7);
                //toDate = DatabaseObject.getCurrentDate();
                toDate = DatabaseObject.getCurrentDate();
                fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(toDate), -7));
            }
            // Make sure that the toDate is after the fromDate
            if (DatabaseObject.formatDate(fromDate).after(DatabaseObject.formatDate(toDate))) {
                fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(toDate), -7));
            }
            int dateDaysDiff = DatabaseObject.getNumberOfDaysBetween(DatabaseObject.formatDate(fromDate), DatabaseObject.formatDate(toDate)) + 1;
            int dateWeeksDiff = DatabaseObject.getNumberOfWeeksBetween(DatabaseObject.formatDate(fromDate), DatabaseObject.formatDate(toDate)) + 1;
            boolean anyHoursWorked = false;

            StringBuilder resultPage = new StringBuilder();
            resultPage.append("<table width='100%' align='center'><FORM name=\"tnastats_dates\" METHOD=POST action=\"time_stats.jsp\"> ");

            //
            // From Date and To Date Picker (START)
            //
            resultPage.append("<table width='650' align='center'><tr>");
            // From Date
            //
            resultPage.append("<td width=\"100\" align=\"right\" ><h3 class=\"shaddow\">From</h3></td>");

            resultPage.append("<td  width=\"200\"><input type=\"text\" name=\"fromDate\" value=\"").append(fromDate).append("\" size=10 maxlength=10");
            resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
            resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
            //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
            resultPage.append(" >");

            resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar3')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");
            // Separator 
            //
            resultPage.append("<td width=\"20\"></td>");
            // To Date
            //
            resultPage.append("<td width=\"100\"  align=\"right\" ><h3 class=\"shaddow\">To</h3></td>");

            resultPage.append("<td width=\"200\" ><input type=\"text\" name=\"toDate\" value=\"").append(toDate).append("\" size=10 maxlength=10");
            resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
            resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
            //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
            resultPage.append(" >");

            resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date.', this, event, '130px')\" href=\"javascript:showCal('Calendar4')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a></td>");
            resultPage.append("<td width=\"80\"><input class=\"gradient-button\" type=\"submit\" value=\"Refresh Selection\" ></td>");

            resultPage.append("</tr></table>");
            //
            // From Date and To Date Picker (END)
            //            
            resultPage.append("</form></table><hr width=\"80%\"><br>");
            out.write(resultPage.toString());

%>
<center>
    <table width="90%">
        <tr>
            <td>
                <table align="center">
                    <tr>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">First In/Last Out</h3>
                                            <br>
                                            <%@include file="_incl_TnA_FirstInLastOut.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="4%"></td>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">Time & Avg Time at Work</h3>
                                            <br>
                                            <%@include file="_incl_TnA_TimeAndAvgTime.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><br></td>
                    </tr>
                    <tr>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">Hours worked (Authorised)</h3>
                                            <br>
                                            <%@include file="_incl_TnA_HoursWorked_Auth.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="4%"></td>
                        <td width="48%">
                            <div class="grouping">
                                <table>
                                    <tr>
                                        <td>
                                            <h3 class="shaddow">Total Hours worked (Authorised)</h3>
                                            <br>
                                            <%@include file="_incl_TnA_TotalHoursWorked_Auth.jsp"%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
<br><br>
<!-- DEPRECATED -->

<%// Now we want to display the actual First-In / Last-Out information 
            PageProducer_TnAFirstInLastOutHistory producer = new PageProducer_TnAFirstInLastOutHistory(employee, fromDate, toDate, false);
            out.write(producer.generateInformationContent());

        }
    }
%>

<br><br><div style="text-align: center;"><small>Note: <a href="http://www.adobe.com/products/flashplayer/">Adobe Flash Player 8 </a>or later required</small></div><br>

<%@include file="html_bottom.jsp"%>