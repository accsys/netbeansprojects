

<%

        // Define Chart
        strBXML = new StringBuffer();
        sqlStatement = new StringBuffer();

        strBXML.append("<");
        strBXML.append("chart caption='Employee Strength: By Type' ");
        strBXML.append("useRoundEdges='1' ");
        strBXML.append("showValues='0' ");
        strBXML.append("showSum='1' ");
        strBXML.append("formatNumberScale='0' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("xAxisName='Month'");
        strBXML.append("yAxisName='Number of Employees'");
        strBXML.append("numVisiblePlot='18' ");
        strBXML.append(">");


        object.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));

        try {
            con = object.getNewConnectionFromPool();

            // Start by loading the employee groups
            sqlStatement.append("select distinct ( left( MonthName(DAY),3)||' '||DatePart(yy,DAY)) as theMonth, (DAY-DatePart(dd,DAY)+1) as DateOrdered,  ");
            sqlStatement.append("(select count(*) from e_master where Company_id=" + employee.getCompany().getCompanyID() + " and ");
            sqlStatement.append("     fn_E_EmployeeActiveOnDate_YN(company_id, employee_id, DateOrdered)='Y' and ");
            sqlStatement.append("     fn_P_GetEmployeeStatus(company_id, employee_id)='Full-time') as nbTotal_FullTime, ");
            sqlStatement.append("(select count(*) from e_master where Company_id=" + employee.getCompany().getCompanyID() + " and ");
            sqlStatement.append("     fn_E_EmployeeActiveOnDate_YN(company_id, employee_id, DateOrdered)='Y' and ");
            sqlStatement.append("     fn_P_GetEmployeeStatus(company_id, employee_id)='Temporary') as nbTotal_Temporary, ");
            sqlStatement.append("(select count(*) from e_master where Company_id=" + employee.getCompany().getCompanyID() + " and ");
            sqlStatement.append("     fn_E_EmployeeActiveOnDate_YN(company_id, employee_id, DateOrdered)='Y' and ");
            sqlStatement.append("     fn_P_GetEmployeeStatus(company_id, employee_id)='Part-time') as nbTotal_PartTime, ");
            sqlStatement.append("(select count(*) from e_master where Company_id=" + employee.getCompany().getCompanyID() + " and ");
            sqlStatement.append("     fn_E_EmployeeActiveOnDate_YN(company_id, employee_id, DateOrdered)='Y' and ");
            sqlStatement.append("     fn_P_GetEmployeeStatus(company_id, employee_id)='Contractor') as nbTotal_Contractor, ");
            sqlStatement.append("(select count(*) from e_master where Company_id=" + employee.getCompany().getCompanyID() + " and ");
            sqlStatement.append("     fn_E_EmployeeActiveOnDate_YN(company_id, employee_id, DateOrdered)='Y' and ");
            sqlStatement.append("     fn_P_GetEmployeeStatus(company_id, employee_id)='Learner') as nbTotal_Learner ");
            sqlStatement.append("from s_calendar_days ");
            sqlStatement.append("where DateOrdered between dateadd(mm,-24,now()) and dateadd(mm,1,now()) ");
            sqlStatement.append("and datepart(dd, DAY)=1 ");
            sqlStatement.append("order by DateOrdered ");

            java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);
            // Build the SETS

            strBXML.append("<categories>");
            while (rs.next()) {
                strBXML.append("<category label='" + rs.getString("theMonth") + "' />");
            }
            strBXML.append("</categories>");

            // Build the attendance per month : FULL TIME
            strBXML.append("<dataset seriesName='Full-Time'>");
            rs.first();
            strBXML.append("<set value='" + rs.getString("nbTotal_FullTime") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + rs.getString("nbTotal_FullTime") + "' />");
            }
            strBXML.append("</dataset>");

            // Build the attendance per month : PART TIME
            strBXML.append("<dataset seriesName='Part-Time'>");
            rs.first();
            strBXML.append("<set value='" + rs.getString("nbTotal_PartTime") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + rs.getString("nbTotal_PartTime") + "' />");
            }
            strBXML.append("</dataset>");

            // Build the attendance per month : TEMPORARY
            strBXML.append("<dataset seriesName='Temporary'>");
            rs.first();
            strBXML.append("<set value='" + rs.getString("nbTotal_Temporary") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + rs.getString("nbTotal_Temporary") + "' />");
            }
            strBXML.append("</dataset>");

            // Build the attendance per month : CONTRACTORS
            strBXML.append("<dataset seriesName='Contractors'>");
            rs.first();
            strBXML.append("<set value='" + rs.getString("nbTotal_Contractor") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + rs.getString("nbTotal_Contractor") + "' />");
            }
            strBXML.append("</dataset>");

            // Build the attendance per month : LEARNERS
            strBXML.append("<dataset seriesName='Learners'>");
            rs.first();
            strBXML.append("<set value='" + rs.getString("nbTotal_Learner") + "' />");
            while (rs.next()) {
                strBXML.append("<set value='" + rs.getString("nbTotal_Learner") + "' />");
            }
            strBXML.append("</dataset>");
            strXML = strBXML.toString();
            rs.close();
        } finally {
            object.releaseConnection(con);
        }
        strBXML.append("</chart>");
        strXML = strBXML.toString();
%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/ScrollStackedColumn2D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="WMode" value="Transparent" />
        <jsp:param name="chartWidth" value="450" />
        <jsp:param name="chartHeight" value="300" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>


