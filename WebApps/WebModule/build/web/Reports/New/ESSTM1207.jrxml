<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="TM1207" language="groovy" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="555" leftMargin="28" rightMargin="12" topMargin="20" bottomMargin="20" whenResourceMissingType="Empty" uuid="5ba7f0ca-c2f7-4ce5-8ae8-4805026c7b94">
	<property name="ireport.zoom" value="1.610510000000033"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="FROM_DATE" class="java.lang.String">
		<defaultValueExpression><![CDATA["2013-01-02"]]></defaultValueExpression>
	</parameter>
	<parameter name="TO_DATE" class="java.lang.String">
		<defaultValueExpression><![CDATA["2013-10-02"]]></defaultValueExpression>
	</parameter>
	<parameter name="REPORT_LOGO" class="java.lang.String">
		<defaultValueExpression><![CDATA["BCXLogo.jpg"]]></defaultValueExpression>
	</parameter>
	<parameter name="GROUP_BY" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["Cost_Centre"]]></defaultValueExpression>
	</parameter>
	<parameter name="EMPLOYEE_LIST" class="java.util.List">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT "C_MASTER"."NAME" AS Company,
        "C_MASTER"."COMPANY_ID",
        "TA_E_MASTER"."PERSON_ID",
        "TA_E_MASTER"."SURNAME" AS Surname,
        "TA_E_MASTER"."FIRSTNAME" AS Firstname,
        "TA_E_MASTER"."COMPANY_EMPLOYEE_NUMBER" AS Company_Employee_Number,
        "vw_TA_Report_FirstInLastOut"."day",
        "vw_TA_Report_FirstInLastOut"."FirstIn",
        "vw_TA_Report_FirstInLastOut"."LastOut",
        "TA_E_MASTER"."SHIFTPROFILE_ID",
        "TA_ROSTERPROFILE"."NAME",
        "vw_TA_Report_FirstInLastOut"."TimeAtWork_DisplayValue",
        "vw_TA_Report_FirstInLastOut"."TimeAtWork_ActualMinutes",
        "C_COSTMAIN"."COST_ID", "C_COSTMAIN"."NAME" AS Cost_Centre,
        (greater(TimeAtWork_DisplayValue, '00:00')) as TimeAtWork_DisplayValue_Edit,
        (greater(TimeAtWork_ActualMinutes, 0)) as TimeAtWork_ActualMinutes_Edit,
        (select TA_ROSTERPROFILE.Name from TA_ROSTERPROFILE where TA_ROSTERPROFILE.SHIFTPROFILE_ID = "TA_E_MASTER".SHIFTPROFILE_ID) as Shift
 FROM   (((("DBA"."C_MASTER" "C_MASTER"
INNER JOIN "DBA"."TA_E_MASTER" "TA_E_MASTER"
ON ("C_MASTER"."COMPANY_ID"="TA_E_MASTER"."COMPANY_ID"))
LEFT OUTER JOIN "DBA"."TA_ROSTERPROFILE" "TA_ROSTERPROFILE"
ON "TA_E_MASTER"."SHIFTPROFILE_ID"="TA_ROSTERPROFILE"."SHIFTPROFILE_ID")
INNER JOIN "DBA"."vw_TA_Report_FirstInLastOut" "vw_TA_Report_FirstInLastOut"
ON ("TA_E_MASTER"."COMPANY_ID"="vw_TA_Report_FirstInLastOut"."company_id")
AND ("TA_E_MASTER"."PERSON_ID"="vw_TA_Report_FirstInLastOut"."person_id"))
INNER JOIN "DBA"."E_MASTER" "E_MASTER"
ON ("TA_E_MASTER"."COMPANY_ID"="E_MASTER"."COMPANY_ID")
AND ("TA_E_MASTER"."EMPLOYEE_ID"="E_MASTER"."EMPLOYEE_ID"))
LEFT OUTER JOIN "DBA"."C_COSTMAIN" "C_COSTMAIN"
ON ("E_MASTER"."COMPANY_ID"="C_COSTMAIN"."COMPANY_ID")
AND ("E_MASTER"."COST_ID"="C_COSTMAIN"."COST_ID")
WHERE vw_TA_Report_FirstInLastOut.FirstIn IS  NOT  NULL
AND (vw_TA_Report_FirstInLastOut.day>=$P{FROM_DATE}
AND vw_TA_Report_FirstInLastOut.day<=$P{TO_DATE})
AND $X{IN,"TA_E_MASTER"."COMPANY_EMPLOYEE_NUMBER", EMPLOYEE_LIST}
ORDER BY Cost_Centre, Surname,Firstname, day]]>
	</queryString>
	<field name="Company" class="java.lang.String"/>
	<field name="COMPANY_ID" class="java.math.BigDecimal"/>
	<field name="PERSON_ID" class="java.math.BigDecimal"/>
	<field name="Surname" class="java.lang.String"/>
	<field name="Firstname" class="java.lang.String"/>
	<field name="Company_Employee_Number" class="java.lang.String"/>
	<field name="day" class="java.sql.Date"/>
	<field name="FirstIn" class="java.sql.Timestamp"/>
	<field name="LastOut" class="java.sql.Timestamp"/>
	<field name="SHIFTPROFILE_ID" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[The shift roster profile the person is linked to]]></fieldDescription>
	</field>
	<field name="NAME" class="java.lang.String"/>
	<field name="TimeAtWork_DisplayValue" class="java.lang.String"/>
	<field name="TimeAtWork_ActualMinutes" class="java.lang.Integer"/>
	<field name="COST_ID" class="java.math.BigDecimal"/>
	<field name="Cost_Centre" class="java.lang.String"/>
	<field name="TimeAtWork_DisplayValue_Edit" class="java.lang.String"/>
	<field name="TimeAtWork_ActualMinutes_Edit" class="java.lang.Integer"/>
	<field name="Shift" class="java.lang.String"/>
	<variable name="Total" class="java.lang.Integer" resetType="Group" resetGroup="Group1" calculation="Sum">
		<variableExpression><![CDATA[$F{TimeAtWork_ActualMinutes_Edit}]]></variableExpression>
		<initialValueExpression><![CDATA[0]]></initialValueExpression>
	</variable>
	<variable name="PROFILE_HEAD_COUNT" class="java.lang.Integer" resetType="Group" resetGroup="GROUP" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{PERSON_ID}]]></variableExpression>
	</variable>
	<variable name="person_id_1" class="java.lang.Integer" resetType="Group" resetGroup="ID" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{PERSON_ID}]]></variableExpression>
	</variable>
	<group name="ID">
		<groupExpression><![CDATA[$F{COMPANY_ID}]]></groupExpression>
		<groupFooter>
			<band height="34">
				<line>
					<reportElement x="0" y="-1" width="555" height="1" uuid="0215b9bd-28d5-4dad-90f0-f39e056c293e"/>
				</line>
				<textField>
					<reportElement mode="Opaque" x="0" y="0" width="555" height="16" backcolor="#E6E6E6" uuid="98d4d6a4-6572-47ba-9dc2-49a03d54c194"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Total Head Count: " + ($V{person_id_1} != null ? $V{person_id_1} : "0")]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="EEEEE" isBlankWhenNull="true">
					<reportElement x="0" y="20" width="555" height="14" uuid="9f7b7a41-69f3-4b8a-b78d-be2973ad7810"/>
					<textElement textAlignment="Left">
						<font size="8"/>
					</textElement>
					<textFieldExpression><![CDATA["*  Please note that manual clockings will only display on this report, if they have been linked to a shift."]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="GROUP">
		<groupExpression><![CDATA[$P{GROUP_BY}.equalsIgnoreCase("Company") ? $F{Company} :
($P{GROUP_BY}.equalsIgnoreCase("Shift") ? $F{Shift} :
    ($P{GROUP_BY}.equalsIgnoreCase("Cost_Centre") ? $F{Cost_Centre} :
        $F{Company}
    )
)]]></groupExpression>
		<groupHeader>
			<band height="22">
				<textField isBlankWhenNull="false">
					<reportElement mode="Opaque" x="0" y="1" width="555" height="20" backcolor="#E6E6E6" uuid="ea1571d6-a5c5-472b-9039-fd05ca8e3435"/>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA["Cost Centre: " + ($F{Cost_Centre} != null ? $F{Cost_Centre} :"")]]></textFieldExpression>
				</textField>
				<line>
					<reportElement x="0" y="21" width="555" height="1" uuid="acef4a82-d4a2-4122-9da8-69b0f637fc0c"/>
				</line>
				<line>
					<reportElement x="0" y="0" width="555" height="1" uuid="f1eff5d2-16b4-46e7-93aa-d97129265857"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="17">
				<textField>
					<reportElement mode="Opaque" x="0" y="0" width="555" height="16" backcolor="#E6E6E6" uuid="7f197fbe-95cf-4c58-8417-6ff1d50cfb5b"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Cost Centre Head Count: " + ($V{PROFILE_HEAD_COUNT} != null ? $V{PROFILE_HEAD_COUNT} :"0")]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="Group1">
		<groupExpression><![CDATA[$F{PERSON_ID}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<textField evaluationTime="Group" evaluationGroup="Group1" pattern=":00" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="542" y="0" width="13" height="18" backcolor="#E6E6E6" uuid="3c41b1a5-606e-40e2-abc0-e00fc9faa809"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[($V{Total}%60)]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement mode="Opaque" x="65" y="0" width="224" height="18" backcolor="#E6E6E6" uuid="d98541a4-8d67-48d4-8088-f08f7470bcb3"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Surname} == null || $F{Firstname} == null ? "" : " " + $F{Surname} + " " + $F{Firstname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="0" y="0" width="65" height="18" backcolor="#E6E6E6" uuid="baed8bdc-ff4e-4281-b4d8-c150e6ea4e96"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Company_Employee_Number} == null ? "" : " " + $F{Company_Employee_Number}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="Group1" pattern="##00" isBlankWhenNull="true">
					<reportElement mode="Opaque" x="289" y="0" width="253" height="18" backcolor="#E6E6E6" uuid="3c41b1a5-606e-40e2-abc0-e00fc9faa809"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[(($V{Total}-($V{Total}%60))/60)]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band/>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="72">
			<frame>
				<reportElement mode="Opaque" x="-20" y="-20" width="385" height="92" backcolor="#FFFFFF" uuid="c9b6bbf3-1eec-42a9-b078-c37b60acba0a"/>
				<staticText>
					<reportElement x="20" y="20" width="365" height="38" forecolor="#000000" uuid="4dc47214-e576-4fe6-b436-65df87d8dbd1"/>
					<textElement>
						<font fontName="SansSerif" size="28" isBold="false"/>
					</textElement>
					<text><![CDATA[First In - Last Out]]></text>
				</staticText>
				<textField>
					<reportElement x="20" y="58" width="347" height="20" forecolor="#000000" uuid="6cad4336-c06d-4fce-8c21-99eb161d4e58"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14"/>
					</textElement>
					<textFieldExpression><![CDATA["From " + $P{FROM_DATE} + " to " + $P{TO_DATE}]]></textFieldExpression>
				</textField>
			</frame>
			<image scaleImage="RetainShape" hAlign="Center" vAlign="Middle" onErrorType="Blank">
				<reportElement x="379" y="-7" width="164" height="75" uuid="a40d6541-ad09-4393-b30a-33b7629d117e"/>
				<imageExpression><![CDATA[$P{REPORT_LOGO}]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band height="35">
			<textField isBlankWhenNull="false">
				<reportElement x="0" y="0" width="443" height="35" forecolor="#333333" uuid="f1b405cd-0246-4e42-8a42-0e93553ab8ec"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="14" isBold="false"/>
					<paragraph leftIndent="1"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Company} == null? "Company : " : "Company : " + $F{Company}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20">
			<staticText>
				<reportElement mode="Opaque" x="460" y="0" width="95" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="aa20f47e-7277-467b-ab4b-6bff031c8db4"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<text><![CDATA[DURATION]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="366" y="0" width="95" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="708a40c5-76b7-4c66-94c9-a421ba77cb01"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[LAST OUT]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="309" y="0" width="58" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="10e475e7-b046-49d0-bc18-1624c7b677b5"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[FIRST IN]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="173" y="0" width="76" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="9f49e6c3-e1d3-4771-8137-aca053daab77"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[DAY]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="173" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="b871ca77-1394-474e-9184-4399932e0e36"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
					<paragraph leftIndent="1"/>
				</textElement>
				<text><![CDATA[EMPLOYEE DETAILS]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="249" y="0" width="61" height="20" forecolor="#404040" backcolor="#E6E6E6" uuid="9f49e6c3-e1d3-4771-8137-aca053daab77"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="false"/>
				</textElement>
				<text><![CDATA[DATE]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="14">
			<textField isStretchWithOverflow="true" pattern="h:mm:ss a" isBlankWhenNull="true">
				<reportElement x="461" y="0" width="94" height="14" uuid="29aec2d5-2b9f-40c4-9083-c0959a9297f1"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{TimeAtWork_DisplayValue_Edit}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="HH:mm.ss" isBlankWhenNull="true">
				<reportElement x="368" y="0" width="94" height="14" uuid="18a47b6e-dc66-4d7e-9d4e-cd5d91bdc012"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LastOut}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="yyyy/MM/dd" isBlankWhenNull="true">
				<reportElement x="249" y="0" width="60" height="14" uuid="26b2121f-323a-4bf0-b7f4-e923317959a7"/>
				<textElement textAlignment="Left">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{day}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="HH:mm.ss" isBlankWhenNull="true">
				<reportElement x="309" y="0" width="60" height="14" uuid="cd83a5c6-a6c7-426d-944d-159eb8eb7cbf"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FirstIn}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="EEEEE" isBlankWhenNull="true">
				<reportElement x="173" y="0" width="76" height="14" uuid="26b2121f-323a-4bf0-b7f4-e923317959a7"/>
				<textElement textAlignment="Left">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{day}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band/>
	</columnFooter>
	<pageFooter>
		<band height="15">
			<textField pattern="yyyy/MM/dd h:mm a">
				<reportElement mode="Opaque" x="446" y="1" width="109" height="13" backcolor="#E6E6E6" uuid="d93abed3-766a-4bec-804f-bd89e03bb5c6"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="172" y="1" width="117" height="13" backcolor="#E6E6E6" uuid="0f7c36ee-0c6d-4339-812b-d3320972ee92"/>
				<textElement textAlignment="Right">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="0" y="1" width="173" height="13" backcolor="#E6E6E6" uuid="86f94e73-f2ae-4e36-bafc-0d4e189700f7"/>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Accsys PeopleWare ESSTM1207"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement mode="Opaque" x="289" y="1" width="157" height="13" backcolor="#E6E6E6" uuid="b7e8f272-f0eb-4f1c-947d-3c75a9ce985c"/>
				<textElement>
					<font size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
