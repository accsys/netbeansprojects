package za.co.ucs.ess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.TreeMap;
import javax.servlet.http.HttpSession;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcess_DailySignoff;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_TnADailySignoff inherits from PageProducer and allows for the
 * approval/rejection of calculated hours </p>
 */
public class PageProducer_TnADailySignoffDetail extends PageProducer {

    /**
     * Creates an instance of PageProducer_TnADailySignoff for a single employee
     *
     * @param owner
     * @param employee
     * @param sDetailDate
     * @param sessionObject
     */
    public PageProducer_TnADailySignoffDetail(Employee owner, Employee employee, String sDetailDate, HttpSession sessionObject) {
        this.employee = employee;
        this.owner = owner;
        this.canAutoriseHours = true;
        this.sessionObject = sessionObject;
        //this.selectionID = selectionID;

        if (this.employee == null) {
            this.canAutoriseHours = false;
        } else {
            this.detailDate = sDetailDate;
        }
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {
        String imageLink = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");

        if (!canAutoriseHours) {
            // <editor-fold defaultstate="collapsed" desc="Unable to authorise hours">
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to authorise T&A hours.<br>It is possible that this employee is not linked to a payroll, or that the payroll is presently not open for calculation.", true, sessionObject);
            producer.setMessageInSessionObject();
            return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
            // </editor-fold>
        } else {
            StringBuilder resultPage = new StringBuilder();

            if (WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_AllowTnAHoursFor, "").length() == 0) {
                resultPage.append("<CENTER><h3 class=\"shaddow\">Daily Signoff requires the 'WebModulePreferences.DISPLAY_AllowTnAHoursFor' setting to be configured first.</h3></CENTER>");
            } else {
                //resultPage.append(getDataSelectionHTML());
                //
                //
                String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
                String link = baseURL + "Controller.jsp";

                // <editor-fold defaultstate="collapsed" desc="SINGLE-EMPLOYEE SELECTION">
                // History Table - per Leave_Type
                String[] headers = new String[9];
                headers[0] = "Date & Time";
                headers[1] = "Clock";
                headers[2] = "Shift";
                headers[3] = "From";
                headers[4] = "To";
                headers[5] = "In/Out";
                headers[6] = "Transaction Type";
                headers[7] = "Calculate";
                headers[8] = "Reason";

                resultPage.append("<CENTER>");
                resultPage.append("<h3 class=\"shaddow\">Daily Signoff Detail for ").append(employee.getFirstName()).append(" ").append(employee.getSurname()).append("</h3>");
                resultPage.append("<span class=\"standardHeading_NotBold\"> (note that you are limited to the current open payroll period)</span><br><br>");
                //resultPage.append(PageSectionGenerator.getInstance().getHTMLInnerTableTag("displaytable_wide"));
                resultPage.append("<br><h3 class=\"shaddow\">Clocking detail").append(" - ").append(detailDate).append("</h3>");

                int nbRecords = getClockingRecordCount();
                String[][] contents = new String[9][nbRecords];

                //
                // Load all the First-In / Last-Out records for the given period
                // But ONLY if the system has been set up to allow certain T&A variables
                StringBuilder sqlStatement = new StringBuilder();
                java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
                try {
                    sqlStatement.append("select substr(c.timestamp,11,9) as timestamp,");
                    sqlStatement.append("(select ta_clock.clock_number from DBA.ta_clock with(READUNCOMMITTED) where ta_clock.clock_id = c.clock_id) as clock,");
                    sqlStatement.append("(select ta_shift.name from DBA.ta_shift with(READUNCOMMITTED) where ta_shift.shift_id = c.shift_id) as shift,");
                    sqlStatement.append("(select ta_area.name from DBA.ta_area with(READUNCOMMITTED) where ta_area.area_id = c.fromArea_id) as from_Area,");
                    sqlStatement.append("(select ta_area.name from DBA.ta_area with(READUNCOMMITTED) where ta_area.area_id = c.toArea_id) as to_Area,");
                    sqlStatement.append("c.direction,c.Transaction_type,c.calculate,c.changed,c.Reason as ReasonForChange");
                    sqlStatement.append(" from DBA.ta_clockingrecord as c with(READUNCOMMITTED),DBA.ta_e_master as ta with(READUNCOMMITTED)");
                    sqlStatement.append(" where  ta.company_id = c.company_id and ta.person_id = c.person_id and date(c.\"timestamp\") = '");
                    sqlStatement.append(this.detailDate);
                    sqlStatement.append("' and ta.employee_id = ");
                    sqlStatement.append(employee.getEmployeeID());
                    sqlStatement.append(" and c.company_id = ");
                    sqlStatement.append(employee.getCompany().getCompanyID());
                    sqlStatement.append(" order by \"timestamp\" asc");

                    ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                    int row = 0;
                    while (rs.next()) {
                        contents[0][row] = rs.getString("timestamp");
                        contents[1][row] = rs.getString("clock");
                        // Shift
                        String shift = rs.getString("shift");
                        if (shift == null) {
                            shift = "";
                        }
                        contents[2][row] = shift;
                        contents[3][row] = rs.getString("from_Area");
                        contents[4][row] = rs.getString("to_Area");
                        // Direction
                        String direction = rs.getString("direction");
                        if (direction == null) {
                            direction = "";
                        } else if (direction.contains("1")) {
                            direction = "In";
                        } else if (direction.contains("0")) {
                            direction = "Out";
                        }
                        contents[5][row] = direction;
                        // Transaction Type
                        String transType = rs.getString("Transaction_type");
                        if (transType == null) {
                            transType = "";
                        } else if (transType.contains("0")) {
                            transType = "Manual";
                        } else if (transType.contains("1")) {
                            transType = "Reader";
                        } else if (transType.contains("2")) {
                            transType = "Live";
                        } else if (transType.contains("3")) {
                            transType = "System";
                        } else if (transType.contains("4")) {
                            transType = "Import";
                        }
                        //contents[6][row] = transType; value get assigned later
                        // Calculate
                        String calc = rs.getString("calculate");
                        if (calc == null) {
                            calc = "<img src=\"../images/AccessControl.bmp\">&nbsp;&nbsp;Access Control";
                        } else if (calc.contains("1")) {
                            calc = "<img src=\"../images/Paid.bmp\">&nbsp;&nbsp;Paid";
                        } else if (calc.contains("0")) {
                            calc = "<img src=\"../images/NotPaid.bmp\">&nbsp;&nbsp;Not Paid";
                        }
                        contents[7][row] = calc;
                        // Changed
                        String changed = rs.getString("changed");
                        if (changed == null || changed.contains("0")) {
                            changed = transType;
                        } else if (changed.contains("1")) {
                            changed = "<span style=\"color:red;\">" + transType + " ( modified )</span>";
                        }
                        contents[6][row] = changed;
                        // Reason
                        String reason = rs.getString("ReasonForChange");
                        if (reason == null) {
                            reason = "";
                        }
                        contents[8][row] = reason;

                        row++;
                    }
                    // If there is no records type it on screen
                    if (nbRecords <= 0) {
                        contents = new String[0][0];
                        
                    }
                    
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    DatabaseObject.releaseConnection(con);
                }

                resultPage.append(PageSectionGenerator.getInstance().buildHTMLTableOuter("displaytable_wide", headers, contents));
                resultPage.append("</CENTER>");
//</editor-fold>

                ///////////////////////
                // Signoff for today
                //////////////////////
                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<hr align=\"center\" width=\"1025px\">");
                PageProducer_TnADailySignoff producer;
                producer = new PageProducer_TnADailySignoff(owner, employee, detailDate, detailDate, true, sessionObject);
                resultPage.append(producer.generateInformationContent());
                resultPage.append("<br>&nbsp;<br>");
            }

            return resultPage.toString();
        }
    }

    private int getClockingRecordCount() {
        int rslt = 0;
        StringBuilder sqlStatement = new StringBuilder();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            sqlStatement.append("select count(c.timestamp) as nbRecords");
            sqlStatement.append(" from DBA.ta_clockingrecord as c with(READUNCOMMITTED),DBA.ta_e_master as ta with(READUNCOMMITTED)");
            sqlStatement.append(" where  ta.company_id = c.company_id and ta.person_id = c.person_id and date(c.\"timestamp\") = '");
            sqlStatement.append(this.detailDate);
            sqlStatement.append("' and ta.employee_id = ");
            sqlStatement.append(employee.getEmployeeID());
            sqlStatement.append(" and c.company_id = ");
            sqlStatement.append(employee.getCompany().getCompanyID());

            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            if (rs.next()) {
                rslt = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    private boolean isSignedOff(Employee anEmployee, java.util.Date aDay) {
        StringBuilder sqlStatement = new StringBuilder();
        String signedOff = null;
        java.sql.Connection con2 = DatabaseObject.getNewConnectionFromPool();
        try {
            sqlStatement.append("select first SIGNEDOFF_YN from DBA.TA_CALC_EMPLOYEELIST_DAILY where DAY = '").append(aDay).append("'");
            sqlStatement.append(" and Company_id=").append(anEmployee.getCompany().getCompanyID());
            sqlStatement.append(" and Employee_id=").append(anEmployee.getEmployeeID());
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con2);

            // Context sensitive content, to be added into the HTML later
            if (rs.next()) {
                signedOff = rs.getString("SIGNEDOFF_YN");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con2);
        }
        if ((signedOff != null) && (signedOff.compareToIgnoreCase("Y") == 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Scans the 'Live' process list to see if hours for this day has already
     * been logged. We cannot allow the person to authorise hours for a day if
     * he/she has already done so.
     *
     * @param anEmployee
     * @param aDay
     * @return
     */
    private WebProcess_DailySignoff getAlreadyLoggedProcess(Employee anEmployee, java.util.Date aDay) {
        boolean rslt = false;
        TreeMap webProcesses = FileContainer.getInstance().getWebProcessesInitiatedBy(owner, true);
        Iterator iter = webProcesses.values().iterator();
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();
            if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                Employee processEmployee = process.getEmployee();
                if (processEmployee.equals(anEmployee)) {
                    WebProcess_DailySignoff aProc = (WebProcess_DailySignoff) process;
                    if (DatabaseObject.formatDate(aProc.getAuthDay()).equals(aDay)) {
                        if (aProc.isActive()) {
                            return aProc;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * This is the MONSTER of this page. Depending on the status of that day,
     * different inner-table content is generated
     *
     * @param anEmployee
     * @param aDay
     * @param isSignedOff
     * @param loggedProcess
     * @return
     */
    private String getCalculationBreakdown(Employee anEmployee, java.util.Date aDay, boolean isSignedOff, WebProcess_DailySignoff loggedProcess) {
        StringBuilder sqlStatement = new StringBuilder();
        StringBuilder rslt = new StringBuilder();
        String imageLink = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");

        if ((!isSignedOff) && (loggedProcess != null)) {
            StringBuilder loggedDetail = new StringBuilder();
            // <editor-fold defaultstate="collapsed" desc="LOGGED AND APPROVED">
            //
            // Header 
            //
            loggedDetail.append("<table border=0 width=100% align=\"left\">");
            loggedDetail.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td></tr> ");
            loggedDetail.append("  <tr><td>").append(loggedProcess.toShortHTMLString()).append("</td>");
            loggedDetail.append("  </tr></table>");
            return loggedDetail.toString();
            // </editor-fold>
        }

        // <editor-fold defaultstate="collapsed" desc="OPEN / NOT YET LOGGED">
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String variableList = WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_AllowTnAHoursFor, "");
            // <editor-fold defaultstate="collapsed" desc="SELECT VARIABLES TO DISPLAY">
            if (variableList.length() > 0) {

                // We need to build a list of variable names that can be called from the SQL's ... in (  ) clause
                StringBuilder tokenisedList = new StringBuilder();
                StringTokenizer token = new StringTokenizer(variableList, ",");
                while (token.hasMoreElements()) {
                    if (tokenisedList.length() == 0) {
                        tokenisedList.append("'").append(token.nextToken().trim()).append("'");
                    } else {
                        tokenisedList.append(",'").append(token.nextToken().trim()).append("'");
                    }
                }
                sqlStatement.append("select v.variable_id, v.name as theVariable, e.company_id, e.employee_id, s.day, ");
                sqlStatement.append("round(COALESCE((select sum(ta.theValue) from DBA.tal_emaster_pvariable_daily as ta where ta.company_id = e.company_id and ta.employee_id = e.employee_id ");
                sqlStatement.append("and ta.Day = s.Day and ta.variable_id = v.variable_id)*60,0),0) as MinutesAfterSCL, ");
                sqlStatement.append("right('00'||convert(integer,MinutesAfterSCL/60),2)||':'||right('00'||convert(integer,remainder(MinutesAfterSCL,60)),2) as HHMM_CALC, ");
                sqlStatement.append("round(COALESCE((select sum(ta.FINAL_VALUE) from DBA.tal_emaster_pvariable_daily as ta where ta.company_id = e.company_id and ta.employee_id = e.employee_id ");
                sqlStatement.append("and ta.Day = s.Day and ta.variable_id = v.variable_id)*60,0),0) as MinutesAfterSignoff, ");
                sqlStatement.append("right('00'||convert(integer,MinutesAfterSignoff/60),2)||':'||right('00'||convert(integer,remainder(MinutesAfterSignoff,60)),2) as HHMM_SO ");

                sqlStatement.append(" from p_variable v, e_master e, S_CALENDAR_DAYs s with (readuncommitted) ");
                sqlStatement.append(" where s.day='").append(aDay).append("'");
                sqlStatement.append(" and v.name in  (").append(tokenisedList.toString()).append(") ");
                sqlStatement.append(" and e.Company_id=").append(anEmployee.getCompany().getCompanyID());
                sqlStatement.append(" and e.Employee_id=").append(anEmployee.getEmployeeID());
                sqlStatement.append(" order by v.name");

                ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                StringBuilder pageContent = new StringBuilder();

                if (isSignedOff) {
                    // <editor-fold defaultstate="collapsed" desc="SIGNED-OFF">
                    pageContent.append("<table id=\"displaytable_inner_italics\" >");
                    //pageContent.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td><td width=\"30\" align=\"right\"></td></tr> ");
                    pageContent.append("  <tr><td>");
                    pageContent.append("          <table  id=\"displaytable_inner_italics\"><font size='-3'>");
                    pageContent.append("             <tr><th>Variable</th><th>Signed Off</th></tr> ");
                    while (rs.next()) {
                        if (rs.getString("HHMM_SO").compareTo("00:00") != 0) {
                            pageContent.append("         <tr><td align=\"left\">").append(rs.getString("theVariable")).append("</td>");
                            pageContent.append("             <td align=\"left\">").append(rs.getString("HHMM_SO")).append("</td>");
                            pageContent.append("             </tr>");
                        }
                    }
                    pageContent.append("          </table></td></tr>");

                    // </editor-fold>
                } else {
                    // <editor-fold defaultstate="collapsed" desc="NOT SIGNED-OFF">

                    pageContent.append("<div class=\"grouping\" style=\"display: table; border: none;\"><table id=\"displaytable_inner\" width=100% align=\"left\">");
                    //pageContent.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td><td width=\"30\" align=\"right\"></td></tr> ");
                    pageContent.append("  <tr><td>");
                    pageContent.append("          <table id=\"displaytable_inner\"><font size='-3'>");
                    pageContent.append("             <tr><th>Variable</th><th>Calculated</th><th>Modified</th><th>Reason</th></tr> ");

                    //
                    // Does the client use a User Defined Field to define the drop-down list of reasons, or is it text?
                    //
                    String userDefinedFieldName = WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_UserDefinedFieldForTnAReasons, "");
                    LinkedList userDefReasons = new LinkedList();
                    StringBuilder userDefinedHTMLOptions = new StringBuilder();
                    if (userDefinedFieldName.trim().length() > 0) {
                        // Add Reasons from the User Defined Fields
                        //
                        userDefReasons = FileContainer.getInstance().getUserDefinedFieldItems(userDefinedFieldName.trim());
                        Iterator iter = userDefReasons.iterator();
                        while (iter.hasNext()) {
                            String aReason = (String) iter.next();
                            userDefinedHTMLOptions.append("<option VALUE=\"").append(aReason).append("\" >").append(aReason).append("</option>");
                        }
                    }

                    while (rs.next()) {
                        pageContent.append("  <tr>");
                        pageContent.append("    <td class=\"smallFont\">").append(rs.getString("theVariable")).append("</td>");
                        pageContent.append("    <td class=\"smallFont\">").append(rs.getString("HHMM_CALC")).append("</td>");

                        // Construct a field that we can decipher again later
                        // consisting of company_id, employee_id, day, variable_id, old_value 
                        String subsetHrs = rs.getString("HHMM_CALC").substring(0, 2);
                        String subsetMins = rs.getString("HHMM_CALC").substring(3, 5);

                        String fieldValueHrsName = "HRS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC").substring(0, 5);
                        String fieldValueMinsName = "MNS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC").substring(0, 5);
                        String fieldReasonName = "RSN:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC");

                        pageContent.append("<td>");
                        pageContent.append("<input maxlength=\"2\" size=\"2\"  name=\"").append(fieldValueHrsName).append("\" value=\"").append(subsetHrs).append("\">");
                        pageContent.append(":");
                        pageContent.append("<input maxlength=\"2\" size=\"2\"  name=\"").append(fieldValueMinsName).append("\" value=\"").append(subsetMins).append("\">");
                        pageContent.append("</td>");

                        // TEST reason, or a drop-down list based on User Defined Fields?
                        if ((userDefinedFieldName.trim().length() > 0) && (userDefReasons.size() > 0)) {
                            pageContent.append("   <td> <select name=\"").append(fieldReasonName).append("\" >").append(userDefinedHTMLOptions).append(" </select></td>");
                        } else {
                            pageContent.append("   <td> <input maxlength=\"100\" size=\"40\"  name=\"").append(fieldReasonName).append("\" > </td>");
                        }
                        pageContent.append("  </tr>");
                        // </editor-fold>

                    }
                    pageContent.append("          </table></td></tr>");

                }
                rslt.append(pageContent.toString()).append("</font></table></div>");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt.toString();
        // </editor-fold>

    }

    /**
     * @return the canAutoriseHours
     */
    public boolean canAutoriseHours() {
        return canAutoriseHours;
    }
    // <editor-fold defaultstate="collapsed" desc="PRIVATE DECLARATIONS">
    private Employee employee = null;
    private Employee owner = null;
    private String detailDate;
    private boolean canAutoriseHours = true;
    private final HttpSession sessionObject;
    private LinkedList<Employee> selectedEmployees = null;
// </editor-fold>
} // end PageProducer_TnADailySignoff

