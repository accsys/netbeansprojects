package za.co.ucs.ess;

/*
 * ControllerServlet.java Created on May 17, 2004, 1:45 PM
 */
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import za.co.ucs.accsys.peopleware.Company;
import za.co.ucs.accsys.peopleware.Contact;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.Family;
import za.co.ucs.accsys.peopleware.LeaveInfo;
import za.co.ucs.accsys.peopleware.PasswordManager;
import za.co.ucs.accsys.peopleware.Variable;
import za.co.ucs.accsys.webmodule.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * The ControllerServlet will act as the central point of reference for all
 * communication between the various JSP's. Apart from session objects, a
 * parameter named 'action' must always be passed to this servlet. This
 * parameter will dictate which JSP to call. Furthermore a Session Object of
 * type za.co.ucs.accsys.webmodule.LoginSessionObject should be available. If
 * not, the call will be routed to the login screen
 *
 * @author liam
 * @version 1.0.0
 */
public class ControllerServlet extends HttpServlet {

    private String baseURL;
    private String dbURL;

    /**
     * Initializes the servlet.
     *
     * @param config
     * @throws ServletException
     */
    @Override
    @SuppressWarnings("static-access")
    public void init(ServletConfig config) throws ServletException {

//        // Get File Container
//        if (this.fileContainer == null) {
//            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
//            System.out.println("++ Creating FileContainer instance...");
//            this.fileContainer = za.co.ucs.accsys.webmodule.FileContainer.getInstance(true);
//            System.out.println("++          FileContainer instance created.");
//        }
        // Are we running compatible Database and JAR resources?
        if (!validateVersion()) {
            return;
        }

        /**
         * Initialise global variables
         */
        //System.out.println("++ Initializing controller servlet: "+config.getServletName());
        ServletContext context = config.getServletContext();
        baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        dbURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "");

        // Pass the config parameters to the ServletContext object
        context.setAttribute("baseUrl", baseURL);
        context.setAttribute("dbUrl", dbURL);

        super.init(config);
    }

    /**
     * Destroys the servlet.
     */
    @Override
    public void destroy() {
    }

    private void displayRequestInfo(HttpServletRequest request) {
        //System.out.println("\nControllerServlet: Params");
        Enumeration _enum = request.getParameterNames();
        while (_enum.hasMoreElements()) {
            String paramName = (String) _enum.nextElement();
            String paramValue = request.getParameter(paramName);
            // Watch out for the passwords!
            if (paramName.compareToIgnoreCase("password") == 0) {
                System.out.println("\tParam:" + paramName + "\tValue: ****");
            } else {
                System.out.println("\tParam:" + paramName + "\tValue:" + paramValue);
            }
        }
    }

    private Long getHttpServletRequestCheckSum(HttpServletRequest theRequest) {
        long paramChkSum = 0;
        long valChkSum = 0;
        long contentChkSum = 0;
        String contentType = "";
        Enumeration e = theRequest.getParameterNames();
        while (e.hasMoreElements()) {
            String param = (String) e.nextElement().toString().trim();
            if (param.contains("fileID")) {
                contentChkSum += Math.random() * (new Integer(theRequest.getParameter("fileID")));
            }
            paramChkSum += Math.abs(param.hashCode());
            if (theRequest.getSession().getAttribute("selectedEmployeeHashCodesToLink") != null && theRequest.getSession().getAttribute("selectedEmployeeHashCodesToUnlink") != null) {
                paramChkSum += Math.abs(theRequest.getSession().getAttribute("selectedEmployeeHashCodesToLink").hashCode());
                paramChkSum += Math.abs(theRequest.getSession().getAttribute("selectedEmployeeHashCodesToUnlink").hashCode());
            }
            valChkSum += theRequest.getParameterValues(param)[0].hashCode();
        }
        // File upload and download
        contentType = theRequest.getContentType();
        if (contentType != null) {
            contentChkSum += Math.abs(contentType.hashCode());
        }
        return paramChkSum + valChkSum + contentChkSum;
    }

    /**
     * This routine performs a unique check in a circular buffer to see if a
     * person might have double_clicked the action resulting in two identical
     * HTTPServletRequests being sent to the Controller class
     *
     * @param request - HttpServletRequest
     * @return Boolean - True if the request already exists in the buffer.
     */
    private synchronized boolean isDuplicateRequest(HttpServletRequest theRequest) {
        Long hashValueInBuffer;
        Long hashValueInRequest = getHttpServletRequestCheckSum(theRequest);
        Collection c = Collections.synchronizedCollection(FileContainer.serverRequestHashValueBuffer);

        //
        // Step through each hashvalue in the buffer set
        //
        //System.out.println("**** request(param size):" + theRequest.getParameterMap().size());
        synchronized (c) {
            Iterator i = c.iterator(); // Must be in the synchronized block
            while (i.hasNext()) {
                hashValueInBuffer = ((Long) i.next());

                if (hashValueInBuffer.equals(hashValueInRequest)) {
                    return true;
                }
            }
        }

        FileContainer.serverRequestHashValueBuffer.add(hashValueInRequest);

        if (FileContainer.serverRequestHashValueBuffer.size() > 10) {

            FileContainer.serverRequestHashValueBuffer.removeFirst();
            //System.out.println("Buffer size:" + FileContainer.serverRequestHashValueBuffer.size() + "\nRemoved last item.");
        }
        return false;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "processes/Welcome.jsp";

        if (isDuplicateRequest(request)) {
            // Nothing to do as this was probably caused by a double-click
            System.out.println("Duplicate request");
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Duplicate request", false, request.getSession());
            producer.setMessageInSessionObject();
        } else {
            String action = request.getParameter("action");
            String fileID = request.getParameter("fileID");
            LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
            if (action != null) {
                // If this is the LOGIN action, clear the request hashvalue buffer
                if (request.getParameter("action").equals("Login")) {
                    FileContainer.serverRequestHashValueBuffer.clear();
                }

                // Are we Reset my Password?
                if (action.equals("Send new password")) {
                    processResetPasswordURL(request, response);
                } else // Are we logging in?
                 if (action.equals("Login") && (request.getParameter("userName") != null) && request.getParameter("userName").trim().length() > 0) {
                        loginSessionObject = logIn(request);

                        // It might be necessary to forward this screen to one that was originally intended
                        // to go to.  Example: The user wishes to view Process History, but needs to log in first.
                        // If the original URL pointed him to that Process, we should, after successful login,
                        // reroute to the original URI
                        String originalQuery = (String) request.getSession().getAttribute("originalQuery");
                        String originalURI = (String) request.getSession().getAttribute("originalURI");

                        if (originalURI != null) {
                            if (originalQuery != null) {
                                url = originalURI + "?" + originalQuery;
                            } else {
                                url = originalURI;
                            }

                            // If the previous page was the password reset page go to the home page
                            if (originalURI.contains("/WebModule/processes/ForgotPassword.jsp")) {
                                url = "/WebModule/processes/Welcome.jsp";
                            }

                            // After redirecting, we need to remove the attribute
                            request.getSession().setAttribute("originalURI", null);
                        }

                        // Now that we've logged in, is this password still valid?
                        // CR 12012 - Expiry of passwords
                        request.getSession().setAttribute("expiredPassword", "FALSE");
                        if (loginSessionObject != null) {
                            Employee loginEmp = loginSessionObject.getUser();
                            String loginPwd = request.getParameter("password");
                            request.getSession().setAttribute("loginsessionobject", loginSessionObject);
                            int daysBeforePWDExpire = PasswordManager.getInstance().getDaysBeforePwdExpire(loginEmp);

                            // Almost expired
                            if (daysBeforePWDExpire <= 14) {
                                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                        "Your ESS Password will expire in " + daysBeforePWDExpire + " day(s).  <br>Please change your password as soon as possible.", false, request.getSession());
                                producer.setMessageInSessionObject();
                            }

                            // Already expired
                            if (daysBeforePWDExpire <= 0) {
                                request.getSession().setAttribute("expiredPassword", "TRUE");
                                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                        "Your ESS Password has expired.  <br>Please reset your password using the link on the login screen.", false, request.getSession());
                                producer.setMessageInSessionObject();
                            }
                        }

                    } else // Are we logging out?
                     if (action.equals("logout")) {
                            logOut(request);
                        } else // If we are not logging In or Out or Forgot or Reset password, there is no need to process
                        // any requests further
                         if (loginSessionObject == null) {
                                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                        "Your session has timed out, please log in again.", false, request.getSession());
                                producer.setMessageInSessionObject();

                            } else {

                                // Change Password
                                if (action.equals("changepassword")) {
                                    processChangePasswordURL(request, response);
                                }
                                // Request Leave
                                if (action.equals("req_leave")) {
                                    processLeaveRequest(request, response, null);
                                }
                                // Request Leave Cancellation
                                if (action.equals("req_leavecancellation")) {
                                    processLeaveCancellationRequest(request, response);
                                }
                                // Request Variable Value Changes
                                if (action.equals("req_variablechanges")) {
                                    processVariableValueChangeRequest(request, response);
                                }
                                // Family Information Change
                                if (action.equals("req_family")) {
                                    processFamilyRequest(request, response);
                                }
                                // Employee Contact Information Change
                                if (action.equals("req_employeecontact")) {
                                    processEmployeeContactRequest(request, response);
                                }
                                // Training Request
                                if (action.equals("req_training")) {
                                    processEmployeeTrainingRequest(request, response);
                                }
                                // T&A Hours Request
                                if (action.equals("req_tna")) {
                                    processEmployeeTnARequest(request, response);
                                }
                                // T&A Roster Request
                                if (action.equals("req_tna_roster")) {
                                    processEmployeeTnaRosterRequest(request, response);
                                }
                                // Reply to Process Request
                                if (action.equals("repl_proc")) {
                                    processRequestReply(request, response);
                                    url = getInboxURL(request);
                                }
                                // Go to Previous Page
                                if (action.equals("goback")) {
                                    url = getGoBackURL(request);
                                }
                                // Reply to Process Request
                                if (action.equals("gohome")) {
                                    url = getGoHomeURL(request);
                                }
                                // System Admin
                                if (action.equals("admin")) {
                                    processWebAdmin(request, response);
                                }
                                // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
                                loginSessionObject.setEmployee(loginSessionObject.getUser());
                            }
            } else { // File download
                //Need to get apache temp folder
                String relativeWebPath = FileContainer.getOSFileSeperator() + "temp";
                String catalinaBase = System.getProperty("catalina.base");
                String savePath = catalinaBase + relativeWebPath;

                if (fileID != null) {

                    // Get requested file by path info.
                    // Decode the file name (might contain spaces and on) and prepare file object.
                    File file = new File(".");
                    File dir = new File(savePath + FileContainer.getOSFileSeperator());
                    FileFilter fileFilter = new WildcardFileFilter(fileID + ".*");
                    File[] files = dir.listFiles(fileFilter);
                    for (File fileInList : files) {
                        file = fileInList;
                    }

                    if (!(files.length > 0)) {
                        // Do your thing if the file is not supplied to the request URI.
                        // Throw an exception, or send 404, or show default/warning page, or just ignore it.
                        response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
                        return;
                    }

                    // Get content type by filename.
                    String contentType = getServletContext().getMimeType(file.getName());

                    // If content type is unknown, then set the default value.
                    // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
                    // To add new content types, add new mime-mapping entry in web.xml.
                    if (contentType == null) {
                        contentType = "application/octet-stream";
                    }

                    // Init servlet response.
                    response.reset();
                    response.setBufferSize(DEFAULT_BUFFER_SIZE);
                    response.setContentType(contentType);
                    response.setHeader("Content-Length", String.valueOf(file.length()));
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

                    // Prepare streams.
                    BufferedInputStream input = null;
                    BufferedOutputStream output = null;

                    try {
                        // Open streams.
                        input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
                        output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

                        // Write file contents to response.
                        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                        int length;
                        while ((length = input.read(buffer)) > 0) {
                            output.write(buffer, 0, length);
                        }
                    } finally {
                        // Gently close streams.
                        close(output);
                        close(input);
                    }
                } else {
                    // File upload with leave
                    String contentType = request.getContentType();
                    if ((contentType.contains("multipart/form-data"))) {
                        int maxFileSize = 1024 * 1024 * 200;
                        int maxMemSize = 1024 * 1024 * 200;

                        DiskFileItemFactory factory = new DiskFileItemFactory();
                        // maximum size that will be stored in memory
                        factory.setSizeThreshold(maxMemSize);
                        // Location to save data that is larger than maxMemSize.
                        factory.setRepository(new File(savePath));

                        // Create a new file upload handler
                        ServletFileUpload upload = new ServletFileUpload(factory);
                        // maximum file size to be uploaded.
                        upload.setSizeMax(maxFileSize);
                        try {
                            // Parse the request to get file items.
                            List fileItems = upload.parseRequest(request);
                            // Process the uploaded file items
                            Iterator i = fileItems.iterator();

                            while (i.hasNext()) {
                                FileItem fi = (FileItem) i.next();
                                if (fi.isFormField()) {
                                    if (fi.getFieldName().contains("action")) {
                                        action = Streams.asString(fi.getInputStream());
                                        // Request Leave
                                        if (action.equals("req_leave")) {
                                            processLeaveRequest(request, response, fileItems);
                                        }
                                    }
                                } else if (!fi.isFormField()) {
                                    // If the file is to large then stop the upload
                                    if (fi.getSize() > (1024 * 1024)) {
                                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                                "The file you are trying to upload is to big.<br>The file size limit is 1MB.<br>Please try again.", false, request.getSession());
                                        producer.setMessageInSessionObject();
                                        url = "processes/leave_req.jsp";
                                        break;
                                    }
                                }
                            }
                        } catch (FileUploadException | IOException ex) {
                            System.out.println("File upload error: " + ex);
                        }
                    } // End file upload - multipart content
                } // End of FileID != null
            }
        }// End not duplicate

        //System.out.println("ControllerServlet:Forwarding to " + url + "\n");
        // redirects client to message page
        response.sendRedirect(response.encodeURL(url));
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                // Do your thing with the exception. Print it, log it or mail it.
                System.out.println(e);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * Uses the loginName and password to extract the appropriate user
     */
    private LoginSessionObject getNewLoginSessionObject(String loginName, String password) {
        Employee employee = FileContainer.getEmployeeFromContainer(loginName, password);

        LoginSessionObject loginSessionObject = null;
        if (employee != null) {
            loginSessionObject = new LoginSessionObject(employee, employee);
        }

        return loginSessionObject;
    }

    @SuppressWarnings("static-access")
    private LoginSessionObject logIn(HttpServletRequest request) {
        // User Name
        String userName = request.getParameter("userName").trim();
        String password = request.getParameter("password").trim();
        String success = "0";

        // If valid username and password
        //System.out.println("Username & Password:"+userName+" | "+password);
        LoginSessionObject loginSessionObject = getNewLoginSessionObject(userName, password);
        String hostName = null;

        if (loginSessionObject == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Incorrect login information, please try again.", false, request.getSession());
            producer.setMessageInSessionObject();

        } else {
            // CR 6431 - Notify user of successful login attempt
            //System.out.println("On Login:" + WebModulePreferences.getInstance().getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false));
            if (WebModulePreferences.getInstance().getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILMsgOnLogin, false)) {
                StringBuilder msg = new StringBuilder();
                msg.append("<b>Dear ").append(loginSessionObject.getUser().getFirstName()).append(" (").append(loginSessionObject.getUser().getWeb_login()).append(") </b><br>");
                msg.append("This is a security notification. <br>");
                msg.append("You (or someone using your login detail) have just logged into the Peopleware ESS Module from the following address:<br>");
                msg.append("PC IP ADDRESS (").append(request.getRemoteAddr()).append(")<br>");
                try {
                    InetAddress inet = InetAddress.getByName(request.getRemoteHost());
                    hostName = inet.getHostName();
                } catch (UnknownHostException uhe) {
                }

                if ((hostName != null) && (request.getRemoteAddr().compareTo(hostName) != 0)) {
                    msg.append("COMPUTER NAME (").append(hostName).append(")<br>");
                }

                E_Mail.getInstance().send(loginSessionObject.getUser().getEMail(), "", "Peopleware Notification", msg.toString());
            }
            success = "1";
        }

        // Log this login attempt in the ESS_LOGIN_AUDIT Table
        // CR: 14507
        DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            if (loginSessionObject == null) {
                DatabaseObject.executeSQL("INSERT INTO ESS_LOGIN_AUDIT "
                        + "(TIME_STAMP,USERNAME,IP_ADDRESS,COMPUTER_NAME,LOGIN_SUCCESSFUL) "
                        + "VALUES ('"
                        + ((new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS")).format(new java.util.Date())) + "','"
                        + userName + "','"
                        + request.getRemoteAddr() + "','"
                        + hostName + "',"
                        + success + ")", con);
            } else {
                DatabaseObject.executeSQL("INSERT INTO ESS_LOGIN_AUDIT "
                        + "(TIME_STAMP,COMPANY_ID,EMPLOYEE_ID,COMPANY_EMPLOYEE_NUMBER,USERNAME,IP_ADDRESS,COMPUTER_NAME,LOGIN_SUCCESSFUL) "
                        + "VALUES ('"
                        + ((new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS")).format(new java.util.Date())) + "',"
                        + loginSessionObject.getUser().getCompany().getCompanyID() + ","
                        + loginSessionObject.getUser().getEmployeeID() + ",'"
                        + loginSessionObject.getUser().getEmployeeNumber() + "','"
                        + userName + "','"
                        + request.getRemoteAddr() + "','"
                        + hostName + "',"
                        + success + ")", con);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }

        return loginSessionObject;
    }

    private void processChangePasswordURL(HttpServletRequest request, HttpServletResponse response) {

        try {
            PrintWriter out = response.getWriter();
            // New Password
            String newPass = request.getParameter("newPass");
            // Confirm Password
            String confirmNewPass = request.getParameter("confirmNewPass");
            // Who is the currently connected user?
            LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");

            Employee owner = loginSessionObject.getUser();
            String oldPassword = owner.getWeb_pass();

            // Some validation
            //1. Is the password empty?
            if (((newPass == null) || (confirmNewPass == null))
                    || ((newPass.trim().length() == 0) || (confirmNewPass.trim().length() == 0))) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "You must provide a valid password and retype it to confirm the change.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;

            }
            //2. Are both the same?
            if ((newPass.compareTo(confirmNewPass) != 0)) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "The two passwords are not the same.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;

            }
            //3. Is this username/password combination unique?
            if (!owner.canChangePasswordInDatabase(newPass)) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "This is not a valid password.  Please use another password.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;
            }
            //4. Password used before?
            String reusePasswords = WebModulePreferences.getInstance().getPreference(WebModulePreferences.PASSWORDS_reUsable, "TRUE");
            if (reusePasswords.compareToIgnoreCase("FALSE") == 0) {
                if (PasswordManager.getInstance().wasPasswordUsed(owner, newPass)) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "Your ESS disallows the re-use of passwords.  Please choose another password.", true, request.getSession());
                    producer.setMessageInSessionObject();
                    return;
                }
            }

            // Now we can change it
            owner.changePasswordInDatabase(newPass);
            request.getSession().setAttribute("expiredPassword", "FALSE");
            // Notify user of successful change
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "Your have successfully changed your password.", false, request.getSession());
                producer.setMessageInSessionObject();
            }

            // Our FileContainer caches the login/passwords for expediency. 
            // Let's add this new login/password to that mapping
            FileContainer.getInstance().updateLoginPwdMapping(owner, oldPassword, newPass);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private void processResetPasswordURL(HttpServletRequest request, HttpServletResponse response) {

        try {
            PrintWriter out = response.getWriter();
            // New Password
            String email = request.getParameter("email");
            int company_ID = 0;
            int employee_ID = 0;

            // Select the frist person from the database with a matching email
            DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select FIRST COMPANY_ID, EMPLOYEE_ID from E_CONTACT with (nolock) where E_MAIL = '" + email + "' order by EMPLOYEE_ID", con);
                if (rs.next()) {
                    company_ID = rs.getInt("COMPANY_ID");
                    employee_ID = rs.getInt("EMPLOYEE_ID");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }

            // Send message if the mail is not valid
            if (company_ID == 0 || employee_ID == 0) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "No matching employee found for e-mail address: '" + email + "'.<br>Please make sure that you have entered the correct e-mail address and try again.",
                        false, request.getSession());
                producer.setMessageInSessionObject();
            } else {
                // Valid email then continue
                Employee employee = (Employee) FileContainer.getEmployeeFromContainer(company_ID, employee_ID);
                // Gerate new random  password
                String oldPassword = employee.getWeb_pass();
                SecureRandom random = new SecureRandom();
                String newPass = new BigInteger(32, random).toString(32).toUpperCase();
                // Now we can change it
                employee.changePasswordInDatabase(newPass);
                request.getSession().setAttribute("expiredPassword", "FALSE");
                // send new password to the employee's email address
                String message = "<h4>Dear " + employee.toString() + "</h4>"
                        + "<br>Your ESS login credentials was successfully reset."
                        + "<br>&nbsp;<br>Your new ESS login credentials are:"
                        + "<br>&nbsp;&nbsp;&nbsp;Username - " + employee.getWeb_login()
                        + "<br>&nbsp;&nbsp;&nbsp;Password  - " + newPass
                        + "<br>&nbsp;<br>Please log onto ESS using these new credentials.";
                E_Mail.getInstance().send(email, "", "ESS Password Reset", message, null);
                // Our FileContainer caches the login/passwords for expediency. 
                // Let's add this new login/password to that mapping
                FileContainer.getInstance().updateLoginPwdMapping(employee, oldPassword, newPass);
                // Notify user of successful change
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "Your new password was sent to your e-mail address.", false, request.getSession());
                producer.setMessageInSessionObject();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private void logOut(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        HttpSession session = request.getSession(true);
        session.removeAttribute("loginsessionobject");
    }

    private String getGoBackURL(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        HttpSession session = request.getSession(true);
        return ("javascript:history.back()");
    }

    private String getGoHomeURL(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        return ("processes/Welcome.jsp");
    }

    private String getForgotPwdURL(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        return ("processes/ForgotPassword.jsp");
    }

    private String getChangePasswordURL(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        return ("processes/ChangePassword.jsp");
    }

    private String getInboxURL(HttpServletRequest request) {
        // Remove the current LoginSessionObject
        HttpSession session = request.getSession(true);
        return ("processes/proc_todo.jsp");
    }

    /**
     * Generates a leave request and forwards the result to a new URL
     */
    @SuppressWarnings("CallToThreadDumpStack")
    private void processLeaveRequest(HttpServletRequest request, HttpServletResponse response, List fileItems) {
        try {
            // Param init
            FileItem attachment = null;
            PrintWriter out = response.getWriter();
            // Leave Type
            int leaveTypeID = 0;
            // Leave Unit
            int leaveUnit = 0;
            // From Date
            String fromDate = "";
            // Hours and Minutes
            int hours = 0;
            int minutes = 0;
            // To Date
            String toDate = "";
            // Half Day
            String halfDayString = "";
            boolean halfDay = false;
            // Reason
            String reason = "";
            // AdditionalDetail
            String additionalDetail = "";
            // IncidentID
            String incidentID = "";

            // Check if this request has a file attached
            if (!(fileItems == null) && fileItems.size() > 0) {
                // Extract all the paramaters
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (fi.isFormField()) {
                        String paramName = fi.getFieldName();
                        switch (paramName) {
                            // Leave Type
                            case "cbbLeaveTypeID":
                                leaveTypeID = new Integer(Streams.asString(fi.getInputStream()));
                                break;
                            // Leave Unit
                            case "cbbLeaveUnit":
                                leaveUnit = new Integer(Streams.asString(fi.getInputStream()));
                                break;
                            // From Date
                            case "fromDate":
                                fromDate = Streams.asString(fi.getInputStream());
                                break;
                            // Hours
                            case "cbbHours":
                                hours = new Integer(Streams.asString(fi.getInputStream()));
                                break;
                            //Minutes
                            case "cbbMinutes":
                                minutes = new Integer(Streams.asString(fi.getInputStream()));
                                break;
                            // To Date
                            case "toDate":
                                toDate = Streams.asString(fi.getInputStream());
                                break;
                            // Half Day
                            case "cbHalfDay":
                                halfDayString = Streams.asString(fi.getInputStream());
                                halfDay = ((halfDayString != null) && (halfDayString.compareToIgnoreCase("HalfDay") == 0));
                                break;
                            // Reason
                            case "reason":
                                reason = Streams.asString(fi.getInputStream());
                                break;
                            // AdditionalDetail
                            case "additionalDetail":
                                additionalDetail = Streams.asString(fi.getInputStream());
                                break;
                            // IncidentID
                            case "incidentID":
                                incidentID = Streams.asString(fi.getInputStream());
                                break;
                            default:
                                break;
                        } // End switch
                    } else if (!fi.isFormField()) { // End isFormField
                        if (!fi.isFormField()) {
                            try {
                                // Set the attachment to use it later
                                attachment = fi;
                            } catch (Exception ex) {
                                System.out.println(ex);
                            }
                        }
                    } // End is NOT FormField - File itself
                } // End while
            } else {
                // Leave Type
                leaveTypeID = new Integer(request.getParameter("cbbLeaveTypeID"));
                // Leave Unit
                leaveUnit = new Integer(request.getParameter("cbbLeaveUnit"));
                // From Date
                fromDate = request.getParameter("fromDate");
                // Hours and Minutes
                if (leaveUnit == 1) {
                    // Hours
                    hours = new Integer(request.getParameter("cbbHours"));
                    //Minutes
                    minutes = new Integer(request.getParameter("cbbMinutes"));
                }
                // To Date
                toDate = request.getParameter("toDate");
                // Half Day
                halfDayString = request.getParameter("cbHalfDay");
                halfDay = ((halfDayString != null) && (halfDayString.compareToIgnoreCase("HalfDay") == 0));
                // Reason
                reason = request.getParameter("reason");
                // AdditionalDetail
                additionalDetail = request.getParameter("additionalDetail");
                // IncidentID
                incidentID = request.getParameter("incidentID");
            }

            // If the user requested a Half-Day leave, set the ToDate=FromDate
            if (halfDay || leaveUnit == 1) {
                toDate = fromDate;
            }
            //System.out.println("ControllerServlet - Requesting Leave (fromDate="+fromDate+", toDate="+toDate+")");

            // Perform simple validation
            if (leaveUnit == 0) {
                if ((fromDate == null) || (toDate == null) || (fromDate.trim().length() == 0) || (toDate.trim().length() == 0)) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "You must enter a <b>From Date</b> and a <b>To Date</b>.", true, request.getSession());
                    producer.setMessageInSessionObject();
                    return;
                }
            }

            // Perform date format validation
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            // From Date
            try {
                df.parse(fromDate);
            } catch (ParseException e) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "You must enter a valid <b>From Date</b><br>The required format is:<b>yyyy/mm/dd</b>.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;
            }

            // To Date
            if (leaveUnit == 0) {
                try {
                    df.parse(toDate);
                } catch (ParseException e) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "You must enter a valid <b>To Date</b><br>The required format is:<b>yyyy/mm/dd</b>.", true, request.getSession());
                    producer.setMessageInSessionObject();
                    return;
                }
            }

            //1. StartDate before EndDate
            if (DatabaseObject.formatDate(fromDate).after(DatabaseObject.formatDate(toDate))) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Start Date cannot be later than End Date.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;
            }

            // Who is the user requesting this process?
            LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
            Employee owner = loginSessionObject.getUser();
            Employee employee = loginSessionObject.getEmployee();

            //2. StartDate before Engagement Date
            if (DatabaseObject.formatDate(fromDate).before(employee.getLastDOE())) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Leave start date (" + fromDate + ") cannot be earlier than the latest engagement date (" + DatabaseObject.formatDate(employee.getLastDOE()) + ").", true, request.getSession());
                producer.setMessageInSessionObject();
                return;
            }

            // Generate instance of a WebProcess_LeaveRequisition
            // Get the leave type and profile name and then choose the right structure
            LeaveInfo leaveInfo = FileContainer.getInstance().getLeaveInfoFromContainer(employee);
            String profileID = leaveInfo.getLeaveProfileID(leaveTypeID);
            String caption = leaveInfo.getProfileName(leaveInfo.getIndexOfLeaveType(leaveTypeID));
            String leaveType = leaveInfo.getLeaveTypeName(Integer.toString(leaveTypeID));
            String webProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition" + profileID + "_" + leaveType + "_" + caption;
            WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);
            // Select the default leave structure if the specific one is not available
            if (procDef == null) {
                procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
            }
            WebProcess_LeaveRequisition leaveRequest;
            float nbWorkingDays = 0;

            // Half Day?
            if (halfDay) {
                leaveRequest
                        = new WebProcess_LeaveRequisition(procDef, owner, employee, fromDate, true, leaveTypeID, reason, additionalDetail, incidentID, attachment);
                nbWorkingDays = leaveRequest.getRequestedWorkingDays(DatabaseObject.formatDate(fromDate), DatabaseObject.formatDate(fromDate));
            } else if (leaveUnit == 1) {
                leaveRequest
                        = new WebProcess_LeaveRequisition(procDef, owner, employee, fromDate, hours, minutes, leaveTypeID, reason, additionalDetail, incidentID, attachment);
                nbWorkingDays = leaveRequest.getRequestedWorkingDays(DatabaseObject.formatDate(fromDate), DatabaseObject.formatDate(fromDate));
            } else {
                leaveRequest
                        = new WebProcess_LeaveRequisition(procDef, owner, employee, fromDate, toDate, leaveTypeID, reason, additionalDetail, incidentID, attachment);
                nbWorkingDays = leaveRequest.getRequestedWorkingDays(DatabaseObject.formatDate(fromDate), DatabaseObject.formatDate(toDate));
            }

            //3. Working day?
            if (nbWorkingDays <= 0) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Leave cannot be captured because no working days have been selected.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;
            }

            FileContainer.getInstance().getWebProcesses().add(leaveRequest);
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(leaveRequest);

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A leave request has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * Generates a leave cancellation change request and forwards the result to
     * a new URL
     */
    private void processLeaveCancellationRequest(HttpServletRequest request, HttpServletResponse response) {
        boolean didProcessRequests = false;
        // Get File Container

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        Employee employee = loginSessionObject.getEmployee();

        // Parameters
        String historyID = request.getParameter("historyID");
        if ((historyID == null) || (historyID.trim().length() == 0)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You must select a leave entry for the cancellation.  Please try again.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;
        }

        String cancellationReason = request.getParameter("cancellationReason");
        if ((cancellationReason == null) || (cancellationReason.trim().length() == 0)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You must provide a reason for the cancellation.  Please try again.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;
        }

        // Generate instance of a WebProcess_LeaveCancellation
        WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation");
        WebProcess_LeaveCancellation leaveCancellationRequest = new WebProcess_LeaveCancellation(procDef, owner, employee, historyID, cancellationReason);

        FileContainer.getInstance().getWebProcesses().add(leaveCancellationRequest);
        didProcessRequests = true;

        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(leaveCancellationRequest.isActive());

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to cancel existing leave has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();

            }
        }
    }

    /**
     * Generates a variable value change request and forwards the result to a
     * new URL
     */
    private void processVariableValueChangeRequest(HttpServletRequest request, HttpServletResponse response) {
        boolean didProcessRequests = false;
        // Get File Container

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        Employee employee = loginSessionObject.getEmployee();

        // Parameters
        String indicatorCode = request.getParameter("indicatorcode");
        String changedValue = request.getParameter("variableValue");
        float validChangedValue;
        String comment = request.getParameter("variableComment");

        // No indicator code
        // System.out.println("indicatorCode :" + indicatorCode);
        // System.out.println("changedValue :" + changedValue);
        // System.out.println("comment :" + comment);
        if (indicatorCode == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Indicator Code associated with this request.  Please contact your payroll administrator.",
                    true, request.getSession());
            producer.setMessageInSessionObject();
            return;

        }

        // Invalid value
        try {
            validChangedValue = Float.valueOf(changedValue).floatValue();
        } catch (NumberFormatException e) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Invalid value.  Please try again.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;

        }

        // Generate instance of a WebProcess_VariableChanges
        Variable variableToChange = WebProcess_VariableChanges.getVariable(employee, indicatorCode);
        WebProcessDefinition procDef = null;
        WebProcess_VariableChanges variableChangesRequest = null;

        // INDICATOR CODE = 55001
        if (indicatorCode.compareToIgnoreCase("55001") == 0) {
            procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001");
            variableChangesRequest
                    = new WebProcess_VariableChanges_55001(procDef, owner, employee, validChangedValue, comment);
        }

        // INDICATOR CODE = 55002
        if (indicatorCode.compareToIgnoreCase("55002") == 0) {
            procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002");
            variableChangesRequest
                    = new WebProcess_VariableChanges_55002(procDef, owner, employee, validChangedValue, comment);
        }
        // INDICATOR CODE = 55003

        if (indicatorCode.compareToIgnoreCase("55003") == 0) {
            procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003");
            variableChangesRequest
                    = new WebProcess_VariableChanges_55003(procDef, owner, employee, validChangedValue, comment);
        }
        // INDICATOR CODE = 55004

        if (indicatorCode.compareToIgnoreCase("55004") == 0) {
            procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004");
            variableChangesRequest
                    = new WebProcess_VariableChanges_55004(procDef, owner, employee, validChangedValue, comment);
        }
        // INDICATOR CODE = 55005

        if (indicatorCode.compareToIgnoreCase("55005") == 0) {
            procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005");
            variableChangesRequest
                    = new WebProcess_VariableChanges_55005(procDef, owner, employee, validChangedValue, comment);
        }

        if ((procDef == null) || (variableChangesRequest == null)) {
            return;
        }

        FileContainer.getInstance().getWebProcesses().add(variableChangesRequest);
        didProcessRequests = true;

        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(variableChangesRequest.isActive());

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to add " + changedValue + " to variable:" + variableToChange.getDescription() + ", has been logged.  "
                        + "Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();

            }
        }

    }

    /**
     * Generates a contact information change request and forwards the result to
     * a new URL
     */
    private void processEmployeeContactRequest(HttpServletRequest request, HttpServletResponse response) {
        // Was all the compulsory fields filled in?
        if (request.getParameter("home_street").trim().length() == 0) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You need to provide the Street Name.  Please go back and complete this information.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;
        }

        if (request.getParameter("home_suburb").trim().length() == 0) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You need to provide the Suburb Name.  Please go back and complete this information.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;

        }

        if (request.getParameter("home_city").trim().length() == 0) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You need to provide the City Name.  Please go back and complete this information.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;

        }

        boolean didProcessRequests = false;
        // Get File Container

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        Employee employee = loginSessionObject.getEmployee();

        // Parameters
        // This is the contact instance whose properties will
        // eventually replace the actual contact information of
        // the given employee
        Employee newEmployee = new za.co.ucs.accsys.peopleware.Employee(employee.getCompany(), employee.getEmployeeID());
        Contact newContact = new Contact(newEmployee);

        // Personal
        newEmployee.setFirstName(request.getParameter("firstName"));
        newEmployee.setSecondName(request.getParameter("secondName"));
        newEmployee.setPreferredName(request.getParameter("preferredName"));
        newEmployee.setInitials(request.getParameter("initials"));
        newEmployee.setSurname(request.getParameter("surname"));
        newEmployee.setTaxNumber(request.getParameter("taxNumber"));
        // Perform simple date validation
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        // DOB
        try {
            df.parse(request.getParameter("birthDate"));
        } catch (ParseException e) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "You must enter a valid <b>Birth Date</b><br>The required format is:<b>yyyy/mm/dd</b>.", true, request.getSession());
            producer.setMessageInSessionObject();
            return;
        }

        newEmployee.setBirthDate(DatabaseObject.formatDate(request.getParameter("birthDate")));

        newEmployee.setPassportNumber(request.getParameter("passportNumber"));
        newEmployee.setCountryOfIssue(request.getParameter("CountryOfIssue"));

        //EMERGENCY
        newEmployee.setDoctor(request.getParameter("doctorName"));
        newEmployee.setPhone(request.getParameter("doctorPhone"));
        newEmployee.setKnownConditions(request.getParameter("knownConditions"));
        newEmployee.setAllergies(request.getParameter("allergies"));

        // Numbers
        newContact.setHomePostalCode(request.getParameter("postalCode"));
        newContact.setHomeStreetCode(request.getParameter("streetCode"));
        newContact.setHomeStreet1(request.getParameter("street1"));
        newContact.setHomeStreet2(request.getParameter("street2"));
        newContact.setHomeStreet3(request.getParameter("street3"));
        newContact.setPhys_HOME_UNITNO(request.getParameter("home_unitno"));
        newContact.setPhys_HOME_COMPLEX(request.getParameter("home_complex"));
        newContact.setPhys_HOME_STREETNO(request.getParameter("home_streetno"));
        newContact.setPhys_HOME_STREET(request.getParameter("home_street"));
        newContact.setPhys_HOME_SUBURB(request.getParameter("home_suburb"));
        newContact.setPhys_HOME_CITY(request.getParameter("home_city"));
        newContact.setPhys_HOME_PROVINCE(request.getParameter("home_province"));
        newContact.setPhys_HOME_COUNTRY(request.getParameter("home_country"));
        newContact.setPhys_HOME_CODE(request.getParameter("home_code"));

        newContact.setHomePostal1(request.getParameter("postal1"));
        newContact.setHomePostal2(request.getParameter("postal2"));
        newContact.setHomePostal3(request.getParameter("postal3"));
        newContact.setFaxHome(request.getParameter("faxHome"));
        newContact.setFaxWork(request.getParameter("faxWork"));
        newContact.setTelHome(request.getParameter("telHome"));
        newContact.setTelWork(request.getParameter("telWork"));
        newContact.setCellNo(request.getParameter("cell"));
        newContact.setEMail(request.getParameter("email"));

        // Race
        if (request.getParameter("lstRace") != null) {
            newEmployee.setRace(request.getParameter("lstRace"));
        }
        // Gender
        if (request.getParameter("lstGender") != null) {
            newEmployee.setGender(request.getParameter("lstGender"));
        }

        // Generate instance of a WebProcess_EmployeeContact
        WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact");
        WebProcess_EmployeeContact contactRequest = new WebProcess_EmployeeContact(procDef, owner, employee, newContact, newEmployee);

        FileContainer.getInstance().getWebProcesses().add(contactRequest);
        didProcessRequests
                = true;

        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(contactRequest.isActive());

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to change your contact information has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();

            }
        }

    }

    /**
     * Generates a family information change request and forwards the result to
     * a new URL
     */
    private void processEmployeeTrainingRequest(HttpServletRequest request, HttpServletResponse response) {
        boolean didProcessRequests = false;
        int numberOfClosedProcesses = 0;

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        Employee employee = loginSessionObject.getEmployee();

        // Parameters
        // There might be a large number of events that the person may want to attend
        for (int eventID = 0; eventID
                <= 200; eventID++) {
            // We might create more than one request in rapid succession.  As the
            // hashCode() of each Web-Process is a function of its creation date, we
            // have to make sure there is at least one millisecond between the creation date
            // of each Web Process.

            // locate the edtSurname_n parameter.  If it exists, look for the other paramemters
            // for this family member
            String reqEventID = request.getParameter("eventid_" + eventID);
            //System.out.println("Checking for eventid_" + eventID);
            if (reqEventID == null) {
                continue;
            }
            try {
                // Create Web Process
                Thread.sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(ControllerServlet.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            // Generate instance of a WebProcess_FamilyRequisition
            WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition");
            WebProcess_TrainingRequisition trainingRequisition
                    = new WebProcess_TrainingRequisition(procDef, owner, employee, reqEventID);

            FileContainer.getInstance().getWebProcesses().add(trainingRequisition);
            didProcessRequests = true;
            if (!trainingRequisition.isActive()) {
                numberOfClosedProcesses++;
            }

        }
        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(numberOfClosedProcesses > 0);

            // Notify user of request being logged
            {

                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to attend one or more training events has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();

            }
        }
    }

    /**
     * Generates a family information change request and forwards the result to
     * a new URL
     */
    private void processFamilyRequest(HttpServletRequest request, HttpServletResponse response) {
        boolean didProcessRequests = false;
        int numberOfClosedProcesses = 0;
        // Get File Container

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        Employee employee = loginSessionObject.getEmployee();

        // Parameters
        // There might be a large number of family members added/edited at any one point in time
        //   The best one can do, seeing as the web-page generates parameters with an '_n' indicating the
        //   family_id, is to step through, say 20 iterations, looking for those parameters that matches
        //   the given familyID
        for (int familyID = 1; familyID
                <= 20; familyID++) {
            // We might create more than one request in rapid succession.  As the
            // hashCode() of each Web-Process is a function of its creation date, we
            // have to make sure there is at least one millisecond between the creation date
            // of each Web Process.

//            // locate the edtSurname_n parameter.  If it exists, look for the other paramemters
//            // for this family member
//            if (request.getParameter("edtSurname_" + familyID) == null) {
//                //System.out.println("No Surname for edtSurname_" + familyID);
//                continue;
//            }
            int commitAction = -1;
            if (request.getParameter("cbOption_" + familyID) != null) {
                if (request.getParameter("cbOption_" + familyID).compareToIgnoreCase("Delete") == 0) {
                    commitAction = WebProcess_FamilyRequisition.ctDelete;
                }

                if (request.getParameter("cbOption_" + familyID).compareToIgnoreCase("Change") == 0) {
                    commitAction = WebProcess_FamilyRequisition.ctUpdate;
                }
            }

            if (request.getParameter("cbDelete_" + familyID) != null) {
                commitAction = WebProcess_FamilyRequisition.ctDelete;
            }

            if (request.getParameter("cbAdd_" + familyID) != null) {
                commitAction = WebProcess_FamilyRequisition.ctInsert;
            }

            if (request.getParameter("cbNone_" + familyID) != null) {
                commitAction = WebProcess_FamilyRequisition.ctInsert;
            }
// If not Insert/Update/Deletes were found, look for next family member with changes

            if (commitAction == -1) {
                //System.out.println("No Commit Action");
                continue;

            }

            String firstName = request.getParameter("edtFirstName_" + familyID);
            String secondName = request.getParameter("edtSecondName_" + familyID);
            String surname = request.getParameter("edtSurname_" + familyID);
            String relation = request.getParameter("cbbRelation_" + familyID);
            String gender = request.getParameter("cbbGender_" + familyID);
            String birthDate = request.getParameter("edtBirthDate_" + familyID);
            String telHome = request.getParameter("edtTelHome_" + familyID);
            String telWork = request.getParameter("edtTelWork_" + familyID);
            String telCell = request.getParameter("edtTelCell_" + familyID);
            String EMail = request.getParameter("edtEMail_" + familyID);
            String medDependent = request.getParameter("cbbMedDependent_" + familyID);
            if (medDependent == null) {
                Family famMember = new Family(employee, familyID);
                medDependent = famMember.getMedDependent();
            }

            // If this is an ADD without a Surname, use the parent's surname
            if (commitAction == WebProcess_FamilyRequisition.ctInsert) {
                if (surname.trim().length() == 0) {
                    surname = employee.getSurname();
                }
            }
            if (commitAction == WebProcess_FamilyRequisition.ctInsert) {
                if (firstName.trim().length() == 0) {
                    firstName = "_";
                }
            }

            // Create Web Process
            try {
                // Create Web Process
                Thread.sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(ControllerServlet.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            // Generate instance of a WebProcess_FamilyRequisition
            WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition");
            WebProcess_FamilyRequisition familyRequest
                    = new WebProcess_FamilyRequisition(procDef, owner, employee, new Integer(familyID),
                            firstName, secondName, surname, gender, relation, birthDate, telHome, telWork,
                            telCell, EMail, medDependent, commitAction);

            FileContainer.getInstance().getWebProcesses().add(familyRequest);
            didProcessRequests
                    = true;
            if (!familyRequest.isActive()) {
                numberOfClosedProcesses++;
            }

        }
        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(numberOfClosedProcesses > 0);

            // Notify user of request being logged
            {

                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to change family information has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();

            }
        }
    }

    /**
     * Processes the reply that was entered from the repl_proc.jsp page
     */
    private void processRequestReply(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            // Action
            String action = request.getParameter("rbApprove");
            boolean approved = action.compareToIgnoreCase("Approve") == 0;
            // Reason
            String reason = request.getParameter("txtReason");

            // Who is the user requesting accepting/rejecting this process?
            LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
            Employee owner = loginSessionObject.getUser();

            // HashCode
            String hashValue = request.getParameter("hashValue");

            // Get Process
            WebProcess process = FileContainer.getInstance().getWebProcess(hashValue);

            // Some validation
            //1. Does this process exist?
            if (process == null) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "This is not a valid Web Process.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;

            }

            //2. Is this process still active?
            if (!process.isActive()) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "This Web process is no longer active.", false, request.getSession());
                producer.setMessageInSessionObject();
                return;

            }
            //3. Can the current user approve/reject this process?

            if (!owner.equals(process.getCurrentStage().getOwner())) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        owner.toString() + " is not the current owner of this Web Process.", true, request.getSession());
                producer.setMessageInSessionObject();
                return;

            }
            // Finalise process

            if (approved) {
                process.acceptProcess(reason);
            } else {
                process.cancelProcess(reason);
            }
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes

            //fileContainer.saveProcessDetail(process.isActive());
            // Notify user of request being logged
            {

                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "Your changes have been applied to the Web Process.  Please check your e-mail for confirmation.",
                        false, request.getSession());
                producer.setMessageInSessionObject();

            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private boolean validateDBVersion(String requiredVersion) {
        return (requiredVersion.compareToIgnoreCase(getCurrentDBVersion()) <= 0);
    }

    private boolean validateJarVersion(String requiredVersion) {
        if (getCurrentJarVersion().length() == 0) {
            return false;
        }

        return (requiredVersion.compareToIgnoreCase(getCurrentJarVersion()) <= 0);
    }

    /**
     * Applies the changes made in the admin.jsp page
     */
    private void processWebAdmin(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();

            // Who is the user requesting this process?
            LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
            Employee owner = loginSessionObject.getUser();
            Employee employee = loginSessionObject.getEmployee();

            // Parameters
            Enumeration enumr = request.getParameterNames();
            // Preferences
            java.util.prefs.Preferences webPreferences = za.co.ucs.accsys.webmodule.WebModulePreferences.getPreferences();
            while (enumr.hasMoreElements()) {
                String paramName = (String) enumr.nextElement();
                String paramVal = request.getParameter(paramName);
                String key = paramName.replaceAll(" ", "_"); // we took the '_' out in the admin.jsp, so we have to put it back again
                String value = paramVal.replaceAll("'", "\""); // we took the '"' out in the admin.jsp, so we have to put it back again
                // Now we update the Preferences class
                webPreferences.put(key, value);
            }

            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                    "The Administrator changes were saved successfully.", false, request.getSession());

            // Backup the preferences
            try {
                webPreferences.exportNode(new java.io.FileOutputStream(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getBackupFileName()));
            } catch (java.util.prefs.BackingStoreException be) {
                be.printStackTrace();
            }

            producer.setMessageInSessionObject();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * Generates a request for T&A roster change
     */
    private void processEmployeeTnaRosterRequest(HttpServletRequest request, HttpServletResponse response) {
        LinkedList<String> empsToLink = (java.util.LinkedList<String>) request.getSession().getAttribute("selectedEmployeeHashCodesToLink");
        LinkedList<String> empsToUnlink = (java.util.LinkedList<String>) request.getSession().getAttribute("selectedEmployeeHashCodesToUnlink");
        boolean didProcessRequests = false;
        // Get File Container

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();
        String shiftID = request.getParameter("shiftID");
        String rosterDayString = request.getParameter("rosterDay");
        java.util.Date rosterDay = DatabaseObject.formatDate(rosterDayString);
        WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition");

        // Parameters
        // There might be a large number of roster changes at any one point in time
        //   The way we process all of them is that we get a list of employees in two 
        //   seperate session objects.
        //   We loop through the list and process each request
        for (String empHash : empsToLink) {
            // We might create more than one request in rapid succession.  As the
            // hashCode() of each Web-Process is a function of its creation date, we
            // have to make sure there is at least one millisecond between the creation date
            // of each Web Process.

            // Create Web Process
            Employee employeeToLink = FileContainer.getEmployeeFromContainer(empHash);
            try {
                // Create Web Process
                Thread.sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(ControllerServlet.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            // Generate instance of a WebProcess_FamilyRequisition
            WebProcess_TnaRoster rosterToRequest
                    = new WebProcess_TnaRoster(procDef, owner, employeeToLink, shiftID, rosterDay);

            FileContainer.getInstance().getWebProcesses().add(rosterToRequest);
            didProcessRequests = true;

        }
        // Unlink
        for (String empHash : empsToUnlink) {
            // We might create more than one request in rapid succession.  As the
            // hashCode() of each Web-Process is a function of its creation date, we
            // have to make sure there is at least one millisecond between the creation date
            // of each Web Process.

            // Create Web Process
            Employee employeeToUnlink = FileContainer.getEmployeeFromContainer(empHash);
            try {
                // Create Web Process
                Thread.sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(ControllerServlet.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            // Generate instance of a WebProcess_FamilyRequisition
            WebProcess_TnaRoster rosterFromRequest
                    = new WebProcess_TnaRoster(procDef, owner, employeeToUnlink, null, rosterDay);

            FileContainer.getInstance().getWebProcesses().add(rosterFromRequest);
            didProcessRequests = true;

        }
        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "Request(s) to change employee shift roster information has been logged.  Please check your e-mail for confirmation."
                        + "<br>You can also go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();
            }
        }
    }

    /**
     * Generates a request for T&A hours authorisation (Daily)
     */
    private void processEmployeeTnARequest(HttpServletRequest request, HttpServletResponse response) {
        boolean didProcessRequests = false;
        int numberOfClosedProcesses = 0;

        //
        // Only for a given person for a given day
        //
        Employee onlyForEmployee = null;
        String onlyForADay = null;
        /*
         * if (request.getParameter("onlyForCompID") != null) { String companyID
         * = request.getParameter("onlyForCompID"); if (companyID.compareTo("0")
         * != 0) { String employeeID = request.getParameter("onlyForEmpID");
         * onlyForEmployee = new Employee(new
         * Company(Integer.parseInt(companyID)), Integer.parseInt(employeeID));
         * onlyForADay = request.getParameter("onlyForDay"); } }
         */

        // Who is the user requesting this process?
        LoginSessionObject loginSessionObject = (LoginSessionObject) request.getSession().getAttribute("loginsessionobject");
        Employee owner = loginSessionObject.getUser();

        // All the hours are stored as request paramaters in an ecoding that looks like this:
        // 1. The requested change on hours: 
        //      String fieldValueName = "HRS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC");
        // 2. The accompanied reason: 
        //      String fieldReasonName = "RSN:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC");
        //
        // So we need to find/locate these paramaters in the request session
        TnARequestList reqList = new TnARequestList();
        Enumeration reqEnums_step1 = request.getParameterNames();

        // 
        // Start by creating objects for each request paramater
        // and adding the HRS parameter
        //
        while (reqEnums_step1.hasMoreElements()) {
            String requestParam = (String) reqEnums_step1.nextElement();
            //System.out.println("Request:" + requestParam);
            String paramVal = request.getParameter(requestParam);
            // Does this request start with "HRS:"?
            if (requestParam.startsWith("HRS:")) {
                // Display in the log - for now
                // Add it to the list
                TnARequest tnaRequest = new TnARequest(owner, requestParam);
                //
                // Should we limit it to only those days of a specific employee?
                //
                if (onlyForEmployee != null) {
                    if (onlyForEmployee.equals(tnaRequest.employee)) {
                        if (onlyForADay.compareToIgnoreCase(tnaRequest.date) == 0) {
                            tnaRequest.setNewHours(paramVal);
                            reqList.addRequestToList(tnaRequest);
                        }
                    } else {
                    }
                } else {
                    tnaRequest.setNewHours(paramVal);
                    reqList.addRequestToList(tnaRequest);
                }

            }
        }
        //
        // Now that we have the objects, we can go back and add the NEW MINUTES
        //
        Enumeration reqEnums_step2 = request.getParameterNames();
        while (reqEnums_step2.hasMoreElements()) {
            String requestParam = (String) reqEnums_step2.nextElement();
            // Does this request start with "HRS:"?
            if (requestParam.startsWith("MNS:")) {
                // Display in the log - for now
                //System.out.println("\tValue:" + request.getParameter(requestParam));
                // Add it to the list
                TnARequest tnaRequest = reqList.getRequestFor(requestParam);
                if (tnaRequest != null) {
                    tnaRequest.setNewMinutes(request.getParameter(requestParam));
                }
            }
        }
        //
        // Add the REASON
        //
        Enumeration reqEnums_step3 = request.getParameterNames();
        while (reqEnums_step3.hasMoreElements()) {
            String requestParam = (String) reqEnums_step3.nextElement();
            // Does this request start with "HRS:"?
            if (requestParam.startsWith("RSN:")) {
                // Display in the log - for now
                //System.out.println("\tReason:" + request.getParameter(requestParam));
                // Add it to the list
                TnARequest tnaRequest = reqList.getRequestFor(requestParam);
                if (tnaRequest != null) {
                    tnaRequest.Reason = request.getParameter(requestParam);
                }
            }
        }
        //
        // Remove 'unchanged' hours
        //
        LinkedList<TnARequest> removeList = new LinkedList<>();
        for (TnARequest req : reqList.requestList) {
            if ((req.oldValue.trim().compareTo("00:00") == 0) && (req.newValue.trim().compareTo("00:00") == 0)) {
                removeList.add(req);
            }
        }
        for (TnARequest req : removeList) {
            reqList.requestList.remove(req);
        }

        //
        // Now that we have the request with all its details, its time to create a web process for it.
        //
        Iterator reqItems = reqList.requestList.iterator();
        while (reqItems.hasNext()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Create Web Process
            TnARequest reqForProcess = (TnARequest) reqItems.next();

            // Generate instance of a WebProcess_DailySignoff
            //System.out.println("Creating web process:" + reqForProcess.toString());
            WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
            //System.out.println("******** T&A Request begin...");
            WebProcess_DailySignoff tnaRequisition = new WebProcess_DailySignoff(procDef, reqForProcess.user, reqForProcess.employee, reqForProcess.date, reqForProcess.variableID, reqForProcess.oldValue, reqForProcess.newValue, reqForProcess.Reason);
            //System.out.println("Created web process:" + tnaRequisition.toString());
            //System.out.println("******** T&A Request done\n");

            FileContainer.getInstance().getWebProcesses().add(tnaRequisition);
            didProcessRequests = true;
            if (!tnaRequisition.isActive()) {
                numberOfClosedProcesses++;
            }
        }
        if (didProcessRequests) {
            // If the process is still active, there is no need to update the 'closed' processes as well.
            // One would thus only update the 'live' processes
            //fileContainer.saveProcessDetail(numberOfClosedProcesses > 0);

            // Notify user of request being logged
            {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "A request to authorise one or more sets of T&A hours has been logged."
                        + "<br>You can go to the <i>Inbox</i> tab to see the current status "
                        + "of this request, and others.", false, request.getSession());
                producer.setMessageInSessionObject();
            }
        }
    }

    @SuppressWarnings("static-access")
    private String getCurrentDBVersion() {
        DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select first(VERSION_NR) from s_db_ver with (nolock) order by date_of_version desc", con);
            if (rs.next()) {
                return (rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }

// No version was found in database
        System.out.println("No DB Ver:");
        return "";
    }

    private static String getCurrentJarVersion() {
        String jv = ESSVersion.getInstance().getJarVersion();
        return jv;
    }

    private boolean validateVersion() {

        // Are we running the expected JAR file version?
        if (!validateJarVersion(minJarVer)) {
            System.out.println("************************************************");
            System.out.println("*****     Incorrect Jar Version ");
            System.out.println("*****     Expected:" + minJarVer);
            System.out.println("*****     Found   :" + getCurrentJarVersion());
            System.out.println("*****     ESS WILL NOT BE FUNCTIONAL");
            System.out.println("************************************************");
            // Output to the 'messages.txt' file too
            FileContainer fc = za.co.ucs.accsys.webmodule.FileContainer.getInstance();

            fc.writeMessageToLog("************************************************");
            fc.writeMessageToLog("*****     Incorrect Jar Version ");
            fc.writeMessageToLog("*****     Expected:" + minJarVer);
            fc.writeMessageToLog("*****     Found   :" + getCurrentJarVersion());
            fc.writeMessageToLog("*****     ESS WILL NOT BE FUNCTIONAL");
            fc.writeMessageToLog("************************************************");
            return false;
        }

// Are we running the correct database version?
        if (!validateDBVersion(minDBVer)) {
            System.out.println("************************************************");
            System.out.println("*****     Incorrect Database Version ");
            System.out.println("*****     Expected:" + minDBVer);
            System.out.println("*****     Found   :" + getCurrentDBVersion());
            System.out.println("*****     ESS WILL NOT BE FUNCTIONAL");
            System.out.println("************************************************");

            // Output to the 'messages.txt' file too
            FileContainer fc = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
            fc.writeMessageToLog("************************************************");
            fc.writeMessageToLog("*****     Incorrect Database Version ");
            fc.writeMessageToLog("*****     Expected:" + minDBVer);
            fc.writeMessageToLog("*****     Found   :" + getCurrentDBVersion());
            fc.writeMessageToLog("*****     ESS WILL NOT BE FUNCTIONAL");
            fc.writeMessageToLog("************************************************");

            return false;
        }

        return true;

    }

    /**
     * This class is used to simplify the processing of TnA Hours
     */
    class TnARequest {

        /**
         * Constructs a TnARequest instance based on the request parameter from
         * PageProducer_TnADailySignoff
         *
         * @param requestParameter
         */
        private TnARequest(Employee user, String requestParameter) {
            // 1. The requested change on hours: 
            //      String fieldValueName = "HRS:company_id, employee_id, day, variable_id, calculated_hours_and_minutes;
            this.user = user;
            //System.out.println("Tokenizing request:" + requestParameter);
            StringTokenizer token = new StringTokenizer(requestParameter.substring(4, requestParameter.length() - 4), ",");
            // Who is this request for?
            String companyid = token.nextToken();
            //System.out.println("\tcompanyid:" + companyid);
            String employeeid = token.nextToken();
            //System.out.println("\temployeeid:" + employeeid);
            this.employee = new Employee(new Company(Integer.parseInt(companyid)), Integer.parseInt(employeeid));
            //System.out.println("\tuser:" + user.toString());
            //System.out.println("\temployee:" + employee.toString());
            // Requested date
            this.date = token.nextToken();
            //System.out.println("\tdate:" + date);
            // Which variable?
            this.variableID = token.nextToken();
            //System.out.println("\tvariableID:" + variableID);
            // What was the old value? (last 5 characters of the string)
            this.oldValue = requestParameter.trim().substring(requestParameter.trim().length() - 5, requestParameter.trim().length());
            //System.out.println("\toldValue:" + oldValue);
        }

        public void setNewHours(String newHrs) {
            newHrs = "00" + newHrs;
            newHrs = newHrs.substring(newHrs.length() - 2, newHrs.length());

            this.newHrs = newHrs;
            if (this.newMins != null) {
                this.newValue = this.newHrs + ":" + this.newMins;
            }
        }

        public void setNewMinutes(String newMins) {
            newMins = newMins + "00";
            newMins = newMins.substring(0, 2);

            this.newMins = newMins;
            if (this.newHrs != null) {
                this.newValue = this.newHrs + ":" + this.newMins;
            }
        }
        public Employee employee;
        public Employee user;
        public String date;
        public String variableID;
        public String oldValue;
        private String newHrs = null;
        private String newMins = null;
        private String newValue;
        public String Reason;
        public String additionalDetail;
    }

    class TnARequestList {

        public void addRequestToList(TnARequest request) {
            requestList.add(request);
        }

        public TnARequest getRequestFor(String companyid, String employeeid, String day, String variableid) {
            for (TnARequest req : requestList) {
                if ((req.employee.getCompany().getCompanyID().toString().compareTo(companyid) == 0)
                        && (req.employee.getEmployeeID().toString().compareTo(employeeid) == 0)
                        && (req.date.compareTo(day) == 0)
                        && (req.variableID.compareTo(variableid) == 0)) {
                    return req;
                }
            }
            return null;
        }

        public void removeUnchangedItems() {
            for (TnARequest request : requestList) {
                if (request.oldValue.trim().compareToIgnoreCase(request.newValue.trim()) == 0) {
                    requestList.remove(request);
                }
            }
        }

        public TnARequest getRequestFor(String requestParameter) {
            // 1. The requested change on hours: 
            //      String fieldValueName = "HRS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC");
            //System.out.println("Tokenizer:"+requestParameter);
            StringTokenizer token = new StringTokenizer(requestParameter.substring(4, requestParameter.length() - 4), ",");
            // Who is this request for?
            String companyid = token.nextToken();
            String employeeid = token.nextToken();
            // Requested date
            String date = token.nextToken();
            // Which variable?
            String variableID = token.nextToken();
            // What was the old value?
            String oldValue = token.nextToken();

            return getRequestFor(companyid, employeeid, date, variableID);
        }
        private LinkedList<ControllerServlet.TnARequest> requestList = new LinkedList<>();
    }
    // Maximum fiel upload size
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 1024; // 1MB.
    // Required Database Version in order to run Module
    private static final String minDBVer = "11.3.03.03";
    // Required JAR version in order to run Module
    private static final String minJarVer = "11.30.09";
}
