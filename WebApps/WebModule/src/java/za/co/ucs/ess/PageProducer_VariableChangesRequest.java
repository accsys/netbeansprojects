package za.co.ucs.ess;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.Variable;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcess_VariableChanges;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_VariableChangesRequest inherits from PageProducer and will
 * be used to request the posting of variable values Variables with a given
 * indicator code, can be logged and forwarded through the reporting structure
 * up to the point where it gets logged inside the database. </p>
 */
public class PageProducer_VariableChangesRequest extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_VariableChangesRequest(Employee employee, String processClassName, String indicatorCode, String selectionID) {
        this.employee = employee;
        this.processClassName = processClassName;
        this.indicatorCode = indicatorCode;
        this.selectionID = selectionID;
    }

    /**
     * A VariableChangesRequest Page Producer
     */
    @Override
    public String generateInformationContent() {
        try {
            StringBuilder resultPage = new StringBuilder();
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = baseURL + "Controller.jsp";
            resultPage.append("<FORM name=\"req_variablechange\" METHOD=POST action=\"").append(link).append("\"\">");
            resultPage.append("<center><div class=\"outer-grouping-no-style\">");

            // ********************************
            // Display variable that can be logged
            // ********************************
            DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();


            // Value and Reason
            {
                Variable requestedVariable = WebProcess_VariableChanges.getVariable(employee, indicatorCode);
                resultPage.append("<table><tr style=\"text-align: center;\"><td style=\"vertical-align: top;\">");
                resultPage.append("<div class=\"grouping\" style=\"display: inline-block;\">");
                resultPage.append("<h3 class=\"shaddow\">Value for <i>").append(requestedVariable.getDescription()).append("</i></h3>");
                resultPage.append("<input type=\"text\" maxlength=250 name=\"variableValue\">");
                resultPage.append("</div>");
                resultPage.append("</td><td>");
                resultPage.append("<div class=\"grouping\" style=\"display: inline-block;\">");
                resultPage.append("<h3 class=\"shaddow\">Comment</h3>");
                resultPage.append("<textarea maxlength=500 name=\"variableComment\" rows=\"2\" cols=\"30\"></textarea>");
                resultPage.append("</div>");
                resultPage.append("</td></tr></table>");
//                resultPage.append("<table class=\"center\" >");
//                resultPage.append("<tr><td align=\"center\">Value for <i>").append(requestedVariable.getDescription()).append("</i></td>");
//                resultPage.append("    <td align=\"center\">Comment</td></tr>");
//                resultPage.append("<tr><td align=\"center\"><input type=\"text\" name=\"variableValue\"></td>");
//                resultPage.append("    <td align=\"center\"><textarea name=\"variableComment\" rows=\"2\" cols=\"30\">");
//                resultPage.append("</textarea></td></tr>");
//                resultPage.append("</table>");
            }

            // SUBMIT
            {
                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<hr class=\"center\" style=\"margin-left: 15px; margin-right: 15px;\">");
                resultPage.append("<table class=\"center\" >");
                resultPage.append("<tr>");
                resultPage.append("<td align=\"right\"><input class=\"gradient-button\" style=\"margin-right: 0px;\" type=\"submit\" value=\"Submit Variable Update Request\"></td>");
                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_variablechanges\">");
                resultPage.append("<input type=\"hidden\" name=\"indicatorcode\" value=\"").append(this.indicatorCode).append("\">");
                resultPage.append("<input type=\"hidden\" name=\"selSelection\" value=\"").append(selectionID).append("\">");
                resultPage.append("</tr>");
                resultPage.append("</table>");
            }
            resultPage.append("</div></center></form>");

            if (getOtherActiveRequests().size() > 0) {
                resultPage.append("<!-- Horizontal Ruler -->");


                resultPage.append("<CENTER>");
                resultPage.append("<h3 class=\"shaddow\">Variable History - Active requests</h3>");
                resultPage.append("<div class=\"grouping\" style=\"width: 405px;\"><table id=\"displaytable\">");
//                resultPage.append("<font size=\"-2\">Other requests on this variable that are still active</font>");
                /*
                 * Note: I realize I could have used the same method inplemented
                 * in WebProcess_VariableChanges, but at this stage, we're not
                 * working with an actual instance of a WebProcess, yet. So,
                 * unless I change large numbers of methods in the
                 * WebProcess_VariableChanges class to static, (which I'm
                 * affraid poses other security issues), I'm compelled to
                 * duplicate some of the code
                 */
                //resultPage.append("<table class=\"center_narrow\" >");
                resultPage.append("<tr><th>Requested</th><th>Value</th><th>Comment</th><th>Current Owner</th></tr>");
                LinkedList requests = getOtherActiveRequests();
                for (int i = 0; i < requests.size(); i++) {
                    WebProcess_VariableChanges webRequest = (WebProcess_VariableChanges) requests.get(i);
                    resultPage.append("<tr><td>").append(DatabaseObject.formatDate(webRequest.getCreationDate())).append("</td>");
                    resultPage.append("    <td>").append(webRequest.getValue()).append("</td>");
                    resultPage.append("    <td>").append(webRequest.getComment()).append("</td>");
                    resultPage.append("    <td>").append(webRequest.getCurrentStage().getOwner()).append("</td></tr>");
                }
                resultPage.append("</table></div>");

            }
            return resultPage.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }


    }

    /**
     * Returns a LinkedList of WebProcesses currently in the system, that are
     * also requesting updates for the same employee, for the same variable.
     */
    private LinkedList getOtherActiveRequests() {
        // Processes initiated by this employee
        TreeMap allRequests = FileContainer.getInstance().getWebProcessesInitiatedBy(this.employee, true);
        LinkedList requestsForThisVariable = new LinkedList();

        Iterator iter = allRequests.values().iterator();
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();
            // Is this also a WebProcess_VariableChanges?
            if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase(processClassName) == 0) {
                requestsForThisVariable.add(process);
            }
        }

        return requestsForThisVariable;
    }
    private Employee employee;
    private String processClassName;
    private String indicatorCode;
    private String selectionID;
} // end LeavePageProducer

