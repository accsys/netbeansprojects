package za.co.ucs.ess;

import java.util.LinkedList;
import za.co.ucs.accsys.peopleware.BankAccount;
import za.co.ucs.accsys.peopleware.Contact;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.Family;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_PersonalInfo inherits from PageProducer and will display all the
 * personal information of a given employee in HTML </p>
 */
public class PageProducer_PersonalInfo extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_PersonalInfo(Employee employee) {
        this.employee = employee;
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {
        StringBuilder resultPage = new StringBuilder();
        Contact contact = employee.getContact();
        // 1st : Personal Detail

        resultPage.append("<table col=0><tr><td>");

        resultPage.append("<div class=\"item\">");
        //resultPage.append("<h3 class=\"ui-widget-header ui-corner-all\">Personal Details</h3>");
        resultPage.append("<h2 class=\"title\">Personal Details</h2>");
        resultPage.append("<div class=\"grouping\">");

        resultPage.append("<table class=\"center\" >");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Firstname:</b></td><td>").append(employee.getFirstName()).append("</td>");
        resultPage.append("      <td><b>Second Name:</b></td><td>").append(employee.getSecondName()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Surname:</b></td><td>").append(employee.getSurname()).append("</td>");
        resultPage.append("      <td><b>Preferred Name:</b></td><td>").append(employee.getPreferredName()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Initials:</b></td><td>").append(employee.getInitials()).append("</td>");
        resultPage.append("      <td><b>Date Of Birth:</b></td><td>").append(DatabaseObject.formatDate(employee.getBirthDate())).append("</td>");
        resultPage.append("      <td></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Gender:</b></td><td>").append(employee.getGender()).append("</td>");
        resultPage.append("      <td><b>Race:</b></td><td>").append(employee.getRace()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Tax Number:</b></td><td>").append(employee.getTaxNumber()).append("</td>");
        resultPage.append("      <td><b>Group Join Date:</b></td><td>").append(DatabaseObject.formatDate(employee.getGroupJoinDate())).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Last Join Date:</b></td><td>").append(DatabaseObject.formatDate(employee.getLastDOE())).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>ID Number:</b></td><td>").append(employee.getIDNumber()).append("</td>");
        resultPage.append("      <td><b>Passport Number:</b></td><td>").append(employee.getPassportNumber()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Country of Issue:</b></td><td>").append(employee.getCountryOfIssue()).append("</td><td></td>");
        resultPage.append("  </tr>");
        resultPage.append("  </table></div></div>");

        // 2nd : Phone Numbers
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Numbers</h2>");
        resultPage.append("<div class=\"grouping\">");
        resultPage.append(" <table class=\"center\" >");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Tel Home:</b></td><td>").append(contact.getTelHome()).append("</td>");
        resultPage.append("      <td><b>Tel Office:</b></td><td>").append(contact.getTelWork()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>E-Mail:</b></td><td>").append(contact.getEMail()).append("</td>");
        resultPage.append("      <td><b>Fax Home:</b></td><td>").append(contact.getFaxHome()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Fax Office:</b></td><td>").append(contact.getFaxWork()).append("</td>");
        resultPage.append("      <td><b>Cell:</b></td><td>").append(contact.getCellNo()).append("</td><td></td>");
        resultPage.append("  </tr>");
        resultPage.append("  </table></div></div>");

        //Emergency Details
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Emergency Details</h2>");
        resultPage.append("<div class=\"grouping\">");
        resultPage.append(" <table class=\"center\" >");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Doctor Name:</b></td><td>").append(employee.getDoctor()).append("</td>");
        resultPage.append("      <td><b>Tel Number:</b></td><td>").append(employee.getPhone()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Known Conditions:</b></td><td>").append(employee.getKnownConditions()).append("</td>");
        resultPage.append("      <td><b>Allergies:</b></td><td>").append(employee.getAllergies()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  </table></div></div>");

        // 3rd Row: Street Address
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Physical Address</h2>");
        resultPage.append("<div class=\"grouping\">");
        resultPage.append(" <table class=\"center\" >");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Unit No:</b></td><td>").append(contact.getPhys_HOME_UNITNO()).append("</td>");
        resultPage.append("      <td><b>Complex:</b></td><td>").append(contact.getPhys_HOME_COMPLEX()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Street No:</b></td><td>").append(contact.getPhys_HOME_STREETNO()).append("</td>");
        resultPage.append("      <td><b>Street:</b></td><td>").append(contact.getPhys_HOME_STREET()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Suburb:</b></td><td>").append(contact.getPhys_HOME_SUBURB()).append("</td>");
        resultPage.append("      <td><b>City:</b></td><td>").append(contact.getPhys_HOME_CITY()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Province:</b></td><td>").append(contact.getPhys_HOME_PROVINCE()).append("</td>");
        resultPage.append("      <td><b>Country:</b></td><td>").append(contact.getPhys_HOME_COUNTRY()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>Country:</b></td><td>").append(contact.getPhys_HOME_COUNTRY()).append("</td>");
        resultPage.append("      <td><b>Code:</b></td><td>").append(contact.getPhys_HOME_CODE()).append("</td><td></td>");
        resultPage.append("  </tr>");
        resultPage.append("  </table></div></div>");

        // 4th Row: Postal Address
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Postal Address</h2>");
        resultPage.append("<div class=\"grouping\">");
        resultPage.append(" <table class=\"center\" >");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b>PO Box:</b></td><td>").append(contact.getHomePostal1()).append("</td>");
        resultPage.append("      <td><b>Area:</b></td><td>").append(contact.getHomePostal2()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("      <td><b>City:</b></td><td>").append(contact.getHomePostal3()).append("</td>");
        resultPage.append("      <td><b>Code:</b></td><td>").append(contact.getHomePostalCode()).append("</td>");
        resultPage.append("  </tr>");
        resultPage.append("</table></div></div>");

        resultPage.append("</td></tr></table>");

        // 5th Row: Banking Details
        LinkedList bankAccounts = this.employee.getBankAccounts();
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Banking Details</h2>");

        // Overview Table
        String[] aHeader = {"Account Name", "Type", "Bank", "Branch", "Bank Code", "Branch Code", "Account Number"};
        String[][] aContent = new String[7][bankAccounts.size()];
        // construct the contents matrix
        for (int row = 0; row < bankAccounts.size(); row++) {
            BankAccount account = (BankAccount) bankAccounts.get(row);
            // Detail
            aContent[0][row] = account.getAccountName();
            aContent[1][row] = account.getAccountType();
            aContent[2][row] = account.getBank();
            aContent[3][row] = account.getBranch();
            aContent[4][row] = account.getBankCode();
            aContent[5][row] = account.getBranchCode();
            aContent[6][row] = account.getAccountNumber();
        }
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(aHeader, aContent));
        resultPage.append("</div>");

        // 6th Row: Family
        LinkedList familyMembers = this.employee.getFamilyMembers();
        resultPage.append("<div class=\"item\">");
        resultPage.append("<h2 class=\"title\">Family Members</h2>");
        // Overview Table
        String[] fHeader = {"Relation", "Name", "Surname", "Birth Date", "Tel Home", "Tel Work", "Cell", "E-mail", "Medical Aid Dependent"};
        String[][] fContent = new String[9][familyMembers.size()];
        // construct the contents matrix
        for (int row = 0; row < familyMembers.size(); row++) {
            Family family = (Family) familyMembers.get(row);
            // Detail
            fContent[0][row] = family.getRelation();
            fContent[1][row] = family.getFirstName();
            fContent[2][row] = family.getSurname();
            fContent[3][row] = family.getBirthDate();
            fContent[4][row] = family.getTelHome();
            fContent[5][row] = family.getTelWork();
            fContent[6][row] = family.getTelCell();
            fContent[7][row] = family.getEMail();
            if (family.isMedDependent()) {
                fContent[8][row] = "Y";
            } else {
                fContent[8][row] = "";
            }
        }
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(fHeader, fContent));
        resultPage.append("</div>");

        return resultPage.toString();
    }
    private Employee employee;
} // end LeavePageProducer

