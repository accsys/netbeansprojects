/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.ess;

import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.prefs.BackingStoreException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.LeaveHistoryEntry;
import za.co.ucs.accsys.peopleware.PasswordManager;
import za.co.ucs.accsys.webmodule.E_Mail;
import za.co.ucs.accsys.webmodule.EmployeeSelectionRule;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.ReportingStructure;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;
import za.co.ucs.accsys.webmodule.WebProcess_VariableChanges;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 *
 * @author lterblanche
 */
public class Daemon implements ServletContextListener {

    private ScheduledExecutorService dataRefreshScheduler;
    private ScheduledExecutorService processRefreshScheduler;
    private ScheduledExecutorService ondemandRefreshScheduler;
    private ScheduledExecutorService processReminderScheduler;
    private ScheduledExecutorService mobileRequestQueueScheduler;
    private ScheduledExecutorService mobileReportingScheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {

// <editor-fold defaultstate="collapsed" desc=" Schedulin Data Refresh Timer ">
        // --------------------------
        // --- DATA REFRESH TIMER ---
        // --------------------------
        System.out.println("**********************************************************");
        System.out.println(" Initialising Daemon ");
        java.util.GregorianCalendar now = new java.util.GregorianCalendar();
        now.setTime(new java.util.Date());

        java.util.GregorianCalendar midnight = (GregorianCalendar) now.clone();

        // reset hour, minutes, seconds and millis
        midnight.set(GregorianCalendar.HOUR_OF_DAY, 0);
        midnight.set(GregorianCalendar.MINUTE, 0);
        midnight.set(GregorianCalendar.SECOND, 0);
        midnight.set(GregorianCalendar.MILLISECOND, 0);
        // next day
        midnight.add(GregorianCalendar.DAY_OF_MONTH, 1);

        System.out.println("*** now = " + DatabaseObject.formatDateTime(now.getTime()));
        System.out.println("*** midnight = " + DatabaseObject.formatDateTime(midnight.getTime()));
        long minDifference = DatabaseObject.getNumberOfMinutesBetween(now.getTime(), midnight.getTime());

        System.out.println("***     minDifference: " + minDifference + " minute(s) ***");

        // Set up a schedule to run at 23:00
        DataRefreshTimerTask dataRefreshTimerTask = new DataRefreshTimerTask();
        System.out.println("***     Scheduling DataRefreshTimerTask for every " + 24 + " hours, commencing after " + minDifference + " minutes ***");
        dataRefreshScheduler = Executors.newSingleThreadScheduledExecutor();
        dataRefreshScheduler.scheduleAtFixedRate(dataRefreshTimerTask, minDifference, 60 * 24, TimeUnit.MINUTES);
        // Now that it's sheduled to run at 23:00, let's run it once as part of our startup
        //dataRefreshTimerTask.run();

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Scheduling Process Escalation Timer ">
        // --------------------------
        // --- PROCESS TIMER ---
        // --------------------------
        ProcessTimerTask myProcessTask = new ProcessTimerTask();
        //long processInterval = (WebModulePreferences.getPreferences().getLong(WebModulePreferences.DAEMON_ProcessInterval, 20));
        long theProcessInterval = 1;
        System.out.println("***     Scheduling ProcessTimerTask for every " + theProcessInterval + " minute(s), commencing after " + theProcessInterval + " minute(s) ***");
        processRefreshScheduler = Executors.newSingleThreadScheduledExecutor();
        processRefreshScheduler.scheduleAtFixedRate(myProcessTask, theProcessInterval, theProcessInterval, TimeUnit.MINUTES);

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Scheduling On-Demand Timer ">
        // --------------------------
        // --- ON DEMAND TIMER ---
        // --------------------------
        OnDemandDataRefreshTask myOnDemandTask = new OnDemandDataRefreshTask();
        long onDemandRefreshInterval = 3;
        System.out.println("***     Scheduling OnDemandDataRefreshTask for every " + onDemandRefreshInterval + " minute(s) ***");
        ondemandRefreshScheduler = Executors.newSingleThreadScheduledExecutor();
        ondemandRefreshScheduler.scheduleAtFixedRate(myOnDemandTask, onDemandRefreshInterval, onDemandRefreshInterval, TimeUnit.MINUTES);

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Scheduling Process Reminders ">
        if (WebModulePreferences.getInstance().sendProcessReminders()) {
            // --------------------------
            // --- PROCESS REMINDER TIMER ---
            // --------------------------
            // We want to send out the reminders at 07:00 am
            // This timer will
            // a) Remind managers of any processes in their names that requires action
            // b) Notify employees if a process that is date-limited is either becoming available or about to be closed for the
            //    remainder of the month.
            // c) Notify the administrator of the amount of ESS credits remaining if its under CREDIT_WARNING_THRESHOLD

            java.util.GregorianCalendar nextMorning = new java.util.GregorianCalendar();
            // Should initiate tomorrow morning at 05:00
            nextMorning.set(GregorianCalendar.HOUR, 5);
            nextMorning.set(GregorianCalendar.MINUTE, 00);
            nextMorning.set(GregorianCalendar.SECOND, 00);
            nextMorning.add(GregorianCalendar.DAY_OF_MONTH, 1);
            System.out.println("*** now = " + DatabaseObject.formatDateTime(now.getTime()));
            System.out.println("*** nextMorning = " + DatabaseObject.formatDateTime(nextMorning.getTime()));

            minDifference = DatabaseObject.getNumberOfMinutesBetween(now.getTime(), nextMorning.getTime());

            // Set up a schedule to run at 05:00
            ProcessReminderTask myProcessReminderTask = new ProcessReminderTask();
            System.out.println("***     Scheduling ProcessReminderTask for every " + 24 + " hours, commencing in  " + minDifference + " minute(s) ***");
            processReminderScheduler = Executors.newSingleThreadScheduledExecutor();
            processReminderScheduler.scheduleAtFixedRate(myProcessReminderTask, minDifference, 60 * 24, TimeUnit.MINUTES);

        }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc=" Schedulin Mobile Request Queue Timer ">
        // --------------------------
        // --- MOBILE REQUEST QUEUE TIMER ---
        // --------------------------
        MobileRequestQueueProcessTask myMobileRequestQueueProcessTask = new MobileRequestQueueProcessTask();
        long MobileRequestQueueRefreshInterval = WebModulePreferences.getPreferences().getLong(WebModulePreferences.DAEMON_MobiRefreshInterval, 1);
        System.out.println("***     Scheduling MobileRequestQueueProcessTask for every " + MobileRequestQueueRefreshInterval + " minute(s) ***");
        mobileRequestQueueScheduler = Executors.newSingleThreadScheduledExecutor();
        mobileRequestQueueScheduler.scheduleAtFixedRate(myMobileRequestQueueProcessTask, MobileRequestQueueRefreshInterval, MobileRequestQueueRefreshInterval, TimeUnit.MINUTES);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Scheduling Mobile Reporting Task ">
        // --------------------------
        // --- MOBILE REPORTING TASK ---
        // --------------------------
        MobileReportingTask myMobileReportingTask = new MobileReportingTask();
        mobileReportingScheduler = Executors.newSingleThreadScheduledExecutor();
        mobileReportingScheduler.schedule(myMobileReportingTask, 15, TimeUnit.SECONDS);
// </editor-fold>        

        //System.out.println("**********************************************************");
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        dataRefreshScheduler.shutdownNow();
        processRefreshScheduler.shutdownNow();
        ondemandRefreshScheduler.shutdownNow();
        processReminderScheduler.shutdownNow();
        mobileRequestQueueScheduler.shutdownNow();
        mobileReportingScheduler.shutdownNow();
    }
}

class UpdateCounts implements Runnable {

    @Override
    public void run() {
        Toolkit.getDefaultToolkit().beep();
        System.out.println(new java.util.Date() + "\nRunning Runnable...");
    }
}

/**
 * This is the private DataRefreshTimerTask class that will update database info
 * into the binary files stored on the Web Server.
 */
class DataRefreshTimerTask extends TimerTask {

    public DataRefreshTimerTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {
        if (!FileContainer.isCachingFromDB()) {
            System.out.println("**********************************************************");
            System.out.println("*** Daemon: Data Refresh at " + (new java.util.Date().toString()));
            System.out.println("*** (Reloading cached employee data from the database)");

            FileContainer.loadDatabaseCache();

            // Seeing as Employee Information might have changed, now is a
            // good time to rebuild the employee lists contained in the
            // EmployeeSelection classes.
            System.out.println("*** (Reloading Employee Selections and workflow structures)");
            FileContainer.getInstance().rebuildSelectionInterpreterCache();

            // Once the employee selections are built, we can trigger the delayed start of
            // the reporting structure caching
            System.out.println("*** (Starting Subordinate Caching ...)    ***");
            // Reload database objects
            int lenEmployees = FileContainer.getEmployees().size();
            int lenWebProcessDefs = FileContainer.getInstance().getWebProcessDefinitions().size();

            for (int i = 0; i < lenEmployees; i++) {
                Employee employee = (Employee) FileContainer.getEmployees().get(i);
                //System.out.println("\t\t\tCaching Employee #"+i+"/"+lenEmployees);
                for (int j = 0; j < lenWebProcessDefs; j++) {
                    WebProcessDefinition webDef = (WebProcessDefinition) FileContainer.getInstance().getWebProcessDefinitions().get(j);
                    FileContainer.getInstance().getSubordinates(employee, webDef);
                }
            }
            System.out.println("***              ...Caching Completed.");
        }
    }
}

/**
 * This is the private ProcessTimerTask class that will check the active
 * WebProcesses at regularly.
 */
class ProcessTimerTask extends TimerTask {

    public ProcessTimerTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {

        //1. Refreshes the list of database objects (Employees, Payslips, etc)
        //2. Manage the escalation/cancelling of current Web Processes
        //System.out.println("***********************************************");
        //System.out.println("*** Daemon: Process Checking at " + (new java.util.Date().toString()));
        //System.out.println("*** (Checking if processes timed-out, etc.) ***");
        // Process web processes
        //System.out.println("(" + (new java.util.Date().toString()) + ")\tChecking Processes..." + getNbActiveProcesses(FileContainer.getInstance().getWebProcesses()));
        for (int i = 0; i < FileContainer.getInstance().getWebProcesses().size(); i++) {
            WebProcess webProcess = (WebProcess) FileContainer.getInstance().getWebProcesses().get(i);
            if (!webProcess.isActive()) {
                continue;
            }
            // Is the process still valid?
            if (!webProcess.validate().isValid()) {
                webProcess.cancelProcess("Process no longer valid");
            } else {

                // Did the ProcessStage expire?
                if (webProcess.getActiveStageAge() > webProcess.getProcessDefinition().getMaxStageAge()) {
                    webProcess.escalateProcess(WebProcess.et_TIMEOUT, "Stage Timed Out");
                } else if (webProcess.getProcessAge() > webProcess.getProcessDefinition().getMaxProcessAge()) {
                    webProcess.cancelProcess("Process Timed Out");
                }

                // Is the current owner of this process on leave?
                // CR 12188: If the manager is on leave for only half-a-day, do not escalate the process to another
                FileContainer.getInstance().resetLeaveInfoForEmployee(webProcess.getCurrentStage().getOwner());
                LeaveHistoryEntry leaveOverToday = FileContainer.getInstance().getLeaveInfoFromContainer(webProcess.getCurrentStage().getOwner()).didTakeLeaveOn(new java.util.Date());

                if (webProcess.getCurrentStage().getOwner().isEmployeeDischarged()) {
                    webProcess.escalateProcess(WebProcess.et_FORWARD,
                            webProcess.getCurrentStage().getOwner().getFirstName() + " "
                            + webProcess.getCurrentStage().getOwner().getSurname()
                            + " is discharged.");
                } else {
                    if (leaveOverToday != null) {
                        //System.out.println(webProcess.getEmployee() + " logged a process that has to be authorised by " + webProcess.getCurrentStage().getOwner());
                        //System.out.println(" ... but " + webProcess.getCurrentStage().getOwner() + " is on leave today.");
                        if (leaveOverToday.getUnitsTaken() > 0.5) {
                            // CR 13564: If the manager is due back before the original escalation period, do not escalate, but rather wait
                            //  for the manager to return and to address the request himself/
                            int maxStageAgeInHours = webProcess.getProcessDefinition().getMaxStageAge();
                            int activeStageAgeInHours = webProcess.getActiveStageAge();
                            int hoursBeforeManagerReturns = webProcess.getWorkingHoursPerDay(new java.util.Date().getTime(), leaveOverToday.getToDate().getTime());

                            // If the manager will not be back in time to approve/reject the request, or booked more leave will we escalate it to the next person
                            // CR 13653: Escalate process if manger's leave is extended
                            // Day after return date
                            GregorianCalendar plusOneDay = new GregorianCalendar();
                            plusOneDay.setTime(leaveOverToday.getToDate());
                            plusOneDay.add(Calendar.DAY_OF_WEEK, 1);
                            // Check if owner has more leave
                            LeaveHistoryEntry hasMoreLeave = FileContainer.getInstance().getLeaveInfoFromContainer(webProcess.getCurrentStage().getOwner()).didTakeLeaveOn(plusOneDay.getTime());
                            if ((activeStageAgeInHours + hoursBeforeManagerReturns) > maxStageAgeInHours || (hasMoreLeave != null)) {
                                System.out.println(webProcess.getEmployee() + " logged a process that has to be authorised by " + webProcess.getCurrentStage().getOwner());
                                System.out.println(" ... but " + webProcess.getCurrentStage().getOwner() + " is on leave today.");
                                System.out.println(" ... and will return in " + hoursBeforeManagerReturns + " working hours.");
                                System.out.println("     The process is already " + activeStageAgeInHours + " working hours old.");
                                System.out.println("     The process stage times-out in  " + maxStageAgeInHours + " working hours.");
                                System.out.println("     So we'll escalate the process to the next person...");
                                webProcess.escalateProcess(WebProcess.et_FORWARD,
                                        webProcess.getCurrentStage().getOwner().getFirstName() + " "
                                        + webProcess.getCurrentStage().getOwner().getSurname()
                                        + " is on leave and will not be back in time to approve/reject the application.");
                            }
                        }
                    }
                }
            }
        }

        // Make sure all our Web Preferences are persisted
        try {
            WebModulePreferences.getPreferences().flush();
        } catch (BackingStoreException be) {
            be.printStackTrace();
        }
        //System.out.println("***********************************************");
    }

    /**
     * Returns the number of WebProcesses that are still active.
     */
    private int getNbActiveProcesses(ArrayList webProcesses) {
        int result = 0;
        for (int i = 0; i < webProcesses.size(); i++) {
            if (((WebProcess) webProcesses.get(i)).isActive()) {
                result++;
            }
        }
        return result;
    }
}

/**
 * This is the private OnDemandDataRefreshTask class that will update database
 * info into the binary files stored on the Web Server. The difference between
 * OnDemandDataRefreshTask and DataRefreshTimerTask is that DataRefreshTimerTask
 * gets fired every X number of minutes, whereas OnDemandDataRefreshTask checks
 * every minute if someone forced a 'Refresh' through the Sybase A_SETUP table.
 * It scans A_SETUP where ITEM='ESS_RefreshNow' and DATA='Yes' As soon as this
 * returns a result, we fire the loadObjectsFromDatabase(fc) method and reset
 * the DATA field back to 'No'
 */
class OnDemandDataRefreshTask extends TimerTask {

    public OnDemandDataRefreshTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {
        //1. Refreshes the list of database objects (Employees, Payslips, etc)
        //2. Manage the escalation/cancelling of current Web Processes
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            int cnt = DatabaseObject.getInt("select count(*) as CNT from A_SETUP  with (NOLOCK) where upper(ITEM)=upper('ESS_RefreshNow') and "
                    + "upper(DATA)=upper('Yes')", con);
            if (cnt > 0) {
                // Reset the property
                DatabaseObject.executeSQL("update A_SETUP set DATA='No' where upper(ITEM)=upper('ESS_RefreshNow')", con);
                // Reload database objects
                System.out.println("************************************************************");
                System.out.println("*** Daemon: ...initiating reload from A_SETUP request... ***");
                System.out.println("************************************************************");

                loadObjectsFromDatabase(FileContainer.getInstance());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        //System.out.println("*****************************************************************");
    }

    /**
     * Loads Employees, Payslips, etc from database
     */
    private void loadObjectsFromDatabase(FileContainer fc) {

        System.out.println("**********************************************************");
        System.out.println("*** Daemon: Data Refresh at " + (new java.util.Date().toString()));
        System.out.println("*** (Reloading cached employee data from the database)");

        if (!FileContainer.isCachingFromDB()) {
            FileContainer.loadDatabaseCache();
        }

        // Seeing as Employee Information might have changed, now is a
        // good time to rebuild the employee lists contained in the
        // EmployeeSelection classes.
        System.out.println("*** (Reloading Employee Selections and workflow structures)");
        FileContainer.getInstance().rebuildSelectionInterpreterCache();

        // Once the employee selections are built, we can trigger the delayed start of
        // the reporting structure caching
        System.out.println("*** (Starting Subordinate Caching ...)    ***");
        // Reload database objects
        int lenEmployees = FileContainer.getEmployees().size();
        int lenWebProcessDefs = FileContainer.getInstance().getWebProcessDefinitions().size();

        for (int i = 0; i < lenEmployees; i++) {
            Employee employee = (Employee) FileContainer.getEmployees().get(i);
            //System.out.println("\t\t\tCaching Employee #"+i+"/"+lenEmployees);
            for (int j = 0; j < lenWebProcessDefs; j++) {
                WebProcessDefinition webDef = (WebProcessDefinition) FileContainer.getInstance().getWebProcessDefinitions().get(j);
                FileContainer.getInstance().getSubordinates(employee, webDef);
            }
        }
        System.out.println("***              ...Caching Completed.");
    }
}

/**
 * This is the private ProcessReminderTask class that will remind the
 * responsible people that processes needs to be actioned by them.
 */
class ProcessReminderTask extends TimerTask {

    public ProcessReminderTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {

        //1. Remind managers of tasks that needs to be actioned by them
        //2. Remind users of processes being activated / de-activated
        //3. Remind Administrators of Credit Balance (if below 100)
        System.out.println("*** Daemon: Process Reminders at " + (new java.util.Date().toString()));
        System.out.println("*** (Reminding managers of processes that they need to action) ***");
        // Process web processes
        remindOfActiveProcesses(FileContainer.getInstance());
        remindOfDateConstraintProcesses(FileContainer.getInstance());
        remindOfLowCreditLimit(FileContainer.getInstance());
        remindOfNearlyExpiredPasswords(FileContainer.getInstance());
    }

    /**
     * Checks the WebProcesses for expirations, etc.
     */
    private void remindOfActiveProcesses(FileContainer fc) {
        LinkedList<Employee> listOfManagers = new LinkedList<>();

        for (int i = 0; i < fc.getWebProcesses().size(); i++) {
            WebProcess webProcess = (WebProcess) fc.getWebProcesses().get(i);
            if (webProcess.isActive()) {
                Employee manager = webProcess.getCurrentStage().getOwner();
                if (!listOfManagers.contains(manager)) {
                    listOfManagers.add(manager);
                }
            }
        }
        // Now we step through the managers and send them all a SINGLE reminder
        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String linkStart = "<a href=\"" + baseURL + "processes/proc_todo.jsp\">";
        String linkEnd = "</a>";

        for (int j = 0; j < listOfManagers.size(); j++) {
            Employee manager = listOfManagers.get(j);
            //
            // Test to see if hyperlink must be included in e-mail
            //
            String message;
            if (WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.Email_Suppress_Hyperlinks, false)) {
                message = "<h4>Dear " + manager.toString() + "</h4>"
                        //updated to change wording and remove link
                        + "<br>This is a reminder that there are one or more ESS processes that require your attention."
                        //+ "<br><br>Follow this link for more information:" + link;
                        + "<br>Please log onto ESS to view these in your Inbox.";
            } else {
                message = "<h4>Dear " + manager.toString() + "</h4>"
                        //updated to change wording and remove link
                        + "<br>This is a reminder that there are one or more ESS processes that require your attention."
                        //+ "<br><br>Follow this link for more information:" + link;
                        + "<br>Please log onto ESS to view these in your " + linkStart + "Inbox" + linkEnd + ".";
            }
            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                E_Mail.getInstance().send(manager.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Reminder]", message, null);
            }
        }
    } // end checkProcesses

    /**
     * Checks the Passwords for expirations, etc.
     */
    private void remindOfNearlyExpiredPasswords(FileContainer fc) {
        if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendPasswordNotifications, false))) {
            LinkedList<Employee> listOfEmployees = fc.getEmployees();

            // Now we step through the employees and send them all a SINGLE reminder
            for (int j = 0; j < listOfEmployees.size(); j++) {
                Employee employee = listOfEmployees.get(j);
                //
                // Test to see if the password is about to expire
                //
                int daysBeforePWDExpire = PasswordManager.getInstance().getDaysBeforePwdExpire(employee);
                String message = "";

                // Almost expired
                if (daysBeforePWDExpire <= 14) {
                    message = "<h4>Dear " + employee.toString() + "</h4>"
                            + "Your ESS Password will expire in " + daysBeforePWDExpire + " day(s).  <br>Please change your password as soon as possible.";
                }

                // Already expired
                if (daysBeforePWDExpire <= 0) {
                    message = "<h4>Dear " + employee.toString() + "</h4>"
                            + "Your ESS Password has expired.  <br>Please reset your password using the link on the ESS login screen.";
                }
                // send the email
                E_Mail.getInstance().send(employee.getEMail(), "", "Password expiry Reminder", message, null);
            }
        }
    } // end Password expiry notification

    /**
     * Reminds the Administrator(s) of the remaining ESS credits when it's below
     * CREDIT_WARNING_THRESHOLD
     */
    private void remindOfLowCreditLimit(FileContainer fc) {
        int CREDIT_WARNING_THRESHOLD = 100;

        int credits = DatabaseObject.getAvailableESSCredits();
        if (credits < CREDIT_WARNING_THRESHOLD) {
            ArrayList structures = fc.getReportingStructures();
            Iterator iter = structures.iterator();
            while (iter.hasNext()) {
                ReportingStructure structure = (ReportingStructure) iter.next();
                ArrayList topList = structure.getTopNodes();
                if (topList.get(0) != null) {
                    EmployeeSelectionRule employeeSelection = (EmployeeSelectionRule) (topList.get(0));

                    HashSet employees = employeeSelection.getEmployees();
                    Iterator anotherIter = employees.iterator();
                    while (anotherIter.hasNext()) {
                        Employee employee = (Employee) anotherIter.next();
                        String message = "<h4>Dear " + employee.toString() + "</h4>"
                                + "<br>This is a reminder that the ESS is running low on process credits.  The current credit count is " + credits + "<br>";

                        if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                            E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Reminder]", message, null);
                        }
                    }
                }
            }
        }
    } // end checkProcesses    

    /**
     * Checks the WebProcesses for expirations, etc.
     */
    private void remindOfDateConstraintProcesses(FileContainer fc) {

        System.out.println("\nChecking Date Constraint Processes..." + fc.getWebProcessDefinitions().size());
        ArrayList definitions = fc.getWebProcessDefinitions();
        Iterator iter = definitions.iterator();
        while (iter.hasNext()) {
            WebProcessDefinition def = (WebProcessDefinition) iter.next();
            //
            // Is a process definition about to become active?
            //
            if (def.isNotifyOfPendingActivation()) {
                boolean isOnActivationDay = (def.getActiveFromDayOfMonth() > 0) && (def.getActiveFromDayOfMonth() == DatabaseObject.getDayOfMonth(new java.util.Date()));
                if (isOnActivationDay) {
                    // Notify all employees who may use this process
                    //
                    ArrayList employees = def.getReportingStructure().getAllEmployeesInEmployeeSelections();
                    for (int i = 0; i < employees.size(); i++) {
                        Employee employee = (Employee) employees.get(i);
                        String procDescr = getWebProcessEMailDescription(def, employee);
                        if (!procDescr.equals("")) {
                            StringBuilder message = new StringBuilder();
                            message.append("<h4>Dear ").append(employee.toString()).append("</h4>");
                            message.append("<br>This is a reminder that the following ESS process has become available for this month:<br><i>").append(procDescr).append("</i>");
                            if (def.getActiveToDayOfMonth() > 0) {
                                message.append("<br>The process will remain available until the ").append(def.getActiveToDayOfMonth()).append(getStringDaySuffix(def.getActiveToDayOfMonth())).append(" day of the month.");
                            }
                            // Send e-mail
                            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                                E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Process Activation Notification]", message.toString(), null);
                            }
                        }
                    }
                }
            }

            //
            // Is a process definition about to be deactivated?
            //
            if (def.isNotifyOfPendingDeactivation()) {
                boolean isDayBeforeDeactivationDay = (def.getActiveToDayOfMonth() > 0) && ((def.getActiveToDayOfMonth() - DatabaseObject.getDayOfMonth(new java.util.Date())) == 1);
                if (isDayBeforeDeactivationDay) {
                    // Notify all employees who may use this process
                    //
                    ArrayList employees = def.getReportingStructure().getAllEmployeesInEmployeeSelections();
                    for (int i = 0; i < employees.size(); i++) {
                        Employee employee = (Employee) employees.get(i);
                        String procDescr = getWebProcessEMailDescription(def, employee);
                        if (!procDescr.equals("")) {
                            StringBuilder message = new StringBuilder();
                            message.append("<h4>Dear ").append(employee.toString()).append("</h4>");
                            message.append("<br>This is a reminder that as of tomorrow, the following ESS process will be closed for the remainder of this month:<br><i>").append(procDescr).append("</i>");
                            if (def.getActiveFromDayOfMonth() > 0) {
                                message.append("<br>The process will re-open on the ").append(def.getActiveFromDayOfMonth()).append(getStringDaySuffix(def.getActiveFromDayOfMonth())).append(" day of next month.");
                            }
                            // Send e-mail
                            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                                E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Process Activation Notification]", message.toString(), null);
                            }
                        }
                    }
                }
            }
        }
    }

    // Assist in creating a string that reads 1st, 2nd, 3rd, 4th, etc.
    private String getStringDaySuffix(int day) {
        if (day == 1) {
            return "st";
        }
        if (day == 2) {
            return "nd";
        }
        if (day == 3) {
            return "rd";
        }
        return "th";
    }

    private String getWebProcessEMailDescription(WebProcessDefinition def, Employee employee) {
        try {
            // Variables 55001
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55001").getName();
            }
            // Variables 55002
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55002").getName();
            }
            // Variables 55003
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55001").getName();
            }
            // Variables 55004
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55004").getName();
            }
            // Variables 55005
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55005").getName();
            }

            return def.getName();
        } catch (Exception e) {
            return "";
        }
    }
}

/**
 * This is the private mobileRequestQueueProcessTask class that will process
 * mobile request info from the database into ESS processes
 */
class MobileRequestQueueProcessTask extends TimerTask {

    public MobileRequestQueueProcessTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {

        try {
            FileContainer.processMOBIRequests();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

/**
 * This is the private mobileReportingTask class that will populate
 * ESS_MOBI_REPORTING
 */
class MobileReportingTask extends TimerTask {

    public MobileReportingTask() {
    }

    /**
     * This is where the work is done.
     */
    @Override
    public void run() {

        try {
            FileContainer.persistManagementStructureToDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
