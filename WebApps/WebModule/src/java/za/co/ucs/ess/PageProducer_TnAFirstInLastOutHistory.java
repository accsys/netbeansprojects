package za.co.ucs.ess;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_TnAFirstInLastOutHistory inherits from PageProducer and will display the
 * history of In/Out clockings for a given period
 * </p>
 */
public class PageProducer_TnAFirstInLastOutHistory extends PageProducer {

    /** Constructor
     */
    public PageProducer_TnAFirstInLastOutHistory(Employee employee, String fromDate, String toDate, boolean displayDateSelection) {
        this.employee = employee;
        this.displayDateSelection = displayDateSelection;

        if (toDate == null) {
            this.toDate = DatabaseObject.getCurrentDate();
            this.fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(new java.util.Date(), -30));
        } else {
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        // Make sure that the toDate is after the fromDate
        if (DatabaseObject.formatDate(this.fromDate).after(DatabaseObject.formatDate(this.toDate))) {
            this.fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(this.toDate), -30));
        }

    }

    private String getDateSelectionHTML() {

        //
        // Date Selection
        //
        StringBuilder resultPage = new StringBuilder();
        resultPage.append("<table width='100%' align='center'><FORM name=\"tnastats_dates\" METHOD=POST action=\"tna_prev.jsp\"> ");


        //
        // From Date and To Date Picker (START)
        //
        resultPage.append("<table width='700' align='center'><tr>");
        // From Date
        //
        resultPage.append("<td width=\"100\" align=\"right\" ><h3 class=\"shaddow\">From</h3></td>");

        resultPage.append("<td  width=\"160\"><input type=\"text\" name=\"fromDate\" placeholder=\"yyyy/mm/dd\" value=\"").append(fromDate).append("\" size=10 maxlength=10");
        resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
        resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        resultPage.append(" >");

        resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar3')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");
        // Separator 
        //
        resultPage.append("<td width=\"20\"></td>");
        // To Date
        //
        resultPage.append("<td width=\"100\"  align=\"right\" ><h3 class=\"shaddow\">To</h3></td>");

        resultPage.append("<td width=\"160\" ><input type=\"text\" name=\"toDate\" placeholder=\"yyyy/mm/dd\" value=\"").append(toDate).append("\" size=10 maxlength=10");
        resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
        resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        resultPage.append(" >");

        resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date.', this, event, '130px')\" href=\"javascript:showCal('Calendar4')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a></td>");
        resultPage.append("<td><input class=\"gradient-button\" type=\"submit\" value=\"Refresh Selection\" ></td>");

        resultPage.append("</tr></table>");
        //
        // From Date and To Date Picker (END)
        //

        resultPage.append("</form></table><hr width=\"60%\" size=\"1\" color=\"#c0c0c0\"><br>");
        return resultPage.toString();
    }

    /** A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();
        if (displayDateSelection) {
            resultPage.append(getDateSelectionHTML());
        }
        // History Table - per Leave_Type
        String[] headers = new String[5];
        headers[0] = "Day";
        headers[1] = "First In";
        headers[2] = "Last Out";
        headers[3] = "Time At Work";
        headers[4] = "Calculation Summary";
        
      

//        resultPage.append("<CENTER><span class=\"standardHeading\">First-In / Last-Out for ").append(fromDate).append(" - ").append(toDate).append("</span></CENTER>");
        resultPage.append("<center><h3 class=\"shaddow\">First-In / Last-Out for ").append(fromDate).append(" - ").append(toDate).append("</h3>");
        
        //resultPage.append(PageSectionGenerator.getInstance().getHTMLInnerTableTag());

        int nbRecords = getClockingRecordCount();
        String[][] contents = new String[5][nbRecords];

// Load all the First-In / Last-Out records for the given period
        StringBuilder sqlStatement = new StringBuilder();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            sqlStatement.append("select dayname(day) as DayName, \"date\"(day) as day, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(FirstIn), FirstIn),0) as  FirstIn_Num, ");
            sqlStatement.append("right('00'||datepart(hh,FirstIn),2)||':'||right('00'||datepart(mi,FirstIn),2) as FirstIn_Displ, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(LastOut), LastOut),0) as  LastOut_Num, ");
            sqlStatement.append("right('00'||datepart(hh,LastOut),2)||':'||right('00'||datepart(mi,LastOut),2) as LastOut_Displ, ");
            sqlStatement.append("COALESCE(TimeAtWork_ActualMinutes,0) as TimeAtWork_ActualMinutes, TimeAtWork_DisplayValue ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (readuncommitted)  ");
            sqlStatement.append("where Company_id=").append(employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (readuncommitted) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=").append(employee.getEmployeeID()).append(") ");
            sqlStatement.append("and day between '").append(fromDate).append("' and '").append(toDate).append("' ");
            sqlStatement.append("order by day ");
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            int row = 0;
            while (rs.next()) {
                contents[0][row] = rs.getDate("day") + " (" + rs.getString("DayName") + ")";
                contents[1][row] = rs.getString("FirstIn_Displ");
                contents[2][row] = rs.getString("LastOut_Displ");
                contents[3][row] = rs.getString("TimeAtWork_DisplayValue");
                contents[4][row] = getCalculationBreakdown(rs.getDate("day"));
                row++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        resultPage.append("<div style=\"width: 1035px;\">");
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide",headers, contents));

        resultPage.append("</div></center>");
        return resultPage.toString();
    }

    private int getClockingRecordCount() {
        int rslt = 0;
        StringBuilder sqlStatement = new StringBuilder();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            sqlStatement.append("select count(*) ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw  ");
            sqlStatement.append("where Company_id=").append(employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=").append(employee.getEmployeeID()).append(") ");
            sqlStatement.append("and day between '").append(fromDate).append("' and '").append(toDate).append("' ");
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            if (rs.next()) {
                rslt = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    private String getCalculationBreakdown(Date aDay) {
        StringBuilder sqlStatement = new StringBuilder();
        StringBuilder rslt = new StringBuilder();

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        boolean signedOff = false;
        try {
            sqlStatement.append("select \"Variable\" as theVariable, SignedOff_YN,  ");
            sqlStatement.append("right('00'||convert(integer,MinutesAfterSignoff/60),2)||':'||right('00'||convert(integer,remainder(MinutesAfterSignoff,60)),2) as HHMM ");
            sqlStatement.append(" from vw_TA_DailyCalculationDetail with (readuncommitted) ");
            sqlStatement.append(" where Company_id=").append(employee.getCompany().getCompanyID());
            sqlStatement.append(" and Employee_id=").append(employee.getEmployeeID());
            sqlStatement.append(" and day = '").append(aDay).append("'");
            sqlStatement.append("order by theVariable ");
            //System.out.println("Debug:" + sqlStatement.toString());
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            rslt.append("<table id=\"displaytable_inner\"><tr><td><table id=\"displaytable_inner\" >");
            while (rs.next()) {
                rslt.append("  <tr>");
                rslt.append("    <td width=\"70%\" class=\"smallFont\">").append(rs.getString("theVariable")).append("</td>");
                rslt.append("    <td width=\"30%\"  class=\"smallFont\">").append(rs.getString("HHMM")).append("</td></tr>");
                signedOff = rs.getString("SignedOff_YN").compareToIgnoreCase("Y") == 0;
            }
            rslt.append("</table></td><td>");
            if (signedOff) {
                rslt.append("<a class=\"hintanchor\" onMouseover=\"showhint('These hours have been authorised.', this, event, '150px')\">");
                rslt.append("    <img  style=\"border: 0px\" alt=\"Approved\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/note.png\"></a>");
                rslt.append("</td></tr></table>");
            } else {
                rslt.append("</td></tr></table>");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt.toString();

    }
    private Employee employee;
    private String fromDate;
    private String toDate;
    private boolean displayDateSelection;
} // end LeavePageProducer

