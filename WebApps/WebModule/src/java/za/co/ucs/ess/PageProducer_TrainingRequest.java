package za.co.ucs.ess;

import java.util.LinkedList;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.TREvent;
import za.co.ucs.accsys.peopleware.TREventList;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_TrainingRequest inherits from PageProducer and will display
 * the detail of a Training Request in HTML </p>
 */
public class PageProducer_TrainingRequest extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_TrainingRequest(Employee employee, Employee user) {
        this.employee = employee;
        this.user = user;
    }

    /**
     * A List of available Training Events
     */
    @Override
    public String generateInformationContent() {

        FileContainer fc = FileContainer.getInstance();
        StringBuilder resultPage = new StringBuilder();
        TREventList eventList = new TREventList(employee);
        LinkedList list = eventList.getList();

        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String link = baseURL + "Controller.jsp";
        resultPage.append("<center><table col=0><tr><td>");
        resultPage.append("<div class=\"outer-grouping\">");
        resultPage.append("<center><h3 class=\"shaddow\" style=\"margin-left: 5px;\">Select which training course you'd like to attend</h3></center>");
        resultPage.append("<FORM name=\"req_training\" METHOD=POST action=\"").append(link).append("\"\">");        

        // Overview Table
        String[] headers = {"", "Course", "Provider", "Venue", "From Date", "To Date", "Course Description"};
        String[][] contents = new String[7][list.size()];


        // construct the contents matrix
        for (int row = 0; row < list.size(); row++) {
            TREvent event = (TREvent) list.get(row);
            // Detail
            contents[0][row] = "<input name=\"eventid_" + row + "\" value=\"" + event.getEventID() + "\" type=\"checkbox\">";
            contents[1][row] = event.getCourseName();
            contents[2][row] = event.getProviderName();
            contents[3][row] = event.getVenue();
            contents[4][row] = DatabaseObject.formatDate(event.getFromDate());
            contents[5][row] = DatabaseObject.formatDate(event.getToDate());
            contents[6][row] = event.getCourseDescription();
        }
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));

        // SUBMIT
        {
            resultPage.append("<!-- Horizontal Ruler -->");
            resultPage.append("<input class=\"gradient-button\"  style=\"margin-right: 10px; margin-bottom: 10px;\" type=\"submit\" value=\"Submit Training Request\" ></td>");
            resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_training\">");
        }
        resultPage.append("</form>");
        resultPage.append("</td></tr></table></div></center>");

        return resultPage.toString();
    }
    private Employee employee;
    private Employee user;
} // end LeavePageProducer

