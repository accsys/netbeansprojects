package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.webmodule.*;

/**
 * <p> PageProducer_Processes_TopLevel inherits from PageProducer and will
 * display a complete list of Processes recorded in the ESS for all employees
 * </p>
 */
public class PageProducer_Processes_TopLevel extends PageProducer {

    /**
     * Constructor
     *
     * @param processDefinition The WebProcessDefinition instance for which we
     * should list the appropriate processes
     * @param lastProcessAction [sa_ACCEPT,sa_REJECT,sa_TIMEOUT,sa_CANCEL, null]
     * lists the processes according to their last process-stage status
     * @param onlyActiveProcesses Displays either all processes, or only the
     * ones still active
     */
    public PageProducer_Processes_TopLevel(WebProcessDefinition processDefinition, String lastProcessAction, boolean onlyActiveProcesses) {
        this.onlyActiveProcesses = onlyActiveProcesses;
        this.lastProcessAction = lastProcessAction;
        this.processDefinition = processDefinition;
    }

    /**
     * A PageProducer_Processes Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();

        // Overview Table
        String[] headers = {"Type", "Employee", "Detail", "Requested", "Status"};
        // Contents of Leave Processes as found in the FileContainer
        FileContainer fc = FileContainer.getInstance();
        ArrayList processesByType = fc.getWebProcesses();
        TreeMap processes = new TreeMap();
        //System.out.println("processesByType:"+processesByType.size());

        // Filter out all the processes of the type in processDefinition
        Iterator iter_proc = processesByType.iterator();
        while (iter_proc.hasNext()) {
            WebProcess process = (WebProcess) iter_proc.next();
            // System.out.println("process:"+process);
            // System.out.println("process.getProcessDefinition():"+process.getProcessDefinition());

            if (process.getProcessDefinition().toString().contains(processDefinition.toString())) {
                // List all process action types?
                if ((this.lastProcessAction == null) || (this.lastProcessAction.trim().length() == 0)) {
                    //System.out.println("lastProcessAction == null");
                    if (onlyActiveProcesses) {
                        if (process.isActive()) {
                            processes.put(new Long(Long.MAX_VALUE - (process.getCreationDate().getTime())), process);
                        }
                    } else {
                        processes.put(new Long(Long.MAX_VALUE - (process.getCreationDate().getTime())), process);
                    }
                }

                // Action = sa_ACCEPT or sa_REJECT or sa_TIMEOUT or sa_CANCEL
                if ((process.getCurrentStage() != null) && (this.lastProcessAction.compareToIgnoreCase(process.getCurrentStage().getStageAction()) == 0)) {
                    //System.out.println("LastProcessAction:"+process.getProcessHistory().getLastProcessStage().getStageAction().toString());
                    if (onlyActiveProcesses) {
                        if (process.isActive()) {
                            processes.put(new Long(Long.MAX_VALUE - (process.getCreationDate().getTime())), process);
                        }
                    } else {
                        processes.put(new Long(Long.MAX_VALUE - (process.getCreationDate().getTime())), process);
                    }
                }
            }
        }

        System.out.println("e:" + processes.size());

        String[][] contents = new String[5][processes.values().size()];

        // Create an entry in the table for each WebProcess
        Iterator iter = processes.values().iterator();
        int row = 0;
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();

            String link = "<a href=\"proc_prev.jsp?hashValue=" + process.hashCode() + "\" >";
            contents[0][row] = link + process.getProcessName() + "</a>";
            contents[1][row] = process.getEmployee().toString();
            contents[2][row] = process.toHTMLString();
            contents[3][row] = process.formatDate(process.getCreationDate());
            if (!process.isActive()) {
                contents[4][row] = "<font color=\"#ff0000\">" + process.getStatus() + "</font>";
            } else {
                contents[4][row] = process.getStatus();
            }
            row++;
        }

        // Add detail into a formatted Web Page
        resultPage.append("<h3 class=\"shaddow\">Detailed Report for ").append(processDefinition.toString()).append("</h3>");

        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));

        return resultPage.toString();
    }
    private boolean onlyActiveProcesses;
    private String lastProcessAction;
    private WebProcessDefinition processDefinition;
} // end LeavePageProducer

