package za.co.ucs.ess;

import java.io.*;

import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.DatabaseObject;
import java.sql.*;
import java.util.LinkedList;

/**
 * <p>
 * PageProducer_TaxCertificatePreview inherits from PageProducer. It is used to
 * display the tax certificates for a given submission period ("2010/08")
 * </p>
 */
public class PageProducer_TaxCertificatePreview extends PageProducer {

    /**
     * Constructor for a Tax Certificate
     *
     * @param webProcess The Process that needs previewing
     * @param payrollDetail PayrollDetail class directing us to the right period
     * @param pathAndTemplateFileName The physical path+filename on the server
     * to the TaxCertificateTemplate.html file
     */
    public PageProducer_TaxCertificatePreview(Employee employee, String submissionPeriod, String pathAndTemplateFileName) {
        this.employee = employee;
        this.submissionPeriod = submissionPeriod;
        this.pathAndTemplateFileName = pathAndTemplateFileName;
    }

    /**
     * A ProcessPreview Page Producer
     */
    @Override
    public String generateInformationContent() {
        if (employee == null) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();

        File aFile = new File(pathAndTemplateFileName);
        resultPage.append(generateReplacementStringContent(getContents(aFile)));
        return resultPage.toString();
    }

    /**
     * Replaces all the related strings in the TaxCertificate HTML document with
     * the appropriate values from the database
     *
     * @param anHTMLPage
     * @return String representation of the anHTML page with all the tax codes
     * replaced from the database
     */
    private String generateReplacementStringContent(String anHTMLPage) {
        String interimHTMLPage = anHTMLPage;

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder aStatement = new StringBuilder(constSQLString);
            aStatement.append("     vw_P_Employee_TaxCertificate_Prev.company_id=").append(employee.getCompany().getCompanyID());
            aStatement.append(" and vw_P_Employee_TaxCertificate_Prev.employee_id=").append(employee.getEmployeeID());
            aStatement.append(" and vw_P_Employee_TaxCertificate_Prev.payroll_id=").append(employee.getPayroll().getPayrollID());
            aStatement.append(" and left(\"2031\",4)||'_'||right(\"2031\",2)='").append(submissionPeriod).append("'");
            ResultSet rs = DatabaseObject.openSQL(aStatement.toString(), con);
            //
            // Step 1: Replace the 'simple' codes
            //
            String aFileID = "0";
            if (rs.next()) {
                aFileID = rs.getString("FILE_ID");
                for (int i = 0; i < replacementCodes.length / 2; i++) {
                    String replaceCode = replacementCodes[i * 2];
                    String replaceWith = rs.getString(replacementCodes[i * 2 + 1]);
                    interimHTMLPage = interimHTMLPage.replace(replaceCode, coalesce(replaceWith, ""));
                }
            }

            //
            // Step 2: Tax Directives
            //
            LinkedList directives = getListOfDirectiveNumbers(aFileID);
            String directivesList = directives.toString().replace("[", "").replace("]", "");
            interimHTMLPage = interimHTMLPage.replace("XDIRECTIVE", directivesList);

            //
            // Step 3: Get Earnings
            //
            StringBuilder earningWhereClause = new StringBuilder(" where FILE_CODE in (3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610, 3611, 3612, 3613, 3614, 3615, ");
            earningWhereClause.append("  3616, 3617, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, ");
            earningWhereClause.append("  3664, 3665, 3666, 3667, 3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710, 3711, ");
            earningWhereClause.append("  3712, 3713, 3714, 3715, 3716, 3717, 3718, 3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, ");
            earningWhereClause.append("  3759, 3760, 3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3801, 3802, 3803, 3804, 3805, ");
            earningWhereClause.append("  3806, 3807, 3808, 3809, 3810, 3813, 3851, 3852, 3853, 3854, 3855, 3856, 3857, 3858, 3859, ");
            earningWhereClause.append("  3860, 3863, 3901, 3902, 3903, 3904, 3905, 3906, 3907, 3908, 3909, 3915, 3920, 3921, 3951, ");
            earningWhereClause.append("  3952, 3953, 3954, 3955, 3956, 3957) ");
            earningWhereClause.append(" and file_id=").append(aFileID);

            String[][] codeValuePairsE = getCodeValuePairs(earningWhereClause.toString());
            for (int i = 0; i < 10; i++) {
                // If we have earnings, show them, otherwise clear those replacement strings from the template
                if (i < codeValuePairsE.length) {
                    String[] codeValuePair = codeValuePairsE[i];
                    interimHTMLPage = interimHTMLPage.replace("CEAR" + (i + 1), codeValuePair[1]);
                    interimHTMLPage = interimHTMLPage.replace("EAR" + (i + 1), codeValuePair[0]);
                } else {
                    interimHTMLPage = interimHTMLPage.replace("CEAR" + (i + 1), "");
                    interimHTMLPage = interimHTMLPage.replace("EAR" + (i + 1), "");
                }
            }

            //
            // Step 4: Get Total Earnings
            //
            StringBuilder earningTotalWhereClause = new StringBuilder(" WHERE  FILE_CODE in (3696,3697,3698) ");
            earningTotalWhereClause.append(" and file_id=").append(aFileID);

            String[][] codeValuePairsTE = getCodeValuePairs(earningTotalWhereClause.toString());
            for (int i = 0; i < 3; i++) {
                // If we have earnings, show them, otherwise clear those replacement strings from the template
                if (i < codeValuePairsTE.length) {
                    String[] codeValuePair = codeValuePairsTE[i];
                    interimHTMLPage = interimHTMLPage.replace("CEARTOT" + (i + 1), codeValuePair[1]);
                    interimHTMLPage = interimHTMLPage.replace("EARTOT" + (i + 1), codeValuePair[0]);
                } else {
                    interimHTMLPage = interimHTMLPage.replace("CEARTOT" + (i + 1), "");
                    interimHTMLPage = interimHTMLPage.replace("EARTOT" + (i + 1), "");
                }
            }

            //
            // Step 5: Get Deductions
            //
            StringBuilder deductionWhereClause = new StringBuilder(" WHERE  FILE_CODE in (4497, 4001, 4002, 4003, 4005, 4006, 4007, 4018, 4024, 4026, 4030, 4474, 4475, 4476, 4477, 4478, 4479, 4480, 4481, 4482, 4483, 4484, 4485, 4486, 4487, 4489, 4490, 4491, 4492, 4493) ");
            deductionWhereClause.append(" and file_id=").append(aFileID);

            String[][] codeValuePairsD = getCodeValuePairs(deductionWhereClause.toString());
            for (int i = 0; i < 7; i++) {
                // If we have earnings, show them, otherwise clear those replacement strings from the template
                if (i < codeValuePairsD.length) {
                    String[] codeValuePair = codeValuePairsD[i];
                    if (codeValuePair[0].compareTo("4497") == 0) {
                        interimHTMLPage = interimHTMLPage.replace("CDED" + (i + 1), "<br>" + codeValuePair[1]);
                        interimHTMLPage = interimHTMLPage.replace("DED" + (i + 1), "Total Deductions / Contributions <br>" + codeValuePair[0]);
                    } else {
                        interimHTMLPage = interimHTMLPage.replace("CDED" + (i + 1), codeValuePair[1]);
                        interimHTMLPage = interimHTMLPage.replace("DED" + (i + 1), codeValuePair[0]);
                    }
                } else {
                    interimHTMLPage = interimHTMLPage.replace("CDED" + (i + 1), "");
                    interimHTMLPage = interimHTMLPage.replace("DED" + (i + 1), "");
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }



        return interimHTMLPage;

    }

    /**
     * Fetch the entire contents of a text file, and return it in a String.
     *
     * @param aFile is a file which already exists and can be read.
     */
    static public String getContents(File aFile) {
        //...checks on aFile are elided
        StringBuilder contents = new StringBuilder();

        try {
            //use buffering, reading one line at a time
            //FileReader always assumes default encoding is OK!
            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = null; //not declared within while loop
        /*
                 * readLine is a bit quirky :
                 * it returns the content of a line MINUS the newline.
                 * it returns null only for the END of the stream.
                 * it returns an empty String if two newlines appear in a row.
                 */
                while ((line = input.readLine()) != null) {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return contents.toString();
    }

    /**
     * Returns a list of FILE_CODE, "VALUE" results from
     * P_E_TaxCertificate_Employee_Record
     *
     * @param whereClause - String containing the 'where clause' of the
     * selection
     * @return
     */
    private String[][] getCodeValuePairs(String whereClause) {

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder aStatement = new StringBuilder();
            aStatement.append(" SELECT FILE_CODE, \"VALUE\" ");
            aStatement.append(" FROM   P_E_TAXCERTIFICATE_EMPLOYEE_RECORD with (nolock) ");
            aStatement.append(whereClause);
            ResultSet rs = DatabaseObject.openSQL(aStatement.toString(), con);

            String[][] result = new String[getRowCount(rs)][2];

            int i = 0;
            while (rs.next()) {
                result[i][0] = rs.getString("FILE_CODE");
                result[i][1] = rs.getString("VALUE");
                i++;
            }
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }

        // If all else fails
        return new String[0][0];
    }

    /**
     * Returns the number of rows in the ResultSet
     */
    private int getRowCount(ResultSet rs) {
        int i = 0;
        try {
            while (rs.next()) {
                i++;
            }
            rs.beforeFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    private LinkedList getListOfDirectiveNumbers(String fileID) {
        LinkedList rsltList = new LinkedList();
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder aStatement = new StringBuilder();
            aStatement.append(" select \"VALUE\" FROM   P_E_TAXCERTIFICATE_EMPLOYEE_RECORD with (nolock)");
            aStatement.append(" where file_code=3230 and file_id=").append(fileID);
            ResultSet rs = DatabaseObject.openSQL(aStatement.toString(), con);
            while (rs.next()) {
                rsltList.add(rs.getString("VALUE"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rsltList;
    }

    /**
     * Returns the first non-null value of the two parameters passed to it
     *
     * @param a
     * @param b
     * @return First Non-Null of 'a' and 'b'
     */
    private static <T> T coalesce(T a, T b) {
        return a != null ? a : b;
    }
    private Employee employee;
    private String submissionPeriod;
    private String pathAndTemplateFileName;
    // <editor-fold defaultstate="collapsed" desc="String Constants">
    /**
     * This constant array represents paired codes of [a] - The Replacement
     * String in the Template HTML file [a+1] - The field name in the SQL result
     * that should be used to replace [a] with.
     */
    private String[] replacementCodes = {/*-- TOP --*/
        "X2030", "2030", "X3025", "3025", "X2031", "2031", "X3010", "3010", "X3015", "3015",
        /*-- EMPLOYEE INFORMATION --*/
        "X3030", "3030", "X3040", "3040", "X3050", "3050", "X3070", "3070", "X3125", "3125", "X3020", "3020", "X3075", "3075", "X3080", "3080", "X3060", "3060", "X3100", "3100", "X3135", "3135", "X3136", "3136", "X3137", "3137", "X3138", "3138",
        /*-- RESIDENTIAL/POSTAL ADDRESS --*/
        "X3211", "3211", "X3212", "3212", "X3213", "3213", "X3214", "3214", "X3215", "3215", "X3215", "3216", "X3217", "3217", "X2020", "2020", "X2022", "2022", "X2024", "2024", "X3221", "3221", "X3222", "3222", "X3223", "3223", "X3229", "3229",
        /*-- BANK ACCOUNT DETAILS --*/
        "X3241", "3241", "X3242", "3242", "X3243", "3242", "X3244", "3244", "X3245", "3245",
        /*-- TAX CERTIFICATE INFO --*/
        "X2010", "2010",
        /*-- ADRESS - BUSINESS --*/
        "X3216", "3216", "X3144", "3144", "X3145", "3145", "X3146", "3146", "X3147", "3147", "X3148", "3148", "X3149", "3149", "X3150", "3150",
        /*-- TAX WITHELD --*/
        "X4101", "4101", "X4102", "4102", "X4115", "4115", "X4116", "4116", "X4141", "4141", "X4142", "4142", "X4149", "4149", "X4150", "4150",
        /*-- PAY PERIODS --*/
        "X3200", "3200", "X3210", "3210", "X3170", "3170", "X3180", "3180",};
    private String constSQLString = " SELECT "
            + "       \"P_PAYROLL\".\"PAYROLL_ID\", \"vw_P_Company_TaxCertificate\".\"2010\", \"vw_P_Company_TaxCertificate\".\"2020\", \"vw_P_Company_TaxCertificate\".\"2022\","
            + "       \"vw_P_Company_TaxCertificate\".\"2024\", \"vw_P_Company_TaxCertificate\".\"2030\", \"vw_P_Company_TaxCertificate\".\"2031\", \"vw_P_Employee_TaxCertificate_Prev\".\"3010\","
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3015\", \"vw_P_Employee_TaxCertificate_Prev\".\"3020\", \"vw_P_Employee_TaxCertificate_Prev\".\"3025\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3030\", \"vw_P_Employee_TaxCertificate_Prev\".\"3040\", \"vw_P_Employee_TaxCertificate_Prev\".\"3050\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3060\", \"vw_P_Employee_TaxCertificate_Prev\".\"3080\", \"vw_P_Employee_TaxCertificate_Prev\".\"3100\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3146\", \"vw_P_Employee_TaxCertificate_Prev\".\"3147\", \"vw_P_Employee_TaxCertificate_Prev\".\"3148\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3149\", \"vw_P_Employee_TaxCertificate_Prev\".\"3150\", \"vw_P_Employee_TaxCertificate_Prev\".\"3180\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3200\", \"vw_P_Employee_TaxCertificate_Prev\".\"3210\", \"vw_P_Employee_TaxCertificate_Prev\".\"3213\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3214\", \"vw_P_Employee_TaxCertificate_Prev\".\"3215\", \"vw_P_Employee_TaxCertificate_Prev\".\"3216\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3217\", \"vw_P_Employee_TaxCertificate_Prev\".\"3218\", \"vw_P_Employee_TaxCertificate_Prev\".\"3221\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3222\", \"vw_P_Employee_TaxCertificate_Prev\".\"3223\", \"vw_P_Employee_TaxCertificate_Prev\".\"3229\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3240\", \"vw_P_Employee_TaxCertificate_Prev\".\"3241\", \"vw_P_Employee_TaxCertificate_Prev\".\"4141\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"4142\", \"vw_P_Employee_TaxCertificate_Prev\".\"4149\", \"vw_P_Employee_TaxCertificate_Prev\".\"4150\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3070\", \"vw_P_Employee_TaxCertificate_Prev\".\"3135\", \"vw_P_Employee_TaxCertificate_Prev\".\"3136\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3137\", \"vw_P_Employee_TaxCertificate_Prev\".\"3138\", \"vw_P_Employee_TaxCertificate_Prev\".\"3075\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3125\", \"vw_P_Employee_TaxCertificate_Prev\".\"3211\", \"vw_P_Employee_TaxCertificate_Prev\".\"3212\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3245\", \"vw_P_Employee_TaxCertificate_Prev\".\"3242\", \"vw_P_Employee_TaxCertificate_Prev\".\"3243\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"3244\", \"vw_P_Employee_TaxCertificate_Prev\".\"3144\", \"vw_P_Employee_TaxCertificate_Prev\".\"3145\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"FILE_ID\", \"vw_P_Employee_TaxCertificate_Prev\".\"3170\", \"vw_P_Employee_TaxCertificate_Prev\".\"4101\", "
            + "        \"vw_P_Employee_TaxCertificate_Prev\".\"4102\", \"vw_P_Employee_TaxCertificate_Prev\".\"4115\", \"vw_P_Employee_TaxCertificate_Prev\".\"3246\", "
            + "        \"E_MASTER\".\"COMPANY_EMPLOYEE_NUMBER\", \"E_MASTER\".\"EMPLOYEE_ID\", \"E_MASTER\".\"SURNAME\", \"E_MASTER\".\"FIRSTNAME\", \"P_PAYPOINT\".\"NAME\", "
            + "        \"P_PAYPOINT\".\"PAYPOINT_ID\", \"C_MASTER\".\"NAME\", \"C_MASTER\".\"COMPANY_ID\", \"vw_P_CostCentrePath\".\"CCPath\", \"P_PAYROLL\".\"NAME\", "
            + "        \"vw_P_Company_TaxCertificate\".\"TRANSACTION_YEAR\", \"vw_P_Employee_TaxCertificate_Prev\".\"4116\", \"vw_P_Company_TaxCertificate\".\"TYPE_ID\""
            + " FROM     "
            + "((((((\"DBA\".\"C_MASTER\" \"C_MASTER\" INNER JOIN \"DBA\".\"vw_P_Company_TaxCertificate\" \"vw_P_Company_TaxCertificate\" "
            + "    ON \"C_MASTER\".\"COMPANY_ID\"=\"vw_P_Company_TaxCertificate\".\"COMPANY_ID\") INNER JOIN \"DBA\".\"vw_P_Employee_TaxCertificate_Prev\" \"vw_P_Employee_TaxCertificate_Prev\" "
            + "    ON (\"vw_P_Company_TaxCertificate\".\"RUN_ID\"=\"vw_P_Employee_TaxCertificate_Prev\".\"RUN_ID\") "
            + "    AND (\"vw_P_Company_TaxCertificate\".\"PAYROLL_ID\"=\"vw_P_Employee_TaxCertificate_Prev\".\"PAYROLL_ID\")) INNER JOIN \"DBA\".\"E_MASTER\" \"E_MASTER\" "
            + "    ON (\"vw_P_Employee_TaxCertificate_Prev\".\"COMPANY_ID\"=\"E_MASTER\".\"COMPANY_ID\") AND (\"vw_P_Employee_TaxCertificate_Prev\".\"EMPLOYEE_ID\"=\"E_MASTER\".\"EMPLOYEE_ID\")) "
            + "    INNER JOIN \"DBA\".\"P_PAYROLL\" \"P_PAYROLL\" ON \"vw_P_Employee_TaxCertificate_Prev\".\"PAYROLL_ID\"=\"P_PAYROLL\".\"PAYROLL_ID\") INNER JOIN \"DBA\".\"P_E_MASTER\" \"P_E_MASTER\" "
            + "    ON (\"E_MASTER\".\"COMPANY_ID\"=\"P_E_MASTER\".\"COMPANY_ID\") AND (\"E_MASTER\".\"EMPLOYEE_ID\"=\"P_E_MASTER\".\"EMPLOYEE_ID\")) "
            + "    LEFT OUTER JOIN \"DBA\".\"vw_P_CostCentrePath\" \"vw_P_CostCentrePath\" ON (\"E_MASTER\".\"COMPANY_ID\"=\"vw_P_CostCentrePath\".\"COMPANY_ID\") "
            + "    AND (\"E_MASTER\".\"COST_ID\"=\"vw_P_CostCentrePath\".\"COST_ID\")) LEFT OUTER JOIN \"DBA\".\"P_PAYPOINT\" \"P_PAYPOINT\" "
            + "    ON (\"P_E_MASTER\".\"PAYPOINT_ID\"=\"P_PAYPOINT\".\"PAYPOINT_ID\") AND (\"P_E_MASTER\".\"COMPANY_ID\"=\"P_PAYPOINT\".\"COMPANY_ID\")"
            + " WHERE   ";
    // </editor-fold>
} // end LeavePageProducer

