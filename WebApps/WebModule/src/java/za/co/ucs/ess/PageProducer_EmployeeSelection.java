package za.co.ucs.ess;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.EmployeeSelection;
import za.co.ucs.accsys.webmodule.ReportingStructure;
import za.co.ucs.accsys.webmodule.SelectionInterpreter;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;

/**
 * <p>
 * PageProducer_EmployeeSelection inherits from PageProducer and will display a
 * list of Employees on who's behalf this user can request changes. This is
 * obviously dependent on the type of Web Process in question. </p>
 */
public class PageProducer_EmployeeSelection extends PageProducer {

    /**
     * Constructor
     *
     * @param user
     * @param webProcessClassName
     * @param sessionObject
     * @param allowMultiEmpSelection
     * @param selectAllAction
     * @param employeeList
     * @param leaveView
     */
    public PageProducer_EmployeeSelection(Employee user, String webProcessClassName, HttpSession sessionObject, boolean allowMultiEmpSelection, String selectAllAction, LinkedList<Employee> employeeList, String leaveView) {
        // Page Detail
        this.webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);
        this.webProcessClassname = webProcessClassName;
        this.sessionObject = sessionObject;
        // Clear whatever is presently stored in the sesssion attribute "selectedEmployeeHashCodes"
        this.sessionObject.removeAttribute("selectedEmployeeHashCodes");

        this.user = user;
        this.multiEmployeeSelection = allowMultiEmpSelection;
        this.selectAllAction = selectAllAction;
        this.employeeList = employeeList;
        this.leaveView = leaveView;
        if (this.leaveView == null || !(this.leaveView.length() > 1)) {
            this.leaveView = "History";
        }

    }

    /**
     * A PageProducer_EmployeeSelection Page Producer
     *
     * @return
     */
    @Override
    public String generateInformationContent() {
        if (this.webProcessDefinition == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Web Process Definition available.", true, sessionObject);
            producer.setMessageInSessionObject();
            return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
        }

        ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
        StringBuilder resultPage = new StringBuilder();

        // Place all the employees into a sorted map.
        LinkedList<Employee> sortedEmployeeList = new LinkedList();
        TreeMap employees = new TreeMap();
        for (int i = 0; i < employeeList.size(); i++) {
            String key = null;
            key = ((Employee) employeeList.get(i)).getEmployeeNumber();
            employees.put(key, employeeList.get(i));
        }
        Iterator iter = employees.values().iterator();
        while (iter.hasNext()) {
            Employee employee = (Employee) iter.next();
            sortedEmployeeList.add(employee);
        }
        // Overview Table
        String multiEmp = "&MultiEmployee=false";
        if (this.multiEmployeeSelection) {
            multiEmp = "&MultiEmployee=true";
        }

        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String selectAllURL = "<a class=\"hintanchor\" onMouseover=\"showhint('Select All',this,event,'80px')\" href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + multiEmp + "&webProcessClassName=" + webProcessClassname + "&action=1 class=\".mediumFontNoBackground\" >";;
        String selectNoneURL = "<a class=\"hintanchor\" onMouseover=\"showhint('Select None',this,event,'80px')\" href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + multiEmp + "&webProcessClassName=" + webProcessClassname + "&action=0 class=\".mediumFontNoBackground\" >";;
        resultPage.append("<center>");
        // Get the menu header
        PageProducer_GroupSelector gen = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, Integer.toString(reportingStructure.hashCode()), "Today", null, null); // Scan the existing list of process definitions
        resultPage.append("<h3 class=\"shaddow\">").append(gen.getMenuCaption()).append(" - Select Employee(s)</h3>");

        //
        // MULTI-EMPLOYEE SELECTION
        //
        if (this.multiEmployeeSelection) {
            String link = "";
            // TnA Daily Signoff
            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                link = baseURL + "processes/tna_signoff_req.jsp";
                resultPage.append("<FORM name=\"req_tna\" METHOD=POST action=\"").append(link).append("\">");
            } else if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                link = baseURL + "processes/leave" + leaveView + "_prev.jsp";
                resultPage.append("<FORM name=\"view_leave").append(leaveView).append("\" METHOD=POST action=\"").append(link).append("\">");
            }

            // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE SELECTION">
            String[] headersMultiSelection = {"", "Employee Number", "Surname", "Firstname", "Cost Centre", "Employee Selection"};

            // No use having 'Select ALL', etc. if there is only one process
            resultPage.append("<table width=\"98%;\"><tr><td>");
            if ((sortedEmployeeList.size() > 1)) {
                // Create the table that allows for multiple APPROVE/REJECT
                resultPage.append("<table class=\"select_all_select_none\" style=\"border: none;\"><tr><td>");
                resultPage.append(selectAllURL).append("<img alt=\"Select All\" src=\"../images/selectall.gif\"></a></td><td>");
                resultPage.append(selectNoneURL).append("<img alt=\"Select All\" src=\"../images/selectnone.gif\"></a></td></tr></table></td>");
                resultPage.append("<td><table class=\"center\"  >");
                resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" value=\"Process Selected\" style=\"margin-right: 20px;\"></td>");
                // TnA Daily Signoff
                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                } else if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"view_leave").append(leaveView).append("\">");
                }

                resultPage.append("</tr></table>");
            }
            resultPage.append("    </td></tr>");
            resultPage.append("    </table><hr width=\"98%\">");

            String[][] contentsMulti = new String[6][sortedEmployeeList.size()];

            // Create an entry in the table for each Employee
            int row = 0;
            for (Employee employee : sortedEmployeeList) {
                String forEmployeeEncrypted = Long.toString(employee.longHashCode());
                // Select ALL?
                //
                if ((selectAllAction != null) && (selectAllAction.compareTo("1") == 0)) {
                    contentsMulti[0][row] = "<input checked=\"checked\" id=\"multiEmp_" + row + "\" name=\"multiEmp_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                } else {
                    contentsMulti[0][row] = "<input id=\"multiEmp_" + row + "\" name=\"multiEmp_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                }
                // TnA Daily Signoff
                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                    contentsMulti[1][row] = "<a href=\"tna_signoff_req.jsp?forEmployee=" + forEmployeeEncrypted + "&webProcessClassName=" + webProcessClassname + "\" >" + employee.getEmployeeNumber() + "</a>";
                } else if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                    contentsMulti[1][row] = "<a href=\"leave" + leaveView + "_prev.jsp?multiEmp_" + row + "=" + forEmployeeEncrypted + "\" >" + employee.getEmployeeNumber() + "</a>";
                } else {
                    contentsMulti[1][row] = employee.getEmployeeNumber();
                }
                contentsMulti[2][row] = employee.getSurname();
                contentsMulti[3][row] = employee.getFirstName();
                if (employee.getCostCentre() == null) {
                    contentsMulti[4][row] = "";
                } else {
                    contentsMulti[4][row] = employee.getCostCentre().getName();
                }
                if (reportingStructure.getEmployeeSelection(employee) == null) {
                    contentsMulti[5][row] = "";
                } else {
                    contentsMulti[5][row] = reportingStructure.getEmployeeSelection(employee).getName();
                }

                row++;
            }

            // Add detail into a formatted Web Page
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("employeeTable", headersMultiSelection, contentsMulti));

            if (sortedEmployeeList.size() > 0) {

                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<hr width=\"98%\">");
                resultPage.append("<table class=\"center\" style=\"width: 100%;\" >");
                resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" id=\"ActionButton\" value=\"Process Selected\" style=\"margin-right: 30px;\"></td>");
                // TnA Daily Signoff
                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                } else if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"view_leave").append(leaveView).append("\">");
                }
                resultPage.append("</tr></table>");
                resultPage.append("</center>");
                resultPage.append("</FORM>");
            }
//</editor-fold>
        } else {
            // <editor-fold defaultstate="collapsed" desc="SINGLE-EMPLOYEE SELECTION">
            //
            // SINGLE-EMPLOYEE SELECTION
            //
            String[] headersSingleSelection = {"Employee Number", "Surname", "Firstname", "Cost Centre", "Employee Selection"};
            // List of all employees reporting 'user' in the given WebProcess
            String[][] contentsSingle = new String[5][sortedEmployeeList.size()];

            // Create an entry in the table for each Employee
            String link = new String();
            int row = 0;

            for (Employee employee : sortedEmployeeList) {
                try {
                    //String forEmployeeEncrypted = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.HEX_ENCRYPTION_SCHEME).encrypt(new Long(employee.longHashCode()).toString());
                    String forEmployeeEncrypted = Long.toString(employee.longHashCode());
                    // We create a hyperlink back to the normal request pages, but this time, we're
                    // passing a parameter to say on who's behalf the request is logged.
                    if (webProcessDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                        link = "<a href=\"leave_req.jsp?forEmployee=" + forEmployeeEncrypted + "&webProcessClassName=" + webProcessClassname + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) {
                        link = "<a href=\"contact_req.jsp?forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) {
                        link = "<a href=\"family_req.jsp?forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) {
                        link = "<a href=\"leavecancel_req.jsp?forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) {
                        link = "<a href=\"training_req.jsp?forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // Indicator Code = 55001
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55001&forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // Indicator Code = 55002
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55002&forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // Indicator Code = 55003
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55003&forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // Indicator Code = 55004
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55004&forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // Indicator Code = 55005
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55005&forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                    // TnA Daily Signoff
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                        link = "<a href=\"tna_signoff_req.jsp?forEmployee=" + forEmployeeEncrypted + "\"  >";
                    }
                } catch (Exception ex) {
                    Logger.getLogger(PageProducer_EmployeeSelection.class.getName()).log(Level.SEVERE, null, ex);
                }

                //
                // For single-employee
                //
                contentsSingle[0][row] = link + employee.getEmployeeNumber() + "</a><br>";
                contentsSingle[1][row] = employee.getSurname();
                contentsSingle[2][row] = employee.getFirstName();
                if (employee.getCostCentre() == null) {
                    contentsSingle[3][row] = "";
                } else {
                    contentsSingle[3][row] = employee.getCostCentre().getName();
                }
                if (reportingStructure.getEmployeeSelection(employee) == null) {
                    contentsSingle[4][row] = "";
                } else {
                    contentsSingle[4][row] = reportingStructure.getEmployeeSelection(employee).getName();
                }

                row++;
            }

            // Add detail into a formatted Web Page
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("employeeTable", headersSingleSelection, contentsSingle));
            //</editor-fold>
        }
        resultPage.append("</center>");

        return resultPage.toString();
    }
    private final Employee user;
    private final WebProcessDefinition webProcessDefinition;
    private final String webProcessClassname;
    //private EmployeeSelection employeeSelection;
    private final HttpSession sessionObject;
    private boolean multiEmployeeSelection = false;
    private String selectAllAction = null;
    private LinkedList<Employee> employeeList;
    private String leaveView = null;
} // end PageProducer_EmployeeSelection

