package za.co.ucs.ess;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.EmployeeSelection;
import za.co.ucs.accsys.webmodule.SelectionInterpreter;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;

/**
 * <p> PageProducer_EmployeeSelection inherits from PageProducer and will
 * display a list of Employees on who's behalf this user can request changes.
 * This is obviously dependent on the type of Web Process in question. </p>
 */
public class PageProducer_EmployeeSelection extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_EmployeeSelection(Employee user, String webProcessClassName, EmployeeSelection employeeSelection, HttpSession sessionObject, String orderBy, String hashValue, String selectedSelectionString, boolean allowMultiEmpSelection, String selectAllAction, String selectionID) {
        // Page Detail
        this.webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);
        this.webProcessClassname = webProcessClassName;
        this.sessionObject = sessionObject;
        // Clear whatever is presently stored in the sesssion attribute "selectedEmployeeHashCodes"
        this.sessionObject.removeAttribute("selectedEmployeeHashCodes");

        this.user = user;
        this.hashValue = hashValue;
        this.selectedSelectionString = selectedSelectionString;
        this.employeeSelection = employeeSelection;
        if (orderBy == null) {
            orderBy = "EmployeeNumber";
        }
        this.orderBy = orderBy.trim();
        this.multiEmployeeSelection = allowMultiEmpSelection;
        this.selectAllAction = selectAllAction;
        this.selectionID = selectionID;
    }

    /**
     * A PageProducer_EmployeeSelection Page Producer
     */
    @Override
    public String generateInformationContent() {
        if (this.webProcessDefinition == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Web Process Definition available.", true, sessionObject);
            producer.setMessageInSessionObject();
            return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
        }

        StringBuilder resultPage = new StringBuilder();

        // Overview Table
        String multiEmp = "&MultiEmployee=false";
        if (this.multiEmployeeSelection) {
            multiEmp = "&MultiEmployee=true";
        }

        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String trailingURL = "&hashValue=" + hashValue + multiEmp + "&selSelection=" + selectedSelectionString + "&webProcessClassName=" + webProcessClassname + " >";
        String selectAllURL = "<a class=\"hintanchor\" onMouseover=\"showhint('Select All',this,event,'80px')\" href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + "&hashValue=" + hashValue + multiEmp + "&selSelection=" + selectedSelectionString + "&webProcessClassName=" + webProcessClassname + "&action=1 class=\".mediumFontNoBackground\" >";;
        String selectNoneURL = "<a class=\"hintanchor\" onMouseover=\"showhint('Select None',this,event,'80px')\" href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + "&hashValue=" + hashValue + multiEmp + "&selSelection=" + selectedSelectionString + "&webProcessClassName=" + webProcessClassname + "&action=0 class=\".mediumFontNoBackground\" >";;
        resultPage.append("<center>");
        resultPage.append("<h3 class=\"shaddow\">Select Employee(s)</h3>");

        //
        // MULTI-EMPLOYEE SELECTION
        //
        if (this.multiEmployeeSelection) {
            String link = baseURL + "processes/tna_signoff_req.jsp";
            resultPage.append("<FORM name=\"req_tna\" METHOD=POST action=\"").append(link).append("\">");

//            
            // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE SELECTION">
            //
            // MULTI-EMPLOYEE SELECTION
            //
            String headerLinkEmpNum = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + trailingURL;
            String headerSurname = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=Surname" + trailingURL;
            String headerLinkFirstname = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=Firstname" + trailingURL;
            String headerLinkCostCentre = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=CostCentre" + trailingURL;

            // Multi-employees
            String[] headersMultiSelection = {"", headerLinkEmpNum + "Employee Number</a>", headerSurname + "Surname</a>", headerLinkFirstname + "Firstname</a>", headerLinkCostCentre + "Cost Centre</a>"};

            // List of all employees reporting 'user' in the given WebProcess
            LinkedList unsortedEmployees = new LinkedList(SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection));

            // No use having 'Select ALL', etc. if there is only one process
            resultPage.append("<table width=\"800\"><tr><td>");
            if ((unsortedEmployees.size() > 1)) {
                // Create the table that allows for multiple APPROVE/REJECT
                resultPage.append("<table class=\"select_all_select_none\" style=\"border: none;\"><tr><td>");
                resultPage.append(selectAllURL).append("<img alt=\"Select All\" src=\"../images/selectall.gif\"></a></td><td>");
                resultPage.append(selectNoneURL).append("<img alt=\"Select All\" src=\"../images/selectnone.gif\"></a></td></tr></table></td>");
//                resultPage.append("<input class=\"gradient-button\" style=\"display: inline-block;\" type=\"submit\" name=\"ActionButton\" value=\"Process Selected\"  >");
//                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                resultPage.append("<td><table class=\"center\"  >");
                resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" value=\"Process Selected\"  ></td>");
                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                resultPage.append("<input type=\"hidden\" name=\"selSelection\" value=\"").append(selectionID).append("\">");
                resultPage.append("</tr></table>");
            }
            resultPage.append("    </td></tr>");
            resultPage.append("    </table><hr width=\"80%\">");


            // Place all the employees into a sorted map.
            for (int i = 0; i < unsortedEmployees.size(); i++) {
                String key = null;
                if ((this.orderBy == null) || (this.orderBy.compareToIgnoreCase("EmployeeNumber") == 0)) {
                    key = ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                if (this.orderBy.compareToIgnoreCase("Surname") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getSurname() + ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                if (this.orderBy.compareToIgnoreCase("Firstname") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getFirstName() + ((Employee) unsortedEmployees.get(i)).getSurname();
                }
                if (this.orderBy.compareToIgnoreCase("CostCentre") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getCostCentre().getName() + ((Employee) unsortedEmployees.get(i)).getSurname() + ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                employees.put(key, unsortedEmployees.get(i));
            }

            String[][] contentsMulti = new String[5][employees.values().size()];

            // Create an entry in the table for each Employee
            //
            Iterator iter = employees.values().iterator();
            int row = 0;
            while (iter.hasNext()) {
                Employee employee = (Employee) iter.next();
                String forEmployeeEncrypted = new Long(employee.longHashCode()).toString();
                // Select ALL?
                //
                if ((selectAllAction != null) && (selectAllAction.compareTo("1") == 0)) {
                    contentsMulti[0][row] = "<input checked=\"checked\"  name=\"multiEmp_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                } else {
                    contentsMulti[0][row] = "<input name=\"multiEmp_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                }
                // TnA Daily Signoff
                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                    contentsMulti[1][row] = "<a href=\"tna_signoff_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\" >" + employee.getEmployeeNumber();
                } else {
                    contentsMulti[1][row] = employee.getEmployeeNumber() + "</a><br>";
                }
                contentsMulti[2][row] = employee.getSurname();
                contentsMulti[3][row] = employee.getFirstName();
                if (employee.getCostCentre() != null) {
                    contentsMulti[4][row] = employee.getCostCentre().getName();
                }

                row++;
            }


            // Add detail into a formatted Web Page
//            String link = baseURL + "processes/tna_signoff_req.jsp";
//            resultPage.append("<FORM name=\"req_tna\" METHOD=POST action=\"").append(link).append("\">");
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide",headersMultiSelection, contentsMulti));

            if (unsortedEmployees.size() > 0) {

                if (this.webProcessClassname.compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                    resultPage.append("<!-- Horizontal Ruler -->");
                    resultPage.append("<hr class=\"center\">");
                    resultPage.append("<table class=\"center\"  >");
                    resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" value=\"Process Selected\"  ></td>");
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                    resultPage.append("</tr></table>");
                    resultPage.append("</center>");
                    resultPage.append("</FORM>");
                }
            }
//</editor-fold>
        } else {
            // <editor-fold defaultstate="collapsed" desc="SINGLE-EMPLOYEE SELECTION">
            //
            // SINGLE-EMPLOYEE SELECTION
            //
            String headerLinkEmpNum = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=EmployeeNumber" + trailingURL;
            String headerSurname = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=Surname" + trailingURL;
            String headerLinkFirstname = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=Firstname" + trailingURL;
            String headerLinkCostCentre = "<a href=" + baseURL + "processes/employeelist_prev.jsp?orderBy=CostCentre" + trailingURL;

            // Multi-employees
            String[] headersSingleSelection = {headerLinkEmpNum + "Employee Number</a>", headerSurname + "Surname</a>", headerLinkFirstname + "Firstname</a>", headerLinkCostCentre + "Cost Centre</a>"};
            // List of all employees reporting 'user' in the given WebProcess

            LinkedList unsortedEmployees = new LinkedList(SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection));

            // Place all the employees into a sorted map.
            for (int i = 0; i < unsortedEmployees.size(); i++) {
                String key = null;
                if ((this.orderBy == null) || (this.orderBy.compareToIgnoreCase("EmployeeNumber") == 0)) {
                    key = ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                if (this.orderBy.compareToIgnoreCase("Surname") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getSurname() + ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                if (this.orderBy.compareToIgnoreCase("Firstname") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getFirstName() + ((Employee) unsortedEmployees.get(i)).getSurname();
                }
                if (this.orderBy.compareToIgnoreCase("CostCentre") == 0) {
                    key = ((Employee) unsortedEmployees.get(i)).getCostCentre().getName() + ((Employee) unsortedEmployees.get(i)).getSurname() + ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
                }
                employees.put(key, unsortedEmployees.get(i));
            }


            String[][] contentsSingle = new String[4][employees.values().size()];
            String[][] contentsMulti = new String[5][employees.values().size()];

            // Create an entry in the table for each Employee
            Iterator iter = employees.values().iterator();
            int row = 0;
            String link = new String();
            while (iter.hasNext()) {
                Employee employee = (Employee) iter.next();
                try {
                    //String forEmployeeEncrypted = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.HEX_ENCRYPTION_SCHEME).encrypt(new Long(employee.longHashCode()).toString());
                    String forEmployeeEncrypted = new Long(employee.longHashCode()).toString();
                    // We create a hyperlink back to the normal request pages, but this time, we're
                    // passing a parameter to say on who's behalf the request is logged.
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                        link = "<a href=\"leave_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) {
                        link = "<a href=\"contact_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) {
                        link = "<a href=\"family_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) {
                        link = "<a href=\"leavecancel_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) {
                        link = "<a href=\"training_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // Indicator Code = 55001
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55001&forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // Indicator Code = 55002
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55002&forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // Indicator Code = 55003
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55003&forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // Indicator Code = 55004
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55004&forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // Indicator Code = 55005
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                        link = "<a href=\"variable_req.jsp?indCode=55005&forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                    // TnA Daily Signoff
                    if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                        link = "<a href=\"tna_signoff_req.jsp?forEmployee=" + forEmployeeEncrypted + "&selSelection=" + selectedSelectionString + "\"  >";
                    }
                } catch (Exception ex) {
                    Logger.getLogger(PageProducer_EmployeeSelection.class.getName()).log(Level.SEVERE, null, ex);
                }

                //
                // For single-employee
                //
                contentsSingle[0][row] = link + employee.getEmployeeNumber() + "</a><br>";
                contentsSingle[1][row] = employee.getSurname();
                contentsSingle[2][row] = employee.getFirstName();
                if (employee.getCostCentre() != null) {
                    contentsSingle[3][row] = employee.getCostCentre().getName();
                }

                row++;
            }

            // Add detail into a formatted Web Page
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide",headersSingleSelection, contentsSingle));
            //</editor-fold>
        }
        resultPage.append("</center>");

        return resultPage.toString();
    }
    private Employee user;
    private String orderBy;
    private WebProcessDefinition webProcessDefinition;
    private String webProcessClassname;
    private EmployeeSelection employeeSelection;
    private TreeMap employees = new TreeMap();
    private HttpSession sessionObject;
    private String hashValue;
    private String selectedSelectionString;
    private boolean multiEmployeeSelection = false;
    private String selectAllAction = null;
    private String selectionID;
} // end PageProducer_EmployeeSelection

