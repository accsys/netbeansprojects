package za.co.ucs.ess;

import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpSession;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_ShiftRoster inherits from PageProducer and will display a shift
 * roster overview for all employees that reports to a given person in the shift
 * roster reporting structures. The purpose of this display is to empower the
 * manager to see a complete breakdown of which employees that reports to
 * him/her are on which shift
 * </p>
 */
public class PageProducer_ShiftRoster extends PageProducer {

    /**
     * Constructor
     *
     * @param employeeList
     * @param shiftID
     * @param forwardingURL
     * @param rosterDay
     * @param referenceDate The date for which the calendar should be set up
     * @param sessionObject
     */
    public PageProducer_ShiftRoster(LinkedList<Employee> employeeList, String shiftID, java.util.Date referenceDate, java.util.Date rosterDay, String forwardingURL, HttpSession sessionObject) {

        this.employeeList = employeeList;
        this.forwardingURL = forwardingURL;
        // The date range/week to be displayed 
        this.referenceDate = referenceDate;
        if (this.referenceDate == null) {
            this.referenceDate = new java.util.Date();
        }
        // The day for which to change employee's rostered shift
        this.rosterDay = rosterDay;
        this.shift = new Shift(shiftID);
        this.webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
        this.sessionObject = sessionObject;

        LinkedList fcEmployees = za.co.ucs.accsys.webmodule.FileContainer.getEmployees();
        // 
        // Multiple Employees to LINK
        //
        // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEES TO LINK">
        if (sessionObject.getAttribute("selectedEmployeeHashCodesToLink") != null) {
            LinkedList selectedEmployeeLongHashCodesToLink = (java.util.LinkedList<String>) sessionObject.getAttribute("selectedEmployeeHashCodesToLink");

            // Step through all the employees, and thos who have this LongHashCode should be added to our internal list
            Iterator iter = selectedEmployeeLongHashCodesToLink.iterator();
            while (iter.hasNext()) {
                String hashCode = (String) iter.next();

                for (int i = 0; i < fcEmployees.size(); i++) {
                    Employee fcEmployee = (Employee) fcEmployees.get(i);
                    String longHashCodeForEmployee = Long.toString(fcEmployee.longHashCode());
                    // Move the employee if he/she was selected to be moved
                    if (longHashCodeForEmployee.compareToIgnoreCase(hashCode) == 0) {
                        employeesToLink.add(fcEmployee);
                    }
                }
            }
        }
        //</editor-fold>
        // 
        // Multiple Employees to UNLINK
        //
        // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEES TO UNLINK">
        if (sessionObject.getAttribute("selectedEmployeeHashCodesToUnlink") != null) {
            LinkedList selectedEmployeeLongHashCodesToUnlink = (java.util.LinkedList<String>) sessionObject.getAttribute("selectedEmployeeHashCodesToUnlink");

            // Step through all the employees, and thos who have this LongHashCode should be added to our internal list
            Iterator iter = selectedEmployeeLongHashCodesToUnlink.iterator();
            while (iter.hasNext()) {
                String hashCode = (String) iter.next();

                for (int i = 0; i < fcEmployees.size(); i++) {
                    Employee fcEmployee = (Employee) fcEmployees.get(i);
                    String longHashCodeForEmployee = Long.toString(fcEmployee.longHashCode());
                    if (longHashCodeForEmployee.compareToIgnoreCase(hashCode) == 0) {
                        employeesToUnlink.add(fcEmployee);
                    }
                }
            }
        }
        //</editor-fold>
    }

    /**
     * A LeaveInfo Page Producer
     *
     * @return
     */
    @Override
    public String generateInformationContent() {
        if (this.webProcessDefinition == null) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Web Process Definition available.", true, sessionObject);
            producer.setMessageInSessionObject();
            return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
        }

        //ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
        StringBuilder resultPage = new StringBuilder();

        // No use building info if there are no employees that has requested leave
        // during the same period.
        if (this.shift == null) {
            return "";
        }

        // Array that will store the last 10 weeks (names)
        java.util.Date[] daysInWeek = DatabaseObject.getDaysOfWeek(this.referenceDate);

        // Table Header
        resultPage.append("<CENTER><div class=\"outer-grouping\" style=\"display: inline-block;\"><table id=\"displayTable\" style=\"min-width: 1000px;\">");
        // 1st Row: 'Previous Week', 'Selected Week Name', 'Next Week'
        resultPage.append("<tr><td><table style=\"width: 100%;\">");

        resultPage.append("<tr>");

        java.util.Date referredDate;
        String referredURL;
        String webprocessClassName = "?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition";

        // **** PREVIOUS MONTH ****
        if (this.forwardingURL != null) { // No use creating a forwarding session if we do not know where it came from
            referredDate = DatabaseObject.removeOneWeek(this.referenceDate);
            referredURL = forwardingURL + webprocessClassName + "&referenceDate=" + DatabaseObject.formatDate(referredDate);
            if (this.shift.getShiftID() != null) {
                referredURL = referredURL + "&shiftID=" + this.shift.getShiftID();
            }
            //System.out.println("newURL:" + referredURL);
            resultPage.append("<td width=\"10%\" style=\"text-align: left;\"><a href=\"").append(referredURL).append("\"><img  style=\"border: 0px\" alt=\"Previous Month\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/mvLeft.png\"></a></td>");
        }

        // **** CURRENTLY SELECTED WEEK DISPLAYED ****
        resultPage.append("<td width=\"80%\" style=\"text-align: center;\" class=\"standardHeading_NotBold\"><b>").append(DatabaseObject.formatDateToMonthYearWeek(referenceDate)).append("</b></td>");

        // **** NEXT MONTH ****
        if (this.forwardingURL != null) {
            referredDate = DatabaseObject.addOneWeek(this.referenceDate);
            referredURL = forwardingURL + webprocessClassName + "&referenceDate=" + DatabaseObject.formatDate(referredDate);
            if (this.shift.getShiftID() != null) {
                referredURL = referredURL + "&shiftID=" + this.shift.getShiftID();
            }
            resultPage.append("<td width=\"10%\" style=\"text-align: right;\"><a href=\"").append(referredURL).append("\"><img style=\"border: 0px\" alt=\"Next Month\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/mvRight.png\"></a></td>");
        }

        resultPage.append("</tr>");
        resultPage.append("</table></td></tr>");

        // 2nd Row: Days of month
        resultPage.append("<tr><td><div class=\"grouping\" style=\"display: inline-block;\"><table style=\"min-width: 980px;\">");
        resultPage.append("  <tr><td width=\"15%\">&nbsp;</td>");
        // Build the date headers and the shift person count in seperate stringbuilders and add them up in the end
        StringBuilder dateHeaders = new StringBuilder();
        StringBuilder shiftCount = new StringBuilder();
        // Date formats
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dayFormat = new SimpleDateFormat("'('E')'");
        // Loop through the week
        for (Date dayInWeek : daysInWeek) {
            // Date header
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(dayInWeek);
            if (rosterDay != null && dateFormat.format(rosterDay).contains(dateFormat.format(dayInWeek))) {
                dateHeaders.append("<td width=\"12%\" class=\"smallFont\"><center><table style=\"color: #ca2229;\"><tr><td style=\"text-align: center;\"><b>").append(dateFormat.format(dayInWeek)).append("</b></td></tr><tr><td style=\"text-align: center;\">");
            } else {
                dateHeaders.append("<td width=\"12%\" class=\"smallFont\"><center><table><tr><td style=\"text-align: center;\"><b>").append(dateFormat.format(dayInWeek)).append("</b></td></tr><tr><td style=\"text-align: center;\">");
            }
            dateHeaders.append(dayFormat.format(dayInWeek)).append("</td></tr></table></center></td>");
            // Add the count for the day
            String shiftColor = this.shift.getColor();
            String textColor = "#CFCFCF";
            int red = new Integer(shiftColor.substring(shiftColor.indexOf("(") + 1, shiftColor.indexOf(",")));
            int green = new Integer(shiftColor.substring(shiftColor.indexOf(",") + 1, shiftColor.indexOf(",", shiftColor.indexOf(",") + 1)));
            int blue = new Integer(shiftColor.substring(shiftColor.indexOf(",", shiftColor.indexOf(",") + 1) + 1, shiftColor.indexOf(")")));
            double rgb = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
            if (rgb > 128) {
                textColor = "#111";
            }
            shiftCount.append("<td><center><a href=\"../processes/tna_roster.jsp").append(webprocessClassName);
            shiftCount.append("&shiftID=").append(this.shift.getShiftID()).append("&rosterDay=").append(dateFormat.format(dayInWeek));
            shiftCount.append("&referenceDate=").append(dateFormat.format(this.referenceDate)).append("\">");
            shiftCount.append("<div style=\"background: ").append(this.shift.getColor());
            shiftCount.append("; border-radius: 20px; width: 35px; height: 35px;\"><div id=\"shiftCount\" style=\"color: ").append(textColor).append(";\">");
            shiftCount.append(this.shift.getPersonsOnShiftForDate(dayInWeek)).append("</div></div></a></center></td>");
        }
        // Put the headers and the data together
        resultPage.append(dateHeaders);
        resultPage.append("</tr>");
        resultPage.append("<tr><td><b>Linked Employees</b></td>");
        // Add the count for the day
        resultPage.append(shiftCount);
        resultPage.append("</tr>");

        // Table Footer
        resultPage.append("</table></div></td></tr>");
        resultPage.append("</table></div></CENTER>");

        // Is there a roster day selected to process
        if (this.rosterDay == null) {

        } else {
            LinkedList<Employee> availableEmployees = new LinkedList();
            LinkedList<Employee> linkedEmployees = new LinkedList();
            // Place all the employees into a sorted map.
            LinkedList<Employee> sortedEmployeeList = new LinkedList();
            TreeMap employees = new TreeMap();
            for (int i = 0; i < employeeList.size(); i++) {
                String key = null;
                key = ((Employee) employeeList.get(i)).getEmployeeNumber();
                employees.put(key, employeeList.get(i));
            }
            Iterator iter = employees.values().iterator();
            while (iter.hasNext()) {
                Employee employee = (Employee) iter.next();
                sortedEmployeeList.add(employee);
            }

            // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE SELECTION">
            //
            // MULTI-EMPLOYEES AVAILABLE
            //
            String[] headersMultiSelection = {"", "Employee Number", "Surname", "Firstname", "Shift"};

            //
            // Create an entry in the table for each Employee
            //
            for (Employee employee : sortedEmployeeList) {
                ShiftRosterInfo sri = new ShiftRosterInfo(employee);
                Shift empShiftOnThisDay = sri.getEmployeeShiftThisDay(this.rosterDay);
                // Move the employee is he/she has an existing process
                WebProcess_TnaRoster aProc = getAlreadyLoggedProcess(employee, rosterDay);
                if (aProc != null) {
                    // In the process of being unlinked
                    if (aProc.getNewShift() == null || aProc.getNewShift().getName() == null) {
                        availableEmployees.add(employee);
                        continue;
                    }
                    // In the process of being linked
                    if (aProc.getNewShift().getName().equals(this.shift.getName())) {
                        linkedEmployees.add(employee);
                        continue;
                    }
                    if (!(aProc.getNewShift().getName().equals(this.shift.getName()))) {
                        availableEmployees.add(employee);
                        continue;
                    }

                }
                // What is selected
                if (employeesToLink.contains(employee)) {
                    linkedEmployees.add(employee);
                    continue;
                }
                if (employeesToUnlink.contains(employee)) {
                    availableEmployees.add(employee);
                    continue;
                }
                // Sort employees into Linked and Available employee lists 
                if (empShiftOnThisDay.getName() != null && empShiftOnThisDay.getName().contains(this.shift.getName())) {
                    linkedEmployees.add(employee);
                } else {
                    availableEmployees.add(employee);
                }
            }

            resultPage.append("<CENTER><div class=\"outer-grouping\" style=\"display: inline-block;\"><table id=\"displayTable\" style=\"min-width: 1100px; margin: 10px;\">");
            resultPage.append("<tr><td>");
            // Unlinked employees heading
            resultPage.append("<center><span class=\"standardHeading_NotBold\"><b>Available Employees (").append(availableEmployees.size()).append(")</b></span>");
            // Linked employees heading
            resultPage.append("</center></td><td><center><span class=\"standardHeading_NotBold\"><b>Linked Employees (").append(linkedEmployees.size()).append(")</b></span></center></td></tr><tr><td>");
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = baseURL + "processes/tna_roster.jsp" + webprocessClassName + "&rosterDay=" + DatabaseObject.formatDate(this.rosterDay);

            ////////////////////////////////////////////////////////////////////
            // Add all the employees 
            ////////////////////////////////////////////////////////////////////
            resultPage.append("<FORM name=\"req_tna_roster_available\" METHOD=POST action=\"").append(link).append("\">");

            String[][] contentsMultiAvailable = new String[5][availableEmployees.size()];
            String[][] contentsMultiAvailableStyles = new String[5][availableEmployees.size()];
            String[][] contentsMultiLinked = new String[5][linkedEmployees.size()];
            String[][] contentsMultiLinkedStyles = new String[5][linkedEmployees.size()];
            // Reset the iterator for available employees
            int row = 0;
            FileContainer fc = FileContainer.getInstance();
            for (Employee employee : availableEmployees) {
                String forEmployeeEncrypted = Long.toString(employee.longHashCode());
                ShiftRosterInfo sri = new ShiftRosterInfo(employee);
                Shift empShiftOnThisDay = sri.getEmployeeShiftThisDay(this.rosterDay);
                WebProcess_TnaRoster aProc = getAlreadyLoggedProcess(employee, rosterDay);
                LeaveInfo leaveInfo = fc.getLeaveInfoFromContainer(employee);
                LeaveHistoryEntry leaveHistoryEntry = leaveInfo.didTakeLeaveOn(rosterDay);
                boolean isOnLeave = (leaveHistoryEntry != null);
                if (employeesToUnlink.contains(employee) || aProc != null || isOnLeave) {
                    if (aProc != null || isOnLeave) {
                        contentsMultiAvailable[0][row] = "<input name=\"hiddenAvailable_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"hidden\">";
                    } else {
                        contentsMultiAvailable[0][row] = "<input name=\"multiEmpToLink_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                    }
                    for (int i = 0; i < 5; i++) {
                        contentsMultiAvailableStyles[i][row] = "transferred";
                    }
                } else {
                    contentsMultiAvailable[0][row] = "<input name=\"multiEmpToLink_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                }
                contentsMultiAvailable[1][row] = employee.getEmployeeNumber();
                contentsMultiAvailable[2][row] = employee.getSurname();
                contentsMultiAvailable[3][row] = employee.getFirstName();
                if (empShiftOnThisDay.getName() == null) {
                    contentsMultiAvailable[4][row] = "&nbsp;";
                } else {
                    contentsMultiAvailable[4][row] = empShiftOnThisDay.getName();
                }
                // Overwrite shift if a process is already logged
                if (aProc != null) {
                    Shift newShift = aProc.getNewShift();
                    String newShiftName;
                    if (newShift == null || newShift.getName().isEmpty()) {
                        newShiftName = "none (No Profile)";
                    } else {
                        newShiftName = newShift.getName();
                    }
                    contentsMultiAvailable[4][row] = "Moving to " + newShiftName;
                }
                // Is the employee on leave
                if (isOnLeave) {
                    contentsMultiAvailable[4][row] = "On Leave";
                }
                row++;
            }

            // Add detail into a formatted Web Page
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("employeeTable", headersMultiSelection, contentsMultiAvailable, contentsMultiAvailableStyles));
            if (availableEmployees.size() > 0) {

                resultPage.append("<!-- Horizontal Ruler -->");
                //resultPage.append("<hr width=\"98%\">");
                resultPage.append("<table class=\"center\" style=\"width: 100%;\" >");
                resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button-green\" type=\"submit\" name=\"ActionButton\" value=\"Link Selected >>\" style=\"margin-right: 30px;\"></td>");
                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna_roster_to\">");
                resultPage.append("<input type=\"hidden\" name=\"shiftID\" value=\"").append(this.shift.getShiftID()).append("\">");
                resultPage.append("<input type=\"hidden\" name=\"rosterDay\" value=\"").append(DatabaseObject.formatDate(this.rosterDay)).append("\">");
                resultPage.append("</tr></table>");
            }

            resultPage.append("</FORM>");
//</editor-fold>
            ////////////////////////////////////////////////////////////////////
            // End 
            resultPage.append("</td><td>");
            // Linked Employees
            ////////////////////////////////////////////////////////////////////
            // Add all the employees 
            ////////////////////////////////////////////////////////////////////
            resultPage.append("<FORM name=\"req_tna_roster_linked\" METHOD=POST action=\"").append(link).append("\">");

            // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEES LINKED">// Reset the iterator for available employees
            row = 0;
            for (Employee employee : linkedEmployees) {
                String forEmployeeEncrypted = Long.toString(employee.longHashCode());
                ShiftRosterInfo sri = new ShiftRosterInfo(employee);
                Shift empShiftOnThisDay = sri.getEmployeeShiftThisDay(this.rosterDay);
                WebProcess_TnaRoster aProc = getAlreadyLoggedProcess(employee, rosterDay);
                LeaveInfo leaveInfo = fc.getLeaveInfoFromContainer(employee);
                LeaveHistoryEntry leaveHistoryEntry = leaveInfo.didTakeLeaveOn(rosterDay);
                boolean isOnLeave = (leaveHistoryEntry != null);
                if (employeesToLink.contains(employee) || aProc != null || isOnLeave) {
                    if (aProc != null) {
                        contentsMultiLinked[0][row] = "<input name=\"hiddenLinked_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"hidden\">";
                    } else {
                        contentsMultiLinked[0][row] = "<input name=\"multiEmpToUnlink_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                    }
                    for (int i = 0; i < 5; i++) {
                        contentsMultiLinkedStyles[i][row] = "transferred";
                    }
                } else {
                    contentsMultiLinked[0][row] = "<input name=\"multiEmpToUnlink_" + row + "\" value=\"" + forEmployeeEncrypted + "\" type=\"checkbox\">";
                }
                contentsMultiLinked[1][row] = employee.getEmployeeNumber();
                contentsMultiLinked[2][row] = employee.getSurname();
                contentsMultiLinked[3][row] = employee.getFirstName();
                if (empShiftOnThisDay.getName() == null) {
                    contentsMultiLinked[4][row] = "&nbsp;";
                } else {
                    contentsMultiLinked[4][row] = empShiftOnThisDay.getName();
                }
                // Overwrite shift if a process is already logged
                if (aProc != null) {
                    Shift newShift = aProc.getNewShift();
                    String newShiftName;
                    if (newShift == null || newShift.getName().isEmpty()) {
                        newShiftName = "none (No Profile)";
                    } else {
                        newShiftName = newShift.getName();
                    }
                    contentsMultiLinked[4][row] = "Moving to " + newShiftName;
                }
                // Is the employee on leave
                if (isOnLeave) {
                    contentsMultiLinked[4][row] = "On Leave";
                }
                row++;
            }

            // Add detail into a formatted Web Page
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("employeeTable2", headersMultiSelection, contentsMultiLinked, contentsMultiLinkedStyles));
            if (linkedEmployees.size() > 0) {

                resultPage.append("<!-- Horizontal Ruler -->");
                //resultPage.append("<hr width=\"98%\">");
                resultPage.append("<table class=\"center\" style=\"width: 100%;\" >");
                resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button-grey\" type=\"submit\" name=\"ActionButton\" value=\"<< Unlink Selected\" style=\"margin-left: 20px; float: left;\"></td>");
                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna_roster_from\">");
                resultPage.append("<input type=\"hidden\" name=\"shiftID\" value=\"").append(this.shift.getShiftID()).append("\">");
                resultPage.append("<input type=\"hidden\" name=\"rosterDay\" value=\"").append(DatabaseObject.formatDate(this.rosterDay)).append("\">");
                resultPage.append("</tr></table>");
            }

            resultPage.append("</FORM>");
//</editor-fold>
            ////////////////////////////////////////////////////////////////////
            // End 
            resultPage.append("</td></tr>");

            resultPage.append("</table></div>");

            // Submit changes
            link = baseURL + "Controller.jsp";
            resultPage.append("<div style=\"width: 1100px;\"><FORM name=\"req_tna_roster\" METHOD=POST action=\"").append(link).append("\">");
            resultPage.append("<!-- Horizontal Ruler -->");
            resultPage.append("<hr width=\"98%\">");
            resultPage.append("<table class=\"center\" style=\"width: 100%;\" >");
            resultPage.append("<tr><td align=\"right\" width=120><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" value=\"Submit changes\" style=\"margin-right: 30px;\"></td>");
            resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna_roster\">");
            resultPage.append("<input type=\"hidden\" name=\"shiftID\" value=\"").append(this.shift.getShiftID()).append("\">");
            resultPage.append("<input type=\"hidden\" name=\"rosterDay\" value=\"").append(DatabaseObject.formatDate(this.rosterDay)).append("\">");
            resultPage.append("</tr></table>");
            resultPage.append("</FORM></div>");

            resultPage.append("</CENTER>");
        }

        resultPage.append("<br>&nbsp;<br>&nbsp;");

        return resultPage.toString();
    }

    /**
     * Scans the 'Live' process list to see if a shift change for this day has
     * already been logged. We cannot allow the person to make a roster change
     * for a day if he/she has already done so.
     *
     * @param anEmployee
     * @param aDay
     * @return
     */
    private WebProcess_TnaRoster getAlreadyLoggedProcess(Employee anEmployee, java.util.Date aDay) {
        boolean rslt = false;
        TreeMap webProcesses = FileContainer.getInstance().getWebProcessesAffecting(anEmployee, true);
        Iterator iter = webProcesses.values().iterator();
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();
            if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition") == 0) {
                Employee processEmployee = process.getEmployee();
                if (processEmployee.equals(anEmployee)) {
                    WebProcess_TnaRoster aProc = (WebProcess_TnaRoster) process;
                    if (aProc.getRosterDay().equals(aDay)) {
                        if (aProc.isActive()) {
                            return aProc;
                        }
                    }
                }
            }
        }
        return null;
    }

    private final Shift shift;
    private java.util.Date referenceDate;
    private final java.util.Date rosterDay;
    private final String forwardingURL;
    private final WebProcessDefinition webProcessDefinition;
    private final HttpSession sessionObject;
    private final LinkedList<Employee> employeeList;
    private final LinkedList<Employee> employeesToLink = new LinkedList();
    private final LinkedList<Employee> employeesToUnlink = new LinkedList();
} // end ShiftRosterPageProducer

