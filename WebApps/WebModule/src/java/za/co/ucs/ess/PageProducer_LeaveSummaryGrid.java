package za.co.ucs.ess;

import java.text.DecimalFormat;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_LeaveSummaryGrid inherits from PageProducer and will display a summary of leave taken for
 * a given employee, spanning two years from the latest entry in the leave history table </p>
 */
public class PageProducer_LeaveSummaryGrid extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_LeaveSummaryGrid(Employee employee) {
        this.employee = employee;
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();
        LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);

        java.util.Date lastLeaveTaken = leaveInfo.getDateOfLastLeaveTaken();
        if (lastLeaveTaken == null) {
            return "";
        }

        // If the last leave taken was somewhere in the past, use today as the last
        // point of reference
        if (lastLeaveTaken.before(new java.util.Date())) {
            lastLeaveTaken = new java.util.Date();
        }

        // Array that will store the last 12 months (names)
        String[] months;
        months = DatabaseObject.getPrevious_N_Months(lastLeaveTaken, 12);

        // Start by defining the CSS script for vertical text
        //resultPage.append(getCSSScript());
        // Table Header
        resultPage.append("<CENTER>");
        resultPage.append("<h3 class=\"shaddow\">Leave Summary</h3>");
        resultPage.append("<div class=\"grouping\" style=\"width: 1170px;\">");
        resultPage.append("<table id=\"displaytable\">");
        // 1st Row: Month Headers
        resultPage.append("  <thead><tr><th style=\"border-right: 1px solid #000000;\"></th>");
        for (int i = 0; i < months.length; i++) {
            resultPage.append("<th scope=\"col\">").append(months[i]).append("</th>");
        }
        resultPage.append("  </tr></thead>");

        // Remaining rows, one per leave type
        for (int j = 0; j < leaveInfo.getNumberOfLeaveTypes(); j++) {
            int leaveTypeID = leaveInfo.getLeaveTypeID(j);
            if (didTakeLeave(leaveInfo, months, leaveTypeID)) {
                String leaveHeading;
                if (WebModulePreferences.getInstance().useLeaveProfileDescription()) {
                    resultPage.append("<tr><td style=\"border-right: 1px solid #000000;\"><b>").append(leaveInfo.getLeaveCaption(j)).append("</b></td>");
                } else {
                    resultPage.append("<tr><td style=\"border-right: 1px solid #000000;\"><b>").append(leaveInfo.getLeaveType(j)).append("</b></td>");
                }

                for (int k = 0; k < months.length; k++) {
                    java.util.Date leaveMonth = DatabaseObject.formatMonthYearToDate((String) months[k]);
                    float unitsTaken = leaveInfo.getUnitsTakenInMonth(leaveMonth, leaveTypeID);
                    String leaveDetail = leaveInfo.getLeaveDetailOfUnitsTakenInMonth(leaveMonth, leaveTypeID);
                    if (unitsTaken == 0) {
                        //resultPage.append("<td bgcolor=\"#ECECEC\">");
                        resultPage.append("<td>");
                        resultPage.append("</td>");
                    } else {
                        resultPage.append("<td bgcolor=\"#ffffaf\"><font size=\"-1\">");
                        //resultPage.append("<a href=\"#\" class=\"hintanchor\" onMouseover=\"showhint('From:2006/01/01<br>To:2006/01/06', this, event, '150px')\">"+unitsTaken+"</a>");
                        resultPage.append("<a class=\"hintanchor\" onMouseover=\"showhint('").append(leaveDetail).append("', this, event, '150px')\">").append(new DecimalFormat("#.##").format(unitsTaken)).append("</a>");
                        resultPage.append("</font></td>");
                    }
                }
                resultPage.append("  </tr>");
            }
        }

        // Table Footer
        resultPage.append("</table>");
        resultPage.append("</div></CENTER>");

        return resultPage.toString();
    }

    /**
     * CSS script to allow writing of text vertically
     */
    private String getCSSScript() {
        return ("<style> "
                + "<!-- .verticaltext { "
                + "writing-mode: tb-rl; "
                + "filter: flipv fliph; "
                + "} "
                + "--> "
                + "</style> ");
    }

    // Checks to see if any leave were taken in these 12 months
    private boolean didTakeLeave(LeaveInfo leaveInfo, String[] months, int leaveTypeID) {
        boolean result = false;
        for (int k = 0; k < months.length; k++) {
            java.util.Date leaveMonth = DatabaseObject.formatMonthYearToDate((String) months[k]);
            float unitsTaken = leaveInfo.getUnitsTakenInMonth(leaveMonth, leaveTypeID);
            if (unitsTaken > 0) {
                return true;
            }
        }
        return result;
    }
    private Employee employee;
} // end LeavePageProducer

