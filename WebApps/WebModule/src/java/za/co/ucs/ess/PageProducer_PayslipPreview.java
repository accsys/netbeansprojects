package za.co.ucs.ess;

import java.text.DecimalFormat;
import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_PayslipPreview inherits from PageProducer. It is used to display
 * the payslip for a given period.
 * </p>
 */
public class PageProducer_PayslipPreview extends PageProducer {
    // To simplify the creation and maintenance of the payslips, we'll be
    // breaking the code into blocks.  Ie. Earning-block, Deduction-block, etc.

    /**
     * Constructor for a payslip in the CURRENT TAX YEAR
     *
     * @param employee
     * @param payrollDetail PayrollDetail class directing us to the right period
     */
    public PageProducer_PayslipPreview(Employee employee, PayrollDetail payrollDetail) {
        this.employee = employee;
        this.payrollDetail = payrollDetail;
        // We create an Instance of PayslipInfo for EVERY runtype that has been calculated in this period
        for (int runtype = 1; runtype <= 5; runtype++) {
            PayslipInfo info;
            info = new PayslipInfo(employee, payrollDetail.getPayrolldetailID().intValue(), runtype);
            if (info != null) {
                if (info.isCalculatedForEmployee()) {
                    payslipInfos.add(info);
                }
            }

        }
    }

    /**
     * Constructor for a payslip from PREVIOUS TAX YEARS
     *
     * @param employee
     * @param archivedPayrollDetail ArchivedPayrollDetail class directing us to
     * the right period
     */
    public PageProducer_PayslipPreview(Employee employee, ArchivedPayrollDetail archivedPayrollDetail) {
        this.employee = employee;
        this.isArchivedPayrollDetail = true;
        this.archivedPayrollDetail = archivedPayrollDetail;
        // We create an Instance of PayslipInfo for EVERY runtype that has been calculated in this period
        for (int runtype = 1; runtype <= 5; runtype++) {
            ArchivedPayslipInfo info;
            info = new ArchivedPayslipInfo(employee, archivedPayrollDetail.getPayrolldetailID(), runtype, archivedPayrollDetail.getTaxYearStart());
            if (info != null) {
                if (info.isCalculatedForEmployee()) {
                    payslipInfos.add(info);
                }
            }
        }
    }

    /**
     * A ProcessPreview Page Producer
     * @return 
     */
    @Override
    public String generateInformationContent() {
        if (employee == null) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();
        // One payslip per calculated RunType
        for (int i = 0; i < payslipInfos.size(); i++) {
            PayslipInfo info = (PayslipInfo) payslipInfos.get(i);
            //
            // Generate PDF Payslip if ESS_Reporting_Registered is true
            //
            if (FileContainer.isESSReportingRegistered && i == 0) {
                if (isArchivedPayrollDetail) {
                    if (FileContainer.getInstance().isPayslipPdfCompatible(info.getEmployee().getCompany().getCompanyID().toString(),
                            info.getEmployee().getEmployeeID().toString(), info.getEmployee().getPayroll().getPayrollID().toString(),
                            info.getPayrollDetail_id(), this.archivedPayrollDetail.getEndDate().toString())) {

                        resultPage.append("<hr style=\"width: 80%;\"><center><iframe id='iframe' style=\"width: 80%; height: 800px;\" src=\"report_app.jsp?wmode=transparent&reportNum="
                                + "ESSP1102_OLD"
                                + "&payrollDetailID="
                                + info.getPayrollDetail_id()
                                + "&payrollID="
                                + info.getEmployee().getPayroll().getPayrollID()
                                + "&endDate="
                                + this.archivedPayrollDetail.getEndDate()
                                + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");
                    } else {
                        resultPage.append(getHTMLPayslip(info));
                    }

                } else {
                    resultPage.append("<hr style=\"width: 80%;\"><center><iframe id='iframe' style=\"width: 80%; height: 800px;\" src=\"report_app.jsp?reportNum="
                            + "ESSP1102"
                            + "&payrollDetailID="
                            + info.getPayrollDetail_id()
                            + "&payrollID="
                            + info.getEmployee().getPayroll().getPayrollID()
                            + "&runtypeID="
                            + info.getRuntype_id()
                            + "\" name=\"iframe_main\" frameborder=\"2\"></iframe></center>");

//                    System.out.print("PayrollDetailID: " + info.getPayrollDetail_id()
//                            + "\nPayrollID: " + this.employee.getPayroll().getPayrollID()
//                            + "\nRuntypeID: " + info.getRuntype_id());
                }
            } else if (!FileContainer.isESSReportingRegistered) {
                resultPage.append(getHTMLPayslip(info));
            }
        }
        return resultPage.toString();
    }

    private String getHTMLPayslip(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        //
        //  Print normal HTML Payslip
        //
        result.append("<div class=\"outer-grouping\"\"><table><tr><td>");
        result.append(getHeaderTable());
        result.append("<hr style=\"margin-left: 15px; margin-right: 15px;\" size=\"1\" color=\"#c0c0c0\">");
        result.append("<table style=\"width: 980px; margin-left: 10px;\">");
        result.append(" <tr><td align=\"left\"><b>Run Type: </b></td><td>").append(info.getRunType()).append("</td>");
        result.append("     <td align=\"left\"><b>Rate Of Pay: </b></td><td>").append(info.getRateOfPay1()).append("</td>");
        result.append("     <td align=\"left\"><b>Job Position: </b></td><td>").append(info.getJobPosition()).append("</td>");
        result.append(" </tr>");
        result.append(" </table>");

        result.append("<table>");
        result.append("     <tr><td><div class=\"grouping\">");
        //result.append("         <table width=\"100%\" border=\"1\" bordercolor=\"#C0C0C0\">");
        result.append("         <table id=\"displaytable_wide\">");
        result.append("             <thead><tr><th><h3  class=\"shaddow\"><b>Earnings</b></td>");
        result.append("                    <th><h3  class=\"shaddow\"><b>Deductions</b></td>");
        result.append("                    <th><h3  class=\"shaddow\"><b>Year To Date</b></td>");
        result.append("             </tr></thead>");
        result.append("             <tbody><tr>");
        result.append("                 <td style=\"vertical-align: top;\">");
        result.append(getEarningsBreakdown(info));
        result.append("                 </td>");
        result.append("                 <td style=\"vertical-align: top;\">");
        result.append(getDeductionsBreakdown(info));
        result.append("                 </td>");
        result.append("                 <td style=\"vertical-align: top;\">");
        result.append(getYTDBreakdown(info));
        result.append("                 </td>");
        result.append("             </tr>");
        result.append("             <tr>");
        result.append("                 <td>");
        result.append(getTotalEarnings(info));
        result.append("                 </td>");
        result.append("                 <td>");
        result.append(getTotalDeductions(info));
        result.append("                 </td>");
        result.append("                 <td>");
        result.append(getNettPay(info));
        result.append("                 </td>");
        result.append("             </tr>");
        result.append("         </tbody></table></div>");
        result.append("	</td></tr>");

        result.append("     <tr><td>");
        result.append(getOthersBreakdown(info));
        result.append("	</td></tr>");
        result.append("</table>");
        
        result.append("</td></tr></table></div>");
        
        return result.toString();
    }

    private String getEarningsBreakdown(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList earnings = info.getEarnings();
        result.append("        <table id=\"displaytable_inner\">");
        for (int i = 0; i < earnings.size(); i++) {
            VariableDetail detail = (VariableDetail) earnings.get(i);
            result.append("            <tr>");
            // Do not display 0 quantities
            if (detail.getQty() == 0) {
                result.append("            <td><font size=\"-1\"></font></td>");
            } else {
                result.append("            <td align=\"left\"><font size=\"-1\">").append(new DecimalFormat("0.00").format((double) detail.getQty())).append("</font></td>");
            }

            result.append("            <td align=\"left\"><font size=\"-1\">").append(detail.getName()).append("</font></td>");
            result.append("            <td align=\"right\"><font size=\"-1\">").append(new DecimalFormat("0.00").format((double) detail.getValue())).append("</font></td>");
            result.append("            </tr>");
        }
        result.append("        </table>");
        return result.toString();
    }

    private String getDeductionsBreakdown(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList deductions = info.getDeductions();
        result.append("        <table id=\"displaytable_inner\">");
        for (int i = 0; i < deductions.size(); i++) {
            VariableDetail detail = (VariableDetail) deductions.get(i);
            result.append("            <tr>");
            result.append("            <td align=\"left\"><font size=\"-1\">").append(detail.getName()).append("</font></td>");
            result.append("            <td align=\"right\"><font size=\"-1\">").append(new DecimalFormat("0.00").format((double) detail.getValue())).append("</font></td>");
            // Do not display 0 balances
            if (detail.getBalance() == 0) {
                result.append("            <td align=\"right\"><font size=\"-1\"></font></td>");
            } else {
                result.append("            <td align=\"right\"><font size=\"-1\">").append(new DecimalFormat("0.00").format((double) detail.getBalance())).append("</font></td>");
            }

            result.append("            </tr>");
        }
        result.append("        </table>");
        return result.toString();
    }

    private String getYTDBreakdown(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList ytd = info.getYTDs();
        result.append("        <table id=\"displaytable_inner\">");
        for (int i = 0; i < ytd.size(); i++) {
            VariableDetail detail = (VariableDetail) ytd.get(i);
            result.append("            <tr>");
            result.append("            <td align=\"left\"><font size=\"-1\">").append(detail.getName()).append("</font></td>");
            result.append("            <td align=\"right\"><font size=\"-1\">").append(new DecimalFormat("0.00").format((double) detail.getValue())).append("</font></td>");
            result.append("            </tr>");
        }
        result.append("        </table>");
        return result.toString();
    }

    private String getTotalEarnings(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList earnings = info.getEarnings();
        float total = 0;
        for (int i = 0; i < earnings.size(); i++) {
            VariableDetail detail = (VariableDetail) earnings.get(i);
            total += detail.getValue();
        }

        result.append("	<table id=\"displaytable_inner\">");
        result.append("	<tr>");
        result.append("		<td width=70% align=\"left\"><font size=\"-1\"><b>Total Earnings</b</font></td>");
        result.append("		<td align=\"right\" ><font size=\"-1\"><b>").append(new DecimalFormat("0.00").format((double) total)).append("</b></font></td>");
        result.append("	</tr>");
        result.append("	</table>");
        return result.toString();
    }

    private String getTotalDeductions(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList deductions = info.getDeductions();
        float total = 0;
        for (int i = 0; i < deductions.size(); i++) {
            VariableDetail detail = (VariableDetail) deductions.get(i);
            total += detail.getValue();
        }

        result.append("	<table id=\"displaytable_inner\">");
        result.append("	<tr>");
        result.append("		<td width=70% align=\"left\"><font size=\"-1\"><b>Total Deductions</b</font></td>");
        result.append("		<td align=\"right\" ><font size=\"-1\"><b>").append(new DecimalFormat("0.00").format((double) total)).append("</b></font></td>");
        result.append("	</tr>");
        result.append("	</table>");
        return result.toString();
    }

    private String getNettPay(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        result.append("	<table id=\"displaytable_inner\">");
        result.append("	<tr>");
        result.append("		<td width=70% align=\"left\"><font size=\"-1\"><b>Nett Pay</b></font></td>");
        result.append("		<td align=\"right\" ><font size=\"-1\"><b>").append(new DecimalFormat("0.00").format((double) new Float(info.getNettPay()))).append("</b></font></td>");
        result.append("	</tr>");
        result.append("	</table>");
        return result.toString();
    }

    private String getOthersBreakdown(PayslipInfo info) {
        StringBuilder result = new StringBuilder();
        LinkedList other = info.getOthers();
        result.append("<font style=\"margin-left: 10px;\" size=\"-1\"><b>Other Values</b></font><br><table style=\"margin-left: 10px;\" border=\"0\" bordercolor=\"#C0C0C0\">");
        for (int i = 0; i < other.size(); i++) {
            VariableDetail detail = (VariableDetail) other.get(i);
            result.append("            <tr>");
            result.append("            <td align=\"left\"><font size=\"-2\">").append(detail.getName()).append("</font></td>");
            result.append("            <td align=\"right\"><font size=\"-2\"><b>").append(new DecimalFormat("0.00").format((double) detail.getValue())).append("</b></font></td>");
            result.append("            </tr>");
        }
        result.append("        </table>");
        return result.toString();
    }

    private String getHeaderTable() {
        String endDate;
        if (isArchivedPayrollDetail) {
            endDate = DatabaseObject.formatDate(this.archivedPayrollDetail.getEndDate());
        } else {
            endDate = DatabaseObject.formatDate(this.payrollDetail.getEndDate());
        }

        StringBuilder result = new StringBuilder();
        result.append("<table style=\"margin: 10px; width: 980px;\">");
        result.append("  <tr><td><h3  class=\"shaddow\">Name: </h3></td><td>").append(this.employee.getSurname()).append(", ").append(this.employee.getFirstName()).append("</td>");
        result.append("		 <td><h3  class=\"shaddow\">Company: </h3></td><td>").append(this.employee.getCompany().getName()).append("</td>");
        result.append("	  	 <td><h3  class=\"shaddow\">Payroll: </h3></td><td>").append(this.employee.getPayroll().getName()).append("</td></tr>");
        result.append("  <tr><td><h3  class=\"shaddow\">First Start Date: </h3></td><td>").append(DatabaseObject.formatDate(this.employee.getFirstDOE())).append("</td>");
        result.append("          <td><h3  class=\"shaddow\">Group Join Date: </h3></td><td>").append(DatabaseObject.formatDate(this.employee.getGroupJoinDate())).append("</td>");
        result.append("	 	 <td><h3  class=\"shaddow\">Period Ending: </h3></td><td>").append(endDate).append("</td></tr>");
        result.append("  <tr><td><h3  class=\"shaddow\">Employee Number: </h3></td><td>").append(this.employee.getEmployeeNumber()).append("</td>");
        result.append("          <td><h3  class=\"shaddow\">ID Number: </h3></td><td>").append(this.employee.getIDNumber()).append("</td>");
        result.append("          <td><h3  class=\"shaddow\">Tax Number: </h3></td><td>").append(this.employee.getTaxNumber()).append("</td></tr>");
        result.append("</table>");

        return result.toString();
    }
    private ArchivedPayrollDetail archivedPayrollDetail;
    private PayrollDetail payrollDetail;
    private boolean isArchivedPayrollDetail = false;
    private Employee employee;
    private LinkedList payslipInfos = new LinkedList(); // One PayslipInfo object for each runtype
} // end LeavePageProducer

