package za.co.ucs.ess;

import za.co.ucs.accsys.webmodule.WebModulePreferences;

/**
 * PageSectionGenerator is a Singleton used to provide the standard look&feel of
 * the internal contents of our Web Pages.
 *
 * @author liam
 */
public class PageSectionGenerator {

    /**
     * Creates a new instance of PageSectionGenerator
     */
    private PageSectionGenerator() {
    }

    public static PageSectionGenerator getInstance() {
        if (pageSectionGenerator == null) {
            pageSectionGenerator = new PageSectionGenerator();
        }
        return pageSectionGenerator;
    }

    /**
     * Generates a standard TOP - CENTRE heading for a Web Page
     */
    public String buildHTMLPageHeader(String majorHeading, String minorHeading) {
        StringBuilder result = new StringBuilder();

        result.append("<table id=\"htmlPageHeader\" style=\"width: 90%;\"><tr><td width=\"40%\">");
        result.append("<span class=\"standardHeading\">");
        result.append(majorHeading).append(": </span>");
        result.append("<span class=\"standardHeading_NotBold\">");
        result.append(minorHeading).append("</span>");
        result.append("<hr  align=\"left\" color=\"#848589\" size=\"1\" width=\"100%\"><br>");
        result.append("</td><td width=\"50%\">");
        result.append("<img width=300 height=128 style=\"float: right;\" alt=\"Your Logo\" ");
        result.append("      src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.HTML_CompanyLogo, "../images/CompanyLogo.png")).append("\"");
        result.append(" hspace=\"30\" vspace=\"12\"></td>");
        result.append("</tr></table>");

        return (result.toString());
    }

    /**
     * Returns a standardised <table ...> tag for outer tables
     */
    public String getHTMLOuterTableTag() {
        return "<table width=\"100%\"";
    }

    /**
     * Returns a standardised <table ...> tag for tables within tables
     *
     * @param cssTableID - the CSS name of the table that should be used
     */
    public String getHTMLInnerTableTag(String cssTableID) {
        return ("<table id=\"" + cssTableID + "\" >");
        //return ("<table class=\"center\">");
    }

    /**
     * Generates an HTML table with the contents displayed in it.
     *
     * @param cssTableID - the CSS name of the table that should be used.
     * Defaults to "displaytable"
     * @param headers - Header column captions
     * @param contents - [col][row]
     */
    public String buildHTMLTable(String[] headers, String[][] contents) {
        return buildHTMLTable("displaytable", headers, contents, null);
    }
    
    public String buildHTMLTable(String cssTableID, String[] headers, String[][] contents) {
        return buildHTMLTable(cssTableID, headers, contents, null);
    }

    /**
     * Builds a CSS table with
     *
     * @param cssTableID - the CSS name of the table that should be used
     * @param headers - Header column captions
     * @param contents - [col][row]
     * @param styles
     * @return
     */
    public String buildHTMLTable(String cssTableID, String[] headers, String[][] contents, String[][] styles) {
        StringBuilder result = new StringBuilder();
        result.append("<div class=\"grouping\">");
        // Open Table tag
        result.append(getHTMLInnerTableTag(cssTableID));

        // Headers
        result.append("<thead><tr>");
        for (String header : headers) {
            result.append("<th scope=\"col\">").append(header).append("</th>");
        }
        result.append("</tr></thead>");

        // Data
        if (contents != null) {
            if (contents.length > 0) {
                for (int row = 0; row < contents[0].length; row++) {
                    result.append("<tr>");
                    for (int col = 0; col < contents.length; col++) {
                        String value = contents[col][row];
                        String styleClass = "smallFont";
                        if (styles != null) {
                            styleClass = styles[col][row];
                            if (styleClass == null) {
                                styleClass = "smallFont";
                            }
                        }
                        if (value == null) {
                            result.append("<td>").append("&nbsp;").append("</td>");
                        }
                        if (value != null) {
                            result.append("<td class=\"").append(styleClass).append("\">").append(value).append("</td>");
                        }
                    }
                    result.append("</tr>");
                }
            } else {
                result.append("<tr><td><i>No Data</i></td></tr>");
            }
        }

        // Close Table Tag
        result.append("</table></div>");
        return (result.toString());
    }

    /**
     * Builds a CSS table with
     *
     * @param cssTableID - the CSS name of the table that should be used
     * @param headers - Header column captions
     * @param contents - [col][row]
     * @return
     */
    public String buildHTMLTableOuter(String cssTableID, String[] headers, String[][] contents) {
        StringBuilder result = new StringBuilder();
        result.append("<div class=\"outer-grouping\">");
        // Open Table tag
        result.append(getHTMLInnerTableTag(cssTableID));

        // Headers
        result.append("<thead><tr>");
        for (int i = 0; i < headers.length; i++) {
            result.append("<th scope=\"col\">").append(headers[i]).append("</th>");
        }
        result.append("</tr></thead>");

        // Data
        if (contents != null) {
            if (contents.length > 0) {
                for (int row = 0; row < contents[0].length; row++) {
                    result.append("<tr>");
                    for (int col = 0; col < contents.length; col++) {
                        String value = contents[col][row];
                        if (value == null) {
                            result.append("<td style=\"display:none\">").append(" ").append("</td>");
                        }
                        if (value != null) {
                            result.append("<td class=\"smallFont\">").append(value).append("</td>");
                        }
                    }
                    result.append("</tr>");
                }
            } else {
                result.append("<tr><td colspan=\"").append(headers.length).append("\"><center><i><b>No Data</b></i></center></td></tr>");
            }
        }

        // Close Table Tag
        result.append("</table></div>");
        return (result.toString());
    }

    // Returns True if this is an even number
    private boolean isEven(int value) {
        return ((java.lang.Math.floor(value / 2)) * 2 == new Double(value).doubleValue());
    }
    static private PageSectionGenerator pageSectionGenerator = null;
}
