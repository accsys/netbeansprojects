package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.*;

/**
 * <p> PageProducer_Processes inherits from PageProducer and will display a list
 * of Processes (Active / All) being logged for the given employee </p>
 */
public class PageProducer_Processes extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_Processes(Employee employee, boolean onlyActiveProcesses) {
        this.employee = employee;
        this.onlyActiveProcesses = onlyActiveProcesses;
    }

    /**
     * A PageProducer_Processes Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();

        // Overview Table
        String[] headers = {"Type", "Detail", "Requested", "Status"};
        // Contents of Leave Processes as found in the FileContainer
        FileContainer fc = FileContainer.getInstance();
        TreeMap processes = fc.getWebProcessesInitiatedBy(this.employee, onlyActiveProcesses);
        String[][] contents = new String[4][processes.values().size()];

        // Create an entry in the table for each WebProcess
        Iterator iter = processes.values().iterator();
        int row = 0;
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();

            String link = "<a href=\"proc_prev.jsp?hashValue=" + process.hashCode() + "\" >";
            contents[0][row] = link + process.getProcessName() + "</a>";
            contents[1][row] = process.toHTMLString();
            contents[2][row] = process.formatDate(process.getCreationDate());
            if (!process.isActive()) {
                contents[3][row] = "<font color=\"#ff0000\">" + process.getStatus() + "</font>";
            } else {
                contents[3][row] = process.getStatus();
            }
            row++;
        }


        // Add detail into a formatted Web Page
        String heading;
        if (onlyActiveProcesses) {
            heading = "List of Active processes";
        } else {
            heading = "Historical list of all processes";
        }
        resultPage.append("<center><h3 class=\"shaddow\">").append(heading).append("</h3>");
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));
        resultPage.append("</center>");
        return resultPage.toString();
    }
    private Employee employee;
    private boolean onlyActiveProcesses;
} // end LeavePageProducer

