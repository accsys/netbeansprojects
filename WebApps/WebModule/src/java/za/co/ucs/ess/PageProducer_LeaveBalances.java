package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_LeaveBalances inherits from PageProducer and will display the Leave Balances for this
 * employee </p>
 */
public class PageProducer_LeaveBalances extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_LeaveBalances(Employee employee, String balanceDate, String callerURL) {
        //System.out.println("PageProducer_LeaveBalances:" + callerURL);
        this.leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);
        // Watch out for "TODAY"
        if ((balanceDate.compareToIgnoreCase("today") == 0) || (balanceDate.trim().length() == 0)) {
            this.balanceDate = new java.util.Date();
        } else {
            this.balanceDate = DatabaseObject.formatDate(balanceDate);
        }
        this.callerURL = callerURL;
    }

    /**
     * Returns true if the balance date is the same as today()
     */
    private boolean isToday() {
        if (balanceDate == null) {
            balanceDate = new java.util.Date();
        }
        GregorianCalendar balanceCalendar = new GregorianCalendar();
        balanceCalendar.setTime(balanceDate);
        GregorianCalendar todayCalendar = new GregorianCalendar();
        todayCalendar.setTime(new java.util.Date());
        return ((balanceCalendar.get(GregorianCalendar.DAY_OF_YEAR) == todayCalendar.get(GregorianCalendar.DAY_OF_YEAR))
                && (balanceCalendar.get(GregorianCalendar.YEAR) == todayCalendar.get(GregorianCalendar.YEAR)));
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();


        String[] headers = {"Leave Type", "Balance:<br>" + DatabaseObject.formatDate(balanceDate)};
        resultPage.append("<CENTER>");
        resultPage.append("<div style=\"width: 335px;\">");
        if (this.callerURL.toUpperCase().indexOf("LEAVE_PREV.JSP") > 0) {
            resultPage.append("<h3 class=\"shaddow\">Leave Balances As At ").append(getSelectHTML(balanceDate)).append("</h3>");
        } else {
            resultPage.append("<h3 class=\"shaddow\">Leave Balances - Today </h3>");
        }

        // Let's count how many rows we are allowed to display, based on the HidLeaveBalancesFor attribute
        int nbRows = 0;
        for (int row = 0; row < leaveInfo.getNumberOfLeaveTypes(); row++) {
            if (!WebModulePreferences.getInstance().hideLeaveBalanceFor(leaveInfo.getLeaveType(row))) {
                nbRows++;
            }
        }

        String[][] contents = new String[2][nbRows];
        int aRowIndex = 0;
        // construct the contents matrix
        for (int row = 0; row < leaveInfo.getNumberOfLeaveTypes(); row++) {
            // Only show balance if the client did not explicitly exclude it
            if (!WebModulePreferences.getInstance().hideLeaveBalanceFor(leaveInfo.getLeaveType(row))) {
                // Leave Type
                if (WebModulePreferences.getInstance().useLeaveProfileDescription()) {
                    contents[0][aRowIndex] = leaveInfo.getLeaveCaption(row);
                } else {
                    contents[0][aRowIndex] = leaveInfo.getLeaveType(row);
                }
                // Leave Balance
                if (isToday()) {
                    contents[1][aRowIndex] = new Float(leaveInfo.getCurrentBalance(row)).toString();
                } else {
                    contents[1][aRowIndex] = new Float(leaveInfo.getOpeningBalanceInMonth(balanceDate, leaveInfo.getLeaveTypeID(row))).toString();
                }
                aRowIndex++;
            }
        }
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers, contents));
        resultPage.append("</div></CENTER>");


        return resultPage.toString();
    }

    /**
     * Returns the HTML that constructs the selection of available leave balance dates
     */
    private String getSelectHTML(java.util.Date selectedDate) {
        StringBuilder selection = new StringBuilder();

        selection.append("<SELECT name=\"balanceDate\" ONCHANGE=\"location = this.options[this.selectedIndex].value;\">");
        //selection.append("<SELECT name=\"balanceDate\" >");
        // Add TODAY
        String lnk;
        if (callerURL.indexOf("?") >= 0) {
            lnk = "&";
        } else {
            lnk = "?";
        }
        if (isToday()) {
            selection.append(" <option value=\"" + callerURL + lnk + "balanceDate=Today\" selected>Today</option>");
        } else {
            selection.append(" <option value=\"" + callerURL + lnk + "balanceDate=Today\">Today</option>");
        }
        // Now we create a list of months surrounding the current date
        String[] months = new String[18];
        String link;
        months = DatabaseObject.get_Next_N_Months(new java.util.Date(), 12);
        for (int i = 0; i < months.length; i++) {
            // Have we already included the '?' in the link?
            link = callerURL + lnk + "balanceDate=" + months[i];

            if (DatabaseObject.formatDate(selectedDate).compareToIgnoreCase(months[i]) == 0) {
                selection.append(" <option value=\"" + link + "\" selected>" + months[i] + "</option>");
            } else {
                selection.append(" <option value=\"" + link + "\" >" + months[i] + "</option>");
            }
        }
        selection.append("</select>");
        return selection.toString();
    }
    private String callerURL;
    private java.util.Date balanceDate;
    private LeaveInfo leaveInfo;
} // end LeavePageProducer

