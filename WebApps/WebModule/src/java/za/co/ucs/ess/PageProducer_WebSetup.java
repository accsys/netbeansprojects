/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.ess;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import za.co.ucs.accsys.webmodule.EmployeeSelection;
import za.co.ucs.accsys.webmodule.EmployeeSelectionRule;
import za.co.ucs.accsys.webmodule.ReportingStructure;
import za.co.ucs.accsys.webmodule.SelectionInterpreter;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;

/**
 *
 * @author lterblanche
 */
public class PageProducer_WebSetup extends PageProducer {

    za.co.ucs.accsys.webmodule.FileContainer fileContainer;

    public PageProducer_WebSetup(za.co.ucs.accsys.webmodule.FileContainer fileContainer) {
        this.fileContainer = fileContainer;
    }

    @Override
    public String generateInformationContent() {
        StringBuilder result = new StringBuilder();

        // Title
        result.append("<CENTER><h3 class=\"shaddow\" style=\"text-decoration: underline;\">ESS Configuration</h3></CENTER>");
        result.append("<table class=\"center\"><tr><td>");
        result.append("<font size=\"-2\">Print Date:").append(new java.util.Date()).append("</font>");
        result.append("<br><font size=\"-2\">JAR Version:").append(za.co.ucs.accsys.webmodule.ESSVersion.getInstance().getJarVersion()).append("</font><center>");

        // Existing Web Processes
        result.append("<br><div class=\"outer-grouping\"><span style=\"padding: 5px; float: left;\"><b>Current Web Processes:</b></span><br><br>");
        result.append("<div class=\"grouping\"><table id=\"displaytable_wide\" style=\"min-width: 1000px; border: 0;\">");
        result.append("<font size=\"-3\">");
        result.append("<tr><th>Name</th><th>Max Stage Age</th><th>Max Process Age</th>");
        result.append("    <th>Reporting Structure</th><th>Supervisor Process</th></tr>");
        Iterator wpIter = fileContainer.getWebProcessDefinitions().iterator();
        while (wpIter.hasNext()) {
            WebProcessDefinition wpd = (WebProcessDefinition) wpIter.next();
            result.append("<tr><td>").append(wpd.getName()).append("</td>");
            result.append("    <td>").append(wpd.getMaxStageAge()).append("</td>");
            result.append("    <td>").append(wpd.getMaxProcessAge()).append("</td>");
            if (wpd.getReportingStructure() == null) {
                result.append("    <td>-- None --</td>");
            } else {
                result.append("    <td>").append(wpd.getReportingStructure()).append("</td>");
            }
            if (wpd.getCannotInstantiateSelf()) {
                result.append("    <td>Y</td></tr>");
            } else {
                result.append("    <td>N</td></tr>");
            }
        }
        result.append("</font>");
        result.append("</table></div></div>");

        // Existing Reporting Structures
        result.append("<br><div class=\"outer-grouping\"><span style=\"padding: 5px; float: left;\"><b>Reporting Structures:</b></span><br>");
        result.append("<table style=\"min-width: 1000px; border: 0;\">");
        result.append("<font size=\"-3\">");
        Iterator rsIter = fileContainer.getReportingStructures().iterator();
        while (rsIter.hasNext()) {
            ReportingStructure rep = (ReportingStructure) rsIter.next();
            result.append("<tr><td><table><tr><td><div class=\"grouping\" style=\"margin: 5px;\"><table style=\"min-width: 998px;\"><tr><td width=\"15%\"><b>").append(rep.toString()).append("</b></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("</tr><tr><td colspan=\"6\"><hr></td></tr>");

            // Get TopMost Levels
            ArrayList topNodes = rep.getTopNodes();
            Iterator nodeIter = topNodes.iterator();

            if (nodeIter.hasNext()) {
                // There can only be one TopNode
                EmployeeSelection topNode = (EmployeeSelection) nodeIter.next();
                result.append("<tr><td>").append(topNode.getName()).append("</td></tr>");

                LinkedList children1 = rep.getChildEmployeeSelections(topNode);
                Iterator children1Iter = children1.iterator();
                while (children1Iter.hasNext()) {
                    EmployeeSelection child1 = (EmployeeSelection) children1Iter.next();
                    result.append("<tr><td></td><td>").append(child1.getName()).append("</td></tr>");

                    LinkedList children2 = rep.getChildEmployeeSelections(child1);
                    Iterator children2Iter = children2.iterator();
                    while (children2Iter.hasNext()) {
                        EmployeeSelection child2 = (EmployeeSelection) children2Iter.next();
                        result.append("<tr><td></td><td></td><td>").append(child2.getName()).append("</td></tr>");

                        LinkedList children3 = rep.getChildEmployeeSelections(child2);
                        Iterator children3Iter = children3.iterator();
                        while (children3Iter.hasNext()) {
                            EmployeeSelection child3 = (EmployeeSelection) children3Iter.next();
                            result.append("<tr><td></td><td></td><td></td><td>").append(child3.getName()).append("</td></tr>");

                            LinkedList children4 = rep.getChildEmployeeSelections(child3);
                            Iterator children4Iter = children4.iterator();
                            while (children4Iter.hasNext()) {
                                EmployeeSelection child4 = (EmployeeSelection) children4Iter.next();
                                result.append("<tr><td></td><td></td><td></td><td></td><td>").append(child4.getName()).append("</td></tr>");

                                LinkedList children5 = rep.getChildEmployeeSelections(child4);
                                Iterator children5Iter = children5.iterator();
                                while (children5Iter.hasNext()) {
                                    EmployeeSelection child5 = (EmployeeSelection) children5Iter.next();
                                    result.append("<tr><td></td><td></td><td></td><td></td><td></td><td>").append(child5.getName()).append("</td></tr>");
                                }
                            }
                        }
                    }
                }
            }
            result.append("</table></div>");
            result.append("</td></tr></table></tr>");
        }
        result.append("</font>");
        result.append("</table></div>");

        // Existing Employee Selections
        result.append("<br><div class=\"outer-grouping\"><span style=\"padding: 5px; float: left;\"><b>Employee Selections:</b></span><br><br>");
        result.append("<div class=\"grouping\"><table width=\"100%\" border=\"0\">");
        result.append("<font size=\"-3\">");
        result.append("<tr><td><b>Name</b></td><td><b>Rule</b></td></tr>");
        Iterator esIter = fileContainer.getEmployeeSelections().iterator();
        while (esIter.hasNext()) {
            EmployeeSelectionRule es = (EmployeeSelectionRule) esIter.next();
            result.append("<tr><td colspan=\"2\"><hr></td></tr><tr><td>").append(es.getName()).append("</td>");
            result.append("    <td>").append(es.getRule()).append("</td></tr>");
        }
        result.append("</font>");
        result.append("</table></div></div>");

        // Listing all employees in Employee Selections
        if (true) {
            result.append("<br><div class=\"outer-grouping\"><span style=\"padding: 5px; float: left;\"><b>Detailed Employee Selections:</b></span><br><br>");
            result.append("<div class=\"grouping\"><table width=\"100%\" border=\"0\">");
            result.append("<font size=\"-3\">");
            result.append("<tr><td><b>Rule Name</b></td><td><b>Employees</b></td></tr>");
            Iterator ruleIter = fileContainer.getEmployeeSelections().iterator();
            while (ruleIter.hasNext()) {
                EmployeeSelectionRule es = (EmployeeSelectionRule) ruleIter.next();
                result.append("<tr><td colspan=\"2\"><hr></td></tr><tr><td>").append(es.getName()).append("</td>");
                result.append("    <td>");
                Iterator employeeIterator = SelectionInterpreter.getInstance().generateEmployeeList(es).iterator();
                int counter = 0;
                while (employeeIterator.hasNext()) {
                    za.co.ucs.accsys.peopleware.Employee employee = (za.co.ucs.accsys.peopleware.Employee) employeeIterator.next();
                    result.append(employee.toString());
                    // Lists 4 employees in one row before forcing a <br> in the HTML file
                    if (counter == 4) {
                        result.append("<br>");
                        counter = 0;
                    } else {
                        result.append("; ");
                    }
                    counter++;
                }
                result.append("</td></tr>");
            }
            result.append("</font>");
            result.append("</table>");
        }
        result.append("</CENTER></td></tr></table></div></div>");

        return result.toString();

    }

}
