package za.co.ucs.ess;

import java.util.LinkedList;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.LeaveHistoryEntry;
import za.co.ucs.accsys.peopleware.LeaveInfo;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_LeaveHistory inherits from PageProducer and will display the history of leave taken for
 * given employee </p>
 */
public class PageProducer_LeaveHistory extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_LeaveHistory(Employee employee) {
        this.employee = employee;
        this.leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();


        // History Table 
        for (int i = 0; i < leaveInfo.getNumberOfLeaveTypes(); i++) {
            LinkedList history = leaveInfo.getLeaveHistory(i);
            String leaveHeading;
            if (WebModulePreferences.getInstance().useLeaveProfileDescription()) {
                leaveHeading = leaveInfo.getLeaveCaption(i);
            } else {
                leaveHeading = leaveInfo.getLeaveType(i);
            }
            resultPage.append("<div class=\"item\">");

            resultPage.append("<h3 class=\"shaddow\" style=\"margin-left: 33px;\">").append(leaveHeading).append(" Leave History</h3>");

            resultPage.append("<div class=\"grouping\">");
            
            resultPage.append("<table id=\"displaytable\"");
            resultPage.append("  <thead><tr>");
            resultPage.append("     <th  width=10px scope=\"col\">From Date</th>");
            resultPage.append("     <th  width=10px scope=\"col\">To Date</th>");
            resultPage.append("     <th  width=10px scope=\"col\">Units</th>");
            resultPage.append("     <th  width=10px scope=\"col\">Reason</th>");
            resultPage.append("  </tr></thead>");

            for (int j = 0; j < history.size(); j++) {
                {
                    resultPage.append("  <tr>");
                    // The first column contains a radio button that shows the From and To dates 
                    // and passes the HISTORY_ID as the selection value
                    LeaveHistoryEntry entry = (LeaveHistoryEntry) history.get(j);
                    resultPage.append("  <td>").append(DatabaseObject.formatDate(entry.getFromDate())).append("</td>");
                    resultPage.append("  <td>").append(DatabaseObject.formatDate(entry.getToDate())).append("</td>");
                    resultPage.append("  <td>").append(String.valueOf(entry.getUnitsTaken())).append(" ").append(entry.getUnit()).append("</td>");
                    resultPage.append("  <td>").append(entry.getReason()).append("</td>");
                    resultPage.append("</tr>");
                }
            }
            resultPage.append("</table>");
            resultPage.append("</div></div>");
        }

        return resultPage.toString();
    }
    private LeaveInfo leaveInfo;
    private Employee employee;
} // end LeavePageProducer

