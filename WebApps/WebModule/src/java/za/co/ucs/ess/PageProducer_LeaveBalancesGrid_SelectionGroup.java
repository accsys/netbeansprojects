package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.LeaveInfo;
import za.co.ucs.accsys.webmodule.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_LeaveBalancesGrid_SelectionGroup inherits from PageProducer and will display
 * all leave balances for all employees that reports to a given person in the
 * leave request reporting structures.
 * </p>
 */
public class PageProducer_LeaveBalancesGrid_SelectionGroup extends PageProducer {

    /** Constructor
     * @param webProcess The current web process.  The reference date for displaying the calendar will be as at the time of the requested leave
     */
    public PageProducer_LeaveBalancesGrid_SelectionGroup(WebProcess webProcess, String forwardingURL, String hashValue) {
        this(webProcess.getProcessHistory().getOriginProcessStage().getEmployeeSelection(),
                (((WebProcess_LeaveRequisition) webProcess).getFromDate()), forwardingURL, hashValue);
    }

    /** Constructor
     * @param employeeSelection The list of employees that should be comprised in the calendar view.  The reference date is now(*)
     */
    public PageProducer_LeaveBalancesGrid_SelectionGroup(EmployeeSelection employeeSelection) {
        this(employeeSelection, new java.util.Date(), null, null);
    }

    /** Constructor
     * @param employeeSelection The list of employees that should be comprised in the calendar view.
     * @param referenceDate The date for which the calendar should be set up
     */
    public PageProducer_LeaveBalancesGrid_SelectionGroup(EmployeeSelection employeeSelection, java.util.Date referenceDate, String forwardingURL) {
        this(employeeSelection, referenceDate, forwardingURL, null);
    }

    /** Constructor
     * @param webProcess The current web process.
     * @param referenceDate The date for which the calendar should be set up
     */
    public PageProducer_LeaveBalancesGrid_SelectionGroup(WebProcess webProcess, java.util.Date referenceDate, String forwardingURL, String hashValue) {
        this(webProcess.getProcessHistory().getOriginProcessStage().getEmployeeSelection(), referenceDate, forwardingURL, hashValue);
    }

    /** Constructor
     * @param employeeSelection The list of employees that should be comprised in the calendar view.
     * @param referenceDate The date for which the calendar should be set up
     */
    public PageProducer_LeaveBalancesGrid_SelectionGroup(EmployeeSelection employeeSelection, java.util.Date referenceDate, String forwardingURL, String hashValue) {
        //System.out.println("PageProducer_LeaveBalancesGrid_SelectionGroup:  employeeSelection=" + employeeSelection);
        //System.out.println("\n\t" + "consisting of " + SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection).size() + " employees.");
        this.forwardingURL = forwardingURL;
        this.hashValue = hashValue;

        // The original group to which the logger of the request belonged to
        originalSelection = employeeSelection;
        // The Start_Date of the requested leave
        this.referenceDate = referenceDate;
        if (this.referenceDate == null) {
            this.referenceDate = new java.util.Date();
        }

        LinkedList unsortedEmployees = new LinkedList(SelectionInterpreter.getInstance().generateEmployeeList(originalSelection));

        // Place all the employees into a sorted map.
        System.out.println("Preparing detail for " + unsortedEmployees.size() + " employees");
        for (int i = 0; i < unsortedEmployees.size(); i++) {
            Employee employee = (Employee) unsortedEmployees.get(i);
            String key = ((Employee) unsortedEmployees.get(i)).getSurname() + ((Employee) unsortedEmployees.get(i)).getEmployeeNumber();
            employees.put(key, unsortedEmployees.get(i));
        }
    }

    /** A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        // No use building info if there are no employees in this group
        if (this.employees.size() == 0) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();


        // Start by defining the CSS script for vertical text
        //resultPage.append(getCSSScript());

        // Table Header
        resultPage.append("<CENTER><h3 class=\"shaddow\">Monthly Opening Balances Of Group: <span style=\"color:#f02229\">").append(this.originalSelection.getName()).append("</span></h3>");
        resultPage.append("<div><br></div><div class=\"outer-grouping\" style=\"display: inline-block;\"><table style=\"min-width: 800px;\">");

        // 1st Row: 'Previous Month', 'Selected Month Name', 'Next Month'
        resultPage.append("<tr><td><table style=\"width: 100%;\">");

        resultPage.append("<tr>");

        java.util.Date referredDate;
        String referredURL;

        // **** PREVIOUS MONTH ****
        if (this.forwardingURL != null) { // No use creating a forwarding session if we do not know where it came from
            referredDate = DatabaseObject.removeOneMonth(this.referenceDate);
            referredURL = forwardingURL + "?referenceDate=" + DatabaseObject.formatDate(referredDate)
                    + "&hashValue=" + hashValue + "&selSelection=" + this.originalSelection.hashCode();
            //System.out.println("newURL:" + referredURL);
            resultPage.append("<td width=\"10%\" style=\"text-align: left;\"><a href=\"").append(referredURL).append("\"><img  style=\"border: 0px\" alt=\"Previous Month\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/mvLeft.png\"></a></td>");
        }


        // **** CURRENTLY SELECTED MONTH DISPLAYED ****
        resultPage.append("<td width=\"80%\" style=\"text-align: center;\" class=\"standardHeading_NotBold\"><b>").append(DatabaseObject.formatDateToMonthYear(referenceDate)).append("</b></td>");

        // **** NEXT MONTH ****
        if (this.forwardingURL != null) {
            referredDate = DatabaseObject.addOneMonth(this.referenceDate);
            referredURL = forwardingURL + "?referenceDate=" + DatabaseObject.formatDate(referredDate)
                    + "&hashValue=" + hashValue + "&selSelection=" + this.originalSelection.hashCode();
            //System.out.println("newURL:" + referredURL);
            resultPage.append("<td width=\"10%\" style=\"text-align: right;\"><a href=\"" + referredURL + "\"><img style=\"border: 0px\" alt=\"Next Month\" src=\""
                    + WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "") + "images/mvRight.png\"></a></td>");
        }

        resultPage.append("</tr>");
        resultPage.append("</table></td></tr>");

        // determine the spacing of the page
        String[] leaveTypes = getCommonLeaveTypes(this.employees);
        int empPerc = 40;
        int leaveTypesPerc = java.lang.Math.round(60 / leaveTypes.length);

        // 2nd Row: Common Leave Types
        resultPage.append("<tr><td><div class=\"grouping\" style=\"display: inline-block;\"><table style=\"min-width: 780px;\">");
        resultPage.append("  <tr><td style=\"width: " + empPerc + "%\"></td>");


        for (int i = 0; i < leaveTypes.length; i++) {
            resultPage.append("    <td style=\"width: " + leaveTypesPerc + "%\"><b><span style=\"text-decoration: underline;\">" + leaveTypes[i] + "</span></b></td>");
        }
        resultPage.append("  </tr>");


        // Remaining rows, one per employee
        Iterator iter = this.employees.values().iterator();
        while (iter.hasNext()) {
            Employee employee = (Employee) iter.next();

            LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);
            // Get the leave balances per leave type for this employee

            // Add employee to left of table
            resultPage.append("  <tr><td><small>").append(employee.toString()).append("</small></td>");

            // Step through all the leave types, listing the appropriate leave balances
            for (int idx = 0; idx < leaveTypes.length; idx++) {
/*
 if (!WebModulePreferences.getInstance().hideLeaveBalanceFor(leaveInfo.getLeaveType(idx))) {
 *
 */
                    resultPage.append(getLeaveBalanceString(referenceDate, employee, leaveInfo, leaveTypes[idx]));
/*                } else {
                    resultPage.append("<td> - </td>");
                }
 *
 */
            }
            resultPage.append("  </tr>");
        }

        // Table Footer
        resultPage.append("</table></div></td></tr>");

        resultPage.append("</table></div></CENTER>");

        return resultPage.toString();
    }

    // Returns the HTML entry for a single cell for an employee on a given day
    private String getLeaveBalanceString(java.util.Date aDay, Employee employee, LeaveInfo leaveInfo, String leaveType) {
        // Get the first day of the month
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDay);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DATE));
        java.util.Date firstDayOfMonth = calendar.getTime();

        String defaultColor = "\"#ededed\"";

        // Get the leave balances at month-end for given leave type
        float openingBalance = leaveInfo.getOpeningBalanceInMonth(firstDayOfMonth, leaveInfo.getLeaveTypeID(leaveType));
        String result = "<td>" + new Float(openingBalance).toString() + "</td>";

        return (result);
    }

    /** CSS script to allow writing of text vertically
     */
    private String getCSSScript() {
        /*
        return (
        "<style> "+
        "<!-- .verticaltext { "+
        "writing-mode: tb-rl; "+
        "filter: flipv fliph; "+
        "} "+
        "--> "+
        "</style> "
        );
         **/
        return ("<style> "
                + "<!-- .verticaltext { "
                + "writing-mode: tb-rl; "
                + "filter: flipv fliph; "
                + "} "
                + "--> "
                + "</style> ");
    }

    /** Returns a sorted list of common-used leave types
     */
    private String[] getCommonLeaveTypes(TreeMap employees) {
        TreeSet leaveTypeSet = new TreeSet();
        Iterator iter = employees.values().iterator();
        while (iter.hasNext()) {
            Employee employee = (Employee) iter.next();
            LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);
            int nbLeaveTypes = leaveInfo.getNumberOfLeaveTypes();
            String leaveHeading;
            for (int i = 0; i < nbLeaveTypes; i++) {
                
                if (WebModulePreferences.getInstance().useLeaveProfileDescription()) {
                    leaveHeading = leaveInfo.getLeaveCaption(i);
                } else {
                    leaveHeading = leaveInfo.getLeaveType(i);
                }                
                leaveTypeSet.add(leaveHeading);
            }
        }
        Iterator iter2 = leaveTypeSet.iterator();
        String[] commonLeaveTypes = new String[leaveTypeSet.size()];
        int idx = 0;
        while (iter2.hasNext()) {
            commonLeaveTypes[idx++] = (String) iter2.next();
        }
        return commonLeaveTypes;
    }
// The employees that reports to the given manager
    private TreeMap employees = new TreeMap();
    private java.util.Date referenceDate;
    private EmployeeSelection originalSelection;
    private String forwardingURL;
    private String hashValue;
} // end LeavePageProducer

