package za.co.ucs.ess;

import java.util.Iterator;
import java.util.LinkedList;
import za.co.ucs.accsys.peopleware.Contact;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_EmployeeContactRequest inherits from PageProducer and will
 * display the detail of an employee's contact information in HTML The generated
 * page allows for changing of these contact detail
 * </p>
 */
public class PageProducer_EmployeeContactRequest extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_EmployeeContactRequest(Employee employee) {
        this.employee = employee;
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        Contact contact = employee.getContact();

        StringBuilder resultPage = new StringBuilder();

        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String link = baseURL + "Controller.jsp";

        resultPage.append("<FORM name=\"req_employeecontact\" METHOD=POST action=\"").append(link).append("\"\">");
        
        resultPage.append("<div class=\"outer-grouping\" style=\"width: 1100px;\">");
        
        resultPage.append("<table  class=\"center\" style=\"margin-left: 0px; margin-right: 0px;\">");
        // 1st : Personal Detail
        resultPage.append("  <tr><td colspan=6><h3 class=\"shaddow\" style=\"padding-left: 5px;\">Personal Details</h3></td></tr>");
        resultPage.append("  <tr><td><div class=\"grouping\" style=\"width: 1060px;\"><table><tr>");
        resultPage.append("      <td>Firstname:</td><td><input type=\"text\" name=\"firstName\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getFirstName()).append("\"></td>");
        resultPage.append("      <td>Second Name:</td><td><input type=\"text\" name=\"secondName\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getSecondName()).append("\"></td>");
        resultPage.append("      <td>Preferred Name:</td><td><input type=\"text\" name=\"preferredName\" size=\"15\" maxlength=\"15\" value=\"").append(employee.getPreferredName()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>Initials:</td><td><input type=\"text\" name=\"initials\" size=\"6\" maxlength=\"6\" value=\"").append(employee.getInitials()).append("\"></td>");
        resultPage.append("      <td>Surname:</td><td><input type=\"text\" name=\"surname\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getSurname()).append("\"></td>");
        resultPage.append("      <td>Date Of Birth:</td><td><input type=\"text\" name=\"birthDate\" placeholder=\"yyyy/mm/dd\" size=\"10\" maxlength=\"10\" value=\"").append(DatabaseObject.formatDate(employee.getBirthDate())).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        //
        // Gender Selection
        //
        resultPage.append("      <td>Gender:</td>");
        resultPage.append("      <td><select name=\"lstGender\">");
        if (employee.getGender().equals("Male")) {
            resultPage.append("<option value=\"Male\" selected>Male</option>");
        } else {
            resultPage.append("<option value=\"Male\" >Male</option>");
        }
        if (employee.getGender().equals("Female")) {
            resultPage.append("<option value=\"Female\" selected>Female</option>");
        } else {
            resultPage.append("<option value=\"Female\" >Female</option>");
        }
        resultPage.append("      </select> </td>");
        //
        // Race Selection
        //
        resultPage.append("      <td>Race:</td>");
        resultPage.append("      <td><select name=\"lstRace\">");
        if (employee.getRace().equals("African")) {
            resultPage.append("<option value=\"African\" selected>African</option>");
        } else {
            resultPage.append("<option value=\"African\" >African</option>");
        }
        if (employee.getRace().equals("Coloured")) {
            resultPage.append("<option value=\"Coloured\" selected>Coloured</option>");
        } else {
            resultPage.append("<option value=\"Coloured\" >Coloured</option>");
        }
        if (employee.getRace().equals("Indian")) {
            resultPage.append("<option value=\"Indian\" selected>Indian</option>");
        } else {
            resultPage.append("<option value=\"Indian\" >Indian</option>");
        }
        if (employee.getRace().equals("White")) {
            resultPage.append("<option value=\"White\" selected>White</option>");
        } else {
            resultPage.append("<option value=\"White\" >White</option>");
        }

        resultPage.append("      </select> </td>");

        resultPage.append("      <td>Tax Number:</td><td><input type=\"text\" name=\"taxNumber\" size=\"16\" maxlength=\"20\" value=\"").append(employee.getTaxNumber()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>Passport Number:</td><td><input type=\"text\" name=\"passportNumber\" size=\"16\" maxlength=\"20\" value=\"").append(employee.getPassportNumber()).append("\"></td>");
        resultPage.append("      <td>Country Of Issue:</td>");
        //changed from CountryOfIssue
        resultPage.append("     <td><SELECT name=\"CountryOfIssue\" >");
        //make the first selection blank
        resultPage.append("     <option></option>");
        LinkedList passportCountries = employee.getCompany().getPassportCountryNameList();
        Iterator iter = passportCountries.iterator();
        while (iter.hasNext()) {
            String country = (String) iter.next();
            //System.out.println("\n"+country);
            if (employee.getCountryOfIssue() == null) {
                resultPage.append(" <option value=\"").append(country).append("\" >").append(country).append("</option>");
            } else {
                if (employee.getCountryOfIssue().compareToIgnoreCase(country) == 0) {
                    resultPage.append(" <option value=\"").append(country).append("\" selected>").append(country).append("</option>");
                } else {
                    resultPage.append(" <option value=\"").append(country).append("\" >").append(country).append("</option>");
                }
            }
        }
        resultPage.append("      </select></td>");
        resultPage.append("  </tr></table></div></td></tr>");

        // 2nd : Phone Numbers
        resultPage.append("  <tr><td colspan=6><h3 class=\"shaddow\" style=\"padding-left: 5px;\">Numbers</h3></td></tr>");
        resultPage.append("  <tr><td><div class=\"grouping\" style=\"width: 1060px;\"><table><tr>");
        resultPage.append("      <td>Tel Home:</td><td><input type=\"text\" name=\"telHome\" size=\"13\" maxlength=\"15\" value=\"").append(contact.getTelHome()).append("\"></td>");
        resultPage.append("      <td>Tel Office:</td><td><input type=\"text\" name=\"telWork\" size=\"13\" maxlength=\"15\" value=\"").append(contact.getTelWork()).append("\"></td>");
        resultPage.append("      <td>E-Mail:</td><td><input type=\"text\" name=\"email\" size=\"35\" maxlength=\"75\" value=\"").append(contact.getEMail()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>Fax Home:</td><td><input type=\"text\" name=\"faxHome\" size=\"13\" maxlength=\"15\" value=\"").append(contact.getFaxHome()).append("\"></td>");
        resultPage.append("      <td>Fax Office:</td><td><input type=\"text\" name=\"faxWork\" size=\"13\" maxlength=\"15\" value=\"").append(contact.getFaxWork()).append("\"></td>");
        resultPage.append("      <td>Cell:</td><td><input type=\"text\" name=\"cell\" size=\"13\" maxlength=\"15\" value=\"").append(contact.getCellNo()).append("\"></td>");
        resultPage.append("  </tr></table></div></td></tr>");
        // 3rd Row: Street Address 
        resultPage.append("  <tr><td colspan=6><h3 class=\"shaddow\" style=\"padding-left: 5px;\">Physical Address</h3></td></tr><tr><td><div class=\"grouping\" style=\"width: 1060px;\"><table><tr>");
        resultPage.append("      <td>Unit No:</td><td><input type=\"text\" name=\"home_unitno\" size=\"5\" maxlength=\"10\" value=\"").append(contact.getPhys_HOME_UNITNO()).append("\"></td>");
        resultPage.append("      <td>Complex:</td><td><input type=\"text\" name=\"home_complex\" size=\"35\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_COMPLEX()).append("\"></td>");
        resultPage.append("      <td>Street No:</td><td><input type=\"text\" name=\"home_streetno\" size=\"5\" maxlength=\"10\" value=\"").append(contact.getPhys_HOME_STREETNO()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td><b><font color=\"red\">* </font></b>Street:</td><td><input type=\"text\" name=\"home_street\" size=\"30\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_STREET()).append("\"></td>");
        resultPage.append("      <td><b><font color=\"red\">* </font></b>Suburb:</td><td><input type=\"text\" name=\"home_suburb\" size=\"30\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_SUBURB()).append("\"></td>");
        resultPage.append("      <td><b><font color=\"red\">* </font></b>City:</td><td><input type=\"text\" name=\"home_city\" size=\"30\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_CITY()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>Province:</td><td><input type=\"text\" name=\"home_province\" size=\"30\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_PROVINCE()).append("\"></td>");
        resultPage.append("      <td>Country:</td><td><input type=\"text\" name=\"home_country\" size=\"30\" maxlength=\"100\" value=\"").append(contact.getPhys_HOME_COUNTRY()).append("\"></td>");
        resultPage.append("      <td>Code:</td><td><input type=\"text\" name=\"home_code\" size=\"8\" maxlength=\"10\" value=\"").append(contact.getPhys_HOME_CODE()).append("\"></td>");
        resultPage.append("  </tr></table></div></td></tr>");
        // 3rd Row: Postal Address
        resultPage.append("  <tr><td colspan=6><h3 class=\"shaddow\" style=\"padding-left: 5px;\">Postal Address</h3></td></tr><td><div class=\"grouping\" style=\"width: 1060px;\"><table><tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>PO Box:</td><td><input type=\"text\" name=\"postal1\" size=\"25\" maxlength=\"25\" value=\"").append(contact.getHomePostal1()).append("\"></td>");
        resultPage.append("      <td>Area:</td><td><input type=\"text\" name=\"postal2\" size=\"25\" maxlength=\"25\" value=\"").append(contact.getHomePostal2()).append("\"></td>");
        resultPage.append("      <td>City:</td><td><input type=\"text\" name=\"postal3\" size=\"25\" maxlength=\"25\" value=\"").append(contact.getHomePostal3()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr><td></td>");
        resultPage.append("      <td>Code:</td><td><input type=\"text\" name=\"postalCode\" size=\"8\" maxlength=\"8\" value=\"").append(contact.getHomePostalCode()).append("\"></td><td></td>");
        resultPage.append("  </tr></table></div></td></tr>");

        // 4th row : Emergency Details
        resultPage.append("  <tr><td colspan=6><h3 class=\"shaddow\" style=\"padding-left: 5px;\">Emergency Details</h3></td></tr>");
        resultPage.append("  <tr><td><div class=\"grouping\" style=\"width: 1060px;\"><table><tr>");
        resultPage.append("      <td>Doctors Name:</td><td><input type=\"text\" name=\"doctorName\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getDoctor()).append("\"></td>");
        resultPage.append("      <td>Phone Number:</td><td><input type=\"text\" name=\"doctorPhone\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getPhone()).append("\"></td>");
        resultPage.append("      <td>Known Conditions:</td><td><input type=\"text\" name=\"knownConditions\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getKnownConditions()).append("\"></td>");
        resultPage.append("  </tr>");
        resultPage.append("  <tr>");
        resultPage.append("      <td>Allergies:</td><td><input type=\"text\" name=\"allergies\" size=\"20\" maxlength=\"20\" value=\"").append(employee.getAllergies()).append("\"></td>");
        resultPage.append("  </tr></table></div></td></tr>");
        resultPage.append("</table>");

        // SUBMIT
        {
            resultPage.append("<!-- Horizontal Ruler -->");
            resultPage.append("<hr class=\"center\" style=\"margin-left: 15px; margin-right: 15px;\">");
            resultPage.append("<table class=\"center\">");
            resultPage.append("<tr>");
            resultPage.append("<td align=\"left\"><font color=\"red\"><b>* </b><i>Mandatory fields</i></font></td><td align=\"right\"><input class=\"gradient-button\" type=\"submit\" value=\"Submit Change(s)\"></td>");
            resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_employeecontact\">");
            resultPage.append("</tr>");
            resultPage.append("</table>");
        }

        resultPage.append("</div></form>");
        return resultPage.toString();
    }
    private Employee employee;
} // end LeavePageProducer

