package za.co.ucs.ess;

import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcessStage;
import za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition;

/**
 * <p>
 * PageProducer_ProcessAction inherits from PageProducer. This is where a
 * web-process gets accepted or rejected </p>
 */
public class PageProducer_ProcessAction extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_ProcessAction(WebProcess webProcess) {
        this.webProcess = webProcess;
    }

    /**
     * A ProcessPreview Page Producer
     */
    @Override
    public String generateInformationContent() {

        if (webProcess == null) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();

        // Overview Table
        String[] headers = {"Type", "Logged By", "On Behalf Of", "Logged On", "Detail"};
        String[][] contents = new String[5][1];
        // construct the contents matrix
        contents[0][0] = webProcess.getProcessName();
        contents[1][0] = webProcess.getLogger().toString();
        contents[2][0] = webProcess.getEmployee().toString();
        contents[3][0] = webProcess.formatDate(webProcess.getCreationDate());
        contents[4][0] = webProcess.toHTMLString();
        resultPage.append("<center><h3 class=\"shaddow\">Process Details</h3>");
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));

        {
            // Detail of leave request
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = baseURL + "Controller.jsp";
            resultPage.append("<FORM name=\"req_leave\" METHOD=POST action=\"").append(link).append("\" onSubmit=\"return confirm('Are you sure?');\">");

            // Detail Table - per WebProcessStage
            String[] detailHeader = {"Created", "Responsibility", "Action", "Reason", "Action Date"};
            String[][] processHistoryDetail = new String[5][webProcess.getProcessHistory().getProcessStages().size() + 1];
            resultPage.append("<br>");
            resultPage.append("<center><h3 class=\"shaddow\">Process History</h3>");

            //resultPage.append("<table id=\"displaytable_wide\"><tr><td><h3 class=\"ui-widget-header ui-corner-all\">Process History</h3></td></tr>");
            // Put the detail inside it's own table
            for (int i = 0; i < webProcess.getProcessHistory().getProcessStages().size(); i++) {
                WebProcessStage stage = (WebProcessStage) webProcess.getProcessHistory().getProcessStages().get(i);
                processHistoryDetail[0][i] = webProcess.formatDate(stage.getCreationDate());
                processHistoryDetail[1][i] = stage.getOwner().toString();
                processHistoryDetail[2][i] = stage.getStageAction();
                if (webProcess.getProcessName().compareToIgnoreCase("Leave Request") == 0 && i == 0) {
                    WebProcess_LeaveRequisition currentWebProcess_LeaveRequisition = (WebProcess_LeaveRequisition) webProcess;
                    if (currentWebProcess_LeaveRequisition.additionalDetail.compareToIgnoreCase("") != 0) {
                        processHistoryDetail[3][i] = stage.getStageReason() + ": " + currentWebProcess_LeaveRequisition.additionalDetail;
                    }
                    else {
                        processHistoryDetail[3][i] = stage.getStageReason();
                    }
                } else {
                    processHistoryDetail[3][i] = stage.getStageReason();
                }
                processHistoryDetail[4][i] = webProcess.formatDate(stage.getActionDate());
            }

            // For the current stage, we give the user the option to respond to the request
            WebProcessStage currentStage = (WebProcessStage) webProcess.getCurrentStage();
            processHistoryDetail[0][webProcess.getProcessHistory().getProcessStages().size()] = webProcess.formatDate(currentStage.getCreationDate());
            processHistoryDetail[1][webProcess.getProcessHistory().getProcessStages().size()] = currentStage.getOwner().toString();
            processHistoryDetail[2][webProcess.getProcessHistory().getProcessStages().size()]
                    = "<font style=\"white-space: nowrap; padding: 5px;\"><input type=\"radio\" name=\"rbApprove\" value=\"Approve\" checked> Approve<br></font>"
                    + "<font style=\"white-space: nowrap; padding: 5px; color: #EE091C;\"><input type=\"radio\" name=\"rbApprove\" value=\"Reject\"> Reject<br></font>";
            processHistoryDetail[3][webProcess.getProcessHistory().getProcessStages().size()]
                    = "<center><textarea maxlength=500 name=\"txtReason\" cols=\"25\" rows=\"5\" value=\"\"></textarea></center>";
            processHistoryDetail[4][webProcess.getProcessHistory().getProcessStages().size()]
                    = "<center><input class=\"gradient-button\" style=\"margin-right: 30px;\" type=\"submit\" value=\"Submit\"></center>";

            //resultPage.append("<tr><td>").append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", detailHeader, processHistoryDetail)).append("</td></tr>");
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", detailHeader, processHistoryDetail));
            resultPage.append("<input type=\"hidden\" name=\"action\" value=\"repl_proc\">");
            resultPage.append("<input type=\"hidden\" name=\"hashValue\" value=\"").append(webProcess.hashCode()).append("\">");
//            resultPage.append("</table>");
        }

        resultPage.append("</form>");
        return resultPage.toString();
    }
    private WebProcess webProcess;
} // end LeavePageProducer

