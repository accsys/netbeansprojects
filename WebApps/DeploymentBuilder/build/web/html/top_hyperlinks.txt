    <TABLE WIDTH = 80% BORDER=1  class="coolBar">
        <TR>
            <TD width=20% class="coolButton"><a href='/DeploymentBuilder/process/Home.jsp' title='Home'>Home</a></TD>
            <TD width=20% class="coolButton"><a href='/DeploymentBuilder/process/Management.jsp' title='Management'>Management</a></TD>
            <TD width=20% class="coolButton"><a href='/DeploymentBuilder/process/QA.jsp' title='Quality Assurance'>Quality Assurance</a></TD>
            <TD width=20% class="coolButton"><a href='/DeploymentBuilder/process/Development.jsp' title='Development'>Development</a></TD>
        </TR>
    </TABLE>
	<br>
    <TABLE WIDTH = 80% BORDER=1  class="coolBar">
        <TR>
            <TD width=20% class="coolButton"><a href='/DeploymentBuilder/process/FullList.jsp' title='Full List of all CRs'>Full List</a></TD>
            <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/process/Reminders.jsp' title='Reminders created on existing CR resources'>Reminders</a></TD>
            <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/process/TestInstallationHistory.jsp' title='Test Installation History'>Test Installation History</a></TD>
            <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/process/BuildHistory.jsp' title='Build History'>Build History</a></TD>
        </TR>
    </TABLE>

	<br>
    <TABLE WIDTH = 80% BORDER=1  class="coolBar">
        <TR>
            <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/process/PossibleProblems.jsp' title='Warnings'>Warnings</a></TD>
            <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/process/SearchForCR.jsp' title='Locate Change Request'>Locate Change Request</a></TD>
            <TD width=20%  class="coolButton"></TD>
	    <TD width=20%  class="coolButton"><a href='/DeploymentBuilder/jnlp/DeploymentAgent.jnlp' title='Launch Deployment Agent'>Launch Deployment Agent</a></TD>
        </TR>
    </TABLE>
