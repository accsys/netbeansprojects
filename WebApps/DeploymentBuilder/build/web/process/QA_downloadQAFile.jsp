<jsp:useBean id="theBean" scope="page" class="com.brainysoftware.web.FileDownloadBean" />
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }
  
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest(request.getParameter("crnumber"));
  }

  // Where is this file?
  TestInstallation testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());
  String file = fileContainer.getArchiveFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+testInstallation.getName()+FileContainer.TestInstallationExtension;
  History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DOWNLOADTESTINSTALLATION, testInstallation.getName()));
  theBean.download(response, file);

%>