<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Development Overview (New Server)</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

    User user = null;
    if (session.getAttribute("user")!=null){
        user = (User)session.getAttribute("user");
    }

    if (user == null){
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    }

    boolean showAll = false;
    if ((request.getParameter("showall") != null) && (request.getParameter("showall").compareTo("true")==0)){
        showAll = true;
    }

    // Only allow Developers and Management into this screen
    if ((user.getCategory().compareToIgnoreCase("Development")!=0)
    && (user.getCategory().compareToIgnoreCase("Management")!=0)){
        response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
    }

    FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
    TestInstallation testInstallation = null;
%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Find Files -->
        <br>
        <FORM action=Development_findFile.jsp method="post">
            <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><b>Locate a file</b></TD></TR>
            <tr><td>File Name (or part thereoff)</td>
                <td><input type="text" name="fileName" size="25" maxlength="40"></td>
                <td><INPUT TYPE=SUBMIT VALUE="Locate"></td>
            </tr>
            </TABLE>
        </FORM>
                <!-- New CR's available for TestInstallations -->
                <br>
                <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
                <tr><td><br><b>Change Requests available for TestInstallations (Dev Complete)</b></td></tr>
                <tr><td><% if (showAll){ out.write("<b>Complete List</b> | <a href=\"Development.jsp?showall=false\"><i>Latest 50</i></a>");}
        if (!showAll){ out.write("<a href=\"Development.jsp?showall=true\"><i>Complete List</i></a> | <b>Latest 50</b>");} %></td></tr>
                <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                    <tr bgcolor="#f0f0f0"><td width="10%">CR Number</td><td width="20%">Synopsis</td><td width="30%">Resources</td><td  width="20%">Attach More Resources</td><td width="20%">Action</td></tr>
                    <tr>
                        <% 
                            LinkedList incompleteCRs = fileContainer.getCRsThatHasIncompleteTestInstallations();
                            LinkedList devcompleteCRs = null;
                            if (showAll){ devcompleteCRs = fileContainer.getCRsThatRequireTestInstallations(0); }
                            if (!showAll){ devcompleteCRs = fileContainer.getCRsThatRequireTestInstallations(50); }

                            // Traverse through 'incompleteCRs', building up the table on the Web Page
                            int upperLimit = incompleteCRs.size();
                            if ((upperLimit > 50) && (!showAll)) upperLimit = 50;
                            for (int i=0; i<upperLimit; i++){
                                ChangeRequest changeRequest = (ChangeRequest)incompleteCRs.get(i);
                                changeRequest.refreshFromDatabase();
                                testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());

                                out.write("<tr>");

                                out.write("<td"+fileContainer.getChangeRequestBackgroundColor(changeRequest)+"><a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a>");

                                if (testInstallation.getInstallationInstructions().length()>0){
                                    out.write("<br><img src=\"../images/installationInstruction.bmp\" border=\"0\" width=\"32\" height=\"32\" "+
                                            "hspace=\"6\" vspace=\"2\">");
                                    out.write("<br><b>Installation Instructions</b>");
                                    out.write("<br>"+testInstallation.getInstallationInstructions());
                                }
                                out.write("</td>");
                                out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis())+"</td>");
                        %><%@ include file="_displayQAFileDetail.jsp" %><%            
                            out.write("<td>");
                            out.write("    <a href=\"Development_addCRFile.jsp?crnumber="+changeRequest.getCRNumber()+"\">Add Single File</a>");
                            out.write("    <br><a href=\"Development_addCRProgramFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Program Files</b></a>");
                            out.write("    <br><a href=\"Development_addCRScripts.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Scripts</b></a>");
                            out.write("    <br><a href=\"Development_addCRConfigFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Config Files</b></a>");
                            out.write("    <br><a href=\"Development_addCRDllFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Dll's</b></a>");
                            out.write("    <br><a href=\"Development_addCRReportFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Reports</b></a>");
                            out.write("</td>");

                            out.write("<td>");
                            if (testInstallation != null){
                                //                                            out.write("   <a href=\"Development_createBuild.jsp?crnumber="+changeRequest.getCRNumber()+"\">Create Build</a>");
                                out.write("   <br><a href=\"Development_disassembleBuild.jsp?crnumber="+changeRequest.getCRNumber()+"\" onclick=\"return confirm('Are you sure you want to disassemble this Build?')\">DisassembleBuild</a>");
                            }
                            out.write("</td>");
                            out.write("</tr>");
                                }
                                // Traverse through 'devcompleteCRs', building up the table on the Web Page
                                upperLimit = devcompleteCRs.size();
                                if ((upperLimit > 50) && (!showAll)) upperLimit = 50;
                                for (int i=0; i<upperLimit; i++){
                                    ChangeRequest changeRequest = (ChangeRequest)devcompleteCRs.get(i);
                                    changeRequest.refreshFromDatabase();
                                    testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());

                                    out.write("<tr>");
                                    out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a>");
                                    // Is this CR waiting for any other Test Installation?
                                    LinkedList bookingItems = fileContainer.getBookingItemsFor(changeRequest);
                                    if (bookingItems.size()>0){
                                        out.write("<img title=\"This CR is waiting for the completion of another CR.  See Reminders.\" src=\"../images/waitingForOthers.gif\" border=\"0\" width=\"21\" height=\"21\" "+
                                                "alt=\"This CR is waiting for the completion of another CR.  See Reminders.\""+
                                                "hspace=\"6\" vspace=\"2\"><br>");
                                    }

                                    // Are there any items waiting for the completion of this one?
                                    LinkedList bookingItems2 = fileContainer.getBookingItemsWaitingFor(testInstallation);
                                    if (bookingItems2 != null){
                                        int cntr = 0;
                                        for (int bi=0;bi<bookingItems2.size();bi++){
                                            cntr++;
                                            BookingItem bookingItem = (BookingItem)bookingItems2.get(bi);
                                            out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                                    "title=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\"><img src=\"../images/WaitingForMe.png\" border=\"0\" width=\"42\" height=\"42\" "+
                                                    "alt=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\""+
                                                    "hspace=\"6\" vspace=\"2\"></a>");
                                            if (cntr>1){
                                                cntr=0;
                                                out.write("<br>");
                                            }
                                        }
                                    }

                                    out.write("</td>");

                                    out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis())+"</td>");
                        %><%@ include file="_displayQAFileDetail.jsp" %><%            
                            out.write("<td>");
                            out.write("    <a href=\"Development_addCRFile.jsp?crnumber="+changeRequest.getCRNumber()+"\">Add Single File</a>");
                            out.write("    <br><a href=\"Development_addCRProgramFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Program Files</b></a>");
                            out.write("    <br><a href=\"Development_addCRScripts.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Scripts</b></a>");
                            out.write("    <br><a href=\"Development_addCRConfigFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Config Files</b></a>");
                            out.write("    <br><a href=\"Development_addCRDllFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Dll's</b></a>");
                            out.write("    <br><a href=\"Development_addCRReportFiles.jsp?crnumber="+changeRequest.getCRNumber()+"\"><b>Reports</b></a>");
                            out.write("</td><td></td></tr>");
                            }

                        %>
                    </tr>
                </table>
            </td></tr>
                </table>

<!-- Existing TestInstallations -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><br><b>Existing TestInstallations</b></td></tr>
            <tr><TD>
            <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                <tr bgcolor="#f0f0f0"><td width="10%">CR Number</td><td width="30%">Synopsis</td><td width="30%">Resources</td><td width="15%">Required DB Ver</td><td width="15%">Action</td></tr>
                <% 
                    // Scan FileContainer for TestInstallations with a status of BUILT
                    LinkedList testInstallations = fileContainer.getTestInstallations();
                    for (int i=0; i<testInstallations.size(); i++){
                        testInstallation = (TestInstallation)testInstallations.get(i);
                        if (testInstallation.getStatus() == TestInstallation.BUILT){
                            ChangeRequest changeRequest = testInstallation.getChangeRequest();
                            out.write("<tr>");
                            out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a><br>");

                            // Are there any items waiting for the completion of this one?
                            LinkedList bookingItems3 = fileContainer.getBookingItemsWaitingFor(testInstallation);
                            if (bookingItems3 != null){
                                int cntr = 0;
                                for (int bi=0;bi<bookingItems3.size();bi++){
                                    cntr++;
                                    BookingItem bookingItem = (BookingItem)bookingItems3.get(bi);
                                    out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                            "title=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\"><img src=\"../images/WaitingForMe.png\" border=\"0\" width=\"42\" height=\"42\" "+
                                            "alt=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\""+
                                            "hspace=\"6\" vspace=\"2\"></a>");
                                    if (cntr>1){
                                        cntr=0;
                                        out.write("<br>");
                                    }
                                }
                            }
                            if (testInstallation.getInstallationInstructions().length()>0){
                                out.write("<br><img src=\"../images/installationInstruction.bmp\" border=\"0\" width=\"32\" height=\"32\" "+
                                        "hspace=\"6\" vspace=\"2\">");
                                out.write("<br><b>Installation Instructions</b>");
                                out.write("<br>"+testInstallation.getInstallationInstructions());
                            }
                            out.write("</td>");
                            out.write("<td><font size=\"-1\">"+fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis())+"</font></td>");
                %><%@ include file="_displayQAFileDetail.jsp" %><%            
                    out.write("<td>"+testInstallation.getChangeRequest().getRequiredDBVersion()+"</td>");
                    out.write("<td>");
                    out.write("    <a href=\"QA_downloadQAFile.jsp?crnumber="+changeRequest.getCRNumber()+"\">Download File</a>");
                    out.write("</td>");
                    out.write("</tr>");
                    }
                    }
                %>
                </TR>
            </table>
            </td></tr>
	</table>
        


<%@include file="../html/bottom.html"%>
