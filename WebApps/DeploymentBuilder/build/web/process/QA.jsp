<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>


<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - QA Overview</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>
    <%

        User user = (User) session.getAttribute("user");

        // No Session User, try parameters
        if (user == null) {
            String loginUser = request.getParameter("loginName");
            user = new User(loginUser);
            if (!user.userExistsInDb()) {
                user = null;
            }
        }

        if (user == null) {
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        } else {

            boolean showQAComplete = false;
            if ((request.getParameter("showQAComplete") != null) && (request.getParameter("showQAComplete").compareTo("true") == 0)) {
                showQAComplete = true;
            }

            // Only allow QA and Management into this screen
            if ((user.getCategory().compareToIgnoreCase("QA") != 0)
                    && (user.getCategory().compareToIgnoreCase("Management") != 0)
                    && (user.getCategory().compareToIgnoreCase("Support") != 0)) {
                response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
            }

            FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
            TestInstallation testInstallation = null;
    %>
    <!-- Hyper Links -->
    <%@ include file="../html/top_hyperlinks.txt" %>
    <!-- Find Files -->
    <br>
    <FORM action=Development_findFile.jsp method="post">
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><b>Locate a file</b></TD></TR>
            <tr><td>File Name (or part thereoff)</td>
                <td><input type="text" name="fileName" size="25" maxlength="40"></td>
                <td><INPUT TYPE=SUBMIT VALUE="Locate"></td>
            </tr>
        </TABLE>
    </FORM>
    <!-- Existing TestInstallations -->
    <br>
    <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
        <tr><td><br><b>Existing TestInstallations</b></td></tr>
        <tr><td><% if (showQAComplete) {
                out.write("<b>Include 'QA Complete'</b> | <a href=\"QA.jsp?showQAComplete=false\"><i>Exclude 'QA Complete'</i></a>");
            }
            if (!showQAComplete) {
                out.write("<a href=\"QA.jsp?showQAComplete=true\"><i>Include 'QA Complete'</i></a> | <b>Exclude 'QA Complete'</b>");
            } %></td></tr>
        <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                    <tr bgcolor="#f0f0f0"><td width="10%">CR Number</td><td width="10%">Status</td><td width="18%">Synopsis</td><td width="9%">Required Ver</TD><td width="23%">Resources</td><td width="15%">Download Installation</td><td width="15%">Action</td></tr>
                            <%
                                // Scan FileContainer for TestInstallations with a status of BUILT
                                LinkedList testInstallations = fileContainer.getTestInstallations();
                                for (int i = 0; i < testInstallations.size(); i++) {
                                    testInstallation = (TestInstallation) testInstallations.get(i);
                                    if (testInstallation.getStatus() == TestInstallation.BUILT) {
                                        ChangeRequest changeRequest = testInstallation.getChangeRequest();
                                        changeRequest.refreshFromDatabase();
                                        if (changeRequest.getStatus().compareToIgnoreCase("QA Complete") == 0) {
                                            if (!showQAComplete) {
                                                continue;
                                            }
                                        }
                                        out.write("<tr>");
                                        // 
                                        // CR Number
                                        //
                                        out.write("<td" + fileContainer.getChangeRequestBackgroundColor(changeRequest) + "><a href=\"ChangeRequestInfo.jsp?crnumber=" + changeRequest.getCRNumber() + "\">" + changeRequest.getCRNumber() + "</a><br><br>");
                                        // Are there any items waiting for the completion of this one?
                                        LinkedList bookingItems = fileContainer.getBookingItemsWaitingFor(testInstallation);
                                        int cntr = 0;
                                        for (int bi = 0; bi < bookingItems.size(); bi++) {
                                            cntr++;
                                            BookingItem bookingItem = (BookingItem) bookingItems.get(bi);
                                            out.write("<a href=\"ChangeRequestInfo.jsp?crnumber=" + bookingItem.getChangeRequest().getCRNumber() + "\" "
                                                    + "title=\"CR " + bookingItem.getChangeRequest().getCRNumber() + " is waiting for the completion of this TestInstallation!\"><img src=\"../images/WaitingForMe.png\" border=\"0\" width=\"42\" height=\"42\" "
                                                    + "alt=\"CR " + bookingItem.getChangeRequest().getCRNumber() + " is waiting for the completion of this TestInstallation!\""
                                                    + "hspace=\"6\" vspace=\"2\"></a>");
                                            if (cntr > 1) {
                                                cntr = 0;
                                                out.write("<br>");
                                            }
                                        }
                                        if (testInstallation.getInstallationInstructions().length() > 0) {
                                            out.write("<br><img src=\"../images/installationInstruction.bmp\" border=\"0\" width=\"32\" height=\"32\" "
                                                    + "hspace=\"6\" vspace=\"2\">");
                                            out.write("<br><b>Installation Instructions</b>");
                                            out.write("<br>" + testInstallation.getInstallationInstructions());
                                        }
                                        out.write("</td>");
                                        //
                                        // Status
                                        //
                                        out.write("<td>" + changeRequest.getStatus() + "</td>");
                                        //
                                        // Synopsis
                                        //
                                        out.write("<td>" + fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis()) + "</td>");
                                        //
                                        // Req Version
                                        //
                                        out.write("<td>" + changeRequest.getRequiredDBVersion() + "</td>");
                                        // 
                                        // Resources
                                        //
%>
                            <%@ include file="_displayQAFileDetail.jsp" %>
                            <%                                                    // 
                                        // Download
                                        //
                                        out.write("<td>");

                                        out.write("    <a href=\"QA_downloadQAFile.jsp?crnumber=" + changeRequest.getCRNumber() + "\">Download File</a>");
                                        out.write("</td>");

                                        out.write("<td>");
                                        if ((testInstallation != null) && (user.getCategory().compareToIgnoreCase("Support") != 0)) {
                                            out.write("   <a href=\"QA_disassembleBuild.jsp?crnumber=" + changeRequest.getCRNumber() + "\" onclick=\"return confirm('Are you sure you want to disassemble this Build?')\">Disassemble Build</a>");
                                        }
                                        out.write("</td>");
                                        out.write("</tr>");

                                    }
                                }
                            %>
        </TR>
    </table>
</td></tr>
</table>

<%}%>

<%@include file="../html/bottom.html"%>
