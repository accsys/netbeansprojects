<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }
  
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest(request.getParameter("crnumber"));
  }

  // Can we disassemble this testInstallation?
  LinkedList buildInstallations = fileContainer.getBuildInstallations(fileContainer.getTestInstallation(request.getParameter("crnumber")));
  if (buildInstallations.size()>0){
     String message = "This Test Installation is still part of Builds.  You must disassemble the builds first.";
     response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message="+message);
     return;
  }

    History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DISSASEMBLETESTINSTALLATION, fileContainer.getTestInstallation(request.getParameter("crnumber")).getName()));
    fileContainer.dismantleTestInstallation(fileContainer.getTestInstallation(request.getParameter("crnumber")), user, true);
    fileContainer.save();

        // Return to previous page
        response.sendRedirect("QA.jsp");
%>
