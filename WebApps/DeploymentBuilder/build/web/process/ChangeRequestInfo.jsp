<!-- This JSP displays the detailed information of a given Change Request -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="java.io.*" %>
<%


  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest(request.getParameter("crnumber"));
  }
%>



    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Change Request Information</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>

<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Change Request Detail -->
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
		<tr><TD>
<%              if ( changeRequest == null){
                out.write("<tr><td><br><b>Invalid Change Request</b></td></tr>");
                } else{
                out.write("<tr><td><br><b>Change Request: "+changeRequest.getCRNumber()+"</b></td></tr>");
                out.write("<tr><TD>");
                out.write(      "<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
                out.write(      "<tr bgcolor=\"#f0f0f0\"><td width=\"50%\">Synopsis</td><td width=\"50%\">Description</td></tr>");
                out.write(      "<tr><td>"+fileContainer.formatChangeRequestSynopsisInHTML(  changeRequest.getSynopsis() )+"</td><td>"+changeRequest.getDescription()+"</td></tr>");
                out.write(      "</table>");
                out.write("</td></tr>");
                out.write("<tr><TD>");
                out.write(      "<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
                out.write(      "<tr bgcolor=\"#f0f0f0\"><td width=\"12%\">Entered By</td><td width=\"13%\">Entered On</td><td width=\"17%\">Responsibility</td><td width=\"13%\">Status</td>");
                out.write(      "    <td width=\"15%\">Category</td><td width=\"15%\">Severity</td><td width=\"15%\">Type</td></tr>");
                out.write(      "<tr><td>"+changeRequest.getEnteredBy()+"</td><td>"+changeRequest.getEnteredOn()+"</td><td>"+changeRequest.getResponsibility()+"</td><td>"+changeRequest.getStatus()+"</td>");
                out.write(      "    <td>"+changeRequest.getCategory()+"</td><td>"+changeRequest.getSeverity()+"</td><td>"+changeRequest.getType()+"</td></tr>");
                out.write(      "</table>");
                out.write("</td></tr>");
                // Is this CR included in any Builds?
                TestInstallation testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());
                if (testInstallation != null){
                    if (fileContainer.getBuildInstallations(testInstallation).size()>0){
                       out.write("<tr><td>");
                       out.write("<b>Included in Build(s)</b></td><td>");
                       for (int bi=0;bi<fileContainer.getBuildInstallations(testInstallation).size();bi++){
                         out.write(((BuildInstallation)fileContainer.getBuildInstallations(testInstallation).get(bi)).getBuildName()+"<br>");
                       }
                       out.write("</td>");
                    }   
                }
                }
%>
		</td></tr>
	</table>        


<%@include file="../html/bottom.html"%>
