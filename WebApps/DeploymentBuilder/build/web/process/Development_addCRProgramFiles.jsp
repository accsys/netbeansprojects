<!-- This JSP displays the detailed information of a given Change Request -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%

   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }
  
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest(request.getParameter("crnumber"));
  }
    TestInstallation testInstallation = fileContainer.getTestInstallation(request.getParameter("crnumber"));

  session.setAttribute("crnumber", new String(changeRequest.getCRNumber()));
%>



    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Add Multiple Program Files to a TestInstallation</H3>
            <H4><b>Note: The Applet used to allow for multiple uploads does NOT refresh the page after an upload.<br>
                Please ensure that the correct files were in fact uploaded by pressing <i>F5</I>.<br>
                <br>Wherever files were already in use, these files will not be attached to your Change Request.<br>
                A complete list of these exceptions will be e-mailed to you.</B></H4>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>

<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Change Request Detail -->

 <BR>
  <applet
		title="JUpload"
		name="JUpload"
		code="com.smartwerkz.jupload.classic.JUpload"
		codebase="../"
		archive="dist/jupload.jar,
				dist/commons-codec-1.3.jar,
				dist/commons-httpclient-3.0-rc4.jar,
				dist/commons-logging.jar,
				dist/skinlf/skinlf-6.2.jar"
			width="640"
			height="480"
			mayscript="mayscript"
			alt="JUpload by www.jupload.biz">

		<param name="Gui.FileChooser.Preview.Enabled" value="false">
		<param name="Gui.FileChooser.Preview.Show" value="false">
		<param name="Gui.FileChooser.Preview.Smooth" value="false">
		<param name="Gui.ServerResponse.Enable" value="true">
		<param name="Gui.Status.ShowSuccessDialog" value="true">
		<param name="Gui.Status.ShowPanel" value="true">
		<param name="Upload.Http.MaxRequestSize" value="-1">
		<param name="Files.Filter.Folders" value="true">
		<param name="Gui.ContextMenu.Files" value="">
		<param name="Gui.ContextMenu.General" value="">
		<param name="Upload.MaxTotalFileCount" value="-1">
		<param name="Upload.MaxFileSize" value="-1">
		<param name="Upload.MaxTotalFileSize" value="-1">

    
    <!-- Java Plug-In Options -->


    <!-- Target links -->
    <%out.write("<param name=\"Upload.URL.Action\" value=\"process/addQAProgramFiles.jsp?crnumber="+request.getParameter("crnumber")+"&loginName="+user.getLoginName()+"\">");%>
 <!--<param name="actionURL" value="addQAProgramFiles.jsp">-->
 <!param name="imageURL" value="JUpload.gif">

 <!-- Colors -->
 <param name="backgroundColor" value="#c0c0c0">
 <param name="mainSplitpaneLocation" value="370">
<PARAM NAME="maxTotalRequestSize" VALUE="-1">
<PARAM NAME="maxTotalRequestSizeTitle" VALUE="File too large">
<PARAM NAME="maxTotalRequestSizeWarning" VALUE="File too large for upload">
  <PARAM NAME="maxFreeSpaceOnServer" VALUE="-1">

 <!-- Switches -->
<!-- <param name="checkResponse" value="true"> -->
 <param name="showSuccessDialog" value="true">
 <param name="fixJakartaBug" value="true">

 <!-- IF YOU HAVE PROBLEMS, CHANGE THIS TO TRUE BEFORE CONTACTING SUPPORT -->
 <param name="debug" value="false">

 Your browser does not support applets. Or you have disabled applet in your options.
 To use this applet, please install the newest version of Sun's java. You can get it from <a href="http://www.java.com/">java.com</a>


 </applet>

        <BR>    
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><b><%out.write("<a href=\"SaveContainer.jsp\">Save latest changes</a>");%></b><br></td></tr>
        <tr><td>Current files attached to Change Request: <b><%out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a>");%></b></td>
        <tr><td>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" width="100%">
                <tr><%@ include file="_displayQAFileDetail.jsp" %></tr>
                </table>
        </td></tr>
        <tr><td>
                <%
                    LinkedList bookingItems = fileContainer.getBookingItemsFor(changeRequest);
                    if (bookingItems.size()>0){

                        out.write("<br><b>Current Reminders logged against Change Request: <a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a></b></td>");
                        out.write("<br><table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"70%\">");
                        out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">CR Number</td><td width=\"25%\">Synopsis</td><td width=\"35%\">Required Files</td><td  width=\"20%\">Booked by</td><td  width=\"10%\">Action</td></tr>");
                        for (int bi=0;bi<bookingItems.size();bi++){
                            BookingItem bookingItem = (BookingItem)bookingItems.get(bi);
                            out.write("<tr>");
                            out.write("<td"+fileContainer.getChangeRequestBackgroundColor(changeRequest)+"><a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\">"+bookingItem.getChangeRequest().getCRNumber()+"</a></td>");
                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(bookingItem.getChangeRequest().getSynopsis())+"</td>");
                            String requiredFileDetail = fileContainer.getRequiredFilesWithDetailInHTML(bookingItem);
                            out.write("<td>"+requiredFileDetail+"</td>");
                            out.write("<td>"+bookingItem.getUser().getName()+"</td>");
                            out.write("<td>");
                            out.write("  <br><a href=\"Reminders_deleteReminder.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" onclick=\"return confirm('Are you sure you want to cancel this Reminder?')\"><b>Cancel</b></a>");
                            out.write("</td>");
                            out.write("</tr>");
                            }                            
                        
                        out.write("</table>");
                    }
                %>
                </tr></td>
                <tr><td><FORM action=Development_createBuild.jsp method="post">
                            <input type="hidden" name="crnumber" value="<%out.write(changeRequest.getCRNumber());%>">
                            Required DB Version:<input type="text" name="requireddbversion" size="40" maxlength="40">
                            <br>Installation Instructions (Optional):<textarea cols="30" rows="3" name="installInstructions"></textarea>
                            <INPUT TYPE=SUBMIT VALUE="Create Build">
                </FORM></td></TR>
                </table>
        </table>   


<%@include file="../html/bottom.html"%>
