<jsp:useBean id="theBean" scope="page" class="com.brainysoftware.web.FileDownloadBean" />
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }
  
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  BuildInstallation buildInstallation = null;
  if (request.getParameter("buildname") != null){
    buildInstallation = fileContainer.getBuildInstallation(request.getParameter("buildname"));
    // Where is this file?
    String file = fileContainer.getArchiveFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+buildInstallation.getBuildName()+FileContainer.BuildInstallationExtension;
    History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DOWNLOADBUILDINSTALLATION, buildInstallation.getBuildName()));
    theBean.download(response, file);
  }


%>