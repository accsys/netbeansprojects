<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@ page import="org.apache.commons.fileupload.FileUpload" %>
<%@ page import="org.apache.commons.fileupload.DiskFileUpload" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>



<%
  ChangeRequest alternativeChangeRequest = null;
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  User user = null;
  // Get Change Request
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest((String)request.getParameter("crnumber"));
  }
  // Get User loginName
  if (request.getParameter("loginName") != null){
    user = new User(request.getParameter("loginName"));
  }

    String savePath = FileContainer.getTemporaryFolder().getAbsolutePath();  
    LinkedList files = new LinkedList(); // List of files uploaded
    LinkedList filesAlreadyInUse = new LinkedList(); // List of files uploaded

		int sizeThresholdUploadFileMemory = 1;
		
		// maximum size before a FileUploadException will be thrown
		int maxUploadFileSize = 20*1000*1024;
		
		// save the file to the root of the drive where web server is
		// running; change as appropriate
		String COMPLETE_UPLOAD_DIRECTORY = savePath;
		
		// the location for temporary saving uploaded files that exceed 
		// the threshold sizeThresholdUploadFileMemory 
		String tempDirectory = FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+"repository";
                // Make sure this folder exist
                new File(tempDirectory).mkdirs();
	
		// Check that we have a file upload request
		boolean isMultipart;
		isMultipart = FileUpload.isMultipartContent(request);

		if (isMultipart == true)
		{
			// Create a new file upload handler
			DiskFileUpload upload = new DiskFileUpload();

			// Set upload parameters
			upload.setSizeThreshold(sizeThresholdUploadFileMemory);
			upload.setSizeMax(maxUploadFileSize);
			upload.setRepositoryPath(tempDirectory);

			// Parse the request
			// returns a list of "FileItem"
			List items = upload.parseRequest(request);


			// Process the uploaded items
			Iterator iter = items.iterator();
			while (iter.hasNext())
			{
				FileItem fi = (FileItem) iter.next();
				
				String fieldName = fi.getFieldName();
				boolean isFormField = fi.isFormField(); 
				
				if (isFormField == true)
				{

					// Process a regular form field
					String value = fi.getString();

				}  // end of IF
				else
				{

					String filename = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();

					// save to file
					String uploadFilename; 
					File uploadFile;

					uploadFilename = COMPLETE_UPLOAD_DIRECTORY + FileContainer.getOSFileSeperator() + filename;
					uploadFile = new File(uploadFilename);
					
					fi.write(uploadFile);
                                        files.add(uploadFile);
					System.out.println("saved as ["+uploadFilename+"]");
					
					//FileUploadService.processUploadedPhoto(uploadFilename);
					
				}  // end of ELSE
			}  // end of WHILE

		}  // end of IF
		else
		{
		} // end of ELSE


    // Now we need to register this file in the FileContainer
    if (files.size()==0){
            response.sendRedirect("Development_addCRReportFiles.jsp?crnumber="+changeRequest.getCRNumber());
    }

    Iterator iter = files.iterator();

    // What type of file is this?
    int installType = InstallGroup.REPORT;

    // Now let's print the uploaded files
    while (iter.hasNext()){
        File upFile = (File)iter.next();
        System.out.println("File :"+upFile.getName());

        File uploadedFile = new File(savePath+FileContainer.getOSFileSeperator()+upFile.getName());
        QAFile qaFile = new QAFile(uploadedFile, installType);
        // Can we use this file, or is it already used in another CR?
        if (fileContainer.canLinkFileTo(qaFile, changeRequest, user)){
            TestInstallation testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());
            if (testInstallation == null){
                testInstallation = new TestInstallation(changeRequest.getCRNumber(), changeRequest);
            }

            // Register
            fileContainer.registerQAFile(qaFile, testInstallation);
            //fileContainer.save();
        } else { 
        filesAlreadyInUse.add(qaFile);
//        alternativeChangeRequest = fileContainer.getTestInstallation(qaFile).getChangeRequest();
        }


    } 

    // Are there any files that had problems, i.e. already assigned to another CR?
    if(filesAlreadyInUse.size() == 0){
        // Return to previous page
        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        String newLocn = "Development_addCRFile.jsp?crnumber="+changeRequest.getCRNumber();
        response.setHeader("Location",newLocn);

//        response.sendRedirect("Development_addCRFile.jsp?crnumber="+changeRequest.getCRNumber());
    } else
    {
%>
<%@include file="../html/top.html"%>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Add a file to a TestInstallation</H3>
        <BR>

<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
    <%  Iterator iter2 = filesAlreadyInUse.iterator(); 
        while (iter2.hasNext()){
            QAFile usedFile = (QAFile)iter2.next();
            out.write("<br><i>"+usedFile.getFile().getName()+
                      "</i> is already in use by another Change Request:<b><a href=\"ChangeRequestInfo.jsp?crnumber="+fileContainer.getTestInstallation(usedFile).getChangeRequest().getCRNumber()+"\">"+fileContainer.getTestInstallation(usedFile).getChangeRequest().getCRNumber()+"</a></b>");
        }
    
    out.write("</CENTER>");
    }
%>
<%@include file="../html/bottom.html"%>
