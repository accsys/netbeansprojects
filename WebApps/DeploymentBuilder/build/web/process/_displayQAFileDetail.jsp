<%
// Now we list the associated files, per group type
out.write("<td><font size=\"-1\"><dl>");
int counter;
// TYPE = PROGRAM
java.util.LinkedList programFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.PROGRAM);
if (programFiles.size()>0){
    out.write("<dt><b>Program Files</b> ["+programFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<programFiles.size(); counter++){
        if (((QAFile)programFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)programFiles.get(counter)).getFile().getName()+"  ["+((QAFile)programFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)programFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = SCRIPT
java.util.LinkedList scriptFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.SCRIPT);
if (scriptFiles.size()>0){
    out.write("<dt><b>Script</b> ["+scriptFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<scriptFiles.size(); counter++){
        if (((QAFile)scriptFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)scriptFiles.get(counter)).getFile().getName()+"  ["+((QAFile)scriptFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)scriptFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}

// TYPE = COMMS
LinkedList commsFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.COMMS);
if (commsFiles.size()>0){
    out.write("<dt><b>Comms</b> ["+commsFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<commsFiles.size(); counter++){
        if (((QAFile)commsFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)commsFiles.get(counter)).getFile().getName()+"  ["+((QAFile)commsFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)commsFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = UTILITIES
LinkedList utilityFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.UTILITY);
if (utilityFiles.size()>0){
    out.write("<dt><b>Utilities</b> ["+utilityFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<utilityFiles.size(); counter++){
        if (((QAFile)utilityFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)utilityFiles.get(counter)).getFile().getName()+"  ["+((QAFile)utilityFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)utilityFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = DLL
LinkedList dllFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.DLL);
if (dllFiles.size()>0){
    out.write("<dt><b>Dll's</b> ["+dllFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<dllFiles.size(); counter++){
        if (((QAFile)dllFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)dllFiles.get(counter)).getFile().getName()+"  ["+((QAFile)dllFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)dllFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = XML
LinkedList xmlFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.XML);
if (xmlFiles.size()>0){
    out.write("<dt><b>XML's</b> ["+xmlFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<xmlFiles.size(); counter++){
        if (((QAFile)xmlFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)xmlFiles.get(counter)).getFile().getName()+"  ["+((QAFile)xmlFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)xmlFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}

// TYPE = Report
LinkedList reportFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.REPORT);
if (reportFiles.size()>0){
    out.write("<dt><b>Reports</b> ["+reportFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<reportFiles.size(); counter++){
        if (((QAFile)reportFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)reportFiles.get(counter)).getFile().getName()+"  ["+((QAFile)reportFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)reportFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = CONFIG
LinkedList configFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.CONFIG);
if (configFiles.size()>0){
    out.write("<dt><b>Config</b> ["+configFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<configFiles.size(); counter++){
        if (((QAFile)configFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)configFiles.get(counter)).getFile().getName()+"  ["+((QAFile)configFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)configFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
// TYPE = SYSTEM
LinkedList systemFilesFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.SYSTEM);
if (reportFiles.size()>0){
    out.write("<dt><b>System Files</b> ["+systemFilesFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<systemFilesFiles.size(); counter++){
        if (((QAFile)systemFilesFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)systemFilesFiles.get(counter)).getFile().getName()+"  ["+((QAFile)systemFilesFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)systemFilesFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}

// TYPE = HELP
LinkedList helpFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.HELP);
if (helpFiles.size()>0){
    out.write("<dt><b>Help Files</b> ["+helpFiles.size()+" files]</dt><dd></dd>");
    for (counter=0; counter<helpFiles.size(); counter++){
        if (((QAFile)helpFiles.get(counter)).getFile().exists()){
            out.write("<dt></dt><dd>"+((QAFile)helpFiles.get(counter)).getFile().getName()+"  ["+((QAFile)helpFiles.get(counter)).getFile().length() +" bytes]</dd>");
        } else {
            out.write("<dt></dt><dd>"+((QAFile)helpFiles.get(counter)).getFile().getName()+" </dd>");
        }
    }
}
out.write("</dl></font></td>");
%> 
