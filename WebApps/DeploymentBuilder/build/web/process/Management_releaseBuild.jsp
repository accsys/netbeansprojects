<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>


<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - Release Build</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>
    <%
        User user = (User) session.getAttribute("user");

        if (user == null) {
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        } else {

            // Only allow QA and Management into this screen
            if (user.getCategory().compareToIgnoreCase("Management") != 0) {
                response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
            }

            FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
            BuildInstallation buildInstallation = null;
            if (request.getParameter("buildname") != null) {
                buildInstallation = fileContainer.getBuildInstallation(request.getParameter("buildname"));
            }

    %>
    <!-- Hyper Links -->
    <%@ include file="../html/top_hyperlinks.txt" %>

    <!-- Existing BuildInstallations -->
    <br>
    <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
        <tr><td><br>You are about to Release BuildInstallation: <b><%out.write(buildInstallation.getBuildName());%></b>.<br> 
                Please note that Test Installations that are not shared by other Build Installations, will be disassemble and their
                corresponding files will be made available for new Test Installations.</td></tr>
        <tr><td><br></td></tr>
        <tr><TD><b>This Build Installation consists of the following Test Installations:</b></TD></TR>
        <tr><td><%out.write(fileContainer.getTestInstallationNames(buildInstallation, ", "));%></TD></TR>
        <tr><td><br></td></tr>
        <tr><TD><b>The complete list of files included in this Build Installation:</b></TD></TR>
        <tr><td><font size="-1"><%out.write(fileContainer.getFileNames(buildInstallation, "<br>"));%></font></TD></TR>
    </table>

    <br>
    <table>
        <tr>
            <td><FORM action=Management_releaseBuildAction.jsp method="post"><input type="hidden" name="buildname" value="<%out.write(buildInstallation.getBuildName());%>"><input type="submit" value="Release Build"></FORM></TD>
            <td><FORM action=Management.jsp method="post"><input type="submit" value="Go Back"></FORM></TD>
        </TR>
    </TABLE>
    <%}%>
    <%@include file="../html/bottom.html"%>