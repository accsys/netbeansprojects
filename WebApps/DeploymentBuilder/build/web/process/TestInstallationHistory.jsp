<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder</H3>
            <H4>- Test Installation History -</H4>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }

  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  History history = History.getInstance();
  BuildHistory buildHistory = BuildHistory.getInstance();

  LinkedList historyEvents = history.getEvents();
  LinkedList buildHistoryEvents = buildHistory.getEvents();


%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- History, per day -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" >
		<tr><td><br><b>Events</b></td></tr>
		<tr><TD>
				<table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
    
                                <% for (int i=historyEvents.size()-1; i>=0; i--){
                                        HistoryEvent event = (HistoryEvent)historyEvents.get(i);
                                        // Try to locate the appropriate CR
                                        TestInstallation testInstallation = FileContainer.getInstance(FileContainer.getContainerFile()).getTestInstallation(event.getObjectName());
                                        out.write("<tr><td>"+event.getTimeStamp()+"</td>");
                                        out.write("    <td>"+event.getUserName()+"</td>");
                                        out.write("    <td>"+event.getEventTypeString()+"</td>");

                                        if ( (testInstallation != null) && (event.getEventTypeString().indexOf("Create")>=0) ) {
                                          out.write("<td"+FileContainer.getInstance(FileContainer.getContainerFile()).getChangeRequestBackgroundColor(testInstallation.getChangeRequest())+">"+
                                                "<a href=\"ChangeRequestInfo.jsp?crnumber="+event.getObjectName()+"\">"+event.getObjectName()+"</a></td>");
                                        } else {
                                          out.write("    <td>"+event.getObjectName()+"</td>");
                                        }
                                    }
                                out.write("</tr></table>");%>
		</td></tr>
	</table>


<%@include file="../html/bottom.html"%>
