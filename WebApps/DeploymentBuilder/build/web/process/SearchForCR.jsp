<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Locate Change Request</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%
    String searchFor = "";
    if (request.getParameter("crsearchfor") != null) {
        searchFor = request.getParameter("crsearchfor");
    }

    FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
    BuildHistory buildHistory = BuildHistory.getInstance();
    System.out.println("buildHistory:"+buildHistory.getEvents().size());
    LinkedList buildHistoryEvents = buildHistory.getEvents();

%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Find Files -->
        <br>
        <FORM action=SearchForCR.jsp method="post">
            <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><b>Locate Change Request</b></TD></TR>
            <tr><td>Containing:<br><i>You can search for multiple phrases<br>by seperating phrases with commas</i></td>
                <td><input type="text" name="crsearchfor" size="35" maxlength="40"></td>
                <td><INPUT TYPE=SUBMIT VALUE="Find"></td>
            </tr>
            </TABLE>
        </FORM>
                <!-- All / Subset of CR's -->
                <br>
                <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
                <%if (searchFor.trim().length()==0){%>
                <tr><td><br><b>Full List of Change Requests</b></td></tr>
                <%} else {%>
                <tr><td><br><b>Result of search for [<%out.write(searchFor);%>]</b></td></tr>
                <%}%>
                <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                    <tr bgcolor="#f0f0f0">
                    <td width="10%">CR Number</td>
                    <td width="10%">Status</td>
                    <td width="20%">Synopsis</td>
                    <td width="32%">Description</td>
                    <td width="10%">Responsibility</td>
                    <td width="8%">Type</td>
                    <td width="10%">Resolved In</td></tr>
                                    
                    <tr>
                        <% if (searchFor.trim().length()>0){
            LinkedList searchResult = fileContainer.getCRsContaining(searchFor);

            // Traverse through 'incompleteCRs', building up the table on the Web Page
            int upperLimit = searchResult.size();
            for (int i=0; i<upperLimit; i++){
                ChangeRequest changeRequest = (ChangeRequest)searchResult.get(i);
                changeRequest.refreshFromDatabase();

                out.write("<tr>");
                out.write("<td"+fileContainer.getChangeRequestBackgroundColor(changeRequest)+"><a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a></td>");
                out.write("<td>"+changeRequest.getStatus()+"</td>");
                out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis())+"</td>");
                out.write("<td>"+changeRequest.getDescription()+"</td>");
                out.write("<td>"+changeRequest.getResponsibility()+"</td>");
                out.write("<td>"+changeRequest.getType()+"</td>");
                // CR Found in ...
                {
                    out.write("<td>");
                    // Does the CR exist in an existing build?
                    TestInstallation testInstall = fileContainer.getTestInstallation(changeRequest.getCRNumber());
                    LinkedList buildInstallations = fileContainer.getBuildInstallations(testInstall);
                    if (buildInstallations.size()>0){
                        for (int b=0;b<buildInstallations.size();b++){
                            out.write(((BuildInstallation)buildInstallations.get(b)).getBuildName()+"["+
                                    ((BuildInstallation)buildInstallations.get(b)).getRequiredDBVersion()+"]<br>");
                        }
                    }
                    // Did this CR exist somewhere in the build history?
                    Iterator iter = buildHistoryEvents.iterator();
                    while (iter.hasNext()){
                        BuildHistoryEvent event = (BuildHistoryEvent)iter.next();
						System.out.println("buildHistoryEvent:"+event.getBuildNameAndDescription());	
                        
                        if (event.containsChangeRequestNumber(changeRequest.getCRNumber())){
                            out.write(event.getBuildNameAndDescription()+"<p>Made:"+event.getTimeStamp()+"<br>");
                        }
                    }
                    out.write("</td>");
                }

                out.write("</tr>");
            }
                            }

                        %>
                    </tr>
                </table>
            </td></tr>
                </table>
        


<%@include file="../html/bottom.html"%>
