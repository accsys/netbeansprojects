<%@page contentType="text/html"%>
<%@include file="html/top.html"%>

<%-- Central Page to enter into the Administrative Functions --%>
<CENTER>
    <BR>
    <H3>Welcome to Deployment Builder</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>

    <% String loggedInUser = (String) session.getAttribute("loggedinuser");

        if (loggedInUser != null && loggedInUser.compareTo("true") != 0) {
            out.println("<BR>Incorrect login, please try again.");
        }
    %>

    <h1>Log In</h1>

    <form METHOD=POST id="login" ACTION="/DeploymentBuilder/process/WelcomeScreen.jsp">
        <table><tr><td><input id="username" NAME="userName" type="text" placeholder="Login Name" autofocus required></TD></tr>
            <tr><td><input id="password" name="password" type="password" placeholder="Password" required></td></tr>
            <tr><td><input type="submit" id="submit" value="Login"></td></TR></table>

    </form>




    <!--
    
        <FORM METHOD=POST ACTION="/DeploymentBuilder/process/WelcomeScreen.jsp">
            <BR><BR>
            <TABLE>
                <TR>
                    <TD>Login Name:</TD>
                    <TD><INPUT TYPE=text NAME=userName></TD>
                </TR>
                <TR>
                    <TD>Password:</TD>
                    <TD><INPUT TYPE=password NAME=password></TD>
                </TR>
                <TR>
                    <TD><INPUT TYPE=SUBMIT VALUE="Login"></TD>
                    <TD><INPUT TYPE=RESET></TD>
                </TR>
                <TABLE>
                    </FORM>
    -->

</CENTER>

<%@include file="html/bottom.html"%>
