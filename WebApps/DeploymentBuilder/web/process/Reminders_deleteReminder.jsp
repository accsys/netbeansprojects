<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        ChangeRequest changeRequest = null;
        if (request.getParameter("crnumber") != null) {
            changeRequest = new ChangeRequest(request.getParameter("crnumber"));
        }

        if (changeRequest == null) {
            String message = "Unable to find the given Change Request.  Please check the CR Number.";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        // Create new BookingItem
        BookingItem bookingItem = new BookingItem(changeRequest);
        bookingItem.deleteFromDatabase();

        // Return to previous page
        response.sendRedirect("Reminders.jsp");
    }
%>
<%@include file="../html/bottom.html"%>
