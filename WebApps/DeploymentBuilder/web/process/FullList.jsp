<!-- This JSP manages the 'Orphan' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Full List</H3>
            <H4>Complete list of Change Requests that has Test Installations</H4>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

    User user = null;
    if (session.getAttribute("user")!=null){
      user = (User)session.getAttribute("user");
    }
  
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }


  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  TestInstallation testInstallation = null;
%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>

<!-- Existing TestInstallations -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
		<tr><td><br><b>Existing TestInstallations</b></td></tr>
		<tr><TD>
				<table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
				<tr bgcolor="#f0f0f0"><td width="15%">CR Number</td><td width="20%">CR Status</td><td width="20%">Deployment Status</td><td width="40%">Resources</td></tr>
                                <% 
                                    // Scan FileContainer for TestInstallations with a status of BUILT
                                    LinkedList testInstallations = fileContainer.getTestInstallations();
                                    for (int i=0; i<testInstallations.size(); i++){
                                        testInstallation = (TestInstallation)testInstallations.get(i);
                                        if (testInstallation.getStatus() == TestInstallation.BUILT){
                                            ChangeRequest changeRequest = testInstallation.getChangeRequest();
                                            out.write("<tr>");
                                            out.write("<td"+fileContainer.getChangeRequestBackgroundColor(changeRequest)+"><a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a><br>");



                                                // Are there any items waiting for the completion of this one?
                                                LinkedList bookingItems2 = fileContainer.getBookingItemsWaitingFor(testInstallation);
                                                int cntr = 0;
                                                for (int bi=0;bi<bookingItems2.size();bi++){
                                                  cntr++;
                                                  BookingItem bookingItem = (BookingItem)bookingItems2.get(bi);
                                                  out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                                            "title=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\"><img src=\"../images/WaitingForMe.png\" border=\"0\" width=\"42\" height=\"42\" "+
                                                            "alt=\"CR "+bookingItem.getChangeRequest().getCRNumber()+" is waiting for the completion of this TestInstallation!\""+
                                                            "hspace=\"6\" vspace=\"2\"></a>");
                                                  if (cntr>1){
                                                    cntr=0;
                                                    out.write("<br>");
                                                    }
                                                }                                            out.write("</td>");
                                            out.write("<td>"+changeRequest.getStatus()+"</td>");
                                            out.write("<td>");
                                            if (fileContainer.getBuildInstallations(testInstallation).size()>0){
                                               out.write("Included in Build:<br>");
                                               for (int bi=0;bi<fileContainer.getBuildInstallations(testInstallation).size();bi++){
                                                 out.write(((BuildInstallation)fileContainer.getBuildInstallations(testInstallation).get(bi)).getBuildName()+"<br>");
                                               }
                                            }   
                                            out.write("</td>");
                                            %><%@ include file="_displayQAFileDetail.jsp" %><%            
                                            out.write("<td>"+testInstallation.getChangeRequest().getRequiredDBVersion()+"</td>");
                                            out.write("</tr>");
                                        }
                                    }
                                %>
                                </TR>
				</table>
		</td></tr>
	</table>
        
<!-- Waiting for other TestInstallations -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><br><b>Change Requests waiting for Files</b></td></tr>
            <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                <tr bgcolor="#f0f0f0"><td width="10%">CR Number</td><td width="25%">Synopsis</td><td width="45%">Required Files</td><td  width="20%">Booked by</td></tr>
                <tr>
                <% 
                    LinkedList bookedItems = fileContainer.getExistingBookingItems();
                    Iterator iter = bookedItems.iterator();
                    while (iter.hasNext()){
                        BookingItem bookingItem = (BookingItem)iter.next();
                        bookingItem.refreshFromDatabase();
                        // If this bookingItem belongs to a ChangeRequest that already has a TestInstallation, 
                        // remove it from the database
                        if (fileContainer.getTestInstallationUsingFile(bookingItem.getChangeRequest().getCRNumber()) != null){
                            bookingItem.deleteFromDatabase();
                        } else {
                            out.write("<tr>");
                            out.write("<td"+fileContainer.getChangeRequestBackgroundColor(bookingItem.getChangeRequest())+"><a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\">"+bookingItem.getChangeRequest().getCRNumber()+"</a>");

                              out.write("<img title=\"This CR is waiting for the completion of another CR.  See Reminders.\" src=\"../images/waitingForOthers.gif\" border=\"0\" width=\"21\" height=\"21\" "+
                                        "alt=\"This CR is waiting for the completion of another CR.  See Reminders.\""+
                                        "hspace=\"6\" vspace=\"2\">");
                            out.write("</td>");

                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(bookingItem.getChangeRequest().getSynopsis())+"</td>");
                            String requiredFileDetail = fileContainer.getRequiredFilesWithDetailInHTML(bookingItem);
                            out.write("<td><font size=\"-1\">"+requiredFileDetail+"</font></td>");
                            out.write("<td>"+bookingItem.getUser().getName()+"</td>");
                            out.write("</tr>");
                            }
                        }
                %>
                </tr>
                </table>
            </td></tr>
	</table>


<%@include file="../html/bottom.html"%>
