<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Change Request Bookings</H3>
            <H4>Bookings are used to reserve files that are currently assigned to other Change Requests</H4>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

    User user = null;
    if (session.getAttribute("user")!=null){
      user = (User)session.getAttribute("user");
    }
  
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }

  // Only allow Developers and Management into this screen
  if ((user.getCategory().compareToIgnoreCase("Development")!=0)
      && (user.getCategory().compareToIgnoreCase("Management")!=0)){
            response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
    }

  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Existing Bookings -->
        <br>
        <FORM action=Bookings_addBooking.jsp method="post">
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><b>Create a new booking</b></TD></TR>
            <tr><td>Change Request Number<br><font size="-1">(Please unsure correct spelling)</FONT></td>
                <td><input type="text" name="crnumber" size="5" maxlength="5"></td>
                <td>Files that will be required for this Change Request<br>
                    <font size="-1">(Seperate files with commas)</FONT></TD>
                <td><textarea name="filesused" cols="30" rows="5"></textarea></TD>
            </TR>
            <tr><td><INPUT TYPE=SUBMIT VALUE="Create"></td></TR>
        </TABLE>
        </FORM>

        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><br><b>Change Requests waiting for Files</b></td></tr>
            <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                <tr bgcolor="#f0f0f0"><td width="10%">CR Number</td><td width="25%">Synopsis</td><td width="35%">Required Files</td><td  width="20%">Booked by</td><td  width="10%">Action</td></tr>
                <tr>
                <% 
                    LinkedList bookedItems = fileContainer.getExistingBookingItems();
                    Iterator iter = bookedItems.iterator();
                    while (iter.hasNext()){
                        BookingItem bookingItem = (BookingItem)iter.next();
                        bookingItem.refreshFromDatabase();
                        out.write("<tr>");
                        out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\">"+bookingItem.getChangeRequest().getCRNumber()+"</a></td>");
                        out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(bookingItem.getChangeRequest().getSynopsis())+"</td>");
                        String requiredFileDetail = fileContainer.getRequiredFilesWithDetailInHTML(bookingItem);
                        out.write("<td>"+requiredFileDetail+"</td>");
                        out.write("<td>"+bookingItem.getUser().getName()+"</td>");
                        out.write("<td>");
                        out.write("  <a href=\"Bookings_editBooking.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\"><b>Edit</b></a>");
                        out.write("  <br><a href=\"Bookings_deleteBooking?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" onclick=\"return confirm('Are you sure you want to cancel this Booking?')\"><b>Cancel</b></a>");
                        out.write("</td>");
                        out.write("</tr>");
                        }
                %>
                </tr>
                </table>
            </td></tr>
	</table>
<!-- Another button.  This time to create all inter-dependent links in order for one
     to be able to combine Change Requests easier. -->
        <FORM action=Bookings_possibleCombinations.jsp method="post">
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td width="80%"><b>Generate possible combinations of Change Requests</b></TD>
                <td width="20%"><INPUT TYPE=SUBMIT VALUE="Create"></td></TR>
        </TABLE>
        </FORM>


<%@include file="../html/bottom.html"%>
