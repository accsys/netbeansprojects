<%@page contentType="text/html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%@include file="../html/top.html"%>


<CENTER>
    <BR>
    <H3>Accsys Deployment Builder</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>

    <!-- Hyper Links -->
    <%@ include file="../html/top_hyperlinks.txt" %>


    <%

        User user = (User) session.getAttribute("user");

        if (user == null) {
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        } else {

            out.write("<H4>'" + user.getCategory() + "' is not allowed in this section");
        }
    %>


    <%@include file="../html/bottom.html"%>
