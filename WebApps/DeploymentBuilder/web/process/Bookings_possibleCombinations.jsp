<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Combining of Change Requests</H3>
            <H4>Bookings are used to reserve files that are currently assigned to other Change Requests</H4>
            <H4>When more than one Booking(Reminder) requires the same file(s), why not combine them?</H4>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

    User user = null;
    if (session.getAttribute("user")!=null){
      user = (User)session.getAttribute("user");
    }
  
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }

  // Only allow Developers and Management into this screen
  if ((user.getCategory().compareToIgnoreCase("Development")!=0)
      && (user.getCategory().compareToIgnoreCase("Management")!=0)){
            response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
    }

  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Existing Bookings -->
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><br><b>Possible Combinations of Change Requests</b></td></tr>
            <tr><TD>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" summary=""  width="100%">
                <tr bgcolor="#f0f0f0"><td width="20%">CR Number</td><td width="30%">Other CRs</td><td width="50%">Required Files</td></tr>
                <tr>
                <% 
                    // For each booking item, get a combined version.
                    LinkedList combinedChangeRequestList = new LinkedList();
                    LinkedList alreadyBookedItems = fileContainer.getExistingBookingItems();


                    Iterator iter = alreadyBookedItems.iterator();
                    while (iter.hasNext()){
                        BookingItem bookingItem = (BookingItem)iter.next();
                        bookingItem.refreshFromDatabase();

                        // Generate a CombinedChangeRequest
                        CombinedChangeRequest combinedCR = new CombinedChangeRequest(bookingItem,alreadyBookedItems);
                        
                        boolean addToList = true;
                        // If this is the first entry, add it automatically
                        if (combinedChangeRequestList.size()==0){
                            addToList = true;
                        }
                        else {
                            // Add to list
                            Iterator listIter = combinedChangeRequestList.iterator();
                            while (listIter.hasNext()){
                                CombinedChangeRequest crFromList = (CombinedChangeRequest)listIter.next();
                                System.out.println("\nChecking for "+combinedCR.toString()+" in "+crFromList.toString());
                                if (crFromList.overlaps(combinedCR)){
                                    addToList = false;
                                    System.out.println("Yip, its here!");

                                } else {
                                    System.out.println("Nope, its not!");
                                }
                            }
                        }

                        if (addToList){
                            combinedChangeRequestList.add(combinedCR);
                            out.write("<tr>");
                            out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\">"+bookingItem.getChangeRequest().getCRNumber()+"</a></td>");
                            out.write("<td>"+combinedCR.toString()+"</td>");
                            String requiredFileDetail = fileContainer.getRequiredFilesWithDetailInHTML(combinedCR);
                            out.write("<td>"+requiredFileDetail+"</td>");
                            out.write("</tr>");
                            }
                       }
                %>
                </tr>
                </table>
            </td></tr>
	</table>



<%@include file="../html/bottom.html"%>
