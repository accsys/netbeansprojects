<!-- This JSP manages the 'Possible Problems' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Possible Problems</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>
<%

   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }

  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  TestInstallation testInstallation = null;

    DatabaseObject databaseObject = DatabaseObject.getInstance();
    databaseObject.setConnectInfo_TimeSheetODBC();

%>
<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>

<!-- CR's that are overdue -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
		<tr><td><br><b>High Priority CRs that are Overdue</b></td></tr>
		<tr><TD>
                                <% 
				out.write("<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
				out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">CR Number</td><td width=\"10%\">Priority</td><td width=\"45%\">Synopsis</td><td width=\"10%\">Estimated Time</td><td width=\"10%\">Actual Time</td><td width=\"15%\">Responsibility</td></tr>");
                                    // Scan FileContainer for Change Requests that are overdue
                                    LinkedList overdueCRs = fileContainer.getCRsThatExceedEstimatedTime(2);
                                    if (overdueCRs.size()>0){
                                        for (int i=0; i<overdueCRs.size(); i++){
                                            ExtendedChangeRequest extendedChangeRequest = (ExtendedChangeRequest)overdueCRs.get(i);
                                            extendedChangeRequest.refreshFromDatabase();
                                            out.write("<tr>");
                                            out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+extendedChangeRequest.getCRNumber()+"\">"+extendedChangeRequest.getCRNumber()+"</a><br>");
                                                // Are there any items waiting for the completion of this one?
                                                LinkedList bookingItems = fileContainer.getBookingItemsFor(extendedChangeRequest);
                                                int cntr = 0;
                                                for (int bi=0;bi<bookingItems.size();bi++){
                                                  cntr++;
                                                  BookingItem bookingItem = (BookingItem)bookingItems.get(bi);
                                                  out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                                            "title=\"A reminder already exists for this CR.\"><img src=\"../images/waitingForOthers.gif\" border=\"0\"  "+
                                                            "alt=\"A reminder already exists for this CR.\""+
                                                            "hspace=\"6\" vspace=\"2\"></a>");
                                                  if (cntr>1){
                                                    cntr=0;
                                                    out.write("<br>");
                                                    }
                                                }                                               
                                            out.write("</td>");
                                            out.write("<td>"+extendedChangeRequest.getPriority()+"</td>");
                                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(extendedChangeRequest.getSynopsis())+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getEstimatedHours()+"</td>");   
                                            out.write("<td>"+extendedChangeRequest.getActualHours()+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getResponsibility()+"</td>");
                                            out.write("</tr>");
                                        }
                                    }
                                out.write("</TR>");
				out.write("</table>");
                                %>
		</td></tr>
	</table>
        
<!-- New CR's that have not been worked on, yet -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
		<tr><td><br><b>High Priority CRs that might be Overdue</b></td></tr>
		<tr><TD>
                                <% 
				out.write("<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
				out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">CR Number</td><td width=\"10%\">Priority</td><td width=\"40%\">Synopsis</td><td width=\"15%\">Entered On</td><td width=\"10%\">Status</td><td width=\"15%\">Responsibility</td></tr>");
                                    // Scan FileContainer for Change Requests that are overdue
                                    LinkedList unattendedCRs = fileContainer.getCRsThatRequiresAttention(2, 7);
                                    if (unattendedCRs.size()>0){
                                        for (int i=0; i<unattendedCRs.size(); i++){
                                            ExtendedChangeRequest extendedChangeRequest = (ExtendedChangeRequest)unattendedCRs.get(i);
                                            extendedChangeRequest.refreshFromDatabase();
                                            out.write("<tr>");
                                            out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+extendedChangeRequest.getCRNumber()+"\">"+extendedChangeRequest.getCRNumber()+"</a><br>");
                                                // Are there any items waiting for the completion of this one?
                                                LinkedList bookingItems = fileContainer.getBookingItemsFor(extendedChangeRequest);
                                                int cntr = 0;
                                                for (int bi=0;bi<bookingItems.size();bi++){
                                                  cntr++;
                                                  BookingItem bookingItem = (BookingItem)bookingItems.get(bi);
                                                  out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                                            "title=\"A reminder already exists for this CR.\"><img src=\"../images/waitingForOthers.gif\" border=\"0\"  "+
                                                            "alt=\"A reminder already exists for this CR.\""+
                                                            "hspace=\"6\" vspace=\"2\"></a>");
                                                  if (cntr>1){
                                                    cntr=0;
                                                    out.write("<br>");
                                                    }
                                                }                                               
                                            out.write("</td>");
                                            out.write("<td>"+extendedChangeRequest.getPriority()+"</td>");
                                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(extendedChangeRequest.getSynopsis())+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getEnteredOn()+"</td>");   
                                            out.write("<td>"+extendedChangeRequest.getStatus()+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getResponsibility()+"</td>");
                                            out.write("</tr>");
                                        }
                                    }
                                out.write("</TR>");
				out.write("</table>");
                                %>
		</td></tr>
	</table>
        <!-- Other CR's, long overdue -->
        <br>
	<table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
		<tr><td><br><b>High Priority CRs that might be Overdue</b></td></tr>
		<tr><TD>
                                <% 
				out.write("<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
				out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">CR Number</td><td width=\"10%\">Priority</td><td width=\"40%\">Synopsis</td><td width=\"15%\">Entered On</td><td width=\"10%\">Status</td><td width=\"15%\">Responsibility</td></tr>");
                                    // Scan FileContainer for Change Requests that are overdue
                                    LinkedList unattendedCRs2 = fileContainer.getCRsThatRequiresAttention(3, 35);
                                    if (unattendedCRs2.size()>0){
                                        for (int i=0; i<unattendedCRs2.size(); i++){
                                            ExtendedChangeRequest extendedChangeRequest = (ExtendedChangeRequest)unattendedCRs2.get(i);
                                            extendedChangeRequest.refreshFromDatabase();
                                            out.write("<tr>");
                                            out.write("<td><a href=\"ChangeRequestInfo.jsp?crnumber="+extendedChangeRequest.getCRNumber()+"\">"+extendedChangeRequest.getCRNumber()+"</a><br>");
                                                // Are there any items waiting for the completion of this one?
                                                LinkedList bookingItems = fileContainer.getBookingItemsFor(extendedChangeRequest);
                                                int cntr = 0;
                                                for (int bi=0;bi<bookingItems.size();bi++){
                                                  cntr++;
                                                  BookingItem bookingItem = (BookingItem)bookingItems.get(bi);
                                                  out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" "+
                                                            "title=\"A reminder already exists for this CR.\"><img src=\"../images/waitingForOthers.gif\" border=\"0\"  "+
                                                            "alt=\"A reminder already exists for this CR.\""+
                                                            "hspace=\"6\" vspace=\"2\"></a>");
                                                  if (cntr>1){
                                                    cntr=0;
                                                    out.write("<br>");
                                                    }
                                                }                                               
                                            out.write("</td>");
                                            out.write("<td>"+extendedChangeRequest.getPriority()+"</td>");
                                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(extendedChangeRequest.getSynopsis())+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getEnteredOn()+"</td>");   
                                            out.write("<td>"+extendedChangeRequest.getStatus()+"</td>");
                                            out.write("<td>"+extendedChangeRequest.getResponsibility()+"</td>");
                                            out.write("</tr>");
                                        }
                                    }
                                out.write("</TR>");
				out.write("</table>");
                                %>
		</td></tr>
	</table>

<%@include file="../html/bottom.html"%>
