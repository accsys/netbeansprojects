<!-- This JSP manages the 'Development' Screen -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>


<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - Management Overview</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>

    <%
        User user = (User) session.getAttribute("user");

        if (user == null) {
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        } else {

            // Only allow QA and Management into this screen
            if (user.getCategory().compareToIgnoreCase("Management") != 0) {
                response.sendRedirect("/DeploymentBuilder/process/NoRights.jsp");
            }

            FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
            // Let us refresh the list of current change request numbers in the FileContainer
            fileContainer.reloadChangeRequestNumbers();

            boolean selectAll = false;
            if ((request.getParameter("selectAll") != null) && (request.getParameter("selectAll").compareTo("true") == 0)) {
                selectAll = true;
            }

    %>
    <!-- Hyper Links -->
    <%@ include file="../html/top_hyperlinks.txt" %>

    <!-- Existing BuildInstallations -->
    <br>
    <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
        <tr><td><br><b>BuildInstallations</b></td></tr>
        <tr><TD>
                <%                out.write("<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
                    out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">Build Name</td><td width=\"15%\">Description</td><td width=\"30%\">ReadMe Contents</td><td width=\"10%\">Required Ver</td><td width=\"10%\">Updated Ver</td><td width=\"10%\">Download Installation</td><td width=\"15%\">Action</td></tr>");
                    // Scan FileContainer for TestInstallations with a status of BUILT
                    LinkedList buildInstallations = fileContainer.getBuildInstallations();
                    if (buildInstallations.size() > 0) {
                        for (int i = 0; i < buildInstallations.size(); i++) {
                            BuildInstallation buildInstallation = (BuildInstallation) buildInstallations.get(i);
                            out.write("<tr>");
                            out.write("<td>" + buildInstallation.getBuildName());
                            if (buildInstallation.getHTMLInstallationInstructions().length() > 0) {
                                out.write("<br><img src=\"../images/installationInstruction.bmp\" border=\"0\" width=\"32\" height=\"32\" "
                                        + "hspace=\"6\" vspace=\"2\">");
                                out.write("<br><b>Installation Instructions</b>");
                                out.write("<br>" + buildInstallation.getHTMLInstallationInstructions());
                            }
                            out.write("</td>");
                            out.write("<td>" + buildInstallation.getBuildDescription() + "</td>");
                            out.write("<td><font size=\"-1\">" + buildInstallation.getHTMLReadMe() + "</font></td>");
                            out.write("<td>" + buildInstallation.getRequiredDBVersion() + "</td>");
                            out.write("<td>" + buildInstallation.getNewDBVersion() + "</td>");
                            out.write("<td>");
                            out.write("    <a href=\"Management_downloadBuildFile.jsp?buildname=" + buildInstallation.getBuildName() + "\">Download File</a>");
                            out.write("</td>");
                            out.write("<td>");
                            if (buildInstallation != null) {
                                out.write("   <a href=\"Management_disassembleBuild.jsp?buildname=" + buildInstallation.getBuildName() + "\" onclick=\"return confirm('Are you sure you want to disassemble this Build?')\">Disassemble Build</a>");
                                out.write("   <br><a href=\"Management_releaseBuild.jsp?buildname=" + buildInstallation.getBuildName() + "\">Release Build</a>");
                            }
                            out.write("</td>");
                            out.write("</tr>");
                        }
                    }
                    out.write("</TR>");
                    out.write("</table>");
                %>
            </td></tr>
    </table>


    <!-- Existing TestInstallations -->
    <br>
    <FORM action=Management_createBuild.jsp method="post">
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
            <tr><td><br><b>TestInstallations (QA Complete)</b></td></tr>
            <tr><td><%
                if (selectAll) {
                    out.write("<b>Select ALL CR's</b> | <a href=\"Management.jsp?selectAll=false\"><i>Unselect ALL CR's</i></a>");
                }
                if (!selectAll) {
                    out.write("<a href=\"Management.jsp?selectAll=true\"><i>Select ALL CR's</i></a> | <b>Unselect ALL CR's</b>");
                }
                    %></td></tr>
            <tr>
            <tr><TD>
                    <%
                        out.write("<table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"100%\">");
                        out.write("<tr bgcolor=\"#f0f0f0\"><th width=\"10%\">CR Number</th><th width=\"20%\">Synopsis</th><th width=\"5%\">Included In</th><th width=\"10%\">Required Ver</th><th width=\"10%\">Download Installation</th><th width=\"10%\">Include in new Build</th><th width=\"30%\">ReadMe</th></tr>");
                        // Scan FileContainer for TestInstallations with a status of BUILT
                        int counter = 0;
                        LinkedList testInstallations = fileContainer.getTestInstallations();
                        if (testInstallations.size() > 0) {
                            for (int i = 0; i < testInstallations.size(); i++) {
                                TestInstallation testInstallation = (TestInstallation) testInstallations.get(i);
                                // Create a list of builds that contains this TestInstallation
                                LinkedList includedIn = fileContainer.getBuildInstallations(testInstallation);
                                String includedInList = "";
                                for (int k = 0; k < includedIn.size(); k++) {
                                    includedInList = includedInList + ((BuildInstallation) includedIn.get(k)).getBuildName() + "<br>";
                                }

                                if (testInstallation.getStatus() == TestInstallation.BUILT) {
                                    ChangeRequest changeRequest = testInstallation.getChangeRequest();
                                    changeRequest.refreshFromDatabase();
                                    // Only list TestInstallations that are marked as QA Complete
                                    if (changeRequest.getStatus().compareToIgnoreCase("QA Complete") == 0) {
                                        counter++;
                                        out.write("<tr>");
                                        out.write("<td" + fileContainer.getChangeRequestBackgroundColor(changeRequest) + "><a href=\"ChangeRequestInfo.jsp?crnumber=" + changeRequest.getCRNumber() + "\">" + changeRequest.getCRNumber() + "</a><br>");

                                        // Are there any items waiting for the completion of this one?
                                        LinkedList bookingItems = fileContainer.getBookingItemsWaitingFor(testInstallation);
                                        if (bookingItems != null) {
                                            int cntr = 0;
                                            for (int bi = 0; bi < bookingItems.size(); bi++) {
                                                cntr++;
                                                BookingItem bookingItem = (BookingItem) bookingItems.get(bi);
                                                out.write("<a href=\"ChangeRequestInfo.jsp?crnumber=" + bookingItem.getChangeRequest().getCRNumber() + "\" "
                                                        + "title=\"CR " + bookingItem.getChangeRequest().getCRNumber() + " is waiting for the completion of this TestInstallation!\"><img src=\"../images/WaitingForMe.png\" border=\"0\" width=\"42\" height=\"42\" "
                                                        + "alt=\"CR " + bookingItem.getChangeRequest().getCRNumber() + " is waiting for the completion of this TestInstallation!\""
                                                        + "hspace=\"6\" vspace=\"2\"></a>");
                                                if (cntr > 1) {
                                                    cntr = 0;
                                                    out.write("<br>");
                                                }
                                            }
                                        }
                                        out.write("</td>");

                                        out.write("<td><font size=\"-1\">" + fileContainer.formatChangeRequestSynopsisInHTML(changeRequest.getSynopsis()) + "</font></td>");
                                        out.write("<td>" + includedInList + "</td>");
                                        out.write("<td>" + testInstallation.getChangeRequest().getRequiredDBVersion() + "</td>");
                                        out.write("<td>");
                                        out.write("    <a href=\"QA_downloadQAFile.jsp?crnumber=" + changeRequest.getCRNumber() + "\">Download File</a>");
                                        out.write("</td>");
                                        if (selectAll) {
                                            out.write("<td><input type=\"checkbox\" checked=\"checked\" name=\"selectedTestInstallation_" + counter + "\" value=\"" + changeRequest.getCRNumber() + "\">" + changeRequest.getCRNumber() + "</td>");

                                        } else {
                                            out.write("<td><input type=\"checkbox\" name=\"selectedTestInstallation_" + counter + "\" value=\"" + changeRequest.getCRNumber() + "\">" + changeRequest.getCRNumber() + "</td>");
                                        }
                                        out.write("<td>" + testInstallation.getChangeRequest().getReadMe() + "</td>");
                                        out.write("</tr>");
                                    }
                                }
                            }
                        }
                        out.write("</table></td></tr>");
                        out.write("<tr><td><table>");

                        out.write("<tr bgcolor=\"#f0f0f0\">");
                        out.write("  <td width=\"25%\">Build Name:</td>");
                        out.write("  <td width=\"15%\"><input type=\"text\" name=\"buildName\" size=\"30\" maxlength=\"40\"></td>");
                        out.write("  <td width=\"25%\">Description:</td>");
                        out.write("  <td width=\"35%\"><input type=\"text\" name=\"buildDescription\" size=\"50\" maxlength=\"80\"></td>");
                        out.write("</tr>");
                        out.write("<tr bgcolor=\"#f0f0f0\">");
                        out.write("  <td >Updated DB Version:</td>");
                        out.write("  <td ><input type=\"text\" name=\"updateddbversion\" size=\"30\" maxlength=\"30\"></td>");
                        out.write("  <td ><input type=\"submit\" value=\"Create Build\"></td>");
                        out.write("  <td></td>");
                        out.write("</tr>");
                        out.write("</table>");
                    %>
                </td></tr>
        </table>
    </FORM>
    <%}%>

    <%@include file="../html/bottom.html"%>
