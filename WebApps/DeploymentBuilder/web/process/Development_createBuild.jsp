<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());

        ChangeRequest changeRequest = null;
        if (request.getParameter("crnumber") != null) {
            changeRequest = new ChangeRequest(request.getParameter("crnumber"));
        }

        String requiredDBVersion = request.getParameter("requireddbversion");
        String installationInstructions = request.getParameter("installInstructions");

        fileContainer.getTestInstallation(request.getParameter("crnumber")).getChangeRequest().setRequiredDBVersion(requiredDBVersion);
        fileContainer.getTestInstallation(request.getParameter("crnumber")).setInstallationInstructions(installationInstructions);

        fileContainer.buildTestInstallation(fileContainer.getTestInstallation(request.getParameter("crnumber")), FileContainer.getArchiveFolder(), user);
        fileContainer.save();
        History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_NEWTESTINSTALLATION, fileContainer.getTestInstallation(request.getParameter("crnumber")).getName()));

        // Return to previous page
        response.sendRedirect("Development.jsp");
    }
%>
<%@include file="../html/bottom.html"%>