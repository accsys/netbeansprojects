<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        ChangeRequest changeRequest = null;
        String filesUsed = null;
        if (request.getParameter("crnumber") != null) {
            changeRequest = new ChangeRequest(request.getParameter("crnumber"));
        }

        // RequiredFiles
        if (request.getParameter("filesused") != null) {
            filesUsed = (request.getParameter("filesused"));
        }

        if (filesUsed.trim().length() == 0) {
            String message = "You cannot create a reminder without defining the files that has to be monitored.";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        if (changeRequest == null) {
            String message = "Unable to find the given Change Request.  Please check the CR Number.";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        // Create new BookingItem
        if (filesUsed.trim().length() > 0) {
            BookingItem bookingItem = new BookingItem(changeRequest);
            bookingItem.setUser(user);
            bookingItem.setRequiredFileNames(filesUsed);
            bookingItem.saveToDatabase();
        }
        System.out.println("done");

        // Return to previous page
        response.sendRedirect("Reminders.jsp");
    }
%>
<%@include file="../html/bottom.html"%>
