<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="javazoom.upload.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean"></jsp:useBean> 



<%
    ChangeRequest alternativeChangeRequest = null;
    MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
    LinkedList<String> filesAlreadyInUse = new LinkedList();
    FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
    ChangeRequest changeRequest = null;
    // Get Change Request
    if (session.getAttribute("crnumber") != null) {
        changeRequest = new ChangeRequest((String) session.getAttribute("crnumber"));
    }

    String savePath = FileContainer.getTemporaryFolder().getAbsolutePath();
    upBean.setFolderstore(savePath);
    upBean.setFilesizelimit(1024 * 1000 * 20);

    try {
        upBean.setOverwrite(true);
        upBean.store(mrequest);
    } catch (IOException e) {
        e.printStackTrace();
    }

    // Now we need to register this file in the FileContainer
    Hashtable files = mrequest.getFiles();
    if (files.size() == 0) {
        response.sendRedirect("Development_addCRFile.jsp?crnumber=" + changeRequest.getCRNumber());
    }

    Enumeration enumer = files.elements();

    // What type of file is this?
    String fileType = mrequest.getParameter("fileType");
    int installType = InstallGroup.PROGRAM;
    if (fileType.compareToIgnoreCase("Program File") == 0) {
        installType = InstallGroup.PROGRAM;
    }
    if (fileType.compareToIgnoreCase("Script") == 0) {
        installType = InstallGroup.SCRIPT;
    }
    if (fileType.compareToIgnoreCase("Dll") == 0) {
        installType = InstallGroup.DLL;
    }
    if (fileType.compareToIgnoreCase("Config") == 0) {
        installType = InstallGroup.CONFIG;
    }
    if (fileType.compareToIgnoreCase("Help") == 0) {
        installType = InstallGroup.HELP;
    }
    if (fileType.compareToIgnoreCase("Utility") == 0) {
        installType = InstallGroup.UTILITY;
    }
    if (fileType.compareToIgnoreCase("XML") == 0) {
        installType = InstallGroup.XML;
    }
    if (fileType.compareToIgnoreCase("Report") == 0) {
        installType = InstallGroup.REPORT;
    }
    if (fileType.compareToIgnoreCase("Comms") == 0) {
        installType = InstallGroup.COMMS;
    }
    if (fileType.compareToIgnoreCase("System") == 0) {
        installType = InstallGroup.SYSTEM;
    }

    // Now let's print the uploaded files
    while (enumer.hasMoreElements()) {
        UploadFile upFile = (UploadFile) enumer.nextElement();
        String newFileName = upFile.getFileName();

        System.out.println("File :" + upFile.getFileName());

        // Can we use this file, or is it already used in another CR?
        if (!fileContainer.isFileNameInUse(newFileName)) {

            File uploadedFile = new File(savePath + FileContainer.getOSFileSeperator() + upFile.getFileName());
            QAFile qaFile = new QAFile(uploadedFile, installType);

            TestInstallation testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());
            if (testInstallation == null) {
                testInstallation = new TestInstallation(changeRequest.getCRNumber(), changeRequest);
            }

            // Register
            fileContainer.registerQAFile(qaFile, testInstallation);
            //fileContainer.save();
        } else {
            filesAlreadyInUse.add(newFileName);
        }

    }

    // Are there any files that had problems, i.e. already assigned to another CR?
    if (filesAlreadyInUse.size() == 0) {
        // Return to previous page
        response.sendRedirect("Development_addCRFile.jsp?crnumber=" + changeRequest.getCRNumber());
    } else {
%>
<%@include file="../html/top.html"%>

<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - Add a file to a TestInstallation</H3>
    <BR>

    <!-- Hyper links -->
    <%@ include file="../html/top_hyperlinks.txt" %>
    <%  Iterator iter = filesAlreadyInUse.iterator();
            while (iter.hasNext()) {
                String usedFile = (String) iter.next();
                out.write("<br><i>" + usedFile
                        + "</i> is already in use by another Change Request:<b><a href=\"ChangeRequestInfo.jsp?crnumber=" + fileContainer.getCRContainingFile(usedFile) + "\">" + fileContainer.getCRContainingFile(usedFile) + "</a></b>");
            }

            out.write("</CENTER>");
        }
    %>
