<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = za.co.ucs.lwt.deploymentbuilder.FileContainer.getInstance(za.co.ucs.lwt.deploymentbuilder.FileContainer.getContainerFile());
        TestInstallation testInstallation = fileContainer.getTestInstallation(request.getParameter("crnumber"));

        // Can we disassemble this testInstallation?
        LinkedList buildInstallations = fileContainer.getBuildInstallations(testInstallation);
        if (buildInstallations.size() > 0) {
            String message = "This Test Installation is still part of Builds.  You must disassemble the builds first.";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DISSASEMBLETESTINSTALLATION, testInstallation.getName()));
        fileContainer.dismantleTestInstallation(testInstallation, user, true);
        fileContainer.save();

        // Return to previous page
        response.sendRedirect("QA.jsp");
    }
%>
<%@include file="../html/bottom.html"%>
