<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        // Collect information for the BuildInstallation
        String buildName = request.getParameter("buildName");
        String buildDescription = request.getParameter("buildDescription");
        if ((buildName == null) || (buildName.trim().length() == 0)) {
            buildName = new String("<Unknown>");
        }

        if (buildDescription == null) {
            buildDescription = "";
        }

        String newDbVersion = request.getParameter("updateddbversion");
        // Does a build with this name already exist?
        if (fileContainer.getBuildInstallation(buildName) != null) {
            String message = "There is already a Build with this name.  Use another name or disassemble the current one first.";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        BuildInstallation buildInstallation = new BuildInstallation(buildName);
        buildInstallation.setBuildDescription(buildDescription);

        System.out.println("buildInstallation:" + buildInstallation);
        for (int i = 0; i < 500; i++) {
            String crNumber = request.getParameter("selectedTestInstallation_" + i);
            if (crNumber != null) {
                if (crNumber.trim().length() > 0) {
                    TestInstallation testInstallation = fileContainer.getTestInstallation(crNumber);
                    if (testInstallation != null) {
                        buildInstallation.addTestInstallation(testInstallation);
                        System.out.println("buildInstallation.addTestInstallation:" + testInstallation.getChangeRequest().getReadMe());
                    }
                }
            }
        }
        History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_NEWBUILDINSTALLATION, buildInstallation.getBuildName()));
        BuildHistory.getInstance().addEvent(new BuildHistoryEvent(user, buildInstallation));

        // Is this db version later than the latest required db ver of any of the TestInstallations
        //   included in it?
        if (buildInstallation.getRequiredDBVersion().compareToIgnoreCase(newDbVersion) > 0) {
            String message = "The Updated DB Version <b>" + newDbVersion + "</b> is less than the Required DB Version <b>" + buildInstallation.getRequiredDBVersion() + "</b>.<br>Change the Updated DB Version or exclude the relative Test Installations that causes this problem.";
            buildInstallation = null;
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        if (buildInstallation.getTestInstallations().size() > 0) {
            buildInstallation.setNewDBVersion(newDbVersion.trim().toUpperCase());
            fileContainer.addBuildInstallation(buildInstallation);
            fileContainer.buildBuildInstallation(buildInstallation, FileContainer.getArchiveFolder(), user);
            fileContainer.save();
        }

        response.sendRedirect("Management.jsp");
    }

%>
<%@include file="../html/bottom.html"%>