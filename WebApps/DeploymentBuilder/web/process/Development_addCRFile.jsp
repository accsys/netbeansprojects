<!-- This JSP displays the detailed information of a given Change Request -->
<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%

   User user = (User)session.getAttribute("user");
   
  if (user == null){
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }
  
  FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
  ChangeRequest changeRequest = null;
  if (request.getParameter("crnumber") != null){
    changeRequest = new ChangeRequest(request.getParameter("crnumber"));
  }
    TestInstallation testInstallation = fileContainer.getTestInstallation(request.getParameter("crnumber"));

  session.setAttribute("crnumber", new String(changeRequest.getCRNumber()));
%>



    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder - Add a file to a TestInstallation</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>

<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>
<!-- Change Request Detail -->

        <BR>    
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
        <tr><td>Add files for Change Request: <b><%out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a>");%></b></td>
        <tr><td>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" width="100%">
                        <tr><td width=20%>File Type</td><td width=70%>Select File</td><td width=10%></td></tr>
                        <tr>
                            <FORM action=addQAFile.jsp enctype="MULTIPART/FORM-DATA" method="post"> 
                            <td>
                                <input type="radio" name="fileType" value="Program File" checked> Program File<br>
                                <input type="radio" name="fileType" value="Script"> Script<br>
                                <input type="radio" name="fileType" value="Dll"> Dll<br>
                                <input type="radio" name="fileType" value="Config"> Config<br>
                                <input type="radio" name="fileType" value="Help"> Help<br>
                                <input type="radio" name="fileType" value="Utility"> Utility<br>
                                <input type="radio" name="fileType" value="XML"> XML<br>
                                <input type="radio" name="fileType" value="Report"> Report<br>
                                <input type="radio" name="fileType" value="Comms"> Comms<br>

                            </td>
                            <td><INPUT size=65 TYPE=FILE NAME=FileName></TD>
                            <td><INPUT TYPE=SUBMIT VALUE="Upload"></td>
                            </FORM>
                        </TR>
                </TABLE>
        </td></tr>
	</table>        

        <BR>    
        <table width=85% border="0" cellspacing="9" bgcolor="#fafafa" bordercolor="#a0a0a0" summary="">
        <tr><td>Current files attached to Change Request: <b><%out.write("<a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a>");%></b></td>
        <tr><td>
                <table border="1" cellpadding="3" bordercolor="#e0e0e0" width="100%">
                <tr><%@ include file="_displayQAFileDetail.jsp" %></tr>
                </table>
        </td></tr>
        <tr><td>
                <%
                    LinkedList bookingItems = fileContainer.getBookingItemsFor(changeRequest);
                    if (bookingItems.size()>0){

                        out.write("<br><b>Current Reminders logged against Change Request: <a href=\"ChangeRequestInfo.jsp?crnumber="+changeRequest.getCRNumber()+"\">"+changeRequest.getCRNumber()+"</a></b></td>");
                        out.write("<br><table border=\"1\" cellpadding=\"3\" bordercolor=\"#e0e0e0\" width=\"70%\">");
                        out.write("<tr bgcolor=\"#f0f0f0\"><td width=\"10%\">CR Number</td><td width=\"25%\">Synopsis</td><td width=\"35%\">Required Files</td><td  width=\"20%\">Booked by</td><td  width=\"10%\">Action</td></tr>");
                        for (int bi=0;bi<bookingItems.size();bi++){
                            BookingItem bookingItem = (BookingItem)bookingItems.get(bi);
                            out.write("<tr>");
                            out.write("<td"+fileContainer.getChangeRequestBackgroundColor(changeRequest)+"><a href=\"ChangeRequestInfo.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\">"+bookingItem.getChangeRequest().getCRNumber()+"</a></td>");
                            out.write("<td>"+fileContainer.formatChangeRequestSynopsisInHTML(bookingItem.getChangeRequest().getSynopsis())+"</td>");
                            String requiredFileDetail = fileContainer.getRequiredFilesWithDetailInHTML(bookingItem);
                            out.write("<td>"+requiredFileDetail+"</td>");
                            out.write("<td>"+bookingItem.getUser().getName()+"</td>");
                            out.write("<td>");
                            out.write("  <br><a href=\"Reminders_deleteReminder.jsp?crnumber="+bookingItem.getChangeRequest().getCRNumber()+"\" onclick=\"return confirm('Are you sure you want to cancel this Reminder?')\"><b>Cancel</b></a>");
                            out.write("</td>");
                            out.write("</tr>");
                            }                            
                        
                        out.write("</table>");
                    }
                %>
                </tr></td>
                <tr><td><FORM action=Development_createBuild.jsp method="post">
                            <input type="hidden" name="crnumber" value="<%out.write(changeRequest.getCRNumber());%>">
                            Required DB Version (Example: 7.0.05.01. 7=Version. 0=Service Release. 05=Maintenance Release. 01=Attempt #)<br>
			<input type="text" name="requireddbversion" size="40" maxlength="40">
                            <br>Installation Instructions (Optional):<br><textarea cols="30" rows="3" name="installInstructions"></textarea>
                            <INPUT TYPE=SUBMIT VALUE="Create Build">
                </FORM></td></TR>
                </table>
        </table>   


<%@include file="../html/bottom.html"%>
