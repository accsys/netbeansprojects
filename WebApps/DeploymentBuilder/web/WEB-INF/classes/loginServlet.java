/*
 * login.java
 *
 * Created on December 10, 2003, 9:57 AM
 */

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import za.co.ucs.lwt.deploymentbuilder.*;

/**
 *
 * @author  liam
 * @version
 */
public class loginServlet extends HttpServlet {
    
    public static User user;
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if ("login".equals(request.getParameter("exec"))) {
            login(request, response);
        }
        if ("cancel".equals(request.getParameter("exec"))) {
            cancel(request, response);
        }
        else {
            firstTime(request, response);
        }
        out.close();
    }
    
    protected void firstTime(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<center>");
        out.println("<BR>");
        out.println("<form method=get>");
        out.println("<input type=\"hidden\" name=\"exec\" value=\"\">");
        out.println("<table>");
        out.println(" <tr>");
        out.println("<td><b>Login Name:</b></td>");
        out.println("<td><input type=\"text\" name=\"username\" value=\"\"></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><b>Password:</b></td>");
        out.println("<td><input type=\"password\" name=\"password\" value=\"\"></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><input type=\"submit\" value = \"Submit\"  onclick=\"form.exec.value='login';\"></td>");
        out.println("<td><input type=\"submit\" value = \"Cancel\"  onclick=\"form.exec.value='cancel';\"></td>");
        out.println("</tr>");
        out.println("</table>");
        out.println("</form>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        
    }
    
    protected void login(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        // Extract Username and Password
        String loginName = request.getParameter("username");
        String loginPwd = request.getParameter("password");
        
        // Validate Username && Password
        if (loginName == null){ 
            response.sendRedirect("loginServlet");
        }
        
        User user = new User(loginName);
        if (user.getLoginPwd().compareToIgnoreCase(loginPwd) != 0){
            response.sendRedirect("loginServlet");
        }
        
    }
    
    protected void cancel(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
    }
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
    
}
