/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.ess;

import java.awt.SplashScreen;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static za.co.accsys.ess.FXMLDocumentController.contentsChanged;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import javafx.application.Platform;

/**
 *
 * @author Liam
 */
public class ESSSetup extends Application {

    public static ObservableList names = FXCollections.observableArrayList();
    public static Stage stage;
    private FXMLDocumentController controller;
    
    @Override
    public void start(final Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.initStyle((StageStyle.DECORATED));
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                if (contentsChanged) {
                    Dialogs.DialogResponse responseToSave = Dialogs.showConfirmDialog(null,
                            "Do you wish to save your changes first?", "Save before closing", "Close ESSConfig utility", Dialogs.DialogOptions.YES_NO);
                    if (responseToSave == Dialogs.DialogResponse.YES) {
//                        controller.saveToFileContainer();
                        e.consume();
                    }
                    if (responseToSave == Dialogs.DialogResponse.NO) {
                        Platform.exit();
                    }
                }
            }
        });
        stage.show();

        stage.setTitle("ESS Configuration");
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        try {
            Thread.sleep(3000); // the parameter is in milliseconds
        }catch(InterruptedException e) {
            System.out.println(e.getMessage());
        }
            // begin with the interactive portion of the program
            launch(args);
        }
}
