/*
 * This is the Controller part of the MVC used to set up the PeopleWare organograms,
 * rules, and attributes.
 */
package za.co.accsys.ess;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import static za.co.accsys.ess.ESSSetup.stage;
import za.co.ucs.accsys.webmodule.ESSVersion;
import za.co.ucs.accsys.webmodule.EmployeeSelection;
import za.co.ucs.accsys.webmodule.EmployeeSelectionRule;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.ReportingStructure;
import za.co.ucs.accsys.webmodule.SelectionInterpreter;
import za.co.ucs.accsys.webmodule.SelectionLink;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;
import za.co.ucs.lwt.initools.OutputFile;

/**
 *
 * @author lterblanche
 */
public class FXMLDocumentController implements Initializable {

    // Private members
    FileContainer fileContainer = FileContainer.getInstance();
    LinkedList<EmployeeSelection> localEmployeeSelections = new LinkedList<>();
    LinkedList<EmployeeSelection> llES_filteredEmployeeSelections = new LinkedList<>();
    LinkedList<EmployeeSelection> llRS_filteredAndUnusedEmployeeSelections = new LinkedList<>();
    LinkedList<ReportingStructure> llRS_ReportingStructures = new LinkedList<>();
    LinkedList<ReportingStructure> localReportingStructures = new LinkedList<>();
    LinkedList<WebProcessDefinition> localWebProcessDefinitions = new LinkedList<>();
    // Now add observability by wrapping it with ObservableList
//    ObservableList<ReportingStructure> observableReportingStructures = FXCollections.observableList(localReportingStructures);
    ObservableList<ReportingStructure> observableReportingStructures;
//    ObservableList<WebProcessDefinition> observableWebProcessDefinitions = FXCollections.observableList(localWebProcessDefinitions);
    ObservableList<WebProcessDefinition> observableWebProcessDefinitions;

    EmployeeSelection clipboardEmployeeSelection;
    SelectionLink clipboardSelectionLink;
    static boolean contentsChanged = false;
    static String appTitle = "ESS Configuration";
    // Used an observable value to bind to the progress bar (pbProgress)
    private final DoubleProperty progressBarPosition = new SimpleDoubleProperty(0);

    // <editor-fold defaultstate="collapsed" desc=" ${Scene Builder Setup} ">
    @FXML
    private TabPane tpTabPane;
    @FXML
    private Tab tabWebProcesses;
    @FXML
    private ListView<WebProcessDefinition> lvAvailableWebProcesses;
    @FXML
    private Label lblWebProcess_ProcessName;
    @FXML
    private TextField tfWebProcess_ProcessName;
    @FXML
    private Label lblWebProcess_ReportingStructure;
    @FXML
    private Label lblWebProcess_MaxProcAge;
    @FXML
    private Label lblWebProcess_MaxStageAge;
    @FXML
    private TextField tfWebProcess_MaxDurationPerProcess;
    @FXML
    private TextField tfWebProcess_MaxDurationPerStage;
    @FXML
    private CheckBox cbWebProcess_IsSupervisor;
    @FXML
    private Tab tabReportingStructures;
    @FXML
    private Tab tabSelectionFormulas;
    @FXML
    private TextField tfWebProcess_ActivateOn;
    @FXML
    private TextField tfWebProcess_DeactivateOn;
    @FXML
    private Button btnWebProcess_Apply;
    @FXML
    private CheckBox cbWebProcess_NotifyStaffOfActivation;
    @FXML
    private ChoiceBox<ReportingStructure> cbWebProcess_ReportingStructure;
    @FXML
    private CheckBox cbWebProcess_NotifyStaffOfDeActivation;
    @FXML
    private MenuItem mnuSaveConfiguration;
    @FXML
    private ListView<ReportingStructure> lvRS_ExistingReportingStructures;
    @FXML
    private ListView<EmployeeSelection> lvRS_ExistingEmployeeSelections;
    @FXML
    private Insets x1;
    @FXML
    private Button btnRS_ClearFilter;
    @FXML
    private TreeView<EmployeeSelection> tvRS_ReportingStructureTreeView;
    @FXML
    private TextField tfRS_FilterEmployeeSelections;

    private ListView<ReportingStructure> lvRS_FilteredEmployeeSelections;
    @FXML
    private TextField tfRS_FindEmployeeSelection;
    @FXML
    private Button btnRS_ClearLocate;
    @FXML
    private Insets x2;
    @FXML
    private Button btnRS_NewRS;
    @FXML
    private Button btnRS_RenameRS;
    @FXML
    private Button btnRS_CopyRS;
    @FXML
    private Button btnRS_DeleteRS;
    @FXML
    private MenuItem mnuClose;

    @FXML
    private TextField tfES_FilterEmployeeSelections;
    @FXML
    private Button btnES_ClearFilter;
    @FXML
    private Font x4;
    @FXML
    private HTMLEditor htmlSF_Editor;
    @FXML
    private ListView<EmployeeSelection> lvES_ExistingEmployeeSelections;
    @FXML
    private Button btnES_NewES;

    @FXML
    private Button btnES_DeleteES;
    @FXML
    private Button btnES_CopyES;
    @FXML
    private Button btnES_TestFormula;
    @FXML
    private Label lblRS_ReportingStructureHierarchy;
    @FXML
    private TextField tfES_ProcessName;
    @FXML
    private ChoiceBox<String> cbES_Filter;
    @FXML
    private ChoiceBox<String> cbES_Compare;
    @FXML
    private Button btnES_AddToFormula;
    @FXML
    private Button btnES_ApplyChanges;
    @FXML
    private ListView<String> lvES_TestResult;
    @FXML
    private MenuItem mnuValidateConfiguration;
    @FXML
    private Text txtStatus;
    @FXML
    private StackPane root;
    @FXML
    private MenuItem mnuExportConfiguration;
    @FXML
    private Color x3;
    @FXML
    private Font x5;
    @FXML
    private MenuItem mnuAbout;
    @FXML
    private Insets x11;
    @FXML
    private Insets x12;
    @FXML
    private Insets x13;
    @FXML
    private Insets x14;
    @FXML
    private Insets x15;
    @FXML
    private Insets x16;
    @FXML
    private Insets x17;
    @FXML
    private Insets x18;
    @FXML
    private ProgressBar pbProgress;

// </editor-fold>   
    //
    // <editor-fold defaultstate="collapsed" desc=" ${WebProcessDefinition Tab} ">
    /**
     * Setup the components used in the Web Process Definitions
     */
    private void initWebProcessDefinitionsTab() {
        // Populate existing WebProcessDefinitions
        lvAvailableWebProcesses.setItems(observableWebProcessDefinitions);

        // 
        // EVENT LISTENER: Click on Web Proccesses Structure
        //
        lvAvailableWebProcesses.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<WebProcessDefinition>() {

            @Override
            public void changed(ObservableValue<? extends WebProcessDefinition> observable, WebProcessDefinition oldValue, WebProcessDefinition newValue) {
                // Update the display
                if (newValue != null) {
                    tfWebProcess_ProcessName.setText(newValue.getName());
                    tfWebProcess_MaxDurationPerProcess.setText(String.valueOf(newValue.getMaxProcessAge()));
                    tfWebProcess_MaxDurationPerStage.setText(String.valueOf(newValue.getMaxStageAge()));
                    tfWebProcess_ActivateOn.setText(String.valueOf(newValue.getActiveFromDayOfMonth()));
                    tfWebProcess_DeactivateOn.setText(String.valueOf(newValue.getActiveToDayOfMonth()));
                    cbWebProcess_NotifyStaffOfActivation.setSelected(newValue.isNotifyOfPendingActivation());
                    cbWebProcess_NotifyStaffOfDeActivation.setSelected(newValue.isNotifyOfPendingDeactivation());
                    cbWebProcess_IsSupervisor.setSelected(newValue.getCannotInstantiateSelf());

                    // Populate the ChoiceBox with the list of available ReportingStructures
                    cbWebProcess_ReportingStructure.setItems(observableReportingStructures);

                    // Highlight the correct one
                    cbWebProcess_ReportingStructure.getSelectionModel().clearSelection();
                    if (newValue.getReportingStructure() != null) {
                        cbWebProcess_ReportingStructure.getSelectionModel().select(newValue.getReportingStructure());
                    }
                }
            }
        });

        //
        // EVENT LISTENER: Web Process Definition attributes changed
        //
        observableWebProcessDefinitions.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change change) {
                System.out.println("Web Process Definition - Detected a change! ");
                setContentsChanged(true);
            }
        });

        // 
        // EVENT LISTENER: Click on Apply button
        //
        btnWebProcess_Apply.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                int index = lvAvailableWebProcesses.getSelectionModel().getSelectedIndex();
                WebProcessDefinition toChange = localWebProcessDefinitions.get(index);
                saveWebProcessDefinition(toChange);
                observableWebProcessDefinitions.remove(toChange);
                observableWebProcessDefinitions.add(index, toChange);
                setContentsChanged(true);
                // Reselect in the listView
                lvAvailableWebProcesses.getSelectionModel().clearAndSelect(index);
            }
        });

        //
        // TOOL TIPS
        //
        Tooltip t1 = new Tooltip("This list contains all the possible processes that can be used in the ESS");
        Tooltip.install(lvAvailableWebProcesses, t1);

        Tooltip t2 = new Tooltip("When checked, the employee will not have access to this process.\n"
                + "Only a manager can use this process.\nAnd he/she can only use it on employees reporting to him/her.");
        Tooltip.install(cbWebProcess_IsSupervisor, t2);

        Tooltip t3a = new Tooltip("Should an employee be notified via email if this process is being activated?");
        Tooltip t3d = new Tooltip("Should an employee be notified via email if this process is being deactivated?");
        Tooltip.install(cbWebProcess_NotifyStaffOfActivation, t3a);
        Tooltip.install(cbWebProcess_NotifyStaffOfDeActivation, t3d);

        Tooltip t4 = new Tooltip("How many hours (8 hours per day) should a process stay alive before being cancelled?");
        Tooltip.install(tfWebProcess_MaxDurationPerProcess, t4);

        Tooltip t5 = new Tooltip("How long should a process wait for authorisation before escalating to the next person?");
        Tooltip.install(tfWebProcess_MaxDurationPerStage, t5);

        // 
        // Select the first item
        //
        if (lvAvailableWebProcesses.getItems().size() > 0) {
            lvAvailableWebProcesses.getSelectionModel().select(0);
        }

        // 
        // Bind the Progress Bar to the ProgressPosition variable
        //
        pbProgress.progressProperty().bind(progressBarPosition);

    }

    // </editor-fold>   
    //
    // <editor-fold defaultstate="collapsed" desc=" ${ReportingStructure Tab} ">
    /**
     * Setup the components used in the Reporting Structures
     */
    private void initReportingStructuresTab() {
        // ----------------------------------------------
        // SETUP LISTVIEW : REPORTING STRUCTURES
        // Populate existing ReportingStructures

        lvRS_ExistingReportingStructures.setItems(observableReportingStructures);
        // Order the list
        orderReportingStructureList(lvRS_ExistingReportingStructures);
        // Disable the Delete button
        btnRS_DeleteRS.setDisable(true);
        // Disable the Rename button
        btnRS_RenameRS.setDisable(true);
        // Disable the Copy button
        btnRS_CopyRS.setDisable(true);

        // 
        // EVENT LISTENER: Click Reporting Structure
        //
        lvRS_ExistingReportingStructures.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                ReportingStructure newValue = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                if (newValue != null) {
                    System.out.println("Selected item: " + newValue);

                    // Enable the Delete button
                    btnRS_DeleteRS.setDisable(false);
                    // Enable the Rename button
                    btnRS_RenameRS.setDisable(false);
                    // Enable the Copy button
                    btnRS_CopyRS.setDisable(false);

                    // Clear the filters
                    tfRS_FindEmployeeSelection.setText("");
                    tfRS_FilterEmployeeSelections.setText("");

                    buildReportingStructureInTreeView(newValue, tvRS_ReportingStructureTreeView);

                    // For this reporting structure, list the
                    // Employee Selections that has not yet been used, and
                    // apply the filter (if any)
                    rebuildEmployeeSelectionLists();

                }
            }
        });

        //
        // EVENT LISTENER: Reporting Structure List changed
        //
        observableReportingStructures.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change change) {
                System.out.println("Reporting Structures - Detected a change! " + change.toString());
                setContentsChanged(true);
            }
        });

        // ---------------------------------------------
        // SETUP LISTVIEW :EMPLOYEE SELECTIONS
        // FILTER Employee Selections
        //
        // Initiate the filtered list of EmployeeSelections
        llRS_filteredAndUnusedEmployeeSelections.addAll(localEmployeeSelections);
        lvRS_ExistingEmployeeSelections.setItems(FXCollections.observableList(llRS_filteredAndUnusedEmployeeSelections));
        // Order the list
        orderEmployeeSelectionList(lvRS_ExistingEmployeeSelections);

        //
        // EVENT LISTENER: Filter on TextField
        //
        tfRS_FilterEmployeeSelections.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {

                // Filter
                rebuildEmployeeSelectionLists();
            }
        });

        // 
        // EVENT LISTENER: Click on Clear button
        //
        btnRS_ClearFilter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tfRS_FilterEmployeeSelections.setText("");
                // Filter
                rebuildEmployeeSelectionLists();
            }
        });
        //
        // EVENT LISTENER: Click Employee Selection
        //        
        lvRS_ExistingEmployeeSelections.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<EmployeeSelection>() {
            @Override
            public void changed(ObservableValue<? extends EmployeeSelection> observable, EmployeeSelection oldValue, EmployeeSelection newValue) {
                // This is where we build the reporting structure for the TreeView
                //System.out.println("Selected item: " + newValue);

            }
        });

        //
        // DRAG n DROP: On Drag Detected
        //
        lvRS_ExistingEmployeeSelections.setOnDragDetected(
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event
            ) {
                // Can't Drag if no Employee Selection is selected
                if (lvRS_ExistingEmployeeSelections.getSelectionModel().getSelectedItem() == null) {
                    event.consume();
                    return;
                }
                // Can't Drag if no Reporting Structure is selected
                if (lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem() == null) {
                    event.consume();
                    return;
                }
                /* drag was detected, start a drag-and-drop gesture*/
 /* allow any transfer mode */
                Dragboard db = lvRS_ExistingEmployeeSelections.startDragAndDrop(TransferMode.MOVE);

                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.put(DataFormat.PLAIN_TEXT, lvRS_ExistingEmployeeSelections.getSelectionModel().getSelectedItem().toString());
                db.setContent(content);

                event.consume();
            }
        }
        );

        // 
        // EVENT LISTENER: Delete Reporting Structure
        //
        btnRS_DeleteRS.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                ReportingStructure selected = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                if (selected != null) {
                    DialogResponse response = Dialogs.showConfirmDialog(null,
                            "Do you wish to delete this reporting structure?", "Confirm Deletion", "Delete '" + selected + "'", Dialogs.DialogOptions.YES_NO);
                    if (response == DialogResponse.YES) {
                        // Clear this Reporting Structure
                        //lvRS_ExistingReportingStructures.getItems().remove(selected);
                        observableReportingStructures.remove(selected);
                        // Unselect reporting structures
                        //lvRS_ExistingReportingStructures.getSelectionModel().clearSelection();
                        // Clear TreeView
                        tvRS_ReportingStructureTreeView.setRoot(null);
                        //TODO: Still clear up the WebProcessDefinition screen if the reporting structure is gone

                        //rebuildReportingStructureLists();
                        setContentsChanged(true);
                    }
                }
            }
        }
        );

        // 
        // EVENT LISTENER: Rename Reporting Structure
        //
        btnRS_RenameRS.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                ReportingStructure selected = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                if (selected != null) {
                    String newName = Dialogs.showInputDialog(null, "New name:", "Rename Reporting Structure", "Rename '" + selected + "'", selected.getName());
                    if (newName != null) {
                        if (newName.trim().length() > 0) {
                            // Rename Reporting Structure
                            int index = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedIndex();
                            ReportingStructure toChange = localReportingStructures.get(index);
                            String originalName = toChange.toString();
                            observableReportingStructures.remove(toChange);
                            toChange.setName(newName);
                            //
                            // Validate new name
                            //
                            while (!isRSNameUnique(toChange)) {
                                String nameOfCurrentStructure = toChange.getName();
                                String message = "Please rename:\n\n\t" + nameOfCurrentStructure + "\n\nto avoid errors in the reporting structure.";
                                String newStructureName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name", nameOfCurrentStructure);
                                // If the suer makes it blank keep the old name
                                if (newStructureName == null || newStructureName.trim().length() == 0) {
                                    newStructureName = originalName;
                                }
                                toChange.setName(newStructureName);
                                setContentsChanged(true);
                            }
                            showStatus(null, 0, 0);
                            if (toChange.getName() != null) {
                                observableReportingStructures.add(index, toChange);
                            }
                            // Order the list
                            orderReportingStructureList(lvRS_ExistingReportingStructures);
                            orderEmployeeSelectionList(lvRS_ExistingEmployeeSelections);
                            // Once created and added, select it
                            lvRS_ExistingReportingStructures.getSelectionModel().select(toChange);
                            lvRS_ExistingReportingStructures.scrollTo(lvRS_ExistingReportingStructures.getSelectionModel().getSelectedIndex());
                        }
                    }
                }
            }
        }
        );

        // 
        // EVENT LISTENER: New Reporting Structure
        //
        btnRS_NewRS.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String newName = Dialogs.showInputDialog(null, "Name:", "Create a new Reporting Structure", "New Reporting Structure");
                if (newName != null) {
                    if (newName.trim().length() > 0) {
                        // New Reporting Structure
                        ReportingStructure newStructure = new ReportingStructure(newName, newName);

                        setContentsChanged(true);
                        //
                        // Validate new name
                        //
                        int i = 1;
                        while (!isRSNameUnique(newStructure)) {
                            String nameOfCurrentStructure = newStructure.getName();
                            String message = "Please rename:\n\n\t" + nameOfCurrentStructure + "\n\nto avoid errors in the reporting structure.\n*Failure to choose a unique name will result in an automatic rename.";
                            String newStructureName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name", nameOfCurrentStructure);
                            // If the user cancels exit the loop
                            if (newStructureName == null) {
                                return;
                            }
                            // If the user makes it blank rename
                            if (newStructureName.trim().length() == 0) {
                                newStructureName = i++ + "_renamed_" + nameOfCurrentStructure;
                            }
                            newStructure.setName(newStructureName);
                            setContentsChanged(true);
                        }
                        showStatus(null, 0, 0);
                        if (newStructure.getName() != null) {
                            observableReportingStructures.add(newStructure);
                        }
                        // Order the list
                        orderReportingStructureList(lvRS_ExistingReportingStructures);
                        orderEmployeeSelectionList(lvRS_ExistingEmployeeSelections);
                        // Once created and added, select it
                        lvRS_ExistingReportingStructures.getSelectionModel().select(newStructure);
                        lvRS_ExistingReportingStructures.scrollTo(lvRS_ExistingReportingStructures.getSelectionModel().getSelectedIndex());
                    }
                }
            }
        }
        );

        // 
        // EVENT LISTENER: Copy Reporting Structure
        //
        btnRS_CopyRS.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                ReportingStructure toCopy = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                if (toCopy != null) {

                    String newName = Dialogs.showInputDialog(null, "Name for new Reporting Structure", "Copy", "Copy existing Reporting Structure", toCopy.getName() + "(copy)");
                    if (newName != null) {
                        if (newName.trim().length() > 0) {
                            // Rename Reporting Structure
                            ReportingStructure newStructure = new ReportingStructure(newName, newName);
                            LinkedList<SelectionLink> links = new LinkedList(toCopy.selectionLinks);
                            for (SelectionLink sl : links) {
                                newStructure.addSelectionLink(sl);
                            }
                            //
                            // Validate new name
                            //
                            int nbReportingStructures = localReportingStructures.size();
                            if (nbReportingStructures > 0) {
                                int i = 1;
                                showStatus("Testing Reporting Structure: " + newStructure.toString(), i + 1, nbReportingStructures);
                                //
                                // Check that names differ
                                //
                                LinkedList<ReportingStructure> localStructures = new LinkedList(localReportingStructures);
//                                        localStructures.remove(newStructure);
                                for (ReportingStructure structureInList : localStructures) {
                                    // If hashCodes are the same rename the current reporting structure
                                    if (newStructure.getName().equals(structureInList.getName())) {
                                        // Ask user to change name
                                        // Edit a Reporting Structure
                                        // Edit dialog
                                        String nameOfCurrentStructure = newStructure.getName();
                                        String message = "Please rename:\n\n\t" + nameOfCurrentStructure + "\n\nto avoid errors in the reporting structure.\n\n*If you do not rename the reporting structure it will be renamed automatically.";

                                        String newStructureName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name");

                                        // Rename Structure
                                        if ((newStructureName == null) || (newStructureName.trim().length() == 0)) {
                                            newStructureName = i - 1 + "_renamed_" + nameOfCurrentStructure;
                                        }
                                        newStructure.setName(newStructureName);
                                        setContentsChanged(true);
                                    }
                                    i++;
                                }
                                showStatus(null, 0, 0);
                            }
                            // Add to existing list
                            observableReportingStructures.add(newStructure);
                            // Order the list
                            orderReportingStructureList(lvRS_ExistingReportingStructures);
                            orderEmployeeSelectionList(lvRS_ExistingEmployeeSelections);
                            rebuildEmployeeSelectionLists();
                            // Once created and added, select it
                            lvRS_ExistingReportingStructures.getSelectionModel().select(newStructure);
                            lvRS_ExistingReportingStructures.scrollTo(lvRS_ExistingReportingStructures.getSelectionModel().getSelectedIndex());
                            setContentsChanged(true);
                        }
                    }
                } else {
                    Dialogs.showErrorDialog(null, "Please select an existing Reporting Structure before trying to copy it.");

                }
            }
        }
        );

        // --------------------------------------
        // SETUP TREEVIEW: Reporting Structures
        // CELL FACTORY for TreeView
        //
        tvRS_ReportingStructureTreeView.setCellFactory(
                new Callback<TreeView<EmployeeSelection>, TreeCell<EmployeeSelection>>() {
            @Override
            public TreeCell call(TreeView<EmployeeSelection> param
            ) {
                return new DnDCell(param, lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem());
            }
        }
        );

        //
        // EVENT LISTENER: Person typed in search terms
        //
        tfRS_FindEmployeeSelection.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke
            ) {
                // Only SEARCH if we have selected a Reporting Structure
                if (tvRS_ReportingStructureTreeView.getRoot() != null) {
                    TreeItem rootItem = tvRS_ReportingStructureTreeView.getRoot();
                    TreeItem selectedItem = rootItem;

                    // GET FILTER String
                    String strFind = tfRS_FindEmployeeSelection.getText();
                    if (strFind.trim().length() > 0) {

                        //Locate strFind in the TreeView
                        selectedItem = searchInTreeView(rootItem, strFind);
                    }

                    // Only highlight found item if we did, in fact, find an item
                    if (selectedItem != null) {
                        tvRS_ReportingStructureTreeView.getSelectionModel().select(selectedItem);
                        tvRS_ReportingStructureTreeView.scrollTo(tvRS_ReportingStructureTreeView.getRow(selectedItem));
                    }
                }
            }

            /**
             * Iteratively steps through the tree view, looking for the first
             * occurrence of 'searchString' in the given TreeItem
             *
             * @param currentNode
             * @param valueToSearch
             * @return TreeItem - the item that matches the search string
             */
            private TreeItem<EmployeeSelection> searchInTreeView(final TreeItem<EmployeeSelection> currentNode, final String valueToSearch) {
                TreeItem<EmployeeSelection> result = null;
                if (matchesFilter(currentNode.toString(), valueToSearch)) {
                    result = currentNode;
                } else if (!currentNode.isLeaf()) {
                    for (TreeItem<EmployeeSelection> child : currentNode.getChildren()) {
                        result = searchInTreeView(child, valueToSearch);
                        if (result != null) {
                            break;
                        }
                    }
                }
                return result;
            }
        }
        );

        //
        // EVENT LISTENER: Person pressed a KEY
        //
        tvRS_ReportingStructureTreeView.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                System.out.println("KeyCode :" + t.getCode());

                //
                // KEY = DELETE
                //
                if (t.getCode() == KeyCode.DELETE) {
                    // Which item was deleted?
                    TreeItem<EmployeeSelection> ti = tvRS_ReportingStructureTreeView.getSelectionModel().getSelectedItem();
                    EmployeeSelection es = ti.getValue();
                    ReportingStructure rs = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();

                    deleteEmployeeSelectionFromReportingStructure(es, rs);

                    buildReportingStructureInTreeView(rs, tvRS_ReportingStructureTreeView);

                    // For this reporting structure, list the
                    // Employee Selections that has not yet been used, and
                    // apply the filter (if any)
                    rebuildEmployeeSelectionLists();

                    tvRS_ReportingStructureTreeView.getSelectionModel().clearSelection();
                    setContentsChanged(true);
                    t.consume();
                }
                //
                // KEY = CTRL+X (Cut)
                //
                if (t.getCode().equals(KeyCode.X) && t.isControlDown()) {
                    System.out.println("Cut :" + t.getCode());
                    // Which EmployeeSelection was cut?
                    TreeItem<EmployeeSelection> ti = tvRS_ReportingStructureTreeView.getSelectionModel().getSelectedItem();
                    int selectionIndex = tvRS_ReportingStructureTreeView.getSelectionModel().getSelectedIndex();
                    EmployeeSelection es = ti.getValue();
                    clipboardEmployeeSelection = es;
                    // Which SelectionLink should be cut?
                    ReportingStructure rs = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                    EmployeeSelection parentSelection = rs.getParentSelection(clipboardEmployeeSelection);
                    clipboardSelectionLink = rs.getSelectionLink(clipboardEmployeeSelection, parentSelection);

                    buildReportingStructureInTreeView(rs, tvRS_ReportingStructureTreeView);

                    tvRS_ReportingStructureTreeView.getSelectionModel().select(selectionIndex);
                    tvRS_ReportingStructureTreeView.scrollTo(selectionIndex);

                    t.consume();
                }
                //
                // KEY = CTRL+V (Paste)
                //
                if (t.getCode().equals(KeyCode.V) && t.isControlDown()) {
                    System.out.println("Paste :" + t.getCode());

                    // Which item was selected?
                    ReportingStructure rs = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
                    TreeItem<EmployeeSelection> ti = tvRS_ReportingStructureTreeView.getSelectionModel().getSelectedItem();
                    EmployeeSelection newParentSelection = ti.getValue();

                    //
                    // ONLY PASTE IF ITS NOT RECURSIVE!
                    //
                    if (isDecendentOf(tvRS_ReportingStructureTreeView, clipboardEmployeeSelection.toString(), newParentSelection.toString())) // We accept the transfer!!!!!
                    {
                        Dialogs.showErrorDialog(null, "You cannot paste an Employee Selection under a Selection that reports to it.");
                        // Reset the 'clipboardEmployeeSelection'
                        clipboardEmployeeSelection = null;

                        buildReportingStructureInTreeView(rs, tvRS_ReportingStructureTreeView);
                        t.consume();

                    } else {

                        // Delete the CUT selection link
                        rs.getSelectionLinks().remove(clipboardSelectionLink);

                        // Create the PASTE selection link
                        SelectionLink pasteLink = new SelectionLink(clipboardEmployeeSelection, newParentSelection);
                        int selectionIndex = tvRS_ReportingStructureTreeView.getSelectionModel().getSelectedIndex();
                        rs.getSelectionLinks().add(pasteLink);

                        // Reset the 'clipboardEmployeeSelection'
                        clipboardEmployeeSelection = null;

                        buildReportingStructureInTreeView(rs, tvRS_ReportingStructureTreeView);

                        tvRS_ReportingStructureTreeView.getSelectionModel().select(selectionIndex);
                        tvRS_ReportingStructureTreeView.scrollTo(selectionIndex);
                        setContentsChanged(true);
                        t.consume();
                    }
                }
            }
        });

        //
        // EVENT LISTENER: Select an item in the TreeView
        //
        tvRS_ReportingStructureTreeView.getSelectionModel()
                .selectedItemProperty().addListener(new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observable, Object oldValue,
                            Object newValue
                    ) {
                        TreeItem<EmployeeSelection> selectedItem = (TreeItem<EmployeeSelection>) newValue;
                        if (selectedItem != null) {
                            System.out.println("Selected Text : " + selectedItem.getValue());
                            //Display the hierarchy from this selection to the top
                            lblRS_ReportingStructureHierarchy.setText(getHierarchyOfReportingStructure(lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem(), selectedItem.getValue()));

                        }
                    }

                }
                );
    }
// </editor-fold>    
    //
    // <editor-fold defaultstate="collapsed" desc=" ${EmployeeSelections Tab} ">

    /**
     * Setup the components used in the Web Process Definitions
     */
    private void initEmployeeSelectionsTab() {
        // ---------------------------------------------
        // SETUP LISTVIEW :EMPLOYEE SELECTIONS
        // FILTER Employee Selections
        //
        // Initiate the filtered list of EmployeeSelections
        llES_filteredEmployeeSelections.addAll(localEmployeeSelections);
        lvES_ExistingEmployeeSelections.setItems(FXCollections.observableList(llES_filteredEmployeeSelections));
        // Order the list
        orderEmployeeSelectionList(lvES_ExistingEmployeeSelections);
        lvES_ExistingEmployeeSelections.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // ---------------------------------------------
        // SETUP HTML Editor
        // HIDE Controls
        // 
//        for (Node toolBar = htmlSF_Editor.lookup(".tool-bar"); toolBar != null; toolBar = htmlSF_Editor.lookup(".tool-bar")) {
//            ((Pane) toolBar.getParent()).getChildren().remove(toolBar);
//        }

        //
        // SETUP CHOICEBOX: Filter
        //
        // Populate the ChoiceBox with the list of available Filters
        ArrayList<String> keyWords = new ArrayList<>();
        keyWords.addAll(Arrays.asList(EmployeeSelectionRule.getKeyWords()));
        cbES_Filter.setItems(FXCollections.observableList(keyWords));

        //
        // SETUP CHOICEBOX: Comparitors
        //
        // Populate the ChoiceBox with the list of available Filters
        ArrayList<String> comparitors = new ArrayList<>();
        comparitors.addAll(Arrays.asList(getSelectionComparators()));
        cbES_Compare.setItems(FXCollections.observableList(comparitors));

        //
        // Disable all the buttons
        //
        // <editor-fold defaultstate="collapsed" desc=" $Disable all the buttons ">
        {
            // Disable the Delete button
            btnES_DeleteES.setDisable(true);
            // Disable the Copy button
            btnES_CopyES.setDisable(true);
            // Disable Test Formula Button
            btnES_TestFormula.setDisable(true);
            // Disable the AddToFormula button
            btnES_AddToFormula.setDisable(true);
            // Disable dropdown choicebox for Filter
            cbES_Filter.setDisable(true);
            // Disable dropdown choicebox for Compare
            cbES_Compare.setDisable(true);
            // Disable Apply Changes
            btnES_ApplyChanges.setDisable(true);
        }
        // </editor-fold> 

        //
        // EVENT LISTENER: On Test Formula
        //
        btnES_TestFormula.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                // Get the text in the HTMLEditor
                String htmlText = htmlSF_Editor.getHtmlText();
                String plainText = stripHTML(htmlText);
                plainText = plainText.replaceAll("&nbsp;", " ");
                plainText = plainText.replaceAll("&NBSP;", " ");
                // Display the formula
                htmlSF_Editor.setHtmlText(convertStringToHTML(plainText));

                //-------------------
                // Evaluate the rule
                //-------------------
                // Create a new Rule and Execute it to test the results
                EmployeeSelectionRule rule = new EmployeeSelectionRule("Test", "Test");
                // Extract Text from editor and execute rule
                rule.setRule(plainText);
                String testResult = rule.listResult();
                StringTokenizer token = new StringTokenizer(testResult, "\n");

                ObservableList<String> items = FXCollections.observableArrayList();
                while (token.hasMoreTokens()) {
                    String resultEmp = token.nextToken();
                    items.add(resultEmp);
                }
                lvES_TestResult.setItems(items);

                // Enable the Apply button
                btnES_ApplyChanges.setDisable(false);
            }
        });

        //
        // EVENT LISTENER: On AddToFormula
        //
        btnES_AddToFormula.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                // Add a default formula into the Rules Editor
                String comparator = cbES_Compare.getSelectionModel().getSelectedItem();
                String formula = " [" + cbES_Filter.getSelectionModel().getSelectedItem() + " " + comparator + " '...'] ";

                String htmlText = htmlSF_Editor.getHtmlText();
                String text = htmlText.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
                text = text.replaceAll("&nbsp;", " ");
                text = text + formula;
                // Display the formula
                htmlSF_Editor.setHtmlText(convertStringToHTML(text));
            }
        });

        //
        // EVENT LISTENER: Filter on TextField
        //
        tfES_FilterEmployeeSelections.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {

                // Filter
                rebuildEmployeeSelectionLists();
            }
        });

        //
        // EVENT LISTENER: Change on Selection's name
        //
        tfES_ProcessName.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                // Enable the Apply button
                btnES_ApplyChanges.setDisable(false);
            }
        });

        // 
        // EVENT LISTENER: Click on Clear button
        //
        btnES_ClearFilter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                tfES_FilterEmployeeSelections.setText("");
                // Filter
                rebuildEmployeeSelectionLists();
            }
        });

        //
        // EVENT LISTENER: Click Employee Selection
        //        
        lvES_ExistingEmployeeSelections.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<EmployeeSelection>() {
            @Override
            public void changed(ObservableValue<? extends EmployeeSelection> observable, EmployeeSelection oldValue, EmployeeSelection newValue) {
                if (newValue != null) {
                    // This is where we build the reporting structure for the TreeView
                    System.out.println("Selected EmployeeSelection item: " + newValue);

                    // Enable the Delete button
                    btnES_DeleteES.setDisable(false);
                    // Enable the Copy button
                    btnES_CopyES.setDisable(false);

                    // Enable Test Formula Button
                    btnES_TestFormula.setDisable(false);
                    // Enable the AddToFormula button
                    btnES_AddToFormula.setDisable(false);
                    // Enable dropdown choicebox for Filter
                    cbES_Filter.setDisable(false);
                    // Enable dropdown choicebox for Compare
                    cbES_Compare.setDisable(false);

                    // Display the formula
                    htmlSF_Editor.setHtmlText(convertStringToHTML(((EmployeeSelectionRule) newValue).getRule()));

                    // Display the name
                    tfES_ProcessName.setText(((EmployeeSelection) newValue).getName());
                }
            }
        });

        // 
        // EVENT LISTENER: Delete Employee Selection
        //
        btnES_DeleteES.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                // Did we select multiple items?
                EmployeeSelection selected = lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                deleteEmployeeSelectionFromCollection(lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItems());
            }
        }
        );

        //
        // EVENT LISTENER: Person pressed the DELETE button
        //
        lvES_ExistingEmployeeSelections.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                if (t.getCode() == KeyCode.DELETE) {
                    EmployeeSelection selected = lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                    deleteEmployeeSelectionFromCollection(lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItems());

                    t.consume();
                }
            }
        });

        // 
        // EVENT LISTENER: Click on NEW (New Employee Selection)
        //
        btnES_NewES.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String newName = Dialogs.showInputDialog(null, "Name:", "Create a new Employee Selection", "New Employee Selection");
                if (newName != null) {
                    if (newName.trim().length() > 0) {
                        // New employee Selection
                        EmployeeSelection newSelection = new EmployeeSelectionRule(newName, newName);

                        //
                        // Validate new name
                        //
                        int nbEmployeeSelections = localEmployeeSelections.size();
                        if (nbEmployeeSelections > 0) {
                            int i = 1;
                            showStatus("Testing Employee Selection: " + newSelection.toString(), i + 1, nbEmployeeSelections);
                            //
                            // Check that names differ
                            //
                            LinkedList<EmployeeSelection> localSelections = new LinkedList(localEmployeeSelections);
                            for (EmployeeSelection selectionInList : localSelections) {
                                // If hashCodes are the same rename the current reporting structure
                                if (newSelection.hashCode() == selectionInList.hashCode()) {
                                    // Ask user to change name
                                    // Edit a Reporting Structure
                                    // Edit dialog
                                    String nameOfCurrentSelection = newSelection.getName();
                                    String message = "Please rename:\n\n\t" + nameOfCurrentSelection + "\n\nto avoid errors in the employee selection.\n\n*If you do not rename the employee selection it will be renamed automatically.";

                                    String newSelectionName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name", nameOfCurrentSelection);
                                    System.out.println("New selection name:" + newSelectionName);
                                    System.out.println("Currrent selection name:" + nameOfCurrentSelection);

                                    // Rename Selection
                                    if (newSelectionName == null) {
                                        showStatus(null, 0, 0);
                                        return;
                                    }
                                    if ((newSelectionName.trim().length() == 0) || (newSelectionName.equals(nameOfCurrentSelection))) {
                                        newSelectionName = i - 1 + "_renamed_" + nameOfCurrentSelection;
                                    }
                                    // Save the selection
                                    newSelection.setName(newSelectionName);
                                    setContentsChanged(true);
                                }
                                i++;
                            }
                            showStatus(null, 0, 0);
                        }
                        localEmployeeSelections.add(newSelection);

                        rebuildEmployeeSelectionLists();
                        // Once created and added, select it
                        lvES_ExistingEmployeeSelections.getSelectionModel().select(newSelection);
                        lvES_ExistingEmployeeSelections.scrollTo(lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedIndex());
                        setContentsChanged(true);
                    }
                }
            }
        }
        );

        // 
        // EVENT LISTENER: Click on COPY (Copy Employee Selection)
        //
        btnES_CopyES.setOnAction(
                new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                EmployeeSelection toCopy = lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                if (toCopy != null) {

                    String newName = Dialogs.showInputDialog(null, "Name for new Employee Selection", "Copy", "Copy existing Employee Selection", toCopy.getName() + "(copy)");
                    if (newName != null) {
                        if (newName.trim().length() > 0) {
                            // Rename Employee Selection
                            EmployeeSelection newSelection = new EmployeeSelectionRule(newName, newName);
                            // Copy Rule
                            String rule = ((EmployeeSelectionRule) toCopy).getRule();
                            ((EmployeeSelectionRule) newSelection).setRule(rule);

                            //
                            // Validate new name
                            //
                            int nbEmployeeSelections = localEmployeeSelections.size();
                            if (nbEmployeeSelections > 0) {
                                int i = 1;
                                showStatus("Testing Employee Selection: " + newSelection.toString(), i + 1, nbEmployeeSelections);
                                //
                                // Check that names differ
                                //
                                LinkedList<EmployeeSelection> localSelections = new LinkedList(localEmployeeSelections);
                                localSelections.remove(newSelection);
                                for (EmployeeSelection selectionInList : localSelections) {
                                    // If hashCodes are the same rename the current reporting structure
                                    if (newSelection.hashCode() == selectionInList.hashCode()) {
                                        // Ask user to change name
                                        // Edit a Employee Selection
                                        // Edit dialog
                                        String nameOfCurrentSelection = newSelection.getName();
                                        String message = "Please rename:\n\n\t" + nameOfCurrentSelection + "\n\nto avoid errors in the employee selection.\n\n*If you do not rename the employee selection it will be renamed automatically.";

                                        String newSelectionName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name");

                                        // Rename Selection
                                        if ((newSelectionName == null) || (newSelectionName.trim().length() == 0)) {
                                            newSelectionName = i - 1 + "_renamed_" + nameOfCurrentSelection;
                                        }
                                        newSelection.setName(newSelectionName);
                                        setContentsChanged(true);
                                    }
                                    i++;
                                }
                                showStatus(null, 0, 0);
                            }
                            // Add to existing list
                            localEmployeeSelections.add(newSelection);

                            rebuildEmployeeSelectionLists();

                            // Once created and added, select it
                            lvES_ExistingEmployeeSelections.getSelectionModel().select(newSelection);
                            lvES_ExistingEmployeeSelections.scrollTo(lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedIndex());
                            setContentsChanged(true);
                        }
                    }
                } else {
                    Dialogs.showErrorDialog(null, "Please select an existing Employee Selection before trying to copy it.");

                }
            }
        }
        );

        //
        // EVENT LISTENER: Click on APPLY CHANGES
        //
        btnES_ApplyChanges.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                EmployeeSelection selected = lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                String originalName = selected.toString();
                if (selected != null) {
                    DialogResponse response = Dialogs.showConfirmDialog(null,
                            "Do you wish to apply changes made to '" + selected.getName() + "' ?", "Confirm change", "Change Employee Selection", Dialogs.DialogOptions.YES_NO);
                    if (response == DialogResponse.YES) {
                        // Update Name
                        int selHash = selected.hashCode();
                        String newName = tfES_ProcessName.getText();

                        // Steps through existing reporting structures, updating the name in those as well.
                        for (ReportingStructure structure : localReportingStructures) {
                            EmployeeSelection selInStructure = structure.getEmployeeSelection(selHash);
                            if (selInStructure != null) {
                                selInStructure.setName(newName);
                                ((EmployeeSelectionRule) selInStructure).setRule(stripHTML(htmlSF_Editor.getHtmlText()));
                            }
                        }

                        // Update the name of this EmployeeSelection 
                        selected.setName(newName);
                        //
                        // Validate new name
                        //
                        int nbEmployeeSelections = localEmployeeSelections.size();
                        if (nbEmployeeSelections > 0) {
                            int i = 1;
                            showStatus("Testing Employee Selection: " + selected.toString(), i + 1, nbEmployeeSelections);
                            //
                            // Check that names differ
                            //
                            LinkedList<EmployeeSelection> localSelections = new LinkedList(localEmployeeSelections);
                            localSelections.remove(selected);
                            for (EmployeeSelection selectionInList : localSelections) {
                                // If hashCodes are the same rename the current reporting structure
                                if (selected.hashCode() == selectionInList.hashCode()) {
                                    // Ask user to change name
                                    // Edit a Reporting Structure
                                    // Edit dialog
                                    String nameOfCurrentSelection = selected.getName();
                                    String message = "Please rename:\n\n\t" + nameOfCurrentSelection + "\n\nto avoid errors in the employee selection.\n\n*If you do not rename the employee selection it will be renamed automatically.";

                                    String newSelectionName = Dialogs.showInputDialog(getMainStage(), message, "The name already exists!", "Duplicate name", nameOfCurrentSelection);

                                    // Rename Structure
                                    if (newSelectionName != null) {
                                        if ((newSelectionName.trim().length() == 0) || (newSelectionName.equals(nameOfCurrentSelection))) {
                                            newSelectionName = i - 1 + "_renamed_" + nameOfCurrentSelection;
                                            selected.setName(newSelectionName);
                                            setContentsChanged(true);
                                        }
                                    } else if (newSelectionName == null) {
                                        selected.setName(originalName);
                                        setContentsChanged(true);
                                    }
                                    // Steps through existing reporting structures, updating the name in those as well.
                                    for (ReportingStructure structure : localReportingStructures) {
                                        EmployeeSelection selInStructure = structure.getEmployeeSelection(selHash);
                                        if (selInStructure != null) {
                                            selInStructure.setName(newSelectionName);
                                            ((EmployeeSelectionRule) selInStructure).setRule(stripHTML(htmlSF_Editor.getHtmlText()));
                                        }
                                    }
                                }
                                i++;
                            }
                            showStatus(null, 0, 0);
                        }

                        // Update Formula
                        ((EmployeeSelectionRule) selected).setRule(stripHTML(htmlSF_Editor.getHtmlText()));

                        rebuildEmployeeSelectionLists();

                        // Once created and added, select it
                        lvES_ExistingEmployeeSelections.getSelectionModel().select(selected);
                        lvES_ExistingEmployeeSelections.scrollTo(lvES_ExistingEmployeeSelections.getSelectionModel().getSelectedIndex());
                        setContentsChanged(true);
                    }
                }
            }
        }
        );
    }

    // </editor-fold>  
    //
    // <editor-fold defaultstate="collapsed" desc=" ${Menu Items} ">
    /**
     * Setup the components used in the Web Process Definitions
     */
    private void initMenuItems() {
        //
        // EVENT LISTENER: Save Configuration
        //
        mnuSaveConfiguration.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                saveToFileContainer();
            }
        });

        //
        // EVENT LISTENER: Validate Configuration
        //
        mnuValidateConfiguration.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                validateConfiguration();
            }
        });

        //
        // EVENT LISTENER: Export Configuration
        //
        mnuExportConfiguration.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                exportSetup();
            }
        });

        //
        // EVENT LISTENER: Exit
        //
        mnuClose.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                DialogResponse responseToExit = Dialogs.showConfirmDialog(null,
                        "Do you wish to exit?", "Confirm Close", "Close ESSConfig utility", Dialogs.DialogOptions.YES_NO);
                if (responseToExit == DialogResponse.YES) {
                    if (contentsChanged) {
                        DialogResponse responseToSave = Dialogs.showConfirmDialog(null,
                                "Do you wish to save your changes first?", "Save before closing", "Close ESSConfig utility", Dialogs.DialogOptions.YES_NO_CANCEL);
                        if (responseToSave == DialogResponse.YES) {
                            saveToFileContainer();
                        }
                        if (responseToSave == DialogResponse.CANCEL) {
                            return;
                        }
                    }
                    Platform.exit();

                }
            }
        }
        );

        //
        // EVENT LISTENER: Help - About
        //
        mnuAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Dialogs.showInformationDialog(getMainStage(), "Jar Version:" + ESSVersion.getInstance().getJarVersion(), "PeopleWare ESS Config utility", "About");
            }
        });
    }

    // </editor-fold> 
    //
    // <editor-fold defaultstate="collapsed" desc=" ${Private Methods} ">
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Could we try and load stuff here?
        loadFromFileContainer();
        observableWebProcessDefinitions = FXCollections.observableList(localWebProcessDefinitions);
        observableReportingStructures = FXCollections.observableList(localReportingStructures);

        // Setup the WebProcessesDefinition page
        initMenuItems();

//        Platform.runLater(new Runnable() {
//            @Override
//            public void run() {
                // add your init here
                initWebProcessDefinitionsTab();
                initReportingStructuresTab();
                initEmployeeSelectionsTab();
//            }
//        });
    }

    /**
     * Removes HTML tags from a string, returning a - hopefully - plain text
     * string
     *
     * @param htmlString
     * @return
     */
    public String stripHTML(String htmlString) {

        // Retain Spaces
        htmlString = htmlString.replaceAll(" ", "___");
        // Remove HTML tags/etc.
        String text = htmlString.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", "");
        // Put spaces back
        text = text.replaceAll("___", " ");
        return text;
    }

    /**
     * Returns the calling hierarchy from the selected EmployeeSelection through
     * to the top node
     *
     * @param structure
     * @param startingPoint
     */
    private String getHierarchyOfReportingStructure(ReportingStructure structure, EmployeeSelection startingPoint) {
        StringBuilder rslt = new StringBuilder();
        EmployeeSelection parent = startingPoint;
        while (parent != null) {
            rslt.append(parent.toString());
            parent = structure.getParentSelection(parent);
            if (parent != null) {
                rslt.append(" -> ");
            }
        }
        return rslt.toString();
    }

    /**
     * For each EmployeeSelection, create childItems for each EmployeeSelection
     * that reports to it
     *
     * @param reportingStructure
     * @param parentItem
     * @param treeView
     */
    private void buildChildrenForParentInTreeView(ReportingStructure reportingStructure, TreeItem parentItem, TreeView treeView) {
        EmployeeSelection parentSelection = (EmployeeSelection) parentItem.getValue();
        // Get children
        for (EmployeeSelection childSelection : reportingStructure.getChildEmployeeSelections(parentSelection)) {
            TreeItem<EmployeeSelection> childItem = new TreeItem<>(childSelection, null);
            childItem.setExpanded(true);
            parentItem.getChildren().add(childItem);

            // Iteratively create each child and its children
            buildChildrenForParentInTreeView(reportingStructure, childItem, treeView);
        }
    }

    /**
     * Creates a LinkedList<SelectionLink> of all the items that has to be
     * removed if a particular link has been removed. This is used to
     * recursively remove all the child links if a SelectionLink higher up in
     * the tree was removed
     *
     * @param employeeSelection
     * @param reportingStructure
     * @return
     */
    private LinkedList<SelectionLink> getLinksAndSubLinksToDelete(EmployeeSelection employeeSelection, ReportingStructure reportingStructure) {
        final LinkedList<SelectionLink> linksToDelete = new LinkedList<>();

        Iterator<SelectionLink> iterBranch = reportingStructure.getSelectionLinks().iterator();
        while (iterBranch.hasNext()) {
            SelectionLink selectionLink = iterBranch.next();
            if (selectionLink.getReportsTo().toString().compareTo(employeeSelection.toString()) == 0) {
                EmployeeSelection subEmployeeSelection = selectionLink.getReportsFrom();
                linksToDelete.addAll(getLinksAndSubLinksToDelete(subEmployeeSelection, reportingStructure));
                //iterBranch.remove();
                linksToDelete.add(selectionLink);
            }
        }

        // In case employeeSelection is a Leaf item, simply delete it
        Iterator<SelectionLink> iterLeaf = reportingStructure.getSelectionLinks().iterator();
        while (iterLeaf.hasNext()) {
            SelectionLink selectionLink = iterLeaf.next();
            if (selectionLink.getReportsFrom().toString().compareTo(employeeSelection.toString()) == 0) {
                //iterLeaf.remove();
                linksToDelete.add(selectionLink);
            }
        }
        return linksToDelete;
    }

    /**
     * Removes - recursively - this EmployeeSelection from the
     * ReportingStructure. It means that all EmployeeSelections 'under' this
     * selection needs to be removed too
     *
     * @param employeeSelection
     * @param reportingStructure
     */
    private void deleteEmployeeSelectionFromReportingStructure(EmployeeSelection employeeSelection, ReportingStructure reportingStructure) {
        LinkedList<SelectionLink> linksToDelete = getLinksAndSubLinksToDelete(employeeSelection, reportingStructure);

        reportingStructure.getSelectionLinks().removeAll(linksToDelete);
    }

    /**
     * Removes all the EmployeeSelections used in the selectedReportingStructure
     * from the items in the ListView
     *
     * @param selectedReportingStructure
     * @param listViewToRemoveEmployeeSelections
     */
    private LinkedList<EmployeeSelection> removeUsedSelectionsFromListView(ReportingStructure selectedReportingStructure, LinkedList<EmployeeSelection> listToRemoveItemsFrom) {
        if (selectedReportingStructure == null) {
            return listToRemoveItemsFrom;
        }

        if (listToRemoveItemsFrom.size() == 0) {
            return listToRemoveItemsFrom;
        }

        for (SelectionLink link : selectedReportingStructure.getSelectionLinks()) {
            removeEmployeeSelectionFromList(listToRemoveItemsFrom, link.getReportsFrom());
            removeEmployeeSelectionFromList(listToRemoveItemsFrom, link.getReportsTo());
        }
        return listToRemoveItemsFrom;
    }

    /**
     * Steps through the LinkedList of Employee Selections and removes the
     * nominated item
     *
     * @param listToRemoveItemsFrom
     * @param employeeSelection
     */
    private void removeEmployeeSelectionFromList(LinkedList<EmployeeSelection> listToRemoveItemsFrom, EmployeeSelection employeeSelection) {
        for (int i = 0; i < listToRemoveItemsFrom.size(); i++) {
            if (listToRemoveItemsFrom.get(i).equals(employeeSelection)) {
                listToRemoveItemsFrom.remove(i);
            }
        }
    }

    /**
     * Completely deletes the EmployeeSelections from the internal list of
     * EMployeeSelections.
     *
     * @param employeeSelections
     */
    private void deleteEmployeeSelectionFromCollection(ObservableList<EmployeeSelection> employeeSelections) {
        boolean isSingleSelection = (employeeSelections.size() == 1);
        int countInUsed = 0;
        String promptMessage;

        if (employeeSelections == null) {
            return;
        }

        //
        // Different message if more than one being deleted
        if (isSingleSelection) {
            promptMessage = "Do you wish to delete this Employee Selection?";
        } else {
            promptMessage = "Do you wish to delete these Employee Selections?";
        }
        //
        // Confirm deletion
        DialogResponse response = Dialogs.showConfirmDialog(null,
                promptMessage, "Confirm Deletion", "Delete", Dialogs.DialogOptions.YES_NO);
        //
        // YES, delete
        //
        if (response == DialogResponse.YES) {
            // Step through selection
            for (EmployeeSelection employeeSelection : employeeSelections) {

                if (employeeSelection != null) {
                    ReportingStructure selectionUsedIn = isEmployeeSelectionInUse(employeeSelection);
                    //
                    // Is this selection in use?
                    if (selectionUsedIn != null) {
                        countInUsed++;
                    } else {
                        // Clear this EmployeeSelection
                        //lvES_ExistingEmployeeSelections.getItems().remove(employeeSelection);
                        localEmployeeSelections.remove(employeeSelection);
                    }
                }
            }
            // Unselect Employee Selections
            lvES_ExistingEmployeeSelections.getSelectionModel().clearSelection();
            rebuildEmployeeSelectionLists();
            setContentsChanged(true);
            //
            if (countInUsed > 0) {
                Dialogs.showInformationDialog(getMainStage(), "One or more Employee Selections are still in used and cannot be deleted.");
            }
        }
    }

    /**
     * Finds the root node and starts the construction of the reporting
     * structure
     *
     * @param reportingStructure
     * @param treeView
     */
    private void buildReportingStructureInTreeView(ReportingStructure reportingStructure, TreeView treeView) {
        if (reportingStructure == null) {
            treeView.setRoot(null);
            return;
        }

        // Is there a top node?
        if (reportingStructure.getTopNodes().isEmpty()) {
            // Is there still something in the TreeView itself?
            if (treeView.getRoot() != null) {
                treeView.setRoot(null);
            }
            return;
        }
        TreeItem<EmployeeSelection> topItem = new TreeItem<>(reportingStructure.getTopNodes().get(0), null);
        topItem.setExpanded(true);
        treeView.setRoot(topItem);
        // Iteratively create each child and its children
        buildChildrenForParentInTreeView(reportingStructure, topItem, treeView);
    }

    /**
     * Used to see if an EmployeeSelection contains the subset in the
     * filterString text. This function is case-insensitive and performs the
     * search at any position.
     *
     * @param selection
     * @param filterString
     * @return
     */
    private boolean matchesFilter(String fullString, String filterString) {
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        return fullString.toLowerCase().indexOf(lowerCaseFilterString) != -1;
    }

    /**
     * Only shows the list of EmployeeSelections in the ReportingStructures tab
     * that adheres to the filter string AND remove the items that are already
     * in used in the current Reporting Structure
     *
     * @param selectedReportingStructure
     * @param filterText
     */
    private void rs_filterEmployeeSelectionsInListView(ReportingStructure selectedReportingStructure, String filterText) {

        int selIndex = lvRS_ExistingEmployeeSelections.getSelectionModel().getSelectedIndex();

        //1. Clear list
        llRS_filteredAndUnusedEmployeeSelections.clear();

        //2. Recreate the list adhering to the filtered text
        for (EmployeeSelection selection : localEmployeeSelections) {
            if ((filterText.trim().length() == 0)
                    || (matchesFilter(selection.toString(), filterText))) {
                llRS_filteredAndUnusedEmployeeSelections.add(selection);
            }
        }

        // Let's make sure we're only showning in this list what's not already in the TreeView
        if (selectedReportingStructure != null) {
            removeUsedSelectionsFromListView(selectedReportingStructure, llRS_filteredAndUnusedEmployeeSelections);
        }

        lvRS_ExistingEmployeeSelections.setItems(FXCollections.observableList(llRS_filteredAndUnusedEmployeeSelections));
        forceListRefreshOn(lvRS_ExistingEmployeeSelections);

        // Reselect the last item (or the topmost one)
        if (selIndex >= 0) {
            if (lvRS_ExistingEmployeeSelections.getItems().size() >= selIndex) {
                lvRS_ExistingEmployeeSelections.getSelectionModel().select(selIndex);

            }
        }

    }

    /**
     * Only shows the list of EmployeeSelections in the EmployeeSelections tab
     * that adheres to the filter string
     *
     * @param selectedReportingStructure
     * @param filterText
     */
    private void es_filterEmployeeSelectionsInListView(String filterText) {

        //1. Clear list
        llES_filteredEmployeeSelections.clear();

        //2. Recreate the list adhering to the filtered text
        for (EmployeeSelection selection : localEmployeeSelections) {
            if ((filterText.trim().length() == 0)
                    || (matchesFilter(selection.toString(), filterText))) {
                llES_filteredEmployeeSelections.add(selection);
            }
        }
        lvES_ExistingEmployeeSelections.setItems(FXCollections.observableList(llES_filteredEmployeeSelections));
        forceListRefreshOn(lvES_ExistingEmployeeSelections);
        // Unselect all
        lvES_ExistingEmployeeSelections.getSelectionModel().clearSelection();
    }

    /**
     * Updates the currently selected WebProcessDefinition with the properties
     * that had been changed on the screen
     */
    private void saveWebProcessDefinition(WebProcessDefinition webProcessDefinition) {
        // Set Max Process Age
        try {
            webProcessDefinition.setMaxProcessAge(new Integer(tfWebProcess_MaxDurationPerProcess.getText()).intValue());
        } catch (NumberFormatException e) {
            tfWebProcess_MaxDurationPerProcess.setText("24");
            webProcessDefinition.setMaxProcessAge(new Integer(tfWebProcess_MaxDurationPerProcess.getText()).intValue());

        }
        // Set Max Stage Age
        try {
            webProcessDefinition.setMaxStageAge(new Integer(tfWebProcess_MaxDurationPerStage.getText()).intValue());
        } catch (NumberFormatException e) {
            tfWebProcess_MaxDurationPerStage.setText("3");
            webProcessDefinition.setMaxStageAge(new Integer(tfWebProcess_MaxDurationPerStage.getText()).intValue());

        }
        // Set Reporting Structure
        ReportingStructure rs = cbWebProcess_ReportingStructure.getSelectionModel().getSelectedItem();
        webProcessDefinition.setReportingStructure(rs);
        // Set Min Start Day
        try {
            webProcessDefinition.setActiveFromDayOfMonth(new Integer(tfWebProcess_ActivateOn.getText()).intValue());
        } catch (NumberFormatException e) {
            tfWebProcess_ActivateOn.setText("0");
            webProcessDefinition.setActiveFromDayOfMonth(new Integer(tfWebProcess_ActivateOn.getText()).intValue());
        }
        // Set Latest Start Day
        try {
            webProcessDefinition.setActiveToDayOfMonth(new Integer(tfWebProcess_DeactivateOn.getText()).intValue());
        } catch (NumberFormatException e) {
            tfWebProcess_DeactivateOn.setText("0");
            webProcessDefinition.setActiveToDayOfMonth(new Integer(tfWebProcess_DeactivateOn.getText()).intValue());
        }

        // Notify on Activation
        webProcessDefinition.setNotifyOfPendingActivation(cbWebProcess_NotifyStaffOfActivation.isSelected());
        // Notify on Deactivation
        webProcessDefinition.setNotifyOfPendingDeactivation(cbWebProcess_NotifyStaffOfActivation.isSelected());
        // Supervisor process?
        webProcessDefinition.setCannotInstantiateSelf(cbWebProcess_IsSupervisor.isSelected());
    }

    /**
     * Forces a proper redraw of said ListView
     *
     * @param lsv
     */
    private void forceListRefreshOn(ListView lsv) {
        ObservableList items = lsv.getItems();
        lsv.setItems(null);
        lsv.setItems(items);
    }

    /**
     * Scans the Reporting Structures to see if this EmployeeSelection is being
     * used
     *
     * @param selection
     * @return
     */
    private ReportingStructure isEmployeeSelectionInUse(EmployeeSelection selection) {
        ReportingStructure rslt = null;
        for (ReportingStructure structure : localReportingStructures) {
            for (SelectionLink link : structure.getSelectionLinks()) {
                if ((link.getReportsFrom().toString().compareTo(selection.toString()) == 0)
                        || (link.getReportsTo().toString().compareTo(selection.toString()) == 0)) {
                    return structure;
                }
            }
        }

        return rslt;
    }

    /**
     * Loads the serialised classes (WebProcessDefinitions, ReportingStructures,
     * and EmployeeSelections) from the binary files
     */
    private void loadFromFileContainer() {
        // Employee Selections
        for (int i = 0; i < fileContainer.getEmployeeSelections().size(); i++) {
            this.localEmployeeSelections.add((EmployeeSelection) fileContainer.getEmployeeSelections().get(i));
        }

        // Reporting Structures
        for (int j = 0; j < fileContainer.getReportingStructures().size(); j++) {
            this.localReportingStructures.add((ReportingStructure) fileContainer.getReportingStructures().get(j));
        }

        // Process Definitions
        for (int k = 0; k < fileContainer.getWebProcessDefinitions().size(); k++) {
            this.localWebProcessDefinitions.add((WebProcessDefinition) fileContainer.getWebProcessDefinitions().get(k));
        }
    }

    /**
     * Displays a status text message at the bottom of the screen
     *
     * @param status
     */
    private void showStatus(String status, int value, int max) {
        if (status == null) {
            txtStatus.setText("Status: OK");
        } else {
            txtStatus.setText("Status: " + status);
        }

        pbProgress.setVisible((value + max) > 0);

        if (max > 0) {
            progressBarPosition.set(value / max);
        }
    }

    /**
     * This class generates a formatted StringBuffer, containing a detailed
     * description of the WebSetup content. a) List Existing Web Processes, each
     * with its properties b) List all the reporting structures c) List all the
     * Employee Selections with their formulas
     *
     * @includeEmployees Boolean property that, if set to true, returns a list
     * of all employees in each employee selection
     */
    private StringBuffer buildWebSetupContentInHTML(boolean includeEmployees) {

        StringBuffer result = new StringBuffer();

        // Title
        result.append("<h2>WebSetup Configuration</h2>");
        result.append("<font size=\"-2\">Print Date:").append(new java.util.Date()).append("</font>");
        result.append("<br><font size=\"-2\">JAR Version:").append(za.co.ucs.accsys.webmodule.ESSVersion.getInstance().getJarVersion()).append("</font>");

        // Existing Web Processes
        result.append("<br><b>Current Web Processes:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        result.append("<tr><td>Name</td><td>Max Stage Age</td><td>Max Process Age</td>");
        result.append("    <td>Reporting Structure</td><td>Activate On</td><td>Deactivate On</td><td>Supervisor Process</td></tr>");
        for (WebProcessDefinition wpd : localWebProcessDefinitions) {
            // Process Name
            result.append("<tr><td>").append(wpd.getName()).append("</td>");
            // Stage Age
            result.append("    <td>").append(wpd.getMaxStageAge()).append("</td>");
            // Process Age
            result.append("    <td>").append(wpd.getMaxProcessAge()).append("</td>");
            // Reporting Structure
            if (wpd.getReportingStructure() == null) {
                result.append("    <td>-- None --</td>");
            } else {
                result.append("    <td>").append(wpd.getReportingStructure()).append("</td>");
            }
            // Process Activates On
            if (wpd.getActiveFromDayOfMonth() == 0) {
                result.append("   <td colspan=2>Always Active</td>");
            } else {
                result.append("   <td>").append(wpd.getActiveFromDayOfMonth()).append("</td><td>").append(wpd.getActiveToDayOfMonth()).append("</td>");
            }

            // Is Supervisor Process?
            if (wpd.getCannotInstantiateSelf()) {
                result.append("    <td>Y</td></tr>");
            } else {
                result.append("    <td>N</td></tr>");
            }
        }
        result.append("</font>");
        result.append("</table>");

        // Existing Reporting Structures
        result.append("<br><b>Reporting Structures:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        for (ReportingStructure rep : localReportingStructures) {
            result.append("<tr><td width=\"13%\"><b>").append(rep.toString()).append("</b></td>");
            result.append("<td width=\"13%\"></td>");
            result.append("<td width=\"13%\"></td>");
            result.append("<td width=\"13%\"></td>");
            result.append("<td width=\"13%\"></td>");
            result.append("<td width=\"13%\"></td>");
            result.append("<td width=\"12%\"></td>");
            result.append("</tr>");

            // Get TopMost Levels
            ArrayList theTopNodes = rep.getTopNodes();
            Iterator nodeIter = theTopNodes.iterator();

            if (nodeIter.hasNext()) {
                // There can only be one TopNode
                EmployeeSelection topNode = (EmployeeSelection) nodeIter.next();
                result.append("<tr><td>").append(topNode.getName()).append("</td></tr>");

                LinkedList children1 = rep.getChildEmployeeSelections(topNode);
                Iterator children1Iter = children1.iterator();
                while (children1Iter.hasNext()) {
                    EmployeeSelection child1 = (EmployeeSelection) children1Iter.next();
                    result.append("<tr><td></td><td>").append(child1.getName()).append("</td></tr>");

                    LinkedList children2 = rep.getChildEmployeeSelections(child1);
                    Iterator children2Iter = children2.iterator();
                    while (children2Iter.hasNext()) {
                        EmployeeSelection child2 = (EmployeeSelection) children2Iter.next();
                        result.append("<tr><td></td><td></td><td>").append(child2.getName()).append("</td></tr>");

                        LinkedList children3 = rep.getChildEmployeeSelections(child2);
                        Iterator children3Iter = children3.iterator();
                        while (children3Iter.hasNext()) {
                            EmployeeSelection child3 = (EmployeeSelection) children3Iter.next();
                            result.append("<tr><td></td><td></td><td></td><td>").append(child3.getName()).append("</td></tr>");

                            LinkedList children4 = rep.getChildEmployeeSelections(child3);
                            Iterator children4Iter = children4.iterator();
                            while (children4Iter.hasNext()) {
                                EmployeeSelection child4 = (EmployeeSelection) children4Iter.next();
                                result.append("<tr><td></td><td></td><td></td><td></td><td>").append(child4.getName()).append("</td></tr>");

                                LinkedList children5 = rep.getChildEmployeeSelections(child4);
                                Iterator children5Iter = children5.iterator();
                                while (children5Iter.hasNext()) {
                                    EmployeeSelection child5 = (EmployeeSelection) children5Iter.next();
                                    result.append("<tr ><td></td><td></td><td></td><td></td><td></td><td>").append(child5.getName()).append("</td></tr>");

                                    LinkedList children6 = rep.getChildEmployeeSelections(child5);
                                    Iterator children6Iter = children6.iterator();
                                    while (children6Iter.hasNext()) {
                                        EmployeeSelection child6 = (EmployeeSelection) children6Iter.next();
                                        result.append("<tr ><td></td><td></td><td></td><td></td><td></td><td></td><td>").append(child6.getName()).append("</td></tr>");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        result.append("</font>");
        result.append("</table>");

        // Existing Employee Selections
        result.append("<br><b>Employee Selections:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        result.append("<tr ><td><b>Name</b></td><td><b>Rule</b></td></tr>");
        Iterator esIter = localEmployeeSelections.iterator();
        while (esIter.hasNext()) {
            EmployeeSelectionRule es = (EmployeeSelectionRule) esIter.next();
            result.append("<tr ><td>").append(es.getName()).append("</td>");
            result.append("    <td>").append(es.getRule()).append("</td></tr>");
        }
        result.append("</font>");
        result.append("</table>");

        // Listing all employees in Employee Selections
        if (includeEmployees) {
            result.append("<br><b>Detailed Employee Selections:</b>");
            result.append("<table width=\"100%\" border=\"1\">");
            result.append("<font size=\"-3\">");
            result.append("<tr ><td><b>Rule Name</b></td><td><b>Employees</b></td></tr>");
            Iterator ruleIter = localEmployeeSelections.iterator();
            while (ruleIter.hasNext()) {
                EmployeeSelectionRule es = (EmployeeSelectionRule) ruleIter.next();
                result.append("<tr ><td>").append(es.getName()).append("</td>");
                result.append("    <td>");
                Iterator employeeIterator = SelectionInterpreter.getInstance().generateEmployeeList(es, false).iterator();
                int counter = 0;
                while (employeeIterator.hasNext()) {
                    za.co.ucs.accsys.peopleware.Employee employee = (za.co.ucs.accsys.peopleware.Employee) employeeIterator.next();
                    result.append(employee.toString());
                    // Lists 4 employees in one row before forcing a <br> in the HTML file
                    if (counter == 4) {
                        result.append("<br>");
                        counter = 0;
                    } else {
                        result.append("; ");
                    }
                    counter++;
                }
                result.append("</td></tr>");
            }
            result.append("</font>");
            result.append("</table>");
        }

        return result;

    }

    /**
     * Exports the current configuration into an HTML format
     */
    private void exportSetup() {

        // Construct the contents of the StringBuffer that will saved
        StringBuffer htmlContents = buildWebSetupContentInHTML(true);

        FileChooser fileChooser = new FileChooser();
        //fileChooser.
        fileChooser.setTitle("Save ESS Setup");
        //fileChooser.setInitialFileName("ESSsetupExport.html");
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("HTML files (*.html)", "*.html");
        fileChooser.getExtensionFilters().add(extFilter);
        //Set to user directory or go to default if cannot access
        String userDirectoryString = System.getProperty("user.dir");
        File userDirectory = new File(userDirectoryString);
        if (!userDirectory.canRead()) {
            userDirectory = new File("c:/");
        }
        fileChooser.setInitialDirectory(userDirectory);
        //Show save file dialog
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            if (file.getName().contains(".html")) {
                // filename is OK as-is
            } else {
                file = new File(file.toString() + ".html");  // append .xml if "foo.jpg.xml" is OK
            }
            SaveFile(htmlContents.toString(), file);
        } else {
            Dialogs.showErrorDialog(getMainStage(), "Export of ESS Setup file failed", "Export failed", "Failed to save file");
        }
    }

    private void SaveFile(String content, File file) {
        try {
            FileWriter fileWriter = null;

            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
            Dialogs.showInformationDialog(getMainStage(), "File export completed", "Export Completed", "Completed");
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Runs through a series of tests to make sure the reporting structures,
     * employee selections, and web process definitions are all sound and valid
     *
     * @return
     */
    private boolean validateConfiguration() {

        //***************************
        // Employee Selections
        //***************************
        // Let's test each selection
        LinkedList<EmployeeSelection> selectionsToDelete = new LinkedList<>();
        LinkedList<EmployeeSelection> usedSelectionsWithoutEmployees = new LinkedList<>();

        showStatus("Testing Employee Selections...", 0, 0);
        boolean allOK = false;
        int i = 1;

        Iterator iter = localEmployeeSelections.iterator();
        int totalSize = localEmployeeSelections.size();

        if (totalSize > 0) {
            while (!allOK) {
                i = 1;
                while (iter.hasNext()) {
                    allOK = true;  // Resetting the boolean status so that if everything evalautes correctly, it will not run through this loop more than once.

                    EmployeeSelectionRule selection = (EmployeeSelectionRule) iter.next();
                    // Let's test each selection during the saving
                    showStatus("Testing Employee Selection: " + selection.getName(), i, totalSize);
                    i++;

                    if (SelectionInterpreter.getInstance().generateEmployeeList(selection, false).isEmpty()) {
                        ReportingStructure selectionUsedIn = isEmployeeSelectionInUse(selection);
                        // Empty Employee Selection not used in any reporting structure - allow to delete it
                        if (selectionUsedIn == null) {
                            DialogResponse response = Dialogs.showConfirmDialog(null, "The Employee Selection:'" + selection.getName() + "'\ndid not return any values."
                                    + "\n\nIt is not being used in any reporting structure.\nDo you wish to delete it?",
                                    "Empty Employee Selection", "Warning", Dialogs.DialogOptions.YES_NO);
                            if (response == DialogResponse.YES) {
                                selectionsToDelete.add(selection);
                            }
                        } else {
                            usedSelectionsWithoutEmployees.add(selection);
                        }
                    }

                    //
                    // Check that names differ
                    //
                    LinkedList<EmployeeSelection> localSelections = new LinkedList(localEmployeeSelections);
                    //System.out.println(i - 2);
                    localSelections.remove(i - 2);
                    for (EmployeeSelection selectionInList : localSelections) {
                        //System.out.println("selection hashCode: " + selection.hashCode());
                        //System.out.println("selectionInList hashCode: " + selectionInList.hashCode());
                        // If hashCodes are the same rename the current employee selection
                        if (selection.hashCode() == selectionInList.hashCode()) {
                            allOK = false;
                            // Ask user to change name
                            // Edit a Reporting Structure
                            // Edit dialog
                            String nameOfCurrentSelection = selection.getName();
                            String oldName = selectionInList.getName();
                            String message = "The hashcodes produced for '" + nameOfCurrentSelection + "' and '" + oldName + "' are the same.\nPlease rename '" + nameOfCurrentSelection + "' to avoid errors in the employee selection.\n*If you do not rename the selection it will be renamed automatically.";

                            String newSelectoinName = Dialogs.showInputDialog(getMainStage(), message, "Duplicate Hashcodes");

                            // Rename Selection
                            if ((newSelectoinName == null) || (newSelectoinName.trim().length() == 0)) {
                                newSelectoinName = i - 2 + "_renamed_" + nameOfCurrentSelection;
                            }
                            selection.setName(newSelectoinName);
                            setContentsChanged(true);
                        }

                    }
                }
            }
        }

        localEmployeeSelections.removeAll(selectionsToDelete);
        setContentsChanged(true);

        // Warn the user of EMPTY employee selections
        if (usedSelectionsWithoutEmployees.size() > 0) {
            String header;
            int counter = 0;
            // Empty Employee Selection, but used in a reporting structure.  Simply inform the user
            StringBuilder selectionsWithNoEmployees = new StringBuilder();
            for (EmployeeSelection sel : usedSelectionsWithoutEmployees) {
                counter++;
                if (counter <= 10) {
                    selectionsWithNoEmployees.append("\n").append(sel.getName());
                }
            }
            if (counter >= 10) {
                header = "A number of Employee Selections (this list is incomplete) are empty but cannot be deleted as they are linked to reporting structures";
            } else if (counter == 1) {
                header = "The following Employee Selection is empty but cannot be deleted as it is linked to reporting structures";
            } else {
                header = "The following Employee Selections are empty but cannot be deleted as they are linked to reporting structures";
            }

            Dialogs.showWarningDialog(getMainStage(), selectionsWithNoEmployees.toString(), header);
        }

        rebuildEmployeeSelectionLists();

        //***************************
        // Reporting Structures
        //***************************
        // First of all, we need to make sure that the objects stored in Employee Selections are the same ones stored in the links of Reporting Structures
        //updateSelectionClassesInReportingStructures();
        showStatus("Testing Reporting Structures...", 0, 0);

        allOK = false;
        int nbReportingStructures = localReportingStructures.size();
        if (nbReportingStructures > 0) {
            while (!allOK) {
                iter = localReportingStructures.iterator();
                i = 1;
                while (iter.hasNext()) {
                    allOK = true;  // Resetting the boolean status so that if everything evalautes correctly, it will not run through this loop more than once.

                    ReportingStructure structure = (ReportingStructure) iter.next();
                    showStatus("Testing Reporting Structure: " + structure.toString(), i + 1, nbReportingStructures);
                    i++;

                    //
                    // Check that names differ
                    //
                    LinkedList<ReportingStructure> localStructures = new LinkedList(localReportingStructures);
                    //System.out.println(i - 2);
                    localStructures.remove(i - 2);
                    for (ReportingStructure structureInList : localStructures) {
                        //System.out.println("structure hashCode: " + structure.hashCode());
                        //System.out.println("structureInList hashCode: " + structureInList.hashCode());
                        // If hashCodes are the same rename the current reporting structure
                        if (structure.hashCode() == structureInList.hashCode()) {
                            allOK = false;
                            // Ask user to change name
                            // Edit a Reporting Structure
                            // Edit dialog
                            String nameOfCurrentStructure = structure.getName();
                            String oldName = structureInList.getName();
                            String message = "The hashcodes produced for '" + nameOfCurrentStructure + "' and '" + oldName + "' are the same.\nPlease rename '" + nameOfCurrentStructure + "' to avoid errors in the reporting structure.\n*If you do not rename the structure it will be renamed automatically.";

                            String newStructureName = Dialogs.showInputDialog(getMainStage(), message, "Duplicate Hashcodes");

                            // Rename Structure
                            if ((newStructureName == null) || (newStructureName.trim().length() == 0)) {
                                newStructureName = i - 2 + "_renamed_" + nameOfCurrentStructure;
                            }
                            structure.setName(newStructureName);
                            setContentsChanged(true);
                        }

                    }

                    //
                    // Check if there is more than one 'Top Node', in which case we have a problem
                    //
                    ArrayList topLinkNodes = structure.getTopNodes();
                    if (topLinkNodes.size() > 1) {
                        Dialogs.showWarningDialog(getMainStage(), "Reporting Structure '" + structure.toString() + "' contains more than one Top Node."
                                + "\nThis could have been caused by errenous deletion of selection formulas."
                                + "\nAll but the first of these nodes will be deleted.");
                        for (int j = 1; j < topLinkNodes.size(); j++) {
                            EmployeeSelection topNodeSelection = (EmployeeSelection) topLinkNodes.get(j);

                            LinkedList links = structure.getSelectionLinks();
                            for (int k = 0; k < links.size(); k++) {
                                SelectionLink link = (SelectionLink) links.get(k);
                                // See if this link exist
                                EmployeeSelection selFrom = (EmployeeSelection) link.getReportsFrom();
                                EmployeeSelection selTo = (EmployeeSelection) link.getReportsTo();
                                if (selFrom.equals(topNodeSelection) || selTo.equals(topNodeSelection)) {
                                    structure.removeSelectionLink(link);
                                    setContentsChanged(true);
                                    allOK = false;
                                }
                            }
                        }
                    }
                }

            }
        }

        //***************************
        // Web Processes
        //***************************
        showStatus("Testing Web Processes...", 0, 0);
        for (int w = 0; w < localWebProcessDefinitions.size(); w++) {
            WebProcessDefinition def = (WebProcessDefinition) localWebProcessDefinitions.get(w);
            ReportingStructure structure = (ReportingStructure) def.getReportingStructure();
            for (int s = 0; s < localReportingStructures.size(); s++) {
                ReportingStructure localStructure = (ReportingStructure) localReportingStructures.get(s);
                if (localStructure.equals(structure)) {
                    if (localStructure.getSelectionLinks().size() != structure.getSelectionLinks().size()) {
                        def.setReportingStructure(localStructure);
                        setContentsChanged(true);
                    }
                }
            }
        }

        if (contentsChanged) {
            Dialogs.showInformationDialog(getMainStage(), "Validation complete.  Changes were made so be sure to save before exiting the application.", "Validation Complete");
        } else {
            Dialogs.showInformationDialog(getMainStage(), "Validation complete.  No errors reported.");
        }

        showStatus(null, 0, 0);
        return true;
    }

    /**
     * Whenever changes warranting the saving of the configuration has occurred,
     * we'll indicate it here.
     */
    private void setContentsChanged(boolean hasChanged) {
        contentsChanged = hasChanged;
        if (hasChanged) {
            getMainStage().setTitle(appTitle + " *");
        } else {
            getMainStage().setTitle(appTitle);
        }

    }

    private Stage getMainStage() {
        return (Stage) root.getScene().getWindow();
    }

    /**
     * Saves all the configurations (ProcessDefinitions, ReportingStructures and
     * EmployeeSelections) back into the FileContainer's binary files
     */
    public final void saveToFileContainer() {
        // Employee Selections
        System.out.println("------------------------- START ---------------------------------");
        ArrayList employeeSelections = fileContainer.getEmployeeSelections();
        System.out.println("-------------------------    employeeSelections = " + employeeSelections.size());
        employeeSelections.clear();

        updateSelectionClassesInReportingStructures();
        for (int i = 0; i < localEmployeeSelections.size(); i++) {
            // Let's test each selection during the saving
            employeeSelections.add(localEmployeeSelections.get(i));
        }
        fileContainer.setEmployeeSelections(employeeSelections);

        // Reporting Structures
        ArrayList reportingStructures = fileContainer.getReportingStructures();
        reportingStructures.clear();

        for (int j = 0; j < localReportingStructures.size(); j++) {
            reportingStructures.add(localReportingStructures.get(j));
        }
        fileContainer.setReportingStructures(reportingStructures);

        // Process Definitions
        ArrayList processDefinitions = fileContainer.getWebProcessDefinitions();
        processDefinitions.clear();

        for (int k = 0; k < localWebProcessDefinitions.size(); k++) {
            processDefinitions.add(localWebProcessDefinitions.get(k));
            recursiveUpdateWebProcessDefinition((WebProcessDefinition) localWebProcessDefinitions.get(k));
        }
        fileContainer.setWebProcessDefinitions(processDefinitions);
        fileContainer.saveWebSetupDetail();

        System.out.println("Saved");

        // Reset Contents Property
        setContentsChanged(false);
    }

    /**
     * When someone makes changes to rules in EmployeeSelections, the
     * ReportingStructure classes that uses those rules should be updated too
     */
    private void updateSelectionClassesInReportingStructures() {
        // First of all, we need to make sure that the objects stored in Employee Selections are the same ones stored in the links of Reporting Structures
        for (int repIdx = 0; repIdx < localReportingStructures.size(); repIdx++) {
            ReportingStructure rep = (ReportingStructure) localReportingStructures.get(repIdx);
            for (int lnkIdx = 0; lnkIdx < rep.getSelectionLinks().size(); lnkIdx++) {
                SelectionLink lnk = (SelectionLink) rep.getSelectionLinks().get(lnkIdx);
                String fromName = lnk.getReportsFrom().getName().trim();
                String toName = lnk.getReportsTo().getName().trim();
                // Find the EmployeeSelection with this name and update the selectionLink accordingly
                for (int esIdx = 0; esIdx < localEmployeeSelections.size(); esIdx++) {
                    if (((EmployeeSelectionRule) localEmployeeSelections.get(esIdx)).getName().trim().equals(fromName)) {
                        lnk.setReportsFrom((EmployeeSelectionRule) localEmployeeSelections.get(esIdx));
                    }
                    if (((EmployeeSelectionRule) localEmployeeSelections.get(esIdx)).getName().trim().equals(toName)) {
                        lnk.setReportsTo((EmployeeSelectionRule) localEmployeeSelections.get(esIdx));
                    }
                }
            }
        }
    }

    /**
     * Updates the currently selected WebProcessDefinition when changes have
     * been made to selections or structures elsewhere
     */
    private void recursiveUpdateWebProcessDefinition(WebProcessDefinition webProcessDefinition) {
        // Set Max Process Age
        try {
            webProcessDefinition.setMaxProcessAge(webProcessDefinition.getMaxProcessAge());
        } catch (NumberFormatException e) {
            tfWebProcess_MaxDurationPerProcess.setText("24");
            webProcessDefinition.setMaxProcessAge(Integer.parseInt(tfWebProcess_MaxDurationPerProcess.getText()));

        }
        // Set Max Stage Age
        try {
            webProcessDefinition.setMaxStageAge(webProcessDefinition.getMaxStageAge());
        } catch (NumberFormatException e) {
            tfWebProcess_MaxDurationPerStage.setText("3");
            webProcessDefinition.setMaxStageAge(new Integer(tfWebProcess_MaxDurationPerStage.getText()).intValue());

        }

        if (webProcessDefinition.getReportingStructure() != null) {
            String hashString = "" + webProcessDefinition.getReportingStructure().hashCode();
            webProcessDefinition.setReportingStructure((ReportingStructure) fileContainer.getReportingStructure(hashString));
            // Set Reporting Structure
            // Set Min Start Day
            try {
                webProcessDefinition.setActiveFromDayOfMonth(webProcessDefinition.getActiveFromDayOfMonth());
            } catch (NumberFormatException e) {
                tfWebProcess_ActivateOn.setText("0");
                webProcessDefinition.setActiveFromDayOfMonth(new Integer(tfWebProcess_ActivateOn.getText()).intValue());
            }
            // Set Latest Start Day
            try {
                webProcessDefinition.setActiveToDayOfMonth(webProcessDefinition.getActiveToDayOfMonth());
            } catch (NumberFormatException e) {
                tfWebProcess_DeactivateOn.setText("0");
                webProcessDefinition.setActiveToDayOfMonth(new Integer(tfWebProcess_DeactivateOn.getText()).intValue());
            }

        }

    }

    /**
     * Rebuilds the ListViews of EmployeeSelections
     */
    private void rebuildEmployeeSelectionLists() {
        // ----------------------------------------------
        // Employee Selections in Reporting Structure Tab
        // ----------------------------------------------
        ReportingStructure selectedStructure = lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem();
        // Filter on the filter box (if any)
        rs_filterEmployeeSelectionsInListView(selectedStructure, tfRS_FilterEmployeeSelections.getText());
        // Order the list
        orderReportingStructureList(lvRS_ExistingReportingStructures);
        orderEmployeeSelectionList(lvRS_ExistingEmployeeSelections);

        // ----------------------------------------------
        // Employee Selections in Employee Selections Tab
        // ----------------------------------------------
        // Filter on the filter box (if any)
        es_filterEmployeeSelectionsInListView(tfES_FilterEmployeeSelections.getText());
        // Order the list
        orderEmployeeSelectionList(lvES_ExistingEmployeeSelections);

        //-----------------------------------------------------
        // Employee Selections in Reporting Structure TreeView
        //-----------------------------------------------------
        buildReportingStructureInTreeView(lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem(), tvRS_ReportingStructureTreeView);

    }

    /**
     * Orders the ListViews of ReportingStructures
     */
    private void orderReportingStructureList(ListView<ReportingStructure> theList) {
        // Order the list
        Collections.sort(theList.getItems(), new Comparator<ReportingStructure>() {

            @Override
            public int compare(ReportingStructure lhs, ReportingStructure rhs) {

                return lhs.getName().compareTo(rhs.getName());

            }
        });
    }

    /**
     * Rebuilds the ListViews of EmployeeSelections
     */
    private void orderEmployeeSelectionList(ListView<EmployeeSelection> theList) {
        // Order the list
        Collections.sort(theList.getItems(), new Comparator<EmployeeSelection>() {

            @Override
            public int compare(EmployeeSelection lhs, EmployeeSelection rhs) {

                return lhs.getName().compareTo(rhs.getName());

            }
        });
    }

    private String convertStringToHTML(String string) {
        if (string == null) {
            return "";
        } else {
            StringTokenizer token = new StringTokenizer(string, " ()[]\n", true);
            String newString = "";
            while (token.hasMoreTokens()) {

                String word = token.nextToken();
                //if (word.compareTo(" ")==0)
                //    continue;
                if (isKeyWord(word)) {
                    newString = newString + "<font color=\"#009900\">" + word + "</font>";
                    continue;
                }
                if (isComparator(word)) {
                    newString = newString + "<font color=\"#0000ff\">" + word + "</font>";
                    continue;
                }
                if (isBrackets(word)) {
                    newString = newString + "<font color=\"#800000\"><b>" + word + "</b></font>";
                    continue;
                }
                if (isLineBreak(word)) {
                    newString = newString + "<br>";
                    continue;
                }
                if (isInQuotes(word)) {
                    newString = newString + "<font color=\"#ff0000\">" + word + "</font>";
                    continue;
                }
                if (isCombiners(word)) {
                    newString = newString + "<font color=\"#888888\">" + word.toUpperCase() + "</font>";
                    continue;
                }
                if (isSquareBraces(word)) {
                    newString = newString + "<font color=\"#000000\"><b>" + word.toUpperCase() + "</b></font>";
                    continue;
                }
                // Empty Space
                if (word.compareTo(" ") == 0) {
                    newString = newString + " ";
                    continue;
                }
                // Anything else, print as WRONG by drawing a line through it
                //newString = newString + "<font color=\"#ff0000\"><i><strike>" + word + "</strike></i></font>";
                newString = newString + word;
            }
            //return ("<pre>" + newString + "</pre>");
            return newString;
        }
    }

    private boolean isInQuotes(String string) {
        return (string.indexOf("'") >= 0);
    }

    private boolean isLineBreak(String string) {
        return (string.indexOf("\n") == 0);
    }

    private boolean isBrackets(String string) {
        return ((string.indexOf("(") + string.indexOf(")")) >= -1);
    }

    private boolean isSquareBraces(String string) {
        return ((string.indexOf("[") + string.indexOf("]")) >= -1);
    }

    /**
     * Traverses through the tree, first finding the sParentItem, and then scans
     * its nodes to see if sChildItem exists under it
     *
     * @param sParentItem
     * @param sChildItem
     * @return
     */
    private boolean isDecendentOf(TreeView treeView, String sParentItem, String sChildItem) {

        // Root Node
        TreeItem rootItem = treeView.getRoot();
        if (rootItem == null) {
            return false;
        }
        // Find the Parent Item
        TreeItem parentItem = locateInTreeView(rootItem, sParentItem);
        if (parentItem == null) {
            return false;
        }
        // Find possible Child Item
        TreeItem childItem = locateInTreeView(parentItem, sChildItem);

        return (childItem != null);
    }

    private boolean isKeyWord(String string) {
        String[] keywords = EmployeeSelectionRule.getKeyWords();
        for (String keyword : keywords) {
            if (string.toUpperCase().indexOf(keyword.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }

    private boolean isComparator(String string) {
        String[] comparators = EmployeeSelectionRule.Comparators;
        for (String comparator : comparators) {
            if (string.toUpperCase().indexOf(comparator.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }

    private boolean isCombiners(String string) {
        String[] combiners = EmployeeSelectionRule.getCombiners();
        for (String combiner : combiners) {
            if (string.toUpperCase().indexOf(combiner.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds the first TreeItem that fully matches 'valueSearch'
     *
     * @param currentNode
     * @param valueToSearch
     * @return
     */
    private TreeItem<EmployeeSelection> locateInTreeView(final TreeItem<EmployeeSelection> currentNode, final String valueToSearch) {
        TreeItem<EmployeeSelection> result = null;
        String nodeString = currentNode.getValue().toString();
        if (nodeString.toString().trim().compareToIgnoreCase(valueToSearch.trim()) == 0) {
            result = currentNode;
        } else if (!currentNode.isLeaf()) {
            for (TreeItem<EmployeeSelection> child : currentNode.getChildren()) {
                result = locateInTreeView(child, valueToSearch);
                if (result != null) {
                    break;
                }
            }
        }
        return result;

    }

    /**
     * Finds the first TreeItem that fully matches 'valueSearch'
     *
     * @param valueToSearch
     * @return
     */
    private int getTreeItemLevel(TreeItem<EmployeeSelection> item) {
        int result = 1;
        while (item.getParent() != null) {
            result++;
            item = item.getParent();
        }
        return result;

    }

    /**
     * Returns a list of comparators used when constructing selection rules
     *
     * @return
     */
    private String[] getSelectionComparators() {
        String[] result = {"Equals", "IsGreaterThan", "IsLessThan", "IsLike", "IsUnLike", "IsNotEqualTo", "IsEqualOrGreaterThan", "IsEqualOrLessThan"};

        return result;

    }

    private boolean isRSNameUnique(ReportingStructure rs) {
        //
        // Check that names differ
        //
        LinkedList<ReportingStructure> localStructures = new LinkedList();
        localStructures.addAll(observableReportingStructures);
        for (ReportingStructure structureInList : localStructures) {
            // If hashCodes are the same rename the current reporting structure
            if (rs.hashCode() == structureInList.hashCode()) { // The name already exists
                return false;
            }
        }
        return true;
    }
    // </editor-fold> 
    //
    // <editor-fold defaultstate="collapsed" desc=" ${Drag-n-Drop Tree Cell} ">

    /**
     * This internal class DnDCell is used to manage the Drag-And-Drop behaviour
     * of the TreeItems in the TreeView
     */
    private class DnDCell extends TreeCell<EmployeeSelection> {

        private TreeView<EmployeeSelection> parentTree;
        private ReportingStructure controllingReportingStructure;

        public DnDCell(final TreeView<EmployeeSelection> parentTree, final ReportingStructure controllingReportingStructure) {
            this.parentTree = parentTree;
            this.controllingReportingStructure = controllingReportingStructure;
            final String listViewClassName = "ListView[id=lvRS_ExistingEmployeeSelections, styleClass=list-view]";
            // 
            // Handles OnDragDetected on the TreeView for items originating
            // in the TreeView
            //
            setOnDragDetected(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println("Drag detected on " + item);
                    if (item == null) {
                        return;
                    }
                    Dragboard dragBoard = startDragAndDrop(TransferMode.MOVE);
                    ClipboardContent content = new ClipboardContent();
                    content.put(DataFormat.PLAIN_TEXT, item.toString());
                    dragBoard.setContent(content);
                    event.consume();
                }
            });
            // 
            // Handles OnDragDone on the TreeView for items originating
            // in the TreeView or the ExistingEmployeeSelections ListView
            //
            setOnDragDone(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent dragEvent) {
                    System.out.println("Drag done on " + item);
                    dragEvent.consume();

                }
            });

            // 
            // Handles OnDragOver on the TreeView for items originating
            // in the TreeView or the ExistingEmployeeSelections ListView
            //
            setOnDragOver(new EventHandler<DragEvent>() {

                @Override
                public void handle(DragEvent dragEvent) {

                    if (dragEvent.getDragboard().hasString()) {
                        String sItemToMove;
                        EmployeeSelection itemToMove;
                        System.out.println("Gesture Source:" + dragEvent.getGestureSource().toString());
                        // Did we drag from the Reporting Structure ListView?
                        if (dragEvent.getGestureSource().toString().compareToIgnoreCase(listViewClassName) == 0) {

                            itemToMove = lvRS_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                            sItemToMove = itemToMove.toString();
                            System.out.println("Move from ListView: " + sItemToMove);

                        } else {

                            sItemToMove = dragEvent.getDragboard().getString();
                            System.out.println("Move within TreeView: " + sItemToMove);
                            itemToMove = locateInTreeView(tvRS_ReportingStructureTreeView.getRoot(), sItemToMove).getValue();
                        }

                        if (item != null) {
                            if (itemToMove != null) {
                                if ((EmployeeSelection) itemToMove != item) {
                                    if (!isDecendentOf(tvRS_ReportingStructureTreeView, sItemToMove, item.toString())) // We accept the transfer!!!!!
                                    {
                                        System.out.println("Item to receive: " + item.toString());
                                        dragEvent.acceptTransferModes(TransferMode.MOVE);

                                        // Highlight node
                                        setStyle(dragDropHighlighStyle);
                                    }
                                }
                            }
                        } else // We can only have item == null if the Reporting Structure is still empty
                        {
                            if (lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem().getSelectionLinks().isEmpty()) {
                                System.out.println("Item to receive - ROOT");
                                dragEvent.acceptTransferModes(TransferMode.MOVE);
                            }
                        }
                    }
                    dragEvent.consume();
                }
            }
            );

            // 
            // Handles OnDragDropped on the TreeView for items originating
            // in the TreeView or the ExistingEmployeeSelections ListView
            //
            setOnDragDropped(
                    new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent dragEvent
                ) {

                    EmployeeSelection selectionToMove;
                    String valueToMove = dragEvent.getDragboard().getString();
                    // Did we drag from the Reporting Structure ListView?
                    if (dragEvent.getGestureSource().toString().compareToIgnoreCase(listViewClassName) == 0) {
                        //
                        // Dragged from the ListView
                        //
                        selectionToMove = lvRS_ExistingEmployeeSelections.getSelectionModel().getSelectedItem();
                        System.out.println("Move from ListView: " + valueToMove);

                    } else {
                        //
                        // Dragged from the TreeView
                        //
                        selectionToMove = locateInTreeView(tvRS_ReportingStructureTreeView.getRoot(), dragEvent.getDragboard().getString()).getValue();
                        System.out.println("Move within TreeView: " + valueToMove);
                    }

                    if (item != null) {
                        System.out.println("Drag dropped on " + item);
                        // Make sure we don't do more than 6 levels
                        int level = getTreeItemLevel(locateInTreeView(tvRS_ReportingStructureTreeView.getRoot(), item.toString()));
                        if (level >= 6) {
                            Dialogs.showWarningDialog(getMainStage(), "Reporting structures can be a maximum of 6 levels deep", "Maximum of 6 levels reached");
                            System.out.println("Maximum of 6 levels reached");
                            dragEvent.consume();
                            return;
                        }

                        // This item could have originated from the ListView (not already in the reporting structure)
                        // in which case it may be null
                        TreeItem<EmployeeSelection> itemToMove = locateInTreeView(parentTree.getRoot(), valueToMove);
                        // We know what the new parent should be, as the person dropped the mouse on it.
                        TreeItem<EmployeeSelection> newParentItem = locateInTreeView(parentTree.getRoot(), item.toString());

                        // 
                        if (itemToMove != null) {
                            // Remove the old link (if it exists)
                            SelectionLink oldLink = new SelectionLink(itemToMove.getValue(), itemToMove.getParent().getValue());
                            // Update links in ReportingStructure
                            controllingReportingStructure.removeSelectionLink(oldLink);
                            // Remove from former parent.
                            itemToMove.getParent().getChildren().remove(itemToMove);
                        } else {
                            itemToMove = new TreeItem<>(selectionToMove);
                        }

                        // Create the new link
                        SelectionLink newLink = new SelectionLink(itemToMove.getValue(), newParentItem.getValue());

                        // Add to new parent.
                        newParentItem.getChildren().add(itemToMove);
                        newParentItem.setExpanded(true);

                        // Update links in ReportingStructure
                        controllingReportingStructure.addSelectionLink(newLink);

                        // Now that we've dropped the item, highlight it and scroll to it
                        parentTree.getSelectionModel().select(itemToMove);
                        tvRS_ReportingStructureTreeView.scrollTo(tvRS_ReportingStructureTreeView.getRow(itemToMove));
                        rebuildEmployeeSelectionLists();
                        setContentsChanged(true);
                    } else //
                    // EMPTY TREEVIEW
                    //
                    // 'item' can only be null if the ReportingStructure is still empty
                    {
                        if (lvRS_ExistingReportingStructures.getSelectionModel().getSelectedItem().getSelectionLinks().isEmpty()) {
                            System.out.println("Drag dropped on empty TreeView");

                            // This item could have originated from the ListView (not already in the reporting structure)
                            // in which case it may be null
                            TreeItem<EmployeeSelection> itemToMove = new TreeItem<>(selectionToMove);

                            // Add as the new ROOT item.
                            tvRS_ReportingStructureTreeView.setRoot(itemToMove);
//                            // Create the new link/TopNode
//                            SelectionLink newLink = new SelectionLink(itemToMove.getValue(), null);
//                            controllingReportingStructure.addSelectionLink(newLink);
//                            rebuildEmployeeSelectionLists();
//                            setContentsChanged(true);
                        }
                    }

                    dragEvent.consume();
                }
            }
            );

            setOnDragExited(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent dragEvent) {
                    setStyle("");
                    dragEvent.consume();

                }
            });

        }

        private EmployeeSelection item;

        @Override
        protected void updateItem(EmployeeSelection item, boolean empty) {
            super.updateItem(item, empty);
            this.item = item;
            String text = (item == null) ? null : item.toString();
            setText(text);

            // Different icons if there is an EmployeeSelection below this one.
            if (item != null) {
                // Set icon
                ImageView iv;
                // Node to update
                TreeItem<EmployeeSelection> itemNode = locateInTreeView(parentTree.getRoot(), item.toString());

                // Root Node
                if (itemNode.equals(parentTree.getRoot())) {
                    iv = new ImageView(rootIcon);
                } else// Branch Node
                {
                    if (!itemNode.isLeaf()) {
                        iv = new ImageView(subIcon);
                    } else // Leaf Node
                    {
                        iv = new ImageView(noSubIcon);
                    }
                }

                setGraphic(iv);
                setStyle("-fx-font-size:12;");

                // Is this selection in the process of being cut?
                if (item == clipboardEmployeeSelection) {
                    setStyle("-fx-font-style:italic; -fx-font-weight:lighter;");
                }

            }

        }

        private final Image rootIcon
                = new Image(getClass().getResourceAsStream("three.png"));

        private final Image subIcon
                = new Image(getClass().getResourceAsStream("two.png"));

        private final Image noSubIcon
                = new Image(getClass().getResourceAsStream("one.png"));

        private final String dragDropHighlighStyle = "-fx-background-color: #707070,\n"
                + "        linear-gradient(#fcfcfc, #f3f3f3),\n"
                + "        linear-gradient(#f2f2f2 0%, #ebebeb 49%, #dddddd 50%, #cfcfcf 100%);\n"
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-background-radius: 1,1,1;\n"
                + "    -fx-padding: 1 2 1 2;\n"
                + "    -fx-text-fill: black;\n"
                + "    -fx-font-size: 13px;";
    }
// </editor-fold> 
//

}
