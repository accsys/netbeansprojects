/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statusbuzzer;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SoundEffect {

    File soundFile;

    public SoundEffect(String fileName) {
        soundFile = new File(fileName);
    } // end of constructor

    public void playSound() {

        AudioInputStream stream;
        AudioFormat format;
        DataLine.Info info;
        Clip clip;

        try {
            stream = AudioSystem.getAudioInputStream(soundFile);
            format = stream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            try {
                clip = (Clip) AudioSystem.getLine(info);
                clip.open(stream);
                System.out.print("\nPlaying sound file:"+soundFile);
                clip.start();
                System.out.println("\tdone\n");
            } catch (LineUnavailableException ex) {
                Logger.getLogger(SoundEffect.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (UnsupportedAudioFileException | IOException ex) {
            Logger.getLogger(SoundEffect.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
