/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statusbuzzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author liam
 */
public class WebMonitor {

    /**
     * Monitors a given URL to see if any JSON response are being returned.
     *
     * @param url The URL that will be called
     * @param mediaFile *.mp3 or *.wav file to be played as the alarm
     * @param alarmOnJSONResponse Sound alarm if a non-empty JSON object is being returned
     * @param alarmOnNoJSONResponse Sound alarm of empty JSON object, or no response at all, is returned
     * @param searchForReponseContaining If non-empty, sound alarm if the response contains this text (case
     * sensitive)
     * @param searchForReponseNotContaining If non-empty, sound alarm if the response does not contain this
     * text (case sensitive)
     *
     */
    public void monitorURL(String url, String mediaFile, boolean alarmOnJSONResponse, boolean alarmOnNoJSONResponse, String searchForReponseContaining, String searchForReponseNotContaining) {
        SoundEffect soundEffect = new SoundEffect(mediaFile);
        String response = getJSONResponse(url);
        // Whatever we want to do in terms of warnings, alarms, etc. will have to be done here!
        boolean hasResponse = !response.replace("{", "").replace("}", "").isEmpty();

        //
        // Case #1 & Case #2 : Alarm on JSON Response || Alarm on NO JSON Response
        //
        if ((alarmOnJSONResponse && hasResponse) || (alarmOnNoJSONResponse && !hasResponse)) {
            System.out.println("\nAlarm: " + new java.util.Date());
            System.out.println("\tUrl: " + url);
            System.out.println("\tResponse: " + response);
            soundEffect.playSound();
        }

        //
        // Case #3 : Alarm on response containing certain text
        //
        if ((hasResponse && !searchForReponseContaining.trim().isEmpty())) {
            if (response.contains(searchForReponseContaining)) {
                System.out.println("\nAlarm: " + new java.util.Date());
                System.out.println("\tUrl: " + url);
                System.out.println("\tFound search string :\"" + searchForReponseContaining + "\"");
                System.out.println("\tResponse: " + response);
                soundEffect.playSound();
            }
        }
        //
        // Case #4 : Alarm on response NOT containing certain text
        //
        if ((!hasResponse) || (hasResponse && !searchForReponseNotContaining.trim().isEmpty())) {
            if (!response.contains(searchForReponseNotContaining)) {
                System.out.println("\nAlarm: " + new java.util.Date());
                System.out.println("\tUrl: " + url);
                System.out.println("\tSearch string :\"" + searchForReponseNotContaining + "\" not found.");
                System.out.println("\tResponse: " + response);
                soundEffect.playSound();
            }
        }
        
        try {
            // Wait for the sound file to complete playing
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WebMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns true if the call returned JSON objects
     *
     * @param baseURL
     * @param wsCall processed/returned
     * @param nameValuePairs - Parameters to be passed to web service call
     * @return List of parameters returned from call
     */
    private String getJSONResponse(String fullURL) {

        HttpClient client = new DefaultHttpClient();

        //System.out.println("getOneLineResponse:" + fullURL);
        ArrayList<NameValuePair> rslt = new ArrayList<>();
        try {
            HttpPost post = new HttpPost(fullURL);
            // Reading the response
            StringBuilder httpResponse = new StringBuilder();

            // Executes the web service (with parameters)
            //post.setEntity(new UrlEncodedFormEntity(null));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line;
            while ((line = rd.readLine()) != null) {
                httpResponse.append(line);
            }

            // now let's parse it properly
            HashMap<String, String> result = parseJSONLine(httpResponse.toString());
            // Any sites down?
            return httpResponse.toString();

        } catch (java.net.ConnectException e) {
        } catch (IOException ce) {
        }
        return "";
    }

    /**
     * parses a JSON String (flat structure) and returns the key-value pairs in a HashMap (intended for a
     * single line result-set)
     *
     * @param jsonString
     * @return
     */
    private HashMap<String, String> parseJSONLine(String jsonString) {
        /*
         * Some house-keeping
         * Remove first '[' and last ']'
         */
        jsonString = jsonString.trim();
        if (jsonString.startsWith("[")) {
            jsonString = jsonString.substring(1, jsonString.length() - 1);
        }
        if (jsonString.endsWith("]")) {
            jsonString = jsonString.substring(0, jsonString.length() - 2);
        }
        HashMap<String, String> rslt = new HashMap<>();

        if (jsonString.trim().length() > 0) {
            try {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
                return (HashMap<String, String>) jsonObject;

            } catch (ParseException | org.json.simple.parser.ParseException pe) {
            }
        }
        return null;

    }

}
