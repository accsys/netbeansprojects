package lwt_tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import za.co.ucs.accsys.peopleware.Company;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.PasswordManager;
import za.co.ucs.accsys.tools.StringEncrypterSingleton.EncryptionException;
import za.co.ucs.accsys.webmodule.*;
import static za.co.ucs.accsys.webmodule.FileContainer.getOSFileSeperator;
import za.co.ucs.lwt.db.DatabaseObject;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import javax.imageio.ImageIO;
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

public class RunTests {

    public static void saveToBinaryFile(String templateRAW, File toFile) {
        try {
            toFile.delete();

            FileOutputStream fos = new FileOutputStream(toFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            ObjectOutputStream oos = new ObjectOutputStream(bos);

            oos.writeObject(templateRAW);

            oos.flush();
            fos.flush();
            oos.close();
            fos.close();

        } catch (FileNotFoundException fnfe) {
        } catch (IOException ioe) {
        }
    }

    public static void saveTemplateAndImageFiles() {
        java.sql.Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            // Get Template and Image from DB
            java.sql.ResultSet rs = DatabaseObject.openSQL("SELECT * FROM DBA.TAL_EMPLOYEE_FINGER WHERE READER_ID = 3 and TEMPLATE_IMAGE is not NULL  and PERSON_ID > 103021 order by PERSON_ID, FINGER_ID, READING", con);
            // Save Template and Image to files
            String workingDirectory = FileContainer.getWorkingDirectory() + "\\Files";
            String binFileName;
            String imageFileName;
            String template;
            BufferedImage image = new BufferedImage(288, 320, BufferedImage.TYPE_INT_RGB);
            int person_ID;
            int finger_ID;
            int reading;

            while (rs.next()) {
                person_ID = rs.getInt("PERSON_ID");
                finger_ID = rs.getInt("FINGER_ID");
                reading = rs.getInt("READING");

                System.out.println("Person_ID: " + person_ID);
                System.out.println("Finger_ID: " + finger_ID);
                System.out.println("Reading: " + reading);
                System.out.println("*******************************************************************");

                template = rs.getString("TEMPLATE");
                binFileName = person_ID + "_" + finger_ID + "_" + reading + ".bin";

                image = ImageIO.read(rs.getBinaryStream("TEMPLATE_IMAGE"));
                imageFileName = person_ID + "_" + finger_ID + "_" + reading + ".png";

                saveToBinaryFile(template, new File(workingDirectory + getOSFileSeperator() + binFileName));
                ImageIO.write(image, "png", new File(workingDirectory + getOSFileSeperator() + imageFileName));
            }
            rs.close();

        } catch (java.sql.SQLException e) {
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    private static String getWebProcessEMailDescription(WebProcessDefinition def, Employee employee) {
        try {
            // Variables 55001
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55001").getName();
            }
            // Variables 55002
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55002").getName();
            }
            // Variables 55003
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55001").getName();
            }
            // Variables 55004
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55004").getName();
            }
            // Variables 55005
            if (def.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                return WebProcess_VariableChanges.getVariable(employee, "55005").getName();
            }

            return def.getName();
        } catch (Exception e) {
            return "";
        }
    }

    // Assist in creating a string that reads 1st, 2nd, 3rd, 4th, etc.
    private static String getStringDaySuffix(int day) {
        if (day == 1) {
            return "st";
        }
        if (day == 2) {
            return "nd";
        }
        if (day == 3) {
            return "rd";
        }
        return "th";
    }

    /**
     * Checks the WebProcesses for expirations, etc.
     */
    private static void remindOfDateConstraintProcesses(FileContainer fc) {

        System.out.println("\nChecking Date Constraint Processes..." + fc.getWebProcessDefinitions().size());
        ArrayList definitions = fc.getWebProcessDefinitions();
        Iterator iter = definitions.iterator();
        while (iter.hasNext()) {
            WebProcessDefinition def = (WebProcessDefinition) iter.next();
            //
            // Is a process definition about to become active?
            //
            if (def.isNotifyOfPendingActivation()) {
                boolean isOnActivationDay = (def.getActiveFromDayOfMonth() > 0) && (def.getActiveFromDayOfMonth() == DatabaseObject.getDayOfMonth(new java.util.Date()));
                if (isOnActivationDay) {
                    // Notify all employees who may use this process
                    //
                    ArrayList employees = def.getReportingStructure().getAllEmployeesInEmployeeSelections();
                    for (int i = 0; i < employees.size(); i++) {
                        Employee employee = (Employee) employees.get(i);
                        String procDescr = getWebProcessEMailDescription(def, employee);
                        if (!procDescr.equals("")) {
                            StringBuilder message = new StringBuilder();
                            message.append("<h4>Dear ").append(employee.toString()).append("</h4>");
                            message.append("<br>This is a reminder that the following ESS process has become available for this month:<br><i>").append(procDescr).append("</i>");
                            if (def.getActiveToDayOfMonth() > 0) {
                                message.append("<br>The process will remain available until the ").append(def.getActiveToDayOfMonth()).append(getStringDaySuffix(def.getActiveToDayOfMonth())).append(" day of the month.");
                            }
                            // Send e-mail
                            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                                E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Process Activation Notification]", message.toString(), null);
                            }
                        }
                    }
                }
            }

            //
            // Is a process definition about to be deactivated?
            //
            if (def.isNotifyOfPendingDeactivation()) {
                boolean isDayBeforeDeactivationDay = (def.getActiveToDayOfMonth() > 0) && ((def.getActiveToDayOfMonth() - DatabaseObject.getDayOfMonth(new java.util.Date())) == 1);
                if (isDayBeforeDeactivationDay) {
                    // Notify all employees who may use this process
                    //
                    ArrayList employees = def.getReportingStructure().getAllEmployeesInEmployeeSelections();
                    for (int i = 0; i < employees.size(); i++) {
                        Employee employee = (Employee) employees.get(i);
                        String procDescr = getWebProcessEMailDescription(def, employee);
                        if (!procDescr.equals("")) {
                            StringBuilder message = new StringBuilder();
                            message.append("<h4>Dear ").append(employee.toString()).append("</h4>");
                            message.append("<br>This is a reminder that as of tomorrow, the following ESS process will be closed for the remainder of this month:<br><i>").append(procDescr).append("</i>");
                            if (def.getActiveFromDayOfMonth() > 0) {
                                message.append("<br>The process will re-open on the ").append(def.getActiveFromDayOfMonth()).append(getStringDaySuffix(def.getActiveFromDayOfMonth())).append(" day of next month.");
                            }
                            // Send e-mail
                            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                                E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [Process Activation Notification]", message.toString(), null);
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) throws EncryptionException {
        FileContainer fileContainer = FileContainer.getInstance();
        remindOfDateConstraintProcesses(fileContainer);
        System.out.println("HELLLLLLLLLLLLLLLLLLOOOOOOOOOOOOOOOOOO");
        
        double result = 0;
        double hours = 2;
        double minutes = 18;
        double hoursPerDay = 7.500;
        double hrs = (minutes / 60);
        result = ((hours + (minutes / 60)) / hoursPerDay);
        String str = String.format("%f",result);
        System.out.println("Result = " + str);
        System.out.println("hrs = " + hrs);
        //FileContainer.persistManagementStructureToDB();

//        // Step 1: Get processes from container
//        System.out.println("\n\n\t-- Loading from Binary Files --");
//        startTime = new Date();
//        fileContainer.loadWebProcessDetail();
//        LinkedList fileProcesses = fileContainer.getWebProcesses();
//        endTime = new Date();
//        diffInSeconds = (endTime.getTime() - startTime.getTime()) / 1000;
//        System.out.println("\n\n\t-- Elapsed time:" + diffInSeconds + " seconds");
//
//
//        // Step 2: Get processes from database
//        System.out.println("\n\n\t-- Loading from Database Files --");
//        startTime = new Date();
//        LinkedList dbProcesses = getProcessesFromDB();
//        endTime = new Date();
//        diffInSeconds = (endTime.getTime() - startTime.getTime()) / 1000;
//        System.out.println("\n\n\t-- Elapsed time:" + diffInSeconds + " seconds");
//
//        WebProcess aProcess;
//        // Step 3: Update an existing process
//        System.out.println("\n\n\t-- Changing File Process --");
//        startTime = new Date();
//        for (int i = 1; i < 100; i++) {
//            aProcess = (WebProcess) fileProcesses.get(i);
//            if (aProcess.isActive()) {
//                aProcess.cancelProcess("No longer required", true);
//            }
//        }
//        endTime = new Date();
//        diffInSeconds = (endTime.getTime() - startTime.getTime()) / 1000;
//        System.out.println("\n\n\t-- Elapsed time:" + diffInSeconds + " seconds");
//
//
//        // Step 4: Update an existing process
//        // Connect to JavaDB
//        Connection con;
//        try {
//            con = DriverManager.getConnection("jdbc:derby://localhost:1527/WebModule", "DBA", "I,don'tKNOW!1t.");
//
//            System.out.println("\n\n\t-- Changing Database Process --");
//            startTime = new Date();
//            for (int i = 1; i < 100; i++) {
//                aProcess = (WebProcess) dbProcesses.get(i);
//                if (aProcess.isActive()) {
//                    aProcess.cancelProcess("No longer required", false);
//                    updateProcessInDB(con, aProcess);
//                }
//            }
//
//            endTime = new Date();
//            diffInSeconds = (endTime.getTime() - startTime.getTime()) / 1000;
//            System.out.println("\n\n\t-- Elapsed time:" + diffInSeconds + " seconds");
//            con.close();
//        } catch (SQLException ex) {
//            Logger.getLogger(RunTests.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        //int processesStored = storeProcessesInDB(processes);
//        //System.out.println("Processes stored :"+storeProcessesInDB(processes));
    }
}
