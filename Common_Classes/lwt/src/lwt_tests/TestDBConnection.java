/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lwt_tests;

import za.co.ucs.lwt.db.*;
import za.co.ucs.accsys.webmodule.*;
import java.sql.*;

/**
 *
 * @author lterblanche
 */
public class TestDBConnection 
{
    

    public TestDBConnection()
    {
     System.out.println("test");
    }

    @SuppressWarnings("static-access")
    public String getCurrentDBVersion(){
        DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database,""));
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select first(VERSION_NR) from s_db_ver with (nolock) order by date_of_version desc", con);
            if (rs.next()){
                return (rs.getString(1));
            }
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }

        // No version was found in database
        System.out.println("No DB Ver:");
        return "";
    }
}
