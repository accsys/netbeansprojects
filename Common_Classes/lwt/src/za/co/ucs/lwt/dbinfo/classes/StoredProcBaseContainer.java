/*
 * StoredProcBaseContainer.java
 *
 * Created on October 21, 2003, 1:30 PM
 */

package za.co.ucs.lwt.dbinfo.classes;
import java.util.*;

/**
 *
 * @author  liam
 */
public class StoredProcBaseContainer {
    
    /**
     * Creates a new instance of StoredProcBaseContainer
     */
    public StoredProcBaseContainer() {
        qrStoredProcs = new TreeMap();
        qrStoredProcsSequence = new TreeMap();
    }
    
    public TreeMap getStoredProcs(){
        return qrStoredProcs;
    }
    
    public StoredProcDef getStoredProc(String procName){
        return (StoredProcDef)qrStoredProcs.get(procName.toUpperCase());
    }
    
    public StoredProcDef getStoredProc(int procIndex){
        return (StoredProcDef)qrStoredProcsSequence.get(new Integer(procIndex));
    }
    
    public void addStoredProc(StoredProcDef storedProc){
        qrStoredProcs.put(storedProc.getName().toUpperCase(), storedProc);
        qrStoredProcsSequence.put(new Integer(qrStoredProcs.size()), storedProc);
    }
    
    
    private TreeMap qrStoredProcs;
    private TreeMap qrStoredProcsSequence;
}
