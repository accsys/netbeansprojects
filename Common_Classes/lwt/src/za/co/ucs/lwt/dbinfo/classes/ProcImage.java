/*
 * ProcImage.java
 *
 * Created on November 4, 2003, 8:10 AM
 */

package za.co.ucs.lwt.dbinfo.classes;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.awt.print.*;


/**
 *
 * @author  liam
 */
public class ProcImage extends JPanel implements Printable{
    
    /**
     * Creates a new instance of ProcImage
     */
    public ProcImage(LinkedList calledFromLinks, LinkedList callingToLinks, StoredProcDef baseScript) {
        super();
        this.calledFromLinks = calledFromLinks;
        this.callingToLinks = callingToLinks;
        this.baseScript = baseScript;
        
        scriptPositionsOnScreen = new LinkedList();
        
        // Let us add a mouse Listener
        scrptImgMouseListener = new ProcImageMouseListener();
        this.addMouseListener(scrptImgMouseListener);
    }
    
    public ProcImageMouseListener getMouseListener(){
        return scrptImgMouseListener;
    }
    
    public void paint(Graphics g){
/*
        g.setFont(new Font( g.getFont().getFontName(), Font.PLAIN, 9));
        Dimension d = getSize();
        nbScriptsWide = getLevelCount();
        nbScriptsHigh = getMaxLevelHeight();
        screenWidth = d.width-(2*xOffset);
        screenHeight = d.height-(2*yOffset);
 
        xSpacing = screenWidth / (nbScriptsWide+1);
        ySpacing = screenHeight / (nbScriptsHigh+1);
 
        // Clear the LinkedList that stores the script positions
        scriptPositionsOnScreen.clear();
 
        // Draw Base Level Script
        StoredProcDef base = getBaseScript();
        drawScript(base, xOffset, (screenHeight/2) - yOffset, g,0);
 */
        
        // We start by drawing the selected script in the center of the screen
        g.setFont(new Font( g.getFont().getFontName(), Font.PLAIN, 9));
        Dimension d = getSize();
        nbScriptsWide = 3;
        nbScriptsHigh = getMaxLevelHeight(baseScript);
        
        xSpacing = d.width / (3);
        ySpacing = d.height / (nbScriptsHigh+1);
        
        // Clear the LinkedList that stores the script positions
        scriptPositionsOnScreen.clear();
        
        // Draw Base Level Script
        StoredProcDef base = getBaseScript();
        int scriptWidth = g.getFontMetrics().stringWidth(base.getName());
        drawBaseScript(base, (d.width/2)- scriptWidth, (d.height/2) , g);
    }
    
    /* When two scripts overlap, we will move the second script slightly down so that one can see both
     */
    private boolean isOverlappingAnotherScript(StoredProcDef script, int x, int y){
        for (int i = 0; i<scriptPositionsOnScreen.size(); i++){
            if ( (((ProcPosition)scriptPositionsOnScreen.get(i)).getScript() != script) &&
                    ((ProcPosition)scriptPositionsOnScreen.get(i)).inLine(x, y)){
                return true;
            }
        }
        return false;
    }
    
    public StoredProcDef getScriptAt(int X, int Y){
        for (int i = 0; i<scriptPositionsOnScreen.size(); i++){
            if (((ProcPosition)scriptPositionsOnScreen.get(i)).inPosition(X,Y)){
                return ((ProcPosition)scriptPositionsOnScreen.get(i)).getScript();
            }
        }
        return null;
    }
    
    private Color getColor(StoredProcDef script){
        if (script.getType().compareToIgnoreCase("PROCEDURE")==0){
            return(Color.yellow);
        }
        if (script.getType().compareToIgnoreCase("VIEW")==0){
            return(Color.CYAN);
        }
        if (script.getType().compareToIgnoreCase("TRIGGER")==0){
            return(Color.GREEN);
        }
        if (script.getType().compareToIgnoreCase("TABLE")==0){
            return(Color.BLUE);
        }        
        // Default
        return(Color.LIGHT_GRAY);
    }
    
    /* Draws the script and all the scripts calling it or being called by it.
     */
    private void drawBaseScript(StoredProcDef baseScript, int x, int y, Graphics g){
        if (baseScript != null){
            
            int textWidth = g.getFontMetrics().stringWidth(baseScript.toString());
            int textHeight = g.getFontMetrics().getHeight();
            int nbScriptsToLeft = getLinkedFromScripts(baseScript).size();
            int nbScriptsToRight = getLinkedToScripts(baseScript).size();
            
            // Place this script in the LinkedList that stores the scripts and their positions
            scriptPositionsOnScreen.add(new ProcPosition(baseScript, x, y, textWidth+6, textHeight+4));
            
            // Draw the script
            g.setColor(Color.black);
            g.draw3DRect(x, y,textWidth+6, textHeight+4, false);
            g.setColor(getColor(baseScript));
            g.fill3DRect(x+1, y+1,textWidth+5, textHeight+3, true);
            g.setColor(Color.black);
            g.drawString(baseScript.toString(), x+2, y+textHeight-1);
            
            // Draw the 'LEFT' scripts
            LinkedList callingScripts = getLinkedFromScripts(baseScript);
            for (int i=0; i<callingScripts.size(); i++){
                ProcLink callingLink = (ProcLink)callingScripts.get(i);
                // Is there a link?
                if (callingLink == null){
                    continue;
                }
                StoredProcDef nextScript = callingLink.getFrom();
                
                int newX = (2*xOffset);
                int newY = yOffset+(i*getSize().height)/(nbScriptsToLeft+1)+((getSize().height)/(nbScriptsToLeft+1))/2;
                
                // Now that we know where the new script is going to be drawn, we can
                // draw the link as well
//                drawLink(x+textWidth+6, y+(textHeight+4)/2, newX, newY+(textHeight+4)/2, g);
                drawLink(x, y+(textHeight+4)/2, newX+g.getFontMetrics().stringWidth(nextScript.toString())+6
                        ,newY+(textHeight+4)/2, g);
                
                // Draw the script
                drawScript(g, nextScript, newX, newY);
            }
            
            // Draw the 'RIGHT' scripts
            LinkedList callerScripts = getLinkedToScripts(baseScript);
            for (int i=0; i<callerScripts.size(); i++){
                ProcLink callerLink = (ProcLink)callerScripts.get(i);
                // Is there a link?
                if (callerLink == null){
                    continue;
                }
                StoredProcDef nextScript = callerLink.getTo();
                
                int newX = 3*(getSize().width/4)-(2*xOffset);
                int newY = yOffset+(i*getSize().height)/(nbScriptsToRight)+((getSize().height)/(nbScriptsToRight+1))/2;
                
                // Now that we know where the new script is going to be drawn, we can
                // draw the link as well
//                drawLink(x+textWidth+6, y+(textHeight+4)/2, newX, newY+(textHeight+4)/2, g);
                drawLink(x+textWidth+6, y+(textHeight+4)/2, newX ,newY, g);
                
                // Draw the script
                drawScript(g, nextScript, newX, newY);
            }
        }
    }
    
    private void drawScript(Graphics g,StoredProcDef script, int x, int y){
        int width = g.getFontMetrics().stringWidth(script.toString());
        int height = g.getFontMetrics().getHeight();
        g.setColor(Color.black);
        g.draw3DRect(x, y,width+4, height+4, false);
        g.setColor(getColor(script));
        g.fill3DRect(x+1, y+1,width+2, height+2, true);
        g.setColor(Color.black);
        g.drawString(script.toString(), x+2, y+height-1);
        
        scriptPositionsOnScreen.add(new ProcPosition(script, x, y, width+6, height+4));
        
    }
    
    
    
    private void drawLink(int x1, int y1, int x2, int y2, Graphics g){
        g.setColor(Color.red);
        int midX = x1+(x2-x1)/2;
        //        g.drawLine(x1, y1, midX, y1);
        //        g.drawLine(midX, y1, midX, y2);
        //        g.drawLine(midX, y2, x2, y2);
        g.drawLine(x1, y1, x2, y2);
    }
    
    // Returns the number of scripts that are linked to this script;
    private int getLinkedToScriptsCount(StoredProcDef script){
        return (getLinkedToScripts(script).size());
    }
    
    // Returns the number of scripts that are linked to this script;
    private int getLinkedFromScriptsCount(StoredProcDef script){
        return (getLinkedFromScripts(script).size());
    }
    
    // Returns the links where 'script' is on the left side
    private LinkedList getLinkedToScripts(StoredProcDef script){
        LinkedList list = new LinkedList();
        for (int I=0; I<callingToLinks.size(); I++){
            if (((ProcLink)callingToLinks.get(I)).getFrom() == script){
                list.add((ProcLink)callingToLinks.get(I));
            }
        }
        return list;
    }
    
    // Returns the links where 'script' is on the right side
    private LinkedList getLinkedFromScripts(StoredProcDef script){
        LinkedList list = new LinkedList();
        for (int I=0; I<calledFromLinks.size(); I++){
            if (((ProcLink)calledFromLinks.get(I)).getTo() == script){
                list.add((ProcLink)calledFromLinks.get(I));
            }
        }
        return list;
    }
    
    // Returns the script that are the 'Base' of all the other scripts
    private StoredProcDef getBaseScript(){
        return baseScript;
    }
    
    private TreeMap getScriptsPerLevel(){
        StoredProcDef baseScript = getBaseScript();
        LinkedList list = new LinkedList();
        TreeMap levels = new TreeMap();
        // Add the starting point
        list.add(baseScript);
        levels.put(new Integer(0), list);
        LinkedList currentLevel;
        int i=1;
        do{
            // Get all the scripts called by the scripts in the previous level
            currentLevel = new LinkedList();
            LinkedList previousLevel = (LinkedList)levels.get(new Integer(i-1));
            ListIterator previousLevelIterator = previousLevel.listIterator();
            while (previousLevelIterator.hasNext()){
                StoredProcDef callerScript = (StoredProcDef)previousLevelIterator.next();
                if (callerScript == null){
                    continue;
                }
                LinkedList calledScripts = getLinkedToScripts(callerScript);
                ListIterator calledScriptsIterator = calledScripts.listIterator();
                while (calledScriptsIterator.hasNext()){
                    ProcLink calledLink = (ProcLink)calledScriptsIterator.next();
                    StoredProcDef calledScript = calledLink.getTo();
                    if ( treeMapContains(levels, calledScript)){
                        int existsIn = levelOfTreeMapThatContains(levels, calledScript);
                        if (existsIn >=0){
                            // Remove from lower level
                            removeFromTreeMap(levels, calledScript, existsIn);
                        }
                    }
                    if (currentLevel.indexOf(calledScript)==-1){
                        currentLevel.add(calledScript);
                    }
                }
            }
            
            // Add a lew 'level' into the TreeMap
            if (currentLevel.size()>0){
                levels.put(new Integer(i), currentLevel);
                i++;
            }
            if (i>4){
                break;
            }
        } while (currentLevel.size()>0);
        
        return (levels);
    }
    
    private boolean treeMapContains(TreeMap levels, StoredProcDef script){
        for (int a=levels.size()-1; a>=0 ; a--){
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            if (currentLevel.contains(script)){
                return true;
            }
        }
        return false;
    }
    
    private int levelOfTreeMapThatContains(TreeMap levels, StoredProcDef script){
        for (int a=levels.size()-1; a>=0 ; a--){
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            if (currentLevel.contains(script)){
                return a;
            }
        }
        return -1;
    }
    
    private void removeFromTreeMap(TreeMap levels, StoredProcDef script, int level){
        LinkedList currentLevel = (LinkedList)levels.get(new Integer(level));
        currentLevel.set(currentLevel.indexOf(script), null);
        //        currentLevel.remove(script);
    }
    
    private void printAll(TreeMap levels){
        System.out.println("////////////////////");
        for (int a=levels.size()-1; a>=0 ; a--){
            System.out.println("Level: "+a);
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            for (int b=0; b<currentLevel.size(); b++){
                StoredProcDef printScript = (StoredProcDef)currentLevel.get(b);
                System.out.println("  Script: "+b+" = "+printScript.toString());
            }
        }
    }
    
    
    /** Maximum number of scripts on a singe level.
     * This will determine the available space we've got for
     * drawing our graphs
     */
    private int getMaxLevelHeight(StoredProcDef script){
        return java.lang.Math.max(getLinkedFromScripts(script).size(),getLinkedToScripts(script).size());
    }
    
    
    public LinkedList getScriptCoordinates(){
        return scriptPositionsOnScreen;
    }
    
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        
        Graphics2D g2 = (Graphics2D) graphics;
        g2.translate(pageFormat.getImageableX()+10, pageFormat.getImageableY()+10);
        //g2.translate(5, 5);
        paint(g2);
        return Printable.PAGE_EXISTS;
    }
    
    private int nbScriptsWide;
    private int nbScriptsHigh;
    private int xOffset=10;
    private int yOffset=10;
    private int xSpacing;
    private int ySpacing;
    private LinkedList calledFromLinks;
    private LinkedList callingToLinks;
    private LinkedList scriptPositionsOnScreen;
    private StoredProcDef baseScript;
    private ProcImageMouseListener scrptImgMouseListener = null;
    
    
}


