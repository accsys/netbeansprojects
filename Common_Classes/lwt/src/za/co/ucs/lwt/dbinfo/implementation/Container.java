/*
 * Container.java
 *
 * Created on October 23, 2003, 12:38 PM
 */

package za.co.ucs.lwt.dbinfo.implementation;
import java.io.*;
import java.util.*;
import za.co.ucs.lwt.dbinfo.classes.StoredProcBaseContainer;
import za.co.ucs.lwt.dbinfo.classes.StoredProcDef;

/**
 *
 * @author  liam
 */
public class Container extends Observable {
    
    /** Creates a new instance of Container
     * @params storedProcedures is a TreeMap with the StoredProc NAME as the key and the StoredProcDef as the VALUE
     */
    public Container(TreeMap storedProcedures) {
        mainContainer = new StoredProcBaseContainer();
        buildMainContainerList(storedProcedures);
    }
    
    /* Populates the mainContainer with the TreeMap containing NAME, DEFINITION, and TYPE of each Stored Procedure */
    private void buildMainContainerList(TreeMap storedProcedures){
        // For every entry in the TreeMap, create a StoredProcDef class and add it to the script container
        Iterator iter = storedProcedures.values().iterator();
        
        while (iter.hasNext()){
            StoredProcDef procDef = (StoredProcDef)iter.next();
            // set the main container to 'this.maincontainer'
            procDef.setBaseContainer(mainContainer);
            mainContainer.addStoredProc(procDef);
        }
        
        // After we've got the scripts in the mainContainer, we need to build the contents of
        // every script
        Iterator iter2 = mainContainer.getStoredProcs().values().iterator();
        System.out.println("Interpreting Stored Procedures...");
        int cnt = 0;
        while (iter2.hasNext()){
            cnt++;
            if (java.lang.Math.IEEEremainder(cnt,50)==0){
                System.out.println(cnt+"/"+mainContainer.getStoredProcs().values().size());
            }
            ((StoredProcDef)iter2.next()).interpretContentsOfFile();
        }
        System.out.println("\nDone.");
        
    }
    
    public StoredProcBaseContainer getMainContainer(){
        return mainContainer;
    }
    
    private StoredProcBaseContainer mainContainer;
}

class MyScriptsFilter implements java.io.FilenameFilter{
    public boolean accept(File dir, String name){
        if (name.toUpperCase().endsWith("AWL")==true)
            return true;
        else
            return false;
    }
}