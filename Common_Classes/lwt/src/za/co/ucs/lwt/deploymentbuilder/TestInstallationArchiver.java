/*
 * TestInstallationArchiver.java
 *
 * Created on December 18, 2003, 7:37 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/** The TestInstallationArchiver Class is used to create a single compressed file that will be
 * distributed to the QA department for testing
 * <br>
 * This file contains QAFiles, which in turns includes a file as well as its distribution rules.
 * Remember to use the 'openArchiver()' before, and 'closeArchiver()' after adding files
 * into the archive
 */
public class TestInstallationArchiver extends ArchiverBase {
    
    /** ArchiveFile - the File that will contain the compressed, combined content
     * of all the files that will be archived by the call to addToArchive()
     */
    public TestInstallationArchiver(File archiveFile, ChangeRequest changeRequest){
        super();
        this.changeRequest = changeRequest;
        
        // Ensure the Temp Folders exists
        FileContainer.getTemporaryFolder().mkdirs();
        this.compressedArchiveFile = archiveFile;
        this.uncompressedArchiveFile = new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+archiveFile.getName()+".tmp");
    }
    
    /** ArchiveFile - the File that will contain the compressed, combined content
     * of all the files that will be archived by the call to addToArchive()
     */
    public TestInstallationArchiver(File archiveFile){
        this(archiveFile, null);
    }
    
    /** One sometimes needs to remove a complete folder with all the files in it
     */
    private void removeFolder(File folder){
        // If this is only a file, delete it
        if (!folder.isDirectory()){
            folder.delete();
            return;
        }
        
        // In case of a folder, delete all the files inside the folder,
        //  before deleting the folder
        File[] files = folder.listFiles();
        for (int i=0;i<files.length;i++){
            File file = files[i];
            removeFolder(file);
        }
        
        // Now, remove oneself
        folder.delete();
    }
    
    public void setChangeRequest(ChangeRequest changeRequest){
        this.changeRequest = changeRequest;
    }
    
    public ChangeRequest getChangeRequest(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        
        if (changeRequest != null){
            return changeRequest;
        }
        try{
            if (compressedArchiveFile.exists()){
                if  (!uncompressedArchiveFile.exists()){
                    uncompressFile(this.compressedArchiveFile, this.uncompressedArchiveFile);
                }
                
                fis = new FileInputStream(this.uncompressedArchiveFile);
                ois = new ObjectInputStream(fis);
                
                // start by reading the Change Request
                changeRequest = (ChangeRequest)ois.readObject();
                ois.close();
                fis.close();
                return changeRequest;
            }
        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        
        return null;
    }
    
    /** Adds a file into the archive.
     * Remember that one needs to call 'prepareArchive()' BEFORE adding
     * files into the archive.
     * <br>E.g.
     * <br>  Archiver archiver = new Archiver(new File("c:\\demo.arc") );
     * <br>  archiver.prepareArchive();
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\A.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\B.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\C.exe")));
     * <br>  archiver.buildArchive();
     */
    public void addToArchive(QAFile qaFile){
        if (changeRequest == null){
            Exception e = new Exception("Cannot create archives without an attached Change Request");
        }
        
        try{
            oos.writeObject(qaFile);
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    public void installArchive(File installFolder, TestInstallationArchiver backupArchiver){
        installArchive(installFolder, backupArchiver, null);
    }
    
    /** This method dissasembles the archived file.  Each file inside the
     * archive will be copied relative to the BASE directory ('installFolder').
     * Where actions are required to be taken (scripts, etc.) it will
     * be triggered within this method.
     * <br> installFolder - Folder that acts as the ROOT for the subsequent files
     * <br> backupArchiver - Used to save all the files that are being replaced during
     * the installation.  That way we can simply install the backup in order to uninstall
     *  the current.
     * <br> progressBar - an optional JProgressBar component to display the installation progress
     */
    public void installArchive(File installFolder, TestInstallationArchiver backupArchiver, javax.swing.JProgressBar progressBar){
        try{
            // Can I run this installation on the database?
            if (!isDatabaseRunning()){
                return;
            }
            // Uncompress the file
            uncompressFile(this.compressedArchiveFile, this.uncompressedArchiveFile);
            FileInputStream fis = new FileInputStream(this.uncompressedArchiveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            QAFile qaFile = null;
            FileOutputStream fos = null;
            
            // start by reading the Change Request
            changeRequest = (ChangeRequest)ois.readObject();
            
            // If this is a normal archive, we need to create a backup archive of all the files we're replacing.
            //   That way, we can easily uninstall by installing the backup archive
            if (backupArchiver != null){
                if (changeRequest != null){
                    backupArchiver.setChangeRequest(changeRequest);
                }
                backupArchiver.prepareArchive();
            }
            
            if (progressBar != null){
                progressBar.setIndeterminate(true);
            }
            
            while ((qaFile = (QAFile)ois.readObject()) != null){
                System.out.println("Installing :"+qaFile.getFile().getAbsoluteFile());
                // Create the required folders
                new File(qaFile.getInstallGroup().getInstallFolder(installFolder).getAbsolutePath()).mkdirs();
                
                File fromFile = new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+qaFile.getFile().getName());
                File toFile = new File(qaFile.getInstallGroup().getInstallFolder(installFolder).getAbsolutePath()+FileContainer.getOSFileSeperator()+fromFile.getName());
                
                if (progressBar != null){
                    progressBar.setString("Installing "+qaFile.getFile().getName()+" ...");
                }
                
                /** Before replacing existing files, we need to create a 'backup' archive that we can use
                 *    at a later stage to restore to.
                 * For scripts, this imply creating a new scripts file, containing the original script in the
                 * database.  That way, we can restore stored-procedures, functions and views back to the
                 * way they were prior to installation.
                 * For EXE's, DLL's etc, we store a copy of the original file in the backup archive.
                 */
                QAFile backupQAFile = qaFile.getInstallGroup().performTasksBefore(qaFile, installFolder);
                
                // Add this to the backup archiver
                if ((backupArchiver != null) && (backupQAFile != null)) {
                    backupArchiver.addToArchive(backupQAFile);
                }
                
                // Now we can copy the archiver files into their required destinations
                moveFile(fromFile, toFile );
                
                // Certain file groups have actions that must take place after the copy.
                // One example is the script files that needs to be executed.
                qaFile.getInstallGroup().performTasksAfter(qaFile);
                System.out.println("Installing :"+qaFile.getFile().getName()+" completed.");
            }
            
            ois.close();
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        catch (EOFException eofe){
            // Delete the uncompressed archive file
            if (backupArchiver != null){
                uncompressedArchiveFile.delete();
                
                // Prepare the Backup Archive
                backupArchiver.buildArchive();
            }
            
            // Reset the ProgressBar
            if (progressBar != null){
                progressBar.setString(null);
                progressBar.setMaximum(0);
                progressBar.setValue(0);
                progressBar.setIndeterminate(false);
            }
        }
        
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    
    /** Prepares the archive for the adding of files that will follow.
     * Remember that one needs to call 'prepareArchive()' BEFORE adding
     * files into the archive.
     * <br>E.g.
     * <br>  Archiver archiver = new Archiver(new File("c:\\demo.arc") );
     * <br>  archiver.prepareArchive();
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\A.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\B.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\C.exe")));
     * <br>  archiver.buildArchive();
     *
     */
    public void prepareArchive(){
        if (changeRequest == null){
            Exception e = new Exception("Cannot create archives without an attached Change Request");
        }
        try{
            fos = new FileOutputStream(uncompressedArchiveFile);
            oos = new ObjectOutputStream(fos);
            // Add the Change Request
            oos.writeObject(changeRequest);
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    /** It is imperative that one 'build' the Archive class in order to finalise the
     * archiving process.  Building the archive will compress the archived file and
     * save it into the required file name.
     * Remember that one needs to call 'closeArchiver()' AFTER adding
     * files into the archive.
     * <br>E.g.
     * <br>  Archiver archiver = new Archiver(new File("c:\\demo.arc") );
     * <br>  archiver.prepareArchive();
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\A.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\B.exe")));
     * <br>  archiver.addToArchive(new QAFile(new File("C:\\C.exe")));
     * <br>  archiver.buildArchive();
     *
     */
    public void buildArchive(){
        if (changeRequest == null){
            Exception e = new Exception("Cannot create archives without an attached Change Request");
        }
        try{
            oos.close();
            this.compressFile(uncompressedArchiveFile, compressedArchiveFile);
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    
    
    private File compressedArchiveFile;
    private File uncompressedArchiveFile;
    private ObjectOutputStream oos;
    private FileOutputStream fos;
    private ChangeRequest changeRequest = null;
}
