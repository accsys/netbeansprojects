/*
 * CombinedChangeRequest.java
 *
 * Created on 04 March 2005, 03:03
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;

/**
 * A CombinedChangeRequest is used to advise the user of which Change Requests
 * should be combined based on the BookingItems (Reminders) created.
 * A user can create an instance of this class, and, by referencing consecutive
 * Booking Items, a list is build up of all Change Requests that will either
 * directly or indirectly share resources assigned to the original Change Request
 * @author  lwt
 */
public class CombinedChangeRequest {
    
    /** Creates a new instance of CombinedChangeRequest.
     * The baseItem will be the point of origin.  Scanning through
     * each Booking Item inside the list 'allBookingItems', all those
     * BookingItems that might use the same resources as the ones required
     * by the baseItem, will be added to this CombinedRequest.  This will
     * be a recursive excersize.
     */
    public CombinedChangeRequest(BookingItem baseItem, LinkedList allBookingItems) {
        
        // Get all the BookingItems contained in 'allBookingItems' that directly or indirectly
        // share resources with the 'baseItem'
        includedBookingItems = getSharedResourceItems(baseItem.getRequiredFileNames(), allBookingItems);
    }
    
    /** For a given list of files, return all the bookingItems that has at least
     * one of those files in them
     */
    private LinkedList getSharedResourceItems(LinkedList fileNames, LinkedList allBookingItems){
        boolean itemsAdded = true;
        LinkedList result = new LinkedList();
        
        // Repeat until no more files/booking items can be added
        while (itemsAdded){
            itemsAdded = false;
            // For every filename in 'fileNames',
            //  find all the booking items that uses that file
            Iterator iter = fileNames.iterator();
            while (iter.hasNext()){
                String fileName = (String)iter.next();
                // Get all the Booking Items that require this file
                LinkedList bookingItemsRequiringFile = getSharedResourceItems(fileName, allBookingItems);
                // Add the BookingItems to the overall list
                Iterator subIter = bookingItemsRequiringFile.iterator();
                while (subIter.hasNext()){
                    BookingItem item = (BookingItem)subIter.next();
                    if (!result.contains(item)){
                        result.add(item);
                        System.out.println("  getSharedResourceItems1: file names:"+fileNames.toString()+" is used by "+item.getChangeRequest().getCRNumber());
                        itemsAdded = true;
                    }
                }
            }
            
            // Now that we've got all the booking items that are using all the files in 'fileNames',
            // we need to reconstruct 'fileNames' with not only the original fileNames, but also
            // ALL the other files required by said BookingItems
            fileNames = getUniqueFileNames(result);
        }
        
        return result;
    }
    
    /** For a given file, return all the bookingItems that require that file
     */
    private LinkedList getSharedResourceItems(String fileName, LinkedList allBookingItems){
        LinkedList sharedBookingItems = new LinkedList();
        Iterator iter = allBookingItems.iterator();
        while (iter.hasNext()){
            BookingItem item = (BookingItem)iter.next();
            if ((bookingItemRequiresFile(item, fileName)) &&
            (!sharedBookingItems.contains(item))){
                sharedBookingItems.add(item);
                System.out.println("    getSharedResourceItems2:"+fileName+" is used by "+item.getChangeRequest().getCRNumber());
            }
        }
        return sharedBookingItems;
    }
    
    
    private boolean bookingItemRequiresFile(BookingItem item, String fileName){
        Iterator iter = item.getRequiredFileNames().iterator();
        while (iter.hasNext()){
            if (((String)iter.next()).compareToIgnoreCase(fileName)==0){
                return true;
            }
        }
        return false;
    }
    
    
    /** Returns a unique list of filenames required by these BookingItems
     */
    public LinkedList getUniqueFileNames(LinkedList bookingItems){
        LinkedList fileNames = new LinkedList();
        Iterator iter = bookingItems.iterator();
        while (iter.hasNext()){
            BookingItem item = (BookingItem)iter.next();
            LinkedList itemFiles = item.getRequiredFileNames();
            Iterator fileIter = itemFiles.iterator();
            while (fileIter.hasNext()){
                String fileName = (String)fileIter.next();
                if (!fileNames.contains(fileName)){
                    fileNames.add(fileName);
                    System.out.println("      getUniqueFileNames:bookingItems:"+bookingItems+" uses "+fileName);
                }
            }
        }
        return fileNames;
    }
    
    /** Does this CombinedChangeRequest include the given ChangeRequest Number?
     */
    public boolean includes(String crNumber){
        Iterator iter = includedBookingItems.iterator();
        while (iter.hasNext()){
            BookingItem item = (BookingItem)iter.next();
            ChangeRequest cr = item.getChangeRequest();
            if (cr.getCRNumber().compareToIgnoreCase(crNumber)==0){
                return true;
            }
        }
        return false;
    }
    
    /** Does this CombinedChangeRequest overlap in its contents with the
     * one passed as a parameter?
     */
    public boolean overlaps(CombinedChangeRequest combinedCR){
        Iterator otherIter = combinedCR.getIncludedBookingItems().iterator();
        while (otherIter.hasNext()){
            BookingItem otherItem = (BookingItem)otherIter.next();
            ChangeRequest cr = otherItem.getChangeRequest();
            if (this.includes(cr.getCRNumber())){
                return true;
            }
        }
        return false;
    }
    
    /** Getter for property includedBookingItems.
     * @return Value of property includedBookingItems.
     *
     */
    public java.util.LinkedList getIncludedBookingItems() {
        return includedBookingItems;
    }
    
    /** Setter for property includedBookingItems.
     * @param includedBookingItems New value of property includedBookingItems.
     *
     */
    public void setIncludedBookingItems(java.util.LinkedList includedBookingItems) {
        this.includedBookingItems = includedBookingItems;
    }
    
    public String toString(){
        StringBuffer result = new StringBuffer();
        Iterator iter = includedBookingItems.iterator();
        while (iter.hasNext()){
            String crNumber = ((BookingItem)iter.next()).getChangeRequest().getCRNumber();
            result.append(crNumber);
            result.append(", ");
        }
        return result.toString();
        
    }
    
    
    private LinkedList includedBookingItems;
    private BookingItem baseItem;
}
