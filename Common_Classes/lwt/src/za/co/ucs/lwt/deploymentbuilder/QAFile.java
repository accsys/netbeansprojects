/*
 * QAFile.java
 *
 * Created on November 27, 2003, 8:33 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/**
 * QAFile is the container for a file, as well as it's distribution properties.
 * Each QAFile knows how it should be extracted and backed up for uninstallation
 * purposes at a later stage.
 * @author  liam
 */
public class QAFile implements java.io.Serializable{

    static final long serialVersionUID = -7021285268942403425L;

    private void readObject(java.io.ObjectInputStream stream)
    throws IOException, ClassNotFoundException{
        try{
            // Start by reading the contents of the file Header
            this.file = new File(FileContainer.getTemporaryFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+((File)stream.readObject()).getName());
            
            // Now, let's rename this file so that we work in a 'current' folder
            //this.file = new File(this.file.getName());
            this.installGroup = (InstallGroup)stream.readObject();
            this.fileLength = stream.readDouble();
            this.originalFilePath = (String)stream.readObject();
            
            // Now, let's read the contents of the file
            FileOutputStream fos = new FileOutputStream(new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+this.file.getName()));
            
            DataInputStream dis = new DataInputStream(stream);
            double remainingBytes = this.fileLength;
            byte data[] = new byte[BUFFER];
            while (remainingBytes > BUFFER){
                dis.readFully(data);
                fos.write(data);
                remainingBytes -= BUFFER;
            }
            for (int I=0; I<remainingBytes; I++){
                fos.write(stream.read());
            }
            fos.close();
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)
    throws IOException{
        try{
            
            // Start by Writing the contents of the file Header
            stream.writeObject(file);
            stream.writeObject(installGroup);
            stream.writeDouble(getUncompressedFileLength());
            stream.writeObject(originalFilePath);
            
            // Now, let's write the contents of the file
//            FileInputStream fis = new FileInputStream(new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+file.getName()));
                        FileInputStream fis = new FileInputStream(this.getFile());
            
            double remainingBytes = this.fileLength;
            byte data[] = new byte[BUFFER];
            while (remainingBytes > BUFFER){
                fis.read(data);
                stream.write(data);
                remainingBytes -= BUFFER;
            }
            for (int I=0; I<remainingBytes; I++){
                stream.write(fis.read());
            }
            fis.close();
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
    }
    
    /** installType = (PROGRAM, DLL, etc)
     * See the enumerators defined in the InstallGroup class
     */
    public QAFile(File file, int installType){
        this.file = file;
        this.fileLength = file.length();
        this.originalFilePath = file.getAbsolutePath();
        installGroup = new InstallGroup(installType);
    }
    
    public double getUncompressedFileLength(){
        return fileLength;
    }
    
    public InstallGroup getInstallGroup(){
        return this.installGroup;
    }
    
    public File getFile(){
        return this.file;
    }
    
    @Override
    public String toString(){
        return (file.getName()+"["+installGroup.toString()+"]");
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj.getClass() != QAFile.class){
            return false;
        }
        if ( ((QAFile)obj).getFile().getName().compareToIgnoreCase(this.getFile().getName())==0){
            return true;
        }
        return false;
    }
    
    public String getOriginalFilePath(){
        return this.originalFilePath;
    }
    
    public void setOriginalFilePath(String originalFilePath){
        this.originalFilePath = originalFilePath;
    }
    
    private File file;
    private double fileLength;
    private String originalFilePath;
    private InstallGroup installGroup;
    static final int BUFFER = 2048;
    
}
