/*
 * ExtendedChangeRequest.java
 *
 * Created on February 3, 2004, 11:41 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * As the name implies, the ExtendedChangeRequest is an extension to ChangeRequest.
 * It was necessary to extend ChangeRequest as the original class is already in use in
 * the live implementation of the Deployment Builder.
 * @author  liam
 */
public class ExtendedChangeRequest extends ChangeRequest {
    
    /** Creates a new instance of ExtendedChangeRequest */
    public ExtendedChangeRequest(String crNumber) {
        super(crNumber);
        updateFromDB();
    }
    
    private void updateFromDB(){
        try{
            LinkedList result = new LinkedList();
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "select fn_S_GetPriority(CRNumber) as Priority, "+
            " fn_S_GetActualTime(CRNumber) as ActualTime, "+
            " fn_S_GetEstimatedTime(CRNumber) as EstimatedTime from cr "+
            " where upper(CRNumber)='"+this.getCRNumber().toUpperCase()+"'";
            ResultSet rs = databaseObject.openSQL(sqlString, con);
            while (rs.next()){
                priority = new Integer(rs.getInt("Priority"));
                estimatedHours = new Integer(rs.getInt("EstimatedTime"));
                actualHours = new Integer(rs.getInt("ActualTime"));
            }
            rs.close();
            databaseObject.releaseConnection(con);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    
    /** Getter for property estimatedHours.
     * @return Value of property estimatedHours.
     *
     */
    public java.lang.Integer getEstimatedHours() {
        return estimatedHours;
    }
    
    
    /** Getter for property actualHours.
     * @return Value of property actualHours.
     *
     */
    public java.lang.Integer getActualHours() {
        return actualHours;
    }
    
    
    /** Getter for property priority.
     * @return Value of property priority.
     *
     */
    public java.lang.Integer getPriority() {
        return priority;
    }
    
    
    private Integer estimatedHours;
    private Integer actualHours;
    private Integer priority;
    
}
