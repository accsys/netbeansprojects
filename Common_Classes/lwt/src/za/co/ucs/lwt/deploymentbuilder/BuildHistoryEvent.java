/*
 * BuildHistoryEvent.java
 *
 * Created on April 2, 2004, 12:45 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import za.co.ucs.lwt.db.*;
/**
 *
 * @author  liam
 */
public class BuildHistoryEvent implements java.io.Serializable {
    
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
        C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.lwt.deploymentbuilder.BuildHistoryEvent
za.co.ucs.lwt.deploymentbuilder.BuildHistoryEvent:    static final long serialVersionUID = 5857080544350316674L;
     
     */
    static final long serialVersionUID = 5857080544350316674L;
    
    /** Creates a new instance of BuildHistoryEvent
     * @param user
     * @param buildInstallation */
    public BuildHistoryEvent(User user, BuildInstallation buildInstallation) {
        if (buildInstallation==null) {
            System.out.println("Null BuildInstallation - No Event History saved.");
            System.out.print(this.objectName);
        }
        this.user = user;
        this.currentTimeStamp = DatabaseObject.getInstance().getCurrentDateAndTime();
        this.eventDetail = getHTMLBuildBreakdown(buildInstallation);
    }
    
    /** Assumed to be inserted into an existing HTLP page at a precise place:
     * <table>
     * <tr><td>...</td>
     * <td>...</td>
     * .....
     * <td>...</td>
     * </tr>
     * </table>
     */
    private String getHTMLBuildBreakdown(BuildInstallation buildInstallation){
        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        StringBuilder rslt = new StringBuilder();
        // Build Name
        rslt.append("<td>Build:<br><b>").append(buildInstallation.getBuildName()).append("</b>" + "<br><i>").append(buildInstallation.getBuildDescription()).append("</i>");
        rslt.append("<font size=\"-1\">");
        // Change Requests and related files
        LinkedList testInstallations = buildInstallation.getTestInstallations();
        rslt.append("</td><td><table border=1>");
        for (Object testInstallation1 : testInstallations) {
            TestInstallation testInstallation = (TestInstallation) testInstallation1;
            ChangeRequest changeRequest = testInstallation.getChangeRequest();
            changeRequest.refreshFromDatabase();
            // CR Info
            rslt.append("<tr>");
            rslt.append("<td").append(fileContainer.getChangeRequestBackgroundColor(changeRequest)).append(">");
            rslt.append("TestInstallation:  <a href=\"ChangeRequestInfo.jsp?crnumber=").append(changeRequest.getCRNumber()).append("\">").append(changeRequest.getCRNumber()).append("</a><br>");
            rslt.append("Required DB Version:").append(buildInstallation.getRequiredDBVersion()).append("<br>");
            rslt.append("</td>");
            rslt.append(getHTMLFileDetail(testInstallation));
            rslt.append("</tr>");
            // Add Change Request Number to LinkedList
        }
        rslt.append("</td></table></font>");
        return (rslt.toString());
    }
    
    /** Returns TRUE if the current BuildHistoryEvent contains the given ChangeRequest Number (String)
     * @param crNumber
     * @return 
     */
    public boolean containsChangeRequestNumber(String crNumber){
        String subString = "ChangeRequestInfo.jsp?crnumber=";
        StringBuilder eventBuffer = new StringBuilder(eventDetail);
        while (eventBuffer.length()>0){
            int crLocation = eventBuffer.indexOf(subString);
            if (crLocation < 0){
                return false;
            }
            if (crLocation+subString.length()>eventBuffer.length()){
                return false;
            }
            //System.out.println("eventBuffer.size="+eventBuffer.length()+"; crLocation="+crLocation+"; subString.length()="+subString.length());
            String crEventNumber = eventBuffer.substring(crLocation+subString.length(), crLocation+subString.length() + 5);
            if (crNumber.trim().compareToIgnoreCase(crEventNumber.trim())==0){
                return true;
            }
            eventBuffer.delete(crLocation,crLocation+subString.length()+4);
        }
        return false;
    }
    
    /** Returns a string containing the Build name and description
     * @return 
     */
    public String getBuildNameAndDescription(){
        // locate the CRNumber in the HTMLBreakdown
        int crLocation = eventDetail.indexOf("Build:<br><b>");
        int crLocationEnd = eventDetail.indexOf("</td>",crLocation)-1;
        if (crLocation>0 && crLocationEnd>0){
            String crInternalValue = eventDetail.substring(crLocation,crLocationEnd);
            return (crInternalValue);
        } else {return crLocation+" to "+crLocationEnd;}
    }
    
    
    private String getHTMLFileDetail(TestInstallation testInstallation){
        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        
        StringBuilder rslt = new StringBuilder();
        // Now we list the associated files, per group type
        rslt.append("<td><font size=\"-1\"><dl>");
        int counter;
        // TYPE = PROGRAM
        LinkedList programFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.PROGRAM);
        if (programFiles.size()>0){
            rslt.append("<dt><b>Program Files</b> [").append(programFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<programFiles.size(); counter++){
                if (((QAFile)programFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)programFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)programFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)programFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = SCRIPT
        LinkedList scriptFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.SCRIPT);
        if (scriptFiles.size()>0){
            rslt.append("<dt><b>Script</b> [").append(scriptFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<scriptFiles.size(); counter++){
                if (((QAFile)scriptFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)scriptFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)scriptFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)scriptFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = CONFIG
        LinkedList configFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.CONFIG);
        if (configFiles.size()>0){
            rslt.append("<dt><b>Config</b> [").append(configFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<configFiles.size(); counter++){
                if (((QAFile)configFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)configFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)configFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)configFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = UTILITIES
        LinkedList utilityFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.UTILITY);
        if (utilityFiles.size()>0){
            rslt.append("<dt><b>Utilities</b> [").append(utilityFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<utilityFiles.size(); counter++){
                if (((QAFile)utilityFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)utilityFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)utilityFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)utilityFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = DLL
        LinkedList dllFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.DLL);
        if (dllFiles.size()>0){
            rslt.append("<dt><b>Dll's</b> [").append(dllFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<dllFiles.size(); counter++){
                if (((QAFile)dllFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)dllFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)dllFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)dllFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = XML
        LinkedList xmlFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.XML);
        if (xmlFiles.size()>0){
            rslt.append("<dt><b>XML's</b> [").append(xmlFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<xmlFiles.size(); counter++){
                if (((QAFile)xmlFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)xmlFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)xmlFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)xmlFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        // TYPE = REPORT
        LinkedList reportFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.REPORT);
        if (reportFiles.size()>0){
            rslt.append("<dt><b>Reports</b> [").append(reportFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<reportFiles.size(); counter++){
                if (((QAFile)reportFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)reportFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)reportFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)reportFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        
        // TYPE = HELP
        LinkedList helpFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.HELP);
        if (helpFiles.size()>0){
            rslt.append("<dt><b>Help Files</b> [").append(helpFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<helpFiles.size(); counter++){
                if (((QAFile)helpFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)helpFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)helpFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)helpFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }


        // TYPE = SYSTEM
        LinkedList systemFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.SYSTEM);
        if (systemFiles.size()>0){
            rslt.append("<dt><b>System Files</b> [").append(systemFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<systemFiles.size(); counter++){
                if (((QAFile)systemFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)systemFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)systemFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)systemFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }

        // TYPE = COMMS
        LinkedList commsFiles = fileContainer.getQAFiles(testInstallation, InstallGroup.COMMS);
        if (commsFiles.size()>0){
            rslt.append("<dt><b>Comms</b> [").append(commsFiles.size()).append(" files]</dt><dd></dd>");
            for (counter=0; counter<commsFiles.size(); counter++){
                if (((QAFile)commsFiles.get(counter)).getFile().exists()){
                    rslt.append("<dt></dt><dd>").append(((QAFile)commsFiles.get(counter)).getFile().getName()).append("  [").append(((QAFile)commsFiles.get(counter)).getFile().length()).append(" bytes]</dd>");
                } else {
                    rslt.append("<dt></dt><dd>").append(((QAFile)commsFiles.get(counter)).getFile().getName()).append(" </dd>");
                }
            }
        }
        rslt.append("</dl></font></td>");
        
        return (rslt.toString());
        
    }
    
    
    public String getObjectName(){
        return this.objectName;
    }
    
    public int getEventType(){
        return this.eventType;
    }
    
    public String getUserName(){
        if (this.user != null) {
            return this.user.getName();
        }
        else {
            return "Unknown User";
        }
    }
    
    
    public String getTimeStamp(){
        return this.currentTimeStamp;
    }
    
    /** Getter for property eventDetail.
     * @return Value of property eventDetail.
     *
     */
    public java.lang.String getEventDetail() {
        return eventDetail;
    }
    
    /** Setter for property eventDetail.
     * @param eventDetail New value of property eventDetail.
     *
     */
    public void setEventDetail(java.lang.String eventDetail) {
        this.eventDetail = eventDetail;
    }
    
    private User user = null;
    private int eventType;
    private String objectName;
    private String currentTimeStamp;
    private String eventDetail;
    
    
    
}
