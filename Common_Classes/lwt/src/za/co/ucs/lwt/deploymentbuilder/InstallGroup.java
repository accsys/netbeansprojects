/*
 * InstallGroup.java
 *
 * Created on November 27, 2003, 8:50 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java .io.*;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Defines the behavior of the QAFile that is linked to this object
 * @author  liam
 */
public class InstallGroup implements java.io.Serializable {
    private static String pwd2 = "qa,dev7";
    private static String pwd1 = "I,don'tKNOW!1t.";
    private static String validPwd = null;
    
    /** Creates a new instance of InstallGroup */
    public InstallGroup(int groupType) {
        this.groupType = groupType;
    }
    
    public int getGroupType(){
        return this.groupType;
    }
    
    public void setGroupType(int groupType){
        this.groupType = groupType;
    }
    
    public File getInstallFolder(File baseFolder){
        // Program Files
        if (groupType == PROGRAM){
            return baseFolder;
        }
        if (groupType == CONFIG){
            return new File(baseFolder.getAbsolutePath()+"\\Config");
        }
        if (groupType == DLL){
            return new File(baseFolder.getAbsolutePath()+"\\DLLs");
        }
        if (groupType == UTILITY){
            return new File(baseFolder.getAbsolutePath()+"\\Utilities");
        }
        if (groupType == HELP){
            return new File(baseFolder.getAbsolutePath()+"\\Help Documents");
        }
        if (groupType == SCRIPT){
            return new File(baseFolder.getAbsolutePath()+"\\Scripts\\SFI");
        }
        if (groupType == XML){
            return new File(baseFolder.getAbsolutePath()+"\\XMLs");
        }
        if (groupType == REPORT){
            return new File(baseFolder.getAbsolutePath()+"\\Reports");
        }
        if (groupType == COMMS){
            return new File(baseFolder.getAbsolutePath()+"\\Comms");
        }
        if (groupType == SYSTEM){
            return new File(baseFolder.getAbsolutePath()+"\\System32");
        }
        
        return (new File(baseFolder.getAbsolutePath()+"\\Unknown Type"));
        
    }
    
    public String toString(){
        if (groupType == PROGRAM) return "PROGRAM";
        if (groupType == CONFIG) return "CONFIG";
        if (groupType == DLL) return "DLL";
        if (groupType == HELP) return "HELP";
        if (groupType == SCRIPT) return "SCRIPT";
        if (groupType == UTILITY) return "UTILITY";
        if (groupType == SYSTEM) return "SYSTEM";
        if (groupType == XML) return "XML";
        if (groupType == REPORT) return "REPORT";
        if (groupType == COMMS) return "COMMS";
        return ("UNKNOWN");
    }
    
    /** Each Installation Group might have specific actions that must
     * take place as part of the installation
     */
    public QAFile performTasksBefore(QAFile file, File installFolder){
        System.out.println("");
        System.out.println("performTasksBefore for "+file);
        if (groupType == SCRIPT){
            return performTasksBefore_SCRIPT(file);
        }
        // Before replacing existing files, we need to create a 'backup' archive that we can use
        //    at a later stage to restore to
        File toFile = new File(file.getInstallGroup().getInstallFolder(installFolder).getAbsolutePath()+FileContainer.getOSFileSeperator()+file.getFile().getName());
        if (toFile.exists()){
            return (new QAFile(toFile, file.getInstallGroup().getGroupType()));
        } else{
            return null;
        }
    }
    
    /** Each Installation Group might have specific actions that must
     * take place as part of the installation
     */
    public void performTasksAfter(QAFile file){
        System.out.println("");
        System.out.println("performTasksAfter for "+file);
        if (groupType == SCRIPT){
            performTasksAfter_SCRIPT(file);
        } else
            return;
    }
    
    /** Before installing Scripts, one needs to 'backup' the existing script.
     *  file - the Script file that is about to be installed
     */
    private QAFile performTasksBefore_SCRIPT(QAFile file){
        ScriptBackup backup = ScriptBackup.getInstance();
        File backupFile = backup.createBackup(file);
        if (backupFile != null)
            return (new QAFile(backupFile, file.getInstallGroup().getGroupType()));
        else
            return null;
    }
    
    /** After installing Scripts, one needs to 'run' the new script
     */
    private void performTasksAfter_SCRIPT(QAFile file){
        if ( (file.getFile().getName().toUpperCase().endsWith("TXT")) ) {
            // Should we skip the running of scripts?
            if (FileContainer.getConfigProperty("SkipScripts").compareToIgnoreCase("YES")!=0){
                
                
                // Which password do we use?
                if (validPwd == null) {
                    System.out.println("Establishing correct Password...");
                    try{
                        Process estConnection = java.lang.Runtime.getRuntime().exec("dbping -c \"uid=DBA; pwd="+pwd1+"; DSN=VISAGENERAL\"");
                        estConnection.waitFor();
                        if (estConnection.exitValue()>0){
                            validPwd = pwd2;
                        } else {
                            validPwd = pwd1;
                        }
                        
                        System.out.println("         password used:"+validPwd);
                        
                        
                    } catch (InterruptedException ie){
                        javax.swing.JOptionPane.showMessageDialog(null,ie.getMessage());
                    } catch(IOException ioe2){
                        javax.swing.JOptionPane.showMessageDialog(null, "Exception in running of Scripts:"+ioe2.getMessage());
                    }
                }
                
                System.out.println("Command:"+"dbisqlc -q -nogui -c \"uid=DBA; pwd="+validPwd+"; DSN=VISAGENERAL\" read \""+file.getFile().getAbsolutePath()+"\"");
                try {
                    // Try running with DBISQLG password 1
                    Process process = java.lang.Runtime.getRuntime().exec("dbisqlc -q -nogui -c \"uid=DBA; pwd="+validPwd+"; DSN=VISAGENERAL\" read \""+file.getFile().getAbsolutePath()+"\"");
                    process.waitFor();
                    if (process.exitValue()>0){
                        javax.swing.JOptionPane.showMessageDialog(null, "Exception in running scripts in file:"+file.getFile().getAbsolutePath());
                    }
                } catch (InterruptedException ie){
                    javax.swing.JOptionPane.showMessageDialog(null,ie.getMessage());
                } catch (IOException ioe){
                    javax.swing.JOptionPane.showMessageDialog(null, "Exception in running of Scripts:"+ioe.getMessage());
                }
            }
        }
    }
    
    /*
     *  /u1/lwt_work/j2sdk1.4.2/bin/serialver
     *  -classpath /u1/lwt_work/jakarta-tomcat-5.0.16/webapps/DeploymentBuilder/WEB-INF/lib/lwt.jar z
     * a.co.ucs.lwt.deploymentbuilder.InstallGroup
     */
    static final long serialVersionUID = -2310629005511420405L;
    
    private int groupType = 0;
    
    public static int PROGRAM = 0;
    public static int CONFIG  = 1;
    public static int DLL     = 2;
    public static int HELP    = 3;
    public static int SCRIPT  = 4;
    public static int UTILITY = 5;
    public static int SYSTEM  = 6;
    public static int XML     = 7;
    public static int REPORT  = 8;
    public static int COMMS   = 9;
}

/** This private static class is used to create a bakcup of a procedure or function before
 * it gets overwritten by the new one
 */
class ScriptBackup{
    
    private static synchronized void createInstance(){
        if (scriptBackup == null){
            scriptBackup = new ScriptBackup();
        }
    }
    
    /** Constructor (Private)
     */
    private ScriptBackup(){
    }
    
    /** Get single instance of this class. */
    public static ScriptBackup getInstance(){
        if (scriptBackup == null)
            ScriptBackup.createInstance();
        return scriptBackup;
    }
    
    /** scriptFile - the Script that will modify/create a Stored Procedure, Function or View
     */
    public File createBackup(QAFile scriptFile){
        String entityName = getFileNameWithoutExtension(scriptFile.getFile());
        int scriptType = getScriptType(scriptFile.getFile());
        File backupFile = null;
        
        // Get the original procedure, function or view
        String originalDefinition = getBackupScriptStatement(entityName, scriptType);
        
        // If this is a valid script, save it for the archive
        if (originalDefinition.trim().length()>0){
            try{
                //Let us put this temporary backup of the function,view, etc in a temporary folder.
                
                String backupFileName = System.getProperty("java.io.tmpdir")+scriptFile.getFile().getName();
                backupFile = new File(backupFileName);
                StringBuffer buffer = new StringBuffer();
                String c;
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(backupFile));
                bufferedWriter.write(originalDefinition);
                bufferedWriter.close();
                return backupFile;
            } catch ( IOException ioe ){
                ioe.printStackTrace();
                return null;
            }
        }
        return null;
        
    }
    
    /** Removes the extension from a file
     */
    private String getFileNameWithoutExtension(File file){
        String fileName = file.getName();
        String result = fileName.substring(0, file.getName().indexOf('.'));
        return result;
    }
    
    /** Scan script file to see if this is a function, procedure or view.
     */
    private int getScriptType(File scriptFile){
        try{
            System.out.println("Scanning for ScriptType:"+scriptFile.getAbsolutePath());
            StringBuffer buffer = new StringBuffer();
            String c;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(scriptFile));
            while((c = bufferedReader.readLine()) != null){
                buffer.append(c);
            }
            // *** DEBUG ***
            System.out.println(" New Script contents:"+buffer.toString());
            // Test for key words (Procedure, Function, View)
            if (buffer.toString().compareToIgnoreCase("PROCEDURE")>0){
                return PROCEDURE;
            }
            if (buffer.toString().compareToIgnoreCase("FUNCTION")>0){
                return FUNCTION;
            }
            if (buffer.toString().compareToIgnoreCase("VIEW")>0){
                return VIEW;
            }
            return OTHER;
        } catch( FileNotFoundException fnfe ) {
            fnfe.printStackTrace();
            return OTHER;
        } catch( IOException ioe ) {
            ioe.printStackTrace();
            return OTHER;
        }
    }
    
    /** Returns the current procedure, function, view as it exist in the database.
     * <br> entityName - name of procedure, function, etc
     * <br> scriptType - enumerated. (See static declarations)
     */
    private String getBackupScriptStatement(String entityName, int scriptType){
        try{
            // Connect to Accsys Peopleware via the ODBC connection
            DatabaseObject.setConnectInfo_PeopleWareLocalHost();
        
            Connection con = DatabaseObject.getNewConnection();
            // Procedure
            if (scriptType == PROCEDURE ){
                String sqlString = "if exists (select * from sysprocedure where upper(proc_name) = '"+
                        entityName.toUpperCase()+"'  ) then  drop procedure "+entityName+"; "+
                        "end if; commit;";
                // Get original procedure
                ResultSet rs = DatabaseObject.openSQL("select proc_defn from sysprocedure where upper(proc_name) = '"+entityName.toUpperCase()+"'",
                        con);
                while (rs.next()){
                    sqlString = sqlString + rs.getString(1);
                }
                return sqlString;
            }
            // Function
            if (scriptType == FUNCTION ){
                String sqlString = "if exists (select * from sysprocedure where upper(proc_name) = '"+
                        entityName.toUpperCase()+"'  ) then  drop function "+entityName+"; "+
                        "end if; commit;";
                // Get original procedure
                ResultSet rs = DatabaseObject.openSQL("select proc_defn from sysprocedure where upper(proc_name) = '"+entityName.toUpperCase()+"'",
                        con);
                while (rs.next()){
                    sqlString = sqlString + rs.getString(1);
                }
                return sqlString;
            }
            
            // View
            if (scriptType == VIEW ){
                String sqlString = "if exists (select * from systable where upper(table_type) = ''VIEW'' and upper(table_name)='"+
                        entityName.toUpperCase()+"'  ) then  drop view "+entityName+"; "+
                        "end if; commit;";
                // Get original procedure
                ResultSet rs = DatabaseObject.openSQL("select view_def from systable where upper(table_name) = '"+entityName.toUpperCase()+"'",
                        con);
                while (rs.next()){
                    sqlString = sqlString + rs.getString(1);
                }
                return sqlString;
            }
        } catch( SQLException sqle ) {
            sqle.printStackTrace();
            return "";
        }
        return "";
    }
    
    private static ScriptBackup scriptBackup;
    private static int PROCEDURE = 0;
    private static int FUNCTION  = 1;
    private static int VIEW      = 3;
    private static int OTHER     = 4;
    
}
