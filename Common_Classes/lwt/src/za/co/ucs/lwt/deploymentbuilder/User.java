/*
 * User.java
 *
 * Created on December 10, 2003, 9:10 AM
 */

package za.co.ucs.lwt.deploymentbuilder;

/**
 * User class is used to define the users and the categories they belong to.
 * These users can be retrieved from the USR table in the TimeSheet database.
 * @author  liam
 */
public class User implements java.io.Serializable {
    
    /** Creates a new instance of User */
    public User(String loginName) {
        this.loginName = loginName;
        refreshFromDatabase();
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setCategory(String category){
        this.category = category;
    }
    
    public void setUserID(String userId){
        this.userId = userId;
    }
    
    public void setLoginName(String loginName){
        this.loginName = loginName;
    }
    
    public void setLoginPwd(String loginPwd){
        this.loginPwd = loginPwd;
    }
    
    public String getName(){
        if (this.name == null) {
            this.name = "";
        }
        return this.name;
    }
    
    
    public String getCategory(){
        if (this.category == null) {
            this.category = "";
        }
        return this.category;
    }
    
    public String getUserID(){
        if (this.userId == null) {
            this.userId = "";
        }
        return this.userId;
    }
    
    public String getLoginName(){
        if (this.loginName == null) {
            this.loginName = "";
        }
        return this.loginName;
    }
    
    public String getLoginPwd(){
        if (this.loginPwd == null) {
            this.loginPwd = "";
        }
        return this.loginPwd;
    }
    
    public String getEMail(){
        if (this.e_mail == null) {
            this.e_mail = "";
        }
        return this.e_mail;
    }
    
    public void setEMail(String e_mail){
        this.e_mail = e_mail;
    }
        
    /** Populate a Change Request's properties from the
     * CR table in the TimeSheet Database
     */
    public boolean refreshFromDatabase(){
        return (UserDatabaseInterface.getInstance().refreshFromDatabase(this));
    }
    
    public boolean userExistsInDb(){
        return refreshFromDatabase();
    }
    
    private String name;
    private String category;
    private String userId;
    private String loginName;
    private String loginPwd;
    private String e_mail;
    
    
}
