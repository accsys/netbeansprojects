/*
 * BuildInstallation.java
 *
 * Created on December 17, 2003, 2:43 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/**
 * BuildInstallation contains a list of TestInstallations
 * @author  liam
 */
public class BuildInstallation implements java.io.Serializable   {
    
    /** Creates a new instance of BuildInstallation */
    public BuildInstallation(String buildName) {
        this.buildName = buildName;
        this.testInstallations = new LinkedList();
    }
    
    public String getBuildName(){
        return this.buildName;
    }
    
    public String getNewDBVersion(){
        return this.newDBVersion;
    }
    
    public void setBuildName(String buildName){
        this.buildName = buildName;
    }
    
    public void setNewDBVersion(String newDBVersion){
        this.newDBVersion = newDBVersion;
    }
    
    public void addTestInstallation(TestInstallation testInstallation){
        testInstallations.add(testInstallation);
    }
    
    private String getMinRequiredDBVersion(){
        Iterator iter = testInstallations.iterator();
        String requiredDBVersion = "";
        while (iter.hasNext()){
            TestInstallation testInstallation = (TestInstallation)iter.next();
            if (testInstallation.getChangeRequest().getRequiredDBVersion().compareToIgnoreCase(requiredDBVersion)>0){
                requiredDBVersion = testInstallation.getChangeRequest().getRequiredDBVersion();
            }
        }
        return requiredDBVersion;
    }
    
    public String getRequiredDBVersion(){
        return getMinRequiredDBVersion();
    }
    
    private String getMaxRequiredDBVersion(){
        Iterator iter = testInstallations.iterator();
        String requiredDBVersion = "";
        while (iter.hasNext()){
            TestInstallation testInstallation = (TestInstallation)iter.next();
            if (testInstallation.getChangeRequest().getRequiredDBVersion().compareToIgnoreCase(requiredDBVersion)>0){
                requiredDBVersion = testInstallation.getChangeRequest().getRequiredDBVersion();
            }
        }
        return requiredDBVersion;
    }
    
    public LinkedList getTestInstallations(){
        return testInstallations;
    }
    
    public void clear(){
        testInstallations.clear();
    }
    
    public String getReadMe(){
        String rslt = "";
        Iterator iter = testInstallations.iterator();
        while (iter.hasNext()){
            TestInstallation testInstallation = (TestInstallation)iter.next();
            rslt = rslt + "[CR "+testInstallation.getChangeRequest().getCRNumber()+"]"+
                    "\n  "+testInstallation.getChangeRequest().getReadMe()+"\n";
        }
        return rslt;
    }
    public String getHTMLReadMe(){
        String rslt = "";
        Iterator iter = testInstallations.iterator();
        while (iter.hasNext()){
            TestInstallation testInstallation = (TestInstallation)iter.next();
            rslt = rslt + "[CR "+testInstallation.getChangeRequest().getCRNumber()+"]"+
                    "<br>- "+testInstallation.getChangeRequest().getReadMe()+"<br>";
        }
        return rslt;
    }
    
    public String getHTMLInstallationInstructions(){
        String rslt = "";
        Iterator iter = testInstallations.iterator();
        while (iter.hasNext()){
            TestInstallation testInstallation = (TestInstallation)iter.next();
            
            if (testInstallation.getInstallationInstructions().length()>0){
                rslt = rslt + "[CR "+testInstallation.getChangeRequest().getCRNumber()+"]"+
                        "<br>"+testInstallation.getInstallationInstructions()+"<br>";
            }
        }
        return rslt;
    }
    /** Returns the total number of QA files in all the TestInstallations
     * that are part of this build
     */
    private int getNumberOfQAFiles(){
        int rslt = 0;
        Iterator iter = testInstallations.iterator();
        while (iter.hasNext()){
            rslt += ((TestInstallation)iter).getQAFiles().size();
        }
        return rslt;
    }
    
    /** Getter for property buildDescription.
     * @return Value of property buildDescription.
     *
     */
    public java.lang.String getBuildDescription() {
        if (this.buildDescription == null){
            this.buildDescription = "";
        }
        return buildDescription;
    }
    
    /** Setter for property buildDescription.
     * @param buildDescription New value of property buildDescription.
     *
     */
    public void setBuildDescription(java.lang.String buildDescription) {
        this.buildDescription = buildDescription;
    }
    
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     */
    static final long serialVersionUID = -6312192530167720139L;
    
    private String newDBVersion = "";
    private String buildName = "";
    private String buildDescription = "";
    private LinkedList testInstallations;
}
