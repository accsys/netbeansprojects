/*
 * ChangeRequest.java
 *
 * Created on December 4, 2003, 8:47 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * ChangeRequest is the encapsulation of a StarTeam Change Request
 * @author  liam
 */
public class ChangeRequest implements java.io.Serializable {
    
    /** Creates a new instance of ChangeRequest */
    public ChangeRequest(String crNumber) {
        this.crNumber = crNumber;
        refreshFromDatabase();
    }
    
    public void setReadMe(String readMe){
        this.readMe = readMe;
    }
    
    public String getCRNumber(){
        return crNumber;
    }
    
    public String getReadMe(){
        return readMe;
    }
    
    public void setStatus(String status){
        this.status = status;
    }
    
    public String getStatus(){
        return this.status;
    }
    
    public void setSynopsis(String synopsis){
        this.synopsis = synopsis;
    }
    
    public String getSynopsis(){
        return this.synopsis;
    }
    
    public void setDescription(String description){
        this.description = description;
    }
    
    public String getDescription(){
        return this.description;
    }
    
    public void setRequiredDBVersion(String requiredDBVersion){
        this.requiredDBVersion = requiredDBVersion.toUpperCase();
    }
    
    public String getRequiredDBVersion(){
        return (this.requiredDBVersion.toUpperCase());
    }
    
    public void setResponsibility(String responsibility){
        this.responsibility = responsibility;
    }
    
    public String getResponsibility(){
        return this.responsibility;
    }
    
    public void setCategory(String category){
        this.category = category;
    }
    
    public String getCategory(){
        return this.category;
    }
    
    public void setType(String type){
        this.type = type;
    }
    
    public String getType(){
        if (this.type == null) {
            return "";
        } else {
            return this.type;
        }
    }
    
    public void setSeverity(String severity){
        this.severity = severity;
    }
    
    public String getSeverity(){
        return this.severity;
    }
    
    public void setEnteredBy(String enteredBy){
        this.enteredBy = enteredBy;
    }
    
    public String getEnteredBy(){
        return this.enteredBy;
    }
    
    public void setEnteredOn(String enteredOn){
        this.enteredOn = enteredOn;
    }
    
    public String getEnteredOn(){
        return this.enteredOn;
    }
    
    /** Populate a Change Request's properties from the
     * CR table in the TimeSheet Database
     */
    public void refreshFromDatabase(){
        ChangeRequestDatabaseInterface.getInstance().refreshFromDatabase(this);
    }
    
    private String requiredDBVersion;
    private String crNumber;
    private String status;
    private String synopsis;
    private String readMe;
    private String description;
    private String responsibility;
    private String category;
    private String severity;
    private String type;
    private String enteredBy;
    private String enteredOn;
}
