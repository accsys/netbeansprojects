/*
 * HistoryEvent.java
 *
 * Created on December 19, 2003, 12:52 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import za.co.ucs.lwt.db.*;
/**
 *
 * @author  liam
 */
public class HistoryEvent implements java.io.Serializable {
    
    /** Creates a new instance of HistoryEvent */
    public HistoryEvent(User user, int eventType, String objectName) {
        this.user = user;
        this.eventType = eventType;
        this.objectName = objectName;
        this.currentTimeStamp = DatabaseObject.getInstance().getCurrentDateAndTime(); 
    }
    
    public String getEventTypeString(){
        if (eventType == EVT_NEWTESTINSTALLATION)
            return ("Create new Test Installation");
        if (eventType == EVT_DOWNLOADTESTINSTALLATION)
            return ("Download Test Installation");
        if (eventType == EVT_DISSASEMBLETESTINSTALLATION)
            return ("Disassemble Test Installation");
        if (eventType == EVT_NEWBUILDINSTALLATION)
            return ("Create new Build Installation");
        if (eventType == EVT_DOWNLOADBUILDINSTALLATION)
            return ("Download Build Installation");
        if (eventType == EVT_DISSASEMBLEBUILDINSTALLATION)
            return ("Disassemble Build Installation");
        if (eventType == EVT_RELEASEBUILDINSTALLATION)
            return ("Release Build Installation");
        return "Unknown";
    }
    
    public String getObjectName(){
      return this.objectName;
    }
    
    public int getEventType(){
        return this.eventType;
    }
    
    public String getUserName(){
        if (this.user != null)
            return this.user.getName();
        else
            return "Unknown User";
    }
    
    
    public String getTimeStamp(){
        return this.currentTimeStamp;    
    }
    
    private User user = null;
    private int eventType;
    private String objectName;
    private String currentTimeStamp;
    
    
    public static int EVT_NEWTESTINSTALLATION = 0;
    public static int EVT_DOWNLOADTESTINSTALLATION = 1;
    public static int EVT_DISSASEMBLETESTINSTALLATION = 2;
    public static int EVT_NEWBUILDINSTALLATION = 3;
    public static int EVT_DOWNLOADBUILDINSTALLATION = 4;
    public static int EVT_DISSASEMBLEBUILDINSTALLATION = 5;
    public static int EVT_RELEASEBUILDINSTALLATION = 6;
}
