/*
 * BuildInstallationArchiver.java
 *
 * Created on December 18, 2003, 7:39 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/** The BuildInstallationArchiver Class is used to create a single compressed file that
 * contains one or more TestInstallations
 */
public class BuildInstallationArchiver extends ArchiverBase {
    
    /** ArchiveFile - the File that will contain the compressed, combined content
     * of all the files that will be archived by the call to addToArchive()
     */
    public BuildInstallationArchiver(File archiveFile, BuildInstallation buildInstallation){
        super();
        this.compressedArchiveFile = archiveFile;
        this.uncompressedArchiveFile = new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+archiveFile.getName()+".tmp");
        this.buildInstallation = buildInstallation;
    }
    
    /** ArchiveFile - the File that will contain the compressed, combined content
     * of all the files that will be archived by the call to addToArchive()
     */
    public BuildInstallationArchiver(File archiveFile){
        this.buildInstallation = null;
        this.compressedArchiveFile = archiveFile;
        this.uncompressedArchiveFile = new File(FileContainer.getTemporaryFolder()+FileContainer.getOSFileSeperator()+archiveFile.getName()+".tmp");
    }
    
    public void setBuildInstallation(BuildInstallation buildInstallation){
        this.buildInstallation = buildInstallation;
    }
    
    public BuildInstallation getBuildInstallation(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        
        if (buildInstallation != null){
            return buildInstallation;
        }
        try{
            if (compressedArchiveFile.exists()){
                if  (!uncompressedArchiveFile.exists()){
                    uncompressFile(this.compressedArchiveFile, this.uncompressedArchiveFile);
                }
                fis = new FileInputStream(this.uncompressedArchiveFile);
                ois = new ObjectInputStream(fis);
                
                // start by reading the Build Installation
                buildInstallation = (BuildInstallation)ois.readObject();
                ois.close();
                fis.close();
                return buildInstallation;
            }
        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        
        return null;
    }
    
    public void installArchive(File installFolder, BuildInstallationArchiver backupArchiver){
        installArchive(installFolder, backupArchiver, null);
    }
    
    /** This method dissasembles the archived file.  Each file inside the
     * archive will be copied relative to the BASE directory ('installFolder').
     * Where actions are required to be taken (scripts, etc.) it will
     * be triggered within this method.
     * <br> installFolder - Folder that acts as the ROOT for the subsequent files
     * <br> backupArchiver - Used to save all the files that are being replaced during
     * the installation.  That way we can simply install the backup in order to uninstall
     *  the current.
     * <br> progressBar - an optional JProgressBar component to display the installation progress
     */
    public void installArchive(File installFolder, BuildInstallationArchiver backupArchiver, javax.swing.JProgressBar progressBar){
        try{
            // Uncompress the file
            uncompressFile(this.compressedArchiveFile, this.uncompressedArchiveFile);
            FileInputStream fis = new FileInputStream(this.uncompressedArchiveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            QAFile qaFile = null;
            FileOutputStream fos = null;
            
            // start by reading the Build Installation
            buildInstallation = (BuildInstallation)ois.readObject();
            System.out.println("BuildInstallation:"+buildInstallation);
            
            // If this is a normal archive, we need to create a backup archive of all the files we're replacing.
            //   That way, we can easily uninstall by installing the backup archive
            if (backupArchiver != null){
                if (buildInstallation != null){
                    backupArchiver.setBuildInstallation(buildInstallation);
                    backupArchiver.prepareArchive();
                }
            }
            
            if (progressBar != null){
                progressBar.setIndeterminate(true);
            }
            
            while ((qaFile = (QAFile)ois.readObject()) != null){
                
                // Create the required folders
                new File(qaFile.getInstallGroup().getInstallFolder(installFolder).getAbsolutePath()).mkdirs();
                
                File fromFile = qaFile.getFile();
                File toFile = new File(qaFile.getInstallGroup().getInstallFolder(installFolder).getAbsolutePath()+FileContainer.getOSFileSeperator()+fromFile.getName());
                
                if (progressBar != null){
                    progressBar.setString("Installing "+qaFile.getFile().getName()+" ...");
                }
                
                /** Before replacing existing files, we need to create a 'backup' archive that we can use
                 *    at a later stage to restore to.
                 * For scripts, this imply creating a new scripts file, containing the original script in the
                 * database.  That way, we can restore stored-procedures, functions and views back to the
                 * way they were prior to installation.
                 * For EXE's, DLL's etc, we store a copy of the original file in the backup archive.
                 */
                QAFile backupQAFile = qaFile.getInstallGroup().performTasksBefore(qaFile, installFolder);
                
                // Add this to the backup archiver
                if ((backupArchiver != null) && (backupQAFile != null)) {
                    backupArchiver.addToArchive(backupQAFile);
                }
                
                // Now we can copy the archiver files into their required destinations
                moveFile(fromFile, toFile );
                
                // Certain file groups have actions that must take place after the copy.
                // One example is the script files that needs to be executed.
                qaFile.getInstallGroup().performTasksAfter(qaFile);
            }
            ois.close();
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
        catch (EOFException eofe){
            // Delete the uncompressed archive file
            if (backupArchiver != null){
                uncompressedArchiveFile.delete();
                
                // Prepare the Backup Archive
                backupArchiver.buildArchive();
            }
            
            // Reset the ProgressBar
            if (progressBar != null){
                progressBar.setString(null);
                progressBar.setMaximum(0);
                progressBar.setValue(0);
                progressBar.setIndeterminate(false);
            }
            
            // Update the database version
            updateDBVersion(getBuildInstallation().getNewDBVersion());
        }
        
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    /** Prepares the archive for the adding of files that will follow.
     * Remember that one needs to call 'prepareArchive()' BEFORE adding
     * files into the archive.
     * <br>E.g.
     * <br>  Archiver archiver = new Archiver(buildInstallation);
     * <br>  archiver.prepareArchive();
     * <br>  archiver.buildArchive();
     */
    public void prepareArchive(){
        if (buildInstallation == null){
            Exception e = new Exception("Cannot create archives without an attached BuildInstallation");
        }
        try{
            fos = new FileOutputStream(uncompressedArchiveFile);
            oos = new ObjectOutputStream(fos);
            // Add the BuildInstallation
            oos.writeObject(buildInstallation);
            System.out.println("  preparing archive - writeObject(buildInstallation)");
            
            // Step through each TestInstallation contained in this build installation,
            // adding all the relevant files into the archive
            Iterator iter = buildInstallation.getTestInstallations().iterator();
            while (iter.hasNext()){
                TestInstallation testInstallation = (TestInstallation)iter.next();
                FileContainer.extractTestInstallationFiles(testInstallation);
 
                // Check that we 'uncompress' all the files in this testInstallation
                Iterator fileIterator = testInstallation.getQAFiles().iterator();
                while (fileIterator.hasNext()){
                    QAFile qaFile = (QAFile)fileIterator.next();
/*                    System.out.println("**Uncompressing file:"+qaFile.getFile()+
                        "\n\t -> "+ new File(FileContainer.getTemporaryFolder()+
                        FileContainer.getOSFileSeperator()+qaFile.getFile().getName()));
                    
                    uncompressFile(qaFile.getFile(), new File(FileContainer.getTemporaryFolder()+
                        FileContainer.getOSFileSeperator()+qaFile.getFile().getName()));
 */
                    addToArchive(qaFile);
                    
                }
            }
            System.out.println("  archive complete");
            
            
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    /** Prepares the archive for the adding of files that will follow.
     * Remember that one needs to call 'prepareArchive()' BEFORE adding
     * files into the archive.
     * <br>E.g.
     * <br>  Archiver archiver = new Archiver(buildInstallation);
     * <br>  archiver.prepareArchive();
     * <br>  archiver.buildArchive();
     */
    public void buildArchive(){
        if (buildInstallation == null){
            Exception e = new Exception("Cannot create archives without an attached BuildInstallation");
        }
        try{
            oos.close();
            this.compressFile(uncompressedArchiveFile, compressedArchiveFile);
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    /** Adds a file into the archive.
     */
    private void addToArchive(QAFile qaFile){
        if (buildInstallation == null){
            Exception e = new Exception("Cannot create archives without an attached BuildInstallation");
        }
        
        try{
            oos.writeObject(qaFile);
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
    }
    
    private File compressedArchiveFile;
    private File uncompressedArchiveFile;
    private ObjectOutputStream oos;
    private FileOutputStream fos;
    private BuildInstallation buildInstallation = null;
}
