/*
 * Archiver.java
 *
 * Created on November 26, 2003, 11:32 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.io.*;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/** The Archiver Class is used to create a single compressed file that will be
 * distributed to the QA department for testing
 * <br>
 * This file contains QAFiles, which in turns includes a file as well as its distribution rules.
 * Remember to use the 'openArchiver()' before, and 'closeArchiver()' after adding files
 * into the archive
 */
public abstract class ArchiverBase {
    
    protected static void moveFile(File fromFile, File toFile){
        try{
            BufferedOutputStream dest = null;
            FileOutputStream fos = new FileOutputStream(toFile);
            FileInputStream fis = new FileInputStream(fromFile);
            System.out.println(" Moving '"+fromFile.getAbsoluteFile()+"' ");
            System.out.println("         to '"+toFile.getAbsoluteFile()+"' ...");
            dest = new  BufferedOutputStream(fos, BUFFER);
            byte data[] = new byte[BUFFER];
            int count;
            
            while ((count = fis.read(data, 0, BUFFER)) != -1) {
                dest.write(data, 0, count);
            }
            
            dest.flush();
            dest.close();
            fromFile.delete();
            
        }
        catch (FileNotFoundException fe){
            fe.printStackTrace();
        }
        catch (EOFException eof){
        }
        catch (IOException ie){
            ie.printStackTrace();
        }
        
    }
    
    protected static void compressFile(File uncompressedFile, File compressedFile){
        Compressor compressor = new Compressor();
        compressor.compressFile(uncompressedFile, compressedFile);
    }
    
    protected static void uncompressFile(File compressedFile, File uncompressedFile){
        Compressor compressor = new Compressor();
        compressor.uncompressFile(compressedFile, uncompressedFile);
    }
    
    /** Returns the current Database Version
     */
    protected String getCurrentDBVersion(){
        String result = "Unknown";
        DatabaseObject.setConnectInfo_PeopleWareLocalHost();
        Connection con = DatabaseObject.getNewConnection();
        try {
            ResultSet rs = DatabaseObject.openSQL("select first version_nr from s_db_ver order by date_of_version desc", con);
            while (rs.next()){
                result = rs.getString(1).toUpperCase();
            }
            DatabaseObject.releaseConnection(con);
            return result;
        }
        catch( SQLException e ){
            e.printStackTrace();            
            return "Unknown";
        }
    }
    
    /** Checks the connection to the Accsys Peopleware database
     */
    protected boolean isDatabaseRunning(){
        DatabaseObject.setConnectInfo_PeopleWareLocalHost();
        
        Connection con = DatabaseObject.getNewConnection();
        try {
            con.getTransactionIsolation();
            con.close();
            con = null;
            return true;
        }
        catch( SQLException e ){
            System.out.println("** Unable to connect to databse (localhost:2638) **");
            return false;
        }
    }
    
    protected void updateDBVersion(String newVersion){
        String sqlString = "insert into s_db_ver (company_id, date_of_version, version_nr) "+
                           " (select max(company_id), now(*), '"+newVersion.toUpperCase()+"' from c_master);commit;";
        DatabaseObject.setConnectInfo_PeopleWareLocalHost();
        
        Connection con = DatabaseObject.getNewConnection();
        try {
            DatabaseObject.executeSQL(sqlString, con);
        }
        catch( SQLException e ){
            e.printStackTrace();
        }
    }
    
    static final int BUFFER = 2048;
}


