/*
 * UserDatabaseInterface.java
 *
 * Created on December 9, 2003, 2:19 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * Singleton, used to update User from the TimeSheet Database
 * @author  liam
 */
public class UserDatabaseInterface {
    
    /** Creates a new instance of UserDatabaseInterface */
    private static synchronized void createInstance(){
        if (dbInterface == null){
            dbInterface = new UserDatabaseInterface();
        }
    }
    
    private UserDatabaseInterface(){
    }
    
    /** Get single instance of this class. */
    public static UserDatabaseInterface getInstance(){
        if (dbInterface == null)
            UserDatabaseInterface.createInstance();
        return dbInterface;
    }
    
    /** Populate a User's properties from the
     * USR table in the TimeSheet Database
     */
    public boolean refreshFromDatabase(User user){
        try{
            boolean rslt = false;
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "select USR_ID, Name, Category, Login_Name, Login_Pwd, e_mail "+
            "from USR where upper(Login_Name)='"+user.getLoginName()+"'";
            ResultSet rs = databaseObject.openSQL(sqlString, con);
            while (rs.next()){
                user.setUserID(rs.getString("USR_ID"));
                user.setName(rs.getString("Name"));
                user.setCategory(rs.getString("Category"));
                user.setLoginName(rs.getString("Login_Name"));
                user.setLoginPwd(rs.getString("Login_Pwd"));
                user.setEMail(rs.getString("e_mail"));
                rslt = true;
            }
            rs.close();
            databaseObject.releaseConnection(con);
            return rslt;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }
    
    private static UserDatabaseInterface dbInterface;
}
