/*
 * BookingItemDatabaseInterface.java
 *
 * Created on December 9, 2003, 2:19 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import za.co.ucs.lwt.db.*;
import java.sql.*;
import java.util.*;

/**
 * Singleton, used to update BookingItem from the TimeSheet Database
 * @author  liam
 */
public class BookingItemDatabaseInterface {
    
    /** Creates a new instance of ChangeRequestDatabaseInterface */
    private static synchronized void createInstance(){
        if (dbInterface == null){
            dbInterface = new BookingItemDatabaseInterface();
        }
    }
    
    private BookingItemDatabaseInterface(){
    }
    
    /** Get single instance of this class. */
    public static BookingItemDatabaseInterface getInstance(){
        if (dbInterface == null)
            BookingItemDatabaseInterface.createInstance();
        return dbInterface;
    }
    
    /** Adds a new record in the BookingItem table
     */
    public void insertIntoDatabase(BookingItem bookingItem){
        try{
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "insert into BookingItem (USR_ID, CRNumber, FileNames) values "+
            "("+bookingItem.getUser().getUserID()+","+
            bookingItem.getChangeRequest().getCRNumber()+",'"+
            bookingItem.getRequiredFileNamesAsString().toUpperCase()+"')";
            System.out.println("SQL:"+sqlString);
            databaseObject.executeSQL(sqlString, con);
            databaseObject.releaseConnection(con);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /** Updates an existing record in the BookingItem table
     */
    public void updateDatabase(BookingItem bookingItem){
        try{
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "update BookingItem set USR_ID="+bookingItem.getUser().getUserID()+
                                ", FileNames='"+bookingItem.getRequiredFileNamesAsString().toUpperCase()+"' "+
                                " where CRNumber="+bookingItem.getChangeRequest().getCRNumber();
            System.out.println("SQL:"+sqlString);
            databaseObject.executeSQL(sqlString, con);
            databaseObject.releaseConnection(con);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /** Updates an existing record in the BookingItem table
     */
    public void deleteFromDatabase(BookingItem bookingItem){
        try{
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "delete from BookingItem "+
                                " where CRNumber="+bookingItem.getChangeRequest().getCRNumber();
            databaseObject.executeSQL(sqlString, con);
            databaseObject.releaseConnection(con);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }    
    
    
    
    
    /** Populate a Change Request's properties from the
     * CR table in the TimeSheet Database
     */
    public boolean refreshFromDatabase(BookingItem bookingItem){
        try{
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "select bi.CRNumber, bi.FileNames, usr.login_name "+
            " from BookingItem bi, usr where usr.USR_ID=bi.USR_ID and "+
            " upper(bi.CRNumber)='"+bookingItem.getChangeRequest().getCRNumber().toUpperCase()+"'";
            ResultSet rs = databaseObject.openSQL(sqlString, con);
            while (rs.next()){
                String loginName = rs.getString("login_name");
                if (loginName.length()>0){
                    // Set the User
                    bookingItem.setUser(new User(loginName));
                    
                    // Create a linked list and add all the file names into it
                    LinkedList requiredFileNames = new LinkedList();
                    String fileNames = rs.getString("FileNames");
                    StringTokenizer token = new StringTokenizer(fileNames, ",;\n");
                    while (token.hasMoreElements()){
                        requiredFileNames.add(token.nextElement());
                    }
                    bookingItem.setRequiredFileNames(requiredFileNames);
                    rs.close();
                    databaseObject.releaseConnection(con);
                    return true;
                }
                else {
                    rs.close();
                    databaseObject.releaseConnection(con);
                    return false;
                }
            }
            return false;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }
    
    private static BookingItemDatabaseInterface dbInterface;
}
