/*
 * FileContainer.java
 *
 * Created on December 3, 2003, 8:40 AM
 */
package za.co.ucs.lwt.deploymentbuilder;

import java.io.*;
import java.sql.*;
import java.util.*;
import za.co.ucs.lwt.db.*;
import za.co.ucs.lwt.initools.*;

/**
 * Singleton. This will act as a Factory Class that manages the availability of QAFiles for TestInstallations
 * and BuildInstallations.
 *
 * @author liam
 */
public class FileContainer implements java.io.Serializable {
  static final long serialVersionUID = -4625856855392886360L;
    /**
     * Creates a new instance of FileContainer. fileContainerFile - the File that is used for saving/loading
     * existing testInstallations/QAFiles
     */
    private FileContainer(File fileContainerFile) {
        if (usedQAFiles == null) {
            usedQAFiles = new LinkedList();
        }
        if (buildInstallations == null) {
            buildInstallations = new LinkedList();
        }

        // Setup OS dependent folders
        System.out.println("Creating/Defining working folders for");
        System.out.println("\tos.name:" + System.getProperty("os.name"));
        System.out.println("\tos.arch:" + System.getProperty("os.arch"));
        System.out.println("\tos.version:" + System.getProperty("os.version"));

        // Make sure all the default folders exists
        System.out.println("getContainerFile():" + getContainerFile().getAbsolutePath());
        System.out.println("getTemporaryFolder():" + getTemporaryFolder().getAbsolutePath());
        System.out.println("getArchiveFolder():" + getArchiveFolder().getAbsolutePath());
        getContainerFile().mkdirs();
        getTemporaryFolder().mkdirs();
        getArchiveFolder().mkdirs();

        FileContainer.fileContainerFile = fileContainerFile;
        load();
    }

    // To speed-up the generation of certain HTML pages, it helps to
    // store a list of currently loaded ChangeRequestNumbers statically.
    @SuppressWarnings("ConvertToTryWithResources")
    private void buildChangeRequestNumbersList() {
        if (changeRequestNumbers == null) {
            changeRequestNumbers = new LinkedList();
        }
        DatabaseObject databaseObject = DatabaseObject.getInstance();
        Connection con = DatabaseObject.getNewConnectionFromPool();
        String sqlString = "select CRNumber from cr order by CRNUMBER";
        try {
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                changeRequestNumbers.add(crNumber);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseObject.releaseConnection(con);
    }

    public void reloadChangeRequestNumbers() {
        if (changeRequestNumbers == null) {
            changeRequestNumbers = new LinkedList();
        }
        changeRequestNumbers.clear();
        buildChangeRequestNumbersList();
    }

    public static FileContainer getInstance(File fileContainerFile) {
        if (fileContainer == null) {
            FileContainer.createInstance(fileContainerFile);
        }
        return fileContainer;
    }

    private static synchronized void createInstance(File fileContainerFile) {
        if (fileContainer == null) {
            fileContainer = new FileContainer(fileContainerFile);
        }
    }

    /**
     * isFileAvailable() returns true if this specific QAFile can be used in a TestInstallation. I.e. no other
     * TestInstallation is currently using that file
     *
     * @param file
     * @return
     */
    public boolean isFileAvailable(QAFile file) {
        return (getTestInstallation(file) == null);
    }

    /**
     * isFileNameInUse() returns true if this specific QAFile already exists in a TestInstallation.
     *
     * @param fileName
     * @return
     */
    public boolean isFileNameInUse(String fileName) {
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getQAFile().getFile().getName().equalsIgnoreCase(fileName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * getCRContainingFile() returns the CR that currently uses this file name.
     *
     * @param fileName
     * @return
     */
    public String getCRContainingFile(String fileName) {
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getQAFile().getFile().getName().equalsIgnoreCase(fileName)) {
                return ((TestInstallationMapping) usedQAFile).getTestInstallation().getChangeRequest().getCRNumber();
            }
        }
        return "";
    }

    /**
     * canLinkFileTo(QAFile file, ChangeRequest changeRequest, User notifyUser) returns true if this specific
     * QAFile can be linked to changeRequest I.e. no other TestInstallation is currently using that file. If
     * this file is not available, a notification will be send to the User
     *
     * @param file
     * @param changeRequest
     * @param notifyUser
     * @return
     */
    public boolean canLinkFileTo(QAFile file, ChangeRequest changeRequest, User notifyUser) {
        boolean result = isFileAvailable(file);
        if (!result) {
            String msg = (notifyUser.getName()
                    + "<br><br>You tried to attach '" + file.getFile().getName() + "' to a Test Installation (" + changeRequest.getCRNumber() + ").  This file is already in use by Change Request:" + this.getTestInstallation(file).getChangeRequest().getCRNumber());

            System.out.println("EMail to Development:" + notifyUser.getName() + "\n" + msg);

            E_Mail.getInstance().send(notifyUser.getEMail(), "", "WARNING: Deployment Agent - Conflicting File Usage", msg);
        }
        return result;
    }

    /**
     * isFileUsed() returns true if this specific QAFile is already part of a TestInstallation.
     *
     * @param file
     * @return
     */
    public boolean isFileUsed(QAFile file) {
        return (getTestInstallation(file) != null);
    }

    /**
     * Maps this file to the given TestInstallation. After registering a file, the isFileAvailable() will
     * return false.
     *
     * @param file
     * @param testInstallation
     * @return
     */
    public boolean registerQAFile(QAFile file, TestInstallation testInstallation) {
        if (isFileAvailable(file)) {
            System.out.println("Register QAFile:" + file.getFile().getName());
            usedQAFiles.add(new TestInstallationMapping(file, testInstallation));
            testInstallation.setStatus(TestInstallation.MODIFIED);
            testInstallation.addQAFile(file);
            return true;
        } else {
            return false;
        }
    }

    public void addBuildInstallation(BuildInstallation buildInstallation) {
        if (!buildInstallations.contains(buildInstallation)) {
            buildInstallations.add(buildInstallation);
        }
    }

    public void removeBuildInstallation(BuildInstallation buildInstallation) {
        buildInstallations.remove(buildInstallation);
    }

    /**
     * Returns the TestInstallation that uses this file
     *
     * @param file
     * @return
     */
    public TestInstallation getTestInstallation(QAFile file) {
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getQAFile().equals(file)) {
                return ((TestInstallationMapping) usedQAFile).getTestInstallation();
            }
        }
        return null;
    }

    /**
     * Returns the TestInstallation that uses this file
     */
    /**
     * Returns the TestInstallation that incorporates the given ChangeRequest
     *
     * @param changeRequestNumber
     * @return
     */
    public TestInstallation getTestInstallation(String changeRequestNumber) {
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getTestInstallation().getChangeRequest().getCRNumber().compareToIgnoreCase(changeRequestNumber) == 0) {
                return ((TestInstallationMapping) usedQAFile).getTestInstallation();
            }
        }
        return null;
    }

    /**
     * Locates the Test Installation that is currently using the file
     *
     * @param fileName
     * @return
     */
    public TestInstallation getTestInstallationUsingFile(String fileName) {
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getQAFile().getFile().getName().trim().compareToIgnoreCase(fileName.trim()) == 0) {
                return ((TestInstallationMapping) usedQAFile).getTestInstallation();
            }
        }
        return null;
    }

    /**
     * Locates the Test Installation(s) that is currently using files that resembles the search string
     *
     * @param partialFileName
     * @return
     */
    public String findFilesInTestInstallationsInHTML(String partialFileName) {
        StringBuilder result = new StringBuilder();
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getQAFile().getFile().getName().toUpperCase().trim().contains(partialFileName.toUpperCase().trim())) {
                QAFile qaFile = ((TestInstallationMapping) usedQAFile).getQAFile();
                TestInstallation testInstallation = ((TestInstallationMapping) usedQAFile).getTestInstallation();
                result.append(qaFile.getFile().getName()).append(" in <b><i> [").append(testInstallation.getName()).append("]</i></b><br>");
            }
        }
        return result.toString();
    }

    /**
     * Returns possible Change Request numbers listed within this Change Request's synopsis. The result is a
     * HTML-formatted representation of the synopsis, with hyperlinks on the Change Requests. This wil enable
     * the user to jump from one change request to the others contained in that one.
     *
     * @param synopsis
     * @return
     */
    public String formatChangeRequestSynopsisInHTML(String synopsis) {
        Integer crnumber;
        StringBuilder rslt = new StringBuilder();
        StringTokenizer token = new StringTokenizer(synopsis, " :.,\t\n\r\f", true);
        // Step through each part of the synopsis, looking for valid CR numbers
        while (token.hasMoreElements()) {
            String element = token.nextToken();
            String oldElement = element;
            // Does this token start with the prefix CR?
            if (element.toUpperCase().startsWith("CR")) {
                // Removes the "CR" prefix
                element = element.substring(2);
            }

            // Try to convert the string to an integer
            try {
                crnumber = new Integer(element);
            } catch (java.lang.NumberFormatException e) {
                // Puts back the "CR" prefix
                rslt.append(oldElement);
                continue;
            }

            // Is this a valid CR Number
            if (changeRequestNumbers == null) {
                reloadChangeRequestNumbers();
            }
            if (changeRequestNumbers.contains(crnumber.toString())) {
                rslt.append(" <a href=\"ChangeRequestInfo.jsp?crnumber=").append(crnumber.toString()).append("\" title=\"See also\">").append(crnumber.toString()).append("</a>");
            } else {
                rslt.append(crnumber.toString());
            }
        }

        return rslt.toString();
    }

    /**
     * Returns the buildInstallations that contains this TestInstallation
     *
     * @param testInstallation
     * @return
     */
    public LinkedList getBuildInstallations(TestInstallation testInstallation) {
        LinkedList localBuildInstallations = new LinkedList();
        for (Object buildInstallation : FileContainer.buildInstallations) {
            if (((BuildInstallation) buildInstallation).getTestInstallations().contains(testInstallation)) {
                localBuildInstallations.add(buildInstallation);
            }
        }
        return localBuildInstallations;
    }

    /**
     * Returns all the buildInstallations
     *
     * @return
     */
    public LinkedList getBuildInstallations() {
        return buildInstallations;
    }

    /**
     * Returns all the buildInstallations
     *
     * @param buildName
     * @return
     */
    public BuildInstallation getBuildInstallation(String buildName) {
        for (Object buildInstallation : FileContainer.buildInstallations) {
            if (((BuildInstallation) buildInstallation).getBuildName().compareToIgnoreCase(buildName) == 0) {
                return (BuildInstallation) buildInstallation;
            }
        }
        return null;
    }

    /**
     * Removes this file from the internal treeMap and makes it available for other TestInstallations
     *
     * @param file
     */
    public void unregisterQAFile(QAFile file) {
        // try to delete the file from the hard disk
        if (file.getFile().exists()) {
            System.out.print("Deleting existing file on server (" + file.getFile().getName() + ")...");
            file.getFile().delete();
            System.out.println(" done");
        }

        usedQAFiles.remove(file);
    }

    /**
     * By dismantling the TestInstallation, all the QAFiles assigned to this class gets released from the
     * FileContainer. This in turn, will allow other TestInstallations to make use of these files Note: One
     * cannot dissasemble a TestInstallation if that test installation is part of a BuildInstallation
     *
     * @param testInstallation
     * @param user
     * @param sendNotification
     */
    public void dismantleTestInstallation(TestInstallation testInstallation, User user, boolean sendNotification) {
        if (getBuildInstallations(testInstallation).size() > 0) {
            System.out.println("Cannot disassemble a TestInstallation that is part of a BuildInstallation.");
            return;
        }

        System.out.println("Disassemble Testinstallation:" + testInstallation.getChangeRequest().getCRNumber());
        LinkedList associatedFiles = this.getQAFiles(testInstallation);

        if (sendNotification) {
            // Notify QA that a build was dissasembled
            LinkedList qaUsers = this.getUsers("Development");
            String fileNames = getFileNames(testInstallation, "<br>");

            for (Object qaUser : qaUsers) {
                StringBuilder msgDev = new StringBuilder(((User) qaUser).getName());
                msgDev.append("<br>The following Test Installation was disassembled by ").append(user.getName());
                msgDev.append("<br>Change Request: <a href=\"").append(getHTTPHost()).append("process/ChangeRequestInfo.jsp?loginName=").append(((User) qaUser).getLoginName()).append("&crnumber=").append(testInstallation.getChangeRequest().getCRNumber()).append("\" target=\"main\">");
                msgDev.append(testInstallation.getChangeRequest().getCRNumber()).append("</a>");
                msgDev.append("<br><br>Please ensure that the following files are unlocked in StarTeam:").append(fileNames);
                msgDev.append("<br><br>Synopsis:<br>").append(testInstallation.getChangeRequest().getSynopsis());
                System.out.println("EMail to Development:" + msgDev.toString());
                System.out.println("Notifying :" + ((User) qaUser).getEMail() + "," + "\n Deployment Agent - DISASEMBLE [CR:" + testInstallation.getChangeRequest().getCRNumber() + "]" + "," + msgDev.toString());
                // Notify users
                String subj = "Deployment Agent - DISASEMBLE [CR:" + testInstallation.getChangeRequest().getCRNumber() + "]";
                String msg = msgDev.toString();
                E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
            }

            // Notify Development that a build was dissasembled
            qaUsers = this.getUsers("QA");
            for (Object qaUser : qaUsers) {
                StringBuilder msgQA = new StringBuilder(((User) qaUser).getName());
                msgQA.append("<br><br>The following Test Installation was disassembled by ").append(user.getName());
                msgQA.append("<br>Change Request: <a href=\"").append(getHTTPHost()).append("process/ChangeRequestInfo.jsp?loginName=").append(((User) qaUser).getLoginName()).append("&crnumber=").append(testInstallation.getChangeRequest().getCRNumber()).append("\" target=\"main\">");
                msgQA.append(testInstallation.getChangeRequest().getCRNumber()).append("</a>");
                msgQA.append("<br><br>Synopsis:<br>").append(testInstallation.getChangeRequest().getSynopsis());
                System.out.println("EMail to QA:" + msgQA.toString());
                // Notify users
                String subj = "Deployment Agent - DISASEMBLE [CR:" + testInstallation.getChangeRequest().getCRNumber() + "]";
                String msg = msgQA.toString();
                E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
            }
        }

        // Notify developers who had Reminders set for the files in question
        System.out.println("Getting booking Items...");
        LinkedList bookingItems = this.getBookingItemsWaitingFor(testInstallation);
        for (Object bookingItem1 : bookingItems) {
            BookingItem bookingItem = (BookingItem) bookingItem1;
            System.out.println("Notifying " + bookingItem.getUser().getName());
            String subj = "Deployment Agent Reminder";
            String msg = bookingItem.getUser().getName()
                    + "<br><br>You have set a reminder for the following Change Request:" + bookingItem.getChangeRequest().getCRNumber()
                    + "<br>Change Request: <a href=\"" + getHTTPHost() + "process/ChangeRequestInfo.jsp?loginName=" + bookingItem.getUser().getLoginName()
                    + "&crnumber=" + bookingItem.getChangeRequest().getCRNumber() + "\" target=\"main\">" + bookingItem.getChangeRequest().getCRNumber() + "</a>"
                    + "<br><br>A TestInstallation (" + testInstallation.getName() + ") included one or more of the files contained in your reminder."
                    + "<br>These files have become available."
                    + "<br><br>Synopsis:<br>" + testInstallation.getChangeRequest().getSynopsis();
            E_Mail.getInstance().send(bookingItem.getUser().getEMail(), "", subj, msg);
        }
        System.out.println("Getting booking Items... Done");

        // Unregister the files
        Iterator iter = associatedFiles.iterator();
        while (iter.hasNext()) {
            unregisterQAFile((QAFile) iter.next());
        }

        LinkedList mappings = new LinkedList();
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getTestInstallation() == testInstallation) {
                mappings.add(usedQAFile);
            }
        }

        for (Object mapping : mappings) {
            usedQAFiles.remove(mapping);
        }
        testInstallation = null;
    }

    /**
     * Returns the files included in this TestInstallation
     *
     * @param testInstallation
     * @param newLineCharacter
     * @return
     */
    public String getFileNames(TestInstallation testInstallation, String newLineCharacter) {
        LinkedList associatedFiles = this.getQAFiles(testInstallation);
        String fileNames = "";
        for (Object associatedFile : associatedFiles) {
            fileNames = fileNames + newLineCharacter + ((QAFile) associatedFile).getOriginalFilePath();
        }
        return fileNames;
    }

    /**
     * Returns the files included in this BuildInstallation
     *
     * @param buildInstallation
     * @param newLineCharacter
     * @return
     */
    public String getFileNames(BuildInstallation buildInstallation, String newLineCharacter) {
        // String affected TestInstallations
        String fileNames = "";
        TreeMap fileNameMap = new TreeMap();

        for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
            TestInstallation testInstallation = (TestInstallation) buildInstallation.getTestInstallations().get(i);
            for (int j = 0; j < testInstallation.getQAFiles().size(); j++) {
                QAFile qaFile = (QAFile) testInstallation.getQAFiles().get(j);
                String fileDetail = qaFile.getOriginalFilePath() + FileContainer.getOSFileSeperator() + qaFile.getFile().getName();
                fileNameMap.put(fileDetail, fileDetail);
            }
        }
        Iterator iter = fileNameMap.keySet().iterator();
        while (iter.hasNext()) {
            String iterVal = (String) iter.next();
            fileNames = fileNames + iterVal + newLineCharacter;
        }
        return fileNames;
    }

    /**
     * Returns the files included in this BuildInstallation that are NOT part of another BuildInstallation
     *
     * @param buildInstallation
     * @param newLineCharacter
     * @return
     */
    public String getSingleInstanceFileNames(BuildInstallation buildInstallation, String newLineCharacter) {
        // String affected TestInstallations
        String fileNames = "";
        TreeMap fileNameMap = new TreeMap();

        for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
            TestInstallation testInstallation = (TestInstallation) buildInstallation.getTestInstallations().get(i);
            if (getBuildInstallations(testInstallation).size() == 1) {
                for (int j = 0; j < testInstallation.getQAFiles().size(); j++) {
                    QAFile qaFile = (QAFile) testInstallation.getQAFiles().get(j);
                    String fileDetail = qaFile.getOriginalFilePath() + FileContainer.getOSFileSeperator() + qaFile.getFile().getName();
                    fileNameMap.put(fileDetail, fileDetail);
                }
            }
        }
        Iterator iter = fileNameMap.keySet().iterator();
        while (iter.hasNext()) {
            String iterVal = (String) iter.next();
            fileNames = fileNames + iterVal + newLineCharacter;
        }
        return fileNames;
    }

    /**
     * Returns the files included in this BuildInstallation
     *
     * @param buildInstallation
     * @param newLineCharacter
     * @return
     */
    public String getTestInstallationNames(BuildInstallation buildInstallation, String newLineCharacter) {
        String testInstallations = "";

        for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
            TestInstallation testInstallation = (TestInstallation) buildInstallation.getTestInstallations().get(i);
            testInstallations = testInstallations + testInstallation.getName() + newLineCharacter;
        }
        return testInstallations;
    }

    /**
     * Returns the files included in this BuildInstallation that are NOT part of another BuildInstallation
     *
     * @param buildInstallation
     * @param newLineCharacter
     * @return
     */
    public String getSingleInstanceTestInstallationNames(BuildInstallation buildInstallation, String newLineCharacter) {
        String testInstallations = "";

        for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
            TestInstallation testInstallation = (TestInstallation) buildInstallation.getTestInstallations().get(i);
            if (getBuildInstallations(testInstallation).size() == 1) {
                testInstallations = testInstallations + testInstallation.getName() + newLineCharacter;
            }
        }
        return testInstallations;
    }

    /**
     * By dismantling the BuildInstallation, all the TestInstallations assigned to this BuildInstallation gets
     * released.
     *
     * @param buildInstallation
     * @param user
     */
    public void dismantleBuildInstallation(BuildInstallation buildInstallation, User user) {

        // Notify Management that a build was dissasembled
        LinkedList users = this.getUsers("Management");

        for (Object manUser : users) {
            String subj = "Deployment Agent - DISASEMBLE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = ((User) manUser).getName() + "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription() + "] was disassembled by " + user.getName();
            E_Mail.getInstance().send(((User) manUser).getEMail(), "", subj, msg);
        }

        // Notify QA that a build was dissasembled
        users = this.getUsers("QA");
        for (Object qaUser : users) {
            String subj = "Deployment Agent - DISASEMBLE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = ((User) qaUser).getName() + "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription() + "] was disassembled by " + user.getName();
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
        }

        // Notify Development that a build was dissasembled
        users = this.getUsers("Development");
        for (Object devUser : users) {
            String subj = "Deployment Agent - DISASEMBLE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = ((User) devUser).getName() + "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription() + "] was disassembled by " + user.getName();
            E_Mail.getInstance().send(((User) devUser).getEMail(), "", subj, msg);
        }

        // Unregister the TestInstallations
        getBuildInstallation(buildInstallation.getBuildName()).clear();
        buildInstallations.remove(buildInstallation);
        buildInstallation = null;
    }

    /**
     * By releasing the BuildInstallation, all the TestInstallations assigned to this BuildInstallation gets
     * released. Furhter more, all the TestInstallations get disassembled.
     * <br> One uses 'releaseBuildInstallation' to make files available for new builds, etc.
     *
     * @param buildInstallation
     * @param user
     */
    public void releaseBuildInstallation(BuildInstallation buildInstallation, User user) {

        // Notify Management that a build was released
        LinkedList qaUsers = this.getUsers("Management");

        for (Object qaUser : qaUsers) {

            String subj = "Deployment Agent - RELEASE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = ((User) qaUser).getName() + "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription() + "] was released by " + user.getName() + "<br>The following TestInstallations were disassembled:<br>" + getSingleInstanceTestInstallationNames(buildInstallation, "<br>");
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
        }

        // Notify QA that a build was released
        qaUsers = this.getUsers("QA");
        for (Object qaUser : qaUsers) {

            String subj = "Deployment Agent - RELEASE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription()
                    + "]  was released by " + user.getName()
                    + "<br>The following TestInstallations were disassembled:<br>" + getSingleInstanceTestInstallationNames(buildInstallation, "<br>");
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
        }

        // Notify Development that a build was released
        qaUsers = this.getUsers("Development");
        for (Object qaUser : qaUsers) {

            String subj = "Deployment Agent - RELEASE [Build:" + buildInstallation.getBuildName() + "]";
            String msg = "<br><br>A Build (" + buildInstallation.getBuildName() + ") [" + buildInstallation.getBuildDescription()
                    + "]  was released by " + user.getName()
                    + "<br>The following TestInstallations were disassembled:<br>" + getSingleInstanceTestInstallationNames(buildInstallation, "<br>")
                    + "<br><br>PLEASE ENSURE THAT THE APPROPRIATE FILES ARE UNLOCKED IN STARTEAM:<br>"
                    + getSingleInstanceFileNames(buildInstallation, "<br>");
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msg);
        }

        LinkedList crNumbers = new LinkedList();
        for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
            crNumbers.add(((TestInstallation) buildInstallation.getTestInstallations().get(i)).getChangeRequest().getCRNumber());
        }

        // Unregister the TestInstallations
        getBuildInstallation(buildInstallation.getBuildName()).clear();
        buildInstallations.remove(buildInstallation);
        buildInstallation = null;

        for (Object crNumber : crNumbers) {
            TestInstallation testInstallation = getTestInstallation((String) crNumber);
            if (getBuildInstallations(testInstallation).size() == 0) {
                dismantleTestInstallation(testInstallation, user, false);
            }
        }

    }

    /**
     * This method adds all the files associated with this TestInstallation into a single compressed file that
     * has the ability to install itself. The resulting file will be in the form of a TestInstallationArchiver
     * Object and can as such be instantiated in the following manner:
     * <br>
     * Archiver archiver = new Archiver(new File("...") )
     *
     * @param testInstallation
     * @param installPath
     * @param user
     * @return
     */
    public File buildTestInstallation(TestInstallation testInstallation, File installPath, User user) {

        // Make sure the path exist
        installPath.mkdirs();
        // Create the archive file
        File file = new File(installPath.getAbsolutePath() + FileContainer.getOSFileSeperator() + testInstallation.getName() + TestInstallationExtension);
        // Create the archiver
        TestInstallationArchiver archiver = new TestInstallationArchiver(file, testInstallation.getChangeRequest());
        // Prepare Archiver to create an archive file
        archiver.prepareArchive();
        // Add all the files to the archive
        LinkedList associatedFiles = this.getQAFiles(testInstallation);
        Iterator iter = associatedFiles.iterator();
        while (iter.hasNext()) {
            QAFile qaFile = (QAFile) iter.next();
            System.out.println(" Archiving " + qaFile + " into " + testInstallation.getName());
            archiver.addToArchive(qaFile);
        }
        // Create the actual file
        archiver.buildArchive();
        testInstallation.setStatus(TestInstallation.BUILT);

        // Notify QA that a build was created
        LinkedList qaUsers = this.getUsers("QA");
        for (Object qaUser : qaUsers) {
            StringBuilder msgQA = new StringBuilder(((User) qaUser).getName());
            msgQA.append("A new Test Installation was generated by ").append(user.getName());
            msgQA.append("<br>Change Request: <a href=\"").append(getHTTPHost()).append("process/ChangeRequestInfo.jsp?loginName=").append(((User) qaUser).getLoginName()).append("&crnumber=").append(testInstallation.getChangeRequest().getCRNumber()).append("\" target=\"main\">");
            msgQA.append(testInstallation.getChangeRequest().getCRNumber()).append("</a>");
            msgQA.append("<br>See also: <a href=\"").append(getHTTPHost()).append("process/QA.jsp?loginName=").append(user.getLoginName()).append("\" target=\"main\">QA Builds</a>");
            msgQA.append("<br><br>Synopsis:<br>").append(testInstallation.getChangeRequest().getSynopsis());

            String subj = "Deployment Agent - NEW TESTINSTALLATION [CR:" + testInstallation.getChangeRequest().getCRNumber() + "]";
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, msgQA.toString());

            System.out.println("EMail to QA:" + msgQA.toString());
        }

        // Send message to the user who created the Change Request
        String fileNames = "";
        LinkedList affectedFiles = this.getQAFiles(testInstallation);
        for (Object affectedFile : affectedFiles) {
            fileNames = fileNames + "<br>" + ((QAFile) affectedFile).getOriginalFilePath();
        }
        StringBuilder msg = new StringBuilder(user.getName());
        msg.append("<br><br>You created a new Test Installation for ");
        msg.append("<br>Change Request: <a href=\"").append(getHTTPHost()).append("process/ChangeRequestInfo.jsp?loginName=").append(user.getLoginName()).append("&crnumber=").append(testInstallation.getChangeRequest().getCRNumber()).append("\" target=\"main\">");
        msg.append(testInstallation.getChangeRequest().getCRNumber()).append("</a>");
        msg.append("<br>See also: <a href=\"").append(getHTTPHost()).append("process/QA.jsp?loginName=").append(user.getLoginName()).append("\" target=\"main\">QA Builds</a>");
        msg.append("<br><br>Please ensure that the following files are locked in StarTeam:").append(fileNames);
        msg.append("<br><br>Synopsis:<br>").append(testInstallation.getChangeRequest().getSynopsis());
        System.out.println("EMail to Developer:" + msg.toString());

        String subj = "Deployment Agent Notification - NEW TESTINSTALLATION [CR:" + testInstallation.getChangeRequest().getCRNumber() + "]";
        E_Mail.getInstance().send(user.getEMail(), "", subj, msg.toString());

        // Return file
        return file;
    }

    /**
     * This method adds all the files associated with this BuildInstallation into a single compressed file
     * that has the ability to install itself. The resulting file will be in the form of a
     * BuildInstallationArchiver Object and can as such be instantiated in the following manner:
     * <br>
     * Archiver archiver = new Archiver(new File("...") )
     *
     * @param buildInstallation
     * @param installPath
     * @param user
     * @return
     */
    public File buildBuildInstallation(BuildInstallation buildInstallation, File installPath, User user) {

        // Make sure the path exist
        installPath.mkdirs();
        // Create the archive file
        File file = new File(installPath.getAbsolutePath() + FileContainer.getOSFileSeperator() + buildInstallation.getBuildName() + BuildInstallationExtension);
        // Create the archiver
        BuildInstallationArchiver archiver = new BuildInstallationArchiver(file, buildInstallation);
        // Prepare Archiver to create an archive file
        archiver.prepareArchive();
        // Create the actual file
        archiver.buildArchive();

        // Notify QA that a build was created
        LinkedList qaUsers = this.getUsers("QA");
        LinkedList managers = this.getUsers("Management");
        LinkedList developers = this.getUsers("Development");

        // Add all the users together
        qaUsers.addAll(managers);
        qaUsers.addAll(developers);

        for (Object qaUser : qaUsers) {
            StringBuilder message = new StringBuilder();
            // Salutation
            message.append(((User) qaUser).getName());
            // Body of message
            message.append("<br>A new Build (").append(buildInstallation.getBuildName()).append(") [").append(buildInstallation.getBuildDescription()).append("]  was generated by ").append(user.getName());
            // List of CR's included in this build
            message.append("<br>The following Change Requests were included:");
            message.append("<table><tr>");
            for (int i = 0; i < buildInstallation.getTestInstallations().size(); i++) {
                TestInstallation testInstallation = (TestInstallation) (buildInstallation.getTestInstallations().get(i));
                message.append("<td>").append(testInstallation.getChangeRequest().getCRNumber()).append("</td>");
            }
            message.append("</tr></table>");

            String subj = "Deployment Agent - NEW BUILD [Build:" + buildInstallation.getBuildName() + "]";
            E_Mail.getInstance().send(((User) qaUser).getEMail(), "", subj, message.toString());

        }

        // Return file
        return file;
    }

    /**
     * Returns all the QAFiles associated with this installation
     *
     * @param testInstallation
     * @return
     */
    public LinkedList getQAFiles(TestInstallation testInstallation) {
        LinkedList result = new LinkedList();
        for (Object usedQAFile : usedQAFiles) {
            if (((TestInstallationMapping) usedQAFile).getTestInstallation() == testInstallation) {
                result.add(((TestInstallationMapping) usedQAFile).getQAFile());
            }
        }
        return result;
    }

    /**
     * Returns all the users within the TimeSheet Database that belongs to this Category
     */
    private LinkedList getUsers(String category) {
        LinkedList users = new LinkedList();
        DatabaseObject databaseObject = DatabaseObject.getInstance();
        if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
            DatabaseObject.setConnectInfo_TimeSheetODBC();
        } else {
            DatabaseObject.setConnectInfo_TimeSheetJDBC();
        }
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select login_name from USR where login_name is not null order by NAME", con);
            while (rs.next()) {
                String loginName = rs.getString(1);
                if (loginName != null) {
                    User user = new User(loginName);
                    if ((user != null) && (user.getCategory() != null)) {
                        if (user.getCategory().compareToIgnoreCase(category) == 0) {
                            users.add(user);
                        }
                    }
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);

            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns all the bookingItems for this change request
     *
     * @param changeRequest
     * @return
     */
    public LinkedList getBookingItemsFor(ChangeRequest changeRequest) {
        LinkedList bookingItems = getExistingBookingItems();
        LinkedList result = new LinkedList();
        for (Object bookingItem1 : bookingItems) {
            BookingItem bookingItem = (BookingItem) bookingItem1;
            if (bookingItem.getChangeRequest().getCRNumber().compareToIgnoreCase(changeRequest.getCRNumber()) == 0) {
                result.add(bookingItem);
            }
        }
        return result;
    }

    /**
     * Returns all the bookingItems that had files booked that are part of this TestInstallation
     *
     * @param testInstallation
     * @return
     */
    public LinkedList getBookingItemsWaitingFor(TestInstallation testInstallation) {
        if (testInstallation == null) {
            return null;
        }
        LinkedList bookingItems = getExistingBookingItems();
        LinkedList result = new LinkedList();
        for (Object bookingItem1 : bookingItems) {
            BookingItem bookingItem = (BookingItem) bookingItem1;
            LinkedList requiredFiles = bookingItem.getRequiredFileNames();
            for (Object requiredFile1 : requiredFiles) {
                String requiredFile = (String) requiredFile1;
                TestInstallation containedIn = getTestInstallationUsingFile(requiredFile);
                if (containedIn != null) {
                    if (containedIn.getChangeRequest().getCRNumber().compareToIgnoreCase(testInstallation.getChangeRequest().getCRNumber()) == 0) {
                        if (!result.contains(bookingItem)) {
                            result.add(bookingItem);
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the QAFiles of a certain type, associated with this installation.
     *
     * @param testInstallation
     * @param installGroupType
     * @return
     */
    public LinkedList getQAFiles(TestInstallation testInstallation, int installGroupType) {
        LinkedList result = new LinkedList();
        for (Object usedQAFile : usedQAFiles) {
            if ((((TestInstallationMapping) usedQAFile).getTestInstallation() == testInstallation) && ((((TestInstallationMapping) usedQAFile).getQAFile().getInstallGroup().getGroupType() == installGroupType))) {
                result.add(((TestInstallationMapping) usedQAFile).getQAFile());
            }
        }
        return result;
    }

    /**
     * Returns all the TestInstallations
     *
     * @return
     */
    public LinkedList getTestInstallations() {
        LinkedList result = new LinkedList();
        for (Object usedQAFile : usedQAFiles) {
            if (!result.contains(((TestInstallationMapping) usedQAFile).getTestInstallation())) {
                result.add(((TestInstallationMapping) usedQAFile).getTestInstallation());
            }
        }
        return result;
    }

    /**
     * Returns the TestInstallations that are of a specific status
     *
     * @param StarTeamStatus
     * @return
     */
    public LinkedList getTestInstallations(String StarTeamStatus) {
        LinkedList result = new LinkedList();
        for (Object usedQAFile : usedQAFiles) {
            if (!result.contains(((TestInstallationMapping) usedQAFile).getTestInstallation())) {
                if (((TestInstallationMapping) usedQAFile).getTestInstallation().getChangeRequest().getStatus().compareToIgnoreCase(StarTeamStatus) == 0) {
                    result.add(((TestInstallationMapping) usedQAFile).getTestInstallation());
                }
            }
        }
        return result;
    }

    /**
     * Returns the change requests that are 'Dev Complete' in StarTeam, but does not have TestInstallations
     * created yet
     *
     * @param numberOfRecords
     * @return
     */
    public LinkedList getCRsThatRequireTestInstallations(int numberOfRecords) {
        try {
            LinkedList result = new LinkedList();
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
                DatabaseObject.setConnectInfo_TimeSheetODBC();
            } else {
                DatabaseObject.setConnectInfo_TimeSheetJDBC();
            }
            Connection con = DatabaseObject.getNewConnectionFromPool();
            String sqlString = "select CRNumber from cr where Status='Dev Complete'order by convert(integer,crnumber) desc";
            if (numberOfRecords > 0) {
                sqlString = "select top " + numberOfRecords + " CRNumber from cr where Status='Dev Complete'order by convert(integer,crnumber) desc";
            }
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                TestInstallation existingTestInstallation = this.getTestInstallation(crNumber);
                // This is a CR that is not yet assigned to a TestInstallation
                if (existingTestInstallation == null) {
                    result.add(new ChangeRequest(crNumber));
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return (new LinkedList());
        }
    }

    /**
     * Returns the change requests (of type ExtendedChangeRequest) that are 'New','Spec In Progress','Dev In
     * Progress' in StarTeam, and the actual development time exceeds the estimated time. Further more, these
     * CR's are marked as relatively high priorities.
     *
     * @param priorityLimit
     * @return
     */
    public LinkedList getCRsThatExceedEstimatedTime(int priorityLimit) {
        try {
            LinkedList result = new LinkedList();
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
                DatabaseObject.setConnectInfo_TimeSheetODBC();
            } else {
                DatabaseObject.setConnectInfo_TimeSheetJDBC();
            }
            Connection con = DatabaseObject.getNewConnectionFromPool();
            String sqlString = "select CRNumber from cr where fn_S_GetActualTime(CRNumber)>fn_S_GetEstimatedTime(CRNumber)"
                    + " and fn_S_GetEstimatedTime(CRNumber) > 0 and Status in ('New','Spec In Progress','Dev In Progress') and "
                    + " fn_S_GetPriority(CRNumber)<=" + priorityLimit + " order by fn_S_GetActualTime(CRNumber)-fn_S_GetEstimatedTime(CRNumber)";
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                TestInstallation existingTestInstallation = this.getTestInstallation(crNumber);
                // This is a CR that is not yet assigned to a TestInstallation
                if (existingTestInstallation == null) {
                    result.add(new ExtendedChangeRequest(crNumber));
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return (new LinkedList());
        }
    }

    /**
     * Returns the change requests with the given search field in either the Synopsis, Number or Description
     *
     * @param searchString
     * @return
     */
    @SuppressWarnings("UnnecessaryBoxing")
    public LinkedList getCRsContaining(String searchString) {
        try {
            StringTokenizer token = new StringTokenizer(searchString, ",\n");
            TreeMap rslt = new TreeMap();

            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
                DatabaseObject.setConnectInfo_TimeSheetODBC();
            } else {
                DatabaseObject.setConnectInfo_TimeSheetJDBC();
            }
            Connection con = DatabaseObject.getNewConnectionFromPool();
            String sqlString = "select CRNumber from cr order by CRNUMBER";

            // Step through each word in the search-string, looking for each one in turn
            while (token.hasMoreTokens()) {
                String tokenString = token.nextToken();
                if (tokenString.trim().length() > 0) {
                    sqlString = "select CRNumber from cr where upper(CRNUMBER) like '%" + tokenString.toUpperCase() + "%' or "
                            + " upper(SYNOPSIS) like '%" + tokenString.toUpperCase() + "%' or "
                            + " upper(DESCRIPTION) like '%" + tokenString.toUpperCase() + "%' "
                            + " order by CRNUMBER desc";

                    ResultSet rs = DatabaseObject.openSQL(sqlString, con);
                    while (rs.next()) {
                        String crNumber = rs.getString(1);

                        // If this CR already exists in the result, increment its importance
                        if (rslt.containsKey(crNumber)) {
                            Integer importance = (Integer) rslt.get(crNumber);
                            rslt.remove(crNumber);
                            rslt.put(crNumber, new Integer(importance.intValue() + 1));
                        } else {
                            rslt.put(crNumber, new Integer(1));
                        }
                    }
                    rs.close();
                }
            }

            DatabaseObject.releaseConnection(con);

            // Swap the keys and values
            TreeMap swappedResult = new TreeMap();
            System.out.println(rslt.size() + " CR(s) found with matching values");
            Iterator iter = rslt.keySet().iterator();
            while (iter.hasNext()) {
                String crNumber = (String) iter.next();
                Integer integer = (Integer) rslt.get(crNumber);
                Integer newMap = new Integer(crNumber);
                swappedResult.put(new Integer(100000000 - integer.intValue() * 100000 + newMap.intValue()), crNumber);
            }

            // Now, copy the contents of the latest TreeMap into the LinkedList
            LinkedList result = new LinkedList();
            Iterator iter2 = swappedResult.values().iterator();
            while (iter2.hasNext()) {
                String crNumber = (String) iter2.next();
                result.add(new ExtendedChangeRequest(crNumber));
            }

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return (new LinkedList());
        }
    }

    /**
     * Returns the change requests (of type ExtendedChangeRequest) that are 'New', 'Dev In Progress', 'Dev
     * Complete', 'QA In Progress' in StarTeam, the Priority is high, and the time since the logging of the CR
     * exceeds the allowed delay. This is used to see which CR's were logged as a high priority and has not
     * yet been addressed.
     *
     * @param priorityLimit
     * @param expiredDays
     * @return
     */
    public LinkedList getCRsThatRequiresAttention(int priorityLimit, int expiredDays) {
        try {
            LinkedList result = new LinkedList();
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
                DatabaseObject.setConnectInfo_TimeSheetODBC();
            } else {
                DatabaseObject.setConnectInfo_TimeSheetJDBC();
            }
            Connection con = DatabaseObject.getNewConnectionFromPool();
            String sqlString = "select CRNumber from cr where datediff(day, EnteredOn, now(*))>" + expiredDays
                    + " and fn_S_GetPriority(CRNumber)<=" + priorityLimit + " and Status in ('New','Spec In Progress','Dev In Progress','Dev Complete','QA In Progress') "
                    + " order by datediff(day, EnteredOn, now(*))";
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                TestInstallation existingTestInstallation = this.getTestInstallation(crNumber);
                // This is a CR that is not yet assigned to a TestInstallation
                if (existingTestInstallation == null) {
                    result.add(new ExtendedChangeRequest(crNumber));
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return (new LinkedList());
        }
    }

    /**
     * Returns the change requests that are 'Dev Complete' in StarTeam, are linked to TestInstallations, but
     * the status is not yet BUILD
     *
     * @return
     */
    public LinkedList getCRsThatHasIncompleteTestInstallations() {
        try {
            LinkedList result = new LinkedList();
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
                DatabaseObject.setConnectInfo_TimeSheetODBC();
            } else {
                DatabaseObject.setConnectInfo_TimeSheetJDBC();
            }
            Connection con = DatabaseObject.getNewConnectionFromPool();
            String sqlString = "select CRNumber from cr where Status='Dev Complete'order by CRNumber desc";
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                TestInstallation existingTestInstallation = this.getTestInstallation(crNumber);
                // This is a CR that is assigned to a TestInstallation, but the TestInstallation status is not BUILD
                if ((existingTestInstallation != null)
                        && ((existingTestInstallation.getStatus() == TestInstallation.NEW)
                        || (existingTestInstallation.getStatus() == TestInstallation.MODIFIED))) {
                    result.add(new ChangeRequest(crNumber));
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return (new LinkedList());
        }
    }

    /**
     * Loads the mappings from a file
     */
    private synchronized void load() {
        try {
            if (fileContainerFile.exists()) {
                // Temporary uncompressed file
                File tempFile = new File(getTemporaryFolder().getAbsolutePath() + getOSFileSeperator() + "tmp");
                if (!tempFile.exists()) {
                    TestInstallationArchiver.uncompressFile(fileContainerFile, tempFile);
                }
                FileInputStream fis = new FileInputStream(tempFile);
                ObjectInputStream ois = new ObjectInputStream(fis);

                // Load each TestInstallationMapping
                TestInstallationMapping fim = null;
                usedQAFiles.clear();
                int numberOfFileMappings = ois.readInt();
                System.out.println("Reading fileMappings...");
                System.out.println("numberOfFileMappings = " + numberOfFileMappings);
                for (int i = 0; i < numberOfFileMappings; i++) {
                    fim = (TestInstallationMapping) ois.readObject();
                    fim.getTestInstallation().getChangeRequest().refreshFromDatabase();
                    usedQAFiles.add(fim);
                }

                // Load each BuildInstallationMapping
                BuildInstallation bi = null;
                buildInstallations.clear();
                int numberOfBuildInstallations = ois.readInt();
                System.out.println("Reading Buildinstallations...");
                System.out.println("numberOfBuildInstallations = " + numberOfBuildInstallations);

                for (int j = 0; j < numberOfBuildInstallations; j++) {
                    bi = (BuildInstallation) ois.readObject();
                    buildInstallations.add(bi);
                }

                System.out.println("Loading complete.");
                // Close Streams
                ois.close();
                fis.close();

            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (EOFException eofe) {

        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Saves the mappings to a file
     */
    public synchronized void save() {
        try {
            // Temporary uncompressed file
            File tempFile = new File(getTemporaryFolder().getAbsolutePath() + getOSFileSeperator() + "tmp");

            // Clears the current contents
            fileContainerFile.delete();
            tempFile.delete();

            FileOutputStream fos = new FileOutputStream(tempFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            // Save each TestInstallationMapping, starting with the number of objects
            System.out.println("Saving FileMappings...");
            System.out.println("numberOfFileMappings = " + usedQAFiles.size());

            oos.writeInt(usedQAFiles.size());
            for (Object usedQAFile : usedQAFiles) {
                oos.writeObject(usedQAFile);
            }

            // Save each BuildInstallationMapping
            System.out.println("Saving BuildInstallations...");
            System.out.println("numberOfBuildInstallations = " + buildInstallations.size());
            oos.writeInt(buildInstallations.size());
            for (Object buildInstallation : buildInstallations) {
                oos.writeObject(buildInstallation);
            }

            // Close Streams
            oos.close();
            fos.close();

            // Let's compress the darn file!
            TestInstallationArchiver.compressFile(tempFile, fileContainerFile);

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Returns an enumerated value for the Operating System currently installed OS_WINDOWS = 0 OS_LINUX = 1
     *
     * @return
     */
    public static int getOS() {
        if (System.getProperty("os.name").toUpperCase().contains("WINDOWS")) {
            return OS_WINDOWS;
        } else {
            return OS_LINUX;
        }
    }

    /**
     * Returns the Operating System dependent File Seperator OS_WINDOWS = "\\" OS_LINUX = "/"
     *
     * @return
     */
    public static String getOSFileSeperator() {
        if (getOS() == OS_WINDOWS) {
            return "\\";
        } else {
            return "/";
        }
    }

    public static File getContainerFile() {
        if (getOS() == OS_WINDOWS) {
            return (new File(root_dir_win + "bin\\container.dat"));

        } else {
            return (new File(root_dir_ux + "DeploymentBuilder/bin/container.dat"));
        }
    }

    /**
     * Returns the subset of file.getAbsolutePath that only consists of the file path EXCLUDING the name.
     * <p>
     * Example:
     * <p>
     * file = "c:\temp\demo.txt"
     * <p>
     * getFileFolder(file)="c:\temp\"
     *
     * @param file
     * @return
     */
    public static String getFileFolder(File file) {
        String folder = new String(file.getAbsolutePath().substring(0, file.getAbsolutePath().indexOf(file.getName()) - 1));
        return folder;
    }

    public static File getTemporaryFolder() {
        if (getOS() == OS_WINDOWS) {
            return (new File(root_dir_win + "Temp\\"));
        } else {
            return (new File(root_dir_ux + "Temp/"));
        }
    }

    public static File getArchiveFolder() {
        if (getOS() == OS_WINDOWS) {
            return (new File(root_dir_win + "Archive\\"));
        } else {
            return (new File(root_dir_ux + "Archive/"));
        }
    }

    public synchronized static String getHTTPHost() {
        if (getOS() == OS_WINDOWS) {
            return ("http://172.31.1.197:8080/DeploymentBuilder/");
        } else {
            return ("http://172.31.160.197:8080/DeploymentBuilder/");
        }
    }

    /**
     * Properties will be get/set from a config.ini file stored in the temp folder
     *
     * @param propertyName
     * @return
     */
    public static String getConfigProperty(String propertyName) {
        IniFile ini = null;
        try {
            ini = new IniFile(getConfigFileName());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        //Now get the database information
        String result = ini.getProfile("FileContainer", propertyName);
        return result;
    }

    /**
     * Properties will be get/set from a config.ini file stored in the temp folder
     *
     * @param propertyName
     * @param propertyValue
     */
    public synchronized static void setConfigProperty(String propertyName, String propertyValue) {
        IniFile ini = null;
        try {
            ini = new IniFile(getConfigFileName());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        try {
            ini.putProfile("FileContainer", propertyName, propertyValue);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static String getConfigFileName() {
        return (getTemporaryFolder() + getOSFileSeperator() + "config.ini");
    }

    // Returns the bookings that resides in the database
    public LinkedList getExistingBookingItems() {
        LinkedList bookingItems = new LinkedList();
        DatabaseObject databaseObject = DatabaseObject.getInstance();
        if (FileContainer.getOS() == FileContainer.OS_WINDOWS) {
            DatabaseObject.setConnectInfo_TimeSheetODBC();
        } else {
            DatabaseObject.setConnectInfo_TimeSheetJDBC();
        }
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select CRNumber from BookingItem", con);
            while (rs.next()) {
                String crNumber = rs.getString(1);
                if (crNumber != null) {
                    ChangeRequest cr = new ChangeRequest(crNumber);
                    BookingItem bookingItem = new BookingItem(cr);
                    if ((bookingItem != null)) {
                        bookingItems.add(bookingItem);
                    }
                }
            }
            rs.close();
            DatabaseObject.releaseConnection(con);

            return bookingItems;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns a color tag (HTML) for the given Change Request. This is used within the Web Pages to display a
     * background color for different types of CRs.
     *
     * @param changeRequest
     * @return
     */
    public String getChangeRequestBackgroundColor(ChangeRequest changeRequest) {
        if (changeRequest == null) {
            return "";
        }
        if (changeRequest.getType().trim().compareToIgnoreCase("enhancement") == 0) {
            return " bgcolor=\"#CCFFCC\"";
        }
        if (changeRequest.getType().trim().compareToIgnoreCase("defect") == 0) {
            return " bgcolor=\"#FFCCCC\"";
        }
        if (changeRequest.getType().trim().compareToIgnoreCase("new requirement") == 0) {
            return " bgcolor=\"#CCCCFF\"";
        }

        return "";

    }

    /**
     * Returns the files booked in this BookingItem as well as the detail of TestInstallations currently
     * holding back those files
     *
     * @param bookingItem
     * @return
     */
    public String getRequiredFilesWithDetailInHTML(BookingItem bookingItem) {
        StringBuilder result = new StringBuilder("<font size=\"-1\">");
        Iterator iter = bookingItem.getRequiredFileNames().iterator();
        while (iter.hasNext()) {
            String fileName = (String) iter.next();
            result.append(fileName.toUpperCase());
            // Which TestInstallation is currently using this file?
            TestInstallation testInstallation = getTestInstallationUsingFile(fileName);
            if (testInstallation != null) {
                result.append("<b><i> [" + testInstallation.getName() + "]</i></b><br>");
            } else {
                result.append("<b><i> -available- </i></b><br>");
            }
        }
        result.append("</font>");
        return result.toString();
    }

    /**
     * Returns the files required by this potential combination of Change Reuqests
     *
     * @param combinedChangeRequest
     * @return
     */
    public String getRequiredFilesWithDetailInHTML(CombinedChangeRequest combinedChangeRequest) {
        StringBuilder result = new StringBuilder("<font size=\"-1\">");
        Iterator iter = combinedChangeRequest.getUniqueFileNames(combinedChangeRequest.getIncludedBookingItems()).iterator();
        while (iter.hasNext()) {
            String fileName = (String) iter.next();
            result.append(fileName.toUpperCase());
            // Which TestInstallation is currently using this file?
            TestInstallation testInstallation = getTestInstallationUsingFile(fileName);
            if (testInstallation != null) {
                result.append("<b><i> [" + testInstallation.getName() + "]</i></b><br>");
            } else {
                result.append("<b><i> -available- </i></b><br>");
            }
        }
        result.append("</font>");
        return result.toString();
    }

    /**
     * We need to re-extract the contents of each and every Test Installation into the folder from where
     * buildInstallations is done. This is to ensure that whatever is tested in a TestInstallation, is what
     * goes into a buildInstallation.
     *
     * I had to add this method in the FileContainer, because the current BuildInstallation and
     * TestInstallation classes are already been serialised, and can therefore not be changed.
     *
     * @param testInstallation
     */
    public static void extractTestInstallationFiles(TestInstallation testInstallation) {
        try {
            // Original archived file
            File compressedArchiveFile = new File(FileContainer.getArchiveFolder() + FileContainer.getOSFileSeperator() + testInstallation.getName() + FileContainer.getTestInstallationExtension());
            // Uncompressed testInstallation file
            File uncompressedArchiveFile = new File(FileContainer.getTemporaryFolder() + FileContainer.getOSFileSeperator() + testInstallation.getName() + ".tmp");
            // Compressor
            System.out.println("Extracting TestInstallation Files for " + testInstallation);
            Compressor compressor = new Compressor();
            compressor.uncompressFile(compressedArchiveFile, uncompressedArchiveFile);

            FileInputStream fis = new FileInputStream(uncompressedArchiveFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            QAFile qaFile = null;
            FileOutputStream fos = null;

            // Start by reading the Change Request
            ChangeRequest changeRequest = (ChangeRequest) ois.readObject();

            while ((qaFile = (QAFile) ois.readObject()) != null) {
                System.out.println("extracting :" + qaFile.getFile().getAbsoluteFile());
            }

            ois.close();
        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (EOFException eofe) {

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public static String getTestInstallationExtension() {
        return TestInstallationExtension;
    }

    public static String getBuildInstallationExtension() {
        return BuildInstallationExtension;
    }

    public static int OS_WINDOWS = 0;
    public static int OS_LINUX = 1;

    // usedQAFiles will be a global repository for all the files in use
    private static LinkedList usedQAFiles;
    private static LinkedList buildInstallations;
    private static File fileContainerFile = null;
    private static FileContainer fileContainer;
    private static LinkedList changeRequestNumbers;  // Static container of ChangeRequestNumbers

    public static String TestInstallationExtension = ".arc";
    public static String BuildInstallationExtension = ".bld";
    public static final String root_dir_win = "c:\\DeploymentBuilder\\";
    public static final String root_dir_ux = "~/DeploymentBuilder/";

}

/**
 * Simple mapping between a QAFile and a TestInstallation
 */
class TestInstallationMapping implements java.io.Serializable {

    public TestInstallationMapping(QAFile qaFile, TestInstallation testInstallation) {
        this.qaFile = qaFile;
        this.testInstallation = testInstallation;
    }

    public QAFile getQAFile() {
        return qaFile;
    }

    public TestInstallation getTestInstallation() {
        return testInstallation;
    }

    private  QAFile qaFile;
    private  TestInstallation testInstallation;

}
