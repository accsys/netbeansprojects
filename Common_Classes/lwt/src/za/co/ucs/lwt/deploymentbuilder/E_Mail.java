package za.co.ucs.lwt.deploymentbuilder;

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import za.co.ucs.accsys.tools.ByteArrayDataSource;

/**
 * Business layer of the E_Mail entitity.
 * <P>
 * Singleton
 *
 * @author Liam Terblanche Note: By its nature, E_Mails need only be created.
 * The saving and handling of the object should thereafter be done automatically
 */
public class E_Mail {

    private E_Mail() {
        smtpServer = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_Server, "");
        smtpPort = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_Port, "25");
        smtpFrom = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_FromAddress, "");
        smtpTo = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_ToAddress, "");
        smtpRequiresAuthentication = (DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_RequireAuthentication, "False").compareToIgnoreCase("True") == 0);
        smtpAuthUser = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_AuthenticationUser, "");
        smtpAuthPwd = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_AuthenticationPwd, "");
        smtpUseSSL = (DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SMTP_UseSSL, "False").compareToIgnoreCase("True") == 0);
    }

    private E_Mail(String smtpServer, String smtpFrom) {
        E_Mail.smtpServer = smtpServer;
        this.smtpFrom = smtpFrom;
    }

    /**
     * Returns the static instance of E_Ma
     *
     * @return il
     */
    public static E_Mail getInstance() {
        if (eMail == null) {
            eMail = new E_Mail();
        }
        return eMail;
    }

    /**
     * Returns the static instance of E_Mail
     *
     * @param smtpServer
     * @param smtpFrom
     * @return
     */
    public static E_Mail getInstance(String smtpServer, String smtpFrom) {
        if (eMail == null) {
            eMail = new E_Mail(smtpServer, smtpFrom);
        }
        return eMail;
    }

    public boolean send(String smtpTo, String smtpCC, String subject, String body) {
        return send(smtpTo, smtpCC, subject, body, null);
    }

    /**
     * Sends an e-mail to specified address Returns true if successful. The SMTP
     * server address and the smtpFrom e-mail address is read from a
     * configuration file called 'config.dat'
     *
     * @param smtpTo
     * @param smtpCC
     * @param subject
     * @param body
     * @param attachedFile
     * @return
     */
    public boolean send(String smtpTo, String smtpCC, String subject, String body, java.io.File attachedFile) {
        try {
            Properties props = new Properties();

            // Do I have a recipient?
            if (smtpTo.trim().length() == 0) {
                System.out.println("Notification Failure: Undefined recipient for Message...");
                System.out.println("Subject:\"" + subject + "\"");
                System.out.println("Body\"" + body + "\"");
                return false;
            }
            // Try to attach to a default session
            //        props.put("mail.smtp.host", smtpServer);
            if (smtpRequiresAuthentication) {
                props.put("mail.smtp.auth", "true");
            }
            if (smtpUseSSL) {
                props.put("mail.smtp.starttls.enable", "true");
            }

            props.put("mail.smtp.host", smtpServer);
            props.put("mail.smtp.port", smtpPort);
            //Session mailSession = Session.getDefaultInstance(props, null);
            //mailSession.setDebug(true);
            Session mailSession;
            if (smtpRequiresAuthentication) {
                mailSession = Session.getInstance(props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(smtpAuthUser, smtpAuthPwd);
                            }
                        });
                mailSession.setDebug(false);
            } else {
                mailSession = Session.getDefaultInstance(props, null);
                mailSession.setDebug(false);
            }

            //Create a new message
            MimeMessage msg = new MimeMessage(mailSession);

            // Set FROM and TO
            msg.setFrom(new InternetAddress(smtpFrom));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(smtpTo, false));
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(smtpCC, false));
            // Set Subject and Body
            msg.setSubject(subject);
            // msg.setText(body);
            //msg.setContent(body,"text/html");
            msg.setDataHandler(new DataHandler(new ByteArrayDataSource(body, "text/html")));
            Transport transport = mailSession.getTransport("smtp");
            if (smtpRequiresAuthentication) {
                transport.connect(smtpServer, smtpAuthUser, smtpAuthPwd);
            }
            if ((attachedFile != null) && (attachedFile.exists())) {

                // create and fill the first message part
                MimeBodyPart mbp1 = new MimeBodyPart();
                mbp1.setText(body);

                // create the second message part
                MimeBodyPart mbp2 = new MimeBodyPart();

                // attach the file to the message
                FileDataSource fds = new FileDataSource(attachedFile);
                mbp2.setDataHandler(new DataHandler(fds));
                mbp2.setFileName(fds.getName());

                // create the Multipart and add its parts to it
                Multipart mp = new MimeMultipart();
                mp.addBodyPart(mbp1);
                mp.addBodyPart(mbp2);

                // add the Multipart to the message
                msg.setContent(mp);

                // set the Date: header
                msg.setSentDate(new Date());

            }
            Transport.send(msg);
            System.out.println("Sending mail to:" + smtpTo + "\nMessage:" + body);

            return true;
        } catch (MessagingException ex) {
            System.out.println("Sendfail exception: " + ex);
            return false;
        }
    }
    static E_Mail eMail = null;
    static String smtpServer;
    static String smtpPort;
    String smtpFrom;
    static String smtpTo;
    String smtpAuthUser;
    String smtpAuthPwd;
    boolean smtpRequiresAuthentication = false;
    boolean smtpUseSSL = false;
}
