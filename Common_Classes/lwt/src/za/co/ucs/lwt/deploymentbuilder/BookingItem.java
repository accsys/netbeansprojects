/*
 * Reminder.java
 *
 * Created on January 27, 2004, 3:38 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;

/**
 * The BookingItem class is used by the developers to attach a reminder
 * to the availability of a certain Files.  This is a handy way of
 * being notified when the given file becomes available for use in another
 * Change Request.
 * @author  liam
 */
public class BookingItem {
    
    /** Creates a new instance of a BookingItem for records that
     * already exists in the database
     */
    public BookingItem(ChangeRequest changeRequest) {
        this.requiredFileNames = new LinkedList();
        this.changeRequest = changeRequest;
        refreshFromDatabase();
    }
    
    /** Populate a BookingItem's properties from the
     * BookingItem table in the TimeSheet Database
     */
    public boolean refreshFromDatabase(){
        return (BookingItemDatabaseInterface.getInstance().refreshFromDatabase(this));
    }
    
    /** Save a BookingItem's properties back to the
     * BookingItem table in the TimeSheet Database
     */
    public void saveToDatabase(){
        if (bookingItemExistsInDb()){
            BookingItemDatabaseInterface.getInstance().updateDatabase(this);
        } else {
            BookingItemDatabaseInterface.getInstance().insertIntoDatabase(this);
        }
    }
    
    /** Save a BookingItem's properties back to the
     * BookingItem table in the TimeSheet Database
     */
    public void deleteFromDatabase(){
        if (bookingItemExistsInDb()){
            BookingItemDatabaseInterface.getInstance().deleteFromDatabase(this);
        }
    }
    
    public boolean bookingItemExistsInDb(){
        return refreshFromDatabase();
    }
    
    /** Getter for property user.
     * @return Value of property user.
     *
     */
    public za.co.ucs.lwt.deploymentbuilder.User getUser() {
        return user;
    }
    
    /** Setter for property user.
     * @param user New value of property user.
     *
     */
    public void setUser(za.co.ucs.lwt.deploymentbuilder.User user) {
        this.user = user;
    }
    
    /** Getter for property requiredQAFiles.
     * @return Value of property requiredQAFiles.
     *
     */
    public java.util.LinkedList getRequiredFileNames() {
        return requiredFileNames;
    }
    
    /** Setter for property requiredQAFiles.
     * @param requiredQAFiles New value of property requiredQAFiles.
     *
     */
    public void setRequiredFileNames(java.util.LinkedList requiredFileNames) {
        this.requiredFileNames = requiredFileNames;
    }
    
    /** Getter for property changeRequest.
     * @return Value of property changeRequest.
     *
     */
    public za.co.ucs.lwt.deploymentbuilder.ChangeRequest getChangeRequest() {
        return changeRequest;
    }
    
    
    public String getRequiredFileNamesAsString(){
        StringBuffer result = new StringBuffer();
        Iterator iter = requiredFileNames.iterator();
        while (iter.hasNext()){
            if (result.length()==0){
                result.append((String)iter.next());
            } else {
                result.append(","+(String)iter.next());
            }
        }
        return result.toString();
    }
    
    public void setRequiredFileNames(String fileNames){
        requiredFileNames.clear();
        StringTokenizer token = new StringTokenizer(fileNames, ",;\n");
        while (token.hasMoreElements()){
            requiredFileNames.add(token.nextElement());
        }
    }
    
    
    private User user;
    private LinkedList requiredFileNames;
    private ChangeRequest changeRequest;
}
