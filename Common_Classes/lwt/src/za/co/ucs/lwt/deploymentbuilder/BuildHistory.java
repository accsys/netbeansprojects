/*
 * BuildHistory.java
 *
 * Created on April 2, 2004, 12:45 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/**
 * The BuildHistory Class will be used to retain detail of CR's, Files, sizes, etc for every build being created
 * Singleton
 * @author  liam
 */
public class BuildHistory implements java.io.Serializable{
    
    
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
        C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.lwt.deploymentbuilder.BuildHistory
        za.co.ucs.lwt.deploymentbuilder.BuildHistory:    static final long serialVersionUID = 4636283733705754393L;
     
     */
    static final long serialVersionUID = 4636283733705754393L;
    
    
    /** Creates a new instance of History */
    private BuildHistory() {
        buildHistoryFile = new File(FileContainer.getArchiveFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"buildhistory.dat");
        buildHistoryFile.mkdirs();
        buildHistoryEvents = new LinkedList();
    }
    
    public static BuildHistory getInstance(){
        System.out.println("BuildHistory.getInstance()");
        if (buildHistory == null)
            BuildHistory.createInstance();
        System.out.println("                      :"+buildHistory);
        return buildHistory;
    }
    
    private static synchronized void createInstance(){
        if (buildHistory == null){
            buildHistory = new BuildHistory();
            buildHistory.load();
        }
    }
    
    /** Loads the mappings from a file
     */
    private void load(){
        try{
            // Temporary uncompressed file
            File tempFile = new File(FileContainer.getTemporaryFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"buildhist");
            System.out.println("Loading buildHistory from: "+buildHistoryFile);
            //if (tempFile.exists()){
            TestInstallationArchiver.uncompressFile(buildHistoryFile, tempFile);
            
            
            FileInputStream fis = new FileInputStream(tempFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            // Load each BuildHistoryEvent
            buildHistoryEvents.clear();
            int numberOfEvents = ois.readInt();
            for (int i=0; i<numberOfEvents; i++){
                BuildHistoryEvent event = (BuildHistoryEvent)ois.readObject();
                buildHistoryEvents.add(event);
            }
            
            // Close Streams
            ois.close();
            fis.close();
            //}
        } catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        } catch (EOFException eofe){
            
        } catch (IOException ioe){
            ioe.printStackTrace();
        } catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
    }
    
    /** Saves the mappings to a file
     */
    public void save(){
        try{
            // Temporary uncompressed file
            File tempFile = new File(FileContainer.getTemporaryFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"buildhist");
            tempFile.mkdirs();
            
            // Clears the current contents
            buildHistoryFile.delete();
            tempFile.delete();
            
            
            System.out.println("Saving buildHistory to: "+tempFile);
            FileOutputStream fos = new FileOutputStream(tempFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            // Save each HistoryEvent
            oos.writeInt(buildHistoryEvents.size());
            for (int i=0; i<buildHistoryEvents.size(); i++){
                oos.writeObject(buildHistoryEvents.get(i));
            }
            
            // Close Streams
            oos.close();
            fos.close();
            
            // Let's compress the darn file!
            TestInstallationArchiver.compressFile(tempFile, buildHistoryFile);
            
        } catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    
    public void addEvent(BuildHistoryEvent event){
        buildHistoryEvents.add(event);
        save();
    }
    
    public LinkedList getEvents(){
        return buildHistoryEvents;
    }
    
    
    private static LinkedList buildHistoryEvents;
    private static BuildHistory buildHistory = null;
    private static File buildHistoryFile = null;
}

