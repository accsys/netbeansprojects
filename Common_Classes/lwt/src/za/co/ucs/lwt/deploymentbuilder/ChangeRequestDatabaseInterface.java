/*
 * ChangeRequestDatabaseInterface.java
 *
 * Created on December 9, 2003, 2:19 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * Singleton, used to update ChangeRequest from the TimeSheet Database
 * @author  liam
 */
public class ChangeRequestDatabaseInterface {
    
    /** Creates a new instance of ChangeRequestDatabaseInterface */
    private static synchronized void createInstance(){
        if (dbInterface == null){
            dbInterface = new ChangeRequestDatabaseInterface();
        }
    }
    
    private ChangeRequestDatabaseInterface(){
    }
    
    /** Get single instance of this class. */
    public static ChangeRequestDatabaseInterface getInstance(){
        if (dbInterface == null)
            ChangeRequestDatabaseInterface.createInstance();
        return dbInterface;
    }
    
    /** Populate a Change Request's properties from the
     * CR table in the TimeSheet Database
     */
    public void refreshFromDatabase(ChangeRequest changeRequest){
        try{
            DatabaseObject databaseObject = DatabaseObject.getInstance();
            if (FileContainer.getOS()==FileContainer.OS_WINDOWS)
                databaseObject.setConnectInfo_TimeSheetODBC();
            else
                databaseObject.setConnectInfo_TimeSheetJDBC();
            Connection con = databaseObject.getNewConnectionFromPool();
            String sqlString = "select Status, Synopsis, Description, Responsibility, Type, Severity, Category, "+
            "EnteredBy, EnteredOn, ReadMe from CR where upper(CRNumber)='"+changeRequest.getCRNumber().toUpperCase()+"'";
            ResultSet rs = databaseObject.openSQL(sqlString, con);
            while (rs.next()){
                if ((rs.getString("ReadMe") != null) &&
                (rs.getString("ReadMe").trim().length()>0)){
                    changeRequest.setReadMe(rs.getString("ReadMe"));}
                else {
                    changeRequest.setReadMe(rs.getString("Synopsis"));
                }
                changeRequest.setCategory(rs.getString("Category"));
                changeRequest.setDescription(rs.getString("Description"));
                changeRequest.setResponsibility(rs.getString("Responsibility"));
                changeRequest.setStatus(rs.getString("Status"));
                changeRequest.setSynopsis(rs.getString("Synopsis"));
                changeRequest.setType(rs.getString("Type"));
                changeRequest.setSeverity(rs.getString("Severity"));
                changeRequest.setEnteredBy(rs.getString("EnteredBy"));
                changeRequest.setEnteredOn(rs.getString("EnteredOn"));
            }
            rs.close();
            databaseObject.releaseConnection(con);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    private static ChangeRequestDatabaseInterface dbInterface;
}
