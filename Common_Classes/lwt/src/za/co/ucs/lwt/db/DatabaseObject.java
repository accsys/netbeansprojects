package za.co.ucs.lwt.db;

import java.sql.*;
import java.util.*;
import java.text.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Singleton, only to be used by {@link za.co.muse.db} classes
 *
 * @author Liam Terblanche
 */ 
public class DatabaseObject {

    private static synchronized void createInstance() {
        if (theObject == null) {
            theObject = new DatabaseObject();
            try {
                debug = false;

            } catch (Exception e) {
                debug = false;
            }
            if (debug) {
                try {
                    logFile = new za.co.ucs.lwt.initools.IniFile(System.getProperty("user.dir") + "\\debug.log");
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Get single instance of this class.
     * @return 
     */
    public static DatabaseObject getInstance() {
        if (theObject == null) {
            DatabaseObject.createInstance();
        }
        return theObject;
    }

    /**
     * execute SQL statement: Not returning results
     *
     * @param aSQLStatement
     * @param con
     * @return cnt number of records updated/inserted/etc.
     * @throws java.sql.SQLException
     */
    public static int executeSQL(String aSQLStatement, Connection con) throws SQLException {
        Statement s = con.createStatement();
        try {
            if (debug) {
                logDebugMessage(aSQLStatement);
            }
            int cnt = s.executeUpdate(fixSQLFieldValue(aSQLStatement));
            return cnt;
        } catch (SQLException e) {
            e.printStackTrace();
            if (debug) {
                logDebugMessage("SQLException:" + e.getMessage());
            }
            System.out.println("SQLException: Statement=" + aSQLStatement);
            return 0;
        }

    }

    /**
     * Returns the number of available ESS credits. This is a function of the
     * number of credits purchased - the number of completed processes logged.
     * @return 
     */
    public static int getAvailableESSCredits() {
        String aSQLStatement = "select fn_P_GetESSCredits()";
        Connection con = getNewConnectionFromPool();
        try {
            try {
                return getInt(aSQLStatement, con);
            } catch (SQLException e) {
                e.printStackTrace();
                if (debug) {
                    logDebugMessage("SQLException:" + e.getMessage());
                }
                System.out.println("SQLException: Statement=" + aSQLStatement);
                System.out.println("Error:" + e.getMessage());
                if (e.getMessage().indexOf("Connection is already closed") >= 0) {
                    ConnectionPool.getInstance().destroyConnection(con);
                    System.out.println("Connection removed from pool.");
                }
                return 0;
            }
        } finally {
            releaseConnection(con);
        }
    }

    /**
     * Returns true if ESS_Reporting_Registered is activated in A_SETUP
     * @return 
     */
    public static boolean isESSReportingRegistered() {
        String aSQLStatement = "select DATA from A_SETUP where ITEM = 'ESS_Reporting_Registered'";
        Connection con = getNewConnectionFromPool();
        boolean rslt = false;
        try {
            try {
                String YesNo = getString(aSQLStatement, con);
                if (YesNo.compareTo("Yes")==0){
                rslt = true;
                }else{
                    rslt = false;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            releaseConnection(con);
        }
        return rslt;
    }
    
    /**
     * Writes debug info into the debug.log file
     */
    private static void logDebugMessage(String string) {
        try {
            logFile.putProfile("SQL_Statements", formatTime(new java.util.Date()), string);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * execute SQL statement: Returning the Result Set
     * @param aSQLStatement
     * @param con
     * @return 
     * @throws java.sql.SQLException
     */
    public static ResultSet openSQL(String aSQLStatement, Connection con) throws SQLException {
        try {
            Statement s = con.createStatement(java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY);
            if (debug) {
                logFile.putProfile("SQL Statements", formatTime(new java.util.Date()), aSQLStatement);
            }
            ResultSet rs = s.executeQuery(fixSQLFieldValue(aSQLStatement));
            return (rs);
        } catch (SQLException e) {
            e.printStackTrace();
            if (debug) {
                logDebugMessage("SQLException:" + e.getMessage());
            }
            System.out.println("SQLException: Statement=" + aSQLStatement);
            System.out.println("Error:" + e.getMessage());
            if (e.getMessage().indexOf("Connection is already closed") >= 0) {
                ConnectionPool.getInstance().destroyConnection(con);
                System.out.println("Connection removed from pool.");
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            if (debug) {
                logDebugMessage("Exception:" + e.getMessage());
            }
            System.out.println("Exception: Statement=" + aSQLStatement);
            return null;
        }
    }

    /**
     * Execute SQL statement: Returns true if successful connection was made.
     *
     * @param aSQLStatement Statement to be used to test the connection with
     * @param con Connection
     * @param autoReleaseConnection If True, this method will automatically
     * release this connection
     * @return
     */
    public static boolean canConnect(String aSQLStatement, Connection con, boolean autoReleaseConnection) {
        boolean rslt = false;
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(fixSQLFieldValue(aSQLStatement));
            rslt = true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQLException: Statement=" + aSQLStatement);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception: Statement=" + aSQLStatement);
            return false;
        } finally {
            if (autoReleaseConnection) {
                DatabaseObject.releaseConnection(con);
            }
        }
        return rslt;
    }

    /**
     * execute SQL statement: Returning the first record as a String
     * @param aSQLStatement
     * @param con
     * @return 
     * @throws java.sql.SQLException
     */
    public static String getString(String aSQLStatement, Connection con) throws SQLException {
        try {
            Statement s = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = s.executeQuery(fixSQLFieldValue(aSQLStatement));
            if (rs.first()) {
                return (rs.getString(1));
            } else {
                return "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQLException: Statement=" + aSQLStatement);
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception: Statement=" + aSQLStatement);
            return "";
        }
    }

    /**
     * execute SQL statement: Returning the first record as an int
     * @param aSQLStatement
     * @param con
     * @return 
     * @throws java.sql.SQLException
     */
    public static int getInt(String aSQLStatement, Connection con) throws SQLException {
        try {
            Statement s = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = s.executeQuery(fixSQLFieldValue(aSQLStatement));
            if (rs.first()) {
                return (rs.getInt(1));
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQLException: Statement=" + aSQLStatement);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception: Statement=" + aSQLStatement);
            return 0;
        }
    }

    /**
     * Releases the connection and makes it available for another object.
     * @param con
     */
    public static void releaseConnection(Connection con) {
        ConnectionPool.getInstance().releaseConnection(con);
    }

    public static Connection getNewConnectionFromPool() {
        try {
            Class.forName(className);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        ConnectionPool pool = ConnectionPool.getInstance();
        // Ask the pool for a connection
        Connection conFromPool = pool.getConnection();
        if (conFromPool != null) {
            return (conFromPool);
        } else {
            // No connection available in pool
            // Get normal connection
            Connection con = getNewConnection();
            if (con != null) {
                pool.addUsedConnection(con);
            }
            return con;
        }
    }

    public static Connection getNewConnection() {
        try {
            // NORMAL CONNECTIONS
            Class.forName(className);
            //System.out.println("Requesting connection from:"+className);
            Connection con = DriverManager.getConnection(url, uid, pwd);
            if (con == null) {
                System.out.println("Unable to establish a connection.");
            }
            return con;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            if (e.getMessage().indexOf("Login failed") > 0) {
                return getAltNewConnection();
            } else if (e.getMessage().indexOf("Invalid user ID or password") > 0) {
                return getAltNewConnection();
            } else {
                e.printStackTrace();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns a connection, using the alternative password, should the original
     * one fails
     */
    private static Connection getAltNewConnection() {
        try {
            // NORMAL CONNECTIONS
            Class.forName(className);
            Connection con = DriverManager.getConnection(url, uid, altPwd);
            return con;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            if (e.getMessage().indexOf("Login failed") > 0) {
            } else {
                e.printStackTrace();
            }
            return null;
        } 
    }

    /**
     * Sets fields to values required to connect to the Time Sheet application
     * via JDBC
     */
    public static void setConnectInfo_TimeSheetJDBC() {
        className = "com.sybase.jdbc3.jdbc.SybDriver";
        url = "jdbc:sybase:Tds:localhost:2638";
        uid = "DBA";
        pwd = "SQL";
    }

    /**
     * Sets fields to values required to connect to a MySQL database
     */
    public static void setConnectInfo_MySQL() {
        className = "com.mysql.jdbc.Driver";
        url = "jdbc:mysql://localhost:3306/recruit";
        uid = "root";
        pwd = "root";
    }

    /**
     * Sets fields to values required to connect to Accsys Peopleware on the local PC, port 2638
     */
    public static void setConnectInfo_PeopleWareLocalHost() {
       className = "com.sybase.jdbc3.jdbc.SybDriver";
        url = "jdbc:sybase:Tds:localhost:2638";
        uid = "DBA";
        pwd = "I,don'tKNOW!1t.";
    }

    /**
     * Sets fields to values required to connect to the Accsys Peopleware
     * database via JDBC
     * @param theUrl
     */
    public static void setConnectInfo_AccsysJDBC(String theUrl) {
        className = "com.sybase.jdbc3.jdbc.SybDriver";
        url = theUrl;//"jdbc:sybase:Tds:172.31.1.38:2638";
        uid = "DBA";
        pwd = "I,don'tKNOW!1t.";
        altPwd = "qa,dev7";
    }

    /**
     * Sets fields to values required to connect to the Time Sheet application
     * via ODBC
     */
    public static void setConnectInfo_TimeSheetODBC() {
        //className = "sun.jdbc.odbc.JdbcOdbcDriver";
        //url = "jdbc:odbc:TimeSheet";
        //uid = "DBA";
        //pwd = "SQL";
        className = "com.sybase.jdbc3.jdbc.SybDriver";
        url = "jdbc:sybase:Tds:localhost:2638";
        uid = "DBA";
        pwd = "SQL";
    }

    public static String getCurrentDate() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            return df.format(new java.util.Date());
        } catch (Exception e) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            return df.format(new java.util.Date());

        }
    }

    /**
     * Returns an array with the last N months in the form of 'March
     * 2003','April 2003',....
     *
     * @param endDate The date from where 24 months should be generated, ending
     * at endDate.
     * @param N Number of months into the past.
     * @return 
     */
    public static String[] getPrevious_N_Months(java.util.Date endDate, int N) {
        String[] result = new String[N];
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(endDate);

        // Step through 24 months, generating a database-specific
        // Date format
        for (int i = 0; i < N; i++) {
            result[N - 1 - i] = formatDateToMonthYear(calendar.getTime());
            // move one month back in time
            calendar.add(Calendar.MONTH, -1);
        }
        return result;
    }

    /**
     * Returns an array with N/2 months before and N/2 months after the given
     * date in the form of 'March 2003','April 2003',....
     *
     * @param referenceDate The date from where N/2 months should be generated
     * before and after
     * @param N Number of months in total - <b>Must be divisible by 2</b>.
     * @return 
     */
    public static String[] get_N_Months_Around(java.util.Date referenceDate, int N) {
        String[] result = new String[N];
        // Is N divisible by 2?
        if ((N % 2) != 0) {
            N++;
        }

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(referenceDate);
        // Move on N/2 months
        calendar.add(Calendar.MONTH, N / 2);

        // Step through N months, generating a database-specific
        // Date format
        for (int i = 0; i < N; i++) {
            result[N - 1 - i] = formatDateToMonthYear(calendar.getTime());
            // move one month back in time
            calendar.add(Calendar.MONTH, -1);
        }
        return result;
    }

    /**
     * Returns an array with N months into the future in the form of '1 March
     * 2007','1 April 2007',....
     *
     * @param referenceDate The date from where N/2 months should be generated
     * before and after
     * @param N Number of months in total - <b>Must be divisible by 2</b>.
     * @return 
     */
    public static String[] get_Next_N_Months(java.util.Date referenceDate, int N) {
        String[] result = new String[N];

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(referenceDate);
        calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);

        // Step through N months, generating a database-specific
        // Date format
        for (int i = 0; i < N; i++) {
            result[i] = formatDate(calendar.getTime());
            // move one month back forward
            calendar.add(Calendar.MONTH, 1);
        }
        return result;
    }

    /**
     * Returns an array with N/2 weeks before and N/2 weeks after the given date
     * in the form of '2003/03/01','2004/03/08',....
     *
     * @param referenceDate The date from where N/2 weeks should be generated
     * before and after
     * @param N Number of weeks in total - <b>Must be divisible by 2</b>.
     * @return 
     */
    public static String[] get_N_Weeks_Around(java.util.Date referenceDate, int N) {
        String[] result = new String[N];
        // Is N divisible by 2?
        if ((N % 2) != 0) {
            N++;
        }

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(referenceDate);
        // Move on N/2 weeks
        calendar.add(Calendar.WEEK_OF_YEAR, N / 2);

        // Step through N weeks, generating a database-specific
        // Date format
        for (int i = 0; i < N; i++) {
            result[N - 1 - i] = formatDate(calendar.getTime());
            // move one week back in time
            calendar.add(Calendar.WEEK_OF_YEAR, -1);
        }
        return result;
    }

    /**
     * Returns name of month
     * @param referenceDate
     * @return 
     */
    public static String getNameOfMonth(java.util.Date referenceDate) {
        String[] monthName = {"January", "February",
            "March", "April", "May", "June", "July",
            "August", "September", "October", "November",
            "December"};

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(referenceDate);

        String month = monthName[calendar.get(Calendar.MONTH)];

        return month;
    }

    /**
     * Returns an array with days of the month in which the referenceDate is
     *
     * @param referenceDate
     * @return 
     */
    public static int getDayOfMonth(java.util.Date referenceDate) {
        GregorianCalendar calendar = new GregorianCalendar();

        // set calendar to the beginning of the month
        calendar.setTime(referenceDate);
        return (calendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Returns an array with days of the month in which the referenceDate is
     *
     * @param referenceDate
     * @return 
     */
    public static java.util.Date[] getDaysOfMonth(java.util.Date referenceDate) {
        GregorianCalendar calendar = new GregorianCalendar();

        // set calendar to the beginning of the month
        calendar.setTime(referenceDate);
        calendar.set(Calendar.DAY_OF_MONTH, 1);


        int N = 31;
        java.util.Date[] result = new java.util.Date[N];

        // Step over 31 days
        for (int i = 0; i < N; i++) {
            java.util.Date aDay = new java.util.Date(calendar.getTimeInMillis());
            result[i] = aDay;
            // move one day forward
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }

    /**
     * Returns the total number of months between two given dates (inclusive)
     * @param fromDate
     * @param toDate
     * @return 
     */
    public static int getNumberOfMonthsBetween(java.util.Date fromDate, java.util.Date toDate) {
        System.out.println("getNumberOfMonthsBetween:" + fromDate + " -> " + toDate);
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.MONTH, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Returns the total number of weeks between two given dates (inclusive)
     * @param fromDate
     * @param toDate
     * @return 
     */
    public static int getNumberOfWeeksBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through weeks, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.WEEK_OF_YEAR, -1);
            result += 1;
        }
        //System.out.println("getNumberOfWeeksBetween:" + fromDate + " -> " + toDate + "=" + result);
        return result;
    }

    /**
     * Returns the total number of days between two given dates (inclusive)
     * @param fromDate
     * @param toDate
     * @return 
     */
    public static int getNumberOfDaysBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            result += 1;
        }
        //System.out.println("getNumberOfWeeksBetween:" + fromDate + " -> " + toDate + "=" + result);
        return result;
    }

    /**
     * Returns the total number of minutes between two given dates (inclusive)
     * @param fromDate
     * @param toDate
     * @return 
     */
    public static int getNumberOfMinutesBetween(java.util.Date fromDate, java.util.Date toDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(toDate);

        // Step through months, generating a database-specific
        // Date format
        while (calendar.getTime().getTime() > fromDate.getTime()) {
            // move one month back in time
            calendar.add(Calendar.MINUTE, -1);
            result += 1;
        }
        return result;
    }

    /**
     * Adds one day to the given date
     */
    public static java.util.Date addOneDay(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        int result = 0;
        calendar.setTime(aDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        return calendar.getTime();
    }

    /**
     * Adds/deducts nbDays to/from the given date
     * @param aDate
     * @param nbDays
     * @return 
     */
    public static java.util.Date addNDays(java.util.Date aDate, int nbDays) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.DAY_OF_YEAR, nbDays);

        return calendar.getTime();
    }

    /**
     * Adds one month to the given date
     * @param aDate
     * @return 
     */
    public static java.util.Date addOneMonth(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.MONTH, 1);

        return calendar.getTime();
    }

    public static java.util.Date addOneYear(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.YEAR, 1);

        return calendar.getTime();
    }

    /**
     * Reverse one month from the given date
     * @param aDate
     * @return 
     */
    public static java.util.Date removeOneMonth(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.MONTH, -1);

        return calendar.getTime();
    }

    /**
     * Reverse one month from the given date
     * @param aDate
     * @return 
     */
    public static java.util.Date removeOneYear(java.util.Date aDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDate);
        calendar.add(Calendar.YEAR, -1);

        return calendar.getTime();
    }

    /**
     * Converts 'month yr' to date, e.g. 'January 2004' -> '2004/01/01'
     * @param stringDate
     * @return 
     */
    public static java.util.Date formatMonthYearToDate(String stringDate) {
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MMMM yyyy");
        try {
            return df.parse(stringDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns true if the two dates are in the same year and the same month
     * @param date1
     * @param date2
     * @return 
     */
    public static boolean inSameMonth(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        if ((calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if the two dates are in the same year and the same week
     * @param date1
     * @param date2
     * @return 
     */
    public static boolean inSameWeek(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        if ((calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.WEEK_OF_YEAR) == calendar2.get(Calendar.WEEK_OF_YEAR))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if the two dates are on the same day
     * @param date1
     * @param date2
     * @return 
     */
    public static boolean onSameDay(java.util.Date date1, java.util.Date date2) {
        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date1);

        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar2.setTime(date2);

        if ((calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR))
                && (calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if refDate is between (inclusive) fromDate and toDate
     * @param refDate
     * @param fromDate
     * @param toDate
     * @return 
     */
    public static boolean betweenDates(java.util.Date refDate, java.util.Date fromDate, java.util.Date toDate) {
        if ((onSameDay(refDate, fromDate)) || (onSameDay(refDate, toDate))) {
            return true;
        }
        if ((refDate.after(fromDate)) && (refDate.before(toDate))) {
            return true;
        }
        return false;
    }

    /**
     * Formats a date to the format 'month yr', e.g. 'January 2004'
     * @param date
     * @return 
     */
    public static String formatDateToMonthYear(java.util.Date date) {
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MMMM yyyy");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd formatted String
     * @param date
     * @return 
     */
    public static String formatDate(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd hh:mm:ss formatted
     * String
     * @param date
     * @return 
     */
    public static String formatDateTime(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd H:m:s");
        return df.format(date);
    }

    /**
     * Converts a date to the Accsys standard yyyy/mm/dd formatted String
     */
    private static String formatTime(java.util.Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("H:m:s:S");
        return df.format(date);
    }

    /**
     * Converts a String representation of a date to a Date type
     * @param date
     * @return 
     */
    public static java.util.Date formatDate(String date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        try {
            return df1.parse(date);
        } catch (ParseException e1) {
            SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                return df2.parse(date);
            } catch (ParseException e2) {
            }
        }
        return null;
    }

    public String getCurrentDateAndTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(new java.util.Date());
    }

    private static String fixSQLFieldValue(String value) {
        if (value == null) {
            return null;
        }
        StringBuilder fixedValue = new StringBuilder((int) (value.length() * 1.1));

        // Get Rid of Double Single Quotes

        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            fixedValue.append(c);
        }
        if (value.compareTo(fixedValue.toString()) != 0) {
            System.out.println("**ORIGINAL STATEMENT:" + value);
            System.out.println("**FIXED STATEMENT:" + fixedValue.toString());
        }
        return fixedValue.toString();
    }
    private static DatabaseObject theObject;
    private static String className;
    private static String url;
    private static String uid;
    private static String pwd;
    private static String altPwd;
    private static boolean debug = false;
    private static za.co.ucs.lwt.initools.IniFile logFile;
}
