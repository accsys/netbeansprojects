/*
 * ConnectionPool.java
 *
 * Created on June 21, 2004, 12:54 PM
 */
package za.co.ucs.lwt.db;

import java.sql.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * The connectionPool is a Singleton managing connections.
 *
 * @author liam
 */
public class ConnectionPool {

    /**
     * Creates a new instance of ConnectionPool
     */
    private ConnectionPool() {
        pooledConnections = new ConcurrentLinkedQueue();
    }

    /**
     * Creates an instance of ConnectionPool, setting the default number of open
     * connections that should be retained at any one time
     */
    private static synchronized void createInstance() {
        if (pool == null) {
            pool = new ConnectionPool();
        }
    }

    /**
     * Get single instance of this class.
     * @return 
     */
    public static ConnectionPool getInstance() {
        if (pool == null) {
            ConnectionPool.createInstance();
        }
        return pool;
    }

    // Returns an unused connection
    public Connection getConnection() {

        ConcurrentLinkedQueue connectionsToDelete = new ConcurrentLinkedQueue();
        Iterator iter = pooledConnections.iterator();
        while (iter.hasNext()) {
            DatedConnection datedCon = ((DatedConnection) iter.next());
            // Is the connection in use
            if (!datedCon.isInUse()) {
                // Has the connection become too old?
                if (datedCon.getConnectionAge() < ConnectionLivenessMinutes) {
                    datedCon.setInUse(true);
                    datedCon.resetConnectionAge();
                    return datedCon.getConnection();
                } else {
                    // This connection is too old now.  We need to destroy it.
                    connectionsToDelete.add(datedCon);
                }
            }
        }

        // Get rid of dated connections
        Iterator iterDel = connectionsToDelete.iterator();
        while (iterDel.hasNext()) {
            pooledConnections.remove(iterDel.next());
        }

        return null;
    }

    // Move the connection from the used to unused list
    public void releaseConnection(Connection connection) {
        Iterator iter = pooledConnections.iterator();
        while (iter.hasNext()) {
            DatedConnection datedCon = ((DatedConnection) iter.next());
            // Is the connection in use
            if (datedCon.getConnection().equals(connection)) {
                datedCon.setInUse(false);
            }
        }
    }

    /*
     * Disconnects and removes the connection from the pool
     * (Used if the connection is no longer valid)
     */
    public void destroyConnection(Connection connection) {
        Iterator iter = pooledConnections.iterator();
        while (iter.hasNext()) {
            DatedConnection datedCon = ((DatedConnection) iter.next());
            // Is the connection in use
            if (datedCon.getConnection().equals(connection)) {
                try {
                    datedCon.getConnection().close();
                } catch (SQLException ex) {
                    // Do nothing
                }
                pooledConnections.remove(datedCon);
                System.out.println("Demoving connection from pool..." + pooledConnections.size());

                return;
            }
        }
    }

    /**
     * Number of open connections not currently in use
     * @return 
     */
    public int getAvailableConnectionCount() {
        int result = 0;
        Iterator iter = pooledConnections.iterator();
        while (iter.hasNext()) {
            DatedConnection datedCon = ((DatedConnection) iter.next());
            // Is the connection in use
            if (datedCon.isInUse()) {
                result++;
            }
        }
        return result;
    }

    /**
     * Total Number of open connections
     * @return 
     */
    public int getTotalConnectionCount() {
        return pooledConnections.size();
    }

    /**
     * Adds the connection to the connection pool
     * @param con
     */
    public void addUsedConnection(Connection con) {
        pooledConnections.add(new DatedConnection(con));
    }
    private static final int ConnectionLivenessMinutes = 5;
    private static ConnectionPool pool = null;
    private static ConcurrentLinkedQueue pooledConnections;
}

/**
 * Internal class that stores a connection with it's creation date.
 */
class DatedConnection {

    // Constructor
    DatedConnection(Connection con) {
        this.con = con;
        this.createdAt = new java.util.Date();
    }

    /**
     * Returns the age of the current ProcessStage (in HOURS)
     */
    public int getConnectionAge() {
        long ms_create = createdAt.getTime();
        long ms_now = new java.util.Date().getTime();
        int age = new Long((ms_now - ms_create) / 1000 / 60).intValue();
        return age;
    }

    public void resetConnectionAge() {
        this.createdAt = new java.util.Date();
    }

    public boolean isInUse() {
        return this.inUse;
    }

    public void setInUse(boolean avalue) {
        this.inUse = avalue;
    }

    public Connection getConnection() {
        return con;
    }
    private final Connection con;
    private java.util.Date createdAt;
    private boolean inUse = false;
}
