/*
 * Container.java
 *
 * Created on October 23, 2003, 12:38 PM
 */

package za.co.ucs.lwt.qar.implementation;
import za.co.ucs.lwt.qar.classes.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

/**
 *
 * @author  liam
 */
public class Container extends Observable {
    
    /** Creates a new instance of Container */
    public Container(File scriptsFolder) {
        mainContainer = new QRBaseContainer();
        buildMainContainerList(scriptsFolder);
    }
    
    /* Populates the mainContainer with the files inside the scriptsFolder */
    private void buildMainContainerList(File scriptsFolder){
        // Does this folder exist?
        if (!scriptsFolder.exists()){
            System.out.println("Illegal scriptsFolder:"+scriptsFolder.toString());
            return;
        }
        
        // Is this File an actual folder?
        if (!scriptsFolder.isDirectory()){
            System.out.println("scriptsFolder:"+scriptsFolder.toString()+" is not a Folder");
            return;
        }
        
        // Get all the files inside this folder with an 'awl' extension
        MyScriptsFilter myFilter = new MyScriptsFilter();
        File[] files = scriptsFolder.listFiles(myFilter);
        
        // For every file, create a script object and throw it into the main container
        for (int I=0; I<files.length;I++){
            String scriptName = files[I].getName();
            String removedExtensionName = scriptName.substring(0, scriptName.toUpperCase().indexOf(".AWL"));
            QRScript script = new QRScript(removedExtensionName, files[I], mainContainer);
            mainContainer.addScript(script);
            System.out.println("Script:"+script.getScriptFile().getName()+" loaded");
        }
        
        // After we've got the scripts in the mainContainer, we need to build the contents of 
        // every script
        Iterator iter = mainContainer.getScripts().values().iterator();
        while (iter.hasNext()){

          ((QRScript)iter.next()).interpretContentsOfFile();
        }
    }
    
    public QRBaseContainer getMainContainer(){
        return mainContainer;
    }
    
    private QRBaseContainer mainContainer;
}

class MyScriptsFilter implements java.io.FilenameFilter{
    public boolean accept(File dir, String name){
        if (name.toUpperCase().endsWith("AWL")==true)
            return true;
        else
            return false;
    }
}