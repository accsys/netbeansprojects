/*
 * QRBase.java
 *
 * Created on October 21, 2003, 8:25 AM
 */

package za.co.ucs.lwt.qar.classes;

/**
 *
 * @author  liam
 */
public class QRBase {
    
    /** Creates a new instance of QRBase */
    public QRBase(String name) {
        this.name = name; 
        this.lineNumber = 0;
    }
    
    public void setName(String name){
        this.name = name; }
    
    public String getName(){
        return this.name; }
    
    public int getLineNumber(){
        return this.lineNumber; }
    
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber; }
    
    public String toString(){
        return (this.getName());
    }    
    private String name;
    private int lineNumber;    
}