/*
 * QRLink.java
 *
 * Created on November 4, 2003, 8:12 AM
 */

package za.co.ucs.lwt.qar.classes;

/**
 *
 * @author  liam
 */
public class QRLink {
    
    /** Creates a new instance of QRLink */
    public QRLink(QRScript from, QRScript to) {
        this.from = from;
        this.to = to;
    }
    
    public QRScript getFrom(){
        return from;
    }
    
    public QRScript getTo(){
        return to;
    }
    
    public String toString(){
        return ("From: "+(from.toString())+" - To: "+(to.toString()));
    }
    
    private QRScript from;
    private QRScript to;
}
