/*
 * ScriptImage.java
 *
 * Created on November 4, 2003, 8:10 AM
 */

package za.co.ucs.lwt.qar.classes;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.awt.print.*;


/**
 *
 * @author  liam
 */
public class ScriptImage extends JPanel implements Printable{
    
    /** Creates a new instance of ScriptImage */
    public ScriptImage(LinkedList links) {
        super();
        this.links = links;
        
        scriptsPerLevel = getScriptsPerLevel();
        scriptsOnScreen = new LinkedList();
        // Let us add a mouse Listener
        scrptImgMouseListener = new ScriptImageMouseListener();
        this.addMouseListener(scrptImgMouseListener);
    }
    
    public ScriptImageMouseListener getMouseListener(){
        return scrptImgMouseListener;
    }
    
    public void paint(Graphics g){
        
        g.setFont(new Font( g.getFont().getFontName(), Font.PLAIN, 11));
        Dimension d = getSize();
        nbScriptsWide = getLevelCount();
        nbScriptsHigh = getMaxLevelHeight();
        screenWidth = d.width-xOffset;
        screenHeight = d.height-yOffset;
        
        xSpacing = screenWidth / (nbScriptsWide+1);
        ySpacing = screenHeight / (nbScriptsHigh+1);
        
        xOffset = xSpacing/4;
        yOffset = ySpacing/2;
        
        // If no links were defined, we cannot draw anything
        if (links.size()==0){
            return; }
        
        // Clear the LinkedList that stores the script positions
        scriptsOnScreen.clear();
        
        // Draw Base Level Script
        QRScript base = getBaseScript();
        drawScript(base, xOffset, (screenHeight/2) - ySpacing, g);
        
    }
    
    private boolean isOverlappingAnotherScript(QRScript script, int x, int y){
        for (int i = 0; i<scriptsOnScreen.size(); i++){
            if ( (((ScriptPosition)scriptsOnScreen.get(i)).getScript() != script) &&
            ((ScriptPosition)scriptsOnScreen.get(i)).inLine(x, y)){
                return true;
            }
        }
        return false;
    }
    
    private void drawScript(QRScript callerScript, int x, int y, Graphics g){
        int textWidth = g.getFontMetrics().stringWidth(callerScript.toString());
        int textHeight = g.getFontMetrics().getHeight();
        
        // Place this script in the LinkedList that stores the scripts and their positions
        scriptsOnScreen.add(new ScriptPosition(callerScript, x, y, textWidth+6, textHeight+4));
        
        // Draw the script
        g.setColor(Color.black);
        g.draw3DRect(x, y,textWidth+6, textHeight+4, false);
        g.setColor(Color.yellow);
        g.fill3DRect(x+1, y+1,textWidth+5, textHeight+3, true);
        g.setColor(Color.black);
        g.drawString(callerScript.toString(), x+2, y+textHeight-1);
        
        // Now draw the rest of the scripts
        LinkedList calledScripts = getLinkedToScripts(callerScript);
        for (int i=0; i<calledScripts.size(); i++){
            QRLink calledLink = (QRLink)calledScripts.get(i);
            if (calledLink == null){
                continue;
            }
            QRScript nextScript = calledLink.getTo();
            int scriptLevel = getScriptLevel(nextScript);
            if (scriptLevel == 0){
                System.out.println("-------------- ScriptLevel = 0 ----------");
                System.out.println("-------------- for "+nextScript+" ----------");
            }
            
            int newX = xOffset+((scriptLevel)*xSpacing);
            int scriptPosInList = ((LinkedList)(scriptsPerLevel.get(new Integer(scriptLevel)))).indexOf(nextScript);
            int newY = yOffset+(scriptPosInList+1)*screenHeight/(getLevelHeight(scriptLevel)+1)-ySpacing/2;
            // Adjust the position of this script not to overlap with another
            if (isOverlappingAnotherScript(nextScript, newX, newY)){
                newY += ySpacing/4;
            }
            // Now that we know where the new script is going to be drawn, we can
            // draw the link as well
            drawLink(x+textWidth+6, y+(textHeight+4)/2, newX, newY+(textHeight+4)/2, g);
            
            drawScript(nextScript, newX , newY, g);
        }
    }
    
    private void drawLink(int x1, int y1, int x2, int y2, Graphics g){
        g.setColor(Color.red);
        int midX = x1+(x2-x1)/2;
        //        g.drawLine(x1, y1, midX, y1);
        //        g.drawLine(midX, y1, midX, y2);
        //        g.drawLine(midX, y2, x2, y2);
        g.drawLine(x1, y1, x2, y2);
    }
    
    // Returns the number of scripts that are linked to this script;
    private int getLinkedToScriptsCount(QRScript script){
        return (getLinkedToScripts(script).size());
    }
    
    // Returns the number of scripts that are linked to this script;
    private int getLinkedFromScriptsCount(QRScript script){
        return (getLinkedFromScripts(script).size());
    }
    
    // Returns the links where 'script' is on the left side
    private LinkedList getLinkedToScripts(QRScript script){
        LinkedList list = new LinkedList();
        for (int I=0; I<links.size(); I++){
            if (((QRLink)links.get(I)).getFrom() == script){
                list.add((QRLink)links.get(I));
            }
        }
        return list;
    }
    
    // Returns the links where 'script' is on the right side
    private LinkedList getLinkedFromScripts(QRScript script){
        LinkedList list = new LinkedList();
        for (int I=0; I<links.size(); I++){
            if (((QRLink)links.get(I)).getTo() == script){
                list.add((QRLink)links.get(I));
            }
        }
        return list;
    }
    
    // Returns the script that are the 'Base' of all the other scripts
    private QRScript getBaseScript(){
        QRScript script = null;
        for (int I=0; I<links.size(); I++){
            if (getLinkedFromScripts(((QRLink)links.get(I)).getFrom()).size()==0){
                script = ((QRLink)links.get(I)).getFrom();
            }
        }
        return script;
    }
    
    private TreeMap getScriptsPerLevel(){
        QRScript baseScript = getBaseScript();
        LinkedList list = new LinkedList();
        TreeMap levels = new TreeMap();
        // Add the starting point
        list.add(baseScript);
        levels.put(new Integer(0), list);
        LinkedList currentLevel;
        int i=1;
        do{
            // Get all the scripts called by the scripts in the previous level
            currentLevel = new LinkedList();
            LinkedList previousLevel = (LinkedList)levels.get(new Integer(i-1));
            ListIterator previousLevelIterator = previousLevel.listIterator();
            while (previousLevelIterator.hasNext()){
                QRScript callerScript = (QRScript)previousLevelIterator.next();
                if (callerScript == null){
                    continue;
                }
                LinkedList calledScripts = getLinkedToScripts(callerScript);
                ListIterator calledScriptsIterator = calledScripts.listIterator();
                while (calledScriptsIterator.hasNext()){
                    QRLink calledLink = (QRLink)calledScriptsIterator.next();
                    QRScript calledScript = calledLink.getTo();
                    if ( treeMapContains(levels, calledScript)){
                        int existsIn = levelOfTreeMapThatContains(levels, calledScript);
                        if (existsIn >=0){
                            // Remove from lower level
                            removeFromTreeMap(levels, calledScript, existsIn);
                        }
                    }
                    if (currentLevel.indexOf(calledScript)==-1){
                        currentLevel.add(calledScript);
                    }
                }
            }
            
            // Add a lew 'level' into the TreeMap
            if (currentLevel.size()>0){
                levels.put(new Integer(i), currentLevel);
                i++;
            }
        } while (currentLevel.size()>0);
        
        return (levels);
    }
    
    private boolean treeMapContains(TreeMap levels, QRScript script){
        for (int a=levels.size()-1; a>=0 ; a--){
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            if (currentLevel.contains(script)){
                return true;
            }
        }
        return false;
    }
    
    private int levelOfTreeMapThatContains(TreeMap levels, QRScript script){
        for (int a=levels.size()-1; a>=0 ; a--){
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            if (currentLevel.contains(script)){
                return a;
            }
        }
        return -1;
    }
    
    private void removeFromTreeMap(TreeMap levels, QRScript script, int level){
        LinkedList currentLevel = (LinkedList)levels.get(new Integer(level));
        currentLevel.set(currentLevel.indexOf(script), null);
        //        currentLevel.remove(script);
    }
    
    private void printAll(TreeMap levels){
        System.out.println("////////////////////");
        for (int a=levels.size()-1; a>=0 ; a--){
            System.out.println("Level: "+a);
            LinkedList currentLevel = (LinkedList)levels.get(new Integer(a));
            for (int b=0; b<currentLevel.size(); b++){
                QRScript printScript = (QRScript)currentLevel.get(b);
                System.out.println("  Script: "+b+" = "+printScript.toString());
            }
        }
    }
    
    // How many levels does this thing go?
    private int getLevelCount(){
        return scriptsPerLevel.size();
    }
    
    // How many scripts are on this level?
    private int getLevelHeight(int level){
        return ((((LinkedList)scriptsPerLevel.get(new Integer(level)))).size());
    }
    
    /** Maximum number of scripts on a singe level.
     * This will determine the available space we've got for
     * drawing our graphs
     */
    private int getMaxLevelHeight(){
        int maxHeight = 0;
        for (int I=0; I<scriptsPerLevel.size(); I++){
            if (((LinkedList)scriptsPerLevel.get(new Integer(I))).size()>maxHeight){
                maxHeight = ((LinkedList)scriptsPerLevel.get(new Integer(I))).size();
            }
        }
        return maxHeight;
    }
    
    /** Returns the level on which this script finds itself
     * It will always start at at least the next level from the one where
     * the caller script is
     */
    private int getScriptLevel(QRScript script){
        for (int I=0; I<scriptsPerLevel.size() ; I++){
            LinkedList scriptsOnThisLevel = ((LinkedList)scriptsPerLevel.get(new Integer(I)));
            for (int J=0; J<scriptsOnThisLevel.size(); J++){
                if (((QRScript)scriptsOnThisLevel.get(J))  == script){
                    return I;
                }
            }
        }
        return 0;
    }
    
    public LinkedList getScriptCoordinates(){
        return scriptsOnScreen;
    }
    
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        
        Graphics2D g2 = (Graphics2D) graphics;
        g2.translate(pageFormat.getImageableX()+10, pageFormat.getImageableY()+10);
        //g2.translate(5, 5);
        paint(g2);
        return Printable.PAGE_EXISTS;
    }
    
    private int nbScriptsWide;
    private int nbScriptsHigh;
    private int xOffset;
    private int yOffset;
    private int screenWidth;
    private int screenHeight;
    private int xSpacing;
    private int ySpacing;
    private LinkedList links;
    private TreeMap scriptsPerLevel;
    private LinkedList scriptsOnScreen;
    private ScriptImageMouseListener scrptImgMouseListener = null;

    
}


