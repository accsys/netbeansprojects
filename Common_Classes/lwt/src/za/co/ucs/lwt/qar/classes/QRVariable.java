/*
 * QRVariable.java
 *
 * Created on October 21, 2003, 12:59 PM
 */

package za.co.ucs.lwt.qar.classes;

/**
 *
 * @author  liam
 */
public class QRVariable extends QRBase {
    
    /** Creates a new instance of QRVariable */
    public QRVariable(String name) {
        super(name);
        this.type = "Local";
    }
    
    public String getType(){
        return this.type; }
    
    public void setType(String type){
        this.type = type; }
    
    private String type;
    
}


