/*
 * QRBaseContainer.java
 *
 * Created on October 21, 2003, 1:30 PM
 */

package za.co.ucs.lwt.qar.classes;
import java.util.*;

/**
 *
 * @author  liam
 */
public class QRBaseContainer {
    
    /** Creates a new instance of QRBaseContainer */
    public QRBaseContainer() {
        qrScripts = new TreeMap();
        qrObjects = new TreeMap();
        qrVariables = new TreeMap();
        qrChecks = new TreeMap();
        qrEvents = new TreeMap();
        qrScriptSequence = new TreeMap();
    }
    
    public TreeMap getScripts(){
        return qrScripts;
    }
    public TreeMap getObjects(){
        return qrObjects;
    }
    public TreeMap getVariables(){
        return qrVariables;
    }
    public TreeMap getChecks(){
        return qrChecks;
    }
    public TreeMap getEvents(){
        return qrEvents;
    }
    
    public QRScript getScript(String name){
        return (QRScript)qrScripts.get(name.toUpperCase());
    }
    public QRScript getScript(int index){
        return (QRScript)qrScriptSequence.get(new Integer(index));
    }
    
    
    public QRObject getObject(String name){
        return (QRObject)qrObjects.get(name.toUpperCase());
    }
    public QRVariable getVariable(String name){
        return (QRVariable)qrVariables.get(name.toUpperCase());
    }
    public QRCheck getCheck(String name){
        return (QRCheck)qrChecks.get(name.toUpperCase());
    }
    public QREvent getEvent(String name){
        return (QREvent)qrEvents.get(name.toUpperCase());
    }
    
    public void addScript(QRScript script){
        qrScripts.put(script.getName().toUpperCase(), script);
        qrScriptSequence.put(new Integer(qrScripts.size()), script);
    }
    
    public void addObject(QRObject object){
        qrObjects.put(object.getName().toUpperCase(), object); }
    public void addVariable(QRVariable variable){
        qrVariables.put(variable.getName().toUpperCase(), variable); }
    public void addCheck(QRCheck check){
        qrChecks.put(check.getName().toUpperCase(), check); }
    public void addEvent(QREvent event){
        qrEvents.put(event.getName().toUpperCase(), event); }
    
    private TreeMap qrScripts;
    private TreeMap qrScriptSequence;
    private TreeMap qrObjects;
    private TreeMap qrVariables;
    private TreeMap qrChecks;
    private TreeMap qrEvents;
}
