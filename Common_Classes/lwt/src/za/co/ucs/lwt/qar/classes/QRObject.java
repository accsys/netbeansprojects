/*
 * QRObject.java
 *
 * Created on October 21, 2003, 12:57 PM
 */

package za.co.ucs.lwt.qar.classes;

/**
 *
 * @author  liam
 */
public class QRObject extends QRBase {
    
    /** Creates a new instance of QRObject */
    public QRObject(String name) {
        super(name);
    }
    
    public String toString(){
        return (this.getName());
    }
    
}
