/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.lwt.codecs;

/**
 * Utilities class for doing tedious string work.
 * <p>
 * Come basic encoding methods as well as some parsing type methods are
 * contained herein. There are only two methods here that are specific to the
 * iSQL-Viewer system and they are easily seen as they iSQL in the method
 * signatures.
 * <p>
 * This class provides most methods that have a parse and or format/encode
 * functionality.
 *
 * @author Markus A. Kobold &lt;mkobold at sprintpcs dot com&gt;
 * @version 1.0
 */
public abstract class StringUtilities {

    private static char toHex(int nibble) {

        return hexDigit[(nibble & 0xF)];
    }

    /**
     * Method for decoding ascii escaped character sequences for UNICODE
     * characters.
     * <p>
     * This will decode text encoded in a fashion similar to the Java Properties
     * encoding mechanisim. Such that \\uxxxx are converted to the proper
     * characters.
     * <p>
     * In fact most of this code came from that private method contained there.
     *
     * @see #encode(String, boolean, boolean)
     * @param str to decode ASCII escaped character sequences from.
     * @return decoded string.
     */
    public static String decode(String str) {

        char aChar;
        int len = str.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len;) {
            aChar = str.charAt(x++);
            if (aChar == '\\') {
                aChar = str.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = str.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't') {
                        aChar = '\t';
                    } else if (aChar == 'r') {
                        aChar = '\r';
                    } else if (aChar == 'n') {
                        aChar = '\n';
                    } else if (aChar == 'f') {
                        aChar = '\f';
                    }
                    outBuffer.append(aChar);
                }
            } else {
                outBuffer.append(aChar);
            }
        }
        return outBuffer.toString();
    }

    /**
     * Encodes method to two resultant types.
     * <p>
     * This method can encode a given text into HTML or text that is very
     * similar to that encoding scheme seen in the Java Properties object using
     * \\uxxxx for non-printable characters
     * <p>
     * If encoding into HTML various entities will be encoded for proper HTML
     * formatting.
     *
     * @param theString to be encoded
     * @param asHTML encode using HTML logic, other while Properties type logic.
     * @param escapeSpaces signals to show spaces with &amp;nbsp; or '\ '
     * @return encoded string.
     */
    public static String encode(String theString, boolean asHTML, boolean escapeSpaces) {

        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len * 2);
        for (int x = 0; x < len; x++) {
            char aChar = theString.charAt(x);
            switch (aChar) {
                case ' ':
                    if (asHTML) {
                        outBuffer.append("&nbsp;");
                    } else {
                        if (escapeSpaces) {
                            outBuffer.append('\\');
                        }
                        outBuffer.append(' ');
                    }
                    break;
                case '\\':
                    outBuffer.append('\\');
                    break;
                case '\t':
                    if (asHTML) {
                        outBuffer.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    } else {
                        outBuffer.append('\\');
                        outBuffer.append('t');
                    }
                    break;
                case '\"':
                    if (asHTML) {
                        outBuffer.append("&quot;");
                    } else {
                        outBuffer.append('\"');
                    }
                    break;
                case '>':
                    if (asHTML) {
                        outBuffer.append("&gt;");
                    } else {
                        outBuffer.append('>');
                    }
                    break;
                case '<':
                    if (asHTML) {
                        outBuffer.append("&lt;");
                    } else {
                        outBuffer.append('<');
                    }
                    break;
                case '\n':
                    if (asHTML) {
                        outBuffer.append("<BR>");
                    } else {
                        outBuffer.append('\\');
                        outBuffer.append('n');
                    }
                    break;
                case '\r':
                    if (asHTML) {
                        outBuffer.append("<BR>");
                    } else {
                        outBuffer.append('\\');
                        outBuffer.append('r');
                    }
                    break;
                case '\f':
                    outBuffer.append('\\');
                    outBuffer.append('f');
                    break;
                default:
                    if ((aChar < 0x0020) || (aChar > 0x007e)) {
                        if (asHTML) {
                            outBuffer.append('&');
                            outBuffer.append('#');
                        } else {
                            outBuffer.append('\\');
                            outBuffer.append('u');
                        }
                        outBuffer.append(toHex((aChar >> 12) & 0xF));
                        outBuffer.append(toHex((aChar >> 8) & 0xF));
                        outBuffer.append(toHex((aChar >> 4) & 0xF));
                        outBuffer.append(toHex(aChar & 0xF));
                    } else {
                        outBuffer.append(aChar);
                    }
            }
        }
        return outBuffer.toString();
    }
    /** A table of hex digits */
    private static final char[] hexDigit = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
}