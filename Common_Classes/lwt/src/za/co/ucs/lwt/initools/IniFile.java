package za.co.ucs.lwt.initools;
import java .util.*;
import java.io.*;

//-------------------------------------------
/**this class handles Windows style ini-files of the form
 * [paragraphname]
 * key1=value1
 * key2=value2
 * [another_paragraphname]
 * keyx = vals
 * tag=foo
 * etc.
 * it will, however, work on any platform
 */
public class IniFile {
    private String filename, path, fullpath;
    private Hashtable paragraphs;
    private iniParagraph para;
    private InputFile f;
    //-----------------------------------------------------
    /**Opens an existing iniFile
     * If it does not exist, an empty one is created and opened
     * @param - pathname
     * @param filename, usually xxx.ini
     */
    public IniFile(String pathname, String fname) throws IOException {
        filename = fname;
        path = pathname;
        if (path.length()==0)
            fullpath= filename;
        else
            fullpath = path +File.separator + filename;
        checkFile(fullpath);
        f = new InputFile(fullpath);
        getParagraphs();
    }
    //-----------------------------------------------------
    private void checkFile(String fname) throws IOException {
        
        File fl = new File(fname);
        if(! fl.exists()) {
            OutputFile outf = new OutputFile(fname);
            outf.close();
        }
    }
    //-----------------------------------------------------
    /**Opens an ini file in the current directory
     * @param - filename, usually xxx.ini
     */
    public IniFile(String fname) throws IOException {
        filename = fname;
        path ="";
        fullpath = filename;
        checkFile(fullpath);
        f = new InputFile(filename);
        getParagraphs();
    }
    //-----------------------------------------------------
    private synchronized void getParagraphs() {
        paragraphs = new Hashtable(10);        //make room for object table
        if (! f.checkErr()) {
            para = new iniParagraph(f);
            
            if (! para.error()) {
                paragraphs.put(para.getName(), para);
                while ((para.nextLine()!=null)&&(para.nextLine().length() > 0)) {
                    para = new iniParagraph(f, para.nextLine());
                    paragraphs.put(para.getName(), para);
                }
            }
            f.close();
        }
    }
    //----------------------------------------------------
    private synchronized void putParagraphs() throws IOException {
        OutputFile f = new OutputFile(fullpath,false);
        Enumeration enumr = paragraphs.elements();
        while(enumr.hasMoreElements())   {
            iniParagraph p = (iniParagraph)enumr.nextElement();
            f.println("["+ p.getName() + "]");
            
            
            Enumeration e = p.getTags();
            while (e.hasMoreElements()) {
                String tag = (String)e.nextElement();
                f.println(tag.toLowerCase()+"="+p.tagValue(tag));
            }
        }
        f.close();
    }
    //-----------------------------------------------------
    /**Gets a profile string from an ini file
     * @param para -  paragraph name in any case
     * @param entry - name of entry in any case
     * @return String value of this entry
     */
    public String getProfile(String para, String entry) {
        iniParagraph ini = null;
        int i =0;
        boolean found = false;
        ini = (iniParagraph)paragraphs.get(para.toLowerCase());
        if (ini != null)  //now look for entry in that paragraph
            return ini.tagValue(entry).trim();
        else
            return "";
    }
    //-----------------------------------------------------
    /**Gets a profile string from an ini file
     * returns default value if no such entry exists
     * @param para -  paragraph name in any case pattern
     * @param entry - name of entry in any case pattern
     * @param default - default value for parameter
     * @return String value of this entry
     */
    
    public String getProfile(String para, String entry, String defalt) {
        String value = getProfile(para, entry);
        if (value.length()<1){
            value = defalt;
            // Having done this, why not put the default into the file?
            try{
                System.out.println("putProfile("+para+","+  entry+", "+defalt+")");
                putProfile(para, entry, defalt);
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
        
        return value.trim();
        
    }
    //-----------------------------------------------------
    /**Puts a profile entry into the ini file
     * @param para -paragraph name
     * @param tag - name of entry
     * @param value - value of entry
     */
    public void putProfile(String para, String tag, String value) throws IOException {
        int i = 0;
        boolean found =false;
        iniParagraph p = null;
        Enumeration enumr = paragraphs.elements();
        while (enumr.hasMoreElements() && ! found) {
            p =(iniParagraph)enumr.nextElement();
            found =(para.equalsIgnoreCase( p.getName()));
        }
        if (found) {
            p.setValue(tag, value);
        } else {
            p = new iniParagraph(para);
            paragraphs.put(p.getName(), p);
            p.setValue(tag.toLowerCase(), value);
        }
        // Delete and rewrite entire ini0file
        
        putParagraphs();    //rewrite entire ini-file
    }
}