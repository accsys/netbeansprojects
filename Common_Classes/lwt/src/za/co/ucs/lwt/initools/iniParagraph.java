package za.co.ucs.lwt.initools;
import java .util.*;
import java.io.*;
/**A private class used by IniFile*/
public class iniParagraph {
    private String paragraph_name;
    private InputFile f;
    private Hashtable tags;
    private String line;
    private boolean error_flag;
    private boolean eof;
    private int index = -1;
    //----------------------------------
    public iniParagraph(InputFile fl) {
        f = fl;
        eof = false;
        String s = getNextLine();
        read_to_nextparagraph(s);
    }
    //----------------------------------
    public Enumeration getTags() {
        return tags.keys();
    }
    public Hashtable getHash() {
        return tags;
    }
    //----------------------------------
    public void setValue(String tag, String value) {
        tags.put(tag.toLowerCase(), value);  //may replace old value
        Enumeration e = tags.elements();
    }
    //----------------------------------
    private void read_to_nextparagraph(String s) {
        error_flag = false;
        tags = new Hashtable();        
        while ((s != null) && (! s.startsWith("[")))
            s = getNextLine();

        //remove brackets
        if (s != null) {
            s = s.substring(1); //all but first char
            int i = s.indexOf("]");
            if (i > 0) {
                s = s.substring(0, i);
                store_paragraph(s);
            } else {
                error_flag = true;
            }
        } else {
            eof = true;        
            error_flag = false;
        }
    }
    //----------------------------------
    public String getName() {
        if (paragraph_name != null){
        return paragraph_name.toLowerCase();
        }
        else {
            return (new String(""));
        }
    }
    //----------------------------------
    public String tagValue(String tagname) {
        String ans;

        ans = (String)tags.get(tagname.toLowerCase());
        if (ans == null)
            return "";
        else
            return ans;
    }
    //----------------------------------
    private void store_paragraph(String s) {
        String next;
        iniElement ini;
        paragraph_name = s;
        next = getNextLine();
        while ((next != null) && (! next.startsWith("["))) {
            ini = new iniElement(next);
            if (! ini.error()) {
            System.out.println(ini.tagName ()+" "+ini.valueName ());
                tags.put(ini.tagName(), ini.valueName());
            }
            next = getNextLine();  
        }  
        if (tags.size()>0)
            error_flag = false;
    }     
    //----------------------------------
    public iniParagraph(InputFile fl, String pname) {
        f = fl;
        read_to_nextparagraph(pname);
    }
    //----------------------------------
    public iniParagraph(String pname)  {
        //call this constructor to create a new paragraph
        tags = new Hashtable();
        paragraph_name = pname;
    }
    //----------------------------------
    private String getNextLine() {
        line = f.readLine();
        while ((line != null) && (line.length() <1))
            line = f.readLine();
        if (line == null)
            error_flag = true;
        else
            line = line.trim();
        return line;    
    }
    //----------------------------------
    public boolean error() {
        return error_flag;
    }
    //----------------------------------
    public String nextLine() {
        return line;
    }
}

