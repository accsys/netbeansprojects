
package za.co.ucs.lwt.e_mail;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

/** Business layer of the Notification entitity.
 * <P>
 * @author Liam Terblanche
 * Note: By its nature, Notifications need only be created.  The saving and handling
 *      of the object should thereafter be done automatically
 */
public class Notification{
    
    /** Generates an e-mail message
     * @param smtpServer
     * @param smtpTo
     * @param smtpFrom
     * @param smtpCC
     * @param subject
     * @param body
     */
    public boolean send(String smtpServer, String smtpAuthUser, String smtpAuthPwd, String smtpTo, String smtpFrom, String smtpCC, String subject, String body){
        try{
            Properties props = new Properties();
            
            // Do I have a recipient?
            if (smtpTo.trim().length()==0){
                System.out.println("Notification Failure: Undefined recipient for Message...");
                System.out.println("Subject:\""+subject+"\"");
                System.out.println("Body\""+body+"\"");
                return false;
            }
            // Try to attach to a default session
            //        props.put("mail.smtp.host", smtpServer);
            
            props.put("mail.smtp.host", smtpServer);
            System.out.println("SMTP Server:"+smtpServer);
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(false);
            
            //Create a new message
            MimeMessage msg = new  MimeMessage(session);

            
            // Set FROM and TO

            msg.setFrom(new InternetAddress(smtpFrom));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(smtpTo, false));
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(smtpCC, false));
            // Set Subject and Body
            msg.setSubject(subject);
            msg.setText(body);
            msg.setContent( body, "text/html");             
            Transport.send(msg);
            
            return true;
        }
        catch( Exception ex ){
            ex.printStackTrace();
            return false;
        }
    }
    
    
}



