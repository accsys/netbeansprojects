package za.co.ucs.accsys.tools;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.xml.bind.DatatypeConverter;

public class StringEncrypterSingleton implements java.io.Serializable {

    static final long serialVersionUID = 8717721841451731300L;
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    public static final String HEX_ENCRYPTION_SCHEME = "Hex";
    public static final String DES_ENCRYPTION_SCHEME = "DES";
    public static final String DEFAULT_ENCRYPTION_KEY = "This is a fairly long phrase used to encrypt";
    public static final String DECRYPTION_ERROR_MESSAGE = "Unable to decrypt value";
    private static KeySpec keySpec;
    private static SecretKeyFactory keyFactory;
    private static Cipher cipher;
    private static StringEncrypterSingleton encrypter;
    private static final String UNICODE_FORMAT = "UTF-8";
    /**
     * It became important to identify non-ASCII characters. As our
     * encryption/decryption does not cater for it, I've decided to - in the
     * event of a non-standard character - not encrypt but rather flag the
     * string so that when I perform the decryption, this string gets returned
     * AS-IS
     */
    static String specialCharacterFlag = "SPECIALCHAR";
    //static CharsetEncoder asciiEncoder =
    //        Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

    private synchronized static boolean isPureAscii(String v) {
        return Charset.forName("US-ASCII").newEncoder().canEncode(v);
    }

    public static StringEncrypterSingleton getInstance(String encryptionScheme) {
        if (encrypter == null) {
            try {
                encrypter = new StringEncrypterSingleton(encryptionScheme, DEFAULT_ENCRYPTION_KEY);
            } catch (EncryptionException e) {
            }
        }
        return encrypter;
    }

    public static StringEncrypterSingleton getInstance(String encryptionScheme, String encryptionKey) {
        if (encrypter == null) {
            try {
                encrypter = new StringEncrypterSingleton(encryptionScheme, encryptionKey);
            } catch (EncryptionException e) {
            }
        }
        return encrypter;
    }

    private StringEncrypterSingleton(String encryptionScheme) throws EncryptionException {
        this(encryptionScheme, DEFAULT_ENCRYPTION_KEY);
    }

    private StringEncrypterSingleton(String encryptionScheme, String encryptionKey)
            throws EncryptionException {

        if (encryptionKey == null) {
            throw new IllegalArgumentException("encryption key was null");
        }
        if (encryptionKey.trim().length() < 24) {
            throw new IllegalArgumentException(
                    "encryption key was less than 24 characters");
        }

        try {
            byte[] keyAsBytes = encryptionKey.getBytes(UNICODE_FORMAT);

            if (encryptionScheme.equals(DESEDE_ENCRYPTION_SCHEME)) {
                keySpec = new DESedeKeySpec(keyAsBytes);
            } else if (encryptionScheme.equals(DES_ENCRYPTION_SCHEME)) {
                keySpec = new DESKeySpec(keyAsBytes);
            } else if (encryptionScheme.equals(HEX_ENCRYPTION_SCHEME)) {
                keySpec = new DESedeKeySpec(keyAsBytes);
            } else {
                throw new IllegalArgumentException("Encryption scheme not supported: " + encryptionScheme);
            }

            keyFactory = SecretKeyFactory.getInstance(encryptionScheme);
            cipher = Cipher.getInstance(encryptionScheme);

        } catch (InvalidKeyException e) {
            throw new EncryptionException(e);
        } catch (UnsupportedEncodingException e) {
            throw new EncryptionException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new EncryptionException(e);
        } catch (NoSuchPaddingException e) {
            throw new EncryptionException(e);
        }

    }

    public synchronized String encrypt(String unencryptedString) throws EncryptionException {
        if (unencryptedString == null || unencryptedString.trim().length() == 0) {
            return "";
        }

        // Strings that contains non-standard ASCII characters, are not encrypted, but simply gets a special string appended at the end
        if (!isPureAscii(unencryptedString)) {
            return unencryptedString + specialCharacterFlag; // Append Bullet character to the end
        }

        try {
            SecretKey key = keyFactory.generateSecret(keySpec);
            byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] ciphertext;
            synchronized(cipher){
                cipher.init(Cipher.ENCRYPT_MODE, key);
                ciphertext = cipher.doFinal(cleartext);
            }

            return DatatypeConverter.printBase64Binary(ciphertext);
        } catch (Exception e) {
            System.out.println("Decryption Exception:"+e.getMessage());
            return "Unable to Encrypt";
        }
    }

    public synchronized String decrypt(String encryptedString) throws EncryptionException {
        if (encryptedString == null || encryptedString.trim().length() <= 0) {
            return "";
        }

        // Strings that contained non-standard ASCII characters, were not encrypted, but simply had a special string appended at the end
        if (encryptedString.endsWith(specialCharacterFlag)) {
            return encryptedString.substring(0, encryptedString.length() - specialCharacterFlag.length());
        }

        try {
            SecretKey key = keyFactory.generateSecret(keySpec);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] cleartext = DatatypeConverter.parseBase64Binary(encryptedString);
            byte[] ciphertext = cipher.doFinal(cleartext);
            return bytes2String(ciphertext);
        } catch (Exception e) {
            System.out.println("Decryption Exception:"+e.getMessage());
            return "Unable to Decrypt";
        }
    }

    private static String bytes2String(byte[] bytes) {
        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            stringBuffer.append((char) bytes[i]);
        }
        return stringBuffer.toString();
    }

    public static class EncryptionException extends Exception {

        public EncryptionException(Throwable t) {
            super(t);
        }
    }
}
