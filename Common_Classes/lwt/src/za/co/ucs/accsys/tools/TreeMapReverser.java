/*
 * TreeMapReverser.java
 *
 * Created on 02 February 2005, 08:27
 */

package za.co.ucs.accsys.tools;


import java.util.*;

/**
 * This class reverses the content of a TreeMap
 * @author  lwt
 */
public class TreeMapReverser {
    
    /** Creates a new instance of TreeMapReverser */
    public TreeMapReverser() {
    }
    
    /**
     * Returns a LinkedList with the objects in aTreeMap positioned in reverse order
     */
    public LinkedList reverse(TreeMap aTreeMap){
       LinkedList rslt = new LinkedList();
       Set originalKeys = aTreeMap.keySet();

       Iterator iter = originalKeys.iterator();
       while (iter.hasNext()){
           Object key = iter.next();
           Object mapItem = aTreeMap.get(key);
           // Inserts the latest object in front of the others
           rslt.add(0, mapItem);
       }
       
       return rslt;
    }
    
}
