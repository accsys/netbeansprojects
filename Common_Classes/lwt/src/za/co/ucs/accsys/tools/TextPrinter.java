/*
 * TextPrinter.java
 *
 * Created on 26 April 2005, 09:10
 */

package za.co.ucs.accsys.tools;
import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.print.*;
import java.text.*;
import java.util.*;

/**
 * A simple Text Printer class.
 * @author  lwt
 */
public class TextPrinter implements Printable  {
    
    /**
     * Our text in a form for which we can obtain a
     * AttributedCharacterIterator.
     */
    private AttributedString mStyledText;
    
    
    /** Creates a new instance of TextPrinter */
    public TextPrinter(StringBuffer textToPrint) {
        this.textToPrint = textToPrint;
    }
    
    public void print(){
        mStyledText = new AttributedString(textToPrint.toString());
        
        /* Get the representation of the current printer and
         * the current print job.
         */
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        
        /* Build a book containing pairs of page painters (Printables)
         * and PageFormats. This example has a single page containing
         * text.
         */
        Book book = new Book();
        book.append(this, new PageFormat());
        
        /* Set the object to be printed (the Book) into the PrinterJob.
         * Doing this before bringing up the print dialog allows the
         * print dialog to correctly display the page range to be printed
         * and to dissallow any print settings not appropriate for the
         * pages to be printed.
         */
        printerJob.setPageable(book);
        
        /* Show the print dialog to the user. This is an optional step
         * and need not be done if the application wants to perform
         * 'quiet' printing. If the user cancels the print dialog then false
         * is returned. If true is returned we go ahead and print.
         */
        boolean doPrint = printerJob.printDialog();
        if (doPrint) {
            
            
            try {
                
                printerJob.print();
                
            } catch (PrinterException exception) {
                
                System.err.println("Printing error: " + exception);
                
            }
            
        }
        
    }
    
    
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
    /* We'll assume that Jav2D is available.
     */
        Graphics2D g2d = (Graphics2D) graphics;
        
        /* Move the origin from the corner of the Paper to the corner
         * of the imageable area.
         */
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        
        /* Set the text color.
         */
        g2d.setPaint(Color.black);
        
        /* Use a LineBreakMeasurer instance to break our text into
         * lines that fit the imageable area of the page.
         */
        Point2D.Float pen = new Point2D.Float();
        AttributedCharacterIterator charIterator = mStyledText.getIterator();
        LineBreakMeasurer measurer = new LineBreakMeasurer(charIterator, g2d.getFontRenderContext());
        float wrappingWidth = (float) pageFormat.getImageableWidth();
        
        
        while (measurer.getPosition() < charIterator.getEndIndex()) {
            
            TextLayout layout = measurer.nextLayout(wrappingWidth);
            pen.y += layout.getAscent();
            float dx = layout.isLeftToRight()? 0 : (wrappingWidth - layout.getAdvance());
            
            layout.draw(g2d, pen.x + dx, pen.y);
            pen.y += layout.getDescent() + layout.getLeading();
            
        }
        return Printable.PAGE_EXISTS;
        
    }
    
    
    StringBuffer textToPrint;
    
}
