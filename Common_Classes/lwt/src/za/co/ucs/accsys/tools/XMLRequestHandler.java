/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.accsys.tools;

import java.io.StringReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author fkleynhans
 */
public class XMLRequestHandler {
    private static XMLRequestHandler handler;
    
    public static XMLRequestHandler getInstance(){
        if (handler == null){
            handler = new XMLRequestHandler();
        }
        return handler;
    }

    /**
     * Given a XMLString containing a request, parses the string
     * and returns an instance of the Request class
     * @param xmlProcessString
     * @return 
     */
   public Request parseXMLRequest(String xmlRequestString){
            try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Request.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            xmlRequestString = xmlRequestString.replaceAll("&lt;", "<");
            xmlRequestString = xmlRequestString.replaceAll("&gt;", ">");
            StringReader reader = new StringReader(xmlRequestString);
            
            Request request = (Request) jaxbUnmarshaller.unmarshal(reader);

            return request;
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
   } 
   
    
}
