/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.*;
import za.co.ucs.lwt.db.*;

/**
 * LeaveInfo displays related information of an employees Leave status
 *
 * @author liam
 */
public final class LeaveInfo implements AccsysReadOnlyObjectInterface {

    /**
     * Creates a new instance of Company <br>If companyID = null, a new Company
     * records will be created.
     *
     * @param employee
     */
    public LeaveInfo(Employee employee) {
        this.employee = employee;
        this.creationDate = new java.util.Date();
        if (isValid()) {
            loadFromDB();
        }
    }

    /**
     * Returns the age in minutes of this instance. This is necessary in order
     * to refresh the instance when it gets a bit 'old', as other processes
     * might have updated the employee's leave attributes already.
     *
     * @return
     */
    public int getInstanceAge() {
        return DatabaseObject.getNumberOfMinutesBetween(creationDate, new java.util.Date());
    }

    // Loads all the leave information from the database
    @Override
    public void loadFromDB() {
        if (this.employee == null) {
        } else {
            Connection con = null;
            String sql;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                sql = "select vw.PROFILE_ID, vw.LEAVE_TYPE_ID, "
                        + "vw.NAME as PROFILE_NAME, lp.DESCRIPTION as PROFILE_DESCRIPTION, vw.BALANCE_NOW, lt.NAME as LEAVE_TYPE, "
                        + " fn_L_GetCompiledUnit(vw.company_id, vw.employee_id, vw.leave_type_id) as CUNIT from "
                        + "vw_L_Balance_Now vw with (nolock) join l_leave_type lt with (nolock) on vw.LEAVE_TYPE_ID=lt.LEAVE_TYPE_ID "
                        + "join L_PROFILE lp with (nolock) on lp.profile_id=vw.PROFILE_ID where "
                        + "vw.Company_id=" + this.employee.getCompany().getCompanyID()
                        + " and vw.Employee_id=" + this.employee.getEmployeeID()
                        + " order by vw.LEAVE_TYPE_ID";
                try (ResultSet rs = DatabaseObject.openSQL(sql, con)) {
                    leaveDetails.clear();
                    while (rs.next()) {
                        String leaveType = rs.getString("LEAVE_TYPE");
                        String leaveCaption = rs.getString("PROFILE_DESCRIPTION");
                        // If the person used a proper description in L_PROFILE, use that for references to the leave
                        LeaveDetail leaveDetail = new LeaveDetail(this.employee, rs.getInt("PROFILE_ID"), rs.getString("PROFILE_NAME"),
                                rs.getInt("LEAVE_TYPE_ID"), leaveType, leaveCaption, rs.getFloat("BALANCE_NOW"), rs.getString("CUNIT"));
                        // Add Leave Detail to TreeMap.  This TreeMap stores one LeaveDetail object per leave type
                        // for this employee.
                        leaveDetails.put(leaveDetail.getProfileName(), leaveDetail);
                    }
                }

                // Load additional data
                this.dateOflastLeaveTaken = loadDateOfLastLeaveTaken();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        return (this.employee.isValid());
    }

    /**
     * How many leave types are assigned to this employee?
     *
     * @return
     */
    public int getNumberOfLeaveTypes() {
        return leaveDetails.size();
    }

    /**
     * Returns the Profile Name of the index'th profile in the TreeMap of
     * leaveDetails
     *
     * @param index
     * @return
     */
    public String getProfileName(int index) {
        Object[] balances = leaveDetails.values().toArray();
        return (((LeaveDetail) balances[index]).getProfileName());
    }

    /**
     * Returns the Leave Type of the index'th profile in the TreeMap of
     * leaveDetails
     *
     * @param index
     * @return
     */
    public String getLeaveType(int index) {
        Object[] balances = leaveDetails.values().toArray();
        try {
            return (((LeaveDetail) balances[index]).getLeaveType());
        } catch (ArrayIndexOutOfBoundsException e) {
            return "UNKNOWN";
        }
    }

    /**
     * Returns the Leave Caption (stored in L_PROFILE.DESCRIPTION of the
     * index'th profile in the TreeMap of leaveDetails
     *
     * @param index
     * @return
     */
    public String getLeaveCaption(int index) {
        Object[] balances = leaveDetails.values().toArray();
        try {
            return (((LeaveDetail) balances[index]).getLeaveCaption());
        } catch (ArrayIndexOutOfBoundsException e) {
            return "UNKNOWN";
        }
    }

    /**
     * Returns the predefined leaveTypeIDs for a given leave-type
     *
     * @param index
     * @return
     */
    public int getLeaveTypeID(int index) {
        Object[] balances = leaveDetails.values().toArray();
        return (((LeaveDetail) balances[index]).getLeaveTypeID());
    }

    /**
     * Returns the date of the last leave that was taken
     *
     * @return
     */
    public java.util.Date getDateOfLastLeaveTaken() {
        return this.dateOflastLeaveTaken;
    }

    private java.util.Date loadDateOfLastLeaveTaken() {
        java.util.Date result = null;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select max(START_DATE) from "
                    + "l_history with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and cancel_date is null and convert_date is null", con)) {
                if (rs.next()) {
                    result = rs.getDate(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of leave units taken for given leave_type in the
     * specific month
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public float getUnitsTakenInMonth(java.util.Date date, int leaveTypeID) {
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select sum(nb_unit) from "
                    + "l_history with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,START_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(mm,START_DATE)=datepart(mm,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID
                    + " and cancel_date is null and convert_date is null", con)) {
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the list (in HTML format - I know, not cool) of leave taken in a
     * given month
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public String getLeaveDetailOfUnitsTakenInMonth(java.util.Date date, int leaveTypeID) {
        StringBuilder result = new StringBuilder();
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select start_date, end_date, nb_unit from "
                    + "l_history with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,START_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(mm,START_DATE)=datepart(mm,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID
                    + " and cancel_date is null and convert_date is null", con)) {
                while (rs.next()) {
                    result.append("<b>_________________</b>");
                    result.append("<br>From:&nbsp;").append(DatabaseObject.formatDate(rs.getDate("start_date")));
                    result.append("<br>To:&nbsp;&nbsp;&nbsp;&nbsp;").append(DatabaseObject.formatDate(rs.getDate("end_date")));
                    result.append("<br>Units:&nbsp;").append(new DecimalFormat("#.##").format(rs.getBigDecimal("nb_unit")));
                    result.append("<br>");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result.toString();
    }

    /**
     * Returns the list of leave history entries for a given leave type in a
     * given month
     *
     * @param date Any day in a month for which the leave would be returned in
     * that same month
     * @param leaveTypeID
     * @return ArrayList of type LeaveHistoryEntry
     */
    public ArrayList<LeaveHistoryEntry> getLeaveHistoryEntriesTakenInMonth(java.util.Date date, int leaveTypeID) {
        ArrayList<LeaveHistoryEntry> result = new ArrayList<>();
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select history_id from "
                    + "l_history with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,START_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(mm,START_DATE)=datepart(mm,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID
                    + " and cancel_date is null and convert_date is null", con)) {
                while (rs.next()) {
                    LeaveHistoryEntry entry = new LeaveHistoryEntry(rs.getString("HISTORY_ID"));
                    result.add(entry);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of leave units taken for given leave_type in the
     * specific week
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public float getUnitsTakenInWeek(java.util.Date date, int leaveTypeID) {
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select sum(nb_unit) from "
                    + "l_history with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,START_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(wk,START_DATE)=datepart(wk,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID
                    + " and cancel_date is null and convert_date is null", con)) {
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of leave unit amendments given leave_type in the
     * specific month
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public float getUnitsAmendedInMonth(java.util.Date date, int leaveTypeID) {
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select sum(delta) from "
                    + "ll_emaster_amendleave with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,AMEND_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(mm,AMEND_DATE)=datepart(mm,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID, con)) {
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of leave unit amendments given leave_type in the
     * specific week
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public float getUnitsAmendedInWeek(java.util.Date date, int leaveTypeID) {
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select sum(delta) from "
                    + "ll_emaster_amendleave with (nolock) where "
                    + "Company_id=" + this.employee.getCompany().getCompanyID()
                    + " and Employee_id=" + this.employee.getEmployeeID()
                    + " and datepart(yy,AMEND_DATE)=datepart(yy,'" + DatabaseObject.formatDate(date) + "') "
                    + " and datepart(wk,AMEND_DATE)=datepart(wk,'" + DatabaseObject.formatDate(date) + "') "
                    + " and leave_type_id=" + leaveTypeID, con)) {
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of days/hours available as at the beginning of the
     * given month
     *
     * @param date
     * @param leaveTypeID
     * @return
     */
    public float getOpeningBalanceInMonth(java.util.Date date, int leaveTypeID) {
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            try (ResultSet rs = DatabaseObject.openSQL("select fn_L_EstimateBalance(" + this.employee.getCompany().getCompanyID()
                    + ", " + this.employee.getEmployeeID() + ", " + leaveTypeID + ", '" + DatabaseObject.formatDate(date) + "')", con)) {
                if (rs.next()) {
                    result = rs.getFloat(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the leave type ID of the given Leave Type
     *
     * @param leaveType
     * @return
     */
    public int getLeaveTypeID(String leaveType) {
        for (Object ld : leaveDetails.values().toArray()) {
            LeaveDetail leaveDetail = (LeaveDetail) ld;
            if (leaveDetail.getLeaveType().equalsIgnoreCase(leaveType)) {
                return (leaveDetail.getLeaveTypeID());
            }
        }
        return 0;
    }

    /**
     * Returns the leave type of the given LeaveTypeID
     *
     * @param leaveTypeID
     * @return
     */
    public String getLeaveTypeName(String leaveTypeID) {
        for (Object ld : leaveDetails.values().toArray()) {
            LeaveDetail leaveDetail = (LeaveDetail) ld;
            if (Integer.toString(leaveDetail.getLeaveTypeID()).equalsIgnoreCase(leaveTypeID)) {
                return (leaveDetail.getLeaveType());
            }
        }
        return "Unknown Leave Type";
    }

    /**
     * Returns the leave profileID of the given LeaveTypeID
     *
     * @param leaveTypeID
     * @return
     */
    public String getLeaveProfileID(int leaveTypeID) {

        for (Object ld : leaveDetails.values().toArray()) {
            LeaveDetail leaveDetail = (LeaveDetail) ld;
            if (leaveDetail.getLeaveTypeID() == leaveTypeID) {
                return (Integer.toString(leaveDetail.getProfileID()));
            }
        }
        return "0";
        
    }

    /**
     * Returns the index of the 'leaveDetails' array that contains, say Annual
     * Leave
     *
     * @param leaveType
     * @return
     */
    public int getIndexOfLeaveType(String leaveType) {
        for (int j = 0; j < this.getNumberOfLeaveTypes(); j++) {
            if (this.getLeaveType(j).compareToIgnoreCase(leaveType) == 0) {
                return (j);
            }
        }
        return 0;
    }

    /**
     * Returns the index of the 'leaveDetails' array that contains, say Leave
     * Type ID = 1
     *
     * @param leaveTypeID
     * @return
     */
    public int getIndexOfLeaveType(int leaveTypeID) {
        for (int j = 0; j < this.getNumberOfLeaveTypes(); j++) {
            if (this.getLeaveTypeID(j) == leaveTypeID) {
                return (j);
            }
        }
        return 0;
    }

    /**
     * Returns the leave reasons for the given leave type
     * [illness_id/incident_id][description]
     *
     * @param leaveTypeID
     * @return
     */
    public String[][] getLeaveReasons(int leaveTypeID) {
        if (leaveTypeID == 0) {
            return (new String[0][0]);
        }
        int index = getIndexOfLeaveType(leaveTypeID);
        Object[] balances = leaveDetails.values().toArray();
        return (((LeaveDetail) balances[index]).getLeaveReasons());
    }

    /**
     * Returns the Current Balance of the I'th profile in the TreeMap of
     * leaveDetails
     *
     * @param index
     * @return
     */
    public float getCurrentBalance(int index) {
        Object[] balances = leaveDetails.values().toArray();
        return (((LeaveDetail) balances[index]).getCurrentBalance());
    }

    /**
     * Returns the history of the given Leave Type. The index correlates with
     * the same index in getLeaveType(index) and getProfileName(index)
     *
     * @param index
     * @return LinkedList of type LeaveHistoryEntry
     */
    public LinkedList getLeaveHistory(int index) {
        Object[] balances = leaveDetails.values().toArray();
        return (((LeaveDetail) balances[index]).getLeaveHistory());
    }

    /**
     * Returns the history of the given Leave Type that occured ON or AFTER the
     * specified date
     *
     * @param index
     * @param fromDate
     * @return LinkedList of type LeaveHistoryEntry
     */
    public LinkedList getLeaveHistory(int index, java.util.Date fromDate) {
        LinkedList fullHistory = getLeaveHistory(index);
        LinkedList partialHistory = new LinkedList();

        DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();

        // Copy all entries later than now(*) to the resultant LinkedList
        Iterator iter = fullHistory.iterator();
        while (iter.hasNext()) {
            LeaveHistoryEntry entry = (LeaveHistoryEntry) iter.next();
            if (entry.getFromDate().after(fromDate)) {
                partialHistory.add(entry);
            }
        }
        return partialHistory;
    }

    /**
     * Returns the (first) LeaveHistoryEntry (or null) if the employee was on
     * leave at that given day
     *
     * @param onDate
     * @return LeaveHistoryEntry representing the leave taken during that period
     * [LWT] 2009/01/30 - In line with recent developments in the Leave Module
     * which allows for the taking of more than one partial day on the same day,
     * we will not return 'true' unless a full day's leave was taken.
     */
    public LeaveHistoryEntry didTakeLeaveOn(java.util.Date onDate) {
        for (int idx = 0; idx < leaveDetails.size(); idx++) {

            LinkedList fullHistory = getLeaveHistory(idx);

            Iterator iter = fullHistory.iterator();
            while (iter.hasNext()) {
                LeaveHistoryEntry entry = (LeaveHistoryEntry) iter.next();
                if ((DatabaseObject.betweenDates(onDate, entry.getFromDate(), entry.getToDate()))
                        && (entry.getUnitsTaken() > 0)) {
                    return entry;
                }
            }
        }
        return null;
    }

    /**
     * Returns the (first) LeaveHistoryEntry (or null) if the employee was on
     * leave at that given day
     *
     * @param fromDate
     * @param toDate
     * @return LeaveHistoryEntry representing the leave taken during that period
     */
    public boolean didTakeLeaveBetween(java.util.Date fromDate, java.util.Date toDate) {
        for (int idx = 0; idx < leaveDetails.size(); idx++) {

            LinkedList fullHistory = getLeaveHistory(idx);

            Iterator iter = fullHistory.iterator();
            while (iter.hasNext()) {
                LeaveHistoryEntry entry = (LeaveHistoryEntry) iter.next();

                if ((DatabaseObject.betweenDates(entry.getFromDate(), fromDate, toDate))
                        || (DatabaseObject.betweenDates(entry.getToDate(), fromDate, toDate))
                        || (DatabaseObject.betweenDates(entry.getToDate(), fromDate, toDate))
                        || ((entry.getFromDate().before(fromDate)) && (entry.getToDate().after(toDate)))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the index of the LeaveType in which one will find the logged
     * 'historyID'
     *
     * @param historyID
     * @return
     */
    public int getLeaveTypeIndexFromHistory(String historyID) {
        Object[] balances = leaveDetails.values().toArray();
        for (int index = 0; index < getNumberOfLeaveTypes(); index++) {
            LinkedList history = ((LeaveDetail) balances[index]).getLeaveHistory();
            for (Object history1 : history) {
                LeaveHistoryEntry entry = (LeaveHistoryEntry) history1;
                if (entry.getHistoryID().compareToIgnoreCase(historyID) == 0) {
                    return index;
                }
            }
        }
        return (-1);
    }

    /**
     * Returns the entry in L_HISTORY of the given Leave Type and HISTORY_ID.
     * The index correlates with the same index in getLeaveType(index) and
     * getProfileName(index)
     *
     * @param historyID
     * @return
     * @returns String[col][row] - a two-dimensional array containing <br>
     * FromDate, ToDate, NbDays, Reason, and HISTORY_ID.
     */
    public LeaveHistoryEntry getLeaveHistoryEntry(String historyID) {
        Object[] balances = leaveDetails.values().toArray();
        for (int index = 0; index < getNumberOfLeaveTypes(); index++) {
            LinkedList history = ((LeaveDetail) balances[index]).getLeaveHistory();
            for (Object history1 : history) {
                LeaveHistoryEntry entry = (LeaveHistoryEntry) history1;
                if (entry.getHistoryID().compareToIgnoreCase(historyID) == 0) {
                    return entry;
                }
            }
        }
        return (null);
    }

    /**
     * Getter for property employee.
     *
     * @return Value of property employee.
     *
     */
    public za.co.ucs.accsys.peopleware.Employee getEmployee() {
        return employee;
    }

    /**
     * In the long run, various leave types will be catered for for half-day
     * leave.
     *
     * @param leaveType
     * @return
     */
    public boolean canTakeHalfDayLeave(String leaveType) {
        return canTakeHalfDayLeave(leaveTypeToLeaveTypeID(leaveType));
    }

    /**
     * In the long run, various leave types will be catered for for half-day
     * leave.
     *
     * @param leaveTypeID
     * @return
     */
    public boolean canTakeHalfDayLeave(int leaveTypeID) {
        boolean rslt = false;
        // For now, only Annual Leave, Sick Leave, Study and Family
        //System.out.println("this.employee.getHoursPerDay():" + this.employee.getHoursPerDay());
        //System.out.println("leaveType.compareToIgnoreCase(\"Annual\") :" + leaveType.compareToIgnoreCase("Annual"));
        if ((this.employee.getHoursPerDay() > 0)) {
            // Annual Leave
            if (leaveTypeID == 1) {
                return true;
            }
            // Sick Leave
            if (leaveTypeID == 3) {
                return true;
            }
            // Study Leave
            if (leaveTypeID == 2) {
                return true;
            }
            // Family Leave
            if (leaveTypeID == 5) {
                return true;
            }
            // Special Leave
            if (leaveTypeID == 6) {
                return true;
            }
            // Unpaid Leave
            if (leaveTypeID == 7) {
                return true;
            }

        }
        return rslt;
    }

    /**
     * Quickly lookup Leave Type to LeaveTypeID
     *
     * @param leaveType
     * @return
     */
    public int leaveTypeToLeaveTypeID(String leaveType) {

        // Annual Leave
        if (leaveType.compareToIgnoreCase("Annual") == 0) {
            return 1;
        }
        // Study Leave
        if (leaveType.compareToIgnoreCase("Study") == 0) {
            return 2;
        }
        // Sick Leave
        if (leaveType.compareToIgnoreCase("Sick") == 0) {
            return 3;
        }
        // Maternity Leave
        if (leaveType.compareToIgnoreCase("Maternity") == 0) {
            return 4;
        }
        // Family Leave
        if (leaveType.compareToIgnoreCase("Family") == 0) {
            return 5;
        }
        // Special Leave
        if (leaveType.compareToIgnoreCase("Special") == 0) {
            return 6;
        }
        // Unpaid Leave
        if (leaveType.compareToIgnoreCase("Unpaid") == 0) {
            return 7;
        }
        // Long Service Leave
        if (leaveType.compareToIgnoreCase("Long Service") == 0) {
            return 8;
        }
        // Compassionate Maternity Leave
        if (leaveType.compareToIgnoreCase("Compassionate Maternity") == 0) {
            return 9;
        }
        // Injury on Duty Leave
        if (leaveType.compareToIgnoreCase("Injury on Duty") == 0) {
            return 10;
        }

        return 0;
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.peopleware.LeaveInfo
     za.co.ucs.accsys.peopleware.LeaveInfo:    static final long serialVersionUID = 1411302484629752550L;

     */
    static final long serialVersionUID = 1411302484629752550L;
    private final Employee employee;
    private java.util.Date dateOflastLeaveTaken;
    // We want the Leave Balances to be sorted by profile name
    // leaveDetails stores one object for every type of profile that this
    // employee is linked to.
    private final TreeMap leaveDetails = new TreeMap();
    private final java.util.Date creationDate;
}
