/*
 * Family.java
 *
 * Created on July 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * BackAccount will be used to store back details for employees and companies
 * @author  liam
 */
public final class BankAccount implements AccsysReadOnlyObjectInterface, java.io.Serializable {

    static final long serialVersionUID = -397456387011600071L;

    /** Creates a new instance of Contact
     */
    public BankAccount(Object owner, int accountID) {
        this.owner = owner;
        this.accountID = new Integer(accountID);
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void loadFromDB() {
        //  If owner is Employee
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Employee") == 0) {
            loadFromDB(((Employee) this.owner));
        }
        //  If owner is Company
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Company") == 0) {
            loadFromDB(((Company) this.owner));
        }
    }

    private void loadFromDB(Employee employee) {
        if (employee == null) {
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // E_BANKDET
                //
                ResultSet rs = DatabaseObject.openSQL("select * from e_bankdet where "
                        + " Company_id=" + employee.getCompany().getCompanyID()
                        + " and Employee_id=" + employee.getEmployeeID()
                        + " and account_id=" + this.accountID, con);
                if (rs.next()) {
                    accountDescription = rs.getString("ACCOUNT_DESCRIPTION");
                    accountName = rs.getString("ACCOUNT_NAME");
                    accountNumber = rs.getString("ACCOUNT_NUMBER");
                    accountType = rs.getString("ACCOUNT_TYPE");
                    bank = rs.getString("BANK");
                    bankCode = rs.getString("BANK_CODE");
                    branch = rs.getString("BRANCH");
                    branchCode = rs.getString("BRANCH_CODE");
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    private void loadFromDB(Company company) {
        if (company == null) {
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // E_BANKDET
                //
                ResultSet rs = DatabaseObject.openSQL("select * from c_bankdet where "
                        + " Company_id=" + company.getCompanyID()
                        + " and account_id=" + this.accountID, con);
                if (rs.next()) {
                    accountDescription = rs.getString("ACCOUNT_DESCRIPTION");
                    accountName = rs.getString("ACCOUNT_NAME");
                    accountNumber = rs.getString("ACCOUNT_NUMBER");
                    accountType = rs.getString("ACCOUNT_TYPE");
                    bank = rs.getString("BANK");
                    bankCode = rs.getString("BANK_CODE");
                    branch = rs.getString("BRANCH");
                    branchCode = rs.getString("BRANCH_CODE");
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        //  If owner is Employee
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Employee") == 0) {
            return (((Employee) this.owner).isValid());
        }
        //  If owner is Company
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Company") == 0) {
            return (((Company) this.owner).isValid());
        }
        return (false);
    }

    public java.lang.String getBankCode() {
        if (bankCode == null) {
            return ("");
        } else {
            return bankCode;
        }
    }

    public java.lang.Integer getAccountID() {
        return accountID;
    }

    public java.lang.String getAccountName() {
        if (accountName == null) {
            return ("");
        } else {
            return accountName;
        }
    }

    public java.lang.String getBranch() {
        if (branch == null) {
            return ("");
        } else {
            return branch;
        }
    }

    public java.lang.String getBranchCode() {
        if (branchCode == null) {
            return ("");
        } else {
            return branchCode;
        }
    }

    public java.lang.String getAccountType() {
        if (accountType == null) {
            return ("");
        } else {
            return accountType;
        }
    }

    public java.lang.String getAccountDescription() {
        if (accountDescription == null) {
            return ("");
        } else {
            return accountDescription;
        }
    }

    public java.lang.String getAccountNumber() {
        if (accountNumber == null) {
            return ("");
        } else {
            return accountNumber;
        }
    }

    public java.lang.String getBank() {
        if (bank == null) {
            return ("");
        } else {
            return bank;
        }
    }
    private Object owner;
    private Integer accountID;
    private String accountType = "";
    private String accountName = "";
    private String accountDescription = "";
    private String bank = "";
    private String branch = "";
    private String bankCode = "";
    private String branchCode = "";
    private String accountNumber = "";
}
