/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * CostCentre maps to dba.C_COSTMAIN
 * @author  liam
 */
public class CostCentre implements AccsysObjectInterface, java.io.Serializable {

    static final long serialVersionUID = -7612809776836358318L;

    /** Creates a new instance of Payroll
     * @param company
     * @param costID
     */
    public CostCentre(Company company, Integer costID) {
        this.company = company;
        this.costID = costID;
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save costcentre values to db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuffer updateSQL = new StringBuffer();
                updateSQL.append("update c_costmain set ");
                updateSQL.append("COST_NUMBER='").append(getCostNumber()).append("', ");
                updateSQL.append("DESCRIPTION='").append(getDescription()).append("', ");
                updateSQL.append("NAME='").append(getName()).append("' ");
                updateSQL.append("where company_id=").append(this.getCompany().getCompanyID());
                updateSQL.append(" and cost_id=").append(this.getCostID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
                // Parent Cost ID
                if (getParent() == null) {
                    updateSQL = new StringBuffer();
                    updateSQL.append("update c_costmain set ");
                    updateSQL.append("PARENT_COST_ID=null ");
                    updateSQL.append("where company_id=").append(this.getCompany().getCompanyID());
                    updateSQL.append(" and cost_id=").append(this.getCostID());
                    DatabaseObject.executeSQL(updateSQL.toString(), con);
                } else {
                    updateSQL = new StringBuffer();
                    updateSQL.append("update c_costmain set ");
                    updateSQL.append("PARENT_COST_ID=").append(this.getParent().getCostID());
                    updateSQL.append("where company_id=").append(this.getCompany().getCompanyID());
                    updateSQL.append(" and cost_id=").append(this.getCostID());
                    DatabaseObject.executeSQL(updateSQL.toString(), con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            System.out.println("Unable to load costcentre values from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from c_costmain where company_id=" + this.getCompany().getCompanyID()
                        + " and cost_id=" + this.getCostID(), con);
                if (rs.next()) {
                    //Parent
                    if (rs.getInt("PARENT_COST_ID") != 0) {
                        this.setParent(new CostCentre(this.getCompany(), rs.getInt("PARENT_COST_ID")));
                    }
                    this.costNumber = rs.getString("COST_NUMBER");
                    this.description = rs.getString("DESCRIPTION");
                    this.name = rs.getString("NAME");
                    this.pathName = rs.getString("PATH_NAME");
                    this.pathNumber = rs.getString("PATH_NUMBER");
                    this.pathDescription = rs.getString("PATH_DESCR");
                    

                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        return ((this.company != null) && (this.costID != null));
    }

    /** Getter for property description.
     * @return Value of property description.
     *
     */
    public java.lang.String getDescription() {
        return description;
    }

    /** Setter for property description.
     * @param description New value of property description.
     *
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }

    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    /** Getter for property costID.
     * @return Value of property costID.
     *
     */
    public java.lang.Integer getCostID() {
        return costID;
    }

    /** Getter for property costNumber.
     * @return Value of property costNumber.
     *
     */
    public java.lang.String getCostNumber() {
        return costNumber;
    }

    /** Setter for property costNumber.
     * @param costNumber New value of property costNumber.
     *
     */
    public void setCostNumber(java.lang.String costNumber) {
        this.costNumber = costNumber;
    }

    /** Getter for property parent.
     * @return Value of property parent.
     *
     */
    public za.co.ucs.accsys.peopleware.CostCentre getParent() {
        return parent;
    }

    /** Setter for property parent.
     * @param parent New value of property parent.
     *
     */
    public void setParent(za.co.ucs.accsys.peopleware.CostCentre parent) {
        this.parent = parent;
    }

    /** 
     * Returns the path of the cost centre, denoted by Cost Centre name
     * @return 
     */
    public String getNamePath(){
        return pathName;
    }
    
    /** 
     * Returns the path of the cost centre, denoted by Cost Centre number
     * @return 
     */
    public String getNumberPath(){
        return pathNumber;
    }

    /** 
     * Returns the path of the cost centre, denoted by Cost Centre description
     * @return 
     */
    public String getDescriptionPath(){
        return pathDescription;
    }
    
    /** Getter for property company.
     * @return Value of property company.
     *
     */
    public za.co.ucs.accsys.peopleware.Company getCompany() {
        return company;
    }
    private final Integer costID;
    private final Company company;
    private String costNumber;
    private String description;
    private String name;
    private CostCentre parent;
    private String pathName;
    private String pathNumber;
    private String pathDescription;
    
}
