/*
 * PayPoint.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Paypoint maps to dba.P_PAYPOINT
 * @author  liam
 */
public final class PayPoint implements AccsysObjectInterface, java.io.Serializable {

    static final long serialVersionUID = -4674477229810255057L;

    /** Creates a new instance of Paypoint
     * <br>If companyID = null or paypointID=null, a new Paypoint record will be created.
     */
    public PayPoint(Company company, Integer paypointID) {
        this.company = company;
        this.paypointID = paypointID;
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save paypoint values to db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuilder updateSQL = new StringBuilder();
                updateSQL.append("update p_paypoint set ");
                updateSQL.append("NAME='").append(getName()).append("', ");
                updateSQL.append("DESCRIPTION='").append(getDescription()).append("' ");
                updateSQL.append("where company_id=").append(this.company.getCompanyID()).append(" and paypoint_id=").append(this.paypointID);
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            System.out.println("Unable to load paypoint values from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from p_paypoint where company_id=" + this.company.getCompanyID()
                        + " and paypoint_id=" + this.paypointID, con);
                if (rs.next()) {
                    this.name = rs.getString("NAME");
                    this.description = rs.getString("DESCRIPTION");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean isValid() {
        return ((this.company != null) && (this.paypointID != null));
    }

    /**
     * Getter for property description.
     * @return Value of property description.
     */
    public java.lang.String getDescription() {
        return description;
    }

    /**
     * Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /** Getter for property paypointID.
     * @return Value of property paypointID.
     *
     */
    public java.lang.Integer getPaypointID() {
        return paypointID;
    }

    /** Getter for property company.
     * @return Value of property company.
     *
     */
    public za.co.ucs.accsys.peopleware.Company getCompany() {
        return company;
    }
    private Integer paypointID;
    private Company company;
    private String name;
    private String description;
}
