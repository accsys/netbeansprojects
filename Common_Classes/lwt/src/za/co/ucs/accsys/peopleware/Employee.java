/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import za.co.ucs.accsys.tools.StringEncrypterSingleton;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * Employee maps to dba.E_MASTER
 *
 * @author liam
 */
public final class Employee implements AccsysObjectInterface, java.io.Serializable {

    /**
     * Creates a new instance of Company <br>If companyID = null, a new Company
     * records will be created.
     *
     * @param company
     * @param employeeID
     */
    public Employee(Company company, Integer employeeID) {
        setCompany(company);
        setEmployeeID(employeeID);
        if (isValid()) {
            loadFromDB();
        }
    }

    /**
     * Change the password to the new Password
     *
     * @param newPassword
     */
    public void changePasswordInDatabase(String newPassword) {
        setWeb_pass(newPassword);
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder updateSQL = new StringBuilder();
            updateSQL.append("update e_master set WEB_PWD=upper(trim(fn_S_EncryptString('").append(newPassword.trim()).append("'))) ");
            updateSQL.append("where company_id=").append(getCompany().getCompanyID()).append(" and employee_id=").append(getEmployeeID());
            DatabaseObject.executeSQL(updateSQL.toString(), con);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    /**
     * Returns true if the username/password combination is not already in use
     *
     * @param newPassword
     * @return
     */
    public boolean canChangePasswordInDatabase(String newPassword) {
        boolean result = false;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select count(*) from e_master with (NOLOCK) where "
                    + "upper(trim(fn_S_DecryptString(WEB_PWD)))=upper(trim('" + newPassword.trim() + "')) "
                    + "and upper(trim(fn_S_DecryptString(WEB_LOGIN)))=upper(trim('" + getWeb_login() + "'))", con);
            if (rs.next()) {
                result = (rs.getInt(1) == 0);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    //Liam: 02/05/2014
    //Corrections made on the applyToDB class that does not seem to have been working for quite some time
    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save employee values to db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuffer updateSQL = new StringBuffer();
                updateSQL.append("update e_master set ");
                updateSQL.append("BIRTHDATE='").append(DatabaseObject.formatDate(getBirthDate())).append("', ");
                updateSQL.append("COMPANY_EMPLOYEE_NUMBER='").append(getEmployeeNumber()).append("', ");
                updateSQL.append("FIRSTNAME='").append(getFirstName()).append("', ");
                if (getGroupJoinDate() != null) {
                    updateSQL.append("GROUP_JOIN_DATE='").append(DatabaseObject.formatDate(getGroupJoinDate())).append("', ");
                }
                updateSQL.append("HOBBIES='").append(getHobbies()).append("', ");
                updateSQL.append("HOME_LANGUAGE='").append(getHomeLanguage()).append("', ");
                updateSQL.append("INITIALS='").append(getInitials()).append("', ");
                updateSQL.append("INSOLVENT_YN='").append(wasInsolvent()).append("', ");

                updateSQL.append("MARITAL_STATUS='").append(getMaritalStatus()).append("', ");
                updateSQL.append("PREFERRED_NAME='").append(getPreferredName()).append("', ");
                updateSQL.append("RELIGION='").append(getReligion()).append("', ");
                updateSQL.append("SECONDNAME='").append(getSecondName()).append("', ");
                updateSQL.append("SURNAME='").append(getSurname()).append("', ");
                updateSQL.append("WEB_LOGIN=fn_S_EncryptString('").append(getWeb_login()).append("'), ");
                updateSQL.append("WEB_PWD=fn_S_EncryptString('").append(getWeb_pass()).append(")'), ");
                updateSQL.append("TITLE='").append(getTitle()).append("' ");
                updateSQL.append("where company_id=").append(getCompany().getCompanyID()).append(" and employee_id=").append(getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
                // Cost Centre
                if (getCostCentre() == null) {
                    updateSQL = new StringBuffer();
                    updateSQL.append("update e_master set ");
                    updateSQL.append("COST_ID=null ");
                    updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                    updateSQL.append(" and employee_id=").append(getEmployeeID());
                    DatabaseObject.executeSQL(updateSQL.toString(), con);
                } else {
                    updateSQL = new StringBuffer();
                    updateSQL.append("update e_master set ");
                    updateSQL.append("COST_ID=").append(getCostCentre().getCostID());
                    updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                    updateSQL.append(" and employee_id=").append(getEmployeeID());
                    DatabaseObject.executeSQL(updateSQL.toString(), con);
                }
                // E_MAIL
                updateSQL = new StringBuffer();
                updateSQL.append("update e_contact set ");
                updateSQL.append("E_MAIL='").append(getEMail()).append("', ");
                updateSQL.append("TEL_HOME='").append(getHomePhone()).append("', ");
                updateSQL.append("TEL_WORK='").append(getWorkPhone()).append("', ");
                updateSQL.append("FAX_HOME='").append(getHomeFax()).append("', ");
                updateSQL.append("FAX_WORK='").append(getWorkFax()).append("', ");
                updateSQL.append("TEL_CELL='").append(getCell()).append("' ");

                updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                updateSQL.append(" and employee_id=").append(getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);

                // ID Number, TAX Number
                updateSQL = new StringBuffer();
                updateSQL.append("update e_number set ");
                updateSQL.append("ID_NUMBER='").append(getIDNumber()).append("', ");
                updateSQL.append("TAX_NUMBER='").append(getIDNumber()).append("' ");
                updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                updateSQL.append(" and employee_id=").append(getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);

                // Race
                updateSQL = new StringBuffer();
                updateSQL.append("delete from e_group where company_id=").append(getCompany().getCompanyID());
                updateSQL.append("  and employee_id=").append(getEmployeeID());
                updateSQL.append("  and group_id=2; ");
                updateSQL.append(" insert into e_group (company_id, employee_id, group_id, subgroup1_id) ");
                updateSQL.append(" values (").append(getCompany().getCompanyID()).append(", ").append(getEmployeeID()).append(", 2, ");
                updateSQL.append(" (select group_id from c_group where parent_group_id=2 ");
                updateSQL.append("  and trim(upper(description))=trim(upper('").append(getRace().trim()).append("')))); commit;");
                DatabaseObject.executeSQL(updateSQL.toString(), con);

                // Gender
                updateSQL = new StringBuffer();
                updateSQL.append("delete from e_group where company_id=").append(getCompany().getCompanyID());
                updateSQL.append("  and employee_id=").append(getEmployeeID());
                updateSQL.append("  and group_id=1; ");
                updateSQL.append(" insert into e_group (company_id, employee_id, group_id, subgroup1_id) ");
                updateSQL.append(" values (").append(getCompany().getCompanyID()).append(", ").append(getEmployeeID()).append(", 1, ");
                updateSQL.append(" (select group_id from c_group where parent_group_id=1 ");
                updateSQL.append("  and trim(upper(description))=trim(upper('").append(getGender().trim()).append("')))); commit;");
                DatabaseObject.executeSQL(updateSQL.toString(), con);

                //PASSPORT & COUNTRY OF ISSUE
                updateSQL = new StringBuffer();
                updateSQL.append("update e_passport set ");
                updateSQL.append("PASSPORT_NUMBER='").append(getPassportNumber()).append("', ");
                updateSQL.append("PASSPORT_HELD='").append(getCountryOfIssue()).append("' ");
                updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                updateSQL.append(" and employee_id=").append(getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);

                //EMERGENCY
                updateSQL = new StringBuffer();
                updateSQL.append("update e_emergency set ");
                updateSQL.append("DOCTOR='").append(getDoctor()).append("', ");
                updateSQL.append("PHONE='").append(getPhone()).append("', ");
                updateSQL.append("KNOWN_CONDITIONS='").append(getKnownConditions()).append("', ");
                updateSQL.append("ALLERGIES='").append(getAllergies()).append("' ");
                updateSQL.append("where company_id=").append(getCompany().getCompanyID());
                updateSQL.append(" and employee_id=").append(getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            System.out.println("Unable to load employee values from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select *, fn_S_DecryptString(WEB_LOGIN) as DEC_LOGIN, "
                        + "fn_S_DecryptString(WEB_PWD) as DEC_PASS, fn_P_GetEmployeeRace(company_id, employee_id) as RACE, "
                        + "fn_P_GetEmployeeGender(company_id, employee_id) as theGENDER "
                        + " from e_master with (NOLOCK) where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID(), con);
                if (rs.next()) {
                    setBirthDate(rs.getDate("BIRTHDATE"));
                    setEmployeeNumber(coalesce(rs.getString("COMPANY_EMPLOYEE_NUMBER"), ""));
                    if (rs.getInt("COST_ID") != 0) {
                        setCostCentre(new CostCentre(getCompany(), new Integer(rs.getInt("COST_ID"))));
                    }
                    setHasCriminalRecord(coalesce(rs.getString("CRIMINAL_RECORD_YN"), "").compareToIgnoreCase("Y") == 0);
                    setFirstName(coalesce(rs.getString("FIRSTNAME"), ""));
                    setGender(coalesce(rs.getString("theGENDER"), ""));
                    setRace(coalesce(rs.getString("RACE"), ""));
                    setGroupJoinDate(rs.getDate("GROUP_JOIN_DATE"));
                    setHobbies(coalesce(rs.getString("HOBBIES"), ""));
                    setHomeLanguage(coalesce(rs.getString("HOME_LANGUAGE"), ""));
                    setInitials(coalesce(rs.getString("INITIALS"), ""));
                    setWasInsolvent(coalesce(rs.getString("INSOLVENT_YN"), "").compareToIgnoreCase("Y") == 0);
                    setMaritalStatus(coalesce(rs.getString("MARITAL_STATUS"), ""));
                    setPreferredName(coalesce(rs.getString("PREFERRED_NAME"), ""));
                    setReligion(coalesce(rs.getString("RELIGION"), ""));
                    setSecondName(coalesce(rs.getString("SECONDNAME"), ""));
                    setSurname(coalesce(rs.getString("SURNAME"), ""));
                    setTitle(coalesce(rs.getString("TITLE"), ""));
                    setWeb_login(coalesce(rs.getString("DEC_LOGIN"), ""));
                    setWeb_pass(coalesce(rs.getString("DEC_PASS"), ""));
                }
                // Get Payroll
                rs.close();
                rs = DatabaseObject.openSQL("select payroll_id from pl_paydef_emaster  with (NOLOCK) where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID(), con);
                this.payroll = null;
                if (rs.next()) {
                    if (rs.getInt("payroll_id") != 0) {
                        setPayroll(new Payroll(new Integer(rs.getInt("payroll_id"))));
                    }
                }
                // Get PersonID
                rs.close();
                rs = DatabaseObject.openSQL("select person_id from ta_e_master  with (NOLOCK) where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID(), con);
                if (rs.next()) {
                    if (rs.getInt("person_id") != 0) {
                        setPersonID(rs.getInt("person_id"));
                    }
                }
                // Get Pay - Information
                rs.close();
                rs = DatabaseObject.openSQL("select * from p_e_basic  with (NOLOCK) where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID(), con);
                if (rs.next()) {
                    setRateOfPay1(rs.getString("RATE_OF_PAY1"));
                    setRateOfPay1(rs.getString("RATE_OF_PAY2"));
                    setRateOfPay1(rs.getString("RATE_OF_PAY3"));
                    setPayPer1(rs.getString("PAY_PER1"));
                    setPayPer2(rs.getString("PAY_PER1"));
                    setPayPer3(rs.getString("PAY_PER1"));
                    setTotalPackage(rs.getString("TOTAL_PACKAGE"));
                    setHoursPerDay(rs.getFloat("HOURS_PER_DAY"));
                    setDaysPerMonth(rs.getFloat("DAYS_PER_MONTH"));
                }

                // Get EMail
                rs.close();
                rs = DatabaseObject.openSQL("select * from e_contact  with (NOLOCK) where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID(), con);
                this.eMail = null;
                if (rs.next()) {
                    setEMail(rs.getString("E_MAIL"));
                    setHomePhone(rs.getString("TEL_HOME"));
                    setWorkPhone(rs.getString("TEL_WORK"));
                    setHomeFax(rs.getString("FAX_HOME"));
                    setWorkFax(rs.getString("FAX_WORK"));
                    setCell(rs.getString("TEL_CELL"));
                }
                rs.close();

                // Load Additional properties
                setPayPoint(loadPayPoint());
                setFirstDOE(loadFirstDOE());
                setLastDOE(loadLastDOE());
                setLatestCalculatedPeriod(loadLatestCalculatedPeriod());
                setJobPosition(loadJobPosition());
                setJobGradeLevel(loadJobGradeLevel());
                setContact(loadContact());
                setBankAccounts(loadBankAccounts());
                setLoans(loadLoans());
                setFamilyMembers(loadFamilyMembers());
                loadIdAndTaxNumber();
                loadPassportDetail();
                loadEmergencyDetails();
                loadArchivedPayslipTaxYears();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    /**
     * Getter for property company.
     *
     * @return Value of property company.
     *
     */
    public za.co.ucs.accsys.peopleware.Company getCompany() {
        return company;
    }

    /**
     * Setter for property company.
     *
     * @param company New value of property company.
     *
     */
    public void setCompany(za.co.ucs.accsys.peopleware.Company company) {
        this.company = company;
    }

    /**
     * Getter for property employeeID.
     *
     * @return Value of property employeeID.
     *
     */
    public java.lang.Integer getEmployeeID() {
        return employeeID;
    }

    /**
     * Setter for property employeeID.
     *
     * @param employeeID New value of property employeeID.
     *
     */
    public void setEmployeeID(java.lang.Integer employeeID) {
        this.employeeID = employeeID;
    }

    /**
     * Getter for property birthDate.
     *
     * @return Value of property birthDate.
     *
     */
    public java.util.Date getBirthDate() {
        return birthDate;
    }

    /**
     * Setter for property birthDate.
     *
     * @param birthDate New value of property birthDate.
     *
     */
    public void setBirthDate(java.util.Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Getter for property employeeNumber.
     *
     * @return Value of property employeeNumber.
     *
     */
    public java.lang.String getEmployeeNumber() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.employeeNumber);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property employeeNumber.
     *
     * @param employeeNumber New value of property employeeNumber.
     *
     */
    public void setEmployeeNumber(java.lang.String employeeNumber) {
        try {
            this.employeeNumber = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(employeeNumber);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property hasCriminalRecord.
     *
     * @return Value of property hasCriminalRecord.
     *
     */
    public String hasCriminalRecord() {
        if (hasCriminalRecord) {
            return "Y";
        } else {
            return "N";
        }
    }

    public boolean isHasCriminalRecord() {
        return hasCriminalRecord;
    }

    /**
     * Setter for property hasCriminalRecord.
     *
     * @param hasCriminalRecord New value of property hasCriminalRecord.
     *
     */
    public void setHasCriminalRecord(boolean hasCriminalRecord) {
        this.hasCriminalRecord = hasCriminalRecord;
    }

    /**
     * Getter for property firstName.
     *
     * @return Value of property firstName.
     *
     */
    public java.lang.String getFirstName() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.firstName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property firstName.
     *
     * @param firstName New value of property firstName.
     *
     */
    public void setFirstName(java.lang.String firstName) {
        try {
            this.firstName = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(firstName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }

    }

    /**
     * Getter for property gender.
     *
     * @return Value of property gender.
     *
     */
    public java.lang.String getGender() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.gender);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property gender.
     *
     * @param gender New value of property gender.
     *
     */
    public void setGender(java.lang.String gender) {
        try {
            this.gender = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(gender);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property groupJoinDate.
     *
     * @return Value of property groupJoinDate.
     *
     */
    public java.util.Date getGroupJoinDate() {
        return groupJoinDate;
    }

    /**
     * Setter for property groupJoinDate.
     *
     * @param groupJoinDate New value of property groupJoinDate.
     *
     */
    public void setGroupJoinDate(java.util.Date groupJoinDate) {
        this.groupJoinDate = groupJoinDate;
    }

    /**
     * Getter for property hobbies.
     *
     * @return Value of property hobbies.
     *
     */
    public java.lang.String getHobbies() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.hobbies);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property hobbies.
     *
     * @param hobbies New value of property hobbies.
     *
     */
    public void setHobbies(java.lang.String hobbies) {
        try {
            this.hobbies = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(hobbies);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property homeLanguage.
     *
     * @return Value of property homeLanguage.
     *
     */
    public java.lang.String getHomeLanguage() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.homeLanguage);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property homeLanguage.
     *
     * @param homeLanguage New value of property homeLanguage.
     *
     */
    public void setHomeLanguage(java.lang.String homeLanguage) {
        try {
            this.homeLanguage = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(homeLanguage);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property initials.
     *
     * @return Value of property initials.
     *
     */
    public java.lang.String getInitials() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.initials);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property initials.
     *
     * @param initials New value of property initials.
     *
     */
    public void setInitials(java.lang.String initials) {
        try {
            this.initials = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(initials);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property maritalStatus.
     *
     * @return Value of property maritalStatus.
     *
     */
    public java.lang.String getMaritalStatus() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.maritalStatus);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property maritalStatus.
     *
     * @param maritalStatus New value of property maritalStatus.
     *
     */
    public void setMaritalStatus(java.lang.String maritalStatus) {
        try {
            this.maritalStatus = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(maritalStatus);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property preferredName.
     *
     * @return Value of property preferredName.
     *
     */
    public java.lang.String getPreferredName() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.preferredName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property preferredName.
     *
     * @param preferredName New value of property preferredName.
     *
     */
    public void setPreferredName(java.lang.String preferredName) {
        try {
            this.preferredName = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(preferredName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property religion.
     *
     * @return Value of property religion.
     *
     */
    public java.lang.String getReligion() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.religion);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property religion.
     *
     * @param religion New value of property religion.
     *
     */
    public void setReligion(java.lang.String religion) {
        try {
            this.religion = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(religion);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property secondName.
     *
     * @return Value of property secondName.
     *
     */
    public java.lang.String getSecondName() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.secondName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property secondName.
     *
     * @param secondName New value of property secondName.
     *
     */
    public void setSecondName(java.lang.String secondName) {
        try {
            this.secondName = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(secondName);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property surname.
     *
     * @return Value of property surname.
     *
     */
    public java.lang.String getSurname() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.surname);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public String getRateOfPay1() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rop1);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setRateOfPay1(String rop1) {
        try {
            this.rop1 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rop1);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getRateOfPay2() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rop2);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setRateOfPay2(String rop2) {
        try {
            this.rop2 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rop2);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getRateOfPay3() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rop3);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setRateOfPay3(String rop3) {
        try {
            this.rop3 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rop3);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getPayPer1() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.payper1);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setPayPer1(String payper1) {
        try {
            this.payper1 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(payper1);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getPayPer2() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.payper2);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setPayPer2(String payper2) {
        try {
            this.payper2 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(payper2);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getPayPer3() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.payper3);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setPayPer3(String payper3) {
        try {
            this.payper3 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(payper3);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public String getTotalPackage() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.totalPackage);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    public void setTotalPackage(String totalPackage) {
        try {
            this.totalPackage = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(totalPackage);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    public float getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(float hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }

    public float getDaysPerMonth() {
        return daysPerMonth;
    }

    public void setDaysPerMonth(float daysPerMonth) {
        this.daysPerMonth = daysPerMonth;
    }

    /**
     * Setter for property surname.
     *
     * @param surname New value of property surname.
     *
     */
    public void setSurname(java.lang.String surname) {
        try {
            this.surname = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(surname);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property title.
     *
     * @return Value of property title.
     *
     */
    public java.lang.String getTitle() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.title);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property title.
     *
     * @param title New value of property title.
     *
     */
    public void setTitle(java.lang.String title) {
        try {
            this.title = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(title);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        return ((getEmployeeID() != null) && (getCompany() != null));
    }

    /**
     * Getter for property wasInsolvent.
     *
     * @return Value of property wasInsolvent.
     *
     */
    public boolean isWasInsolvent() {
        return wasInsolvent;
    }

    public String wasInsolvent() {
        if (wasInsolvent) {
            return "Y";
        } else {
            return "N";
        }
    }

    /**
     * Setter for property wasInsolvent.
     *
     * @param wasInsolvent New value of property wasInsolvent.
     *
     */
    public void setWasInsolvent(boolean wasInsolvent) {
        this.wasInsolvent = wasInsolvent;
    }

    /**
     * Getter for property costCentre.
     *
     * @return Value of property costCentre.
     *
     */
    public za.co.ucs.accsys.peopleware.CostCentre getCostCentre() {
        return costCentre;
    }

    /**
     * Setter for property costCentre.
     *
     * @param costCentre New value of property costCentre.
     *
     */
    public void setCostCentre(za.co.ucs.accsys.peopleware.CostCentre costCentre) {
        this.costCentre = costCentre;
    }

    @Override
    public String toString() {
        if ((this.getPreferredName() != null) && (this.getPreferredName().trim().length() > 0)) {
            return (this.getPreferredName() + " " + this.getSurname() + " [" + this.getEmployeeNumber() + "]");
        } else {
            return (this.getFirstName() + " " + this.getSurname() + " [" + this.getEmployeeNumber() + "]");
        }
    }

    /**
     * The Payroll is read-only
     *
     * @return
     */
    public Payroll getPayroll() {
        return (this.payroll);
    }

    public void setPayroll(Payroll aPayroll) {
        this.payroll = aPayroll;
    }

    /**
     * Getter for property eMail.
     *
     * @return Value of property eMail.
     */
    public java.lang.String getEMail() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.eMail).trim();
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
            return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
        }
    }

    /**
     * Setter for property eMail.
     *
     * @param eMail New value of property eMail.
     */
    public void setEMail(java.lang.String eMail) {
        try {
            this.eMail = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(eMail);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object obj) {
        return (this.hashCode() == obj.hashCode());
    }

    @Override
    public int hashCode() {
        return (company.getCompanyID().intValue() * 10000 + getEmployeeID().intValue());
    }

    public long longHashCode() {
        return (company.getCompanyID().longValue() * 1000000 + getEmployeeID().intValue());
    }

    /**
     * Getter for property web_login.
     *
     * @return Value of property web_login.
     */
    public java.lang.String getWeb_login() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.web_login).trim();
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property web_login.
     *
     * @param web_login New value of property web_login.
     */
    public void setWeb_login(java.lang.String web_login) {
        try {
            this.web_login = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(web_login);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property web_pass.
     *
     * @return Value of property web_pass.
     */
    public java.lang.String getWeb_pass() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.web_pass).trim();
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    /**
     * Setter for property web_pass.
     *
     * @param web_pass New value of property web_pass.
     */
    public void setWeb_pass(java.lang.String web_pass) {
        try {
            this.web_pass = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(web_pass);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the current period that the employee is in.
     *
     * @return "-1" If the employee is not linked to a payroll
     */
    public String getCurrentOpenPeriod() {
        return runQuery("select fn_P_GetOpenPeriod(" + getCompany().getCompanyID()
                + "," + this.payroll.getPayrollID() + ")");
    }

    /**
     * Returns employee's PayPoint
     *
     * @return
     */
    public PayPoint getPayPoint() {
        return this.paypoint;
    }

    public void setPayPoint(PayPoint paypoint) {
        this.paypoint = paypoint;
    }

    private PayPoint loadPayPoint() {
        PayPoint result = null;
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select * from p_e_master with (nolock) where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID(), con);
            if (rs.next()) {
                PayPoint payPoint = new PayPoint(getCompany(), new Integer(rs.getInt("PAYPOINT_ID")));
                result = payPoint;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns employee's PayPoint
     *
     * @return
     */
    public String getIDNumber() {
        return this.idNumber;
    }

    public String getTaxNumber() {
        return this.taxNumber;
    }

    public void setIDNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getPassportNumber() {
        if (this.passportNumber == null) {
            this.passportNumber = "";
        }
        return this.passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * Returns a LinkedList of type Passport Country
     *
     * @return
     */
    public LinkedList getCountryName() {
        return this.passportCountry;
    }

    private void setCountryName(LinkedList passportCountry) {
        this.passportCountry = passportCountry;
    }

    public String getCountryOfIssue() {
        return this.countryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        this.countryOfIssue = countryOfIssue;
    }

    public java.lang.String getDoctor() {
        if (doctor == null) {
            return ("");
        }
        if ("".equals(doctor)) {
            return ("");
        } else {
            return doctor;
        }
    }

    public void setDoctor(java.lang.String doctor) {
        this.doctor = doctor;
    }

    public java.lang.String getPhone() {
        if (phone == null) {
            return ("");
        }
        if ("".equals(phone)) {
            return ("");
        } else {
            return phone;
        }
    }

    public void setPhone(java.lang.String phone) {
        this.phone = phone;
    }

//    public String getPhone() {
//        return this.phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
    public java.lang.String getKnownConditions() {
        if (knownCondition == null) {
            return ("");
        }
        if ("".equals(knownCondition)) {
            return ("");
        } else {
            return knownCondition;
        }
    }

    public void setKnownConditions(java.lang.String knownCondition) {
        this.knownCondition = knownCondition;
    }

    public java.lang.String getAllergies() {
        if (allergies == null) {
            return ("");
        }
        if ("".equals(allergies)) {
            return ("");
        } else {
            return allergies;
        }
    }

    public void setAllergies(java.lang.String allergies) {
        this.allergies = allergies;
    }

    private void loadIdAndTaxNumber() {
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select ID_NUMBER, TAX_NUMBER from e_number with (nolock) where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID(), con);
            if (rs.next()) {
                this.idNumber = rs.getString("ID_NUMBER");
                this.taxNumber = rs.getString("TAX_NUMBER");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    //getting the passport from the database
    private void loadPassportDetail() {
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select first PASSPORT_NUMBER, COUNTRY from e_passport with (nolock) inner join A_PASSPORT_COUNTRYLIST with (nolock) on A_PASSPORT_COUNTRYLIST.COUNTRY_CODE = E_PASSPORT.COUNTRY_CODE where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID() + " order by COUNTRY", con);
            if (rs.next()) {
                this.passportNumber = rs.getString("PASSPORT_NUMBER");
                this.countryOfIssue = rs.getString("COUNTRY");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    //get employee's doctor details
    private void loadEmergencyDetails() {
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select coalesce(DOCTOR,''), coalesce(PHONE,''), coalesce(KNOWN_CONDITIONS,''), coalesce(ALLERGIES,'') from e_emergency with (nolock) where company_id =" + getCompany().getCompanyID()
                    //ResultSet rs = DatabaseObject.openSQL("select DOCTOR, PHONE, KNOWN_CONDITIONS, ALLERGIES from E_EMERGENCY where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID(), con);
            if (rs.next()) {

                setDoctor(coalesce(rs.getString("coalesce(e_emergency.DOCTOR,'')"), ""));
                setPhone(coalesce(rs.getString("coalesce(e_emergency.PHONE,'')"), ""));
                setKnownConditions(coalesce(rs.getString("coalesce(e_emergency.KNOWN_CONDITIONS,'')"), ""));
                setAllergies(coalesce(rs.getString("coalesce(e_emergency.ALLERGIES,'')"), ""));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    /**
     * Returns first Date of Employment
     *
     * @return
     */
    public java.util.Date getFirstDOE() {
        return this.firstDOE;
    }

    public void setFirstDOE(java.util.Date firstDOE) {
        this.firstDOE = firstDOE;
    }

    private java.util.Date loadFirstDOE() {
        java.util.Date result = null;
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select min(START_DATE) from e_on_off with (nolock) where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID(), con);
            if (rs.next()) {
                result = rs.getDate(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns first Date of Employment
     *
     * @return
     */
    public java.util.Date getLastDOE() {
        return this.lastDOE;
    }

    private void setLastDOE(java.util.Date lastDOE) {
        this.lastDOE = lastDOE;
    }

    private java.util.Date loadLastDOE() {
        java.util.Date result = null;
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select max(START_DATE) from e_on_off with (nolock) where company_id=" + getCompany().getCompanyID()
                    + " and employee_id=" + getEmployeeID(), con);
            if (rs.next()) {
                result = rs.getDate(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the last payrolldetail_ID that had any calculations done on this
     * employee.
     *
     * @return
     */
    public String getLatestCalculatedPeriod() {
        return this.lastCalculatedPeriod;
    }

    private void setLatestCalculatedPeriod(String lastCalculatedPeriod) {
        this.lastCalculatedPeriod = lastCalculatedPeriod;
    }

    private String loadLatestCalculatedPeriod() {
        if (getPayroll() == null) {
            return "-1";
        }
        return runQuery("select max(payrolldetail_id) from p_calc_employeelist pc with (nolock) where "
                + "Company_id=" + this.getCompany().getCompanyID()
                + " and Employee_id=" + this.getEmployeeID()
                + " and Payroll_id=" + this.getPayroll().getPayrollID()
                + " and run_status=3 and (closed_yn='Y' or exists "
                + " (select * from cp_setup with (nolock) where company_id=pc.company_id and payroll_id=pc.payroll_id "
                + "  and upper(property)=upper('ShowESSPayslip') and upper(value)=upper('Yes')))");
    }

    /**
     * Returns the employee's current Job Position
     *
     * @return
     */
    public String getJobPosition() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.jobPosition);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    private void setJobPosition(String jobPosition) {
        try {
            this.jobPosition = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(jobPosition);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    private String loadJobPosition() {
        return runQuery("select job_position from vw_J_EMP_JOBDETAIL with (nolock) where "
                + "Company_id=" + this.getCompany().getCompanyID()
                + " and Employee_id=" + this.getEmployeeID());
    }

    /**
     * Returns the employee's current Job Grade Level
     *
     * @return
     */
    public String getJobGradeLevel() {
        try {
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.jobGradeLevel);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }

    private void setJobGradeLevel(String jobGradeLevel) {
        try {
            this.jobGradeLevel = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(jobGradeLevel);
        } catch (StringEncrypterSingleton.EncryptionException e) {
            e.printStackTrace();
        }
    }

    private String loadJobGradeLevel() {
        return runQuery("select grade_level from vw_J_EMP_JOBDETAIL with (nolock) where "
                + "Company_id=" + this.getCompany().getCompanyID()
                + " and Employee_id=" + this.getEmployeeID());
    }

    /**
     * Returns the Contact Information
     *
     * @return
     */
    public Contact getContact() {
        return this.contact;
    }

    private void setContact(Contact contact) {
        this.contact = contact;
    }

    private Contact loadContact() {
        return (new Contact(this));
    }

    /**
     * Returns a LinkedList of type BankAccount
     *
     * @return
     */
    public LinkedList getBankAccounts() {
        return this.bankAccounts;
    }

    private void setBankAccounts(LinkedList bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    private LinkedList loadBankAccounts() {
        LinkedList result = new LinkedList();
        Connection con = DatabaseObject.getNewConnectionFromPool();

        try {
            ResultSet rs = DatabaseObject.openSQL("select account_id from e_bankdet with (nolock) where "
                    + "Company_id=" + this.getCompany().getCompanyID()
                    + " and Employee_id=" + this.getEmployeeID()
                    + " order by account_name", con);
            while (rs.next()) {
                BankAccount account = new BankAccount(this, rs.getInt(1));
                result.add(account);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns a LinkedList of type Loan
     *
     * @return
     */
    public LinkedList getLoans() {
        return this.loans;
    }

    private void setLoans(LinkedList loans) {
        this.loans = loans;
    }

    /**
     * Returns a LinkedList, comprising Strings in the format yyyy/yyyy
     *
     * @return String - TAX_YEAR_START/TAXY_EAR_END
     */
    public LinkedList getArchivedPayslipTaxYears() {
        return this.archivedPayslipTaxYears;
    }

    private LinkedList loadLoans() {
        LinkedList result = new LinkedList();
        Connection con = DatabaseObject.getNewConnectionFromPool();

        try {
            ResultSet rs = DatabaseObject.openSQL("select emloan_id from pl_emaster_loan with (nolock) where "
                    + "Company_id=" + this.getCompany().getCompanyID()
                    + " and Employee_id=" + this.getEmployeeID()
                    + " order by LOAN_DATE", con);
            while (rs.next()) {
                Loan loan = new Loan(this, rs.getInt(1));
                result.add(loan);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns a LinkedList of type Family
     *
     * @return
     */
    public LinkedList getFamilyMembers() {
        return this.familyMembers;
    }

    private void setFamilyMembers(LinkedList familyMembers) {
        this.familyMembers = familyMembers;
    }

    private LinkedList loadFamilyMembers() {
        LinkedList result = new LinkedList();
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select family_id from e_family with (nolock) where "
                    + "Company_id=" + this.getCompany().getCompanyID()
                    + " and Employee_id=" + this.getEmployeeID()
                    + " order by birthdate", con);
            while (rs.next()) {
                Family family = new Family(this, new Integer(rs.getInt(1)));
                result.add(family);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * For payslips from previous Tax Years, it is important to retreive a list
     * of years for which those payslips are available. Returns a LinkedList
     * with Strings containing the TAX_YEAR_START/TAX_YEAR_END date found in the
     * archive tables for this employee The format of the result is yyyy/yyyy
     * i.e. '2004/2005'
     */
    private void loadArchivedPayslipTaxYears() {
        LinkedList rslt = new LinkedList();

        // Get List of archived records in P_PAYLOG_OLD
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            if ((getPayroll() != null) && (getPayroll().getPayrollID() != null)) {
                java.sql.ResultSet rs = DatabaseObject.openSQL(
                        "select distinct datepart(yy,TAX_YEAR_START)||'/'||datepart(yy,TAX_YEAR_END) as PERIOD from p_paylog_old with (nolock) "
                        + " where company_id=" + getCompany().getCompanyID()
                        + " and employee_id=" + getEmployeeID() + " and "
                        + " payroll_id=" + getPayroll().getPayrollID() + " order by PERIOD desc ", con);
                while (rs.next()) {
                    rslt.add(rs.getString("PERIOD"));
                }
            }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        archivedPayslipTaxYears = rslt;
    }

    /**
     * Used to return a single String value from a SQL select statement
     */
    private String runQuery(String query) {
        String result = "";
        Connection con = DatabaseObject.getNewConnectionFromPool();

        try {
            // Get open period
            ResultSet rs = DatabaseObject.openSQL(query, con);
            if (rs.next()) {
                result = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    // Fingerprint of previously compatible version
    /*
     * In order to extract this UID, run the following command in the folder
     * where the current jwt.jar file resides BEFORE changing the contents of
     * the class. C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver
     * -classpath lwt.jar za.co.ucs.accsys.peopleware.Employee
     * za.co.ucs.accsys.peopleware.Employee: static final long serialVersionUID
     * = -6645819543893844770L;
     *
     */
    //static final long serialVersionUID = -6645819543893844770L;
    static final long serialVersionUID = -6645819543893844770L;
    private String race;
    private PayPoint paypoint;
    private Company company;
    private Integer employeeID;
    private Integer personID;
    private java.util.Date birthDate;
    private java.util.Date firstDOE;
    private java.util.Date lastDOE;
    private String lastCalculatedPeriod;
    private String idNumber;
    private String taxNumber;
    private String jobPosition; //#Encrypted
    private String jobGradeLevel;//#Encrypted
    private Contact contact;
    private String employeeNumber;//#Encrypted
    private CostCentre costCentre;
    private boolean hasCriminalRecord;
    private boolean wasInsolvent;
    private String firstName;//#Encrypted
    private String gender;  //#Encrypted
    private java.util.Date groupJoinDate;
    private String hobbies;//#Encrypted
    private String homeLanguage;//#Encrypted
    private String initials;//#Encrypted
    private String maritalStatus;//#Encrypted
    private String preferredName;//#Encrypted
    private String religion;//#Encrypted
    private String secondName;//#Encrypted
    private String surname;//#Encrypted
    private String title;//#Encrypted
    private Payroll payroll;
    private String eMail;//#Encrypted
    private String web_login;//#Encrypted
    private String web_pass;//#Encrypted
    private String rop1;//#Encrypted
    private String rop2;//#Encrypted
    private String rop3;//#Encrypted
    private String payper1;//#Encrypted
    private String payper2;//#Encrypted
    private String payper3;//#Encrypted
    private String totalPackage;//#Encrypted
    private float hoursPerDay = 0;
    private float daysPerMonth = 0;
    private LinkedList bankAccounts;
    private LinkedList loans;
    private LinkedList familyMembers;
    private LinkedHashMap payslips = null; // List of PayslipInfo classes
    private LinkedList passportCountry;
    private String passportNumber;
    private String countryOfIssue;
    private String doctor;
    private String phone;
    private String workPhone;
    private String homePhone;
    private String workFax;
    private String homeFax;
    private String cell;
    private String knownCondition;
    private String allergies;
    private LinkedList archivedPayslipTaxYears;

    /**
     * @return the race
     */
    public String getRace() {
        return race;
    }

    /**
     * @param race the race to set
     */
    public void setRace(String race) {
        this.race = race;
    }

    /**
     * @return the personID
     */
    public Integer getPersonID() {
        return personID;
    }

    /**
     * @param personID the personID to set
     */
    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public boolean isPayslipsLoaded() {
        return (this.payslips != null);
    }

    public void reloadPayslips() {
        this.payslips = new LinkedHashMap();

        java.sql.Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();

            if (this.getPayroll() != null) {
                // Get Periods and RunTypes
                java.sql.ResultSet rs = DatabaseObject.openSQL("select distinct PAYROLLDETAIL_ID, RUNTYPE_ID from "
                        + " p_calc_employeelist pc with (nolock) where "
                        + " Company_id=" + this.getCompany().getCompanyID()
                        + " and Employee_id=" + this.getEmployeeID()
                        + " and Payroll_id=" + this.getPayroll().getPayrollID()
                        + " and run_status=3 and (closed_yn='Y' or exists "
                        + " (select * from cp_setup with (nolock) where company_id=pc.company_id and payroll_id=pc.payroll_id "
                        + "  and upper(property)=upper('ShowESSPayslip') and upper(value)=upper('Yes'))) order by payrolldetail_id", con);
                while (rs.next()) {
                    PayslipInfo payslipInfo = new PayslipInfo(this, rs.getInt("PAYROLLDETAIL_ID"), rs.getInt("RUNTYPE_ID"));
                    StringBuilder payslipKey = new StringBuilder();
                    payslipKey.append(this.hashCode()).append(this.getPayroll().getPayrollID()).append(rs.getInt("PAYROLLDETAIL_ID")).append(rs.getInt("RUNTYPE_ID"));
                    payslips.put(payslipKey, payslipInfo);
                }
                rs.close();
            }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }

    }

    private static <T> T coalesce(T a, T b) {
        return a != null ? a : b;
    }

    /**
     * @return the workPhone
     */
    public String getWorkPhone() {
        return workPhone;
    }

    /**
     * @param workPhone the workPhone to set
     */
    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    /**
     * @return the homePhone
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * @param homePhone the homePhone to set
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * @return the workFax
     */
    public String getWorkFax() {
        return workFax;
    }

    /**
     * @param workFax the workFax to set
     */
    public void setWorkFax(String workFax) {
        this.workFax = workFax;
    }

    /**
     * @return the homeFax
     */
    public String getHomeFax() {
        return homeFax;
    }

    /**
     * @param homeFax the homeFax to set
     */
    public void setHomeFax(String homeFax) {
        this.homeFax = homeFax;
    }

    /**
     * @return the cell
     */
    public String getCell() {
        return cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(String cell) {
        this.cell = cell;
    }

    /**
     * This function returns 'true' if the employee is Discharged
     *
     * @param employee
     * @return
     */
    public boolean isEmployeeDischarged() {
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        java.sql.ResultSet rslt;
        Date startDate = null;
        try {
            rslt = DatabaseObject.openSQL("select FIRST START_DATE from E_ON_OFF with (nolock) where END_DATE is null and COMPANY_ID = " + 
                                          company.getCompanyID() +  " and EMPLOYEE_ID = " + getEmployeeID() + " order by START_DATE desc", con);
            while (rslt.next()) {
                startDate = rslt.getDate("START_DATE");
            }

        } catch (java.sql.SQLException e) {
            System.out.println("******************************************");
            System.out.println("Discharged employee = " + this.firstName + ", " + this.surname + " [" + this.employeeNumber + "]");
            System.out.println("******************************************");
            return true;
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        
        return startDate == null;

    }
}
