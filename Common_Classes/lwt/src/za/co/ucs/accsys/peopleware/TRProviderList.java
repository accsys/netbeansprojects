/*
 * TRProviderList.java
 *
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;
import java.util.LinkedList;

/**
 * TRProviderList lists  to dba.SD_PROVIDER
 * @Author  liam
 */
public final class TRProviderList {

    /** 
     * Creates a LinkedList of all training providers
     */
    public TRProviderList() {
        providerList = new LinkedList();
        loadFromDB();
    }

    public LinkedList getList(){
        return providerList;
    }

    public void loadFromDB() {
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("SELECT PROVIDER_ID FROM TR_PROVIDER order by NAME", con);
            while (rs.next()) {
                int providerID = (new Integer(rs.getInt("PROVIDER_ID")));
                TRProvider provider = new TRProvider(providerID);
                providerList.add(provider);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    private java.util.LinkedList providerList;
}
