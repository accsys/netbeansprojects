/*
 * LeaveHistory.java
 *
 * Created on 20 April 2005, 08:07
 */

package za.co.ucs.accsys.peopleware;

import java.sql.*;
import java.util.Date;
import za.co.ucs.lwt.db.*;

/**
 * The LeaveHistory object is used to store one entry
 * of leave taken by the employee
 * @author  lwt
 */
public class LeaveHistoryEntry implements java.io.Serializable {
    
    /** Creates a new instance of LeaveHistory
     * @param historyID */
    public LeaveHistoryEntry(String historyID) {
        this.historyID = historyID;
        loadFromDB();
    }
    
    
    private void loadFromDB(){
        // Run SQL Statement
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select * from l_history with (nolock) where history_id="+
            this.historyID, con);
            while (rs.next()){
                // Add History detail
                this.fromDate = rs.getDate("START_DATE");
                this.toDate = rs.getDate("END_DATE");
                this.companyID = rs.getString("COMPANY_ID");
                this.employeeID = rs.getString("EMPLOYEE_ID");
                this.unitsTaken = rs.getFloat("NB_UNIT"); 
                this.illnessID = rs.getString("ILLNESS_ID");
                this.incidentID = rs.getString("INCIDENT_ID");
                this.reason = rs.getString("REASON");
                this.leaveTypeID = rs.getString("LEAVE_TYPE_ID");
                this.timeStamp = rs.getDate("TIME_STAMP");
                this.unit = rs.getString("UNIT");
            }

            rs.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        finally{
            DatabaseObject.releaseConnection(con);
        }
        
    }
    
    /** Getter for property companyID.
     * @return Value of property companyID.
     *
     */
    public java.lang.String getCompanyID() {
        return companyID;
    }
    
    
    /** Getter for property employeeID.
     * @return Value of property employeeID.
     *
     */
    public java.lang.String getEmployeeID() {
        return employeeID;
    }
    
    
    /** Getter for property fromDate.
     * @return Value of property fromDate.
     *
     */
    public java.util.Date getFromDate() {
        return fromDate;
    }
    
    
    /** Getter for property historyID.
     * @return Value of property historyID.
     *
     */
    public java.lang.String getHistoryID() {
        return historyID;
    }
    
    
    /** Getter for property illnessID.
     * @return Value of property illnessID.
     *
     */
    public java.lang.String getIllnessID() {
        return illnessID;
    }
    
    
    /** Getter for property incidentID.
     * @return Value of property incidentID.
     *
     */
    public java.lang.String getIncidentID() {
        return incidentID;
    }
    
    
    /** Getter for property leaveTypeID.
     * @return Value of property leaveTypeID.
     *
     */
    public java.lang.String getLeaveTypeID() {
        return leaveTypeID;
    }
    
    
    
    /** Getter for property reason.
     * @return Value of property reason.
     *
     */
    public java.lang.String getReason() {
        return reason;
    }
    
    
    /** Getter for property timeStamp.
     * @return Value of property timeStamp.
     *
     */
    public java.util.Date getTimeStamp() {
        return timeStamp;
    }
    
    
    /** Getter for property toDate.
     * @return Value of property toDate.
     *
     */
    public java.util.Date getToDate() {
        return toDate;
    }
    
    
    /** Getter for property unit.
     * @return Value of property unit.
     *
     */
    public java.lang.String getUnit() {
        return unit;
    }
    
    
    /** Getter for property unitsTaken.
     * @return Value of property unitsTaken.
     *
     */
    public float getUnitsTaken() {
        return unitsTaken;
    }
    
    
    private Date fromDate;
    private Date toDate;
    private final String historyID;
    private String companyID;
    private String employeeID;
    private float unitsTaken;
    private String illnessID;
    private String incidentID;
    private String reason;
    private String leaveTypeID;
    private Date timeStamp;
    private String unit;
}
