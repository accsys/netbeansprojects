/*
 * ArchivedPayslipInfo.java
 *
 * Created on January 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import java.util.*;
import za.co.ucs.accsys.tools.*;
import za.co.ucs.lwt.db.*;

/**
 * ArchivedPayslipInfo inherits from PayslipInfo and gathers all the payslip-related information from previous tax years
 *
 * @author liam
 */
public final class ArchivedPayslipInfo extends PayslipInfo implements AccsysReadOnlyObjectInterface, java.io.Serializable {
    
    /**
     * Creates a new instance of ArchivedPayslipInfo
     *
     * @param employee The employee for which Payslip information is required
     * @param payrollDetail_id The period for which Payslip information is required
     * @param runtype_id Yep, you guessed it.  The Runtype of the Payslip information
     * @param taxYearStart is the start date (yyyy. i.e. '2006') of the Tax Year we are extracting from the archives
     */
    public ArchivedPayslipInfo(Employee employee, int payrollDetail_id, int runtype_id, String taxYearStart) {
        super(employee, payrollDetail_id, runtype_id);
        // The super constructor might have loaded values that will be reloaded in the extended class
        getEarnings().clear();
        getDeductions().clear();
        getOthers().clear();
        getYTDs().clear();
        
        if (isValid())
            loadFromDB(taxYearStart);
    }
    
    
    /**
     * 
     * @param taxYearStart
     */
    public void loadFromDB(String taxYearStart) {
        this.taxYearStart = taxYearStart;
        if ((this.getEmployee() == null)|| (this.getPayrollDetail_id()==-1) || (this.getRuntype_id()==-1))
            return;
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                // EARNINGS
                ResultSet rs = DatabaseObject.openSQL("select pv.QTY_THEVALUE as QTY_VALUE, v.description as DESCRIPTION, last_calc_value as VALUE "+
                        "from p_variables_for_employee_old pv, p_variable v with (nolock) "+
                        " where pv.variable_id=v.variable_id and v.result_type='EARNING' and v.COST_DETAIL in('YY','YN') "+
                        " and pv.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pv.Employee_id="+getEmployee().getEmployeeID()+
                        " and pv.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pv.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pv.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pv.TAX_YEAR_START)='"+taxYearStart+"' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getFloat("QTY_VALUE"),
                            rs.getString("DESCRIPTION"), rs.getFloat("VALUE"));
                    getEarnings().add(variableDetail);
                }
                rs.close();
                
                // DEDUCTIONS
                rs = DatabaseObject.openSQL("select v.description as DESCRIPTION, last_calc_value as VALUE "+
                        "from p_variables_for_employee_old pv  with (NOLOCK) , p_variable v  with (NOLOCK) "+
                        " where pv.variable_id=v.variable_id and v.result_type='DEDUCTION' and "+
                        " v.COST_DETAIL like 'Y_'  "+
                        " and pv.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pv.Employee_id="+getEmployee().getEmployeeID()+
                        " and pv.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pv.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pv.Runtype_id="+getRuntype_id()+
                        " and upper(V.NAME) not in('TAX','TAXONBONUS') "+
                        " and datepart(yy,pv.TAX_YEAR_START)='"+taxYearStart+"' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getDeductions().add(variableDetail);
                }
                rs.close();
                
                // DEDUCTIONS - TAX & TOB
                rs = DatabaseObject.openSQL("select sum(calculated_tax) as VALUE "+
                        "from p_paylog_old pl  with (NOLOCK)  "+
                        " where pl.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pl.Employee_id="+getEmployee().getEmployeeID()+
                        " and pl.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pl.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pl.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pl.TAX_YEAR_START)='"+taxYearStart+"' ", con);
                while (rs.next()){
                    if (rs.getFloat("VALUE")!=0){
                        VariableDetail variableDetail = new VariableDetail("TAX",
                                rs.getFloat("VALUE"));
                        getDeductions().add(variableDetail);
                    }
                }
                rs.close();
                rs = DatabaseObject.openSQL("select sum(calculated_tax_on_bonus) as VALUE "+
                        "from p_paylog_old pl  with (NOLOCK) "+
                        " where pl.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pl.Employee_id="+getEmployee().getEmployeeID()+
                        " and pl.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pl.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pl.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pl.TAX_YEAR_START)='"+taxYearStart+"' ", con);
                while (rs.next()){
                    if (rs.getFloat("VALUE")!=0){
                        VariableDetail variableDetail = new VariableDetail("TAX ON BONUS",
                                rs.getFloat("VALUE"));
                        getDeductions().add(variableDetail);
                    }
                }
                rs.close();
                rs = DatabaseObject.openSQL("select sum(theAdditionalTax) as VALUE "+
                        "from p_paylog_old pl  with (NOLOCK) "+
                        " where pl.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pl.Employee_id="+getEmployee().getEmployeeID()+
                        " and pl.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pl.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pl.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pl.TAX_YEAR_START)='"+taxYearStart+"' ", con);
                while (rs.next()){
                    if (rs.getFloat("VALUE")!=0){
                        VariableDetail variableDetail = new VariableDetail("ADDITIONAL TAX",
                                rs.getFloat("VALUE"));
                        getDeductions().add(variableDetail);
                    }
                }
                rs.close();
                
                // DEDUCTIONS - LOANS
                rs = DatabaseObject.openSQL("select P.NAME as DESCRIPTION,sum(L.AMMOUNT_DEDUCTED) as VALUE "+
                        "from DBA.P_LOAN as P  with (NOLOCK) ,DBA.PL_EMASTER_LOAN_old as PL  with (NOLOCK) ,DBA.P_LOAN_LOG_old as L  with (NOLOCK) "+
                        " where P.LOAN_ID=PL.LOAN_ID and PL.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and PL.Employee_id="+getEmployee().getEmployeeID()+
                        " and PL.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and L.PayrollDetail_id="+getPayrollDetail_id()+
                        " and L.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,L.TAX_YEAR_START)='"+taxYearStart+"' "+
                        " and datepart(yy,PL.TAX_YEAR_START)='"+taxYearStart+"' "+
                        " and L.emloan_id=pl.emloan_id group by P.NAME order by P.NAME", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getDeductions().add(variableDetail);
                }
                rs.close();
                
                // DEDUCTIONS - SAVINGS
                rs = DatabaseObject.openSQL("select P.NAME as DESCRIPTION,sum(L.AMMOUNT_SAVED) as VALUE "+
                        "from DBA.P_SAVING as P with (NOLOCK) ,DBA.PL_EMASTER_SAVING_old as PL with (NOLOCK) ,DBA.P_SAVING_LOG_old as L with (NOLOCK)  "+
                        " where P.SAVING_ID=PL.SAVING_ID and PL.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and PL.Employee_id="+getEmployee().getEmployeeID()+
                        " and PL.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and L.PayrollDetail_id="+getPayrollDetail_id()+
                        " and L.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,L.TAX_YEAR_START)='"+taxYearStart+"' "+
                        " and datepart(yy,PL.TAX_YEAR_START)='"+taxYearStart+"' "+
                        " and L.emsaving_id=PL.emsaving_id group by P.NAME order by P.NAME", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getDeductions().add(variableDetail);
                }
                rs.close();
                
                // OTHERS - COMPANY CONTRIBUTIONS
                rs = DatabaseObject.openSQL("select v.description as DESCRIPTION, last_calc_value as VALUE "+
                        "from p_variables_for_employee_old pv with (NOLOCK) , p_variable v  with (NOLOCK) "+
                        " where pv.variable_id=v.variable_id and v.result_type='EARNING' and v.COST_DETAIL in('NY') "+
                        " and pv.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pv.Employee_id="+getEmployee().getEmployeeID()+
                        " and pv.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pv.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pv.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pv.TAX_YEAR_START)='"+taxYearStart+"' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getOthers().add(variableDetail);
                }
                rs.close();
                
                // OTHERS - INTERIMS
                rs = DatabaseObject.openSQL("select v.description as DESCRIPTION, last_calc_value as VALUE "+
                        "from p_variables_for_employee_old pv with (NOLOCK) , p_variable v  with (NOLOCK) "+
                        " where pv.variable_id=v.variable_id and v.result_type='INTERIM'  "+
                        " and pv.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pv.Employee_id="+getEmployee().getEmployeeID()+
                        " and pv.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pv.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pv.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pv.TAX_YEAR_START)='"+taxYearStart+"' " +
                        " and v.DISPLAYONPAYSLIP_YN='Y' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getOthers().add(variableDetail);
                }
                rs.close();
                
                // OTHERS - NOT PAID EARNINGS/DEDUCTIONS
                rs = DatabaseObject.openSQL("select pv.QTY_THEVALUE as QTY_VALUE, v.description as DESCRIPTION, last_calc_value as VALUE "+
                        "from p_variables_for_employee_old pv with (NOLOCK) , p_variable v  with (NOLOCK) "+
                        " where pv.variable_id=v.variable_id and v.result_type in ('EARNING','DEDUCTION') and v.COST_DETAIL = 'NN' "+
                        " and pv.Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and pv.Employee_id="+getEmployee().getEmployeeID()+
                        " and pv.Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and pv.PayrollDetail_id="+getPayrollDetail_id()+
                        " and pv.Runtype_id="+getRuntype_id()+
                        " and datepart(yy,pv.TAX_YEAR_START)='"+taxYearStart+"' "+
                        " and v.DISPLAYONPAYSLIP_YN='Y' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getFloat("QTY_VALUE"),
                            rs.getString("DESCRIPTION"), rs.getFloat("VALUE"));
                    getOthers().add(variableDetail);
                }
                rs.close();
                
                // YTD TOTALS
                rs = DatabaseObject.openSQL("select DESCRIPTION, VALUE from "+
                        "P_PAYLOG_YTD_old  with (NOLOCK) where Company_id="+getEmployee().getCompany().getCompanyID()+
                        " and Employee_id="+getEmployee().getEmployeeID()+
                        " and Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+getPayrollDetail_id()+
                        " and Runtype_id="+getRuntype_id()+
                        " and datepart(yy,TAX_YEAR_START)='"+taxYearStart+"' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    getYTDs().add(variableDetail);
                }
                rs.close();
                
                
                // Nett Pay
                try{
                    this.nettPay = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt( getArchivedNettPay());
                } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
                
                // RunTYpe
                rs = DatabaseObject.openSQL("select RUNTYPE from p_runtype where runtype_id="+getRuntype_id(), con);
                if (rs.next()){
                    this.runType = rs.getString(1);
                }
                rs.close();
                
                // RATE_OF_PAY1
                setRateOfPay1( "[Archived]");
                
                // Start and End Dates
                rs = DatabaseObject.openSQL("select fn_P_EmploymentStart"+
                        "("+getEmployee().getCompany().getCompanyID()+
                        ","+getEmployee().getEmployeeID()+
                        ") as StartDate, fn_P_EmploymentEnd_Dynamic"+
                        "("+getEmployee().getCompany().getCompanyID()+
                        ","+getEmployee().getEmployeeID()+
                        ") as EndDate ",con);
                if (rs.next()){
                    setStartDate( rs.getString("StartDate"));
                    setEndDate( rs.getString("EndDate"));
                }
                
                setCostCentreName( "[Archived]");
                setJobPosition( "[Archived]");
                setPayMethod( "[Archived]" );
                setAccountNumber( "[Archived]");
                
                
            } catch (SQLException e){
                e.printStackTrace();
            } finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    
    
    /** The easiest way of determining the NettPay at this point in time, is
     * by adding the already collated earnings and deductions
     */
    private String getArchivedNettPay(){
        float netEarnings=0;
        float netDeductions=0;
        float rslt=0;
        // Earnings
        Iterator iter = getEarnings().iterator();
        while (iter.hasNext()){
            VariableDetail earning = (VariableDetail)iter.next();
            netEarnings = netEarnings + earning.getValue();
        }
        // Deductions
        iter = getDeductions().iterator();
        while (iter.hasNext()){
            VariableDetail deduction = (VariableDetail)iter.next();
            netDeductions = netDeductions + deduction.getValue();
        }
        rslt = netEarnings - netDeductions;
        if (rslt>=0) {
            return new Float(rslt).toString();
        } else {
            return "0.00";
            
        }
    }
    
    /** Returns true if the employee has already been closed for the given period
     * For archived tables, we can safely assume this to be true
     */
    public boolean isClosedForEmployee(){
        return true;
    }
    
    
    /** Returns the payroll periods that the employee has been calculated for.
     * Note that this is bound to the specific runtype that the PayslipInfo object were
     * created with.
     * @return LinkedList containing ArchivedPayrollDetail objects
     */
    public LinkedList<ArchivedPayrollDetail> getCalculatedPeriods(){
        LinkedList result = new LinkedList();
        
        Connection con = null;
        ResultSet rs = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            // EARNINGS
            rs = DatabaseObject.openSQL("select distinct payrolldetail_id from P_VARIABLES_FOR_EMPLOYEE_OLD where "+
                    "Company_id="+getEmployee().getCompany().getCompanyID()+
                    " and Employee_id="+getEmployee().getEmployeeID()+
                    " and Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                    " and Runtype_id="+getRuntype_id()+
                    " and Last_Calc_Value<>0 "+
                    " and datepart(yy,TAX_YEAR_START)="+taxYearStart+" order by payrolldetail_id", con);
            
            while (rs.next()){
                ArchivedPayrollDetail payrollDetail = new ArchivedPayrollDetail(getEmployee().getPayroll(), new Integer(rs.getInt(1)), this.taxYearStart);
                result.add(payrollDetail);
            }
            rs.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    
    
    /** Returns true if the employee has already been calculated for the given period.
     * @return 
     */
    @Override
    public boolean isCalculatedForEmployee(){
        boolean result = false;
        
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            // EARNINGS
            ResultSet rs = DatabaseObject.openSQL("select count(*) from p_variables_for_employee_old  with (NOLOCK) where "+
                    "Company_id="+getEmployee().getCompany().getCompanyID()+
                    " and Employee_id="+getEmployee().getEmployeeID()+
                    " and Payroll_id="+getEmployee().getPayroll().getPayrollID()+
                    " and Payrolldetail_id="+getPayrollDetail_id()+
                    " and Runtype_id="+getRuntype_id()+
                    " and last_calc_value<>0"+
                    " and datepart(yy,TAX_YEAR_START)='"+taxYearStart+"'",con);
            
            if (rs.next()){
                result =  (rs.getInt(1)>0);
            }
            rs.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    
    public String getTaxYearStart(){
        return this.taxYearStart;
    }
    
    private String taxYearStart;
    
    
}



