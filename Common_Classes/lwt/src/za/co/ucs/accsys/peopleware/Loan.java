/*
 * Loan.java
 *
 * Created on July 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Loan contains the detail of a loan taken out by the employee
 * @author  liam
 */
public class Loan implements AccsysReadOnlyObjectInterface, java.io.Serializable {

    /** Creates a new instance of Contact
     */
    public Loan(Employee employee, int emloanID) {
        this.employee = employee;
        this.emloanID = new Integer(emloanID);
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void loadFromDB() {
        if (this.employee == null) {
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // PL_EMASTER_LOAN
                //
                ResultSet rs = DatabaseObject.openSQL("select pl.*, l.name as LOANNAME from pl_emaster_loan pl with (nolock), p_loan l with (nolock) where " +
                        " Company_id=" + employee.getCompany().getCompanyID() +
                        " and Employee_id=" + employee.getEmployeeID() +
                        " and pl.emloan_id=" + this.emloanID +
                        " and pl.loan_id=l.loan_id", con);
                if (rs.next()) {
                    loanID = new Integer(rs.getInt("LOAN_ID"));
                    loanName = rs.getString("LOANNAME");
                    loanDate = rs.getDate("LOAN_DATE");
                    loanAmount = rs.getFloat("LOAN_AMMOUNT");
                    premium = rs.getFloat("PREMIUM");
                    tempPremium = rs.getFloat("PREMIUM_TEMP");
                    referenceNumber = rs.getString("REFERENCE");
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public  boolean isValid() {
        return (((Employee) this.employee).isValid());
    }

    public java.lang.Integer getEmloanID() {
        return emloanID;
    }

    public za.co.ucs.accsys.peopleware.Employee getEmployee() {
        return employee;
    }

    public float getLoanAmount() {
        return loanAmount;
    }

    public java.util.Date getLoanDate() {
        return loanDate;
    }

    public java.lang.Integer getLoanID() {
        return loanID;
    }

    public java.lang.String getLoanName() {
        return loanName;
    }

    public float getPremium() {
        return premium;
    }

    public java.lang.String getReferenceNumber() {
        return referenceNumber;
    }

    public float getTempPremium() {
        return tempPremium;
    }
    private Employee employee;
    private Integer emloanID;
    private Integer loanID;
    private String loanName = "";
    private java.util.Date loanDate;
    private float loanAmount;
    private float premium;
    private float tempPremium;
    private String referenceNumber = "";
}

