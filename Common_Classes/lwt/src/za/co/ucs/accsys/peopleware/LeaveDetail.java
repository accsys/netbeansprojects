/*
 * LeaveDetail.java
 *
 * Created on 20 April 2005, 08:03
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import java.util.*;
import za.co.ucs.lwt.db.*;
/**
 *
 * @author  lwt
 */
/** Private class that contains the balance information for a given employee
 */
public class LeaveDetail implements java.io.Serializable{
    
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
        C:\Apache5\shared\lib>C:\Program Files\Java\jdk1.6.0_13\bin\serialver -classpath lwt.jar za.co.ucs.accsys.peopleware.LeaveDetail
        za.co.ucs.accsys.peopleware.LeaveDetail:    static final long serialVersionUID = 4598128384581556719L;
     
     */
    static final long serialVersionUID =  4598128384581556719L;
    
    /** Creates an object that contains the detail of each leave type linked to this employee.
     * @param employee
     * @param profileID
     * @param profileName
     * @param leaveTypeID
     * @param leaveType
     * @param leaveCaption
     * @param currentBalance
     * @param unit
     */
    public LeaveDetail(Employee employee, int profileID, String profileName, int leaveTypeID, String leaveType, String leaveCaption, float currentBalance, String unit){
        this.employee = employee;
        this.profileID = profileID;
        this.leaveTypeID = leaveTypeID;
        this.profileName = profileName.trim();
        this.leaveType = leaveType.trim();
        this.currentBalance = currentBalance;
        this.unit = unit;
        this.leaveHistory = loadLeaveHistory();
        this.leaveReasons = loadLeaveReasons();
        this.leaveCaption = leaveCaption;
    }
    
    /** Loads the history of the specific leave type
     * into a LinkedList of LeaveHistoryEntry's
     */
    @SuppressWarnings("static-access")
    private LinkedList<LeaveHistoryEntry> loadLeaveHistory(){
        LinkedList resultSet = new LinkedList();
        
        // Run SQL Statement
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select history_id from "+
                    "l_history l where l.Company_id="+this.employee.getCompany().getCompanyID()+
                    " and l.Employee_id="+this.employee.getEmployeeID()+" and l.LEAVE_TYPE_ID="+leaveTypeID+
                    " and cancel_date is null and convert_date is null "+
                    " order by START_DATE desc", con);
            while (rs.next()){
                LeaveHistoryEntry entry = new LeaveHistoryEntry(rs.getString("HISTORY_ID"));
                resultSet.add(entry);
            }
            rs.close();
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        
        return resultSet;
    }
    
    @SuppressWarnings("static-access")
    private String[][] loadLeaveReasons(){
        // Sick, Family, Special and Unpaid
        String sqlString = "";
        if (leaveType.trim().compareToIgnoreCase("Sick")==0){
            sqlString="select illness_id, description from l_illness_index order by description";
        }
        if (leaveType.trim().compareToIgnoreCase("Family")==0){

            sqlString="select incident_id, incident from ll_properties_lookup where profile_id in "+
                    "(select profile_id from ll_emaster_lprofile where company_id="+this.employee.getCompany().getCompanyID()+
                    "and employee_id="+this.employee.getEmployeeID()+
                    " and leave_type_id in (select leave_type_id from l_leave_type where name='Family')) order by incident";
                    
        }
        if (leaveType.trim().compareToIgnoreCase("Special")==0){
            sqlString="select incident_id, incident from ll_properties_lookup where profile_id in "+
                    "(select profile_id from ll_emaster_lprofile where company_id="+this.employee.getCompany().getCompanyID()+
                    "and employee_id="+this.employee.getEmployeeID()+
                    " and leave_type_id in (select leave_type_id from l_leave_type where name='Special')) order by incident";
        }
        if (leaveType.trim().compareToIgnoreCase("Unpaid")==0){
            sqlString="select incident_id, incident from ll_properties_lookup where profile_id in "+
                    "(select profile_id from ll_emaster_lprofile where company_id="+this.employee.getCompany().getCompanyID()+
                    "and employee_id="+this.employee.getEmployeeID()+
                    " and leave_type_id in (select leave_type_id from l_leave_type where name='Unpaid')) order by incident";
        }
        // All other leave types, return an empty array
        if (sqlString.length()==0){
            return (new String[0][0]);
        }
        
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            LinkedList linkedList = new LinkedList();
            while (rs.next()){
                // Add Reason
                String[] val = new String[2];
                val[0]=rs.getString(1);
                val[1]=rs.getString(2);
                linkedList.add(val);
            }
            rs.close();
            //Cast LinkedList to an array of type String[]
            return ((String[][])(linkedList.toArray(new String[1][1])));
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        
        return (new String[0][0]);
    }
    
    /** Getter for property profileName.
     * @return Value of property profileName.
     *
     */
    public java.lang.String getProfileName() {
        return profileName;
    }
    
    public String getLeaveCaption(){
        return this.leaveCaption;
    }
    
    /** Getter for property leaveType.
     * @return Value of property leaveType.
     *
     */
    public java.lang.String getLeaveType() {
        return leaveType;
    }
    
    /** Getter for property profileID.
     * @return Value of property profileID.
     *
     */
    public int getProfileID() {
        return profileID;
    }
    
    /** Getter for property leaveTypeID.
     * @return Value of property leaveTypeID.
     *
     */
    public int getLeaveTypeID() {
        return leaveTypeID;
    }
    
    /** Getter for property currentBalance.
     * @return Value of property currentBalance.
     *
     */
    public float getCurrentBalance() {
        return currentBalance;
    }
    
    /** Getter for property unit.
     * @return Value of property unit.
     *
     */
    public java.lang.String getUnit() {
        return unit;
    }
    
    /** Getter for property leaveHistory.
     * @return Value of property leaveHistory.
     *
     */
    public java.util.LinkedList getLeaveHistory() {
        return leaveHistory;
    }
    
    /** Getter for property leaveReasons.
     * @return Value of property leaveReasons.
     *
     */
    public java.lang.String[][] getLeaveReasons() {
        return this.leaveReasons;
    }
    
    
    private final Employee employee;
    private final int profileID;
    private final String profileName;
    private final int leaveTypeID;
    private final String leaveType;
    private final String leaveCaption;
    private final float currentBalance;
    private final String unit;
    private final LinkedList<LeaveHistoryEntry> leaveHistory; // of type LeaveHistoryEntry
    private final String[][] leaveReasons;
}

