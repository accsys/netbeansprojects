/*
 * This class is a Singleton used to manage, verify and validate passwords
 */
package za.co.ucs.accsys.peopleware;

import java.sql.Connection;
import java.sql.SQLException;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * The PasswordManager singleton is used to a) Retrieve passwords b) Change
 * passwords c) Verify passwords
 *
 * @author Liam
 */
public class PasswordManager {

    public static PasswordManager getInstance() {
        if (passwordManager == null) {
            passwordManager = new PasswordManager();
        }
        return passwordManager;
    }

    private PasswordManager() {
    }

    /**
     * Returns the password for the given employee from the database
     *
     * @param employee
     * @return password
     */
    public String getPassword(Employee employee) {
        return employee.getWeb_pass();
    }

    public String getLoginName(Employee employee) {
        return employee.getWeb_login();
    }

    /**
     * Performs a series of password validations and returns one of an
     * enumeration of possible results - of type ValidationReason
     *
     * @param employee
     * @param password
     * @return - ENUM of ValidationReason
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public ValidationReason validatePassword(Employee employee, String password) {
        // Test for empty password
        if (password.trim().length() == 0) {
            return ValidationReason.irEmptyPwd;
        }

        // Expiration?
        int expireIn = getDaysBeforePwdExpire(employee);
        // Expired
        if (expireIn <= 0) {
            return ValidationReason.irExpired;
        }
        // Soon to expire
        if (expireIn < 7) {
            return ValidationReason.irExpireSoon;
        }

        // Was password already used?
        if (wasPasswordUsed(employee, password)) {
            return ValidationReason.irUsedBefore;
        }

        // All OK
        return ValidationReason.irNone;
    }

    /**
     * Returns the number of days before the Employee's password expires
     *
     * @return
     */
    public int getDaysBeforePwdExpire(Employee employee) {
        int daysToPasswordExpiry = 999;
        int nExpiryDays;

        String pwdExpiryDays = WebModulePreferences.getInstance().getPreference(WebModulePreferences.PASSWORDS_ExpireAfterNDays, "");
        // No expiration on passwords
        if (pwdExpiryDays.trim().length() > 0 && pwdExpiryDays.trim().compareTo("0") != 0) {
            // When should passwords expire?
            nExpiryDays = new Integer(pwdExpiryDays.trim()).intValue();
            // When was this password last changed?
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                String selectString = "select min(datediff(day, time_stamp, now())) from vwAudit with (nolock) "
                        + "where table_name='E_MASTER' and column_name='WEB_PWD' "
                        + " and victim_employee_id=" + employee.getEmployeeID()
                        + " and victim_company_id=" + employee.getCompany().getCompanyID();
                int nDaysSinceLastChange = DatabaseObject.getInt(selectString, con);
                // Expired
                daysToPasswordExpiry = nExpiryDays - nDaysSinceLastChange;


            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
        return daysToPasswordExpiry;
    }

    /**
     * Returns the validation reason for the given ValidationReason enumeration
     *
     * @param validationEnumeration - Enumeration of ValidationReason(s)
     * @return
     */
    public String getValidationReason(PasswordManager.ValidationReason validationEnumeration) {
        switch (validationEnumeration) {
            case irNone:
                return "Valid Password";
            case irEmptyPwd:
                return "Empty Password";
            case irExpired:
                return "Password Expired";
            case irExpireSoon:
                return "Password soon to expire";
            case irUsedBefore:
                return "Password was already used before";

        }
        return "Unknown";


    }

    /**
     * Returns TRUE if this password was already used before for the given
     * employee
     *
     * @param newPassword
     * @return
     */
    public boolean wasPasswordUsed(Employee employee, String newPassword) {
        boolean rslt = false;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            String selectString = "if exists(select * from vwAudit with (nolock) "
                    + "where table_name='E_MASTER' and column_name='WEB_PWD' "
                    + " and victim_employee_id=" + employee.getEmployeeID()
                    + " and victim_company_id=" + employee.getCompany().getCompanyID()
                    + " and fn_S_DecryptString(new_value) = '" + newPassword + "') "
                    + "  then select 1 else select 0; end if;";
            int usedCnt = DatabaseObject.getInt(selectString, con);
            rslt = usedCnt > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    public enum ValidationReason {

        irNone, irEmptyPwd, irExpired, irExpireSoon, irUsedBefore
    };
    private static PasswordManager passwordManager = null;
}
