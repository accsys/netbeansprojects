/*
 * PayslipInfo.java
 *
 * Created on January 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import java.util.*;
import za.co.ucs.accsys.tools.*;
import za.co.ucs.lwt.db.*;

/**
 * PayslipInfo gathers all the payslip-related information
 * @author  liam
 */
public class PayslipInfo implements AccsysReadOnlyObjectInterface, java.io.Serializable {
    
    /** Creates a new instance of PayslipInfo
     *  @param employee The employee for which Payslip information is required
     *  @param payrollDetail_id The period for which Payslip information is required
     *  @param runtype_id Yep, you guessed it.  The Runtype of the Payslip information
     */
    public PayslipInfo(Employee employee, int payrollDetail_id, int runtype_id) {
        this.earnings = new LinkedList<>();
        this.deductions = new LinkedList<>();
        this.ytd = new LinkedList<>();
        this.other = new LinkedList<>();
        
        this.employee = employee;
        this.payrollDetail_id = payrollDetail_id;
        this.runtype_id = runtype_id;
        if (isValid()) {
            this.loadFromDB();
        }
    }
    
    
    @Override
    public void loadFromDB() {
        if ((this.employee == null)|| (this.payrollDetail_id==-1) || (this.runtype_id==-1)) {
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                // EARNINGS
                String sqlStatement = "select QTY_VALUE, DESCRIPTION, VALUE from "+
                        "P_REPORT_EARNING  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and COST_DETAIL in ('YY', 'YN')  order by Description";
                ResultSet rs = DatabaseObject.openSQL(sqlStatement, con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getFloat("QTY_VALUE"),
                            rs.getString("DESCRIPTION"), rs.getFloat("VALUE"));
                    earnings.add(variableDetail);
                }
                rs.close();
                
                // DEDUCTIONS
                rs = DatabaseObject.openSQL("select DESCRIPTION, VALUE, BALANCE from "+
                        "P_REPORT_DEDUCTION  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and COST_DETAIL='YY' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"),rs.getFloat("BALANCE"));
                    deductions.add(variableDetail);
                }
                rs.close();
                
                // OTHERS - COMPANY CONTRIBUTIONS
                rs = DatabaseObject.openSQL("select p.DESCRIPTION as DESCRIPTION, p.VALUE as VALUE from "+
                        "P_REPORT_COMPANY p  with (NOLOCK) , p_variable v  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and p.variable_id=v.variable_id  "+
                        " and v.DISPLAYONPAYSLIP_YN='Y' order by p.Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    other.add(variableDetail);
                }
                rs.close();
                // OTHERS - INTERIMS
                rs = DatabaseObject.openSQL("select DESCRIPTION, VALUE from "+
                        "P_REPORT_INTERIM  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and DISPLAYONPAYSLIP_YN='Y' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    other.add(variableDetail);
                }
                rs.close();
                // OTHERS - NOT PAID EARNINGS
                rs = DatabaseObject.openSQL("select QTY_VALUE, DESCRIPTION, VALUE from "+
                        "P_REPORT_EARNING  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and COST_DETAIL='NN' and DISPLAYONPAYSLIP_YN='Y' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getFloat("QTY_VALUE"),
                            rs.getString("DESCRIPTION"), rs.getFloat("VALUE"));
                    other.add(variableDetail);
                }
                rs.close();
                // OTHERS - NOT PAID DEDUCTIONS
                rs = DatabaseObject.openSQL("select DESCRIPTION, VALUE, BALANCE from "+
                        "P_REPORT_DEDUCTION  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " and COST_DETAIL='NN' and DISPLAYONPAYSLIP_YN='Y' order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"),rs.getFloat("BALANCE"));
                    other.add(variableDetail);
                }
                rs.close();
                
                
                // YTD TOTALS
                rs = DatabaseObject.openSQL("select DESCRIPTION, VALUE from "+
                        "P_PAYLOG_YTD  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id+
                        " order by Description", con);
                while (rs.next()){
                    VariableDetail variableDetail = new VariableDetail(rs.getString("DESCRIPTION"),
                            rs.getFloat("VALUE"));
                    ytd.add(variableDetail);
                }
                rs.close();
                
                
                // Nett Pay
                rs = DatabaseObject.openSQL("select fn_P_GetNettPay("+ this.employee.getCompany().getCompanyID()+","+
                        this.employee.getEmployeeID()+","+ this.employee.getPayroll().getPayrollID()+","+
                        this.payrollDetail_id+","+ this.runtype_id+")", con);
                if (rs.next()){
                    try{
                        this.nettPay = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt( rs.getString(1));
                    } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
                }
                rs.close();
                
                // RunTYpe
                rs = DatabaseObject.openSQL("select RUNTYPE from p_runtype  with (NOLOCK) where runtype_id="+this.runtype_id, con);
                if (rs.next()){
                    this.runType = rs.getString(1);
                }
                rs.close();
                
                // Info in P_REPORT_DATA
                rs = DatabaseObject.openSQL("select * from p_report_data  with (NOLOCK) where Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and PayrollDetail_id="+this.payrollDetail_id+
                        " and Runtype_id="+runtype_id, con);
                if (rs.next()){
                    setRateOfPay1( rs.getString("RATE_OF_PAY1"));
                    setRateOfPay2( rs.getString("RATE_OF_PAY2"));
                    setRateOfPay3( rs.getString("RATE_OF_PAY3"));
                    setCostCentreName( rs.getString("COSTCENTRE_NAME"));
                    setJobPosition( rs.getString("JOB_POSITION"));
                    setPayMethod( rs.getString("PAY_METHOD"));
                    setAccountNumber( rs.getString("ACCOUNT_NUMBER"));
                    setStartDate( rs.getString("START_DATE"));
                    setEndDate( rs.getString("END_DATE"));
                }
                rs.close();
                
            } catch (SQLException e){
                e.printStackTrace();
            } finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    @Override
    public final boolean isValid() {
        return (  (this.employee.isValid()) &&
                (this.payrollDetail_id!=-1) && (this.runtype_id!=-1));
    }
    
    /** Returns a LinkedList containing a list of all the Earnings for this Payslip.
     * The Objects in which the values are stored are of type VariableDetail
     * @return 
     */
    public LinkedList<VariableDetail> getEarnings(){
        return this.earnings;
    }
    
    /** Returns a LinkedList containing a list of all the Deductions for this Payslip.
     * The Objects in which the values are stored are of type VariableDetail
     * @return 
     */
    public LinkedList<VariableDetail> getDeductions(){
        return this.deductions;
    }
    
    /** Returns a LinkedList containing a list of all the YTD Toptals for this payslip.
     * The Objects in which the values are stored are of type VariableDetail
     * @return 
     */
    public LinkedList<VariableDetail> getYTDs(){
        return this.ytd;
    }
    
    /** Returns a LinkedList containing a list of all the Company Contributions and Interims for this Payslip.
     * The Objects in which the values are stored are of type VariableDetail
     * @return 
     */
    public LinkedList<VariableDetail> getOthers(){
        return this.other;
    }
    
    /** Returns the NettPay
     * @return 
     */
    public String getNettPay(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.nettPay); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return "Unable to Decrypt";
    }
    
    
    
    /** Returns the payroll periods that the employee has been calculated for.
     * Note that this is bound to the specific runtype that the PayslipInfo object were
     * created with.
     * @return LinkedList containing PayrollDetail objects
     */
    public LinkedList getCalculatedPeriods(boolean onlyClosedPeriods){
        LinkedList result = new LinkedList();
        
        Connection con = null;
        ResultSet rs = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            if (onlyClosedPeriods){
                rs = DatabaseObject.openSQL("select distinct payrolldetail_id from p_calc_employeelist pc  with (NOLOCK) where "+
                        "Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and Runtype_id="+runtype_id+
                        " and run_status=3 and (closed_yn='Y' or exists "+
                        " (select * from cp_setup  with (NOLOCK) where company_id=pc.company_id and payroll_id=pc.payroll_id "+
                        "  and upper(property)=upper('ShowESSPayslip') and upper(value)=upper('Yes')))"+
                        " order by payrolldetail_id", con);} else {
                rs = DatabaseObject.openSQL("select distinct payrolldetail_id from p_calc_employeelist  with (NOLOCK) where "+
                        "Company_id="+this.employee.getCompany().getCompanyID()+
                        " and Employee_id="+this.employee.getEmployeeID()+
                        " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                        " and Runtype_id="+runtype_id+
                        " and run_status=3  "+
                        " order by payrolldetail_id", con);}
            
            while (rs.next()){
                PayrollDetail payrollDetail = new PayrollDetail(employee.getPayroll(), new Integer(rs.getInt(1)));
                result.add(payrollDetail);
            }
            rs.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    
    /** String description of the RunType
     * @return 
     */
    public String getRunType(){
        return this.runType;
    }
    
    
    /** Returns true if the employee has already been calculated for the given period
     * Note that this is bound to the specific runtype that the PayslipInfo object were
     * created with.
     * @return 
     */
    public boolean isCalculatedForEmployee(){
        boolean result = false;
        
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            // EARNINGS
//            
//            ResultSet rs = DatabaseObject.openSQL("select run_status from p_calc_employeelist  with (NOLOCK) where "+
//                    "Company_id="+this.employee.getCompany().getCompanyID()+
//                    " and Employee_id="+this.employee.getEmployeeID()+
//                    " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
//                    " and Payrolldetail_id="+this.payrollDetail_id+
//                    " and Runtype_id="+runtype_id, con);

            String sqlStatement = "select run_status from p_calc_employeelist pc  with (NOLOCK) where "+
                    "Company_id="+this.employee.getCompany().getCompanyID()+
                    " and Employee_id="+this.employee.getEmployeeID()+
                    " and Payroll_id="+this.employee.getPayroll().getPayrollID()+
                    " and Payrolldetail_id="+this.payrollDetail_id+
                    " and Runtype_id="+runtype_id+
                    " and (closed_yn='Y' or exists "+
                        " (select * from cp_setup  with (NOLOCK) where company_id=pc.company_id and payroll_id=pc.payroll_id "+
                        "  and upper(property)=upper('ShowESSPayslip') and upper(value)=upper('Yes')))";
            ResultSet rs = DatabaseObject.openSQL(sqlStatement, con);
            
            
            if (rs.next()){
                result =  (rs.getInt(1)==3);
            }
            rs.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        } finally{
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    
    /** Getter for property employee.
     * @return Value of property employee.
     *
     */
    public za.co.ucs.accsys.peopleware.Employee getEmployee() {
        return employee;
    }
    
    /** Getter for property payrollDetail_id.
     * @return Value of property payrollDetail_id.
     *
     */
    public int getPayrollDetail_id() {
        return payrollDetail_id;
    }
    
    /** Getter for property runtype_id.
     * @return Value of property runtype_id.
     *
     */
    public int getRuntype_id() {
        return runtype_id;
    }
    
    public String getRateOfPay1() {
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rateOfPay1); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setRateOfPay1(String rateOfPay1) {
        try{
            this.rateOfPay1 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rateOfPay1); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getRateOfPay2() {
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rateOfPay2); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setRateOfPay2(String rateOfPay2) {
        try{
            this.rateOfPay2 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rateOfPay2); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getRateOfPay3() {
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.rateOfPay3); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setRateOfPay3(String rateOfPay3) {
        try{
            this.rateOfPay3 = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(rateOfPay3); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public void setCostCentreName(String costCentreName){
        try{
            this.costCentreName = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(costCentreName); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getCostCentreName(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.costCentreName); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setJobPosition(String jobPosition){
        try{
            this.jobPosition = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(jobPosition); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getJobPosition(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.jobPosition); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setPayMethod(String payMethod){
        try{
            this.payMethod = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(payMethod); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getPayMethod(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.payMethod); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setAccountNumber(String accountNumber){
        try{
            this.accountNumber = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(accountNumber); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getAccountNumber(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.accountNumber); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setStartDate(String startDate){
        try{
            this.startDate = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(startDate); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getStartDate(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.startDate); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    public void setEndDate(String endDate){
        try{
            this.endDate = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(endDate); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    public String getEndDate(){
        try{
            return za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.endDate); } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
        return StringEncrypterSingleton.DECRYPTION_ERROR_MESSAGE;
    }
    
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
        C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.peopleware.PayslipInfo
        za.co.ucs.accsys.peopleware.PayslipInfo:    static final long serialVersionUID = -6645819543893844770L;
     
     */
    static final long serialVersionUID =7029711380855603961L;
    
    private final Employee employee;
    private final int payrollDetail_id;
    private final int runtype_id;
    private final LinkedList<VariableDetail> earnings;
    private final LinkedList<VariableDetail> deductions;
    private final LinkedList<VariableDetail> ytd;
    private final LinkedList<VariableDetail> other;
    protected String nettPay = "0";
    protected String runType;
    protected String rateOfPay1;
    protected String rateOfPay2;
    protected String rateOfPay3;
    protected String costCentreName;
    protected String jobPosition;
    protected String payMethod;
    protected String accountNumber;
    protected String startDate;
    protected String endDate;
    
    
}



