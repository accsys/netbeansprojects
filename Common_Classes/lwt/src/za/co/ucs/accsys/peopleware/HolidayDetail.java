/*
 * HolidayDetail.java
 *
 * Created on 20 April 2005, 08:03
 */

package za.co.ucs.accsys.peopleware;
/**
 *
 * @author  lwt
 */
/** Private class that contains the balance information for a given employee
 */
public class HolidayDetail implements java.io.Serializable{
    

    
    /** Creates an object that contains the detail of each holiday linked to this employee.
     * @param employee
     * @param holidayName
     * @param holidayDate
     */
    public HolidayDetail(Employee employee, String holidayName, java.util.Date holidayDate){
        this.employee = employee;
        this.holidayName = holidayName;
        this.holidayDate = holidayDate;
    }
    

    public Employee getEmployee(){
        return this.employee;
    }

    public String getHolidayName(){
        return this.holidayName;
    }
    
    public java.util.Date getDate(){
        return this.holidayDate;
    }
    
    @Override
    public String toString(){
        return ("Employee:"+employee+"\n\t"+holidayName+"\n\tOn: "+holidayDate);
    }
    
    private final Employee employee;
    private final String holidayName;
    private final java.util.Date holidayDate;
}
