/*
 * TRProviderList.java
 *
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import java.util.LinkedList;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.*;

/**
 * TREventList lists events(courses) that a person can apply to attend
 * (FUTURE-Dated)
 *
 * @Author liam
 */
public final class TREventList {

    /**
     * Creates a LinkedList of all training providers
     */
    public TREventList() {
        futureEventList = new LinkedList();
        loadFromDB();
    }

    /**
     * Constructor for a list of training events scheduled in the future.
     *
     * @param forEmployee - if not null, it will exclude the events that are
     * already linked to this person
     */
    public TREventList(Employee forEmployee) {
        futureEventList = new LinkedList();
        this.forEmployee = forEmployee;
        loadFromDB();
    }

    public LinkedList getList() {
        return futureEventList;
    }

    @Override
    public String toString() {

        return ("List Size:" + futureEventList.size());
    }

    public void loadFromDB() {
        Connection con = null;
        try {
            StringBuilder queryString = new StringBuilder("SELECT TR_EVENT.EVENT_ID ");
            con = DatabaseObject.getNewConnectionFromPool();
            if (this.forEmployee == null) {
                queryString.append("FROM ( DBA.TR_EVENT with (nolock) JOIN DBA.TR_COURSE with (nolock) ON DBA.TR_EVENT.COURSE_ID = DBA.TR_COURSE.COURSE_ID ) ");
                queryString.append("        JOIN DBA.TR_PROVIDER wiht (nolock) ON DBA.TR_COURSE.PROVIDER_ID = DBA.TR_PROVIDER.PROVIDER_ID ");
                queryString.append("WHERE datediff(month, now(*), FROM_DATE)>-5  ORDER BY TR_COURSE.NAME ASC , FROM_DATE ASC ");
            } else {
                if (WebModulePreferences.getPreferences().get(WebModulePreferences.FILTER_FilterOnlyPlannedTrainingEvents, "False").compareToIgnoreCase("True") == 0) {
                    queryString.append("FROM ( DBA.TR_EVENT with (nolock) JOIN DBA.TR_COURSE with (nolock) ON DBA.TR_EVENT.COURSE_ID = DBA.TR_COURSE.COURSE_ID ) ");
                    queryString.append("        JOIN DBA.TR_PROVIDER with (nolock) ON DBA.TR_COURSE.PROVIDER_ID = DBA.TR_PROVIDER.PROVIDER_ID ");
                    queryString.append(" WHERE DBA.TR_COURSE.COURSE_ID in (select COURSE_ID from TR_PLANNING with (nolock) where company_id=").append(forEmployee.getCompany().getCompanyID());
                    queryString.append(" and employee_id =").append(forEmployee.getEmployeeID());
                    queryString.append(") and TR_EVENT.TO_DATE > dateadd(day,-30,today()) ORDER BY TR_COURSE.NAME ASC , FROM_DATE ASC ");
                } else {
                    queryString.append("FROM ( DBA.TR_EVENT with (nolock) JOIN DBA.TR_COURSE with (nolock) ON DBA.TR_EVENT.COURSE_ID = DBA.TR_COURSE.COURSE_ID ) ");
                    queryString.append("        JOIN DBA.TR_PROVIDER with (nolock) ON DBA.TR_COURSE.PROVIDER_ID = DBA.TR_PROVIDER.PROVIDER_ID ");
                    queryString.append(" WHERE datediff(month, now(*), FROM_DATE)>-5  ");
                    queryString.append(" and event_id not in (select event_id from TRL_EMPLOYEE_EVENT tr with (nolock) where company_id=").append(forEmployee.getCompany().getCompanyID());
                    queryString.append("                      and employee_id = ").append(forEmployee.getEmployeeID());
                    queryString.append("                     ) ORDER BY TR_COURSE.NAME ASC , FROM_DATE ASC ");
                }
            }
            //System.out.println(queryString.toString());
            ResultSet rs = DatabaseObject.openSQL(queryString.toString(), con);
            while (rs.next()) {
                TREvent event = new TREvent(rs.getInt("EVENT_ID"));
                futureEventList.add(event);
            }
        } catch (SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }
    private java.util.LinkedList futureEventList;
    private Employee forEmployee = null;
}
