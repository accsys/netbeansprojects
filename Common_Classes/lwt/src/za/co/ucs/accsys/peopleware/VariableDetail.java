/*
 * VariableDetail.java
 *
 * Created on July 14, 2004, 12:48 PM
 */

package za.co.ucs.accsys.peopleware;
import za.co.ucs.accsys.tools.*;

/** We need to create a list of variables, with their QTY's and values
 */
public class VariableDetail implements java.io.Serializable{
    
    static final long serialVersionUID = -200479769557696740L;
    
    /** Constructor for Earnings - no balances
     * @param qty
     * @param name
     * @param value
     */
    public VariableDetail(float qty, String name, float value){
        this(name, value);
        this.qty = qty;
    }
    
    /** Constructor for Deductions
     * @param name
     * @param value
     * @param balance
     */
    public VariableDetail(String name, float value, float balance){
        this(name, value);
        this.balance = balance;
    }
    
    /** Constructor for Everything else
     * @param name
     * @param value
     */
    public VariableDetail(String name, float value){
        
        this.qty = 0;
        this.value = value;
        this.balance = 0;
        try{
            this.name = za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).encrypt(name);
        } catch( StringEncrypterSingleton.EncryptionException e){ e.printStackTrace(); }
    }
    
    @Override
    public String toString(){
        return ("Name:"+getName()+"\tValue"+value);
    }
    
    public java.lang.String getName() {
        try {
            return (za.co.ucs.accsys.tools.StringEncrypterSingleton.getInstance(StringEncrypterSingleton.DESEDE_ENCRYPTION_SCHEME).decrypt(this.name));
        } catch( StringEncrypterSingleton.EncryptionException e){
            e.printStackTrace();
        }
        return "[Unable to Decrypt]";
    }
    
    public float getQty() {
        return qty;
    }
    
    public float getValue() {
        return value;
    }
    
    public float getBalance() {
        return balance;
    }
    
    private float qty=0;
    private String name="";
    private float value=0;
    private float balance=0;
}
