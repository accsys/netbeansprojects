/*
 * Loan.java
 *
 * Created on July 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import java.util.*;
import za.co.ucs.lwt.db.*;

/**
 * LoanRepayments: Detail of every repayment done on a given loan
 * @author  liam
 */
public final class LoanRepayments  {
    
    /** Creates a new instance of Contact
     */
    public LoanRepayments(Employee employee, Loan loan) {
        this.employee = employee;
        this.loan = loan;
        this.repayments = new LinkedList();
        if (isValid()) {
            loadFromDB();
        }
    }
    
    public void loadFromDB() {
        if (this.employee == null) {
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // vw_P_Loans
                //
                String sqlString = "select pd.rundate, vw.ammount_deducted, adjusted_balance, "+
                " balance, interest from vw_P_Loans vw with (nolock), p_payrolldetail pd with (nolock) "+
                " where vw.payroll_id=pd.payroll_id and "+
                " vw.payrolldetail_id=pd.payrolldetail_id and "+
                " company_id="+employee.getCompany().getCompanyID()+
                " and Employee_id="+employee.getEmployeeID()+
                " and emloan_id="+this.loan.getEmloanID() +
                " order by rundate, vw.runtype_id";
                
                ResultSet rs = DatabaseObject.openSQL(sqlString, con);
                while (rs.next()){
                    LoanRepayment repayment = new LoanRepayment(rs.getFloat("ammount_deducted"),
                    rs.getDate("rundate"), rs.getFloat("balance"),
                    rs.getFloat("interest"), (rs.getFloat("adjusted_balance")==0));
                    // Add object to the repayments Linked List
                    repayments.add(repayment);
                }
                rs.close();
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    public int getNumberOfRepayments(){
        return repayments.size();
    }
    
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("Number of repayments:").append(getNumberOfRepayments());
        for (int i=0;i<getNumberOfRepayments();i++){
            result.append("\n[").append(i).append("] :").append(getBalance(i)).append(" in ").append(getDeductionDate(i));
        }
        return result.toString();
    }
    
    /** Returns the index'th deduction in the repayments history
     */
    public float getDeduction(int index){
        return ( ((LoanRepayment)repayments.get(index)).deduction );
    }
    /** Returns the index'th deduction date in the repayments history
     */
    public java.util.Date getDeductionDate(int index){
        return ( ((LoanRepayment)repayments.get(index)).deductionDate );
    }
    /** Returns the index'th balance in the repayments history
     */
    public float getBalance(int index){
        return ( ((LoanRepayment)repayments.get(index)).balance );
    }
    /** Returns the index'th interest in the repayments history
     */
    public float getInterest(int index){
        return ( ((LoanRepayment)repayments.get(index)).interest );
    }
    /** Returns the index'th deduction in the repayments history
     */
    public boolean isAdjusted(int index){
        return ( ((LoanRepayment)repayments.get(index)).adjusted );
    }
    
    
    public boolean isValid() {
        return ((this.employee.isValid()) && (this.loan.isValid()) );
    }
    
    private Employee employee;
    private Loan loan;
    private LinkedList repayments;
}

class LoanRepayment{
    
    public LoanRepayment(float deduction, java.util.Date deductionDate, float balance, float interest, boolean adjusted){
        this.deduction = deduction;
        this.deductionDate = deductionDate;
        this.balance = balance;
        this.interest = interest;
        this.adjusted = adjusted;
    }
    
    public float deduction;
    public java.util.Date deductionDate;
    public float balance;
    public float interest;
    public boolean adjusted;
}

