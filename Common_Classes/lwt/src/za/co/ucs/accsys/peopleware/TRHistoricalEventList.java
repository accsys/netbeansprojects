/*
 * TRProviderList.java
 *
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;
import java.util.LinkedList;

/**
 * TRHistoricalEventList lists events(courses) that a person attended
 * @Author  liam
 */
public final class TRHistoricalEventList {

    /** 
     * Creates a LinkedList of all training providers
     */
    public TRHistoricalEventList(Employee employee) {
        this.employee = employee;
        historicalEventList = new LinkedList();
        loadFromDB();
    }

    public LinkedList getList() {
        return historicalEventList;
    }

    public void loadFromDB() {
        if (employee != null) {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("SELECT EVENT_ID FROM TR_EVENT where "+
                                              " EVENT_ID in ("+
                                              "    select EVENT_ID from TRL_EMPLOYEE_EVENT  "+
                                              "    where COMPANY_ID="+employee.getCompany().getCompanyID()+
                                              "    and EMPLOYEE_ID="+employee.getEmployeeID()+
                                              "    )"+
                                              " order by FROM_DATE desc , TO_DATE desc", con);
                while (rs.next()) {
                    int eventID = (new Integer(rs.getInt("EVENT_ID")));
                    TRHistoricalEvent event = new TRHistoricalEvent(employee, eventID);
                    historicalEventList.add(event);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    private java.util.LinkedList historicalEventList;
    private Employee employee;
}
