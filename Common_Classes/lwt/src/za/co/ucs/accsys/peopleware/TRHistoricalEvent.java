/*
 * TRHistoricalEvent.java
 */

package za.co.ucs.accsys.peopleware;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * TRHistoricalEvent maps to dba.SD_PROVIDER
 * @Author  liam
 */
public final class TRHistoricalEvent implements AccsysObjectInterface, java.io.Serializable {
    
    /** Creates an instance of an existing provider in SD_PROVIDER
     * @param providerID As the name suggests. 
     */
    public TRHistoricalEvent(Employee employee, Integer eventID) {
        this.setEventID(eventID);
        this.setCompanyID(employee.getCompany().getCompanyID());
        this.setEmployeeID(employee.getEmployeeID());
        if (isValid())
            loadFromDB();
    }

    @Override
    public boolean isValid() {
        return ((this.getEventID() != null));
    }
    
    @Override
    public void applyToDB() {
        // Not implemented for SD_PROVIDER.  To be managed through the Skills Development Module only.
    }
    
    @Override
    public void loadFromDB() {
        if (this.getEventID() == null)
            return;
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                String queryString = "SELECT c.COURSE_ID, c.PROVIDER_ID,c.NAME AS CourseName,c.ACCREDITED AS isAccredited,"+
                                     "te.FROM_DATE AS fromDate,te.TO_DATE AS toDate,te.VENUE,te.FACILITATOR,"+
                                     "te.ENTITY_TYPE AS entityType,trl.PASS_MARK,r.RESULT_DESC as resultDescription,"+
                                     "trl.COMPANY_ID,trl.EMPLOYEE_ID FROM "+
                                     "( ( DBA.TRL_EMPLOYEE_EVENT AS trl with (nolock) LEFT OUTER JOIN DBA.TR_TRAINING_RESULT AS r with (nolock) "+
                                     "    ON trl.RESULT_ID = r.RESULT_ID ) "+
                                     " JOIN DBA.TR_EVENT AS te with (nolock) ON trl.EVENT_ID = te.EVENT_ID ) "+
                                     " JOIN DBA.TR_COURSE AS c with (nolock) ON te.COURSE_ID = c.COURSE_ID "+
                                     " where trl.COMPANY_ID="+this.getCompanyID()+
                                     "   and trl.EMPLOYEE_ID="+this.getEmployeeID()+
                                     "   and trl.EVENT_ID="+this.getEventID()+
                                     " ORDER BY te.FROM_DATE ASC ,CourseName ASC ";
                ResultSet rs = DatabaseObject.openSQL(queryString, con);
                if (rs.next()){
                    this.setCourseName(rs.getString("CourseName"));
                    this.setCourseID(new Integer(rs.getInt("COURSE_ID")));
                    this.setProviderID(new Integer(rs.getInt("PROVIDER_ID")));
                    this.setEntityType(rs.getString("entityType"));
                    this.setFacilitator(rs.getString("FACILITATOR"));
                    this.setIsAccredited(rs.getBoolean("isAccredited"));
                    this.setFromDate(rs.getString("fromDate"));
                    this.setToDate(rs.getString("toDate"));
                    this.setPassMark(rs.getString("PASS_MARK"));
                    this.setResultDescription(rs.getString("resultDescription"));
                    this.setVenue(rs.getString("VENUE"));
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public String toString(){
        return ("Event:"+getCourseName()+" on "+getFromDate()+" at "+getVenue());
    }
    /**
     * @return the providerID
     */
    public Integer getProviderID() {
        return providerID;
    }

    /**
     * @param providerID the providerID to set
     */
    public void setProviderID(Integer providerID) {
        this.providerID = providerID;
    }

    /**
     * @return the courseID
     */
    public Integer getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    /**
     * @return the companyID
     */
    public Integer getCompanyID() {
        return companyID;
    }

    /**
     * @param companyID the companyID to set
     */
    public void setCompanyID(Integer companyID) {
        this.companyID = companyID;
    }

    /**
     * @return the employeeID
     */
    public Integer getEmployeeID() {
        return employeeID;
    }

    /**
     * @param employeeID the employeeID to set
     */
    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the fromDate
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the venue
     */
    public String getVenue() {
        return venue;
    }

    /**
     * @param venue the venue to set
     */
    public void setVenue(String venue) {
//        if (venue == null) {
//            venue = "";
//        }
        this.venue = venue;
    }

    /**
     * @return the facilitator
     */
    public String getFacilitator() {
        return facilitator;
    }

    /**
     * @param facilitator the facilitator to set
     */
    public void setFacilitator(String facilitator) {
        this.facilitator = facilitator;
    }

    /**
     * @return the entityType
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * @param entityType the entityType to set
     */
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    /**
     * @return the passMark
     */
    public String getPassMark() {
        return passMark;
    }

    /**
     * @param passMark the passMark to set
     */
    public void setPassMark(String passMark) {
        this.passMark = passMark;
    }

    /**
     * @return the resultDescription
     */
    public String getResultDescription() {
        return resultDescription;
    }

    /**
     * @param resultDescription the resultDescription to set
     */
    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    /**
     * @return the isAccredited
     */
    public boolean isIsAccredited() {
        return isAccredited;
    }

    /**
     * @param isAccredited the isAccredited to set
     */
    public void setIsAccredited(boolean isAccredited) {
        this.isAccredited = isAccredited;
    }
    


    private Integer providerID; //
    private Integer courseID; //
    private Integer companyID; //
    private Integer employeeID; //
    private Integer eventID;
    private String courseName;
    private String fromDate;
    private String toDate;
    private String venue;
    private String facilitator;
    private String entityType;
    private String passMark;
    private String resultDescription;
    private boolean isAccredited; //

    /**
     * @return the eventID
     */
    public Integer getEventID() {
        return eventID;
    }

    /**
     * @param eventID the eventID to set
     */
    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

  
}
