/*
 * accsysReadOnlyObjectInterface.java
 *
 * Created on March 13,  2004, 12:35 PM
 */

package za.co.ucs.accsys.peopleware;

/**
 * The AccsysReadOnlyObjectInterface is used to ensure consistency amongs all the Accsys classes
 * This interface is used where entities are supposed to be read-only
 * @author  liam
 */
public interface AccsysReadOnlyObjectInterface {
     /** reads / loads data from database
      */
     void loadFromDB();
     
     /** Do we have enough information to read/update the database table?
     * @return 
      */
     boolean isValid();
}
