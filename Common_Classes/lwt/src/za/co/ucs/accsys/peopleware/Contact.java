/*
 * Family.java
 *
 * Created on July1 8, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * Contact will be used to store addresses and phone numbers for employees and their
 * family members. (might use it for other objects as well)
 * @author  liam
 */
public final class Contact implements AccsysObjectInterface, java.io.Serializable {

    /** Creates a new instance of Contact
     */
    public Contact(Object owner) {
        this.owner = owner;
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void loadFromDB() {
        //  If owner is Employee
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Employee") == 0) {
            loadFromDB(((Employee) this.owner));
        }
        //  If owner is Family
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Family") == 0) {
            loadFromDB(((Family) this.owner));
        }
    }

    private void loadFromDB(Employee employee) {
        if (employee == null) {
            return;
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // E_CONTACT
                //
                ResultSet rs = DatabaseObject.openSQL("select * from e_contact with (nolock) where " +
                        " Company_id=" + employee.getCompany().getCompanyID() +
                        " and Employee_id=" + employee.getEmployeeID(), con);
                if (rs.next()) {
                    telHome = rs.getString("TEL_HOME");
                    telWork = rs.getString("TEL_WORK");
                    faxHome = rs.getString("FAX_HOME");
                    faxWork = rs.getString("FAX_WORK");
                    cellNo = rs.getString("TEL_CELL");
                    eMail = rs.getString("E_MAIL");
                }
                rs.close();

                //
                // E_ADDRESS
                //
                rs = DatabaseObject.openSQL("select * from e_address with (nolock) where " +
                        " Company_id=" + employee.getCompany().getCompanyID() +
                        " and Employee_id=" + employee.getEmployeeID(), con);
                if (rs.next()) {
                    homePostal1 = rs.getString("HOME_POSTAL1");
                    homePostal2 = rs.getString("HOME_POSTAL2");
                    homePostal3 = rs.getString("HOME_POSTAL3");
                    homePostal4 = rs.getString("HOME_POSTAL4");
                    homeStreet1 = rs.getString("HOME_STREET1");
                    homeStreet2 = rs.getString("HOME_STREET2");
                    homeStreet3 = rs.getString("HOME_STREET3");
                    homeStreet4 = rs.getString("HOME_STREET4");
                    homePostalCode = rs.getString("HOME_POSTAL_CODE");
                    homeStreetCode = rs.getString("HOME_STREET_CODE");
                    setPhys_HOME_UNITNO(rs.getString("PHYS_HOME_UNITNO"));
                    setPhys_HOME_COMPLEX(rs.getString("PHYS_HOME_COMPLEX"));
                    setPhys_HOME_STREETNO(rs.getString("PHYS_HOME_STREETNO"));
                    setPhys_HOME_STREET(rs.getString("PHYS_HOME_STREET"));
                    setPhys_HOME_SUBURB(rs.getString("PHYS_HOME_SUBURB"));
                    setPhys_HOME_CITY(rs.getString("PHYS_HOME_CITY"));
                    setPhys_HOME_CODE(rs.getString("PHYS_HOME_CODE"));
                    setPhys_HOME_PROVINCE(rs.getString("PHYS_HOME_PROVINCE"));
                    setPhys_HOME_COUNTRY(rs.getString("PHYS_HOME_COUNTRY"));
                }
                rs.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    private void loadFromDB(Family family) {
        if (family == null) {
            return;
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                //
                // E_CONTACT
                //
                ResultSet rs = DatabaseObject.openSQL("select * from e_family with (nolock) where " +
                        " Company_id=" + family.getEmployee().getCompany().getCompanyID() +
                        " and Employee_id=" + family.getEmployee().getEmployeeID() +
                        " and Family_id=" + family.getFamilyID(), con);
                if (rs.next()) {
                    telHome = rs.getString("TEL_HOME");
                    telWork = rs.getString("TEL_WORK");
                    faxHome = rs.getString("FAX_HOME");
                    faxWork = rs.getString("FAX_WORK");
                    cellNo = rs.getString("TEL_CELL");
                    eMail = rs.getString("E_MAIL");
                    homeStreet1 = rs.getString("HOME_STREET1");
                    homeStreet2 = rs.getString("HOME_STREET2");
                    homeStreet3 = rs.getString("HOME_STREET3");
                    homeStreet4 = rs.getString("HOME_STREET4");
                    homeStreetCode = rs.getString("HOME_STREET_CODE");
                }
                rs.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        //  If owner is Employee
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Employee") == 0) {
            return (((Employee) this.owner).isValid());
        }
        //  If owner is Family
        if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Family") == 0) {
            return (((Family) this.owner).isValid());
        }

        return (false);
    }

    public java.lang.String getCellNo() {
        if (cellNo == null) {
            return ("");
        } else {
            return cellNo;
        }
    }

    public java.lang.String getEMail() {
        if (eMail == null) {
            return ("");
        } else {
            return eMail;
        }
    }

    public java.lang.String getFaxHome() {
        if (faxHome == null) {
            return ("");
        } else {
            return faxHome;
        }
    }

    public java.lang.String getFaxWork() {
        if (faxWork == null) {
            return ("");
        } else {
            return faxWork;
        }
    }

    public java.lang.String getHomePostal1() {
        if (homePostal1 == null) {
            return ("");
        } else {
            return homePostal1;
        }
    }

    public java.lang.String getHomePostal2() {
        if (homePostal2 == null) {
            return ("");
        } else {
            return homePostal2;
        }
    }

    public java.lang.String getHomePostal3() {
        if (homePostal3 == null) {
            return ("");
        } else {
            return homePostal3;
        }
    }

    public java.lang.String getHomePostal4() {
        if (homePostal4 == null) {
            return ("");
        } else {
            return homePostal4;
        }
    }

    public java.lang.String getHomePostalCode() {
        if (homePostalCode == null) {
            return ("");
        } else {
            return homePostalCode;
        }
    }

    public java.lang.String getHomeStreet1() {
        if (homeStreet1 == null) {
            return ("");
        } else {
            return homeStreet1;
        }
    }

    public java.lang.String getHomeStreet2() {
        if (homeStreet2 == null) {
            return ("");
        } else {
            return homeStreet2;
        }
    }

    public java.lang.String getHomeStreet3() {
        if (homeStreet3 == null) {
            return ("");
        } else {
            return homeStreet3;
        }
    }

    public java.lang.String getHomeStreet4() {
        if (homeStreet4 == null) {
            return ("");
        } else {
            return homeStreet4;
        }
    }

    public java.lang.String getHomeStreetCode() {
        if (homeStreetCode == null) {
            return ("");
        } else {
            return homeStreetCode;
        }
    }

    public java.lang.String getTelHome() {
        if (telHome == null) {
            return ("");
        } else {
            return telHome;
        }
    }

    public java.lang.String getTelWork() {
        if (telWork == null) {
            return ("");
        } else {
            return telWork;
        }
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save contact information to db.");
            return;
        }

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder updateSQL = new StringBuilder();

            //  If owner is Employee
            if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Employee") == 0) {
                // E_CONTACT
                updateSQL.setLength(0);
                updateSQL.append("update e_contact set ");
                updateSQL.append("TEL_HOME='").append(getTelHome()).append("', ");
                updateSQL.append("TEL_WORK='").append(getTelWork()).append("', ");
                updateSQL.append("FAX_HOME='").append(getFaxHome()).append("', ");
                updateSQL.append("FAX_WORK='").append(getFaxWork()).append("', ");
                updateSQL.append("TEL_CELL='").append(getCellNo()).append("', ");
                updateSQL.append("E_MAIL='").append(getEMail()).append("' ");
                updateSQL.append("where company_id=").append(((Employee) owner).getCompany().getCompanyID()).append(" and employee_id=").append(((Employee) owner).getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
                // E_ADDRESS
                updateSQL.setLength(0);
                updateSQL.append("update e_address set ");
                updateSQL.append("HOME_STREET1='").append(getHomeStreet1()).append("', ");
                updateSQL.append("HOME_STREET2='").append(getHomeStreet2()).append("', ");
                updateSQL.append("HOME_STREET3='").append(getHomeStreet3()).append("', ");
                updateSQL.append("HOME_STREET4='").append(getHomeStreet4()).append("', ");
                updateSQL.append("HOME_STREET_CODE='").append(getHomeStreetCode()).append("', ");
                updateSQL.append("HOME_POSTAL1='").append(getHomePostal1()).append("', ");
                updateSQL.append("HOME_POSTAL2='").append(getHomePostal2()).append("', ");
                updateSQL.append("HOME_POSTAL3='").append(getHomePostal3()).append("', ");
                updateSQL.append("HOME_POSTAL4='").append(getHomePostal4()).append("', ");
                updateSQL.append("HOME_POSTAL_CODE='").append(getHomePostalCode()).append("', ");
                updateSQL.append("PHYS_HOME_UNITNO='").append(getPhys_HOME_UNITNO()).append("', ");
                updateSQL.append("PHYS_HOME_COMPLEX='").append(getPhys_HOME_COMPLEX()).append("', ");
                updateSQL.append("PHYS_HOME_STREETNO='").append(getPhys_HOME_STREETNO()).append("', ");
                updateSQL.append("PHYS_HOME_STREET='").append(getPhys_HOME_STREET()).append("', ");
                updateSQL.append("PHYS_HOME_SUBURB='").append(getPhys_HOME_SUBURB()).append("', ");
                updateSQL.append("PHYS_HOME_CITY='").append(getPhys_HOME_CITY()).append("', ");
                updateSQL.append("PHYS_HOME_CODE='").append(getPhys_HOME_CODE()).append("', ");
                updateSQL.append("PHYS_HOME_PROVINCE='").append(getPhys_HOME_PROVINCE()).append("', ");
                updateSQL.append("PHYS_HOME_COUNTRY='").append(getPhys_HOME_COUNTRY()).append("' ");
                updateSQL.append("where company_id=").append(((Employee) owner).getCompany().getCompanyID()).append(" and employee_id=").append(((Employee) owner).getEmployeeID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            }
            //  If owner is Family
            if (owner.getClass().getName().compareTo("za.co.ucs.accsys.peopleware.Family") == 0) {
                updateSQL.setLength(0);
                updateSQL.append("update e_family set ");
                updateSQL.append("HOME_STREET1='").append(getHomeStreet1()).append("', ");
                updateSQL.append("HOME_STREET2='").append(getHomeStreet2()).append("', ");
                updateSQL.append("HOME_STREET3='").append(getHomeStreet3()).append("', ");
                updateSQL.append("HOME_STREET4='").append(getHomeStreet4()).append("', ");
                updateSQL.append("HOME_STREET_CODE='").append(getHomeStreetCode()).append("', ");
                updateSQL.append("TEL_HOME='").append(getTelHome()).append("', ");
                updateSQL.append("TEL_WORK='").append(getTelWork()).append("', ");
                updateSQL.append("FAX_HOME='").append(getFaxHome()).append("', ");
                updateSQL.append("FAX_WORK='").append(getFaxWork()).append("', ");
                updateSQL.append("TEL_CELL='").append(getCellNo()).append("', ");
                updateSQL.append("E_MAIL='").append(getEMail()).append("' ");
                updateSQL.append("where company_id=").append(((Family) owner).getEmployee().getCompany().getCompanyID());
                updateSQL.append(" and employee_id=").append(((Family) owner).getEmployee().getEmployeeID());
                updateSQL.append(" and family_id=").append(((Family) owner).getFamilyID());
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    /** Setter for property cellNo.
     * @param cellNo New value of property cellNo.
     *
     */
    public void setCellNo(java.lang.String cellNo) {
        this.cellNo = cellNo;
    }

    /** Setter for property eMail.
     * @param eMail New value of property eMail.
     *
     */
    public void setEMail(java.lang.String eMail) {
        this.eMail = eMail;
    }

    /** Setter for property faxHome.
     * @param faxHome New value of property faxHome.
     *
     */
    public void setFaxHome(java.lang.String faxHome) {
        this.faxHome = faxHome;
    }

    /** Setter for property faxWork.
     * @param faxWork New value of property faxWork.
     *
     */
    public void setFaxWork(java.lang.String faxWork) {
        this.faxWork = faxWork;
    }

    /** Setter for property homePostal1.
     * @param homePostal1 New value of property homePostal1.
     *
     */
    public void setHomePostal1(java.lang.String homePostal1) {
        this.homePostal1 = homePostal1;
    }

    /** Setter for property homePostal2.
     * @param homePostal2 New value of property homePostal2.
     *
     */
    public void setHomePostal2(java.lang.String homePostal2) {
        this.homePostal2 = homePostal2;
    }

    /** Setter for property homePostal3.
     * @param homePostal3 New value of property homePostal3.
     *
     */
    public void setHomePostal3(java.lang.String homePostal3) {
        this.homePostal3 = homePostal3;
    }

    /** Setter for property homePostal4.
     * @param homePostal4 New value of property homePostal4.
     *
     */
    public void setHomePostal4(java.lang.String homePostal4) {
        this.homePostal4 = homePostal4;
    }

    /** Setter for property homePostalCode.
     * @param homePostalCode New value of property homePostalCode.
     *
     */
    public void setHomePostalCode(java.lang.String homePostalCode) {
        this.homePostalCode = homePostalCode;
    }

    /** Setter for property homeStreet1.
     * @param homeStreet1 New value of property homeStreet1.
     *
     */
    public void setHomeStreet1(java.lang.String homeStreet1) {
        this.homeStreet1 = homeStreet1;
    }

    /** Setter for property homeStreet2.
     * @param homeStreet2 New value of property homeStreet2.
     *
     */
    public void setHomeStreet2(java.lang.String homeStreet2) {
        this.homeStreet2 = homeStreet2;
    }

    /** Setter for property homeStreet3.
     * @param homeStreet3 New value of property homeStreet3.
     *
     */
    public void setHomeStreet3(java.lang.String homeStreet3) {
        this.homeStreet3 = homeStreet3;
    }

    /** Setter for property homeStreet4.
     * @param homeStreet4 New value of property homeStreet4.
     *
     */
    public void setHomeStreet4(java.lang.String homeStreet4) {
        this.homeStreet4 = homeStreet4;
    }

    /** Setter for property homeStreetCode.
     * @param homeStreetCode New value of property homeStreetCode.
     *
     */
    public void setHomeStreetCode(java.lang.String homeStreetCode) {
        this.homeStreetCode = homeStreetCode;
    }

    /** Getter for property owner.
     * @return Value of property owner.
     *
     */
    public java.lang.Object getOwner() {
        return owner;
    }

    /** Setter for property owner.
     * @param owner New value of property owner.
     *
     */
    public void setOwner(java.lang.Object owner) {
        this.owner = owner;
    }

    /** Setter for property telHome.
     * @param telHome New value of property telHome.
     *
     */
    public void setTelHome(java.lang.String telHome) {
        this.telHome = telHome;
    }

    /** Setter for property telWork.
     * @param telWork New value of property telWork.
     *
     */
    public void setTelWork(java.lang.String telWork) {
        this.telWork = telWork;
    }
    private Object owner;
    private String homeStreet1 = "";
    private String homeStreet2 = "";
    private String homeStreet3 = "";
    private String homeStreet4 = "";
    private String homeStreetCode = "";
    private String homePostal1 = "";
    private String homePostal2 = "";
    private String homePostal3 = "";
    private String homePostal4 = "";
    private String homePostalCode = "";
    private String telHome = "";
    private String telWork = "";
    private String faxHome = "";
    private String faxWork = "";
    private String cellNo = "";
    private String eMail = "";
    private String phys_HOME_UNITNO = "";
    private String phys_HOME_COMPLEX = "";
    private String phys_HOME_STREETNO = "";
    private String phys_HOME_STREET = "";
    private String phys_HOME_SUBURB = "";
    private String phys_HOME_CITY = "";
    private String phys_HOME_CODE = "";
    private String phys_HOME_PROVINCE = "";
    private String phys_HOME_COUNTRY = "";
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
    C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.peopleware.Contact
    za.co.ucs.accsys.peopleware.Contact:    static final long serialVersionUID = -4677087021120430208L;

     */
    private static final long serialVersionUID = -4677087021120430208L;

    /**
     * @return the phys_HOME_UNITNO
     */
    public String getPhys_HOME_UNITNO() {
        if (phys_HOME_UNITNO == null) {
            return ("");
        } else {
            return phys_HOME_UNITNO;
        }
    }

    /**
     * @return the phys_HOME_COMPLEX
     */
    public String getPhys_HOME_COMPLEX() {
        if (phys_HOME_COMPLEX == null) {
            return ("");
        } else {
            return phys_HOME_COMPLEX;
        }
    }

    /**
     * @return the phys_HOME_STREETNO
     */
    public String getPhys_HOME_STREETNO() {
        if (phys_HOME_STREETNO == null) {
            return ("");
        } else {
            return phys_HOME_STREETNO;
        }
    }

    /**
     * @return the phys_HOME_STREET
     */
    public String getPhys_HOME_STREET() {
        if (phys_HOME_STREET == null) {
            return ("");
        } else {
            return phys_HOME_STREET;
        }
    }

    /**
     * @return the phys_HOME_SUBURB
     */
    public String getPhys_HOME_SUBURB() {
        if (phys_HOME_SUBURB == null) {
            return ("");
        } else {
            return phys_HOME_SUBURB;
        }
    }

    /**
     * @return the phys_HOME_CITY
     */
    public String getPhys_HOME_CITY() {
        if (phys_HOME_CITY == null) {
            return ("");
        } else {
            return phys_HOME_CITY;
        }
    }

    /**
     * @return the phys_HOME_CODE
     */
    public String getPhys_HOME_CODE() {
        if (phys_HOME_CODE == null) {
            return ("");
        } else {
            return phys_HOME_CODE;
        }
    }

    /**
     * @return the phys_HOME_PROVINCE
     */
    public String getPhys_HOME_PROVINCE() {
        if (phys_HOME_PROVINCE == null) {
            return ("");
        } else {
            return phys_HOME_PROVINCE;
        }
    }

    /**
     * @return the phys_HOME_COUNTRY
     */
    public String getPhys_HOME_COUNTRY() {
        if (phys_HOME_COUNTRY == null) {
            return ("");
        } else {
            return phys_HOME_COUNTRY;
        }
    }

    /**
     * @param phys_HOME_UNITNO the phys_HOME_UNITNO to set
     */
    public void setPhys_HOME_UNITNO(String phys_HOME_UNITNO) {
        this.phys_HOME_UNITNO = phys_HOME_UNITNO;
    }

    /**
     * @param phys_HOME_COMPLEX the phys_HOME_COMPLEX to set
     */
    public void setPhys_HOME_COMPLEX(String phys_HOME_COMPLEX) {
        this.phys_HOME_COMPLEX = phys_HOME_COMPLEX;
    }

    /**
     * @param phys_HOME_STREETNO the phys_HOME_STREETNO to set
     */
    public void setPhys_HOME_STREETNO(String phys_HOME_STREETNO) {
        this.phys_HOME_STREETNO = phys_HOME_STREETNO;
    }

    /**
     * @param phys_HOME_STREET the phys_HOME_STREET to set
     */
    public void setPhys_HOME_STREET(String phys_HOME_STREET) {
        this.phys_HOME_STREET = phys_HOME_STREET;
    }

    /**
     * @param phys_HOME_SUBURB the phys_HOME_SUBURB to set
     */
    public void setPhys_HOME_SUBURB(String phys_HOME_SUBURB) {
        this.phys_HOME_SUBURB = phys_HOME_SUBURB;
    }

    /**
     * @param phys_HOME_CITY the phys_HOME_CITY to set
     */
    public void setPhys_HOME_CITY(String phys_HOME_CITY) {
        this.phys_HOME_CITY = phys_HOME_CITY;
    }

    /**
     * @param phys_HOME_CODE the phys_HOME_CODE to set
     */
    public void setPhys_HOME_CODE(String phys_HOME_CODE) {
        this.phys_HOME_CODE = phys_HOME_CODE;
    }

    /**
     * @param phys_HOME_PROVINCE the phys_HOME_PROVINCE to set
     */
    public void setPhys_HOME_PROVINCE(String phys_HOME_PROVINCE) {
        this.phys_HOME_PROVINCE = phys_HOME_PROVINCE;
    }

    /**
     * @param phys_HOME_COUNTRY the phys_HOME_COUNTRY to set
     */
    public void setPhys_HOME_COUNTRY(String phys_HOME_COUNTRY) {
        this.phys_HOME_COUNTRY = phys_HOME_COUNTRY;
    }
}

