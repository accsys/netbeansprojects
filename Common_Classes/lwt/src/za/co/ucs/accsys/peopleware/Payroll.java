/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Payroll maps to dba.P_PAYROLL
 * @author  liam
 */
public final class Payroll implements AccsysObjectInterface, java.io.Serializable {

    static final long serialVersionUID = 1347805141631076580L;

    /** Creates a new instance of Payroll
     */
    public Payroll(Integer payrollID) {
        this.payrollID = payrollID;
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save payroll values to db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuilder updateSQL = new StringBuilder();
                updateSQL.append("update p_payroll set ");
                updateSQL.append("COUNTRY_ID=").append(getCountry().getCountryID()).append(", ");
                updateSQL.append("DESCRIPTION='").append(getDescription()).append("', ");
                updateSQL.append("NAME='").append(getName()).append("', ");
                updateSQL.append("PAYROLL_TYPE='").append(getPayrollType()).append("', ");
                updateSQL.append("TAX_NUMBER='").append(getTaxNumber()).append("', ");
                updateSQL.append("TAX_YEAR_END='").append(getTaxYearEnd()).append("', ");
                updateSQL.append("TAX_YEAR_START='").append(getTaxYearStart()).append("', ");
                updateSQL.append("UIF_NUMBER='").append(getUifNumber()).append("' ");
                updateSQL.append("where payroll_id=").append(this.payrollID);
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean equals(Object object) {
        return (this.payrollID.equals(((Payroll) object).getPayrollID()));
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            System.out.println("Unable to load employee values from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from p_payroll with (nolock) where payroll_id=" + this.payrollID, con);
                if (rs.next()) {
                    this.country = new Country(new Integer(rs.getInt("COUNTRY_ID")));
                    this.payrollID = new Integer(rs.getInt("PAYROLL_ID"));
                    this.description = rs.getString("DESCRIPTION");
                    this.name = rs.getString("NAME");
                    this.payrollType = rs.getString("PAYROLL_TYPE");
                    this.taxNumber = rs.getString("TAX_NUMBER");
                    this.taxYearEnd = rs.getDate("TAX_YEAR_END");
                    this.taxYearStart = rs.getDate("TAX_YEAR_START");
                    this.uifNumber = rs.getString("UIF_NUMBER");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        return (this.payrollID != null);
    }

    /** Getter for property payrollID.
     * @return Value of property payrollID.
     *
     */
    public java.lang.Integer getPayrollID() {
        return payrollID;
    }

    /** Getter for property country.
     * @return Value of property country.
     *
     */
    public Country getCountry() {
        return country;
    }

    /** Setter for property country.
     * @param country New value of property country.
     *
     */
    public void setCountry(Country country) {
        this.country = country;
    }

    /** Getter for property description.
     * @return Value of property description.
     *
     */
    public String getDescription() {
        return description;
    }

    /** Setter for property description.
     * @param description New value of property description.
     *
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public String getName() {
        return name;
    }

    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    /** Getter for property payrollType.
     * @return Value of property payrollType.
     *
     */
    public java.lang.String getPayrollType() {
        return payrollType;
    }

    /** Setter for property payrollType.
     * @param payrollType New value of property payrollType.
     *
     */
    public void setPayrollType(java.lang.String payrollType) {
        this.payrollType = payrollType;
    }

    /** Getter for property taxNumber.
     * @return Value of property taxNumber.
     *
     */
    public java.lang.String getTaxNumber() {
        return taxNumber;
    }

    /** Setter for property taxNumber.
     * @param taxNumber New value of property taxNumber.
     *
     */
    public void setTaxNumber(java.lang.String taxNumber) {
        this.taxNumber = taxNumber;
    }

    /** Getter for property taxYearEnd.
     * @return Value of property taxYearEnd.
     *
     */
    public java.util.Date getTaxYearEnd() {
        return taxYearEnd;
    }

    /** Setter for property taxYearEnd.
     * @param taxYearEnd New value of property taxYearEnd.
     *
     */
    public void setTaxYearEnd(java.util.Date taxYearEnd) {
        this.taxYearEnd = taxYearEnd;
    }

    /** Getter for property taxYearStart.
     * @return Value of property taxYearStart.
     *
     */
    public java.util.Date getTaxYearStart() {
        return taxYearStart;
    }

    /** Setter for property taxYearStart.
     * @param taxYearStart New value of property taxYearStart.
     *
     */
    public void setTaxYearStart(java.util.Date taxYearStart) {
        this.taxYearStart = taxYearStart;
    }

    /** Getter for property uifNumber.
     * @return Value of property uifNumber.
     *
     */
    public java.lang.String getUifNumber() {
        return uifNumber;
    }

    /** Setter for property uifNumber.
     * @param uifNumber New value of property uifNumber.
     *
     */
    public void setUifNumber(java.lang.String uifNumber) {
        this.uifNumber = uifNumber;
    }
    private Integer payrollID;
    private Country country;
    private String description;
    private String name;
    private String payrollType;
    private String taxNumber;
    private java.util.Date taxYearEnd;
    private java.util.Date taxYearStart;
    private String uifNumber;
}
