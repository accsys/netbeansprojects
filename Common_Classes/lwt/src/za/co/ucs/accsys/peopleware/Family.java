/*
 * Family.java
 *
 * Created on July1 8, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * Family maps to E_FAMILY in the database
 *
 * @author liam
 */
public final class Family implements AccsysReadOnlyObjectInterface, java.io.Serializable {

    static final long serialVersionUID = 3911807561110823186L;

    /**
     * Creates a new instance of FamilyMember
     *
     * @param employee
     * @param familyID
     */
    public Family(Employee employee, Integer familyID) {
        this.employee = employee;
        this.familyID = familyID;
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void loadFromDB() {
        if (this.employee == null) {
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from e_family with (nolock) where "
                        + " Company_id=" + this.employee.getCompany().getCompanyID()
                        + " and Employee_id=" + this.employee.getEmployeeID()
                        + " and Family_id=" + this.familyID, con);
                if (rs.next()) {
                    if (rs.getString("FIRSTNAME") != null) {
                        this.firstName = rs.getString("FIRSTNAME");
                    }
                    if (rs.getString("SECONDNAME") != null) {
                        this.secondName = rs.getString("SECONDNAME");
                    }
                    if (rs.getString("SURNAME") != null) {
                        this.surname = rs.getString("SURNAME");
                    }
                    if (rs.getString("RELATION") != null) {
                        this.relation = rs.getString("RELATION");
                    }
                    if (rs.getString("GENDER") != null) {
                        this.gender = rs.getString("GENDER");
                    }
                    if (rs.getDate("BIRTHDATE") != null) {
                        this.birthDate = DatabaseObject.formatDate(rs.getDate("BIRTHDATE"));
                    }
                    if (rs.getString("TEL_HOME") != null) {
                        this.telHome = rs.getString("TEL_HOME");
                    }
                    if (rs.getString("TEL_WORK") != null) {
                        this.telWork = rs.getString("TEL_WORK");
                    }
                    if (rs.getString("TEL_CELL") != null) {
                        this.telCell = rs.getString("TEL_CELL");
                    }
                    if (rs.getString("E_MAIL") != null) {
                        this.EMail = rs.getString("E_MAIL");
                    }
                    if (rs.getString("MED_DEPENDENT_YN") != null) {
                        this.medDependent = (rs.getString("MED_DEPENDENT_YN").compareToIgnoreCase("Y") == 0);
                    }
                    this.taxDependent = false;
                    if (rs.getString("TAX_DEPENDENT_YN") != null) {
                        this.taxDependent = (rs.getString("TAX_DEPENDENT_YN").compareToIgnoreCase("Y") == 0);
                    }

                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        return (this.employee.isValid());
    }

    public String getBirthDate() {
        return birthDate;
    }

    public za.co.ucs.accsys.peopleware.Employee getEmployee() {
        return employee;
    }

    public java.lang.Integer getFamilyID() {
        return familyID;
    }

    public java.lang.String getFirstName() {
        return firstName;
    }

    public java.lang.String getGender() {
        return gender;
    }

    public boolean isMedDependent() {
        return medDependent;
    }

    public String getMedDependent() {
        if (isMedDependent()) {
            return "Y";
        } else {
            return "N";
        }
    }

    public java.lang.String getRelation() {
        return relation;
    }

    public java.lang.String getSecondName() {
        return secondName;
    }

    public java.lang.String getSurname() {
        return surname;
    }

    public boolean isTaxDependent() {
        return taxDependent;
    }

    public String getTelHome() {
        return telHome;
    }

    public java.lang.String getTelWork() {
        return telWork;
    }

    public java.lang.String getTelCell() {
        return telCell;
    }

    public java.lang.String getEMail() {
        return EMail;
    }

    private final Employee employee;
    private final Integer familyID;
    private String firstName = "";
    private String secondName = "";
    private String surname = "";
    private String relation = "";
    private boolean medDependent = false;
    private boolean taxDependent = false;
    private String gender = "";
    private String birthDate = "";
    private String telHome = "";
    private String telWork = "";
    private String telCell = "";
    private String EMail = "";
}
