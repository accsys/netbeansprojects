/*
 * accsysObjectInterface.java
 *
 * Created on January 15, 2004, 12:35 PM
 */

package za.co.ucs.accsys.peopleware;

/**
 * The AccsysObjectInterface is used to ensure consistency amongs all the Accsys classes
 * @author  liam
 */
public interface AccsysObjectInterface {
    /** commits / saves changed to the database 
     */
     void applyToDB();
     
     /** reads / loads data from database
      */
     void loadFromDB();
     
     /** Do we have enough information to read/update the database table?
      * @return 
      */
     boolean isValid();
}
