/*
 * TRHistoricalEvent.java
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * TRHEvent maps to dba.SD_EVENT, SD_PROVIDER, SD_COURSE to create an entity that could be used to schedule future training
 * @Author  liam
 */
public final class TREvent implements AccsysObjectInterface {

    /** Creates an instance of an entity that could be used to schedule future training
     * @param providerID As the name suggests. 
     */
    public TREvent(Integer eventID) {
        this.setEventID(eventID);
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public boolean isValid() {
        boolean valid = false;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder queryString = new StringBuilder("SELECT first * from TR_EVENT where EVENT_ID= ").append(this.getEventID());
            ResultSet rs = DatabaseObject.openSQL(queryString.toString(), con);
            if (rs.next()) {
                valid = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return valid;
    }

    @Override
    public void applyToDB() {
        // Not implemented for SD_EVENT.  To be managed through the Skills Development Module only.
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            return;
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuilder queryString = new StringBuilder("SELECT TR_PROVIDER.NAME AS Provider,");
                queryString.append("TR_COURSE.NAME AS Course, TR_EVENT.VENUE,");
                queryString.append("TR_EVENT.FROM_DATE, TR_EVENT.TO_DATE ,");
                queryString.append("TR_COURSE.DESCRIPTION, TR_EVENT.EVENT_ID, ");
                queryString.append("TR_EVENT.COST_EVENT, TR_EVENT.COST_TRAVEL, TR_EVENT.COST_ACCOMODATION, ");
                queryString.append("TR_EVENT.COST_MATERIALS, TR_EVENT.COST_ALLOWANCES, TR_EVENT.COST_OTHER ");
                queryString.append("FROM ( DBA.TR_EVENT with (nolock) JOIN DBA.TR_COURSE with (nolock) ON DBA.TR_EVENT.COURSE_ID = DBA.TR_COURSE.COURSE_ID ) ");
                queryString.append("        JOIN DBA.TR_PROVIDER ON DBA.TR_COURSE.PROVIDER_ID = DBA.TR_PROVIDER.PROVIDER_ID ");
                queryString.append("WHERE TR_EVENT.EVENT_ID= ").append(this.getEventID());
                ResultSet rs = DatabaseObject.openSQL(queryString.toString(), con);
                if (rs.next()) {
                    setCourseName(rs.getString("Course"));
                    setProviderName(rs.getString("Provider"));
                    setVenue(rs.getString("VENUE"));
                    setFromDate(rs.getDate("FROM_DATE"));
                    setToDate(rs.getDate("TO_DATE"));
                    setCourseDescription(rs.getString("DESCRIPTION"));
                    setEventID(rs.getInt("EVENT_ID"));
                    setCostAccomodation(rs.getFloat("COST_ACCOMODATION"));
                    setCostAllowances(rs.getFloat("COST_ALLOWANCES"));
                    setCostEvent(rs.getFloat("COST_EVENT"));
                    setCostMaterials(rs.getFloat("COST_MATERIALS"));
                    setCostOther(rs.getFloat("COST_OTHER"));
                    setCostTravel(rs.getFloat("COST_TRAVEL"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public String toString() {
        return ("Event:" + getCourseName() + " on " + getFromDate() + " at " + getVenue());
    }
    private int eventID = 0;
    private String providerName;
    private String courseName;
    private String venue;
    private java.util.Date fromDate;
    private java.util.Date toDate;
    private String courseDescription;
    private float costEvent;
    private float costTravel;
    private float costAccomodation;
    private float costMaterials;
    private float costAllowances;
    private float costOther;

    /**
     * @return the eventID
     */
    public int getEventID() {
        return eventID;
    }

    /**
     * @param eventID the eventID to set
     */
    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    /**
     * @return the providerName
     */
    public String getProviderName() {
        return providerName;
    }

    /**
     * @param providerName the providerName to set
     */
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the venue
     */
    public String getVenue() {
        return venue;
    }

    /**
     * @param venue the venue to set
     */
    public void setVenue(String venue) {
        this.venue = venue;
    }

    /**
     * @return the fromDate
     */
    public java.util.Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(java.util.Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public java.util.Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(java.util.Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the courseDescription
     */
    public String getCourseDescription() {
        return courseDescription;
    }

    /**
     * @param courseDescription the courseDescription to set
     */
    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    /**
     * @return the costEvent
     */
    public float getCostEvent() {
        return costEvent;
    }

    /**
     * @param costEvent the costEvent to set
     */
    public void setCostEvent(float costEvent) {
        this.costEvent = costEvent;
    }

    /**
     * @return the costTravel
     */
    public float getCostTravel() {
        return costTravel;
    }

    /**
     * @param costTravel the costTravel to set
     */
    public void setCostTravel(float costTravel) {
        this.costTravel = costTravel;
    }

    /**
     * @return the costAccomodation
     */
    public float getCostAccomodation() {
        return costAccomodation;
    }

    /**
     * @param costAccomodation the costAccomodation to set
     */
    public void setCostAccomodation(float costAccomodation) {
        this.costAccomodation = costAccomodation;
    }

    /**
     * @return the costMaterials
     */
    public float getCostMaterials() {
        return costMaterials;
    }

    /**
     * @param costMaterials the costMaterials to set
     */
    public void setCostMaterials(float costMaterials) {
        this.costMaterials = costMaterials;
    }

    /**
     * @return the costAllowances
     */
    public float getCostAllowances() {
        return costAllowances;
    }

    /**
     * @param costAllowances the costAllowances to set
     */
    public void setCostAllowances(float costAllowances) {
        this.costAllowances = costAllowances;
    }

    /**
     * @return the costOther
     */
    public float getCostOther() {
        return costOther;
    }

    /**
     * @param costOther the costOther to set
     */
    public void setCostOther(float costOther) {
        this.costOther = costOther;
    }
}
