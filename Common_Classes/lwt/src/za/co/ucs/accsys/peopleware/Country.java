/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Country maps to dba.P_COUNTRY
 * @author  liam
 */
public class Country implements AccsysObjectInterface, java.io.Serializable {
    
    /** Creates a new instance of Country
     */
    public Country(Integer countryID) {
        this.countryID = countryID;
        if (isValid()) {
            loadFromDB();
        }
    }
    
    @Override
    public void applyToDB() {
        if (!isValid()){
            System.out.println("Unable to save country values to db.");
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuilder updateSQL = new StringBuilder();
                updateSQL.append("update p_country set ");
                updateSQL.append("CURRENCY='").append(getCurrency()).append("', ");
                updateSQL.append("CURRENCY_CONVERSION_FACTOR=").append(getCurrencyConversionFactor()).append(", ");
                updateSQL.append("LANGUAGE='").append(getLanguage()).append("', ");
                updateSQL.append("NAME='").append(getName()).append("' ");
                updateSQL.append("where country_id=").append(this.countryID);
                
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    @Override
    public void loadFromDB() {
        if (!isValid()){
            System.out.println("Unable to load country values from db.");
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from p_country where country_id="+this.countryID, con);
                if (rs.next()){
                    this.currency = rs.getString("CURRENCY");
                    this.currencyConversionFactor = rs.getFloat("CURRENCY_CONVERSION_FACTOR");
                    this.language = rs.getString("LANGUAGE");
                    this.name = rs.getString("NAME");
                    
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    @Override
    public boolean isValid() {
        return (this.countryID != null);
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
    /** Getter for property language.
     * @return Value of property language.
     *
     */
    public java.lang.String getLanguage() {
        return language;
    }
    
    /** Setter for property language.
     * @param language New value of property language.
     *
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }
    
    /** Getter for property currencyConversionFactor.
     * @return Value of property currencyConversionFactor.
     *
     */
    public float getCurrencyConversionFactor() {
        return currencyConversionFactor;
    }
    
    /** Setter for property currencyConversionFactor.
     * @param currencyConversionFactor New value of property currencyConversionFactor.
     *
     */
    public void setCurrencyConversionFactor(float currencyConversionFactor) {
        this.currencyConversionFactor = currencyConversionFactor;
    }
    
    /** Getter for property currency.
     * @return Value of property currency.
     *
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /** Setter for property currency.
     * @param currency New value of property currency.
     *
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    /** Getter for property countryID.
     * @return Value of property countryID.
     *
     */
    public java.lang.Integer getCountryID() {
        return countryID;
    }
    
    
    
    private Integer countryID;
    private String currency;
    private float currencyConversionFactor;
    private String language;
    private String name;
    
}
