/*
 * HolidayInfo.java
 *
 * Created on 08 May 2006, 01:55
 *
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import java.util.*;
import za.co.ucs.lwt.db.*;


/**
 * HolidayInfo contains records for each and every holiday that this employee are linked to
 * @author lwt
 */
public final class HolidayInfo implements AccsysReadOnlyObjectInterface, java.io.Serializable {
    
    
    /** Creates a new instance of Company
     * <br>If companyID = null, a new Company records will be created.
     * @param employee
     */
    public HolidayInfo(Employee employee) {
        this.employee = employee;
        if (isValid()) {
            loadFromDB();
        }
    }
    
    @Override
    public boolean isValid() {
        return (this.employee.isValid());
    }
    
    // Loads all the leave information from the database
    @Override
    public void loadFromDB() {
        if (this.employee == null) {
        }
        else {
            Connection con = null;
            String sql;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                // Only load holidays for the past year or later
                sql = "select NAME as holName, theDate from ll_public_holiday with (nolock) where "+
                      " holprofile_id in (select max(holprofile_id) from ll_emaster_holidayprofile with (nolock) where "+
                        " company_id =  " +this.employee.getCompany().getCompanyID()+
                        " and  Employee_ID = "+this.employee.getEmployeeID()+
                        " and End_Date is null)";
                ResultSet rs = DatabaseObject.openSQL(sql, con);
                
                while (rs.next()){
                    HolidayDetail holidayDetail = new HolidayDetail(this.employee, rs.getString("holName"), rs.getDate("theDate"));
                    //System.out.println("Loading "+holidayDetail);
                    // Add Holiday to TreeMap.  This TreeMap stores one HolidayDetail object per record for this employee.
                    holidays.add(holidayDetail);
                }
                
                rs.close();
            } catch (SQLException e){
                e.printStackTrace();
            } finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    public Employee getEmployee(){
        return this.employee;
    }
    
    /** Returns a TreeMap with a java.util.Date as the Key and an instance of holidayDetail as the value
     * for each holiday linked to this employee
     * @return 
     */
    public LinkedList<HolidayDetail> getHolidays(){
        return this.holidays;
    }
    
    /** Returns the (first) HolidayDetail instance (or null) if the employee is linked to a
     * holiday entry on the given date
     * @param onDay The 'Long' version of the date, excluding hours, minutes and seconds
     * @return HolidayDetail representing the detail of the public holiday
     */
    public HolidayDetail didHaveHolidayOn(java.util.Date onDay){
        for (HolidayDetail holDetail : holidays) {
            if (DatabaseObject.onSameDay(holDetail.getDate(),onDay)){
                return holDetail;
            }
        }
        return null;
    }
    
    private final Employee employee;
// We want the Holidays linked to this employee sorted by date
    private final LinkedList<HolidayDetail> holidays = new LinkedList<>();
}
