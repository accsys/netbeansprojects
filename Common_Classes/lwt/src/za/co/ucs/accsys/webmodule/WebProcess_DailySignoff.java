package za.co.ucs.accsys.webmodule;

import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;
import java.sql.*;
import java.util.StringTokenizer;

/**
 * <p>
 * A WebProcess_DailySignoff is an extension of WebProcess that specializes in
 * the signing off of T&A hours on a daily basis
 * </p>
 */
public final class WebProcess_DailySignoff extends WebProcess {

    /**
     * To change Contact information, WebProcess_EmployeeContact requires
     *
     * @param processDefinition - details of reporting structure, etc
     * @param owner - who created/is responsible for this process?
     * @param employee - which employee is requesting leave?
     */
    public WebProcess_DailySignoff(WebProcessDefinition processDefinition, Employee owner, Employee employee, String authDate, String variableID, String oldValue, String newValue, String reason) {
        super(processDefinition, owner, employee);

        this.employee = employee;
        this.authDate = authDate;
        this.payrollID = employee.getPayroll().getPayrollID().toString();
        this.payrollDetailID = getPayrollDetailID(payrollID, authDate);
        this.variableID = variableID;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.reason = reason;
        this.calcYear = getCalendarYear(payrollID, payrollDetailID);
        this.variableName = getVariableName();

        // Is this a valid request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            this.escalateProcess(et_INITIAL, "");
        }
    }

    /**
     * This method should be implemented by each and every descendent of
     * WebProcess. It returns an instance of ValidationResult which contains
     * both the isValid() property as well as a reason in case of a non-valid
     * result.
     */
    @Override
    public ValidationResult validate() {
        if (isActive()) {

            /* DESCRIPTION: Presently, I can see no reason why we cannot simply perform the authorisation/signoff once it's reached this stage
             * 
             */
            /*
            
            
             // Does the employee still exist?
             Connection con = null;
             try {             
            
             con = DatabaseObject.getNewConnectionFromPool();
            
             // Is the payroll still open (for this employee)?
             String openPayroll = "select first closed_yn from p_calc_employeelist with (readuncommitted)  "
             + "where company_id=" + employee.getCompany().getCompanyID()
             + "and employee_id=" + employee.getEmployeeID()
             + "and runtype_id=fn_P_GetOpenRuntype(company_id, payroll_id, payrolldetail_id)";
            
             ResultSet rs1 = DatabaseObject.openSQL(openPayroll, con);
             if (rs1.next()) {
             if (rs1.getString(1).compareToIgnoreCase("Y") == 0) {
             rs1.close();
             DatabaseObject.releaseConnection(con);
             return (new ValidationResult(false, "The payroll for employee (employee_id=" + this.getEmployee().getEmployeeID() + ") is already closed for this period."));
             }
             }
             rs1.close();
             } catch (SQLException e) {
             e.printStackTrace();
             } finally {
             DatabaseObject.releaseConnection(con);
             }
             * 
             */
        }
        return (new ValidationResult(true, ""));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("TnA Daily Signoff:").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName());
        result.append("\n\tOn: ").append(DatabaseObject.formatDate(this.authDate)).append("\t").append("For:").append(this.variableName);
        result.append("\n\tValue: ").append(this.oldValue);
        if (this.oldValue.trim().compareToIgnoreCase(this.newValue.trim()) != 0) {
            result.append("\tChanged to: ").append(this.newValue);
        }

        return result.toString();
    }

    // Explanation of process in HTML format
    public String toShortHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append(getChanges());

        return result.toString();
    }

    // Explanation of process in HTML format
    @Override
    public String toHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append("Process Number: [").append(this.hashCode()).append("]<br>");

        // Explanation of what changes were requested
        result.append("Authorise T&A hours: ").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName()).append(getChanges()).append("<br>");

        return result.toString();
    }

    /**
     * Returns a String representation of the request
     */
    private String getChanges() {
        StringBuilder result = new StringBuilder();

        result.append("<table class=process_detail><font size='-3'>");
        // Did the values change?
        if (this.oldValue.trim().compareToIgnoreCase(this.newValue.trim()) != 0) {
            result.append("<tr><th align=\"left\">Day</th><th align=\"left\">Variable</th><th align=\"left\">Calculated</th><th align=\"left\">Changed</th><th align=\"left\">Reason</th></tr>");
            result.append("<tr><td align=\"left\">").append(this.authDate).append("</td><td align=\"left\">").append(this.getVariableName()).append("</td>");
            result.append("    <td align=\"left\">").append(this.oldValue.trim()).append("</td><td align=\"left\"><font color=red>").append(this.newValue.trim()).append("</font></td>");
            result.append("    <td align=\"left\">").append(this.reason).append("</td></tr>");
        } else {
            result.append("<tr><th align=\"left\">Day</th><th align=\"left\">Variable</th><th align=\"left\">Calculated</th><th align=\"left\">Reason</th></tr>");
            result.append("<tr><td align=\"left\">").append(this.authDate).append("</td><td align=\"left\">").append(this.getVariableName()).append("</td>");
            result.append("    <td align=\"left\">").append(this.oldValue.trim()).append("</td>");
            result.append("    <td align=\"left\">").append(this.reason).append("</td></tr>");
        }

        result.append("</font></table>");
        return result.toString();
    }

    /**
     * This statement gets executed when the process is to be written to the
     * database.
     */
    @Override
    String getCommitStatement() {
        return getCommitCONSTANTStatement();
    }

    private String getCommitCONSTANTStatement() {
        StringBuilder result = new StringBuilder();
        //
        // TAL_EMASTER_PVARIABLE_DAILY
        //
        result.append("insert into TAL_EMASTER_PVARIABLE_DAILY (company_id, employee_id, payroll_id, payrolldetail_id, day, variable_id, theValue, final_value, calc_year)");
        result.append(" on existing update ");
        result.append("values ( ").append(employee.getCompany().getCompanyID()).append(", ");
        result.append("         ").append(employee.getEmployeeID()).append(", ");
        result.append("         ").append(this.payrollID).append(", ");
        result.append("         ").append(this.payrollDetailID).append(", ");
        result.append("         '").append(this.authDate).append("', ");
        result.append("         ").append(this.variableID).append(", ");
        result.append("         ").append(getHourMinuteValueInHoursDecimal(this.oldValue)).append(", ");
        result.append("         ").append(getHourMinuteValueInHoursDecimal(this.newValue)).append(", ");
        result.append("         ").append(getCalendarYear(payrollID, payrollDetailID)).append("); ");

        //
        // TA_CALC_EMPLOYEELIST_DAILY
        //
        result.append("insert into TA_CALC_EMPLOYEELIST_DAILY (company_id, employee_id, payroll_id, payrolldetail_id, day, signedoff_yn)");
        result.append(" on existing update ");
        result.append("values ( ").append(employee.getCompany().getCompanyID()).append(", ");
        result.append("         ").append(employee.getEmployeeID()).append(", ");
        result.append("         ").append(this.payrollID).append(", ");
        result.append("         ").append(this.payrollDetailID).append(", ");
        result.append("         '").append(this.authDate).append("', ");
        result.append("         'Y'); ");

        return (result.toString());
    }

    @Override
    public String getProcessName() {
        return "TnA Daily Signoff";
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact
     za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact:    static final long serialVersionUID = -1459535708782221840L;
    
     */

    /**
     * Returns '08:10' as 8.166
     *
     * @return
     */
    private float getHourMinuteValueInHoursDecimal(String hoursAndMinutes) {
        StringTokenizer token = new StringTokenizer(hoursAndMinutes, ":");
        // Pad with leading zeros
        String hrs = "00" + token.nextToken();
        hrs = hrs.substring(hrs.length() - 2, hrs.length());
        float fHrs = new Float(hrs).floatValue();

        // Pad with trailing zeros
        String mins = token.nextToken() + "00";
        mins = mins.substring(0, 2);
        float fMins = new Float(mins).floatValue();

        // Convert to decimal
        float rslt = fHrs + fMins / 60;
        return rslt;
    }

    private String getVariableName() {
        // Does the employee still exist?
        if (this.variableName != null) {
            return this.variableName;
        } else {
            Connection con = null;
            String varName = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();

                // Is the payroll still open (for this employee)?
                String sql = "select name from p_variable with (readuncommitted) where variable_id = " + this.variableID;

                ResultSet rs1 = DatabaseObject.openSQL(sql, con);
                if (rs1.next()) {
                    varName = rs1.getString(1);
                }
                rs1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
            this.variableName = varName;
            return varName;
        }
    }

    private String getPayrollDetailID(String payrollID, String aDate) {
        // Does the employee still exist?
        if (this.payrollDetailID != null) {
            return this.payrollDetailID;
        } else {
            Connection con = null;
            String pdID = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();

                // Is the payroll still open (for this employee)?
                String sql = "select payrolldetail_id from p_payrolldetail with (readuncommitted) where payroll_id=" + payrollID
                        + " and '" + this.authDate + "' between timemod_startdate and timemod_enddate";

                ResultSet rs1 = DatabaseObject.openSQL(sql, con);
                if (rs1.next()) {
                    pdID = rs1.getString(1);
                }
                rs1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
            this.payrollDetailID = pdID;
            return pdID;
        }
    }

    private String getCalendarYear(String payrollID, String payrolldetailID) {
        // Does the employee still exist?
        if (this.calcYear != null) {
            return this.calcYear;
        } else {
            Connection con = null;
            String calcY = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();

                // Is the payroll still open (for this employee)?
                String sql = "select max(convert(char(4),datepart(year,startdate))) from p_payrolldetail with (readuncommitted) where payroll_id=" + payrollID
                        + " and payrolldetail_id=" + payrolldetailID;

                ResultSet rs1 = DatabaseObject.openSQL(sql, con);
                if (rs1.next()) {
                    calcY = rs1.getString(1);
                }
                rs1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
            this.calcYear = calcY;
            return calcY;
        }
    }

    /**
     * Returns the day for which these hours were logged.
     *
     * @return
     */
    public String getAuthDay() {
        return this.authDate;
    }
    private final Employee employee;
    private final String payrollID;
    private String payrollDetailID = null;
    private final String authDate;
    private final String variableID;
    private final String oldValue;
    private final String newValue;
    private final String reason;
    private String calcYear;
    private String variableName = null;
} // end LeaveRequisitionProcess

