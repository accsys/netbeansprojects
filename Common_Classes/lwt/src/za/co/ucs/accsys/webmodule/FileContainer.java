/*
 * FileContainer.java
 *
 * Created on June 21, 2004, 4:26 PM
 */
package za.co.ucs.accsys.webmodule;

import java.io.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import lwt_tests.RunTests;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang.StringEscapeUtils;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.tools.Request;
import za.co.ucs.accsys.tools.XMLRequestHandler;
import za.co.ucs.lwt.db.DatabaseObject;
import za.co.ucs.lwt.initools.OutputFile;

/**
 * This is the MOTHER of all the classes. It houses all the instances of
 * EmployeeSelections, ReportingStructures, ProcessDefinitions and current
 * WebProcesses. 22/09/2004: Added Employees, Payslips and LeaveInfo - To ensure
 * the whole process takes place off-line.
 *
 * @author liam
 */
public final class FileContainer implements java.io.Serializable {

    // <editor-fold  desc="Public Instantiation Methods">
    /**
     * Returns an instance of the FileContainer
     */
//    public static FileContainer getInstance() {
//        return getInstance(true);
//    }
    /**
     * Returns and optionally creates an instance of FileContainer
     *
     * @return FileContainer
     */
    @SuppressWarnings("SleepWhileInLoop")
    public static FileContainer getInstance(/*boolean startDaemon*/) {
        // If we are already trying to instantiate, wait...
        while (isBusyCreatingFileContainer) {
            try {
                Thread.sleep(5000); // do nothing for 4000 miliseconds (5 seconds)
                System.out.println("Waiting for FileContainer inititation...");
            } catch (InterruptedException e) {
            }
        }

        if (fileContainer == null) {
            isBusyCreatingFileContainer = true;

            System.out.println("Step 1: Creating FileContainer");
            if (fileContainer == null) {
                fileContainer = new FileContainer();
            }

            System.out.println("Step 2: Loading Database Cache");
            if (employeeMap.isEmpty()) {
                loadDatabaseCache();
            }

            //Optionally) preparing Derby Database for first use...
            createDerbyTablesIfNotExist();

            // Whatever is still stored in wmProcesses.bin, move it into the Derby Database
            moveProcessesFromFileToDB();

            System.out.println("Step 4: Loading and Synchronizing Web Processes between Derby DB and Sybase DB");
            boolean onlyActive = ((WebModulePreferences.getInstance().getPreference(WebModulePreferences.MEMORY_OnlyLoadActiveProcesses, "false")).compareToIgnoreCase("true") == 0);
            webProcesses = loadFromDerbyAndSyncToSybase(onlyActive);

            System.out.println("\n--------------------------------------------\nFileContainer ready for use\n--------------------------------------------");
            isBusyCreatingFileContainer = false;
            isESSReportingRegistered = DatabaseObject.isESSReportingRegistered();
        }

        return fileContainer;
    }

    // </editor-fold>
    /**
     * Creates a new instance of FileContainer
     */
    private FileContainer() {
        this(getWorkingDirectory());
    }

    public static void persistManagementStructureToDB() {
        System.out.println("**********************************************************");
        System.out.println("*** Daemon: Mobile Repoting Processing at " + (new java.util.Date().toString()));
        System.out.println("*** (Populate ESS_MOBI_REPORTING with entries)");
        System.out.println("**********************************************************");

        int employeeGroup = 0;

        try {
            // 1. Clear ESS_MOBI_REPORTING
            getInstance(); // Instantiate SELF
            FileContainer.clearEssMobiReporting();

            // 2. Get reporting structure for leave requests
            WebProcessDefinition procDef = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
            if (procDef != null) {
                ReportingStructure leaveRequestReportingStructure = procDef.getReportingStructure();
                ArrayList<Employee> allEmployees = leaveRequestReportingStructure.getAllEmployeesInEmployeeSelections();
                for (Employee emp : allEmployees) {
                    if (fileContainer.getSubordinates(emp, procDef).size() > 0) {
                        // 1. Find Employee Selections containing this employee
                        LinkedList<EmployeeSelection> empSelections = leaveRequestReportingStructure.getAllEmployeeSelections(emp);
                        LinkedList<EmployeeSelection> directDecendants = new LinkedList<>();
                        // 2. For each selection, return the selections directly underneath it and add it to 'directDecendants'
                        for (EmployeeSelection selection : empSelections) {
                            directDecendants.addAll(leaveRequestReportingStructure.getChildEmployeeSelections(selection));
                        }
                        // 3. Fetch the employees
                        HashSet<Employee> decendants = new HashSet<>();
                        for (EmployeeSelection selection : directDecendants) {
                            decendants.addAll(SelectionInterpreter.getInstance().generateEmployeeList(selection));
                        }
                        // 4. Persist employees to DB
                        for (Employee decendantEmployee : decendants) {
                            fileContainer.populateEssMobiReporting(emp.getCompany().getCompanyID(), emp.getEmployeeID(), decendantEmployee.getCompany().getCompanyID(), decendantEmployee.getEmployeeID(), employeeGroup);
                        }
                        employeeGroup++;
                    }
                }
                System.out.println("**********************************************************");
                System.out.println("***              ...Done populating ESS_MOBI_REPORTING.");
                System.out.println("**********************************************************");
            } else {
                System.out.println("**********************************************************");
                System.out.println("***              ...Unable to populate ESS_MOBI_REPORTING.");
                System.out.println("***              ...No reporting structure for Leave Requests found.");
                System.out.println("**********************************************************");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("**********************************************************");
            System.out.println("**********************************************************");
            System.out.println("           persistManagementStructureToDB failed!");
            System.out.println("**********************************************************");
            System.out.println("**********************************************************");
        }
    }

    public static void processMOBIRequests() {
        //System.out.println("**********************************************************");
        //System.out.println("*** Daemon: Mobile Request Queue Processing at " + (new java.util.Date().toString()));
        //System.out.println("*** (Scan ESS_Process_Queue for entries)");
        // Connect to database
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();

            // Read entries from ESS_PROCESS_QUEUE
            ResultSet rsESSRequests = DatabaseObject.openSQL("SELECT ID, COMPANY_ID, EMPLOYEE_ID, XML_REQUEST FROM DBA.ESS_PROCESS_QUEUE with (nolock)", con);

            // Process each entry
            ArrayList<Integer> processedIDs = new ArrayList<>();

            while (rsESSRequests.next()) {
                Company company = new Company(rsESSRequests.getInt("COMPANY_ID"));
                Employee employee = new Employee(company, rsESSRequests.getInt("EMPLOYEE_ID"));
                Request essRequest = XMLRequestHandler.getInstance().parseXMLRequest(StringEscapeUtils.escapeXml(rsESSRequests.getString("XML_REQUEST")));
                // Apply this request
                essRequest.actionRequest(employee);
                // Remember this ID in order to delete it later
                processedIDs.add(rsESSRequests.getInt("ID"));
            }
            rsESSRequests.close();

            //Clear ESS_REQUEST_QUEUE
            for (Integer anID : processedIDs) {
                System.out.println("*** (Removing Mobi_process from queue) " + anID);
                DatabaseObject.executeSQL("DELETE FROM DBA.ESS_PROCESS_QUEUE where ID=" + anID, con);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }

        //System.out.println("***              ...Processing Completed.");
        //System.out.println("**********************************************************");
    }

    private FileContainer(String workingDirectory) {

        // Load the state of the system from the predefined files
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));

        System.out.println("\n\n\n***** JAR VERSION *****");
        System.out.println(ESSVersion.getInstance().getJarVersion());
        System.out.println("\n\n\n");

        // At any point, we HAVE to load the Reporting Structures, Employee Selections, etc.
        loadESSConfiguration(workingDirectory);

    }

    /**
     * Getter for property employeeSelections.
     *
     * @return
     */
    public java.util.ArrayList getEmployeeSelections() {
        return employeeSelections;
    }

    /**
     * Returns the list of marital statuses to choose from in PeopleWare
     *
     * @return
     */
    public static ArrayList<String> getMaritalStatusOptions() {
        ArrayList<String> rslt = new ArrayList<>();
        rslt.add("Single");
        rslt.add("Married");
        rslt.add("Divorced");
        rslt.add("Traditional Marriage");
        rslt.add("Living Together");
        rslt.add("Widowed");

        return rslt;

    }

    /**
     * Returns the list of languages available to choose from in PeopleWare
     *
     * @return
     */
    public static ArrayList<String> getLanguageOptions() {
        ArrayList<String> rslt = new ArrayList<>();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select LANGUAGE from a_language with (nolock) order by LANGUAGE";
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);

            while (rs.next()) {
                String race = rs.getString("LANGUAGE");
                rslt.add(race);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Returns the list of races available to choose from in PeopleWare
     *
     * @return
     */
    public static ArrayList<String> getRaceOptions() {
        ArrayList<String> rslt = new ArrayList<>();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select DESCRIPTION from c_group with (nolock) where parent_group_id=2 order by DESCRIPTION";
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);

            while (rs.next()) {
                String race = rs.getString("DESCRIPTION");
                rslt.add(race);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Returns a list of companies in PeopleWare
     *
     * @return
     */
    public java.util.ArrayList getCompanies() {
        ArrayList<Company> rslt = new ArrayList<>();

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select company_id, name from C_MASTER with (nolock) order by \"NAME\"";
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);

            while (rs.next()) {
                Company company = new Company(rs.getInt("company_id"));
                rslt.add(company);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Returns a list of CostCentres in PeopleWare for the given Company
     *
     * @param company
     * @return
     */
    public java.util.ArrayList getCostCentres(Company company) {
        ArrayList<CostCentre> rslt = new ArrayList<>();

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select cost_id from C_COSTMAIN with (nolock) where company_id=" + company.getCompanyID() + " order by \"NAME\"";
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);

            while (rs.next()) {
                CostCentre costCentre = new CostCentre(company, rs.getInt("cost_id"));
                rslt.add(costCentre);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Returns a list of employees in PeopleWare
     *
     * @param company - the company for which we should return Employees. If
     * NULL, return all the employees from all the companies
     * @return
     */
    public java.util.ArrayList getEmployees(Company company) {
        ArrayList<Employee> rslt = new ArrayList<>();

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select company_id, employee_id, surname, firstname from E_MASTER with (nolock) "
                    + " where active_yn='Y' order by surname, firstname";
            if (company != null) {
                selectQuery = "select company_id, employee_id, surname, firstname from E_MASTER with (nolock) "
                        + " where active_yn='Y' and company_id=" + company.getCompanyID() + " order by surname, firstname";

            }
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);

            while (rs.next()) {
                Employee employee;
                if (company == null) {
                    employee = new Employee(new Company(rs.getInt("company_id")), rs.getInt("employee_id"));
                } else {
                    employee = new Employee(company, rs.getInt("employee_id"));
                }
                rslt.add(employee);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Setter for property employeeSelections.
     *
     * @param employeeSelections New value of property employeeSelections.
     */
    public void setEmployeeSelections(java.util.ArrayList employeeSelections) {
        FileContainer.employeeSelections = employeeSelections;
    }

    /**
     * Getter for property reportingStructures.
     *
     * @return
     */
    public java.util.ArrayList getReportingStructures() {
        return reportingStructures;
    }

    /**
     * Setter for property reportingStructures.
     *
     * @param reportingStructures New value of property reportingStructures.
     */
    public void setReportingStructures(java.util.ArrayList reportingStructures) {
        FileContainer.reportingStructures = reportingStructures;
    }

    /**
     * WebProcessDefinitions are the existing WebProcessDefinitions in use in
     * this installation
     *
     * @return
     */
    public java.util.ArrayList getWebProcessDefinitions() {
        return webProcessDefinitions;
    }

    /**
     * Returns the unique list of WebProcessDefinitions where the given user
     * (Employee) is a superior to one or more other employees. NOTE: If the
     * 'employee' is at the top-most level, this method will not return
     * anything. CR 5740
     *
     * @param employee
     * @return
     */
    public java.util.HashMap<Object, WebProcessDefinition> getWebProcessDefinitionsWithSubordinates(Employee employee) {
        java.util.HashMap result = new java.util.HashMap();
        for (WebProcessDefinition webProcessDefinition : webProcessDefinitions) {
            java.util.HashMap childEmployees = getSubordinates(employee, webProcessDefinition);
            if ((childEmployees != null) && (childEmployees.size() > 0)) {
                synchronized (result) {
                    if (!result.containsKey(webProcessDefinition.hashCode())) {
                        result.put(webProcessDefinition.hashCode(), webProcessDefinition);
                    }
                }
            }
        }
        return result;
    }

//    /**
//     * This routine places a date in table RE_DATE for report purposes from the
//     * Viewer section of the ESS
//     *
//     * @param fromDate
//     * @param toDate
//     * @param Employee_ID
//     * @param Report_Name
//     * @return TRUE-success
//     */
//    public boolean setLeaveReportAnalysisDate(String fromDate, String toDate, Integer Employee_ID, String Report_Name) {
//        // Is this a valid Web Administrator?
//        boolean rslt = true;
//        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
//        try {
//            String deleteQuery = "delete from RE_DATE where LOGON_ID = '" + Employee_ID + "';";
//            DatabaseObject.executeSQL(deleteQuery, con);
//
//            String insertQuery = "insert into RE_DATE values ('" + fromDate + "', '" + toDate + "', " + Employee_ID + ", '" + Report_Name + "')";
//            DatabaseObject.executeSQL(insertQuery, con);
//
//        } catch (java.sql.SQLException e) {
//            e.printStackTrace();
//            rslt = false;
//        } finally {
//            DatabaseObject.releaseConnection(con);
//        }
//        return rslt;
//    }
    public boolean isPayslipPdfCompatible(String companyID, String employeeID, String payrollID, int payrollDetailID, String endDate) {

        boolean rslt = false;
        // Connect to database
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();

            // Read entries from ESS_PROCESS_QUEUE
            String str
                    = "SELECT FIRST vw_CUSTOM_EmployeeVariables_AcrossTaxYears.PERIOD_ENDDATE "
                    + "FROM   (((((DBA.C_MASTER C_MASTER "
                    + "INNER JOIN DBA.RE_EMPLOYEE RE_EMPLOYEE "
                    + "ON C_MASTER.COMPANY_ID=RE_EMPLOYEE.COMPANY_ID) "
                    + "INNER JOIN DBA.vw_P_Report_Data_AcrossTaxYears vw_P_Report_Data_AcrossTaxYears "
                    + "ON (RE_EMPLOYEE.COMPANY_ID=vw_P_Report_Data_AcrossTaxYears.COMPANY_ID) "
                    + "AND (RE_EMPLOYEE.EMPLOYEE_ID=vw_P_Report_Data_AcrossTaxYears.EMPLOYEE_ID)) "
                    + "INNER JOIN DBA.E_MASTER E_MASTER "
                    + "ON (vw_P_Report_Data_AcrossTaxYears.COMPANY_ID=E_MASTER.COMPANY_ID) "
                    + "AND (vw_P_Report_Data_AcrossTaxYears.EMPLOYEE_ID=E_MASTER.EMPLOYEE_ID)) "
                    + "INNER JOIN DBA.E_NUMBER E_NUMBER "
                    + "ON (vw_P_Report_Data_AcrossTaxYears.COMPANY_ID=E_NUMBER.COMPANY_ID) "
                    + "AND (vw_P_Report_Data_AcrossTaxYears.EMPLOYEE_ID=E_NUMBER.EMPLOYEE_ID)) "
                    + "INNER JOIN DBA.vw_CUSTOM_EmployeeVariables_AcrossTaxYears vw_CUSTOM_EmployeeVariables_AcrossTaxYears "
                    + "ON vw_CUSTOM_EmployeeVariables_AcrossTaxYears.COMPANY_ID=vw_P_Report_Data_AcrossTaxYears.COMPANY_ID "
                    + "AND vw_CUSTOM_EmployeeVariables_AcrossTaxYears.EMPLOYEE_ID=vw_P_Report_Data_AcrossTaxYears.EMPLOYEE_ID "
                    + "AND vw_CUSTOM_EmployeeVariables_AcrossTaxYears.PAYROLL_ID=vw_P_Report_Data_AcrossTaxYears.PAYROLL_ID "
                    + "AND vw_CUSTOM_EmployeeVariables_AcrossTaxYears.PAYROLLDETAIL_ID=vw_P_Report_Data_AcrossTaxYears.PAYROLLDETAIL_ID "
                    + "and vw_CUSTOM_EmployeeVariables_AcrossTaxYears.period_enddate=vw_P_Report_Data_AcrossTaxYears.period_enddate) "
                    + "WHERE vw_P_Report_Data_AcrossTaxYears.COMPANY_ID=" + companyID
                    + " AND vw_P_Report_Data_AcrossTaxYears.EMPLOYEE_ID=" + employeeID
                    + " AND vw_P_Report_Data_AcrossTaxYears.PAYROLL_ID=" + payrollID
                    + " AND vw_P_Report_Data_AcrossTaxYears.PAYROLLDETAIL_ID=" + payrollDetailID
                    + " and vw_P_Report_Data_AcrossTaxYears.period_enddate='" + endDate + "'"
                    + " order by vw_CUSTOM_EmployeeVariables_AcrossTaxYears.VARIABLE_NAME";
            //System.out.print(str + "\n");
            ResultSet rs = DatabaseObject.openSQL(str, con);

            int dateCount = 0;
            while (rs.next()) {
                dateCount = rs.getDate("PERIOD_ENDDATE").toString().length();
            }
            System.out.println("dateCount: " + dateCount);
            rs.close();
            rslt = dateCount >= 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * This routine places a date in table RE_EMPLOYEE for report purposes from
     * the Viewer section of the ESSe calculation
     *
     * @param Company_ID
     * @param Employee_ID
     * @param Logon_ID
     * @param Report_Name
     * @return TRUE-success
     */
    public boolean setReportAnalysisDate(Integer Company_ID, Integer Employee_ID, Integer Logon_ID, String Report_Name) {

        boolean rslt = true;
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String deleteQuery = "delete from RE_EMPLOYEE where EMPLOYEE_ID = '" + Employee_ID + "' AND LOGON_ID = '" + Logon_ID + "';";
            DatabaseObject.executeSQL(deleteQuery, con);

            String insertQuery = "insert into RE_EMPLOYEE values (" + Company_ID + ", " + Employee_ID + ", " + Logon_ID + ", '" + Report_Name + "')";
            DatabaseObject.executeSQL(insertQuery, con);

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            rslt = false;
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    public LinkedList getChildEmployeeSelectionsForReporting(ReportingStructure repStruct, Employee user) {
        LinkedList<EmployeeSelectionRule> employeeSelectionRules = new LinkedList();
        ArrayList<EmployeeSelection> topNodes = repStruct.getTopNodes();

        for (EmployeeSelection topNode : topNodes) {
            LinkedList<EmployeeSelectionRule> employeeSelectionRulesAll = repStruct.getAllChildEmployeeSelections(topNode);
            // Only add child selections of the user
            for (EmployeeSelectionRule es : employeeSelectionRulesAll) {
                if (repStruct.isUserParentOfSelection(user, es)) {
                    employeeSelectionRules.add(es);
                }
            }
        }
        
        return employeeSelectionRules; 
    }

    /**
     * This routine calculates the tax for the given input
     *
     * @param GrossIncome
     * @param RetirementFundIncome
     * @param NonRetirementFundIncome
     * @param NoOfPayPeriodsInYear
     * @param TaxYearEndDate
     * @param AnnualBonus
     * @param EmployeeAge
     * @param TravelAllowance
     * @param OtherAllowance
     * @param Pension
     * @param Retirement
     * @param Provident
     * @param Income_Replacement
     * @param hasMedical
     * @param NumberOfDependants
     * @param PersonalContribution
     * @param CompanyContribution
     * @return ResultSet from DB
     */
    public ResultSet calculateTax(String GrossIncome, String RetirementFundIncome, String NonRetirementFundIncome, String AnnualBonus,
            String NoOfPayPeriodsInYear, String TaxYearEndDate, String EmployeeAge, String TravelAllowance,
            String OtherAllowance, String Pension, String PensionCompany, String Retirement, String RetirementCompany, String Provident, String ProvidentCompany, String Income_Replacement, int hasMedical, String NumberOfDependants,
            String PersonalContribution, String CompanyContribution) {

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        ResultSet rs = null;
        try {
            String getQuery = "CALL \"DBA\".\"sp_ESS_TaxCalc\"(@inGROSS_INCOME = " + GrossIncome + ", @inRETIREMENT_FUND_INCOME = " + RetirementFundIncome
                    + ", @inNON_RETIREMENT_FUND_INCOME = " + NonRetirementFundIncome + ", @inANNUAL_BONUS = " + AnnualBonus
                    + ", @inNO_OF_PAY_PERIODS_IN_YEAR = " + NoOfPayPeriodsInYear + ", @inTAX_YEAR_END_DATE = '" + TaxYearEndDate
                    + "', @inEmployee_Age = " + EmployeeAge + ", @inTravel_Allowance = " + TravelAllowance
                    + ", @inOther_Allowance = " + OtherAllowance + ", @inEePENSION = " + Pension + ", @inCoPENSION = " + PensionCompany + ", @inEeRETIREMENT = " + Retirement + ", @inCoRETIREMENT = " + RetirementCompany
                    + ", @inEePROVIDENT = " + Provident + ", @inCoPROVIDENT = " + ProvidentCompany + ", @inINCOME_REPLACEMENT = " + Income_Replacement + ", @inHasMedical = " + hasMedical
                    + ", @inNumberOfDependants = " + NumberOfDependants + ", @inPersonal_Contribution = " + PersonalContribution
                    + ", @inCompany_Contribution = " + CompanyContribution + ")";
            rs = DatabaseObject.openSQL(getQuery, con);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rs;
    }

    /* Returns TRUE is a Module with ID=10 and ModDetail with ID=1
     * is found in "s_moddetail"
     */
    public boolean isESSRegistered() {
        // Is this a valid Web Administrator?
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        int cnt = 0;
        try {
            cnt = DatabaseObject.getInt("select count(*) from s_moddetail with (nolock) where module_id=10 and moddetail_id=1", con);

        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return (cnt > 0);
    }

    /**
     * This function returns 'true' if the user belongs to the 'Web
     * Administrator' group in Peopleware
     *
     * @param employee
     * @return
     */
    public static synchronized boolean isEmployeeWebAdministrator(Employee employee) {
        if (webAdministrators.isEmpty()) {
            java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
            java.sql.ResultSet rslt;
            try {
                rslt = DatabaseObject.openSQL("select company_id, employee_id from al_emaster_agroup  with (NOLOCK) where group_id in "
                        + "(select group_id from a_group with (nolock) where trim(upper(description))='WEB ADMINISTRATOR')", con);
                while (rslt.next()) {
                    Employee admin = getEmployeeFromContainer(rslt.getInt(1), rslt.getInt(2));
                    webAdministrators.add(admin);
                }

            } catch (java.sql.SQLException e) {
                System.out.println("******************************************");
                System.out.println("******************************************");
                System.out.println("            WEB ADMIN DISCHARGED          ");
                System.out.println("******************************************");
                System.out.println("******************************************");
                return false;
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
        return webAdministrators.contains(employee);

    }

    /**
     * Is this employee in the top level of the reporting structure for the
     * current webProcessDefinition?
     *
     * @param employee
     * @param webProcessDefinition
     * @return
     */
    public boolean isEmployeeInTopLevel(Employee employee, WebProcessDefinition webProcessDefinition) {

        ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();

        if (reportingStructure != null) {
            ArrayList<EmployeeSelection> topNodes = reportingStructure.getTopNodes();
            for (EmployeeSelection selection : topNodes) {
                HashSet employees = SelectionInterpreter.getInstance().generateEmployeeList(selection, true);
                if (employees.contains(employee)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns a LinkedHashMap of all employees that reports directly or
     * indirectly to this employee in the given WebProcessDefinition. NOTE: If
     * the 'employee' is at the top-most level, this method will not return
     * anything. CR 5740
     *
     * @param employee
     * @param webProcessDefinition
     * @return
     */
    public HashMap<String, Employee> getSubordinates(Employee employee, WebProcessDefinition webProcessDefinition) {
        // The loading of subordinates are quite time-consuming.  It would be wise to cache the values if possible.
        if (cached_subordinates == null) {
            cached_subordinates = new java.util.HashMap<>();
        }

        // The key to the Hashtable is the employee+webProcessDefinition's hashcodes
        String key = employee.longHashCode() + " " + webProcessDefinition.hashCode();

        // Does an entry already exist in the hashtable?
        LinkedHashMap cachedChildEmployees = (LinkedHashMap) cached_subordinates.get(key);
        if (cachedChildEmployees != null) {
            return cachedChildEmployees;
        }

        // There were no entries in the cache, so we'll have to take the long road
        java.util.LinkedHashMap<String, Employee> childEmployees = new LinkedHashMap<>();
        boolean isEmpOnTop = isEmployeeInTopLevel(employee, webProcessDefinition);

        if (((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.WORKFLOW_IncludeTopLevelInManagement, true)) && isEmpOnTop)
                || (!isEmpOnTop)) {

            // Get the Reporting Structure
            ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
            if (reportingStructure == null) {
                return (new java.util.LinkedHashMap());
            }

            // List of all EmployeeSelections that contains this employee
            LinkedList<EmployeeSelection> localEmployeeSelections = reportingStructure.getAllEmployeeSelections(employee);

            for (EmployeeSelection selection : localEmployeeSelections) {
                LinkedList<Employee> employeesInSelection = reportingStructure.getAllChildEmployees(employee, selection);
                for (Employee subEmployee : employeesInSelection) {
                    String subKey = subEmployee.getCompany().getCompanyID() + "," + subEmployee.getEmployeeID();
                    synchronized (childEmployees) {
                        if (!childEmployees.containsKey(subKey)) {
                            childEmployees.put(subKey, subEmployee);
                        }
                    }
                }
            }
        }

        // add the result to the cache
        synchronized (cached_subordinates) {
            if (!cached_subordinates.containsKey(key)) {
                cached_subordinates.put(key, childEmployees);
            }
        }
        return childEmployees;
    }

    /**
     * Returns the instance of WebProcessDefinition that maps the
     * processClassName to the given classImplementationName
     *
     * @param classImplementationName
     * @return
     */
    public WebProcessDefinition getWebProcessDefinition(String classImplementationName) {
        //System.out.println("ProcessDefinition Size:"+getWebProcessDefinitions().size());
        for (WebProcessDefinition webProcDef : webProcessDefinitions) {
            if (webProcDef.getProcessClassName().compareToIgnoreCase(classImplementationName) == 0) {
                return (webProcDef);
            }
        }
        return null;
    }

    /**
     * Setter for property webProcessDefinitions.
     *
     * @param webProcessDefinitions New value of property webProcessDefinitions.
     */
    public void setWebProcessDefinitions(java.util.ArrayList webProcessDefinitions) {
        FileContainer.webProcessDefinitions = webProcessDefinitions;
    }

    /**
     * Setter for property webProcessDefinitionProperties.
     *
     * @param webProcessDefinitionList
     */
    public void setWebProcessDefinitionList(java.util.ArrayList webProcessDefinitionList) {
        FileContainer.webProcessDefinitionList = webProcessDefinitionList;
    }

    /**
     * Getter for property webProcesses.
     *
     * @return
     */
    public java.util.ArrayList getWebProcesses() {
        return webProcesses;
    }

    /**
     * Returns the WebProcess with the given hashCode
     *
     * @param hashCode
     * @return
     */
    public WebProcess getWebProcess(String hashCode) {
        if (!isBusyCreatingFileContainer) {
            for (WebProcess aProcess : webProcesses) {
                if (aProcess.hashCode() == new Integer(hashCode).intValue()) {
                    return aProcess;
                }
            }
        }
        return null;
    }

    /**
     * Returns the ReportingStructure with the given hashCode
     *
     * @param hashCode
     * @return
     */
    public ReportingStructure getReportingStructure(String hashCode) {
        for (ReportingStructure aStructure : reportingStructures) {
            if (aStructure.hashCode() == new Integer(hashCode).intValue()) {
                return (aStructure);
            }
        }
        return null;
    }

    /**
     * Returns the WebProcesses currently owned by the given employee, that are
     * still active.
     *
     * @param owner Employee currently assigned to handle these processes.
     * @return java.util.TreeMap The result is in a TreeMap so that we can sort
     * the results by their age.
     */
    public synchronized java.util.TreeMap<Object, WebProcess> getWebProcessesForActiveList(Employee owner) {
        TreeMap<Object, WebProcess> result = new TreeMap<>();
        Collection<WebProcess> c = Collections.synchronizedCollection(webProcesses);

        if (!isBusyCreatingFileContainer) {
            synchronized (c) {
                for (WebProcess aProc : c) {
                    //System.out.println(aProc.hashCode() + "," + aProc.toString() + ",current owner=" + aProc.getCurrentStage().getOwner());
                    if ((aProc.getCurrentStage().getOwner().equals(owner)) && (aProc.isActive())) {
                        synchronized (result) {
                            Object key = aProc.getCurrentStage().getCreationDate();
                            if (!result.containsKey(key)) {
                                result.put(key, aProc);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the WebProcesses that were assigned to the given employee at one
     * point in time, or those that are presently assigned to this person, but
     * already closed.
     *
     * @param user Employee that were at one time assigned to handle these
     * processes.
     * @return java.util.TreeMap The result is in a TreeMap so that we can sort
     * the results by their age.
     */
    public java.util.TreeMap getWebProcessesForHistoricalList(Employee user) {
        TreeMap result = new TreeMap();
        if (!isBusyCreatingFileContainer) {
            for (WebProcess webProcess : webProcesses) {
                // Is this user the current owner and the process is already closed?
                if ((webProcess.getCurrentStage().getOwner().equals(user)) && !(webProcess.isActive())) {
                    synchronized (result) {
                        Object key = webProcess.getCreationDate();
                        if (!result.containsKey(key)) {
                            result.put(key, webProcess);
                        }
                    }
                } else {
                    // Did user at one point in time own this process?
                    WebProcessHistory webProcessHistory = (WebProcessHistory) webProcess.getProcessHistory();
                    for (int j = 0; j < webProcessHistory.getPreviousProcessOwners().size(); j++) {
                        // Did the 'user' handled this process at some stage?
                        if (webProcessHistory.getPreviousProcessOwners().contains(user)) {
                            synchronized (result) {
                                Object key = webProcess.getCreationDate();
                                if (!result.containsKey(key)) {
                                    result.put(key, webProcess);
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the WebProcesses originally logged by the given employee.
     *
     * @param employee
     * @param onlyActiveProcesses
     * @return java.util.TreeMap The result is in a TreeMap so that we can sort
     * the results by their age.
     */
    public synchronized java.util.TreeMap<Object, WebProcess> getWebProcessesInitiatedBy(Employee employee, boolean onlyActiveProcesses) {
        TreeMap<Object, WebProcess> result = new TreeMap<>();
        Collection<WebProcess> c = Collections.synchronizedCollection(webProcesses);
        if (!isBusyCreatingFileContainer) {
            synchronized (c) {
                for (WebProcess aProcess : c) {
                    if ((onlyActiveProcesses) && (!aProcess.isActive())) {
                        continue;
                    }
                    if (aProcess.getLogger().equals(employee)) {
                        synchronized (result) {
                            Object key = new Long(Long.MAX_VALUE - aProcess.getCreationDate().getTime());
                            if (!result.containsKey(key)) {
                                result.put(key, aProcess);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the WebProcesses that affects the given employee.
     *
     * @param employee
     * @param onlyActiveProcesses
     * @return java.util.TreeMap The result is in a TreeMap so that we can sort
     * the results by their age.
     */
    public synchronized java.util.TreeMap<Object, WebProcess> getWebProcessesAffecting(Employee employee, boolean onlyActiveProcesses) {
        TreeMap<Object, WebProcess> result = new TreeMap<>();
        Collection<WebProcess> c = Collections.synchronizedCollection(webProcesses);
        if (!isBusyCreatingFileContainer) {
            synchronized (c) {
                for (WebProcess aProcess : c) {
                    if ((onlyActiveProcesses) && (!aProcess.isActive())) {
                        continue;
                    }
                    if (aProcess.getEmployee().equals(employee)) {
                        synchronized (result) {
                            Object key = new Long(Long.MAX_VALUE - aProcess.getCreationDate().getTime());
                            if (!result.containsKey(key)) {
                                result.put(key, aProcess);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Setter for property webProcesses.
     *
     * @param webProcesses New value of property webProcesses.
     */
    public void setWebProcesses(java.util.ArrayList webProcesses) {
        FileContainer.webProcesses = webProcesses;
    }

    /**
     * Returns an enumerated value for the Operating System currently installed
     * OS_WINDOWS = 0 OS_LINUX = 1
     *
     * @return
     */
    public static int getOS() {
        if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") >= 0) {
            return OS_WINDOWS;
        } else {
            return OS_LINUX;
        }
    }

    /**
     * Returns the Operating System dependent File Seperator OS_WINDOWS = "\\"
     * OS_LINUX = "/"
     *
     * @return
     */
    public static String getOSFileSeperator() {
        if (getOS() == OS_WINDOWS) {
            return "\\";
        } else {
            return "/";
        }
    }

    private static void closeDerbyConnection(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException e) {
        }
    }

    private static Connection getDerbyConnection() {
        String connectionURL = "jdbc:derby:WebModuleDB;create=true;user=DBA;password=WebModulePwd";
        Connection con = null;
        try {
            try {
                // Create database
                Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
                con = DriverManager.getConnection(connectionURL);

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(RunTests.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

        } catch (InstantiationException | IllegalAccessException | SQLException ex) {
            Logger.getLogger(RunTests.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    /**
     * Saves the mappings of Web Processes, Employee Selections, Reporting
     * Structures to files in the given directory.
     */
    public void saveWebSetupDetail() {
        saveWebSetupDetail(getWorkingDirectory());
    }

    /**
     * Default working directory as defined in the System class
     *
     * @return
     */
    public static String getWorkingDirectory() {
        return (System.getProperty("user.dir"));
    }

    public static void reloadHolidayInfoFromDB() {
        ArrayList linkedList = new ArrayList();
        // Step through employees, creating all Leave Information
        System.out.println("");
        LinkedList employees = getEmployees();

        int j = 0;
        int len = employees.size();

        for (Employee employee : getEmployees()) {
            if (j % 500 == 0) {
                System.out.println("\tHoliday Info [" + j + "/" + len + "]");
            }
            j++;
            HolidayInfo localHolidayInfo = new HolidayInfo(employee);
            linkedList.add(localHolidayInfo);
        }
        setHolidayInfo(linkedList);
        System.out.println("\tdone.");
    }

    /**
     * Creates instance for each payroll/period combination that had been used
     * in p_calc_detail
     */
    public static void reloadPayrollPeriodsFromDB() {
        java.sql.Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ArrayList<PayrollDetail> linkedList = new ArrayList<>();

            // Get Periods and RunTypes
            java.sql.ResultSet rs = DatabaseObject.openSQL("select distinct PAYROLL_ID, PAYROLLDETAIL_ID from "
                    + " p_calc_detail with (nolock)", con);
            while (rs.next()) {
                PayrollDetail payrollDetail = new PayrollDetail(new Payroll(new Integer(rs.getInt("PAYROLL_ID"))), new Integer(rs.getInt("PAYROLLDETAIL_ID")));
                linkedList.add(payrollDetail);
            }
            rs.close();
            setPayrollPeriods(linkedList);

        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    private static int getEmployeesToLoad() {
        int result = 0;
        java.sql.Connection con = null;
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));
        try {
            // Start by finding ou thow many employees should be loaded

            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder sqlStatement = new StringBuilder();
            // We are only interested in employees that are currently engaged or that has
            // been discharged in the last 3 months
            sqlStatement.append("select count(*) from e_master e  with (NOLOCK) where ");
            sqlStatement.append("datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3 ");
            sqlStatement.append("or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null");

            result = DatabaseObject.getInt(sqlStatement.toString(), con);
        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    public static void reloadEmployeesFromDB() {
        int counter = 0;
        int company_id;
        int employee_id;
        LinkedList company_employee = new LinkedList();
        // Clear Login/Pwd mapping
        synchronized (employeeLoginPwdMap) {
            employeeLoginPwdMap.clear();
        }

        java.sql.Connection con = null;
        DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));
        try {
            // Start by finding ou thow many employees should be loaded
            int empsToLoad = getEmployeesToLoad();
            //System.out.println("Initiating loading of " + empsToLoad + " employees...");
            // Load all employees
            con = DatabaseObject.getNewConnectionFromPool();
            StringBuilder sqlStatement = new StringBuilder();
            // We are only interested in employees that are currently engaged or that has
            // been discharged in the last 3 months
            sqlStatement.append("select COMPANY_ID, EMPLOYEE_ID from e_master e with (nolock) where ");
            sqlStatement.append("datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3 ");
            sqlStatement.append("or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null order by surname, firstname ");
            java.sql.ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            while (rs.next()) {
                counter++;
                if ((counter % 500) == 0) {
                    System.out.println("          Fast Load " + counter + " of " + empsToLoad + " employees...");
                }
                company_id = rs.getInt("COMPANY_ID");
                employee_id = rs.getInt("EMPLOYEE_ID");

                company_employee.add(company_id + "," + employee_id);
            }
            rs.close();
        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }

        int j = 0;
        int sz = company_employee.size();

        for (Iterator i = company_employee.iterator(); i.hasNext();) {
            if ((j % 200) == 0) {
                System.out.println("          Detailed Load " + j + " of " + sz + " employees...");
            }
            j++;
            String entry = (String) i.next();

            int pos = entry.indexOf(',');
            Integer companyID = new Integer(entry.substring(0, pos));
            Integer employeeID = new Integer(entry.substring(pos + 1, entry.length()));

            Company company = new Company(companyID);

            // if the Employee is already in the container, reload his/her information.
            // if not, add him/her to the list
            Employee employee;
            employee = getEmployeeFromContainer(companyID, employeeID);
            if (employee != null) {
                employee.loadFromDB();
            } else {
                employee = new Employee(company, employeeID);
                synchronized (employeeMap) {
                    String key = companyID + "," + employeeID;
                    if (!employeeMap.containsKey(key)) {
                        employeeMap.put(key, employee);
                    }
                }
            }

            // Now that we've loaded the employee, rebuild the login/password map too
            synchronized (employeeLoginPwdMap) {
                Object loginKey;
                loginKey = employee.getWeb_login().concat("||").concat(employee.getWeb_pass()).trim().toUpperCase();
                if (!employeeLoginPwdMap.containsKey(loginKey)) {
                    employeeLoginPwdMap.put(loginKey, companyID + "," + employeeID);
                }
            }
        }

    }

    /**
     * When an employee's password is changed through the ESS, one needs to
     * update the EmployeeLoginPwdMap accordingly
     *
     * @param employee
     * @param oldPassword
     * @param newPassword
     */
    public void updateLoginPwdMapping(Employee employee, String oldPassword, String newPassword) {
        synchronized (employeeLoginPwdMap) {
            Object oldLoginKey = employee.getWeb_login().concat("||").concat(oldPassword).trim().toUpperCase();
            Object newLoginKey = employee.getWeb_login().concat("||").concat(newPassword).trim().toUpperCase();
            // Remove old mapping
            if (employeeLoginPwdMap.containsKey(oldLoginKey)) {
                employeeLoginPwdMap.remove(oldLoginKey);
            }
            // Add new mapping
            if (!employeeLoginPwdMap.containsKey(newLoginKey)) {
                employeeLoginPwdMap.put(newLoginKey, employee.getCompany().getCompanyID() + "," + employee.getEmployeeID());
            }
        }
    }

    /**
     * Saves the mappings of Web Processes, Employee Selections, Reporting
     * Structures to files in the given directory.
     *
     * @param workingDirectory Where should this file be stored?
     */
    public void saveWebSetupDetail(String workingDirectory) {
        System.out.print("Saving web setup detail to:" + workingDirectory);
        // Delete the existing files first:
        getEmployeeSelectionsFile(workingDirectory).delete();
        getReportingStructuresFile(workingDirectory).delete();
        getWebProcessDefinitionsFile(workingDirectory).delete();

        saveLinkedList(new LinkedList(employeeSelections), getEmployeeSelectionsFile(workingDirectory));
        saveLinkedList(new LinkedList(reportingStructures), getReportingStructuresFile(workingDirectory));
        saveLinkedList(new LinkedList(webProcessDefinitions), getWebProcessDefinitionsFile(workingDirectory));
        System.out.println("  done.");
    }

    private static synchronized ArrayList extractProcessesByStatus(ArrayList<WebProcess> webProcesses, String status) {
        ArrayList result = new ArrayList();
        for (WebProcess process : webProcesses) {
            if (process.getStatus().compareToIgnoreCase(status) == 0) {
                result.add(process);
            }
        }
        return result;
    }

    /**
     * Is the File Container in the process of loading the saved cache back into
     * memory?
     *
     * @return
     */
    public boolean isLoadingDatabaseCache() {
        return FileContainer.isLoadingCache;
    }

    /**
     * Is the File Container in the process of loading the cache from the
     * database?
     *
     * @return
     */
    public static boolean isCachingFromDB() {
        return FileContainer.isCachingFromDB;
    }

    public int getCachePercentage() {
        return FileContainer.cachePercentage;
    }

    /**
     * Tells the File Container that caching from the database is in progress
     */
    private static void setCachingFromDB(boolean isCaching) {
        FileContainer.isCachingFromDB = isCaching;
    }

    /**
     * Loads Employees, Payrolls, etc from file
     */
    public static void loadDatabaseCache() {
        java.util.Date now = new java.util.Date();
        if (!isCachingFromDB()) {
            // Do not reload within 10 minutes after the previous reload
            if ((databaseCacheReloadTime == null)) {
                databaseCacheReloadTime = new java.util.Date();
            }
            databaseCacheReloadTime.setTime(now.getTime());
            setCachingFromDB(true);

            cachePercentage = 20;
            DatabaseObject.setConnectInfo_AccsysJDBC((WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "")));

            System.gc();  // Force Garbage Collection

            // Load new list of employees and leaveInfo
            cachePercentage = 40;
            System.out.println("\tFileContainer: loading employees...");
            reloadEmployeesFromDB();

            cachePercentage = 70;
            System.out.println("\tFileContainer: loading holiday detail...");
            reloadHolidayInfoFromDB();

            cachePercentage = 80;
            System.out.println("\tFileContainer: loading payroll periods...");
            reloadPayrollPeriodsFromDB();

            cachePercentage = 90;
            System.out.println("\tdone.");
            setCachingFromDB(false);

            // Clear cached structures
            cached_subordinates = null;
        }
    }

    private static ArrayList getProcessDetailFromFile() {
        return getProcessDetailFromFile(getWorkingDirectory());
    }

    /**
     * Loads Reporting Structures, Employee Selections, Web Processes from file
     */
    private static ArrayList getProcessDetailFromFile(String workingDirectory) {
        ArrayList result = new ArrayList();
        java.util.Date now = new java.util.Date();

        if (!isCachingFromDB()) {
            // Do not reload within 10 minutes after the previous reload
            if ((databaseCacheReloadTime == null)) {
                databaseCacheReloadTime = new java.util.Date();
            }
            databaseCacheReloadTime.setTime(now.getTime());
            setCachingFromDB(true);

            // if we did not load the configuration files, do it first
            if (employeeSelections == null) {
                loadESSConfiguration(workingDirectory);
            }

            System.out.println(new java.util.Date() + "Loading process detail from:" + workingDirectory);

            // Active Web Processes
            System.out.println("\n\n\n********************************************************\nLoading Active Processes...");
            result = new ArrayList(loadLinkedList(getCurrentProcessesFile(workingDirectory)));
            System.out.println("\nActive Processes size:" + result.size());
            result.addAll(loadLinkedList(getClosedProcessesFile(workingDirectory)));
            System.out.println("\n\n\n********************************************************\nLoading Closed Processes...");
            System.out.println("\nTotal Processes size:" + result.size());

            System.out.println("   done.");
            setCachingFromDB(false);
        }
        return result;
    }

    /**
     * Loads Reporting Structures, Employee Selections, Web Processes from file
     */
    private static void loadESSConfiguration(String workingDirectory) {
        System.out.println("Loading ESS configurations:" + workingDirectory + "\n");

        // Rules that group employees together
        employeeSelections = new ArrayList(loadLinkedList(getEmployeeSelectionsFile(workingDirectory)));

        // Tree-structure of employee selections
        reportingStructures = new ArrayList(loadLinkedList(getReportingStructuresFile(workingDirectory)));

        // New Existing WebProcess Defintitions implemented by Accsys
        //FileContainer.getInstance().loadWebProcessDefinitionList();
        webProcessDefinitionList = new ArrayList(loadLinkedList(getWebProcessDefinitionListFile(workingDirectory)));

        // Configured Web Process Definitions
        webProcessDefinitions = new ArrayList(loadLinkedList(getWebProcessDefinitionsFile(workingDirectory)));

        refreshWebProcessDefinitions(webProcessDefinitionList, webProcessDefinitions);

    }

    /**
     * Clears and repopulates the internal reporting structure cache
     */
    public void rebuildSelectionInterpreterCache() {
        System.out.println("\n\nFileContainer: rebuildSelectionInterpreterCache...");
        // Internal cache for reporting structures
        // Reset the currently cached selections
        SelectionInterpreter interpreter = SelectionInterpreter.getInstance();
        interpreter.resetInternalCache();
        // Now, for each employee selection, generate the list so that the
        // interpreter's cache can have a copy of it
        for (EmployeeSelection selection : employeeSelections) {
            interpreter.generateEmployeeList(selection, false);
        }

        // While we're at it, clear
        // Hashtable cached_subordinates;
        // Cached Subordinates
        if (cached_subordinates != null) {
            cached_subordinates.clear();
        }
        System.out.println("done");

    }

    /**
     * We now need to update the webProcessDefinitions with any entries found in
     * WebProcessDefinitionList that are new. The reason for duplication is that
     * we want to send out a file containing all the implemented
     * WebProcessDefinitions without overwriting the stuff that has already been
     * implemented.
     */
    private static void refreshWebProcessDefinitions(ArrayList fromDefinitionFileList, ArrayList toLiveList) {
        for (int i = 0; i < fromDefinitionFileList.size(); i++) {
            // If the WebProcess in the Accsys definition file does not already exist
            // in the live list of defined Web Processes, add it
            if (!containsWebProcessDefinition(toLiveList, ((WebProcessDefinition) fromDefinitionFileList.get(i)).getName())) {
                toLiveList.add(fromDefinitionFileList.get(i));
            }
        }
    }

    private static boolean containsWebProcessDefinition(ArrayList<WebProcessDefinition> list, String name) {
        for (Iterator i = list.iterator(); i.hasNext();) {
            if ((((WebProcessDefinition) i.next()).getName().compareToIgnoreCase(name) == 0)) {
                return true;
            }
        }
        return false;

    }

    /**
     * Saves a java.util.ArrayList into a binary file.
     *
     * @param list
     * @param toFile
     */
    public synchronized void saveLinkedList(LinkedList list, File toFile) {
        try {
            if (list.size() > 0) {
                // Clears the current contents
                toFile.delete();
                System.out.println("\tsaveArrayList:" + toFile.getName() + "\t" + list.size() + " items.");

                FileOutputStream fos = new FileOutputStream(toFile);
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                ObjectOutputStream oos = new ObjectOutputStream(bos);

                // save LinkedList
                oos.writeInt(list.size());
                for (int i = 0; i < list.size(); i++) {
                    oos.writeObject(list.get(i));
                }

                oos.flush();
                // Close Streams
                oos.close();
                fos.close();
            } else {
                System.out.println("\tsaveLinkedList:" + toFile.getName() + "\t 0 items - not saved.");
            }

        } catch (FileNotFoundException fnfe) {
        } catch (IOException ioe) {
        }
    }

    public void clearAllProcessDefinitionsFromFile() {
        if (webProcessDefinitionListFile.exists()) {
            webProcessDefinitionListFile.delete();
        }
    }

    /**
     * Add new definitions into definition file
     *
     * @param def
     */
    public void addWebProcessDefinitionIntoFile(WebProcessDefinition def) {
        // New Existing WebProcess Defintitions implemented by Accsys
        LinkedList localWebProcessDefinitionList = loadLinkedList(getWebProcessDefinitionListFile(getWorkingDirectory()));
        if (!localWebProcessDefinitionList.contains(def)) {
            localWebProcessDefinitionList.add(def);
        }
        saveLinkedList(localWebProcessDefinitionList, getWebProcessDefinitionListFile(getWorkingDirectory()));

    }

    /**
     * Loads a java.util.ArrayList from a binary file.
     *
     * @param fromFile
     * @return
     */
    public static synchronized LinkedList loadLinkedList(File fromFile) {
        try {
            if ((fromFile.exists()) && (fromFile.isFile())) {
                FileInputStream fis = new FileInputStream(fromFile);
                BufferedInputStream bis = new BufferedInputStream(fis);
                ObjectInputStream ois = new ObjectInputStream(bis);

                System.out.println("loading from ..." + fromFile);
                // Load each TestInstallationMapping
                LinkedList result = new LinkedList();
                int numberOfItems = ois.readInt();

                for (int i = 0; i < numberOfItems; i++) {
                    Object object = ois.readObject();
                    result.add(object);
                }

                // Close Streams
                ois.close();
                fis.close();

                return result;
            }
            return (new LinkedList());
        } catch (FileNotFoundException fnfe) {
        } catch (EOFException eofe) {
        } catch (IOException | ClassNotFoundException ioe) {
        }
        return (new LinkedList());
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * EmployeeSelection
     */
    private static File getEmployeeSelectionsFile(String workingDirectory) {
        if ((employeeSelectionsFile == null) || (!employeeSelectionsFile.exists())) {
            employeeSelectionsFile = new File(workingDirectory + getOSFileSeperator() + employeeSelectionsFileName);
            employeeSelectionsFile.mkdirs();
        }
        return employeeSelectionsFile;
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * ReportingStructure
     */
    private static File getReportingStructuresFile(String workingDirectory) {
        if ((reportingStructuresFile == null) || (!reportingStructuresFile.exists())) {
            reportingStructuresFile = new File(workingDirectory + getOSFileSeperator() + reportingStructuresFileName);
            reportingStructuresFile.mkdirs();
        }
        return reportingStructuresFile;
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * ProcessDefinition
     */
    private static File getWebProcessDefinitionsFile(String workingDirectory) {
        if ((webProcessDefinitionsFile == null) || (!webProcessDefinitionsFile.exists())) {
            webProcessDefinitionsFile = new File(workingDirectory + getOSFileSeperator() + webProcessDefinitionsFileName);
            webProcessDefinitionsFile.mkdirs();
        }
        return webProcessDefinitionsFile;
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * Process
     *
     * @param workingDirectory
     * @return
     */
    public static File getCurrentProcessesFile(String workingDirectory) {
        if ((currentProcessesFile == null) || (!currentProcessesFile.exists())) {
            currentProcessesFile = new File(workingDirectory + getOSFileSeperator() + currentProcessesFileName);
            currentProcessesFile.mkdirs();
        }
        return currentProcessesFile;
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * Process
     */
    private static File getClosedProcessesFile(String workingDirectory) {
        if ((closedProcessesFile == null) || (!closedProcessesFile.exists())) {
            //System.out.println(closedProcessesFileName + " does not exists.  Creating a new file...");
            closedProcessesFile = new File(workingDirectory + getOSFileSeperator() + closedProcessesFileName);
            try {
                closedProcessesFile.createNewFile();
            } catch (IOException e) {
            }
        }
        return closedProcessesFile;
    }

    /**
     * [Creates] and returns the binary file that contains the stored list of
     * ProcessDefinitionList
     */
    private static File getWebProcessDefinitionListFile(String workingDirectory) {
        if ((webProcessDefinitionListFile == null) || (!webProcessDefinitionListFile.exists())) {
            webProcessDefinitionListFile = new File(workingDirectory + getOSFileSeperator() + webProcessDefinitionListFileName);
            webProcessDefinitionListFile.mkdirs();
        }
        return webProcessDefinitionListFile;
    }

    public void loadWebProcessDefinitionList() {
        WebProcessDefinition def1 = new WebProcessDefinition("Leave Requests", "Request Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
        WebProcessDefinition def2 = new WebProcessDefinition("Update Family Information", "Update Family Information", "za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition");
        WebProcessDefinition def3 = new WebProcessDefinition("Update Employee Contact Information", "Update Employee Contact Information", "za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact");
        WebProcessDefinition def4 = new WebProcessDefinition("Cancel Leave", "Cancel Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation");

        WebProcessDefinition def5 = new WebProcessDefinition("Variable Changes (55001)", "Variable Changes (55001)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001");
        WebProcessDefinition def6 = new WebProcessDefinition("Variable Changes (55002)", "Variable Changes (55002)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002");
        WebProcessDefinition def7 = new WebProcessDefinition("Variable Changes (55003)", "Variable Changes (55003)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003");
        WebProcessDefinition def8 = new WebProcessDefinition("Variable Changes (55004)", "Variable Changes (55004)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004");
        WebProcessDefinition def9 = new WebProcessDefinition("Variable Changes (55005)", "Variable Changes (55005)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005");

        WebProcessDefinition def10 = new WebProcessDefinition("Clocking Authorisation", "Clocking Authorisation", "za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
        WebProcessDefinition def11 = new WebProcessDefinition("Training Requests", "Training Requests", "za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition");
        WebProcessDefinition def12 = new WebProcessDefinition("TnA Roster Requests", "TnA Roster Requests", "za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition");

        FileContainer fc = FileContainer.getInstance();
        fc.clearAllProcessDefinitionsFromFile();
        fc.addWebProcessDefinitionIntoFile(def1);
        fc.addWebProcessDefinitionIntoFile(def2);
        fc.addWebProcessDefinitionIntoFile(def3);
        fc.addWebProcessDefinitionIntoFile(def4);
        fc.addWebProcessDefinitionIntoFile(def5);
        fc.addWebProcessDefinitionIntoFile(def6);
        fc.addWebProcessDefinitionIntoFile(def7);
        fc.addWebProcessDefinitionIntoFile(def8);
        fc.addWebProcessDefinitionIntoFile(def9);
        fc.addWebProcessDefinitionIntoFile(def10);
        fc.addWebProcessDefinitionIntoFile(def11);
        fc.addWebProcessDefinitionIntoFile(def12);

        ////////////////////////////////////////////////////////////////////////
        // Add leave process for each leave profile
        ////////////////////////////////////////////////////////////////////////
        ArrayList<String> leaveProfiles = fc.getLeaveProfileList();
        for (String profile : leaveProfiles) {
            //System.out.println("Leave Profile = " + profile);
            WebProcessDefinition def = new WebProcessDefinition("Leave Requests_" + profile, "Request Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition" + profile);
            fc.addWebProcessDefinitionIntoFile(def);
        }

        fc.saveWebSetupDetail();
    }

    /**
     * Getter for property employees.
     *
     * @return
     */
    public static java.util.LinkedList<Employee> getEmployees() {
        LinkedList<Employee> result = new LinkedList<>(employeeMap.values());
//        return employees;
        return result;
    }

    /**
     * Returns an Employee object contained in the FileContainer's LinkedLists
     *
     * @param company_id
     * @param employee_id
     * @return
     */
    public synchronized static Employee getEmployeeFromContainer(Integer company_id, Integer employee_id) {
        return (Employee) employeeMap.get(company_id + "," + employee_id);
    }

    /**
     * Returns an Employee object contained in the FileContainer's LinkedLists
     *
     * @param hashCode
     * @return
     */
    public synchronized static Employee getEmployeeFromContainer(String hashCode) {
        LinkedList<Employee> employees = getEmployees();
        for (Employee employee : employees) {
            if (hashCode.compareToIgnoreCase(new Long(employee.longHashCode()).toString()) == 0) {
                return employee;
            }
        }
        return null;
    }

    /**
     * Returns an Employee object contained in the FileContainer's LinkedLists
     *
     * @param loginName
     * @param password
     * @return
     */
    public synchronized static Employee getEmployeeFromContainer(String loginName, String password) {
        // We will use the ConcurrentHashMap class (employeeLoginPwdMap) to lookup the employee's company- / employee_ids
        Object loginKey = loginName.concat("||").concat(password).trim().toUpperCase();
        String loginEmployeeKey = (String) employeeLoginPwdMap.get(loginKey);
        Employee employee = (Employee) employeeMap.get(loginEmployeeKey);
        return employee;
    }

    /**
     * Setter for property employees.
     *
     * @param employees New value of property employees.
     *
     */
    public void setEmployees(java.util.LinkedHashMap employees) {
        FileContainer.employeeMap = employees;
    }

    public java.util.ArrayList getHolidayInfo() {
        return holidayInfo;
    }

    /**
     * Returns a LinkedList with SDTrainingRecord objects for this employee
     *
     * @param employee
     * @return
     */
    public LinkedList getTrainingHistoryFromContainer(Employee employee) {
        return (new TRHistoricalEventList(employee).getList());
    }

    /**
     * Returns a LinkedList with SD
     *
     * /** Returns a HolidayInfo object contained in the FileContainer's
     * LinkedLists
     *
     * @param employee
     * @return
     */
    public HolidayInfo getHolidayInfoFromContainer(Employee employee) {
        for (HolidayInfo holidayInfoInstance : holidayInfo) {
            if (holidayInfoInstance.getEmployee().equals(employee)) {
                return holidayInfoInstance;
            }
        }
        return null;
    }

    public static void setHolidayInfo(java.util.ArrayList holidayInfo) {
        FileContainer.holidayInfo = holidayInfo;
    }

    /**
     * For Tax Certificates, retrieve the list of years for which it was
     * generated for this employee The format of the result is yyyy/mm i.e.
     * '2010/08'
     *
     * @param employee
     * @return
     */
    public java.util.LinkedList getTaxCertificateYears(Employee employee) {
        LinkedList rslt = new LinkedList();

        // Get List of records in vw_P_Employee_TaxCertificate
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        if (con == null) {
            System.out.println("Unable to get connection from connection pool. 'getTaxCertificateYears' not returning any results. ");
        } else {
            try {
                if ((employee.getPayroll() != null) && (employee.getPayroll().getPayrollID() != null)) {
                    java.sql.ResultSet rs = DatabaseObject.openSQL(
                            "select a.company_ID, a.employee_ID, a.payroll_ID , a.transaction_year, max(a.submissionperiod) as SubmissionPeriod"
                            + " from (SELECT  distinct vwe.company_id, vwe.employee_id, vwe.payroll_id, vwc.TRANSACTION_YEAR, left(\"2031\",4)||'/'||right(\"2031\",2)"
                            + " as SubmissionPeriod FROM ((((((C_MASTER C with (nolock) INNER JOIN vw_P_Company_TaxCertificate vwc with (nolock) ON C.COMPANY_ID=vwc.COMPANY_ID)"
                            + " INNER JOIN vw_P_Employee_TaxCertificate vwe with (nolock) ON (vwc.RUN_ID=vwe.RUN_ID) AND (vwc.PAYROLL_ID=vwe.PAYROLL_ID))"
                            + " INNER JOIN E_MASTER E_MASTER with (nolock) ON (vwe.COMPANY_ID=E_MASTER.COMPANY_ID) AND (vwe.EMPLOYEE_ID=E_MASTER.EMPLOYEE_ID))"
                            + " INNER JOIN P_PAYROLL P_PAYROLL with (nolock) ON vwe.PAYROLL_ID=P_PAYROLL.PAYROLL_ID)))"
                            + " where vwe.company_id=" + employee.getCompany().getCompanyID()
                            + " and vwe.employee_id=" + employee.getEmployeeID()
                            + " and vwe.payroll_id=" + employee.getPayroll().getPayrollID()
                            + " ) as a group by a.company_id, a.employee_id, a.payroll_id, a.TRANSACTION_YEAR  order by SubmissionPeriod desc", con);
                    while (rs.next()) {
                        rslt.add(rs.getString("SubmissionPeriod"));
                    }
                }
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
        return rslt;
    }

    /**
     * For payslips from previous Tax Years, it is important to retreive a list
     * of years for which those payslips are available. Returns a LinkedList
     * with Strings containing the TAX_YEAR_START/TAX_YEAR_END date found in the
     * archive tables for this employee The format of the result is yyyy/yyyy
     * i.e. '2004/2005'
     *
     * @param employee
     * @return
     */
    public java.util.LinkedList getArchivedPayslipTaxYears(Employee employee) {
        return employee.getArchivedPayslipTaxYears();
    }

    /**
     * For the current Tax Year, retrieve the From/To dates. Returns a
     * LinkedList with Strings containing the TAX_YEAR_START/TAX_YEAR_END date
     * found in the archive tables for this employee The format of the result is
     * yyyy/yyyy i.e. '2004/2005'
     *
     * @param employee
     * @return
     */
    public String getCurrentPayslipTaxYears(Employee employee) {
        String rslt = new String();
        // Get List of archived records in P_PAYLOG_OLD
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            if ((employee.getPayroll() != null) && (employee.getPayroll().getPayrollID() != null)) {
                rslt = DatabaseObject.getString(
                        "select datepart(yy,TAX_YEAR_START)||'/'||datepart(yy,TAX_YEAR_END) as PERIOD from p_payroll with (nolock) "
                        + " where payroll_id=" + employee.getPayroll().getPayrollID(), con);
            }
        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Any exceptions can be sent to this file
     *
     * @param e
     */
    public void writeExceptionToLog(Exception e) {
        try {
            OutputFile outFile = new OutputFile(messageLogFileName);
            // Get Date and Time
            String header = DatabaseObject.getInstance().getCurrentDateAndTime();
            outFile.println("\n" + header);

            // Should we use full exception messages?
            outFile.println("*** " + e.getClass().toString() + " ***");
            for (StackTraceElement element : e.getStackTrace()) {
                outFile.println(element.toString());
            }
            outFile.close();
        } catch (IOException ie) {
        }

    }

    /**
     * Any exceptions/messages can be sent to this file
     *
     * @param s
     */
    public void writeMessageToLog(String s) {
        try {
            OutputFile outFile = new OutputFile(messageLogFileName);
            // Get Date and Time
            String header = DatabaseObject.getInstance().getCurrentDateAndTime();
            outFile.println("\n" + header);

            outFile.println(s);
            outFile.close();
        } catch (IOException ie) {
        }

    }

    /**
     * Getter for property payrollPeriods.
     *
     * @return
     */
    public java.util.ArrayList getPayrollPeriods() {
        return payrollPeriods;
    }

    /**
     * Setter for property payrollPeriods.
     *
     * @param payrollPeriods New value of property payrollPeriods.
     *
     */
    public static void setPayrollPeriods(java.util.ArrayList payrollPeriods) {
        FileContainer.payrollPeriods = payrollPeriods;
    }

    /**
     * Sometimes we need to force a re-load of leave information
     *
     * @param employee
     */
    public void resetLeaveInfoForEmployee(Employee employee) {
        leaveInfoList.remove(employee.hashCode());
    }

    public LeaveInfo getLeaveInfoFromContainer(Employee employee) {
        LeaveInfo leaveInfo;
        leaveInfo = (LeaveInfo) leaveInfoList.get(employee.hashCode());

        if (leaveInfo == null) {
            leaveInfo = new LeaveInfo(employee);
            synchronized (leaveInfoList) {
                leaveInfoList.put(employee.hashCode(), leaveInfo);
            }
            return leaveInfo;
        }

        // Is this instance not too old?
        if (leaveInfo.getInstanceAge() > 60) {
            synchronized (leaveInfoList) {
                if (leaveInfoList.containsKey(employee.hashCode())) {
                    leaveInfoList.remove(employee.hashCode());
                    leaveInfo = new LeaveInfo(employee);
                    leaveInfoList.put(employee.hashCode(), leaveInfo);
                }
            }
        }

        return leaveInfo;
    }

    public ShiftRosterInfo getShiftRosterInfoFromContainer(Employee employee) {
        return new ShiftRosterInfo(employee);
    }

    /**
     * Returns a linked list (String) with each item in the User Defined Fields
     *
     * @param userDefinedFieldName
     * @return LinkedList (String)
     */
    public LinkedList getUserDefinedFieldItems(String userDefinedFieldName) {
        LinkedList rslt = new LinkedList();
        // Get List of archived records in P_PAYLOG_OLD
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            // How many items in the list?
            int itemCount = DatabaseObject.getInt(" select fn_E_GetUserDefinedFieldListItemCount(gen_id) from e_gendef with (nolock) where trim(upper(name))='" + userDefinedFieldName + "';", con);
            for (int i = 0; i < itemCount; i++) {
                String item = DatabaseObject.getString(" select fn_E_GetUserDefinedFieldListItem(gen_id," + (i + 1) + ") from e_gendef with (nolock) where trim(upper(name))='" + userDefinedFieldName + "';", con);
                rslt.add(item);
            }

        } catch (java.sql.SQLException e) {
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    /**
     * Returns a PayrollDetail object contained in the FileContainer's
     * LinkedLists
     *
     * @param payroll
     * @param payrolldetail_id
     * @return
     */
    public PayrollDetail getPayrollDetailFromContainer(Payroll payroll, int payrolldetail_id) {
        for (int i = 0; i < payrollPeriods.size(); i++) {
            PayrollDetail payrollDetail = (PayrollDetail) payrollPeriods.get(i);
            if ((payrollDetail.getPayroll().equals(payroll))) {
                if (payrollDetail.getPayrolldetailID().intValue() == payrolldetail_id) {
                    return payrollDetail;
                }
            }
        }
        return null;
    }

    // For period ranges
    public PayrollDetail getPayrollDetailFromContainer(Payroll payroll, String periodRange) {
        for (int i = 0; i < payrollPeriods.size(); i++) {
            PayrollDetail payrollDetail = (PayrollDetail) payrollPeriods.get(i);
            if ((payrollDetail.getPayroll().equals(payroll))
                    && (payrollDetail.getPeriodRange().compareTo(periodRange) == 0)) {
                return payrollDetail;
            }
        }
        return null;
    }

    /// --------------------------------------------------------------------------------------
    /// ------------------------ MAINTENANCE - Moving processes from files to a Derby DB
    /// --------------------------------------------------------------------------------------
    /**
     * Used to persist a given LinkedList of WebProcess instances into the Derby
     * database
     *
     * @param webProcesses LinkedList of WebProcess instances
     * @param con Derby connection
     * @return Number of records stored
     */
    private static int saveProcessesInDerbyDB(ArrayList<WebProcess> webProcesses, Connection con) {
        int result = 0;
        PreparedStatement ps;
        String sql;
        boolean onlyActiveProcesses = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.MEMORY_OnlyLoadActiveProcesses, false);
        try {

            int progress = 0;
            for (WebProcess webProcess : webProcesses) {
                if ((progress % 500) == 0) {
                    System.out.println("   Storing " + progress + " processes in the Java DB...");
                }
                progress++;
                if (webProcess.isActive() || onlyActiveProcesses == false) {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oos;
                    try {
                        oos = new ObjectOutputStream(bos);

                        oos.writeObject(webProcess);
                        oos.flush();
                        oos.close();
                        bos.close();

                    } catch (IOException ex) {
                        Logger.getLogger(RunTests.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }

                    byte[] data = bos.toByteArray();

                    sql = "insert into webprocess (theHashcode, javaObject) values(?, ?)";
                    ps = con.prepareStatement(sql);
                    ps.setInt(1, webProcess.hashCode());
                    ps.setObject(2, data);
                    try {
                        ps.executeUpdate();
                        con.commit();
                    } catch (SQLIntegrityConstraintViolationException ic) {
                        System.out.println("Failed to store process in database.  Suspected duplicate hashCode");
                        System.out.println("Last executed query:\n" + sql);
                        System.out.println("Last executed theHashcode:\n" + webProcess.hashCode());
                    } catch (SQLException s) {
                        System.out.println("Last executed query:\n" + sql);
                        System.out.println("Last executed theHashcode:\n" + webProcess.hashCode());
                    }
                }
            }
            result = webProcesses.size();

            System.out.println(progress
                    + " Processes stored in the Java DB.");

        } catch (java.sql.SQLException e) {
        }
        return result;
    }

    private boolean processAlreadyInDerbyDB(WebProcess webProcess, Connection con) {
        boolean result = false;
        PreparedStatement ps;

        // Does it already exist?
        String sql;
        try {
            sql = "select count(*) from webprocess where theHashcode=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, webProcess.hashCode());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getInt(1) > 0) {
                    result = true;

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileContainer.class
                    .getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    private static boolean _saveProcessInDerbyDB(WebProcess webProcess, Connection con) {
        boolean result = false;

        PreparedStatement ps;
        boolean onlyActiveProcesses = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.MEMORY_OnlyLoadActiveProcesses, false);
        if (webProcess.isActive() || onlyActiveProcesses == false) {

            // Does it already exist?
            String sql;

            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oos;
                try {
                    oos = new ObjectOutputStream(bos);

                    oos.writeObject(webProcess);
                    oos.flush();
                    oos.close();
                    bos.close();

                } catch (IOException ex) {
                    Logger.getLogger(RunTests.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

                byte[] data = bos.toByteArray();

                sql = "insert into webprocess (theHashcode, javaObject) values(?, ?)";
                ps = con.prepareStatement(sql);
                ps.setInt(1, webProcess.hashCode());
                ps.setObject(2, data);
                result = true;
                try {
                    ps.executeUpdate();
                    con.commit();
                } catch (SQLIntegrityConstraintViolationException ic) {
                    System.out.println("Failed to store process in database.  Suspected duplicate hashCode");
                    System.out.println("Last executed query:\n" + sql);
                    System.out.println("Last executed theHashcode:\n" + webProcess.hashCode());
                } catch (SQLException s) {
                    System.out.println("Last executed query:\n" + sql);
                    System.out.println("Last executed theHashcode:\n" + webProcess.hashCode());
                }
            } catch (java.sql.SQLException e) {
            }
        }
        return result;
    }

    /**
     * We typically do not remove processes from the DerbyDB, but in
     * process-intensive sites, the user may opt to only store active processes.
     * In this case, the call to update processes in the database should remove
     * closed processes accordingly
     *
     * @param webProcess
     * @param con
     * @return
     */
    private static boolean _deleteProcessFromDerbyDB(WebProcess webProcess, Connection con) {
        return _deleteProcessFromDerbyDB(webProcess.hashCode(), con);
    }

    private static boolean _deleteProcessFromDerbyDB(int webProcessHashCode, Connection con) {
        boolean result = false;
        PreparedStatement ps;

        // Does it already exist?
        String sql;

        try {
            sql = "delete from webprocess where theHashcode = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, webProcessHashCode);
            try {
                ps.executeUpdate();
                con.commit();
                result = true;
            } catch (SQLIntegrityConstraintViolationException ic) {
                System.out.println("Failed to delete process in database.  Suspected duplicate hashCode");
                System.out.println("Last executed query:\n" + sql);
                System.out.println("Last executed theHashcode:\n" + webProcessHashCode);
            } catch (SQLException s) {
                System.out.println("Last executed query:\n" + sql);
                System.out.println("Last executed theHashcode:\n" + webProcessHashCode);
            }
        } catch (java.sql.SQLException e) {
        }
        try {
            // If there was an attachment delete it
            //Need to get apache temp folder
            String relativeWebPath = FileContainer.getOSFileSeperator() + "temp";
            String catalinaBase = System.getProperty("catalina.base");
            String savePath = catalinaBase + relativeWebPath;
            // Get requested file by path info.
            // Decode the file name (might contain spaces and on) and prepare file object.
            File dir = new File(savePath + FileContainer.getOSFileSeperator());
            FileFilter fileFilter = new WildcardFileFilter(webProcessHashCode + ".*");
            File[] files = dir.listFiles(fileFilter);
            for (File fileInList : files) {
                fileInList.delete();
            }
        } catch (Exception e) {
            System.out.println("Unable to delete file for ESS[" + webProcessHashCode + "]");
        }
        return result;
    }

    /**
     * After deleting unused/closed web processes, we'd like to reduce the size
     * of the Derby DB
     *
     * @param con
     * @return
     */
    private static boolean _reduceSizeOfDerbyDB(Connection con) {
        boolean result = false;
        PreparedStatement ps;

        // Does it already exist?
        String sql;

        try {
            sql = "call SYSCS_UTIL.SYSCS_INPLACE_COMPRESS_TABLE('DBA', 'WEBPROCESS', 1, 1, 1);";
            ps = con.prepareStatement(sql);
            try {
                ps.executeUpdate();
                con.commit();
                result = true;
            } catch (SQLIntegrityConstraintViolationException ic) {
                System.out.println("Failed to reduce database size.");
                System.out.println("Last executed query:\n" + sql);
            } catch (SQLException s) {
                System.out.println("Last executed query:\n" + sql);
            }
        } catch (java.sql.SQLException e) {
        }
        return result;
    }

    /**
     * If the Derby database and tables do not exist, create it
     *
     * @return
     */
    static private void createDerbyTablesIfNotExist() {
        Connection dbConnection = getDerbyConnection();
        Statement st;
        DatabaseMetaData dbmd;
        try {
            dbmd = dbConnection.getMetaData();
            ResultSet rs = dbmd.getTables(null, null, "webprocess", null);
            if (!rs.next()) {
                System.out.println("Step 3: Preparing Derby Database for first use...");

                // Create schema (it may already exist)
                st = dbConnection.createStatement();
                try {
                    st.executeUpdate("CREATE SCHEMA DBA");
                } catch (SQLException e) {
                    //e.printStackTrace();
                }
                st.close();

                // Create table
                String sql = "CREATE TABLE DBA.webprocess ( theHashcode int NOT NULL, javaObject blob, PRIMARY KEY (theHashcode) )";
                try {
                    st = dbConnection.createStatement();
                    int executeUpdate = st.executeUpdate(sql);
                    st.close();
                } catch (SQLException e) {
                    //System.out.println("Exception in creating Derby database table:  Last SQL statement:"+sql+"\nException:"+e.getMessage());
                }
            } else {
                System.out.println("Step 3: ... Database already exists (skipped).");

            }

            dbConnection.close();

        } catch (SQLException ex) {
            Logger.getLogger(FileContainer.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    /* 
     * This routine will
     * a. Check for the existence of wmProcesses.bin
     * b. Check for the existence of a JavaDB called WebModuleDB. If it exists, clear it.  If it doesn't , create it.
     * c. If the wmProcesses.bin exists
     *      c1. Move processes from files to DB
     *      c2. Remove WebProcesses.bin
     */
    private static boolean moveProcessesFromFileToDB() {

        // a. Does the wmProcesses.bin file exist?
        if ((getOS() == OS_WINDOWS)
                && (getCurrentProcessesFile(getWorkingDirectory()).length() > 0)) {
            System.out.println("Step 3b: Moving all processes from binary files into Derby database...");
            ArrayList processesInFiles = getProcessDetailFromFile();

            Connection cn = getDerbyConnection();

            // c1. Move processes from files to db
            System.out.println("Moving processes into the Java DB...");
            saveProcessesInDerbyDB(processesInFiles, cn);

            // c2. Remove binary files
            System.out.println("Deleting process files ...");
            getCurrentProcessesFile(getWorkingDirectory()).delete();
            getClosedProcessesFile(getWorkingDirectory()).delete();

            // c3. Recreate same structure in PeopleWare database
            System.out.println("Recreating process history in PeopleWare ...");

            closeDerbyConnection(cn);
            dbPersist.ResetProcessDataInDatabase(processesInFiles);

        }
        return true;
    }

    /**
     * 1. Loads all the processes from the Derby database 2. Make sure the
     * processes exists in PeopleWare DB 3. If flagged for Active Only
     * processes, delete the rest from the DerbyDB
     *
     * @param onlyActiveProcesses
     * @return ArrayList comprising all the processes (WebProcess)
     */
    public static ArrayList loadFromDerbyAndSyncToSybase(boolean onlyActiveProcesses) {
        ArrayList result; // Loaded Web Processes
        ArrayList removeableHashCodes; // List of HashCodes to be removed

        // Connect to JavaDB
        result = new ArrayList(2000);
        removeableHashCodes = new ArrayList(2000);
        //
        // 1. Load Processes from Derby DB
        Connection con = getDerbyConnection();
        try {

            Object rmObj = null;
            PreparedStatement ps;
            ResultSet rs = null;
            String sql;

            // How many processes?
            sql = "select count(*) from DBA.webprocess";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            int wsTotal = 0;
            if (rs.next()) {
                wsTotal = rs.getInt(1);
            }
            rs.close();

            ByteArrayInputStream bais;
            ObjectInputStream ins;

            sql = "select * from DBA.webprocess";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            int progress = 0;

            while (rs.next()) {
                if ((progress % 500) == 0) {
                    System.out.println("Loading processes ..." + progress + "/" + wsTotal);
                }
                progress++;

                try {

                    bais = new ByteArrayInputStream(rs.getBytes("javaObject"));
                    ins = new ObjectInputStream(bais);

                    WebProcess wp = (WebProcess) ins.readObject();
                    // Make sure the process exists in PeopleWare
                    dbPersist.SaveToDatabase(wp);

                    // If we're only interested in Active Processes, mark the Inactive ones for deletion.
                    // Can't do it in one step as it will cause cursor manipulation issues.
                    if (!wp.isActive() && onlyActiveProcesses) {
                        removeableHashCodes.add(new Integer(wp.hashCode()));
                    } else {
                        result.add(wp);
                    }

                    ins.close();

                } catch (SQLException | IOException | ClassNotFoundException e) {
                }
            }

        } catch (SQLException e) {
        }

        //
        // 3. Now that we've loaded all the processes (or marked some for deletion after synchronizing it with the PeopleWare db, 
        //    delete the unwanted ones
        if (removeableHashCodes.size() > 0) {
            System.out.println("Deleting " + removeableHashCodes.size() + " closed processes from DerbyDB...");
            for (Iterator j = removeableHashCodes.iterator(); j.hasNext();) {
                int hashCode = (Integer) j.next();
                _deleteProcessFromDerbyDB(hashCode, con);
            }
            // CR 14127 - Free up disk space on Derby db
            System.out.println("Reducing database size after deletion of closed processes...");
            _reduceSizeOfDerbyDB(con);

            removeableHashCodes.clear();

            System.out.println(" done.");
        }

        closeDerbyConnection(con);

        return result;
    }

    public synchronized void updateProcessInDerbyAndSybase(WebProcess aProcess) {
        // Update in Sybase
        dbPersist.SaveToDatabase(aProcess);

        // Update in Derby
        Connection con;
        boolean onlyActiveProcesses = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.MEMORY_OnlyLoadActiveProcesses, false);
        // If the process is still active, or we're allowed to store processes...
        if (aProcess.isActive() || onlyActiveProcesses == false) {

            con = getDerbyConnection();
            if (processAlreadyInDerbyDB(aProcess, con)) {
                _updateProcessInDerbyDB(con, aProcess);
            } else {
                //System.out.println("save new process in Derby DB");
                _saveProcessInDerbyDB(aProcess, con);
            }
            closeDerbyConnection(con);

        }

        // If the process is not active, or we're not to store inactive processes, delete
        if (!aProcess.isActive() && onlyActiveProcesses == true) {
            con = getDerbyConnection();
            //System.out.println("deleting closed process from Derby DB");
            _deleteProcessFromDerbyDB(aProcess, con);
            closeDerbyConnection(con);
        }

    }

    /**
     * Saves the process into the PeopleWare and the Derby databases
     *
     * @param derbyConnection Derby Connection
     * @param aProcess WebProcess
     */
    private static void _updateProcessInDerbyDB(Connection derbyConnection, WebProcess aProcess) {

        // Save into Derby DB
        try {
            PreparedStatement ps;
            String sql;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos;
            try {
                oos = new ObjectOutputStream(bos);

                oos.writeObject(aProcess);
                oos.flush();
                oos.close();
                bos.close();

            } catch (IOException ex) {
                Logger.getLogger(RunTests.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            byte[] data = bos.toByteArray();

            sql = "update DBA.webprocess set javaObject=? where theHashcode=?";
            ps = derbyConnection.prepareStatement(sql);
            ps.setInt(2, aProcess.hashCode());
            ps.setObject(1, data);
            ps.executeUpdate();
            derbyConnection.commit();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public static void clearEssMobiReporting() {
        String connectionURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "");
        DatabaseObject.setConnectInfo_AccsysJDBC(connectionURL);

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            // 1. Clear ESS_MOBI_REPORTING
            DatabaseObject.executeSQL("DELETE FROM DBA.ESS_MOBI_REPORTING", con);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    public static void populateEssMobiReporting(int companyID, int employeeID, int staff_companyID, int staff_employeeID, int staff_employeeGroup) {
        String connectionURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "");
        DatabaseObject.setConnectInfo_AccsysJDBC(connectionURL);

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            // 1. Clear ESS_MOBI_REPORTING
            DatabaseObject.executeSQL("INSERT INTO DBA.ESS_MOBI_REPORTING(COMPANY_ID,EMPLOYEE_ID,STAFF_COMPANY_ID,STAFF_EMPLOYEE_ID,STAFF_EMPLOYEE_GROUP) VALUES(" + companyID + ","
                    + employeeID + "," + staff_companyID + ","
                    + staff_employeeID + "," + staff_employeeGroup + ")", con);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    public String duplicateEmployeeNumbers() {
        String connectionURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, "");
        DatabaseObject.setConnectInfo_AccsysJDBC(connectionURL);

        Connection con = null;
        ArrayList<String> duplicateNumbers = new ArrayList<>();
        try {
            con = DatabaseObject.getNewConnectionFromPool();

            // Read duplicate employee numbers
            ResultSet rs = DatabaseObject.openSQL("select COMPANY_EMPLOYEE_NUMBER, count(COMPANY_EMPLOYEE_NUMBER) as CNT from E_MASTER with (nolock) GROUP BY COMPANY_EMPLOYEE_NUMBER having CNT > 1 order by COMPANY_EMPLOYEE_NUMBER asc", con);

            // Process each entry
            while (rs.next()) {
                // Add employee number to the list
                duplicateNumbers.add(rs.getString("COMPANY_EMPLOYEE_NUMBER"));
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return duplicateNumbers.toString();
    }

    public java.util.ArrayList getLeaveProfileList() {
        ArrayList<String> rslt = new ArrayList<>();

        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String selectQuery = "select LP.PROFILE_ID, LT.NAME as TYPE, LP.NAME as PROFILE_NAME from L_PROFILE LP with (nolock) "
                    + " join L_LEAVE_TYPE LT with (nolock) on LP.LEAVE_TYPE_ID = LT.LEAVE_TYPE_ID WHERE ACTIVE_YN = 'Y' ORDER BY LP.PROFILE_ID, PROFILE_NAME, TYPE";

            ResultSet rs = DatabaseObject.openSQL(selectQuery, con);
            while (rs.next()) {
                String profile = Integer.toString(rs.getInt("PROFILE_ID")) + "_" + rs.getString("TYPE") + "_" + rs.getString("PROFILE_NAME");
                rslt.add(profile);
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

/// --------------------------------------------------------------------------------------
    public static int OS_WINDOWS = 0;
    public static int OS_LINUX = 1;
    private static File employeeSelectionsFile = null;
    private static File reportingStructuresFile = null;
    private static File webProcessDefinitionsFile = null;
    private static File webProcessDefinitionListFile = null;
    private static File currentProcessesFile = null;
    private static File closedProcessesFile = null;
    private static java.util.Date databaseCacheReloadTime = null;
    // Fixed File Names
    private static String employeeSelectionsFileName = "wmEmployeeSelections.bin";
    private static String reportingStructuresFileName = "wmReportingStructures.bin";
    private static String webProcessDefinitionsFileName = "wmProcessDefinitions.bin";
    // The webProcessDefinitionListFile simply lists the available web processes implemented by Accsys
    private static String webProcessDefinitionListFileName = "wmProcessDefinitionList.bin";
    public static String currentProcessesFileName = "wmProcesses.bin";
    public static String closedProcessesFileName = "wmClosedProcesses.bin";
    private static String messageLogFileName = "messages.txt";
    private static ArrayList<EmployeeSelection> employeeSelections;
    private static ArrayList<ReportingStructure> reportingStructures;
    private static ArrayList<WebProcessDefinition> webProcessDefinitions;
    private static ArrayList webProcessDefinitionList;
    private static ArrayList<WebProcess> webProcesses = new ArrayList(2000);
    //private static LinkedHashMap employeeMap = new LinkedHashMap();
    private static HashMap<String, Employee> employeeMap = new HashMap<>(); // Stores employees with "company_id,employee_id" as the KEY
    private static ConcurrentHashMap employeeLoginPwdMap = new ConcurrentHashMap(); // Used for fast login/pwd lookup
    // Stores a lookup 
    // KEY = login||pwd
    // VALUE = "company_id,employee_id"
    private static ArrayList<HolidayInfo> holidayInfo = new ArrayList<>(1000); // List of HolidayInfo classes
    private static ArrayList<PayrollDetail> payrollPeriods = new ArrayList<>(500); // List of PayrollDetail classes
    private static HashMap<Integer, LeaveInfo> leaveInfoList = new LinkedHashMap<>(); // List of LeaveInfo classes
    private static FileContainer fileContainer = null;
//    private static WebControllerDaemon webControllerDaemon = null;
    private static boolean isLoadingCache = false;
    private static boolean isCachingFromDB = false;
    private static int cachePercentage = 0;
    private static ArrayList<Employee> webAdministrators = new ArrayList<>();
    public static boolean isBusyCreatingFileContainer = false;
    private static boolean isBusySavingProcesses = false;
    private static HashMap cached_subordinates; // key = employee.longHashCode() + " " + webProcessDefinition.hashCode();
    private static WebProcessHistory_DBPersist dbPersist = new WebProcessHistory_DBPersist();
    public static LinkedList<Long> serverRequestHashValueBuffer = new LinkedList<>();
    // Derby connection
    public static boolean isESSReportingRegistered = false;
}
