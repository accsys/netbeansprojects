package za.co.ucs.accsys.webmodule;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeMap;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * EmployeeSelectionRule will utilize the fn_P_Math_CalcString() method in the database to return a set of
 * employees.
 * </p>
 */
public class EmployeeSelectionRule extends EmployeeSelection implements IEmployeeSelection, Serializable {

    // mandatory suid field (gotten using serialver on the original Class)
    //static final long serialVersionUID = -6756364686697947626L;
    private static final long serialVersionUID = 5121140767601454708L;

    public EmployeeSelectionRule(String name, String description) {
        super();
        super.setName(name);
        super.setDescription(description);
    }

    @Override
    public HashSet getEmployees() {
        String theRule = getRule();
        if (theRule == null) {
            return (new HashSet());
        }
        LinkedHashSet set = evaluateRule(theRule);
        java.util.TreeMap orderedResult = new TreeMap();

        // Now that we have the list of ids, we can go ahead and create the objects
        Iterator iter = set.iterator();
        //System.out.println("EmployeeSelectionRule: " + super.getName() + " - loading " + set.size() + " employees...");

        while (iter.hasNext()) {
            EmployeeKey key = (EmployeeKey) iter.next();
//            Employee employee = new Employee(company, new Integer(key.getEmployeeID()));
            Employee employee = FileContainer.getEmployeeFromContainer(new Integer(key.getCompanyID()), new Integer(key.getEmployeeID()));

            // place the employees in an ordered order
            orderedResult.put(employee.toString(), employee);
        }

        return (new HashSet(orderedResult.values()));
    }

    private void writeObject(ObjectOutputStream s)
            throws IOException {
        s.defaultWriteObject();
    }

    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        s.defaultReadObject();
    }

    /**
     * The Rule is the Peopleware Script that will be used to generate the list of employees.
     *
     * @return
     */
    public java.lang.String getRule() {
        return rule;
    }

    /**
     * Setter for property rule.
     *
     * @param rule New value of property rule.
     */
    public void setRule(java.lang.String rule) {
        this.rule = rule;
    }

    /**
     * Used to create an instance of a simple rule
     */
    private EmployeeSelectionSimpleRule constructSimpleRule(String simpleRule) {
        simpleRule = simpleRule.replaceAll("&nbsp;", " ");
        StringBuilder remainder = new StringBuilder(simpleRule.trim());
        // Where is the Comparator?
        // Do we have a Combiner before this rule?
        int comparatorPos = getComparatorPos(remainder.toString());
        if (comparatorPos >= 0) {
            // Keyword
            String keyword = remainder.substring(0, comparatorPos).trim();
            remainder.delete(0, comparatorPos);
            // comparator
            comparatorPos = getComparatorPos(remainder.toString());
            String comparator = getComparator(remainder.toString(), comparatorPos).trim();
            remainder.delete(0, comparator.length());
            // comparedTo
            String comparedTo = remainder.toString().trim();
            // Get rid of the single quotes.
            comparedTo = comparedTo.substring(1, comparedTo.length() - 1);

            // Now, create the simple rule
            return (new EmployeeSelectionSimpleRule(keyword, comparator, comparedTo));
        } else {
            return null;
        }

    }

    /**
     * Parser that 1. Breaks the rule up into simple rules of type Rule 2. Evaluates and combines those rules
     * 3. Returns the appropriate list of employees
     */
    private LinkedHashSet evaluateRule(String rule) {
        LinkedHashSet selection = new LinkedHashSet();

        StringBuffer textRule = new StringBuffer(rule);
        // Start by eliminating brackets
        //  Locate first ) and its matching (
        while (textRule.indexOf(")") >= 0) {
            int closeBracketPos = textRule.indexOf(")");
            int openBracketPos = textRule.substring(0, closeBracketPos).lastIndexOf("(");
            String subRule = textRule.substring(openBracketPos + 1, closeBracketPos);
            //return (evaluateRule(subRule));
            // Do we have a Combiner before these combined rules?
            int combinerPos = getCombinerPos(textRule.toString());
            if ((combinerPos >= 0) && (combinerPos < openBracketPos)) {
                selection = combineRuleResults(selection, evaluateRule(subRule), getCombiner(textRule.toString(), combinerPos));
            } else {
                selection = evaluateRule(subRule);
            }

            textRule = textRule.replace(openBracketPos, closeBracketPos + 1, "");
        }

        // At this stage, we should be without round brackets.
        // Now we start by extracting the simple rules and combining their results
        // E.g [Company_Name = 'Achme'] AND [Employee_Surname > 'G']
        while (textRule.toString().trim().length() > 0) {

            // Locate the next SimpleRule
            // Identified by [...]
            int startPos = textRule.indexOf("[");
            int endPos = textRule.indexOf("]", startPos);

            // No [,], treat this as one simple rule
            if ((startPos == -1) && (endPos == -1)) {
                startPos = 0;
                endPos = textRule.length();
            }

            // Evaluate Simple Rule
            EmployeeSelectionSimpleRule simpleRule = constructSimpleRule(textRule.substring(startPos + 1, endPos));
            if (simpleRule != null) {
                LinkedHashSet interimSelection = evaluateSimpleRule(simpleRule);
                // Do we have a Combiner before this rule?
                int combinerPos = getCombinerPos(textRule.toString());
                if ((combinerPos >= 0) && (combinerPos < startPos)) {
                    selection = combineRuleResults(selection, interimSelection, getCombiner(textRule.toString(), combinerPos));

                } else {
                    selection = interimSelection;
                }

                // Now, we need to reduce the textRule.
                textRule.replace(0, endPos + 1, "");
            } else {
                textRule = new StringBuffer("");
            }
        }

        return selection;
    }

    /**
     * Returns the first occurance of any one of the pre-defined Combiners
     */
    private int getCombinerPos(String rule) {
        int index = rule.length() + 1;
        for (String[] Combiner : Combiners) {
            if ((rule.toUpperCase().indexOf(Combiner[0].toUpperCase()) > 0) && (rule.toUpperCase().indexOf(Combiner[0].toUpperCase()) < index)) {
                index = rule.toUpperCase().indexOf(Combiner[0].toUpperCase());
            }
        }
        // If none were found, return -1
        if (index > rule.length()) {
            return -1;
        } else {
            return index;
        }
    }

    /**
     * Returns the actual combiner at the given position
     */
    private String getCombiner(String rule, int index) {
        for (String[] Combiner : Combiners) {
            if (rule.toUpperCase().indexOf(Combiner[0].toUpperCase()) == index) {
                return Combiner[0];
            }
        }
        return null;
    }

    /**
     * Returns the first occurance of any one of the pre-defined Comparators
     */
    private int getComparatorPos(String rule) {
        int index = rule.length() + 1;
        for (String Comparator : Comparators) {
            if ((rule.toUpperCase().indexOf(Comparator.toUpperCase()) >= 0) && (rule.toUpperCase().indexOf(Comparator.toUpperCase()) < index)) {
                index = rule.toUpperCase().indexOf(Comparator.toUpperCase());
            }
        }
        // If none were found, return -1
        if (index > rule.length()) {
            return -1;
        } else {
            return index;
        }
    }

    /**
     * Returns the actual comparator at the given position
     */
    private String getComparator(String rule, int index) {
        for (String Comparator : Comparators) {
            if (rule.toUpperCase().indexOf(Comparator.toUpperCase()) == index) {
                return Comparator;
            }
        }
        return null;
    }

    /**
     * Traverses through the possible rules, building lists accordingly "","","", "","","", "",""}
     */
    private LinkedHashSet evaluateSimpleRule(EmployeeSelectionSimpleRule simpleRule) {
        if (simpleRule.isOf("Company_Name")) {
            return buildCompanyNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Employee_FirstName")) {
            return buildEmployeeFirstNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Employee_Number")) {
            return buildEmployeeNumberList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Employee_Surname")) {
            return buildEmployeeSurnameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Description")) {
            return buildCostCentreDescriptionList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Name")) {
            return buildCostCentreNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Number")) {
            return buildCostCentreNumberList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Parent_Name")) {
            return buildCostCentreParentNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Parent_Number")) {
            return buildCostCentreParentNumberList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("CostCentre_Parent_Description")) {
            return buildCostCentreParentNumberList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("PayPoint_Name")) {
            return buildPayPointNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("PayPoint_Description")) {
            return buildPayPointDescriptionList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Payroll_Name")) {
            return buildPayrollNameList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Job_Position")) {
            return buildJobPositionList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Grade_Level")) {
            return buildGradeLevelList(simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("UserDefinedFieldNamed")) {
            return buildUserDefinedFieldList(simpleRule.getParameter(), simpleRule.getComparator(), simpleRule.getComparedTo());
        }

        if (simpleRule.isOf("Shift_Name")) {
            return buildShiftNameList(simpleRule.getParameter(), simpleRule.getComparator(), simpleRule.getComparedTo());
        }


        return null;
    }

    /**
     * Combines the results of two simple rules
     */
    private LinkedHashSet combineRuleResults(LinkedHashSet result1, LinkedHashSet result2, String combiner) {
        LinkedHashSet result = new LinkedHashSet();
        if (contains(Combiners, combiner)) {
            // "+","OR"
            if ((combiner.compareToIgnoreCase("+") == 0)
                    || (combiner.compareToIgnoreCase("OR") == 0)) {
                result1.addAll(result2);
                return result1;
            }

            // "AND"
            if ((combiner.compareToIgnoreCase("AND") == 0)) {
                // Add items if they occur in both lists
                Iterator iter = result1.iterator();
                while (iter.hasNext()) {
                    EmployeeKey object = (EmployeeKey) iter.next();
                    if (result2.contains(object)) {
                        result.add(object);
                    }
                }

                return result;
            }
        } else {
            System.out.println("! Invalid combiner in EmployeeSelectionRule:" + combiner);
            return null;
        }

        return result;
    }

    // Does the array contain the given word?
    private boolean contains(String[] array, String word) {
        for (String array1 : array) {
            if (array1.compareToIgnoreCase(word) == 0) {
                return true;
            }
        }
        return false;
    }

    private boolean listContains(LinkedHashSet list, Object object) {
        return list.contains(object);
    }

    // Does the array contain the given word?
    private boolean contains(String[][] array, String word) {
        for (String[] arrayOfWords : array) {
            for (String arrayOfWord : arrayOfWords) {
                if (arrayOfWord.compareToIgnoreCase(word) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * **********************************************************************************
     * Implementation of rules
     * **********************************************************************************
     */
    private LinkedHashSet buildCompanyNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "company_id in (select company_id from c_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildEmployeeFirstNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(firstname) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildEmployeeSurnameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(surname) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildEmployeeNumberList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(company_employee_number) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain where company_id=e_master.company_id and "
                + "upper(name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreNumberList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain where company_id=e_master.company_id and "
                + "upper(cost_number) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreDescriptionList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain where company_id=e_master.company_id and "
                + "upper(description) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreParentDescriptionList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain c1 where c1.company_id=e.company_id and "
                + "c1.parent_cost_id in (select cost_id from c_costmain c2 where c2.company_id=c1.company_id and "
                + "upper(description) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreParentNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain c1 where c1.company_id=e.company_id and "
                + "c1.parent_cost_id in (select cost_id from c_costmain c2 where c2.company_id=c1.company_id and "
                + "upper(name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildCostCentreParentNumberList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from e_master e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "cost_id in (select cost_id from c_costmain c1 where c1.company_id=e.company_id and "
                + "c1.parent_cost_id in (select cost_id from c_costmain c2 where c2.company_id=c1.company_id and "
                + "upper(cost_number) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildPayPointNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from p_e_master e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "paypoint_id in (select paypoint_id from p_paypoint where company_id=e.company_id and "
                + "upper(name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildPayPointDescriptionList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from p_e_master e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "paypoint_id in (select paypoint_id from p_paypoint where company_id=e.company_id and "
                + "upper(description) " + comparator + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildPayrollNameList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from pl_paydef_emaster e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "payroll_id in (select payroll_id from p_payroll where "
                + "upper(name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "'))";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildJobPositionList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from vw_J_EMP_JOBDETAIL e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(JOB_POSITION) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildGradeLevelList(String comparator, String compareTo) {
        String selection = "select COMPANY_ID, EMPLOYEE_ID from vw_J_EMP_JOBDETAIL e where "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(GRADE_LEVEL) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildUserDefinedFieldList(String fieldName, String comparator, String compareTo) {
        String selection = "select company_id, employee_id from e_genval gv, e_gendef gd where "
                + " gd.gen_id=gv.gen_id and gd.name='" + fieldName + "' and  "
                + "(datediff(month, fn_E_GetLastEmploymentEndDate(company_id, employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(company_id, employee_id) is null) and "
                + "upper(VALUE) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        //System.out.println("\n\tUDF - SQL: "+selection);
        return retrieveEmployeesFromDatabase(selection);
    }

    private LinkedHashSet buildShiftNameList(String fieldName, String comparator, String compareTo) {
        String selection = "select e.COMPANY_ID, e.EMPLOYEE_ID from ta_e_master e "
                + " join TA_SHIFT s on s.SHIFT_ID=fn_TA_GetPersonExpectedShift(person_id, today(), SHIFTPROFILE_ID, null, 'N') "
                + " where "
                + " (datediff(month, fn_E_GetLastEmploymentEndDate(e.company_id, e.employee_id), now(*))<3"
                + " or fn_E_GetLastEmploymentEndDate(e.company_id, e.employee_id) is null) and "
                + " upper(s.name) " + convertComparatorToSQL(comparator) + " upper('" + compareTo + "')";
        //System.out.println("\n\tSHIFT - SQL: "+selection);
        return retrieveEmployeesFromDatabase(selection);
    }


    private String convertComparatorToSQL(String comparator) {
        if (comparator.compareToIgnoreCase("Equals") == 0) {
            return "=";
        }
        if (comparator.compareToIgnoreCase("IsGreaterThan") == 0) {
            return ">";
        }
        if (comparator.compareToIgnoreCase("IsLessThan") == 0) {
            return "<";
        }
        if (comparator.compareToIgnoreCase("IsLike") == 0) {
            return "like";
        }
        if (comparator.compareToIgnoreCase("IsUnLike") == 0) {
            return "not like";
        }
        if (comparator.compareToIgnoreCase("IsNotEqualTo") == 0) {
            return "<>";
        }
        if (comparator.compareToIgnoreCase("IsEqualOrGreaterThan") == 0) {
            return ">=";
        }
        if (comparator.compareToIgnoreCase("IsEqualOrLessThan") == 0) {
            return "<=";
        }
        return "=";

    }

    /**
     * Used to perform the actual select statements WHATEVER select statement is run, we HAVE to retrieve
     * COMPANY_ID and EMPLOYEE_ID
     */
    private LinkedHashSet retrieveEmployeesFromDatabase(String SQLString) {
        LinkedHashSet result = new LinkedHashSet();

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            // Extract the required employees
            ResultSet rs = DatabaseObject.openSQL(SQLString, con);
            while (rs.next()) {
                int companyID = rs.getInt("COMPANY_ID");
                int employeeID = rs.getInt("EMPLOYEE_ID");
                EmployeeKey empKey = new EmployeeKey(companyID, employeeID);
                result.add(empKey);
            }
            rs.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return null;
    }

    /**
     * List all the employees extrated through the rule, ordered by surname, firstname
     *
     * @return
     */
    public String listResult() {
        StringBuilder result = new StringBuilder();
        HashSet employees = getEmployees();
        result.append("*** EmployeeSelectionRule:").append(this.getName()).append(" returns ").append(employees.size()).append(" Employees.\n");
        Iterator iter = employees.iterator();
        while (iter.hasNext()) {
            result.append("\n").append(((Employee) iter.next()).toString());
        }

        return result.toString();
    }

    @Override
    public String toString() {
        return this.getName();
    }

    /**
     * Returns the Keywords from FilterWords
     *
     * @return
     */
    public static String[] getKeyWords() {
        String[] result = new String[FilterWords.length];
        for (int i = 0; i < FilterWords.length; i++) {
            result[i] = FilterWords[i][0];
        }
        return result;
    }

    /**
     * Returns the Combiners
     *
     * @return
     */
    public static String[] getCombiners() {
        String[] result = new String[Combiners.length];
        for (int i = 0; i < Combiners.length; i++) {
            result[i] = Combiners[i][0];
        }
        return result;
    }

    public static String[][] FilterWords = {
        {"Company_Name", "The Name of the Company as defined in the Company Module"},
        {"CostCentre_Description", "Description of the Employee's Cost Centre"},
        {"CostCentre_Name", "Name of the Employee's Cost Centre"},
        {"CostCentre_Number", "Number of the Employee's Cost Centre"},
        {"CostCentre_Parent_Description", "Cost Centres can be 'stacked' under parent cost centres.\n"
            + "This is the description of the Employee's Cost Centre's Parent Cost Centre"},
        {"CostCentre_Parent_Name", "Cost Centres can be 'stacked' under parent cost centres.\n"
            + "This is the name of the Employee's Cost Centre's Parent Cost Centre"},
        {"CostCentre_Parent_Number", "Cost Centres can be 'stacked' under parent cost centres.\n"
            + "This is the number of the Employee's Cost Centre's Parent Cost Centre"},
        {"Employee_Firstname", "The Firstname of the Employee"},
        {"Employee_Number", "The Employee's employee number"},
        {"Employee_Surname", "The Surname of the Employee"},
        {"Grade_Level", "The Employee's Grade Level"},
        {"Job_Position", "The Employee's job position"},
        {"PayPoint_Name", "Name of Pay Point that this Employee is set to, in the Payroll Module"},
        {"PayPoint_Description", "Number of Pay Point that this Employee is set to, in the Payroll Module"},
        {"Payroll_Name", "Name of Payroll that this Employee is set to, in the Payroll Module"},
        {"UserDefinedFieldNamed{'FieldName'}", "User Defined Field with the given name in curley brackets"},
        {"Shift_Name", "Name of the shift the person is presently assigned to"}};
    public static String[][] Combiners = {
        {"AND", "Includes those employees that adheres to BOTH criteria"},
        {"OR", "Includes employees that adheres to ANY of the criteria"},
        {"XOR", "Only includes employees that adheres to ANY ONE of the criteria"},
        {"+", "Includes employees that adheres to ANY of the criteria"},
        {"-", "Remove second set from first set of criteria"}
    };
    public static String[] Comparators = {"Equals", "IsGreaterThan", "IsLessThan", "IsLike", "IsUnlike", "IsNotEqualTo",
        "IsEqualOrGreaterThan", "IsEqualOrLessThan"};

    private String rule;

}

/**
 * This class is used to extract the primary Keys from the database that will later be used to build employee
 * classes
 */
class EmployeeKey {

    public EmployeeKey(int companyID, int employeeID) {
        this.companyID = companyID;
        this.employeeID = employeeID;
    }

    public int getCompanyID() {
        return this.companyID;
    }

    public int getEmployeeID() {
        return this.employeeID;
    }

    @Override
    public int hashCode() {
        return (this.companyID * 10000) + this.employeeID;
    }

    @Override
    public boolean equals(Object object) {
        return (this.hashCode() == object.hashCode());
    }

    private int companyID;
    private int employeeID;
}
