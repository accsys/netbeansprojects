/*
 * Allows for the saving away (Creation / Update) of
 * Web Processes and its history into the PeopleWare database
 */
package za.co.ucs.accsys.webmodule;

import java.sql.SQLException;
import java.util.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 *
 * @author lterblanche
 */
public class WebProcessHistory_DBPersist {

    public WebProcessHistory_DBPersist() {
    }

    /**
     * Persists the process definition into ESS_PROCESS
     *
     * @param webProcess - WebProcess
     * @param con - Database Connection
     * @return TRUE - Success
     */
    private boolean SaveProcessToDB(WebProcess webProcess, java.sql.Connection con) {
        boolean allOK = true;
        int employee_id = webProcess.getEmployee().getEmployeeID();
        int company_id = webProcess.getEmployee().getCompany().getCompanyID();
        int creator_employee_id = webProcess.getLogger().getEmployeeID();
        int creator_company_id = webProcess.getLogger().getCompany().getCompanyID();
        //
        try {
            int pHashCode = webProcess.hashCode();
            String processName = webProcess.getProcessName().trim();
            java.util.Date creationDate = webProcess.getCreationDate();
            String isActive;
            if (webProcess.isActive()) {
                isActive = "Y";
            } else {
                isActive = "N";
            }

            String processSQL = "insert into ESS_PROCESS "
                    + "(COMPANY_ID, EMPLOYEE_ID, CREATOR_COMPANY_ID, CREATOR_EMPLOYEE_ID, HASHCODE, PROCESS_NAME, CREATION_DATE, ACTIVE_YN, DETAIL_PLAIN, DETAIL_HTML) "
                    + " on existing update values "
                    + "(" + company_id + "," + employee_id + "," + creator_company_id + "," + creator_employee_id + "," + pHashCode + ",'" + processName
                    + "','" + DatabaseObject.formatDateTime(creationDate) + "','" + isActive
                    + "','" + webProcess.toString().replace('\'', '\"') + "','" + webProcess.toHTMLString().replace('\'', '\"') + "'); commit;";

            try {
                // Save Process Definition into ESS_PROCESS
                DatabaseObject.executeSQL(processSQL, con);
                con.commit();


            } catch (java.sql.SQLException e) {
                allOK = false;
                saveExceptionToDatabasse(e.getMessage());
            }
        } catch (NullPointerException np) {
            System.out.println("Unable to save process to db.  Process details no longer exist.");
            saveExceptionToDatabasse(np.getMessage());
            allOK = false;
        }
        return allOK;
    }

    private void saveExceptionToDatabasse(String exceptionMessage) {
        //DatabaseObject.executeSQL("insert into ESS_ERRORS(time_stamp, error_msg) values (now(),'"+exceptionMessage+".  Process Deleted.');",con2);
        System.out.println("Exception on saving web process:" + exceptionMessage);
    }

    private boolean SaveProcessStageToDB(WebProcess webProcess, WebProcessStage webProcessStage, int stageIndex, java.sql.Connection con) {
        boolean allOK = true;
        int sHashCode = webProcess.hashCode();

        int owner_employee_id = webProcessStage.getOwner().getEmployeeID();
        int owner_company_id = webProcessStage.getOwner().getCompany().getCompanyID();
        java.util.Date creationDate = webProcessStage.getCreationDate();
        java.util.Date actionDate = webProcessStage.getActionDate();
        try {
            String employeeSelection = webProcessStage.getEmployeeSelection().getName();
            String stageAction = webProcessStage.getStageAction();
            String stageReason = webProcessStage.getStageReason();
            String processSQL = "insert into ESS_PROCESS_HISTORY "
                    + "(PROCESS_HASHCODE, STAGE_INDEX, OWNER_COMPANY_ID, OWNER_EMPLOYEE_ID, STAGE_CREATION_DATE, STAGE_ACTION_DATE, STAGE_EMPLOYEESELECTION_NAME, STAGE_ACTION, STAGE_REASON) "
                    + " on existing update values "
                    + "(" + sHashCode + "," + stageIndex + "," + owner_company_id + "," + owner_employee_id + ",'" + DatabaseObject.formatDateTime(creationDate)
                    + "','" + DatabaseObject.formatDateTime(actionDate) + "','" + employeeSelection.replace("'", "`") + "','" + stageAction + "','" + stageReason.replace("'", "`") + "');";
            if (actionDate == null) {
                processSQL = "insert into ESS_PROCESS_HISTORY "
                        + "(PROCESS_HASHCODE, STAGE_INDEX, OWNER_COMPANY_ID, OWNER_EMPLOYEE_ID, STAGE_CREATION_DATE, STAGE_EMPLOYEESELECTION_NAME, STAGE_ACTION, STAGE_REASON) "
                        + " on existing update values "
                        + "(" + sHashCode + "," + stageIndex + "," + owner_company_id + "," + owner_employee_id + ",'" + DatabaseObject.formatDateTime(creationDate)
                        + "','" + employeeSelection.replace("'", "`") + "','" + stageAction + "','" + stageReason.replace("'", "`") + "');commit;";
            }
            try {
                DatabaseObject.executeSQL(processSQL, con);
            } catch (java.sql.SQLException e) {
                allOK = false;
                saveExceptionToDatabasse(e.getMessage());
            }
        } catch (NullPointerException np) {
            System.out.println("Unable to save process stage to db.  Employee Selection no longer exists.");
            allOK = false;
            //saveExceptionToDatabasse(np.getMessage());
        }
        return allOK;
    }

    /**
     * This step is only taken if the original wmProcesses.bin file is being moved into the Derby datbase.
     * Once the Derby Database contains the current and historical processes, we will not recreate history in the ESS_PROCESS_... tables
     * 1. Deletes all the content from ESS_PROCESS_HISTORY and ESS_PROCESS from the PeopleWare database
     * 2. Rewrites all the processes in webProcesses (LinkedList) into said table structure
     * @param webProcesses
     */
    public void ResetProcessDataInDatabase(ArrayList webProcesses) {
        // Connects to database
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            // Delete everything
            String deleteDetailStatement = "delete from ESS_PROCESS_HISTORY;";
            String deleteMasterStatement = "delete from ESS_PROCESS;";
            try {
                DatabaseObject.executeSQL(deleteDetailStatement, con);
                DatabaseObject.executeSQL(deleteMasterStatement, con);
            } catch (java.sql.SQLException e) {
                saveExceptionToDatabasse(e.getMessage());
            }

            // Re'save' all processes
            for (Iterator i = webProcesses.iterator(); i.hasNext();) {
                WebProcess process = (WebProcess) i.next();
                SaveProcessToDB(process, con);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    /**
     * Creates/updates any historical record(s) on the given WebProcess into the PeopleWare database
     *
     * @param webProcess - WebProcess to be saved into the database.
     */
    public void SaveToDatabase(WebProcess webProcess) {

        // Connects to database
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            // Save Processes
            SaveProcessToDB(webProcess, con);

            for (int i = 0; i < webProcess.getProcessHistory().getProcessStages().size(); i++) {
                WebProcessStage stage = (WebProcessStage) webProcess.getProcessHistory().getProcessStages().get(i);
                SaveProcessStageToDB(webProcess, stage, i, con);
            }

            // Add the current stage to the list
            WebProcessStage currentStage = (WebProcessStage) webProcess.getCurrentStage();

            // The current process (which is not in the history) should be saved as a seperate event
            if (currentStage != null) {
                SaveProcessStageToDB(webProcess, currentStage, webProcess.getProcessHistory().getProcessStages().size(), con);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }
}
