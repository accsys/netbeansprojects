/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.accsys.webmodule;
import java.io.*;
import java.net.SocketException;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.*;

/**
 *
 * @author lterblanche
 */
public class iCalendar {
    
        public File getICalRequest(String fromEMail, String fromDescription, java.util.Calendar startDate, java.util.Calendar endDate, String eventName, String description, String attendee, String location, String ppmfilename) {
        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        net.fortuna.ical4j.model.TimeZone timezone = registry.getTimeZone("Africa/Maputo");
        VTimeZone tz = timezone.getVTimeZone();

        startDate.setTimeZone(timezone);
        endDate.setTimeZone(timezone);

        //
        // Create the event
        //
        DateTime start = new DateTime(startDate.getTime());
        DateTime end = new DateTime(endDate.getTime());
        VEvent meeting = new VEvent(start, end, eventName);


        VAlarm reminder = new VAlarm(new Dur(0, -1, 0, 0));
        reminder.getProperties().add(new net.fortuna.ical4j.model.property.Repeat(4));
        reminder.getProperties().add(new net.fortuna.ical4j.model.property.Duration(new Dur(0, 0, 15, 0)));
        reminder.getProperties().add(net.fortuna.ical4j.model.property.Action.DISPLAY);
        reminder.getProperties().add(new net.fortuna.ical4j.model.property.Description(description));

        // add timezone info..
        meeting.getProperties().add(tz.getTimeZoneId());

        // generate unique identifier for meeting request..
        net.fortuna.ical4j.util.UidGenerator ug = null;
        try {
            ug = new net.fortuna.ical4j.util.UidGenerator("uidGen");
        } catch (SocketException e) {
            e.printStackTrace();
        }
        net.fortuna.ical4j.model.property.Uid uid = ug.generateUid();
        meeting.getProperties().add(uid);

        // 
        // Extract attendees
        //
        int arraysize = 0;

        String temp_attendee = attendee;
        String[] attendee_list = temp_attendee.split("; ");

        for (int i = 0; i < temp_attendee.length(); i++) {
            if (",".indexOf(temp_attendee.charAt(i)) != -1) {
                arraysize++;
            }
        }
            for (String attendee_list1 : attendee_list) {
                net.fortuna.ical4j.model.property.Attendee dev1 = new net.fortuna.ical4j.model.property.Attendee(java.net.URI.create("MAILTO:" + attendee_list1));
                dev1.getParameters().add(net.fortuna.ical4j.model.parameter.Rsvp.TRUE);
                dev1.getParameters().add(net.fortuna.ical4j.model.parameter.Role.REQ_PARTICIPANT);
                //dev1.getParameters().add(new net.fortuna.ical4j.model.parameter.Cn("Developer " + i));
                meeting.getProperties().add(dev1);
                //System.out.println(attendee_list[i]);
            }

        //Description
        meeting.getProperties().add(new net.fortuna.ical4j.model.property.Description(description));
        meeting.getProperties().add(new net.fortuna.ical4j.model.property.Location(location));

        meeting.getAlarms().add(reminder);

        // Create a calendar
        net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(new net.fortuna.ical4j.model.property.ProdId("-//Microsoft Corporation//Outlook 11.0 MIMEDIR//EN"));
        icsCalendar.getProperties().add(net.fortuna.ical4j.model.property.Method.REQUEST);
        icsCalendar.getProperties().add(net.fortuna.ical4j.model.property.Version.VERSION_2_0);

        //Organizer
        net.fortuna.ical4j.model.property.Organizer org1 = new net.fortuna.ical4j.model.property.Organizer(java.net.URI.create("MAILTO:" + fromEMail));
        org1.getParameters().add(new net.fortuna.ical4j.model.parameter.Cn(fromDescription));
        meeting.getProperties().add(org1);
        meeting.getAlarms().add(reminder);

        net.fortuna.ical4j.model.property.Location loc1 = new net.fortuna.ical4j.model.property.Location(location);
        meeting.getProperties().add(loc1);

        // Add the event and print the file
        icsCalendar.getComponents().add(meeting);
        System.out.println(icsCalendar);

        //File file = new File("/apps/ppmtest/Genworth_Calendar/ICS_Files/bslcalendar_para.ics");

        File file = new File(ppmfilename);

        try {
            FileOutputStream fout = new FileOutputStream(file);

            net.fortuna.ical4j.data.CalendarOutputter outputter = new net.fortuna.ical4j.data.CalendarOutputter(false);
            outputter.output(icsCalendar, fout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (    IOException | ValidationException e) {
            e.printStackTrace();
        }
        return file;
    }

}
