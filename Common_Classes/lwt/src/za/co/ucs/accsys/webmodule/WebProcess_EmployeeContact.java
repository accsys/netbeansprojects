package za.co.ucs.accsys.webmodule;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import za.co.ucs.accsys.peopleware.Contact;
import za.co.ucs.accsys.peopleware.Employee;
import static za.co.ucs.accsys.webmodule.WebProcess.et_INITIAL;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * A WebProcess_EmployeeContact is an extension of WebProcess that specializes
 * in change in Contact Information
 * </p>
 */
public final class WebProcess_EmployeeContact extends WebProcess {

    /**
     * To change Contact information, WebProcess_EmployeeContact requires
     *
     * @param processDefinition - details of reporting structure, etc
     * @param owner - who created/is responsible for this process?
     * @param employee - which employee is requesting changes to contact
     * information?
     * @param contact - Contact class that replaces the current contact
     * information of this employee
     */
    public WebProcess_EmployeeContact(WebProcessDefinition processDefinition, Employee owner, Employee employee, Contact aNewContact, Employee aNewEmployee, String selectionID) {
        super(processDefinition, owner, employee, selectionID);

        this.newContact = aNewContact;
        this.newEmployee = aNewEmployee;

        // Is this a valid leave request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            // Get the initial employee selection
            if (selectionID.contains("null")) {
                this.initialEmployeeSelection = getCurrentStage().getEmployeeSelection();
            } else {
                this.initialEmployeeSelection = processDefinition.getReportingStructure().getEmployeeSelection(Integer.parseInt(selectionID));
            }
            this.escalateProcess(et_INITIAL, "", this.initialEmployeeSelection);
        }
    }

    /**
     * This method should be implemented by each and every descendent of
     * WebProcess. It returns an instance of ValidationResult which contains
     * both the isValid() property as well as a reason in case of a non-valid
     * result.
     */
    @Override
    public ValidationResult validate() {
        if (isActive()) {
            // Does the employee still exist?
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();

                ResultSet rs1 = DatabaseObject.openSQL(getEmployeeExistsStatement(), con);
                if (rs1.next()) {
                    if (rs1.getInt(1) <= 0) {
                        rs1.close();
                        DatabaseObject.releaseConnection(con);
                        return (new ValidationResult(false, "Employee (employee_id=" + this.getEmployee().getEmployeeID() + ") does not exist in database."));
                    }
                }
                rs1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
        return (new ValidationResult(true, ""));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Change contact information: ").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName());
        return result.toString();
    }

    // Explanation of process in HTML format
    @Override
    public String toHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append("Process Number: [").append(this.hashCode()).append("]<br>");

        // Explanation of what changes were requested
        String detail = getChanges(this.getEmployee().getContact(), this.getEmployee(), this.newContact, this.newEmployee, true);
        result.append("Change contact information: ").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName()).append(detail);

        return result.toString();
    }

    /**
     * SQL Statement to be used to check weather the process is still valid.
     * These statements contain replacement characters that will be 'replaced'
     * before being fed to the DatabaseComponent.
     *
     * @COMPANY_ID,
     * @EMPLOYEE_ID, etc.
     */
    private String getEmployeeExistsStatement() {
        return prepareStatement(getEmployeeExistsCONSTANTStatement());
    }

    /**
     * Returns a String representation of the differences between the instance
     * of Family and the current properties in this WebProcess
     */
    private String getChanges(Contact oldContact, Employee oldEmployee, Contact newContact, Employee newEmployee, boolean formatInHTML) {

        StringBuffer result = new StringBuffer();

        if (formatInHTML) {
            if (this.didUpdateChanges && changesHTML.equalsIgnoreCase("<table class=process_detail><font size='-3'></font></table>") == false) {
                return changesHTML;
            } else {
                try {
                    result.append("<table class=process_detail><font size='-3'>");
                    // Personal
                    if (isDifferent(newEmployee.getFirstName(), oldEmployee.getFirstName())) {
                        result.append("<tr><td>Firstname:</td><td>From '").append(oldEmployee.getFirstName()).append("'</td><td>To '").append(newEmployee.getFirstName()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getSecondName(), oldEmployee.getSecondName())) {
                        result.append("<tr><td>Second Name:</td><td>From '").append(oldEmployee.getSecondName()).append("'</td><td>To '").append(newEmployee.getSecondName()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getSurname(), oldEmployee.getSurname())) {
                        result.append("<tr><td>Surname:</td><td>From '").append(oldEmployee.getSurname()).append("'</td><td>To '").append(newEmployee.getSurname()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getPreferredName(), oldEmployee.getPreferredName())) {
                        result.append("<tr><td>Preferred Name:</td><td>From '").append(oldEmployee.getPreferredName()).append("'</td><td>To '").append(newEmployee.getPreferredName()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getInitials(), oldEmployee.getInitials())) {
                        result.append("<tr><td>Initials:</td><td>From '").append(oldEmployee.getInitials()).append("'</td><td>To '").append(newEmployee.getInitials()).append("'</td></tr>");
                    }
                    if (isDifferent(DatabaseObject.formatDate(newEmployee.getBirthDate()), DatabaseObject.formatDate(oldEmployee.getBirthDate()))) {
                        result.append("<tr><td>Birth Date:</td><td>From '").append(DatabaseObject.formatDate(oldEmployee.getBirthDate())).append("'</td><td>To '").append(DatabaseObject.formatDate(newEmployee.getBirthDate())).append("'</td></tr>");
                    }
                    // Race
                    if (isDifferent(newEmployee.getRace(), oldEmployee.getRace())) {
                        result.append("<tr><td>Race:</td><td>From '").append(oldEmployee.getRace()).append("'</td><td>To '").append(newEmployee.getRace()).append("'</td></tr>");
                    }
                    // Gender
                    if (isDifferent(newEmployee.getGender(), oldEmployee.getGender())) {
                        result.append("<tr><td>Gender:</td><td>From '").append(oldEmployee.getGender()).append("'</td><td>To '").append(newEmployee.getGender()).append("'</td></tr>");
                    }
                    // Gender
                    if (isDifferent(newEmployee.getTaxNumber(), oldEmployee.getTaxNumber())) {
                        result.append("<tr><td>Tax Number:</td><td>From '").append(oldEmployee.getTaxNumber()).append("'</td><td>To '").append(newEmployee.getTaxNumber()).append("'</td></tr>");
                    }

                    // Numbers
                    if (isDifferent(newContact.getCellNo(), oldContact.getCellNo())) {
                        result.append("<tr><td>CellNo:</td><td>From '").append(oldContact.getCellNo()).append("'</td><td>To '").append(newContact.getCellNo()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getEMail(), oldContact.getEMail())) {
                        result.append("<tr><td>E-Mail:</td><td>From '").append(oldContact.getEMail()).append("'</td><td>To '").append(newContact.getEMail()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getFaxHome(), oldContact.getFaxHome())) {
                        result.append("<tr><td>Home Fax:</td><td>From '").append(oldContact.getFaxHome()).append("'</td><td>To '").append(newContact.getFaxHome()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getFaxWork(), oldContact.getFaxWork())) {
                        result.append("<tr><td>Office Fax:</td><td>From '").append(oldContact.getFaxWork()).append("'</td><td>To '").append(newContact.getFaxWork()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getTelHome(), oldContact.getTelHome())) {
                        result.append("<tr><td>Home Tel:</td><td>From '").append(oldContact.getTelHome()).append("'</td><td>To '").append(newContact.getTelHome()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getTelWork(), oldContact.getTelWork())) {
                        result.append("<tr><td>Office Tel:</td><td>From '").append(oldContact.getTelWork()).append("'</td><td>To '").append(newContact.getTelWork()).append("'</td></tr>");
                    }
                    // Home Postal
                    if (isDifferent(newContact.getHomePostal1(), oldContact.getHomePostal1())) {
                        result.append("<tr><td>Home Postal 1:</td><td>From '").append(oldContact.getHomePostal1()).append("'</td><td>To '").append(newContact.getHomePostal1()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getHomePostal2(), oldContact.getHomePostal2())) {
                        result.append("<tr><td>Home Postal 2:</td><td>From '").append(oldContact.getHomePostal2()).append("'</td><td>To '").append(newContact.getHomePostal2()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getHomePostal3(), oldContact.getHomePostal3())) {
                        result.append("<tr><td>Home Postal 3:</td><td>From '").append(oldContact.getHomePostal3()).append("'</td><td>To '").append(newContact.getHomePostal3()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getHomePostal4(), oldContact.getHomePostal4())) {
                        result.append("<tr><td>Home Postal 4:</td><td>From '").append(oldContact.getHomePostal4()).append("'</td><td>To '").append(newContact.getHomePostal4()).append("'");
                    }
                    if (isDifferent(newContact.getHomePostalCode(), oldContact.getHomePostalCode())) {
                        result.append("<tr><td>Home Postal Code:</td><td>From '").append(oldContact.getHomePostalCode()).append("'</td><td>To '").append(newContact.getHomePostalCode()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_UNITNO(), oldContact.getPhys_HOME_UNITNO())) {
                        result.append("<tr><td>Unit Number:</td><td>From '").append(oldContact.getPhys_HOME_UNITNO()).append("'</td><td>To '").append(newContact.getPhys_HOME_UNITNO()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_COMPLEX(), oldContact.getPhys_HOME_COMPLEX())) {
                        result.append("<tr><td>Complex Name:</td><td>From '").append(oldContact.getPhys_HOME_COMPLEX()).append("'</td><td>To '").append(newContact.getPhys_HOME_COMPLEX()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_STREETNO(), oldContact.getPhys_HOME_STREETNO())) {
                        result.append("<tr><td>Street Number:</td><td>From '").append(oldContact.getPhys_HOME_STREETNO()).append("'</td><td>To '").append(newContact.getPhys_HOME_STREETNO()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_STREET(), oldContact.getPhys_HOME_STREET())) {
                        result.append("<tr><td>Street Name:</td><td>From '").append(oldContact.getPhys_HOME_STREET()).append("'</td><td>To '").append(newContact.getPhys_HOME_STREET()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_SUBURB(), oldContact.getPhys_HOME_SUBURB())) {
                        result.append("<tr><td>Suburb:</td><td>From '").append(oldContact.getPhys_HOME_SUBURB()).append("'</td><td>To '").append(newContact.getPhys_HOME_SUBURB()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_CITY(), oldContact.getPhys_HOME_CITY())) {
                        result.append("<tr><td>Residential City:</td><td>From '").append(oldContact.getPhys_HOME_CITY()).append("'</td><td>To '").append(newContact.getPhys_HOME_CITY()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_CODE(), oldContact.getPhys_HOME_CODE())) {
                        result.append("<tr><td>Residential Code:</td><td>From '").append(oldContact.getPhys_HOME_CODE()).append("'</td><td>To '").append(newContact.getPhys_HOME_CODE()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_PROVINCE(), oldContact.getPhys_HOME_PROVINCE())) {
                        result.append("<tr><td>Province:</td><td>From '").append(oldContact.getPhys_HOME_PROVINCE()).append("'</td><td>To '").append(newContact.getPhys_HOME_PROVINCE()).append("'</td></tr>");
                    }
                    if (isDifferent(newContact.getPhys_HOME_COUNTRY(), oldContact.getPhys_HOME_COUNTRY())) {
                        result.append("<tr><td>Country:</td><td>From '").append(oldContact.getPhys_HOME_COUNTRY()).append("'</td><td>To '").append(newContact.getPhys_HOME_COUNTRY()).append("'</td></tr>");
                    }

                    //Passport details
                    if (isDifferent(newEmployee.getPassportNumber(), oldEmployee.getPassportNumber())) {
                        result.append("<tr><td>Passport Number:</td><td>From '").append(oldEmployee.getPassportNumber()).append("'</td><td>To '").append(newEmployee.getPassportNumber()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getCountryOfIssue(), oldEmployee.getCountryOfIssue())) {
                        result.append("<tr><td>Country of Issue:</td><td>From '").append(oldEmployee.getCountryOfIssue()).append("'</td><td>To '").append(newEmployee.getCountryOfIssue()).append("'</td></tr>");
                    }

                    //emergency details 
                    if (isDifferent(newEmployee.getDoctor(), oldEmployee.getDoctor())) {
                        result.append("<tr><td>Doctor:</td><td>From '").append(oldEmployee.getDoctor()).append("'</td><td>To '").append(newEmployee.getDoctor()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getPhone(), oldEmployee.getPhone())) {
                        result.append("<tr><td>Phone:</td><td>From '").append(oldEmployee.getPhone()).append("'</td><td>To '").append(newEmployee.getPhone()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getKnownConditions(), oldEmployee.getKnownConditions())) {
                        result.append("<tr><td>Known Conditions:</td><td>From '").append(oldEmployee.getKnownConditions()).append("'</td><td>To '").append(newEmployee.getKnownConditions()).append("'</td></tr>");
                    }
                    if (isDifferent(newEmployee.getAllergies(), oldEmployee.getAllergies())) {
                        result.append("<tr><td>Allergies:</td><td>From '").append(oldEmployee.getAllergies()).append("'</td><td>To '").append(newEmployee.getAllergies()).append("'</td></tr>");
                    }

                    result.append("</font></table>");
                } catch (java.lang.NullPointerException e) {
                    result = new StringBuffer("<table class=process_detail><font size='-3'>");
                    result.append("<tr><td>The system is unable to determine the detail of this process.</td></tr>");
                    result.append("</font></table>");
                }
                // Persist changes for future reference and set didUpdateChanges
                this.didUpdateChanges = true;
                changesHTML = result.toString();
                return changesHTML;
            }

        } else {
            // Non HTML
            if (didUpdateChanges) {
                return changesPlain;
            } else {
                try {
                    String lineBreak = "\n";
                    // Personal
                    if (isDifferent(newEmployee.getFirstName(), oldEmployee.getFirstName())) {
                        result.append(lineBreak).append("Firstname: From '").append(oldEmployee.getFirstName()).append("' To '").append(newEmployee.getFirstName()).append("'");
                    }
                    if (isDifferent(newEmployee.getSecondName(), oldEmployee.getSecondName())) {
                        result.append(lineBreak).append("SecondName: From '").append(oldEmployee.getSecondName()).append("' To '").append(newEmployee.getSecondName()).append("'");
                    }
                    if (isDifferent(newEmployee.getSurname(), oldEmployee.getSurname())) {
                        result.append(lineBreak).append("Surname: From '").append(oldEmployee.getSurname()).append("' To '").append(newEmployee.getSurname()).append("'");
                    }
                    if (isDifferent(newEmployee.getPreferredName(), oldEmployee.getPreferredName())) {
                        result.append(lineBreak).append("Preferred Name: From '").append(oldEmployee.getPreferredName()).append("' To '").append(newEmployee.getPreferredName()).append("'");
                    }
                    if (isDifferent(newEmployee.getInitials(), oldEmployee.getInitials())) {
                        result.append(lineBreak).append("Initials: From '").append(oldEmployee.getInitials()).append("' To '").append(newEmployee.getInitials()).append("'");
                    }
                    if (isDifferent(DatabaseObject.formatDate(newEmployee.getBirthDate()), DatabaseObject.formatDate(oldEmployee.getBirthDate()))) {
                        result.append(lineBreak).append("Birthdate: From '").append(DatabaseObject.formatDate(oldEmployee.getBirthDate())).append("' To '").append(DatabaseObject.formatDate(newEmployee.getBirthDate())).append("'");
                    }
                    // Race
                    if (isDifferent(newEmployee.getRace(), oldEmployee.getRace())) {
                        result.append(lineBreak).append("Race: From '").append(oldEmployee.getRace()).append("' To '").append(newEmployee.getRace()).append("'");
                    }
                    // Gender
                    if (isDifferent(newEmployee.getGender(), oldEmployee.getGender())) {
                        result.append(lineBreak).append("Gender: From '").append(oldEmployee.getGender()).append("' To '").append(newEmployee.getGender()).append("'");
                    }

                    //Numbers
                    if (isDifferent(newContact.getCellNo(), oldContact.getCellNo())) {
                        result.append(lineBreak).append("CellNo: From '").append(oldContact.getCellNo()).append("' To '").append(newContact.getCellNo()).append("'");
                    }
                    if (isDifferent(newContact.getEMail(), oldContact.getEMail())) {
                        result.append(lineBreak).append("E-Mail: From '").append(oldContact.getEMail()).append("' To '").append(newContact.getEMail()).append("'");
                    }
                    if (isDifferent(newContact.getFaxHome(), oldContact.getFaxHome())) {
                        result.append(lineBreak).append("Home Fax: From '").append(oldContact.getFaxHome()).append("' To '").append(newContact.getFaxHome()).append("'");
                    }
                    if (isDifferent(newContact.getFaxWork(), oldContact.getFaxWork())) {
                        result.append(lineBreak).append("Office Fax: From '").append(oldContact.getFaxWork()).append("' To '").append(newContact.getFaxWork()).append("'");
                    }
                    if (isDifferent(newContact.getTelHome(), oldContact.getTelHome())) {
                        result.append(lineBreak).append("Home Tel: From '").append(oldContact.getTelHome()).append("' To '").append(newContact.getTelHome()).append("'");
                    }
                    if (isDifferent(newContact.getTelWork(), oldContact.getTelWork())) {
                        result.append(lineBreak).append("Office Tel: From '").append(oldContact.getTelWork()).append("' To '").append(newContact.getTelWork()).append("'");
                    }
                    // Home Postal
                    if (isDifferent(newContact.getHomePostal1(), oldContact.getHomePostal1())) {
                        result.append(lineBreak).append("Home Postal 1: From '").append(oldContact.getHomePostal1()).append("' To '").append(newContact.getHomePostal1()).append("'");
                    }
                    if (isDifferent(newContact.getHomePostal2(), oldContact.getHomePostal2())) {
                        result.append(lineBreak).append("Home Postal 2: From '").append(oldContact.getHomePostal2()).append("' To '").append(newContact.getHomePostal2()).append("'");
                    }
                    if (isDifferent(newContact.getHomePostal3(), oldContact.getHomePostal3())) {
                        result.append(lineBreak).append("Home Postal 3: From '").append(oldContact.getHomePostal3()).append("' To '").append(newContact.getHomePostal3()).append("'");
                    }
                    if (isDifferent(newContact.getHomePostal4(), oldContact.getHomePostal4())) {
                        result.append(lineBreak).append("Home Postal 4: From '").append(oldContact.getHomePostal4()).append("' To '").append(newContact.getHomePostal4()).append("'");
                    }
                    if (isDifferent(newContact.getHomePostalCode(), oldContact.getHomePostalCode())) {
                        result.append(lineBreak).append("Home Postal Code: From '").append(oldContact.getHomePostalCode()).append("' To '").append(newContact.getHomePostalCode()).append("'");
                    }
                    // Home Street
                    if (isDifferent(newContact.getHomeStreet1(), oldContact.getHomeStreet1())) {
                        result.append(lineBreak).append("Home Street 1: From '").append(oldContact.getHomeStreet1()).append("' To '").append(newContact.getHomeStreet1()).append("'");
                    }
                    if (isDifferent(newContact.getHomeStreet2(), oldContact.getHomeStreet2())) {
                        result.append(lineBreak).append("Home Street 2: From '").append(oldContact.getHomeStreet2()).append("' To '").append(newContact.getHomeStreet2()).append("'");
                    }
                    if (isDifferent(newContact.getHomeStreet3(), oldContact.getHomeStreet3())) {
                        result.append(lineBreak).append("Home Street 3: From '").append(oldContact.getHomeStreet3()).append("' To '").append(newContact.getHomeStreet3()).append("'");
                    }
                    if (isDifferent(newContact.getHomeStreet4(), oldContact.getHomeStreet4())) {
                        result.append(lineBreak).append("Home Street 4: From '").append(oldContact.getHomeStreet4()).append("' To '").append(newContact.getHomeStreet4()).append("'");
                    }
                    if (isDifferent(newContact.getHomeStreetCode(), oldContact.getHomeStreetCode())) {
                        result.append(lineBreak).append("Home Street Code: From '").append(oldContact.getHomeStreetCode()).append("' To '").append(newContact.getHomeStreetCode()).append("'");
                    }

                    if (isDifferent(newContact.getPhys_HOME_UNITNO(), oldContact.getPhys_HOME_UNITNO())) {
                        result.append(lineBreak).append("Unit Number: From '").append(oldContact.getPhys_HOME_UNITNO()).append("' To '").append(newContact.getPhys_HOME_UNITNO()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_COMPLEX(), oldContact.getPhys_HOME_COMPLEX())) {
                        result.append(lineBreak).append("Complex Name: From '").append(oldContact.getPhys_HOME_COMPLEX()).append("' To '").append(newContact.getPhys_HOME_COMPLEX()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_STREETNO(), oldContact.getPhys_HOME_STREETNO())) {
                        result.append(lineBreak).append("Street Number: From '").append(oldContact.getPhys_HOME_STREETNO()).append("' To '").append(newContact.getPhys_HOME_STREETNO()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_STREET(), oldContact.getPhys_HOME_STREET())) {
                        result.append(lineBreak).append("Street Name: From '").append(oldContact.getPhys_HOME_STREET()).append("' To '").append(newContact.getPhys_HOME_STREET()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_SUBURB(), oldContact.getPhys_HOME_SUBURB())) {
                        result.append(lineBreak).append("Suburb: From '").append(oldContact.getPhys_HOME_SUBURB()).append("' To '").append(newContact.getPhys_HOME_SUBURB()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_CITY(), oldContact.getPhys_HOME_CITY())) {
                        result.append(lineBreak).append("Residential City: From '").append(oldContact.getPhys_HOME_CITY()).append("' To '").append(newContact.getPhys_HOME_CITY()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_CODE(), oldContact.getPhys_HOME_CODE())) {
                        result.append(lineBreak).append("Residential Code: From '").append(oldContact.getPhys_HOME_CODE()).append("' To '").append(newContact.getPhys_HOME_CODE()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_PROVINCE(), oldContact.getPhys_HOME_PROVINCE())) {
                        result.append(lineBreak).append("Province: From '").append(oldContact.getPhys_HOME_PROVINCE()).append("' To '").append(newContact.getPhys_HOME_PROVINCE()).append("'");
                    }
                    if (isDifferent(newContact.getPhys_HOME_COUNTRY(), oldContact.getPhys_HOME_COUNTRY())) {
                        result.append(lineBreak).append("Country: From '").append(oldContact.getPhys_HOME_COUNTRY()).append("' To '").append(newContact.getPhys_HOME_COUNTRY()).append("'");
                    }

                    //Passport
                    if (isDifferent(newEmployee.getPassportNumber(), oldEmployee.getPassportNumber())) {
                        result.append(lineBreak).append("Passport Number: From '").append(oldEmployee.getPassportNumber()).append("' To '").append(newEmployee.getPassportNumber()).append("'");
                    }
                    if (isDifferent(newEmployee.getCountryOfIssue(), oldEmployee.getCountryOfIssue())) {
                        result.append(lineBreak).append("Country of Issue: From '").append(oldEmployee.getCountryOfIssue()).append("' To '").append(newEmployee.getCountryOfIssue()).append("'");
                    }

                    //Emergency
                    if (isDifferent(newEmployee.getDoctor(), oldEmployee.getDoctor())) {
                        result.append(lineBreak).append("Doctor: From '").append(oldEmployee.getDoctor()).append("' To '").append(newEmployee.getDoctor()).append("'");
                    }
                    if (isDifferent(newEmployee.getPhone(), oldEmployee.getPhone())) {
                        result.append(lineBreak).append("Phone: From '").append(oldEmployee.getPhone()).append("' To '").append(newEmployee.getPhone()).append("'");
                    }
                    if (isDifferent(newEmployee.getKnownConditions(), oldEmployee.getKnownConditions())) {
                        result.append(lineBreak).append("Known Conditions: From '").append(oldEmployee.getKnownConditions()).append("' To '").append(newEmployee.getKnownConditions()).append("'");
                    }
                    if (isDifferent(newEmployee.getAllergies(), oldEmployee.getAllergies())) {
                        result.append(lineBreak).append("Allergies: From '").append(oldEmployee.getAllergies()).append("' To '").append(newEmployee.getAllergies()).append("'");
                    }

                } catch (java.lang.NullPointerException e) {
                    result = new StringBuffer("< The system is unable to determine the detail of this process. >");
                }
                // Persist non-HTML changes for future reference and set didUpdateChanges
                didUpdateChanges = true;
                changesPlain = result.toString();
                return changesPlain;
            }
        }
    }

    /**
     * This statement gets executed when the process is to be written to the
     * database.
     */
    @Override
    String getCommitStatement() {
        String sqlString = "";
        sqlString = getCommitCONSTANTStatement();

        return (prepareStatement(sqlString));
    }

    /**
     * Internal method to perform parameter replacements
     */
    private String prepareStatement(String aStatementWithParameters) {
        String sqlString = aStatementWithParameters;
        sqlString = sqlString.replaceAll(":companyID", getEmployee().getCompany().getCompanyID().toString());
        sqlString = sqlString.replaceAll(":employeeID", getEmployee().getEmployeeID().toString());

        return sqlString;
    }

    private String getEmployeeExistsCONSTANTStatement() {
        return ("select count(*) from e_master  with (NOLOCK) where company_id=:companyID "
                + "and employee_id=:employeeID");
    }

    private String getCommitCONSTANTStatement() {
        StringBuilder result = new StringBuilder();
        // E_MASTER
        result.append("update e_master set ");
        result.append("SURNAME='").append(newEmployee.getSurname()).append("', ");
        result.append("FIRSTNAME='").append(newEmployee.getFirstName()).append("', ");
        result.append("SECONDNAME='").append(newEmployee.getSecondName()).append("', ");
        result.append("PREFERRED_NAME='").append(newEmployee.getPreferredName()).append("', ");
        result.append("INITIALS='").append(newEmployee.getInitials()).append("', ");
        result.append("BIRTHDATE='").append(DatabaseObject.formatDate(newEmployee.getBirthDate())).append("' ");
        result.append("where company_id=:companyID and employee_id=:employeeID; commit;");
        // E_CONTACT
        result.append("update e_contact set ");
        result.append("TEL_HOME='").append(newContact.getTelHome()).append("', ");
        result.append("TEL_WORK='").append(newContact.getTelWork()).append("', ");
        result.append("FAX_HOME='").append(newContact.getFaxHome()).append("', ");
        result.append("FAX_WORK='").append(newContact.getFaxWork()).append("', ");
        result.append("TEL_CELL='").append(newContact.getCellNo()).append("', ");
        result.append("E_MAIL='").append(newContact.getEMail()).append("' ");
        result.append("where company_id=:companyID and employee_id=:employeeID; commit;");
        // E_ADDRESS
        result.append("update e_address set ");
        result.append("HOME_STREET1='").append(newContact.getHomeStreet1()).append("', ");
        result.append("HOME_STREET2='").append(newContact.getHomeStreet2()).append("', ");
        result.append("HOME_STREET3='").append(newContact.getHomeStreet3()).append("', ");
        result.append("HOME_STREET4='").append(newContact.getHomeStreet4()).append("', ");
        result.append("HOME_STREET_CODE='").append(newContact.getHomeStreetCode()).append("', ");
        result.append("HOME_POSTAL1='").append(newContact.getHomePostal1()).append("', ");
        result.append("HOME_POSTAL2='").append(newContact.getHomePostal2()).append("', ");
        result.append("HOME_POSTAL3='").append(newContact.getHomePostal3()).append("', ");
        result.append("HOME_POSTAL4='").append(newContact.getHomePostal4()).append("', ");
        result.append("HOME_POSTAL_CODE='").append(newContact.getHomePostalCode()).append("', ");
        result.append("PHYS_HOME_UNITNO='").append(newContact.getPhys_HOME_UNITNO()).append("', ");
        result.append("PHYS_HOME_COMPLEX='").append(newContact.getPhys_HOME_COMPLEX()).append("', ");
        result.append("PHYS_HOME_STREETNO='").append(newContact.getPhys_HOME_STREETNO()).append("', ");
        result.append("PHYS_HOME_STREET='").append(newContact.getPhys_HOME_STREET()).append("', ");
        result.append("PHYS_HOME_SUBURB='").append(newContact.getPhys_HOME_SUBURB()).append("', ");
        result.append("PHYS_HOME_CITY='").append(newContact.getPhys_HOME_CITY()).append("', ");
        result.append("PHYS_HOME_CODE='").append(newContact.getPhys_HOME_CODE()).append("', ");
        result.append("PHYS_HOME_PROVINCE='").append(newContact.getPhys_HOME_PROVINCE()).append("', ");
        result.append("PHYS_HOME_COUNTRY='").append(newContact.getPhys_HOME_COUNTRY()).append("' ");
        result.append("where company_id=:companyID and employee_id=:employeeID; commit;");

        // E_NUMBER
        result.append("update e_number set ");
        result.append("TAX_NUMBER='").append(newEmployee.getTaxNumber()).append("' ");
        result.append("where company_id=:companyID and employee_id=:employeeID; commit;");

        // Race
        result.append("delete from e_group where company_id=:companyID and employee_id=:employeeID and group_id=2; ");
        result.append(" insert into e_group (company_id, employee_id, group_id, subgroup1_id) ");
        result.append(" values (:companyID,:employeeID, 2, ");
        result.append(" (select group_id from c_group where parent_group_id=2 ");
        result.append("  and trim(upper(description))=trim(upper('").append(newEmployee.getRace().trim()).append("')))); commit;");

        // Gender
        result.append("delete from e_group where company_id=:companyID and employee_id=:employeeID and group_id=1; ");
        result.append(" insert into e_group (company_id, employee_id, group_id, subgroup1_id) ");
        result.append(" values (:companyID,:employeeID, 1, ");
        result.append(" (select group_id from c_group where parent_group_id=1 ");
        result.append("  and trim(upper(description))=trim(upper('").append(newEmployee.getGender().trim()).append("')))); commit;");

        //E_PASSPORT           
        result.append("INSERT INTO E_PASSPORT (COMPANY_ID, EMPLOYEE_ID, PASSPORT_NUMBER, PASSPORT_HELD)");
        result.append("ON EXISTING UPDATE VALUES('");
        result.append(newEmployee.getCompany().getCompanyID()).append("','");
        result.append(newEmployee.getEmployeeID()).append("','");
        result.append(newEmployee.getPassportNumber()).append("','");
        result.append(newEmployee.getCountryOfIssue()).append("');");
        result.append("commit;");

        //E-EMERGENCY
        result.append("INSERT INTO E_EMERGENCY (COMPANY_ID, EMPLOYEE_ID, DOCTOR, PHONE, KNOWN_CONDITIONS, ALLERGIES)");
        result.append("ON EXISTING UPDATE VALUES('");
        result.append(newEmployee.getCompany().getCompanyID()).append("', '");
        result.append(newEmployee.getEmployeeID()).append("', '");
        result.append(newEmployee.getDoctor()).append("', '");
        result.append(newEmployee.getPhone()).append("', '");
        result.append(newEmployee.getKnownConditions()).append("', '");
        result.append(newEmployee.getAllergies()).append("');");
        result.append("commit;");

        return (result.toString());
    }

    private String getCountryList(String countryList) {
        // Establish LeaveType ID
        String aCountryList = " ";
        //int aLeaveTypeID = -1;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select COUNTRY from A_PASSPORT_COUNTRYLIST where", con);
            if (rs.next()) {
                aCountryList = rs.getString("COUNTRY");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return aCountryList;
    }

    @Override
    public String getProcessName() {
        return "Employee Contact Information";
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact
     za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact:    static final long serialVersionUID = -1459535708782221840L;

     */
    private static final long serialVersionUID = -1459535708782221840L;
    private Contact newContact = null;
    private Employee newEmployee = null;
    private EmployeeSelection initialEmployeeSelection;
} // end LeaveRequisitionProcess

