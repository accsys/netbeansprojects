/*
 * WebProcess_VariableChanges_55001.java
 *
 * Created on 25 April 2005, 09:25
 */

package za.co.ucs.accsys.webmodule;
import za.co.ucs.accsys.peopleware.*;
/**
 * Specialization of WebProcess_VariableChanges for variables with indicator code 55001
 * @author  lwt
 */
public class WebProcess_VariableChanges_55001 extends WebProcess_VariableChanges {
    
    /** Creates a new instance of WebProcess_VariableChanges_55001 */
    public WebProcess_VariableChanges_55001(WebProcessDefinition processDefinition, Employee owner, Employee employee, float value, String comment, String selectionID) {
        super(processDefinition, owner, employee, "55001", value, comment, selectionID);
    }
    
}
