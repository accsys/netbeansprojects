/*
 * ValidationResult.java
 *
 * Created on June 11, 2004, 4:25 PM
 */

package za.co.ucs.accsys.webmodule;

/**
 * ValidationResult is returned by the decendents of WebProcess's isValid() methods.
 * @author  liam
 */
public class ValidationResult {
    
    /** Creates a new instance of ValidationResult
     * @param isValid
     * @param invalidReason */
    public ValidationResult(boolean isValid, String invalidReason) {
        this.valid = isValid;
        if (!isValid){
            this.invalidReason = invalidReason;
        }
        else{
            this.invalidReason = "";
        }
        
    }
    
    /**
     * Getter for property isValid.
     * @return Value of property isValid.
     */
    public boolean isValid() {
        return valid;
    }
    
    public java.lang.String getInvalidReason() {
        return invalidReason;
    }

    
    private final boolean valid;
    private String invalidReason=null;
    
}
