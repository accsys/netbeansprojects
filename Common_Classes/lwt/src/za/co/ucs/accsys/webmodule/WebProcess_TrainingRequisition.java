/*
 * WebProcess_TrainingRequisition.java
 *
 * Created on 09 May 2011
 */
package za.co.ucs.accsys.webmodule;

import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;

/**
 * WebProcess_TrainingRequisition is a class that will requests for Training
 * (Events)
 *
 * @author lwt
 */
public class WebProcess_TrainingRequisition extends za.co.ucs.accsys.webmodule.WebProcess {

    /**
     * Creates a new instance of WebProcess_VariableXXXX
     *
     * @param processDefinition
     * @param owner
     * @param employee
     * @param eventID
     */
    public WebProcess_TrainingRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee, String eventID, String selectionID) {
        super(processDefinition, owner, employee, selectionID);
        this.iEventID = new Integer(eventID);
        ValidationResult validResult = validate();
        // if everything is OK, escalate
        if (validResult.isValid()) {
            // Get the initial employee selection
            if (selectionID.contains("null")) {
                this.initialEmployeeSelection = getCurrentStage().getEmployeeSelection();
            } else {
                this.initialEmployeeSelection = processDefinition.getReportingStructure().getEmployeeSelection(Integer.parseInt(selectionID));
            }
            escalateInitialProcess();
        } else {
            this.cancelProcess(validResult.getInvalidReason());
        }
    }

    private void escalateInitialProcess() {
        // Is this a valid leave request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            this.escalateProcess(et_INITIAL, "", this.initialEmployeeSelection);
        }
    }

    @Override
    String getCommitStatement() {
        // Compile the SQL statement used to validate weather the
        // employee can take leave or not.
        return prepareStatement(getCommitCONSTANTStatement());
    }

    /**
     * Internal method to perform parameter replacements
     */
    private String prepareStatement(String aStatementWithParameters) {
        String sqlString = aStatementWithParameters;
        TREvent event = new TREvent(iEventID);
        if (event.isValid()) {
            sqlString = sqlString.replaceAll(":companyID", getEmployee().getCompany().getCompanyID().toString());
            sqlString = sqlString.replaceAll(":employeeID", getEmployee().getEmployeeID().toString());
            sqlString = sqlString.replaceAll(":eventID", Integer.toString(iEventID));
            sqlString = sqlString.replaceAll(":costEvent", Float.toString(event.getCostEvent()));
            sqlString = sqlString.replaceAll(":costAccomodation", Float.toString(event.getCostAccomodation()));
            sqlString = sqlString.replaceAll(":costAllowances", Float.toString(event.getCostAllowances()));
            sqlString = sqlString.replaceAll(":costMaterials", Float.toString(event.getCostMaterials()));
            sqlString = sqlString.replaceAll(":costOther", Float.toString(event.getCostOther()));
            sqlString = sqlString.replaceAll(":costTravel", Float.toString(event.getCostTravel()));

        } else {
            this.cancelProcess("Unable to update process due to the Event (ID=" + iEventID + ") no longer existing in the database.");
            sqlString = "";
        }
        return sqlString;
    }

    @Override
    public String getProcessName() {
        return "Training Request";
    }

    @Override
    public String toHTMLString() {
        TREvent event = new TREvent(iEventID);
        StringBuilder result = new StringBuilder();
        if (event.isValid()) {
            result.append("Process Number: [").append(this.hashCode()).append("]<br>");
            result.append("<table class=process_detail>");
            result.append("  <tr>");
            result.append("    <td>Course:").append(event.getCourseName()).append("</td>");
            result.append("  </tr>");
            result.append("  <tr>");
            result.append("    <td>Provider:").append(event.getProviderName()).append("</td>");
            result.append("  </tr>");
            result.append("  <tr>");
            result.append("    <td>Date:").append(DatabaseObject.formatDate(event.getFromDate())).append(" - ").append(DatabaseObject.formatDate(event.getToDate())).append("</td>");
            result.append("  </tr>");
            //
            // Cost
            //
            {
                if (event.getCostEvent() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Course:").append(event.getCostEvent()).append("</td>");
                    result.append("  </tr>");
                }
                if (event.getCostAccomodation() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Accomodation:").append(event.getCostAccomodation()).append("</td>");
                    result.append("  </tr>");
                }
                if (event.getCostAllowances() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Allowances:").append(event.getCostAllowances()).append("</td>");
                    result.append("  </tr>");
                }
                if (event.getCostMaterials() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Materials:").append(event.getCostMaterials()).append("</td>");
                    result.append("  </tr>");
                }
                if (event.getCostTravel() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Travel:").append(event.getCostTravel()).append("</td>");
                    result.append("  </tr>");
                }
                if (event.getCostOther() > 0) {
                    result.append("  <tr>");
                    result.append("    <td>Cost - Other:").append(event.getCostOther()).append("</td>");
                    result.append("  </tr>");
                }
            }
            result.append("  </table>");

        } else {
            result.append("Process Number: [").append(this.hashCode()).append("]<br>");
            result.append("<table class=process_detail>");
            result.append("  <tr>");
            result.append("    <td><i>This request is no longer valid.  It is possible that the scheduled event has been deleted from the Training Module.</td>");
            result.append("  </tr>");
            result.append("</table>");
        }
        return result.toString();
    }

    @Override
    public String toString() {
        TREvent event = new TREvent(iEventID);
        StringBuilder result = new StringBuilder();
        if (event.isValid()) {
            result.append("Request to attend Training: ").append(event.getCourseName());
            result.append("\n\tOn: ").append(DatabaseObject.formatDate(event.getFromDate()));
        } else {
            result.append("Invalid Training Request - Event ID: ").append(Integer.toString(iEventID));
        }
        return result.toString();
    }

    @Override
    public final ValidationResult validate() {
        ValidationResult valid = null;
        try {
            TREvent event = new TREvent(iEventID);
            if (event.isValid()) {
                valid = new ValidationResult(true, "All OK");
            }
        } catch (Exception e) {
            valid = new ValidationResult(false, e.getMessage());
        }
        return valid;
    }

    private String getCommitCONSTANTStatement() {
        StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append("insert into TRL_EMPLOYEE_EVENT (EVENT_ID, COMPANY_ID, EMPLOYEE_ID, COST_ENTITY, ");
        sqlStatement.append("COST_TRAVEL, COST_ACCOMODATION, COST_MATERIALS, COST_ALLOWANCES, COST_OTHER) ");
        sqlStatement.append(" on existing update values ");
        sqlStatement.append("(:eventID,:companyID,:employeeID, :costEvent, :costTravel, :costAccomodation, :costMaterials, :costAllowances, :costOther) ");
        return sqlStatement.toString();

    }

    public int getEventID() {
        return this.iEventID;
    }

    private int iEventID = 0;
    private EmployeeSelection initialEmployeeSelection;
}
