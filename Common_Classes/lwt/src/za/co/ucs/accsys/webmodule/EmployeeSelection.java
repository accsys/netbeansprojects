package za.co.ucs.accsys.webmodule;

import java.io.IOException;
import java.io.Serializable;

/**
 * <p>
 * EmployeeSelection is an abstract class.  It's child-classes will have
 * the ability to generate a list of employees based on various rules / logic
 * </p>
 */
public abstract class EmployeeSelection implements Serializable {
//    private static final long serialVersionUID = -1706531587362066341L;
    
    public EmployeeSelection(){
    }
    
    /**
     * Getter for property description.
     * @return Value of property description.
     */
    public java.lang.String getDescription() {
        return description;
    }
    
    /**
     * Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }
    
    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    @Override
    public int hashCode(){
        int v = name.hashCode();
        return v;
    }
    
    public void writeObject(Object obj)
    throws IOException{
        
        writeObject(name);
        writeObject(description);
        writeObject(creationDate);
    }
    
    public Object readObject()
    throws ClassNotFoundException,
    IOException{
        this.name = (String)readObject();
        this.description = (String)readObject();
        this.creationDate = (java.util.Date)readObject();
        System.out.println("Read: Creation Date"+this.getCreationDate());
        return this;
    }
    
    public boolean equals(EmployeeSelection anotherEmployeeSelection){
        if (anotherEmployeeSelection == null){
            return false;
        }    else {
            return (this.hashCode()==anotherEmployeeSelection.hashCode());
        }
    }
    
    /** Used to uniquely identify a selection object
     */
    public java.util.Date getCreationDate(){
        return this.creationDate;
    }
    
    public void setCreationDate(java.util.Date creationDate){
        this.creationDate = creationDate;
        System.out.println("Creation Date"+this.getCreationDate());
    }
    
    private String name;
    private String description;
    private java.util.Date creationDate;
    
    
} // end EmployeeSelection



