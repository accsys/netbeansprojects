/*
 * SimpleRule.java
 *
 * Created on June 15, 2004, 1:44 PM
 */

package za.co.ucs.accsys.webmodule;

/**
 * EmployeeSelectionSimple is the smallest simple configuration of a rule that
 * can be used by the EmployeeSelectionRule class
 * @author  liam
 */
public class EmployeeSelectionSimpleRule {
    
    /** This private class handles the construction of a simple rule
     * E.g. Company_Name > 'Alfa Dealers'
     * keyword="Company_Name", "Job_Title", "UserDefinedFieldNamed{'Shoe Size'}"
     * comparator=">"
     * comparedTo="Alfa Dealers"
     */
    public EmployeeSelectionSimpleRule(String keyword, String comparator, String comparedTo){
        this.keyword = keyword;
        this.comparator = comparator;
        this.comparedTo = comparedTo;
        parseKeyword(keyword);
    }
    
    /** This method takes a keyword and breaks it up into a keyword and a paramater.
     * E.g. CompanyName becomes 'CompanyName' and ""
     *      UserDefinedFieldNamed{'Reports To'} becomes 'UserDefinedFieldNamed' and 'Reports To'
     */
    private void parseKeyword(String keyword){
        int position = keyword.indexOf("{'");
        int endPosition = keyword.indexOf("'}");
        if (position == -1){
            this.keyword = keyword;
            this.parameter = "";
        } else {
            this.keyword = keyword.substring(0,position);
            this.parameter = keyword.substring(position+2, endPosition);
        }
        //System.out.println("parseKeyword:"+keyword+" to "+this.keyword+" and "+this.parameter);
    }
    
    /**
     * Getter for property comparator.
     * @return Value of property comparator.
     */
    public java.lang.String getComparator() {
        return comparator;
    }
    
    /**
     * Getter for property comparedTo.
     * @return Value of property comparedTo.
     */
    public java.lang.String getComparedTo() {
        return comparedTo;
    }
    
    /**
     * Getter for property keyword1.
     * @return Value of property keyword1.
     */
    public java.lang.String getKeyword1() {
        return keyword;
    }
    
    public String toString(){
        return (keyword+":"+comparator+":"+comparedTo);
    }
    
    /** Returns True if the same keyword sequence is used
     */
    public boolean isOf(String keyword){
        return ((this.keyword.compareToIgnoreCase(keyword)==0));
    }
    
    /** Returns the parameter embedded in the KEYWORD section of a rule:
     * E.g. UserDefinedFieldNamed{'Shoe Size'} returns 'Show Size'
     */
    
    public String getParameter(){
        return this.parameter;
    }
    
    private String keyword;
    private String comparator;
    private String comparedTo;
    private String parameter;
    
}
