/*
 * WebConfig.java
 *
 * Created on June 18, 2004, 3:12 PM
 * Last updated on July 29 2010
 */
package za.co.ucs.accsys.webmodule.applications;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.print.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.tree.*;
import za.co.ucs.accsys.webmodule.EmployeeSelection;
import za.co.ucs.accsys.webmodule.EmployeeSelectionRule;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.ReportingStructure;
import za.co.ucs.accsys.webmodule.SelectionInterpreter;
import za.co.ucs.accsys.webmodule.SelectionLink;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;
import za.co.ucs.accsys.webmodule.WebProcessHistory;
import za.co.ucs.accsys.webmodule.WebProcessStage;
import za.co.ucs.lwt.initools.OutputFile;

/**
 *
 * @author liam
 */
public final class WebConfig extends javax.swing.JFrame implements DropTargetListener {

    /**
     * Creates new form WebConfig
     */
    public WebConfig() {
        initComponents();
        // Duplicate contents of the File Container
        loadFromFileContainer();
        initCustomComponents();
    }

    /**
     * Setup the components used in the Web Process Definitions
     */
    private void initWebProcessDefinitionsTab() {
        // Populate existing WebProcessDefinitions
        DefaultListModel listModel = new DefaultListModel();
        lstExistingWebProcessDefinitions.setModel(listModel);

        // Build Existing WebProcessDefinitions list
        populateWebProcessDefinitionsList((DefaultListModel) lstExistingWebProcessDefinitions.getModel());

        // Update combobox of available reporting structures to choose from
        refreshReportingStructureCombobox();

        // Select the first item in the list
        if (lstExistingWebProcessDefinitions.getModel().getSize() > 0) {
            lstExistingWebProcessDefinitions.setSelectedIndex(0);
        }
    }

    /**
     * Setup the components used in the Reporting Structure Tab
     */
    private void initReportingStructureTab() {
        // Populate existing ReportingStructures
        DefaultListModel listModel = new DefaultListModel();
        lstExistingReportingStructures.setModel(listModel);

        // Build Existing ReportingStructure list
        populateReportingStructureList((DefaultListModel) lstExistingReportingStructures.getModel());
        // Select the first item in the list
        if (lstExistingReportingStructures.getModel().getSize() > 0) {
            lstExistingReportingStructures.setSelectedIndex(0);
        }
        // Initialise TreeView for Drag-and-Drop
        DropTarget dropTarget = new DropTarget(treeReportingStructure, DnDConstants.ACTION_MOVE, this);

        // refresh the treeview display
        refreshTreeViewDisplay();
    }

    /**
     * Setup the components used in the Selection Formula Tab
     */
    private void initSelectionFormulaTab() {
        // Scripts Editor Pane's Editor Kit
        editorPane.setEditorKit(new javax.swing.text.html.HTMLEditorKit());

        // Populate Filter Combobox
        cbbFilters.removeAllItems();
        for (int i = 0; i < EmployeeSelectionRule.getKeyWords().length; i++) {
            cbbFilters.addItem(EmployeeSelectionRule.getKeyWords()[i]);
        }

        // Populate Compare Combobox
        cbbCompare.removeAllItems();
        for (int i = 0; i < EmployeeSelectionRule.Comparators.length; i++) {
            cbbCompare.addItem(EmployeeSelectionRule.Comparators[i]);
        }

        // Populate existing EmployeeSelections
        DefaultListModel esModel = new DefaultListModel();
        DefaultListModel rsModel = new DefaultListModel();

        lstExistingEmployeeSelections.setModel(esModel);
        lstExistingEmployeeSelectionsInTreeView.setModel(rsModel);

        populateEmployeeSelectionList(esModel);
        populateEmployeeSelectionList(rsModel);
        // populateEmployeeSelectionListExcludingTreeView(rsModel);
    }

    /**
     * Finds the selected Reporting Structure and rebuilds the treeview
     */
    private void refreshTreeViewDisplay() {
        //removeTopLevel();
        if (lstExistingReportingStructures.getSelectedValue() != null) {
            treeReportingStructure.setModel(createTreeModel((ReportingStructure) lstExistingReportingStructures.getSelectedValue()));
            expandJTree(treeReportingStructure);
        } else {
            // clear treeview
            treeReportingStructure.setModel(new DefaultTreeModel(null));
        }
    }

    /**
     * Updates the combobox listing available reporting structures
     */
    private void refreshReportingStructureCombobox() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (int i = 0; i < lstExistingReportingStructures.getModel().getSize(); i++) {
            model.addElement(lstExistingReportingStructures.getModel().getElementAt(i));
        }
        cbbReportingStructure.setModel(model);
    }
    /**
     * Checks that the reporting structures have different first letters in the name
     */
    private void checkReportingStructureName() {
        Iterator iter = localReportingStructures.iterator();
        boolean allOK = false;
        int i = 1;
        while (iter.hasNext()) {
            allOK = true;  // Resetting the boolean status so that if everything evalautes correctly, it will not run through this loop more than once.

            ReportingStructure structure = (ReportingStructure) iter.next();
            lblValidationProgress.setText("Testing Reporting Structure " + (i + 1) + "/" + localReportingStructures.size() + ": " + structure.toString());
            pbValidationProgress.setValue(i++);
            pbValidationProgress.repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException err) {
            }

            //
            // Check that names differ
            //
            LinkedList<ReportingStructure> localStructures = new LinkedList(localReportingStructures);
            //System.out.println(i - 2);
            localStructures.remove(i - 2);
            for (ReportingStructure structureInList : localStructures) {
                //System.out.println("structure hashCode: " + structure.hashCode());
                //System.out.println("structureInList hashCode: " + structureInList.hashCode());
                // If hashCodes are the same rename the current reporting structure
                if (structure.hashCode() == structureInList.hashCode()) {
                    allOK = false;
                    // Ask user to change name
                    // Edit a Reporting Structure
                    // Edit dialog
                    String nameOfCurrentStructure = structure.getName();
                    String oldName = structureInList.getName();
                    String message = "The hashcodes produced for " + nameOfCurrentStructure + " and " + oldName + " are the same. Please rename " + nameOfCurrentStructure + " to avoid errors in the reporting structure.\nIf you do not rename the structure will be renamed automatically.";
                    String newStructureName = JOptionPane.showInputDialog(null, message, "Similar hashcode error", JOptionPane.WARNING_MESSAGE);
                    // Rename Structure
                    if ((newStructureName != null) && (newStructureName.trim().length() > 0)) {
                        //ReportingStructure existingReportingStructure = (ReportingStructure) lstExistingReportingStructures.getSelectedValue();
                        editReportingStructure(structure, newStructureName);
                        setContentsChanged(true);
                        // Restart validation process
                    } else {
                        // Auto-rename
                        newStructureName = i - 2 + "_renamed_" + nameOfCurrentStructure;
                        editReportingStructure(structure, newStructureName);
                        setContentsChanged(true);

                    }
                }

            }
        }
    }
    
    // Get a Copy of Reporting Structures and Employee Selections from the FileContainer

    private void loadFromFileContainer() {
        // Employee Selections
        for (int i = 0; i < fileContainer.getEmployeeSelections().size(); i++) {
            this.localEmployeeSelections.add(fileContainer.getEmployeeSelections().get(i));
        }

        // Reporting Structures
        for (int j = 0; j < fileContainer.getReportingStructures().size(); j++) {
            this.localReportingStructures.add(fileContainer.getReportingStructures().get(j));
        }

        // Process Definitions
        for (int k = 0; k < fileContainer.getWebProcessDefinitions().size(); k++) {
            this.localWebProcessDefinitions.add(fileContainer.getWebProcessDefinitions().get(k));
        }

    }

    // Prints the current configuration
    private void printSetup() {

        // Construct the contents of the StringBuffer that will be printed
        StringBuffer htmlContents = buildPrinterContentInHTML(false);
        System.out.println(htmlContents.toString());

        // Send the contents to the TextPrinter
        //TextPrinter printer = new TextPrinter(contents);

        // Print
        //printer.print();

        printableEditorPane jEditorPane = new printableEditorPane();

        jEditorPane.setContentType("text/html");
        jEditorPane.setText(htmlContents.toString());

        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(jEditorPane);
        if (job.printDialog()) /* Displays the standard system print dialog
         */ {
            try {
                job.print();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    // Exports the current configuration in HTML format
    private void exportSetup() {
        // Construct the contents of the StringBuffer that will be printed
        StringBuffer htmlContents = buildPrinterContentInHTML(true);

        FileDialog fd = new FileDialog(this, "Export WebModule setup to an HTML file...",
                FileDialog.SAVE);
        fd.setFile(exportFileName);
        fd.setLocation(50, 50);
        fd.setVisible(true);
        String toFile = fd.getFile();
        String toDirectory = fd.getDirectory();
        if (toFile != null) {
            try {
                OutputFile out = new OutputFile(toDirectory + toFile, true);

                out.println(htmlContents.toString());
                out.close();
                JOptionPane.showMessageDialog(this, "Export complete.");
            } catch (IOException io) {
                JOptionPane.showMessageDialog(this, io.getMessage(), "Export failed", JOptionPane.ERROR_MESSAGE);
            }
            //}
        }
    }

    private void exportProcessHistory() {

        FileDialog fd = new FileDialog(this, "Export Process History to a CSV file...",
                FileDialog.SAVE);
        fd.setFile(processExportFileName);
        fd.setLocation(50, 50);
        fd.setVisible(true);
        String toFile = fd.getFile();
        String toDirectory = fd.getDirectory();
        if (toFile != null) {
            StringBuilder result = new StringBuilder();
            // Header
            result.append("\nRow, HashCode, Creation Date, Logged By, On Behalf Of, Type, Status, Details, History");

            // Built content
            int lineCounter = 0;
            for (Iterator iter = fileContainer.getWebProcesses().iterator(); iter.hasNext();) {
                lineCounter++;
                WebProcess process = (WebProcess) iter.next();
                String procDetail = process.toString().trim().replace("\n", "");
                result.append("\n").append(lineCounter).append(",").append(process.hashCode()).append(",").append(process.getCreationDate()).append(",").append(process.getLogger()).append(",").append(process.getEmployee()).append(",").append(process.getProcessName()).append(",").append(process.getStatus()).append(",").append(procDetail).append(",");
                WebProcessHistory history = process.getProcessHistory();
                int historyCounter = 0;
                for (Iterator histIter = history.getProcessStages().iterator(); histIter.hasNext();) {
                    historyCounter++;
                    WebProcessStage stage = (WebProcessStage) histIter.next();
                    String stageDetail = stage.toString().trim().replace("\n", "");
                    result.append("\n").append(lineCounter).append(",,,,,,,,").append(historyCounter).append(" - ").append(stageDetail);
                }
            }
            try {
                OutputFile out = new OutputFile(toDirectory + toFile, true);

                out.println(result.toString());
                out.close();
                JOptionPane.showMessageDialog(this, "Export complete.");
            } catch (IOException io) {
                JOptionPane.showMessageDialog(this, io.getMessage(), "Export failed", JOptionPane.ERROR_MESSAGE);
            }
            //}
        }
    }

    /**
     * This class generates a formatted StringBuffer, containing a detailed
     * description of the WebSetup content. a) List Existing Web Processes, each
     * with its properties b) List all the reporting structures c) List all the
     * Employee Selections with their formulas
     *
     * @includeEmployees Boolean property that, if set to true, returns a list
     * of all employees in each employee selection
     */
    private StringBuffer buildPrinterContentInHTML(boolean includeEmployees) {

        StringBuffer result = new StringBuffer();

        // Title
        result.append("<h2>WebSetup Configuration</h2>");
        result.append("<font size=\"-2\">Print Date:").append(new java.util.Date()).append("</font>");
        result.append("<br><font size=\"-2\">JAR Version:").append(za.co.ucs.accsys.webmodule.ESSVersion.getInstance().getJarVersion()).append("</font>");

        // Existing Web Processes
        result.append("<br><b>Current Web Processes:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        result.append("<tr><td>Name</td><td>Max Stage Age</td><td>Max Process Age</td>");
        result.append("    <td>Reporting Structure</td><td>Supervisor Process</td></tr>");
        Iterator wpIter = localWebProcessDefinitions.iterator();
        while (wpIter.hasNext()) {
            WebProcessDefinition wpd = (WebProcessDefinition) wpIter.next();
            result.append("<tr><td>").append(wpd.getName()).append("</td>");
            result.append("    <td>").append(wpd.getMaxStageAge()).append("</td>");
            result.append("    <td>").append(wpd.getMaxProcessAge()).append("</td>");
            if (wpd.getReportingStructure() == null) {
                result.append("    <td>-- None --</td>");
            } else {
                result.append("    <td>").append(wpd.getReportingStructure()).append("</td>");
            }
            if (wpd.getCannotInstantiateSelf()) {
                result.append("    <td>Y</td></tr>");
            } else {
                result.append("    <td>N</td></tr>");
            }
        }
        result.append("</font>");
        result.append("</table>");


        // Existing Reporting Structures
        result.append("<br><b>Reporting Structures:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        Iterator rsIter = localReportingStructures.iterator();
        while (rsIter.hasNext()) {
            ReportingStructure rep = (ReportingStructure) rsIter.next();
            result.append("<tr><td width=\"15%\"><b>").append(rep.toString()).append("</b></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("<td width=\"15%\"></td>");
            result.append("</tr>");

            // Get TopMost Levels
            ArrayList theTopNodes = rep.getTopNodes();
            Iterator nodeIter = theTopNodes.iterator();

            if (nodeIter.hasNext()) {
                // There can only be one TopNode
                EmployeeSelection topNode = (EmployeeSelection) nodeIter.next();
                result.append("<tr><td>").append(topNode.getName()).append("</td></tr>");

                LinkedList children1 = rep.getChildEmployeeSelections(topNode);
                Iterator children1Iter = children1.iterator();
                while (children1Iter.hasNext()) {
                    EmployeeSelection child1 = (EmployeeSelection) children1Iter.next();
                    result.append("<tr><td></td><td>").append(child1.getName()).append("</td></tr>");

                    LinkedList children2 = rep.getChildEmployeeSelections(child1);
                    Iterator children2Iter = children2.iterator();
                    while (children2Iter.hasNext()) {
                        EmployeeSelection child2 = (EmployeeSelection) children2Iter.next();
                        result.append("<tr><td></td><td></td><td>").append(child2.getName()).append("</td></tr>");

                        LinkedList children3 = rep.getChildEmployeeSelections(child2);
                        Iterator children3Iter = children3.iterator();
                        while (children3Iter.hasNext()) {
                            EmployeeSelection child3 = (EmployeeSelection) children3Iter.next();
                            result.append("<tr><td></td><td></td><td></td><td>").append(child3.getName()).append("</td></tr>");

                            LinkedList children4 = rep.getChildEmployeeSelections(child3);
                            Iterator children4Iter = children4.iterator();
                            while (children4Iter.hasNext()) {
                                EmployeeSelection child4 = (EmployeeSelection) children4Iter.next();
                                result.append("<tr><td></td><td></td><td></td><td></td><td>").append(child4.getName()).append("</td></tr>");

                                LinkedList children5 = rep.getChildEmployeeSelections(child4);
                                Iterator children5Iter = children5.iterator();
                                while (children5Iter.hasNext()) {
                                    EmployeeSelection child5 = (EmployeeSelection) children5Iter.next();
                                    result.append("<tr ><td></td><td></td><td></td><td></td><td></td><td>").append(child5.getName()).append("</td></tr>");
                                }
                            }
                        }
                    }
                }
            }
        }
        result.append("</font>");
        result.append("</table>");

        // Existing Employee Selections
        result.append("<br><b>Employee Selections:</b>");
        result.append("<table width=\"100%\" border=\"1\">");
        result.append("<font size=\"-3\">");
        result.append("<tr ><td><b>Name</b></td><td><b>Rule</b></td></tr>");
        Iterator esIter = localEmployeeSelections.iterator();
        while (esIter.hasNext()) {
            EmployeeSelectionRule es = (EmployeeSelectionRule) esIter.next();
            result.append("<tr ><td>").append(es.getName()).append("</td>");
            result.append("    <td>").append(es.getRule()).append("</td></tr>");
        }
        result.append("</font>");
        result.append("</table>");

        // Listing all employees in Employee Selections
        if (includeEmployees) {
            result.append("<br><b>Detailed Employee Selections:</b>");
            result.append("<table width=\"100%\" border=\"1\">");
            result.append("<font size=\"-3\">");
            result.append("<tr ><td><b>Rule Name</b></td><td><b>Employees</b></td></tr>");
            Iterator ruleIter = localEmployeeSelections.iterator();
            while (ruleIter.hasNext()) {
                EmployeeSelectionRule es = (EmployeeSelectionRule) ruleIter.next();
                result.append("<tr ><td>").append(es.getName()).append("</td>");
                result.append("    <td>");
                Iterator employeeIterator = SelectionInterpreter.getInstance().generateEmployeeList(es, false).iterator();
                int counter = 0;
                while (employeeIterator.hasNext()) {
                    za.co.ucs.accsys.peopleware.Employee employee = (za.co.ucs.accsys.peopleware.Employee) employeeIterator.next();
                    result.append(employee.toString());
                    // Lists 4 employees in one row before forcing a <br> in the HTML file
                    if (counter == 4) {
                        result.append("<br>");
                        counter = 0;
                    } else {
                        result.append("; ");
                    }
                    counter++;
                }
                result.append("</td></tr>");
            }
            result.append("</font>");
            result.append("</table>");
        }

        return result;

    }

    /**
     * When someone makes changes to rules in EmployeeSelections, the
     * ReportingStructure classes that uses those rules should be updated too
     */
    private void updateSelectionClassesInReportingStructures() {
        // First of all, we need to make sure that the objects stored in Employee Selections are the same ones stored in the links of Reporting Structures
        for (int repIdx = 0; repIdx < localReportingStructures.size(); repIdx++) {
            ReportingStructure rep = (ReportingStructure) localReportingStructures.get(repIdx);
            for (int lnkIdx = 0; lnkIdx < rep.getSelectionLinks().size(); lnkIdx++) {
                SelectionLink lnk = (SelectionLink) rep.getSelectionLinks().get(lnkIdx);
                String fromName = lnk.getReportsFrom().getName().trim();
                String toName = lnk.getReportsTo().getName().trim();
                // Find the EmployeeSelection with this name and update the selectionLink accordingly
                for (int esIdx = 0; esIdx < localEmployeeSelections.size(); esIdx++) {
                    if (((EmployeeSelectionRule) localEmployeeSelections.get(esIdx)).getName().trim().equals(fromName)) {
                        lnk.setReportsFrom((EmployeeSelectionRule) localEmployeeSelections.get(esIdx));
                    }
                    if (((EmployeeSelectionRule) localEmployeeSelections.get(esIdx)).getName().trim().equals(toName)) {
                        lnk.setReportsTo((EmployeeSelectionRule) localEmployeeSelections.get(esIdx));
                    }
                }
            }
        }
    }
    final Runnable updateValidationDisplay = new Runnable() {
        @Override
        public void run() {
            lblValidationProgress.setText(lblValidationProgress.getText());
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
        }
    };

    /**
     * Performing validation and corrections in the setup.
     *
     * @return true - changes made
     */
    private class Validation implements Runnable {

        @Override
        public void run() {
            boolean validatedChanges = false;

            // Visual Controls
            lblValidationProgress.setVisible(true);
            pbValidationProgress.setVisible(true);

            //***************************
            // Employee Selections
            //***************************

            // Let's test each selection during the saving
            LinkedList selectionsToDelete = new LinkedList();

            lblValidationProgress.setText("Testing Employee Selections...");
            pbValidationProgress.setMaximum(localEmployeeSelections.size());
            pbValidationProgress.setValue(0);
            int i = 1;
            Iterator iter = localEmployeeSelections.iterator();
            while (iter.hasNext()) {
                EmployeeSelectionRule selection = (EmployeeSelectionRule) iter.next();
                // Let's test each selection during the saving
                lblValidationProgress.setText("Testing Employee Selection " + (i + 1) + "/" + localEmployeeSelections.size() + ": " + selection.getName());
                pbValidationProgress.setValue(i++);
                pbValidationProgress.repaint();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException err) {
                }

                if (SelectionInterpreter.getInstance().generateEmployeeList(selection, false).isEmpty()) {
                    if (JOptionPane.showConfirmDialog(null, "The Employee Selection:'" + selection.getName() + "' did not return any values."
                            + "\nDo you wish to delete this rule?",
                            "Empty Employee Selection", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                            == JOptionPane.YES_OPTION) {
                        selectionsToDelete.add(selection);
                    }
                }
            }
            for (int del = 0; del < selectionsToDelete.size(); del++) {
                deleteEmployeeSelectionRule((EmployeeSelectionRule) selectionsToDelete.get(del));
                validatedChanges = true;
            }

            //***************************
            // Reporting Structures
            //***************************

            // First of all, we need to make sure that the objects stored in Employee Selections are the same ones stored in the links of Reporting Structures
            updateSelectionClassesInReportingStructures();


            lblValidationProgress.setText("Testing Reporting Structures...");
            pbValidationProgress.setMaximum(localReportingStructures.size());
            pbValidationProgress.setValue(0);

            boolean allOK = false;

            while (!allOK) {
                iter = localReportingStructures.iterator();
                i = 1;
                while (iter.hasNext()) {
                    allOK = true;  // Resetting the boolean status so that if everything evalautes correctly, it will not run through this loop more than once.

                    ReportingStructure structure = (ReportingStructure) iter.next();
                    lblValidationProgress.setText("Testing Reporting Structure " + (i + 1) + "/" + localReportingStructures.size() + ": " + structure.toString());
                    pbValidationProgress.setValue(i++);
                    pbValidationProgress.repaint();
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException err) {
                    }

                    //
                    // Check that names differ
                    //
                    LinkedList<ReportingStructure> localStructures = new LinkedList(localReportingStructures);
                    //System.out.println(i - 2);
                    localStructures.remove(i - 2);
                    for (ReportingStructure structureInList : localStructures) {
                        //System.out.println("structure hashCode: " + structure.hashCode());
                        //System.out.println("structureInList hashCode: " + structureInList.hashCode());
                        // If hashCodes are the same rename the current reporting structure
                        if (structure.hashCode() == structureInList.hashCode()) {
                            allOK = false;
                            // Ask user to change name
                            // Edit a Reporting Structure
                            // Edit dialog
                            String nameOfCurrentStructure = structure.getName();
                            String oldName = structureInList.getName();
                            String message = "The hashcodes produced for " + nameOfCurrentStructure + " and " + oldName + " are the same. Please rename " + nameOfCurrentStructure + " to avoid errors in the reporting structure.\nIf you do not rename the structure will be renamed automatically.";
                            String newStructureName = JOptionPane.showInputDialog(null, message, "Similar hashcode error", JOptionPane.WARNING_MESSAGE);
                            // Rename Structure
                            if ((newStructureName != null) && (newStructureName.trim().length() > 0)) {
                                //ReportingStructure existingReportingStructure = (ReportingStructure) lstExistingReportingStructures.getSelectedValue();
                                editReportingStructure(structure, newStructureName);
                                setContentsChanged(true);
                                // Restart validation process
                            } else {
                                // Auto-rename
                                newStructureName = i - 2 + "_renamed_" + nameOfCurrentStructure;
                                editReportingStructure(structure, newStructureName);
                                setContentsChanged(true);

                            }
                        }

                    }



                    //
                    // Check if there is more than one 'Top Node', in which case we have a problem
                    //
                    ArrayList topLinkNodes = structure.getTopNodes();
                    if (topLinkNodes.size() > 1) {
                        JOptionPane.showMessageDialog(null, "Reporting Structure '" + structure.toString() + "' contains more than one Top Node."
                                + "\nThis could have been caused by errenous deletion of selection formulas."
                                + "\nAll but the first of these nodes will be deleted.", "Multiple Top Nodes", JOptionPane.WARNING_MESSAGE);
                        for (int j = 1; j < topLinkNodes.size(); j++) {
                            EmployeeSelection topNodeSelection = (EmployeeSelection) topLinkNodes.get(j);

                            LinkedList links = structure.getSelectionLinks();
                            for (int k = 0; k < links.size(); k++) {
                                SelectionLink link = (SelectionLink) links.get(k);
                                // See if this link exist
                                EmployeeSelection selFrom = (EmployeeSelection) link.getReportsFrom();
                                EmployeeSelection selTo = (EmployeeSelection) link.getReportsTo();
                                if (selFrom.equals(topNodeSelection)) {
                                    deleteSelectionFromReportingStructure(structure, selFrom);
                                }
                                if (selTo.equals(topNodeSelection)) {
                                    deleteSelectionFromReportingStructure(structure, selTo);
                                }
                            }
                        }
                    }

                    //
                    // Check the individual links
                    //
                    LinkedList links = structure.getSelectionLinks();
                    for (int j = 0; j < links.size(); j++) {
                        SelectionLink link = (SelectionLink) links.get(j);
                        // See if this link exist
                        EmployeeSelection selFrom = (EmployeeSelection) link.getReportsFrom();
                        EmployeeSelection selTo = (EmployeeSelection) link.getReportsTo();
                        if (!localEmployeeSelections.contains(selFrom)) {
                            JOptionPane.showMessageDialog(null, "Reporting Structure '" + structure.toString() + "' contains a broken link."
                                    + "\nFrom: " + selFrom.toString() + "   To: " + selTo.toString()
                                    + "\nNOTE: This link will be removed.", "Broken link found", JOptionPane.WARNING_MESSAGE);
                            deleteSelectionFromReportingStructure(structure, selFrom);
                        }
                        if (!localEmployeeSelections.contains(selTo)) {
                            JOptionPane.showMessageDialog(null, "Reporting Structure '" + structure.toString() + "' contains a broken link."
                                    + "\nFrom: " + selFrom.toString() + "   To: " + selTo.toString()
                                    + "\nNOTE: This link will be removed.", "Broken link found", JOptionPane.WARNING_MESSAGE);
                            deleteSelectionFromReportingStructure(structure, selTo);
                        }
                    }
                    validatedChanges = true;

                }
            }

            //***************************
            // Web Processes
            //***************************
            for (int w = 0; w < localWebProcessDefinitions.size(); w++) {
                WebProcessDefinition def = (WebProcessDefinition) localWebProcessDefinitions.get(w);
                ReportingStructure structure = (ReportingStructure) def.getReportingStructure();
                for (int s = 0; s < localReportingStructures.size(); s++) {
                    ReportingStructure localStructure = (ReportingStructure) localReportingStructures.get(s);
                    if (localStructure.equals(structure)) {
                        if (localStructure.getSelectionLinks().size() != structure.getSelectionLinks().size()) {
                            def.setReportingStructure(localStructure);
                            validatedChanges = true;
                        }
                    }
                }
            }

            // Visual Controls
            lblValidationProgress.setVisible(false);
            pbValidationProgress.setVisible(false);


            if (validatedChanges) {
                JOptionPane.showMessageDialog(null, "Validation complete.  Changes were made so be sure to save before exiting the application.", "Validation Complete", JOptionPane.WARNING_MESSAGE);
                setContentsChanged(true);
                // Reset Contents Property
                initCustomComponents();
            } else {
                JOptionPane.showMessageDialog(null, "Validation complete.  No errors found.", "Validation Complete", JOptionPane.INFORMATION_MESSAGE);
            }



        }
    }

// Get a Copy of Reporting Structures and Employee Selections from the FileContainer
    private void saveToFileContainer() {
        // Employee Selections
        ArrayList employeeSelections = fileContainer.getEmployeeSelections();
        employeeSelections.clear();

        updateSelectionClassesInReportingStructures();
        for (int i = 0; i < localEmployeeSelections.size(); i++) {
            // Let's test each selection during the saving
            employeeSelections.add(localEmployeeSelections.get(i));
        }
        fileContainer.setEmployeeSelections(employeeSelections);

        // Reporting Structures
        ArrayList reportingStructures = fileContainer.getReportingStructures();
        reportingStructures.clear();


        for (int j = 0; j < localReportingStructures.size(); j++) {
            reportingStructures.add(localReportingStructures.get(j));
        }
        fileContainer.setReportingStructures(reportingStructures);

        // Process Definitions
        ArrayList processDefinitions = fileContainer.getWebProcessDefinitions();
        processDefinitions.clear();


        for (int k = 0; k < localWebProcessDefinitions.size(); k++) {
            processDefinitions.add(localWebProcessDefinitions.get(k));
            recursiveUpdateWebProcessDefinition((WebProcessDefinition) localWebProcessDefinitions.get(k));
        }
        fileContainer.setWebProcessDefinitions(processDefinitions);
        fileContainer.saveWebSetupDetail();

        // Reset Contents Property
        setContentsChanged(false);
        JOptionPane.showMessageDialog(this, "Changes saved successfully.", "Saved successfully", JOptionPane.INFORMATION_MESSAGE);

    }

    /**
     * Initialize our own components - not those generated by NetBeans
     */
    public void initCustomComponents() {

        initSelectionFormulaTab();
        initReportingStructureTab();
        initWebProcessDefinitionsTab();

        // Reset current GUI Action
        setupScreensAndButtons();
    }

    /**
     * Updates the currently selected WebProcessDefinition when changes have
     * been made to selections or structures elsewhere
     */
    private void recursiveUpdateWebProcessDefinition(WebProcessDefinition webProcessDefinition) {
        // Set Max Process Age
        try {
            webProcessDefinition.setMaxProcessAge(webProcessDefinition.getMaxProcessAge());
        } catch (NumberFormatException e) {
            edtProcessAge.setText("24");
            webProcessDefinition.setMaxProcessAge(new Integer(edtProcessAge.getText()).intValue());

        }
        // Set Max Stage Age
        try {
            webProcessDefinition.setMaxStageAge(webProcessDefinition.getMaxStageAge());
        } catch (NumberFormatException e) {
            edtStageAge.setText("3");
            webProcessDefinition.setMaxStageAge(new Integer(edtStageAge.getText()).intValue());

        }


        //System.out.println(webProcessDefinition + ": " + webProcessDefinition.getReportingStructure());
        if (webProcessDefinition.getReportingStructure() != null) {
            String hashString = "" + webProcessDefinition.getReportingStructure().hashCode();
            webProcessDefinition.setReportingStructure((ReportingStructure) fileContainer.getReportingStructure(hashString));
            // Set Reporting Structure
            // Set Min Start Day
            try {
                webProcessDefinition.setActiveFromDayOfMonth(webProcessDefinition.getActiveFromDayOfMonth());
            } catch (NumberFormatException e) {
                edtAvailableFromDayOfMonth.setText("0");
                webProcessDefinition.setActiveFromDayOfMonth(new Integer(edtAvailableFromDayOfMonth.getText()).intValue());
            }
            // Set Latest Start Day
            try {
                webProcessDefinition.setActiveToDayOfMonth(webProcessDefinition.getActiveFromDayOfMonth());
            } catch (NumberFormatException e) {
                edtAvailableToDayOfMonth.setText("0");
                webProcessDefinition.setActiveToDayOfMonth(new Integer(edtAvailableToDayOfMonth.getText()).intValue());
            }

        }

    }

    /**
     * Updates the currently selected WebProcessDefinition with the properties
     * that had been changed on the screen
     */
    private void updateWebProcessDefinition(WebProcessDefinition webProcessDefinition) {
        // Set Max Process Age
        try {
            webProcessDefinition.setMaxProcessAge(new Integer(edtProcessAge.getText()).intValue());
        } catch (NumberFormatException e) {
            edtProcessAge.setText("24");
            webProcessDefinition.setMaxProcessAge(new Integer(edtProcessAge.getText()).intValue());

        }
        // Set Max Stage Age
        try {
            webProcessDefinition.setMaxStageAge(new Integer(edtStageAge.getText()).intValue());
        } catch (NumberFormatException e) {
            edtStageAge.setText("3");
            webProcessDefinition.setMaxStageAge(new Integer(edtStageAge.getText()).intValue());

        }
        // Set Reporting Structure
        webProcessDefinition.setReportingStructure((ReportingStructure) cbbReportingStructure.getSelectedItem());
        // Set Min Start Day
        try {
            webProcessDefinition.setActiveFromDayOfMonth(new Integer(edtAvailableFromDayOfMonth.getText()).intValue());
        } catch (NumberFormatException e) {
            edtAvailableFromDayOfMonth.setText("0");
            webProcessDefinition.setActiveFromDayOfMonth(new Integer(edtAvailableFromDayOfMonth.getText()).intValue());
        }
        // Set Latest Start Day
        try {
            webProcessDefinition.setActiveToDayOfMonth(new Integer(edtAvailableToDayOfMonth.getText()).intValue());
        } catch (NumberFormatException e) {
            edtAvailableToDayOfMonth.setText("0");
            webProcessDefinition.setActiveToDayOfMonth(new Integer(edtAvailableToDayOfMonth.getText()).intValue());
        }
    }

    private class EmployeeSelectionRuleExtended {

        private EmployeeSelectionRuleExtended(EmployeeSelectionRule selection, String includedIn) {
            this.includedIn = includedIn;
            this.aRule = selection;
        }

        @Override
        public String toString() {
            return aRule.getName() + " [" + includedIn + "]";
        }

        public EmployeeSelectionRule getEmployeeSelection() {
            return aRule;
        }
        private String includedIn = "";
        private EmployeeSelectionRule aRule = null;
    }

    private HashSet getStructuresUsingSelection(EmployeeSelection es) {
        HashSet result = new HashSet();
        for (int i = 0; i < localReportingStructures.size(); i++) {
            ReportingStructure s = (ReportingStructure) localReportingStructures.get(i);
            for (int j = 0; j < s.getSelectionLinks().size(); j++) {
                SelectionLink l = (SelectionLink) s.getSelectionLinks().get(j);
                if ((l.getReportsFrom().equals(es)) || (l.getReportsTo().equals(es))) {
                    result.add(s);
                }
            }
        }
        return result;
    }

    /**
     * Populates the model containing the EmployeeSelectionRules
     */
    private void populateEmployeeSelectionList(DefaultListModel model) {
        // Clear list
        model.clear();

        // Add current selections
        localEmployeeSelections = sortList(localEmployeeSelections);

        Iterator iter = localEmployeeSelections.iterator();


        while (iter.hasNext()) {
            EmployeeSelectionRule es = (EmployeeSelectionRule) iter.next();
            HashSet structuresUsingThisSelection = getStructuresUsingSelection(es);
            String contains = "";
            Iterator iterR = structuresUsingThisSelection.iterator();
            while (iterR.hasNext()) {
                ReportingStructure r = (ReportingStructure) iterR.next();
                if (contains.trim().length() == 0) {
                    contains += r;
                } else {
                    contains += "," + r;
                }
            }

            EmployeeSelectionRuleExtended ex = new EmployeeSelectionRuleExtended(es, contains.trim());
            model.addElement(ex);
        }
    }

    /**
     * Populates the model containing the ReportingStructures
     */
    private void populateReportingStructureList(DefaultListModel model) {
        // Clear list
        model.clear();

        // Add current selections
        localReportingStructures = sortList(localReportingStructures);

        Iterator iter = localReportingStructures.iterator();

        while (iter.hasNext()) {
            ReportingStructure rs = (ReportingStructure) iter.next();
            // Only add those that are not yet listed in the TreeView
            model.addElement(rs);
        }
    }

    /**
     * Populates the model containing the WebProcessDefinitions
     */
    private void populateWebProcessDefinitionsList(DefaultListModel model) {
        // Clear list
        model.clear();

        // Add current selections
        Iterator iter = localWebProcessDefinitions.iterator();

        while (iter.hasNext()) {
            WebProcessDefinition wpd = (WebProcessDefinition) iter.next();
            model.addElement(wpd);
        }
    }

    /**
     * Returns the EmployeeSelection from the container that has the given name.
     */
    public EmployeeSelection getEmployeeSelection(String name) {
        for (int i = 0; i
                < localEmployeeSelections.size(); i++) {
            if (((EmployeeSelection) localEmployeeSelections.get(i)).getName().compareTo(name) == 0) {
                return ((EmployeeSelection) localEmployeeSelections.get(i));
            }
        }
        return null;
    }

    /**
     * Creates a DefaultTreeModel for the given ReportingStructure
     */
    private DefaultTreeModel createTreeModel(ReportingStructure structure) {
        // Get Top Selection
        ArrayList theTopNodes = structure.getTopNodes();
        EmployeeSelectionRule topSelection;

        if (theTopNodes.isEmpty()) {
            topSelection = (EmployeeSelectionRule) getTopNode(structure);

            if (topSelection == null) {
                return null;
            }
        } else {
            topSelection = (EmployeeSelectionRule) theTopNodes.get(0);
        }

        // Top Selection of this Structure
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(topSelection);
        // Create the TreeModel
        DefaultTreeModel model = new DefaultTreeModel(root);
        buildUpTreeModelNode(model, root, structure);
        return model;
    }

    /**
     * Each reporting structure must have one top node. Links between nodes
     * obviously requires more than one node. It is thus necessary to store the
     * top node, in order to create a link the next time an employee selection
     * is dropped onto the treeview
     */
    private EmployeeSelection getTopNode(ReportingStructure structure) {
        EmployeeSelection result = null;

        for (int i = 0; i
                < topNodes.size(); i++) {
            if (((TopNode) topNodes.get(i)).structure == structure) {
                return (((TopNode) topNodes.get(i)).selection);
            }
        }
        return result;
    }

    /**
     * Adds a new Top Node for this reporting structure
     */
    private void setTopNode(ReportingStructure structure, EmployeeSelection selection) {
        // Remove existing node - if any
        for (int i = 0; i
                < topNodes.size(); i++) {
            if (((TopNode) topNodes.get(i)).structure == structure) {
                topNodes.remove(i);
                break;
            }
        }
        // Add new Node
        TopNode newNode = new TopNode(structure, selection);
        topNodes.add(newNode);

    }

// Recursively build up the tree Model
    private DefaultTreeModel buildUpTreeModelNode(DefaultTreeModel model, DefaultMutableTreeNode currentNode, ReportingStructure structure) {
        Object nodeObject = currentNode.getUserObject();
        LinkedList selections = structure.getChildEmployeeSelections((EmployeeSelection) nodeObject);

        // For this structure, build the tree
        for (int i = 0; i < selections.size(); i++) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(selections.get(i));
            currentNode.add(node);
            model = buildUpTreeModelNode(model, node, structure);
        }

        return model;
    }

    /**
     * Expands all nodes in a JTree.
     */
    public static void expandJTree(javax.swing.JTree tree) {
        for (int row = 0; row
                < tree.getRowCount(); row++) {
            tree.expandRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPane = new javax.swing.JTabbedPane();
        panWebProcesses = new javax.swing.JPanel();
        splProcesses = new javax.swing.JSplitPane();
        panWebProcessesList = new javax.swing.JPanel();
        scrlSelections2 = new javax.swing.JScrollPane();
        lstExistingWebProcessDefinitions = new javax.swing.JList();
        panSelection2 = new javax.swing.JPanel();
        lblSelections2 = new javax.swing.JLabel();
        panProcessProperties = new javax.swing.JPanel();
        lblProcessName = new javax.swing.JLabel();
        edtProcessName = new javax.swing.JTextField();
        lblProcessDescription = new javax.swing.JLabel();
        edtProcessDescription = new javax.swing.JTextField();
        lblReportingStructure = new javax.swing.JLabel();
        cbbReportingStructure = new javax.swing.JComboBox();
        lblProcessAge = new javax.swing.JLabel();
        edtProcessAge = new javax.swing.JTextField();
        lblStageAge = new javax.swing.JLabel();
        edtStageAge = new javax.swing.JTextField();
        cbSupervisorProcess = new javax.swing.JCheckBox();
        lblSpacer2 = new javax.swing.JLabel();
        lblHeading = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblAvailableFromDayOfMonth = new javax.swing.JLabel();
        edtAvailableFromDayOfMonth = new javax.swing.JTextField();
        lblAvailableToDayOfMonth = new javax.swing.JLabel();
        edtAvailableToDayOfMonth = new javax.swing.JTextField();
        cbNotifyActivation = new javax.swing.JCheckBox();
        cbNotifyDeactivation = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        panLeftBottom2 = new javax.swing.JPanel();
        btnApplyWebProcessChanges = new javax.swing.JButton();
        panReportingStructures = new javax.swing.JPanel();
        splReportingStructures = new javax.swing.JSplitPane();
        panReportingStructureList = new javax.swing.JPanel();
        panLeftBottom1 = new javax.swing.JPanel();
        btnNewReportingStructure = new javax.swing.JButton();
        btnEditReportingStructure = new javax.swing.JButton();
        btnDeleteReportingStructure = new javax.swing.JButton();
        scrlSelections1 = new javax.swing.JScrollPane();
        lstExistingReportingStructures = new javax.swing.JList();
        panSelection1 = new javax.swing.JPanel();
        lblSelections1 = new javax.swing.JLabel();
        panReportingStructureDetail = new javax.swing.JPanel();
        panRightBottom1 = new javax.swing.JPanel();
        panOKCANCEL1 = new javax.swing.JPanel();
        panRightCentre1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        panReportingStructureEdit = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnExpand = new javax.swing.JButton();
        btnCollapse = new javax.swing.JButton();
        lblSpacer = new javax.swing.JLabel();
        lblCurrentReportingStructure = new javax.swing.JLabel();
        treeReportingStructure = new javax.swing.JTree();
        jScrollPane2 = new javax.swing.JScrollPane();
        panEmployeeSelectionEdit = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lstExistingEmployeeSelectionsInTreeView = new javax.swing.JList();
        panSelectionFormulas = new javax.swing.JPanel();
        splMain = new javax.swing.JSplitPane();
        panEmployeeSelectionList = new javax.swing.JPanel();
        panLeftBottom = new javax.swing.JPanel();
        btnNewEmployeeSelection = new javax.swing.JButton();
        btnEditEmployeeSelection = new javax.swing.JButton();
        btnDeleteEmployeeSelection = new javax.swing.JButton();
        btnCopyEmployeeSelection = new javax.swing.JButton();
        scrlSelections = new javax.swing.JScrollPane();
        lstExistingEmployeeSelections = new javax.swing.JList();
        panSelection = new javax.swing.JPanel();
        lblSelections = new javax.swing.JLabel();
        panEmployeeSelectionRule = new javax.swing.JPanel();
        panRightTop = new javax.swing.JPanel();
        panFormulaEditor = new javax.swing.JPanel();
        lblFilter = new javax.swing.JLabel();
        cbbFilters = new javax.swing.JComboBox();
        lblCompare = new javax.swing.JLabel();
        cbbCompare = new javax.swing.JComboBox();
        btnTest = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        panRename = new javax.swing.JPanel();
        lblEditor = new javax.swing.JLabel();
        txtEmployeeSelectionName = new javax.swing.JTextField();
        panRightBottom = new javax.swing.JPanel();
        panOKCANCEL = new javax.swing.JPanel();
        btnCancelEmployeeSelectionFormula = new javax.swing.JButton();
        btnApplyEmployeeSelectionFormula = new javax.swing.JButton();
        panRightCentre = new javax.swing.JPanel();
        splitEditor = new javax.swing.JSplitPane();
        spEditor = new javax.swing.JScrollPane();
        textEmployees = new javax.swing.JTextArea();
        spEmployees = new javax.swing.JScrollPane();
        editorPane = new javax.swing.JTextPane();
        panValidation = new javax.swing.JPanel();
        lblValidationProgress = new javax.swing.JLabel();
        pbValidationProgress = new javax.swing.JProgressBar();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        saveMenuItem = new javax.swing.JMenuItem();
        printMenuItem = new javax.swing.JMenuItem();
        exportMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Peopleware - Web Configuration");
        setName("frmMain"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        tabPane.setDoubleBuffered(true);

        panWebProcesses.setLayout(new java.awt.BorderLayout());

        splProcesses.setDividerLocation(200);
        splProcesses.setDoubleBuffered(true);
        splProcesses.setLastDividerLocation(200);

        panWebProcessesList.setMinimumSize(new java.awt.Dimension(200, 400));
        panWebProcessesList.setPreferredSize(new java.awt.Dimension(300, 400));
        panWebProcessesList.setLayout(new java.awt.BorderLayout());

        scrlSelections2.setPreferredSize(new java.awt.Dimension(300, 132));

        lstExistingWebProcessDefinitions.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstExistingWebProcessDefinitionsValueChanged(evt);
            }
        });
        scrlSelections2.setViewportView(lstExistingWebProcessDefinitions);

        panWebProcessesList.add(scrlSelections2, java.awt.BorderLayout.CENTER);

        lblSelections2.setText("Available Web Processes");
        panSelection2.add(lblSelections2);

        panWebProcessesList.add(panSelection2, java.awt.BorderLayout.NORTH);

        splProcesses.setLeftComponent(panWebProcessesList);

        panProcessProperties.setLayout(new java.awt.GridLayout(11, 2, 5, 20));

        lblProcessName.setText("Process Name");
        lblProcessName.setToolTipText("");
        lblProcessName.setAlignmentX(2.0F);
        lblProcessName.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        panProcessProperties.add(lblProcessName);

        edtProcessName.setEditable(false);
        panProcessProperties.add(edtProcessName);

        lblProcessDescription.setText("Description");
        panProcessProperties.add(lblProcessDescription);

        edtProcessDescription.setEditable(false);
        edtProcessDescription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtProcessDescriptionActionPerformed(evt);
            }
        });
        panProcessProperties.add(edtProcessDescription);

        lblReportingStructure.setText("Reporting Structure");
        lblReportingStructure.setToolTipText("Select which reporting structure should be used for this process");
        panProcessProperties.add(lblReportingStructure);

        cbbReportingStructure.setToolTipText("Select which reporting structure should be used for this process");
        cbbReportingStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbReportingStructureActionPerformed(evt);
            }
        });
        panProcessProperties.add(cbbReportingStructure);

        lblProcessAge.setText("Max.Duration of Complete Process (Hours)");
        lblProcessAge.setToolTipText("How long before this process gets cancelled altogether?");
        panProcessProperties.add(lblProcessAge);

        edtProcessAge.setToolTipText("How long before this process gets cancelled altogether?");
        edtProcessAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtProcessAgeActionPerformed(evt);
            }
        });
        edtProcessAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                edtProcessAgeKeyTyped(evt);
            }
        });
        panProcessProperties.add(edtProcessAge);

        lblStageAge.setText("Max. Duration of Stage (Hours)");
        lblStageAge.setToolTipText("How long before this process gets escalated?");
        panProcessProperties.add(lblStageAge);

        edtStageAge.setToolTipText("How long before this process gets escalated?");
        edtStageAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtStageAgeActionPerformed(evt);
            }
        });
        edtStageAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                edtStageAgeKeyTyped(evt);
            }
        });
        panProcessProperties.add(edtStageAge);

        cbSupervisorProcess.setText("Supervisor Process");
        cbSupervisorProcess.setToolTipText("If ticked, this process cannot be started by the employee himself");
        cbSupervisorProcess.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        cbSupervisorProcess.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbSupervisorProcessItemStateChanged(evt);
            }
        });
        panProcessProperties.add(cbSupervisorProcess);
        panProcessProperties.add(lblSpacer2);

        lblHeading.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHeading.setText("Date Constraints");
        panProcessProperties.add(lblHeading);
        panProcessProperties.add(jPanel1);

        lblAvailableFromDayOfMonth.setText("Only available from this day [Leave 0 if not applicable]");
        lblAvailableFromDayOfMonth.setToolTipText("If value greater than 0, the ESS process will only be available for use on or after this nth day of the month.");
        panProcessProperties.add(lblAvailableFromDayOfMonth);

        edtAvailableFromDayOfMonth.setText("0");
        edtAvailableFromDayOfMonth.setToolTipText("If value greater than 0, the ESS process will only be available for use on or after this nth day of the month.");
        edtAvailableFromDayOfMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtAvailableFromDayOfMonthActionPerformed(evt);
            }
        });
        edtAvailableFromDayOfMonth.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                edtAvailableFromDayOfMonthKeyTyped(evt);
            }
        });
        panProcessProperties.add(edtAvailableFromDayOfMonth);

        lblAvailableToDayOfMonth.setText("Only available tothis day [Leave 0 if not applicable]");
        lblAvailableToDayOfMonth.setToolTipText("If value greater than 0, the ESS process will not be available for use after this nth day of the month.");
        panProcessProperties.add(lblAvailableToDayOfMonth);

        edtAvailableToDayOfMonth.setText("0");
        edtAvailableToDayOfMonth.setToolTipText("If value greater than 0, the ESS process will not be available for use after this nth day of the month.");
        edtAvailableToDayOfMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtAvailableToDayOfMonthActionPerformed(evt);
            }
        });
        edtAvailableToDayOfMonth.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                edtAvailableToDayOfMonthKeyTyped(evt);
            }
        });
        panProcessProperties.add(edtAvailableToDayOfMonth);

        cbNotifyActivation.setText("Notify of pending activation");
        cbNotifyActivation.setToolTipText("Processes which only activates at a given day of the month can inform users when it becomes active");
        cbNotifyActivation.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        cbNotifyActivation.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbNotifyActivationItemStateChanged(evt);
            }
        });
        panProcessProperties.add(cbNotifyActivation);

        cbNotifyDeactivation.setText("Notify of pending deactivation");
        cbNotifyDeactivation.setToolTipText("Processes which de-activates on a given day of the month can inform users prior to its deactivation");
        cbNotifyDeactivation.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        cbNotifyDeactivation.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbNotifyDeactivationItemStateChanged(evt);
            }
        });
        panProcessProperties.add(cbNotifyDeactivation);
        panProcessProperties.add(jPanel2);

        panLeftBottom2.setLayout(new java.awt.GridLayout(1, 0));

        btnApplyWebProcessChanges.setText("Apply Changes");
        btnApplyWebProcessChanges.setToolTipText("Create a new Reporting Structure");
        btnApplyWebProcessChanges.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApplyWebProcessChanges.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApplyWebProcessChangesActionPerformed(evt);
            }
        });
        panLeftBottom2.add(btnApplyWebProcessChanges);

        panProcessProperties.add(panLeftBottom2);

        splProcesses.setRightComponent(panProcessProperties);

        panWebProcesses.add(splProcesses, java.awt.BorderLayout.CENTER);

        tabPane.addTab("Web Processes", panWebProcesses);

        panReportingStructures.setLayout(new java.awt.BorderLayout());

        splReportingStructures.setDividerLocation(200);
        splReportingStructures.setLastDividerLocation(200);

        panReportingStructureList.setMinimumSize(new java.awt.Dimension(200, 400));
        panReportingStructureList.setPreferredSize(new java.awt.Dimension(300, 400));
        panReportingStructureList.setLayout(new java.awt.BorderLayout());

        panLeftBottom1.setLayout(new java.awt.GridLayout(1, 0));

        btnNewReportingStructure.setText("New");
        btnNewReportingStructure.setToolTipText("Create a new Reporting Structure");
        btnNewReportingStructure.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnNewReportingStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewReportingStructureActionPerformed(evt);
            }
        });
        panLeftBottom1.add(btnNewReportingStructure);

        btnEditReportingStructure.setText("Rename");
        btnEditReportingStructure.setToolTipText("Edit the selected Reporting Structure");
        btnEditReportingStructure.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnEditReportingStructure.setMinimumSize(new java.awt.Dimension(15, 19));
        btnEditReportingStructure.setPreferredSize(new java.awt.Dimension(25, 19));
        btnEditReportingStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditReportingStructureActionPerformed(evt);
            }
        });
        panLeftBottom1.add(btnEditReportingStructure);

        btnDeleteReportingStructure.setText("Delete");
        btnDeleteReportingStructure.setToolTipText("Delete the selected Reporting Structure");
        btnDeleteReportingStructure.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnDeleteReportingStructure.setMinimumSize(new java.awt.Dimension(15, 19));
        btnDeleteReportingStructure.setPreferredSize(new java.awt.Dimension(25, 19));
        btnDeleteReportingStructure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteReportingStructureActionPerformed(evt);
            }
        });
        panLeftBottom1.add(btnDeleteReportingStructure);

        panReportingStructureList.add(panLeftBottom1, java.awt.BorderLayout.SOUTH);

        scrlSelections1.setPreferredSize(new java.awt.Dimension(300, 132));

        lstExistingReportingStructures.setDoubleBuffered(true);
        lstExistingReportingStructures.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstExistingReportingStructuresValueChanged(evt);
            }
        });
        lstExistingReportingStructures.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lstExistingReportingStructuresKeyPressed(evt);
            }
        });
        scrlSelections1.setViewportView(lstExistingReportingStructures);

        panReportingStructureList.add(scrlSelections1, java.awt.BorderLayout.CENTER);

        lblSelections1.setText("Existing Reporting Structures");
        panSelection1.add(lblSelections1);

        panReportingStructureList.add(panSelection1, java.awt.BorderLayout.NORTH);

        splReportingStructures.setLeftComponent(panReportingStructureList);

        panReportingStructureDetail.setEnabled(false);
        panReportingStructureDetail.setFocusable(false);
        panReportingStructureDetail.setMinimumSize(new java.awt.Dimension(250, 400));
        panReportingStructureDetail.setPreferredSize(new java.awt.Dimension(290, 400));
        panReportingStructureDetail.setLayout(new java.awt.BorderLayout());

        panRightBottom1.setLayout(new java.awt.BorderLayout());

        panOKCANCEL1.setLayout(new java.awt.GridLayout(1, 0, 5, 10));
        panRightBottom1.add(panOKCANCEL1, java.awt.BorderLayout.SOUTH);

        panReportingStructureDetail.add(panRightBottom1, java.awt.BorderLayout.SOUTH);

        panRightCentre1.setLayout(new java.awt.BorderLayout());

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.N_RESIZE_CURSOR));

        panReportingStructureEdit.setLayout(new java.awt.BorderLayout());

        jPanel3.setMaximumSize(new java.awt.Dimension(20, 20));
        jPanel3.setLayout(new java.awt.BorderLayout());

        btnExpand.setText("Expand");
        btnExpand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpandActionPerformed(evt);
            }
        });
        jPanel4.add(btnExpand);

        btnCollapse.setText("Collapse");
        btnCollapse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCollapseActionPerformed(evt);
            }
        });
        jPanel4.add(btnCollapse);

        lblSpacer.setText("                           ");
        jPanel4.add(lblSpacer);

        lblCurrentReportingStructure.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCurrentReportingStructure.setText("Edit the current Reporting Structure");
        jPanel4.add(lblCurrentReportingStructure);

        jPanel3.add(jPanel4, java.awt.BorderLayout.PAGE_END);

        panReportingStructureEdit.add(jPanel3, java.awt.BorderLayout.NORTH);

        treeReportingStructure.setDoubleBuffered(true);
        treeReportingStructure.setDragEnabled(true);
        treeReportingStructure.setEditable(true);
        treeReportingStructure.setRowHeight(20);
        treeReportingStructure.setShowsRootHandles(true);
        treeReportingStructure.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                treeReportingStructureKeyPressed(evt);
            }
        });
        panReportingStructureEdit.add(treeReportingStructure, java.awt.BorderLayout.CENTER);

        jScrollPane1.setViewportView(panReportingStructureEdit);

        jSplitPane1.setLeftComponent(jScrollPane1);

        panEmployeeSelectionEdit.setLayout(new java.awt.BorderLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Existing Employee Selections");
        panEmployeeSelectionEdit.add(jLabel1, java.awt.BorderLayout.NORTH);

        lstExistingEmployeeSelectionsInTreeView.setDoubleBuffered(true);
        lstExistingEmployeeSelectionsInTreeView.setDragEnabled(true);
        panEmployeeSelectionEdit.add(lstExistingEmployeeSelectionsInTreeView, java.awt.BorderLayout.CENTER);

        jScrollPane2.setViewportView(panEmployeeSelectionEdit);

        jSplitPane1.setRightComponent(jScrollPane2);

        panRightCentre1.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        panReportingStructureDetail.add(panRightCentre1, java.awt.BorderLayout.CENTER);

        splReportingStructures.setRightComponent(panReportingStructureDetail);

        panReportingStructures.add(splReportingStructures, java.awt.BorderLayout.CENTER);

        tabPane.addTab("Reporting Structures", panReportingStructures);

        panSelectionFormulas.setLayout(new java.awt.BorderLayout());

        splMain.setDividerLocation(200);
        splMain.setLastDividerLocation(200);

        panEmployeeSelectionList.setMinimumSize(new java.awt.Dimension(200, 400));
        panEmployeeSelectionList.setPreferredSize(new java.awt.Dimension(300, 400));
        panEmployeeSelectionList.setLayout(new java.awt.BorderLayout());

        panLeftBottom.setLayout(new java.awt.GridLayout(1, 0));

        btnNewEmployeeSelection.setMnemonic('N');
        btnNewEmployeeSelection.setText("New");
        btnNewEmployeeSelection.setToolTipText("Create a new Employee Selection");
        btnNewEmployeeSelection.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnNewEmployeeSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewEmployeeSelectionActionPerformed(evt);
            }
        });
        panLeftBottom.add(btnNewEmployeeSelection);

        btnEditEmployeeSelection.setMnemonic('E');
        btnEditEmployeeSelection.setText("Edit");
        btnEditEmployeeSelection.setToolTipText("Edit selected Employee Selection");
        btnEditEmployeeSelection.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnEditEmployeeSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditEmployeeSelectionActionPerformed(evt);
            }
        });
        panLeftBottom.add(btnEditEmployeeSelection);

        btnDeleteEmployeeSelection.setMnemonic('D');
        btnDeleteEmployeeSelection.setText("Delete");
        btnDeleteEmployeeSelection.setToolTipText("Delete selected Employee Selection");
        btnDeleteEmployeeSelection.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnDeleteEmployeeSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteEmployeeSelectionActionPerformed(evt);
            }
        });
        panLeftBottom.add(btnDeleteEmployeeSelection);

        btnCopyEmployeeSelection.setMnemonic('C');
        btnCopyEmployeeSelection.setText("Copy");
        btnCopyEmployeeSelection.setToolTipText("Copy selected Employee Selection");
        btnCopyEmployeeSelection.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnCopyEmployeeSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCopyEmployeeSelectionActionPerformed(evt);
            }
        });
        panLeftBottom.add(btnCopyEmployeeSelection);

        panEmployeeSelectionList.add(panLeftBottom, java.awt.BorderLayout.SOUTH);

        scrlSelections.setPreferredSize(new java.awt.Dimension(300, 132));

        lstExistingEmployeeSelections.setDoubleBuffered(true);
        lstExistingEmployeeSelections.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstExistingEmployeeSelectionsMouseClicked(evt);
            }
        });
        lstExistingEmployeeSelections.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstExistingEmployeeSelectionsValueChanged(evt);
            }
        });
        lstExistingEmployeeSelections.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                lstExistingEmployeeSelectionsKeyPressed(evt);
            }
        });
        scrlSelections.setViewportView(lstExistingEmployeeSelections);

        panEmployeeSelectionList.add(scrlSelections, java.awt.BorderLayout.CENTER);

        lblSelections.setText("Existing Employee Selections");
        panSelection.add(lblSelections);

        panEmployeeSelectionList.add(panSelection, java.awt.BorderLayout.NORTH);

        splMain.setLeftComponent(panEmployeeSelectionList);

        panEmployeeSelectionRule.setEnabled(false);
        panEmployeeSelectionRule.setFocusable(false);
        panEmployeeSelectionRule.setMinimumSize(new java.awt.Dimension(250, 400));
        panEmployeeSelectionRule.setPreferredSize(new java.awt.Dimension(290, 400));
        panEmployeeSelectionRule.setLayout(new java.awt.BorderLayout());

        panRightTop.setLayout(new java.awt.BorderLayout());

        panFormulaEditor.setMinimumSize(new java.awt.Dimension(198, 120));
        panFormulaEditor.setLayout(new java.awt.GridLayout(3, 2));

        lblFilter.setDisplayedMnemonic('F');
        lblFilter.setLabelFor(cbbFilters);
        lblFilter.setText("Filter");
        panFormulaEditor.add(lblFilter);

        cbbFilters.setLightWeightPopupEnabled(false);
        cbbFilters.setOpaque(false);
        panFormulaEditor.add(cbbFilters);

        lblCompare.setDisplayedMnemonic('C');
        lblCompare.setLabelFor(cbbCompare);
        lblCompare.setText("Compare");
        panFormulaEditor.add(lblCompare);
        panFormulaEditor.add(cbbCompare);

        btnTest.setMnemonic('T');
        btnTest.setText("Test Formula");
        btnTest.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestActionPerformed(evt);
            }
        });
        panFormulaEditor.add(btnTest);

        btnAdd.setMnemonic('A');
        btnAdd.setText("Add default formula");
        btnAdd.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        panFormulaEditor.add(btnAdd);

        panRightTop.add(panFormulaEditor, java.awt.BorderLayout.CENTER);

        panRename.setLayout(new java.awt.GridLayout(1, 0));

        lblEditor.setText("Setup formula for Employee Selection");
        panRename.add(lblEditor);
        panRename.add(txtEmployeeSelectionName);

        panRightTop.add(panRename, java.awt.BorderLayout.NORTH);

        panEmployeeSelectionRule.add(panRightTop, java.awt.BorderLayout.NORTH);

        panRightBottom.setLayout(new java.awt.BorderLayout());

        panOKCANCEL.setLayout(new java.awt.GridLayout(1, 0, 5, 10));

        btnCancelEmployeeSelectionFormula.setMnemonic('C');
        btnCancelEmployeeSelectionFormula.setText("Cancel");
        btnCancelEmployeeSelectionFormula.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnCancelEmployeeSelectionFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelEmployeeSelectionFormulaActionPerformed(evt);
            }
        });
        panOKCANCEL.add(btnCancelEmployeeSelectionFormula);

        btnApplyEmployeeSelectionFormula.setMnemonic('p');
        btnApplyEmployeeSelectionFormula.setText("Apply");
        btnApplyEmployeeSelectionFormula.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApplyEmployeeSelectionFormula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApplyEmployeeSelectionFormulaActionPerformed(evt);
            }
        });
        panOKCANCEL.add(btnApplyEmployeeSelectionFormula);

        panRightBottom.add(panOKCANCEL, java.awt.BorderLayout.SOUTH);

        panEmployeeSelectionRule.add(panRightBottom, java.awt.BorderLayout.SOUTH);

        panRightCentre.setLayout(new java.awt.BorderLayout());

        splitEditor.setDividerLocation(100);
        splitEditor.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        textEmployees.setLineWrap(true);
        textEmployees.setDoubleBuffered(true);
        spEditor.setViewportView(textEmployees);

        splitEditor.setRightComponent(spEditor);

        editorPane.setDoubleBuffered(true);
        editorPane.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                editorPaneKeyReleased(evt);
            }
        });
        spEmployees.setViewportView(editorPane);

        splitEditor.setLeftComponent(spEmployees);

        panRightCentre.add(splitEditor, java.awt.BorderLayout.CENTER);

        panEmployeeSelectionRule.add(panRightCentre, java.awt.BorderLayout.CENTER);

        splMain.setRightComponent(panEmployeeSelectionRule);

        panSelectionFormulas.add(splMain, java.awt.BorderLayout.CENTER);

        tabPane.addTab("Selection Formulas", panSelectionFormulas);

        getContentPane().add(tabPane, java.awt.BorderLayout.CENTER);

        panValidation.setLayout(new java.awt.GridLayout(1, 1, 5, 5));

        lblValidationProgress.setName("lblValidationProgress"); // NOI18N
        panValidation.add(lblValidationProgress);

        pbValidationProgress.setName("pbValidationProgress"); // NOI18N
        panValidation.add(pbValidationProgress);

        getContentPane().add(panValidation, java.awt.BorderLayout.SOUTH);

        fileMenu.setText("File");
        fileMenu.setMaximumSize(new java.awt.Dimension(40, 32767));

        jMenuItem1.setText("Validate Configuration");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TestConfiguration(evt);
            }
        });
        fileMenu.add(jMenuItem1);
        fileMenu.add(jSeparator2);

        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        printMenuItem.setText("Print...");
        printMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(printMenuItem);

        exportMenuItem.setText("Export WebSetup...");
        exportMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exportMenuItem);
        exportMenuItem.getAccessibleContext().setAccessibleName("Export Setup");

        jMenuItem2.setText("Export Process History ...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportProcessHistory(evt);
            }
        });
        fileMenu.add(jMenuItem2);
        fileMenu.add(jSeparator3);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText("Help");

        aboutMenuItem.setText("About");
        aboutMenuItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aboutMenuItemMouseClicked(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exportMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuItemActionPerformed
        exportSetup();
    }//GEN-LAST:event_exportMenuItemActionPerformed

    private void lstExistingReportingStructuresKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lstExistingReportingStructuresKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (JOptionPane.showConfirmDialog(this, "Are you sure you wish to delete this Reporting Structure?",
                    "Reporting Structure", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                    == JOptionPane.YES_OPTION) {
                deleteReportingStructure((ReportingStructure) lstExistingReportingStructures.getSelectedValue());
                setContentsChanged(true);

            }
        }// TODO add your handling code here:
    }//GEN-LAST:event_lstExistingReportingStructuresKeyPressed

    private void lstExistingEmployeeSelectionsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lstExistingEmployeeSelectionsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (JOptionPane.showConfirmDialog(this, "Are you sure you wish to delete this Rule?",
                    "Employee Selection Rule", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                    == JOptionPane.YES_OPTION) {
                EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
                deleteEmployeeSelectionRule(((EmployeeSelectionRule) ex.getEmployeeSelection()));
            }
        }
    }//GEN-LAST:event_lstExistingEmployeeSelectionsKeyPressed

    private void btnCopyEmployeeSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCopyEmployeeSelectionActionPerformed
        // If the user has an existing Formula selected, make a copy of it
        if (lstExistingEmployeeSelections.getSelectedValue() != null) {
            EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
            EmployeeSelectionRule toCopy = ((EmployeeSelectionRule) ex.getEmployeeSelection());

            String ruleName = JOptionPane.showInputDialog(this, "Please provide a name for the new Employee Selection Rule",
                    "Copy of '" + toCopy.getName() + "'", JOptionPane.QUESTION_MESSAGE);

            if ((ruleName != null) && (ruleName.trim().length() > 0)) {
                EmployeeSelectionRule newRule = new EmployeeSelectionRule(ruleName, "");
                newRule.setCreationDate(new java.util.Date());
                newRule.setRule(toCopy.getRule());
                addEmployeeSelectionRule(newRule);
                setContentsChanged(true);
            }
        }
    }//GEN-LAST:event_btnCopyEmployeeSelectionActionPerformed

    private void btnApplyWebProcessChangesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApplyWebProcessChangesActionPerformed
        setContentsChanged(true);
        updateWebProcessDefinition(
                (WebProcessDefinition) lstExistingWebProcessDefinitions.getSelectedValue());
        processPropertiesChanged = false;
    }//GEN-LAST:event_btnApplyWebProcessChangesActionPerformed
    private void aboutMenuItemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aboutMenuItemMouseClicked
        JOptionPane.showMessageDialog(this, version, "WebConfig Version", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_aboutMenuItemMouseClicked

    private void treeReportingStructureKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_treeReportingStructureKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            // Which formula is being deleted?
            Object pathComponent = treeReportingStructure.getSelectionPath().getLastPathComponent();
            EmployeeSelection selection = (EmployeeSelection) ((DefaultMutableTreeNode) pathComponent).getUserObject();
            // Confirm Deletion
            if (selection != null) {
                if (JOptionPane.showConfirmDialog(this, "Are you sure you wish to delete '" + selection.getName() + "' from the Reporting Structure?",
                        "Reporting Structure", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                        == JOptionPane.YES_OPTION) {
                    ReportingStructure reportingStructure = (ReportingStructure) lstExistingReportingStructures.getSelectedValue();
                    deleteSelectionFromReportingStructure(
                            reportingStructure, selection);
                    setContentsChanged(true);
                    // Update the necessary lists
                    initSelectionFormulaTab();
                }
            }
        }
    }//GEN-LAST:event_treeReportingStructureKeyPressed

    private void printMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printMenuItemActionPerformed
        printSetup();
    }//GEN-LAST:event_printMenuItemActionPerformed
    private void cbSupervisorProcessItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbSupervisorProcessItemStateChanged
        processPropertiesChanged = true;
        ((WebProcessDefinition) lstExistingWebProcessDefinitions.getSelectedValue()).setCannotInstantiateSelf(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_cbSupervisorProcessItemStateChanged
    private void lstExistingEmployeeSelectionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstExistingEmployeeSelectionsMouseClicked
        // If the user double-clicked on a Selection Formula, edit it
        EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();

        if ((evt.getClickCount() == 2) && (ex != null)) {
            btnEditEmployeeSelectionActionPerformed(null);
        }
    }//GEN-LAST:event_lstExistingEmployeeSelectionsMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // Add your handling code here:
        exitMenuItemActionPerformed(null);
    }//GEN-LAST:event_formWindowClosing
    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        saveToFileContainer();
    }//GEN-LAST:event_saveMenuItemActionPerformed
    private void edtStageAgeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtStageAgeKeyTyped
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtStageAgeKeyTyped
    private void edtProcessAgeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtProcessAgeKeyTyped
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtProcessAgeKeyTyped
    private void edtStageAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtStageAgeActionPerformed
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtStageAgeActionPerformed
    private void edtProcessAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtProcessAgeActionPerformed
        processPropertiesChanged = true;

    }//GEN-LAST:event_edtProcessAgeActionPerformed
    private void cbbReportingStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbReportingStructureActionPerformed
        processPropertiesChanged = true;

    }//GEN-LAST:event_cbbReportingStructureActionPerformed
    private void lstExistingWebProcessDefinitionsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstExistingWebProcessDefinitionsValueChanged
        displayWebProcessDefinition();
    }//GEN-LAST:event_lstExistingWebProcessDefinitionsValueChanged

    private void btnDeleteReportingStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteReportingStructureActionPerformed
        // Delete a Reporting Structure
        // Confirm Deletion
        if (JOptionPane.showConfirmDialog(this, "Are you sure you wish to delete this Reporting Structure?",
                "Reporting Structure", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                == JOptionPane.YES_OPTION) {
            deleteReportingStructure((ReportingStructure) lstExistingReportingStructures.getSelectedValue());
            setContentsChanged(true);
        }
    }//GEN-LAST:event_btnDeleteReportingStructureActionPerformed

    private void btnNewReportingStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewReportingStructureActionPerformed
        // Prompt the user for a name
        String structureName = JOptionPane.showInputDialog(this, "Please provide a name for the new Reporting Structure",
                "Reporting Structure", JOptionPane.QUESTION_MESSAGE);

        // Create new Structure
        if ((structureName != null) && (structureName.trim().length() > 0)) {
            addReportingStructure(new ReportingStructure(structureName, ""));
            setContentsChanged(true);
        }
        checkReportingStructureName();
    }//GEN-LAST:event_btnNewReportingStructureActionPerformed

    private void lstExistingReportingStructuresValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstExistingReportingStructuresValueChanged
        // User selected a different reporting structure
        setupScreensAndButtons();
        refreshScreenComponents();
    }//GEN-LAST:event_lstExistingReportingStructuresValueChanged
    private void btnApplyEmployeeSelectionFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApplyEmployeeSelectionFormulaActionPerformed
        // Update the rule & name
        EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
        String oldName = ex.getEmployeeSelection().getName();
        String newName = txtEmployeeSelectionName.getText().trim();
        ((EmployeeSelectionRule) ex.getEmployeeSelection()).setRule(getTextpaneText(editorPane));
        ((EmployeeSelectionRule) ex.getEmployeeSelection()).setName(newName);
        // Now that this rule has been changed, we need to update the EmployeeSelection that may be linked to one or more
        // reporting structures too.
        if (!oldName.trim().equals(newName.trim())) {
            for (int i = 0; i < localReportingStructures.size(); i++) {
                ReportingStructure struct = (ReportingStructure) localReportingStructures.get(i);
                for (int j = 0; j < struct.getSelectionLinks().size(); j++) {
                    SelectionLink link = (SelectionLink) struct.getSelectionLinks().get(j);
                    EmployeeSelection fromR = link.getReportsFrom();
                    EmployeeSelection toR = link.getReportsTo();
                    if (fromR.getName().equals(oldName)) {
                        fromR.setName(newName);
                    }
                    if (toR.getName().equals(oldName)) {
                        toR.setName(newName);
                    }
                }
            }
            initReportingStructureTab();

        }
        // Disable Formula Editor
        setEnabledEmployeeSelectionEditor(false);
        // Setup buttons
        btnNewEmployeeSelection.setEnabled(true);
        btnEditEmployeeSelection.setEnabled(false);
        btnDeleteEmployeeSelection.setEnabled(false);
        setContentsChanged(true);

    }//GEN-LAST:event_btnApplyEmployeeSelectionFormulaActionPerformed

    private void btnCancelEmployeeSelectionFormulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelEmployeeSelectionFormulaActionPerformed
        // Disable Formula Editor
        setEnabledEmployeeSelectionEditor(false);
        // Setup buttons
        btnNewEmployeeSelection.setEnabled(true);
        btnEditEmployeeSelection.setEnabled(false);
        btnDeleteEmployeeSelection.setEnabled(false);
    }//GEN-LAST:event_btnCancelEmployeeSelectionFormulaActionPerformed
    private void btnDeleteEmployeeSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteEmployeeSelectionActionPerformed
        // Confirm Deletion
        if (JOptionPane.showConfirmDialog(this, "Are you sure you wish to delete this Rule?",
                "Employee Selection Rule", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                == JOptionPane.YES_OPTION) {
            EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
            deleteEmployeeSelectionRule(((EmployeeSelectionRule) ex.getEmployeeSelection()));
        }
    }//GEN-LAST:event_btnDeleteEmployeeSelectionActionPerformed

    private void btnEditEmployeeSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditEmployeeSelectionActionPerformed
        EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
        editEmployeeSelectionRule(((EmployeeSelectionRule) ex.getEmployeeSelection()));
    }//GEN-LAST:event_btnEditEmployeeSelectionActionPerformed
    private void btnNewEmployeeSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewEmployeeSelectionActionPerformed
        // Prompt the user for a name
        String ruleName = JOptionPane.showInputDialog(this, "Please provide a name for the new Employee Selection Rule",
                "Employee Selection Rule", JOptionPane.QUESTION_MESSAGE);

        if ((ruleName != null) && (ruleName.trim().length() > 0)) {
            EmployeeSelectionRule newRule = new EmployeeSelectionRule(ruleName, "");
            newRule.setCreationDate(new java.util.Date());
            addEmployeeSelectionRule(newRule);
            setContentsChanged(true);

        }

    }//GEN-LAST:event_btnNewEmployeeSelectionActionPerformed

    private void lstExistingEmployeeSelectionsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstExistingEmployeeSelectionsValueChanged
        // Scrolled between employee selections
        Object selectedObject = lstExistingEmployeeSelections.getSelectedValue();
        setupScreensAndButtons();
        lstExistingEmployeeSelections.setSelectedValue(selectedObject, true);
    }//GEN-LAST:event_lstExistingEmployeeSelectionsValueChanged

    private void btnTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestActionPerformed
        // Create a new Rule and Execute it to test the results
        EmployeeSelectionRule rule = new EmployeeSelectionRule("Test", "Test");
        // Extract Text from editor and execute rule
        rule.setRule(getTextpaneText(editorPane));
        textEmployees.setText(rule.listResult());
    }//GEN-LAST:event_btnTestActionPerformed

    private void editorPaneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_editorPaneKeyReleased
        // Fire the task that formats the Syntax Highlighting
        FormulaFormatter fmt = new FormulaFormatter(editorPane);
        fmt.run();
    }//GEN-LAST:event_editorPaneKeyReleased

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // Add a default formula into the Rules Editor
        String comparator = cbbCompare.getSelectedItem().toString();
        String formula = " [" + cbbFilters.getSelectedItem().toString() + " " + comparator + " '...'] ";
        editorPane.setText(getTextpaneText(editorPane).trim() + " " + formula);
        // Fire the task that formats the Syntax Highlighting
        FormulaFormatter fmt = new FormulaFormatter(editorPane);
        fmt.run();
        setContentsChanged(true);

    }//GEN-LAST:event_btnAddActionPerformed
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        if (contentsChanged) {
            int option = JOptionPane.showConfirmDialog(this, "Do you wish to save the changes before closing?",
                    "Unsaved Changes", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
            //Confirm save
            if (option == JOptionPane.YES_OPTION) {
                saveToFileContainer();
                System.exit(0);
            }
            // Confirm Exit
            if (option == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        }
        if (!contentsChanged) {
            System.exit(0);
        }
    }//GEN-LAST:event_exitMenuItemActionPerformed
    private void TestConfiguration(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TestConfiguration
        // Confirm Test
        if (JOptionPane.showConfirmDialog(this, "Validating (and fixing) may take some time. Do you wish to continue?",
                "Testing Configuration", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)
                == JOptionPane.YES_OPTION) {
            new Thread(new Validation()).start(); //Start the thread
        }
    }//GEN-LAST:event_TestConfiguration

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        lblValidationProgress.setVisible(false);
        pbValidationProgress.setVisible(false);
    }//GEN-LAST:event_formWindowOpened

    private void btnExpandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpandActionPerformed
        TreeNode root = (TreeNode) treeReportingStructure.getModel().getRoot();
        expandAll(treeReportingStructure, new TreePath(root), true);
    }//GEN-LAST:event_btnExpandActionPerformed

    private void btnCollapseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCollapseActionPerformed
        TreeNode root = (TreeNode) treeReportingStructure.getModel().getRoot();
        expandAll(treeReportingStructure, new TreePath(root), false);
    }//GEN-LAST:event_btnCollapseActionPerformed

    private void edtAvailableFromDayOfMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtAvailableFromDayOfMonthActionPerformed
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtAvailableFromDayOfMonthActionPerformed

    private void edtAvailableFromDayOfMonthKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtAvailableFromDayOfMonthKeyTyped
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtAvailableFromDayOfMonthKeyTyped

    private void edtAvailableToDayOfMonthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtAvailableToDayOfMonthActionPerformed
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtAvailableToDayOfMonthActionPerformed

    private void edtAvailableToDayOfMonthKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_edtAvailableToDayOfMonthKeyTyped
        processPropertiesChanged = true;
    }//GEN-LAST:event_edtAvailableToDayOfMonthKeyTyped

    private void cbNotifyActivationItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbNotifyActivationItemStateChanged
        processPropertiesChanged = true;
        ((WebProcessDefinition) lstExistingWebProcessDefinitions.getSelectedValue()).setNotifyOfPendingActivation(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_cbNotifyActivationItemStateChanged

    private void cbNotifyDeactivationItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbNotifyDeactivationItemStateChanged
        processPropertiesChanged = true;
        ((WebProcessDefinition) lstExistingWebProcessDefinitions.getSelectedValue()).setNotifyOfPendingDeactivation(evt.getStateChange() == ItemEvent.SELECTED);
    }//GEN-LAST:event_cbNotifyDeactivationItemStateChanged

    private void edtProcessDescriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtProcessDescriptionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtProcessDescriptionActionPerformed

    private void exportProcessHistory(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportProcessHistory
        exportProcessHistory();
    }//GEN-LAST:event_exportProcessHistory

    private void btnEditReportingStructureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditReportingStructureActionPerformed
        // Edit a Reporting Structure
        // Edit dialog
        String oldName = lstExistingReportingStructures.getSelectedValue().toString();
        String newStructureName = JOptionPane.showInputDialog(this, "Please provide a new name for " + oldName, "Rename Reporting Structure", JOptionPane.QUESTION_MESSAGE);
        // Rename Structure
        if ((newStructureName != null) && (newStructureName.trim().length() > 0)) {
            ReportingStructure existingReportingStructure = (ReportingStructure) lstExistingReportingStructures.getSelectedValue();
            editReportingStructure(existingReportingStructure, newStructureName);
            setContentsChanged(true);
        }
        checkReportingStructureName();
    }//GEN-LAST:event_btnEditReportingStructureActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            // Set System L&F
            //com.sun.java.swing.plaf.windows.WindowsLookAndFeel
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            // handle exception
        } catch (InstantiationException e) {
            // handle exception
        } catch (IllegalAccessException e) {
            // handle exception
        }

        new WebConfig().setVisible(true);
    }

    /**
     * Adds a new reporting structure to the configuration
     */
    private void addReportingStructure(ReportingStructure reportingStructure) {
        // 1.  Add Structure to fileContainer
        localReportingStructures.add(reportingStructure);

        // 2. Refresh display of reporting structures
        initCustomComponents();

        // 3. Select new structure
        lstExistingReportingStructures.setSelectedValue(reportingStructure, true);
    }

    /**
     * Edits an existing reporting structure
     */
    private void editReportingStructure(ReportingStructure existingReportingStructure, String newStructureName) {
        // 1.  Rename Structure
        existingReportingStructure.setName(newStructureName);

        // 2. Refresh display of reporting structures
        initCustomComponents();

        // 3. Select renamed structure
        lstExistingReportingStructures.setSelectedValue(existingReportingStructure, true);
    }

    /**
     * Adds a new employee selection rule to the configuration
     */
    private void addEmployeeSelectionRule(EmployeeSelectionRule employeeSelectionRule) {
        // 1. Disable the editor that is used to view / edit formulas
        setEnabledEmployeeSelectionEditor(false);

        // 2. Add rule to fileContainer
        localEmployeeSelections.add(employeeSelectionRule);

        // 3. Refresh display
        initCustomComponents();

        // 4. Select current rule
        lstExistingEmployeeSelections.setSelectedValue(employeeSelectionRule, true);
    }

    public LinkedList sortList(LinkedList list) {
        TreeMap map = new TreeMap();
        Iterator iter = list.iterator();

        while (iter.hasNext()) {
            Object obj = iter.next();
            map.put(obj.toString(), obj);
        }
        return (new LinkedList(map.values()));
    }

    /**
     * Expands/ Collapses a JTree component
     *
     * @param tree
     * @param parent
     * @param expand - boolean (true-expand, false-collapse)
     */
    private void expandAll(JTree tree, TreePath parent, boolean expand) { // Traverse children
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0) {
            for (Enumeration e = node.children(); e.hasMoreElements();) {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                expandAll(tree, path, expand);
            }
        } // Expansion or collapse must be done bottom-up
        if (expand) {
            tree.expandPath(parent);
        } else {
            tree.collapsePath(parent);
        }
    }

    /**
     * Adds a new employeeselection rule to the configuration
     */
    private void deleteEmployeeSelectionRule(EmployeeSelectionRule employeeSelectionRule) {
        // 1. Disable the editor that is used to view / edit formulas
        setEnabledEmployeeSelectionEditor(false);

        // 2. Setup enabled buttons
        btnNewEmployeeSelection.setEnabled(true);
        btnEditEmployeeSelection.setEnabled(false);
        btnDeleteEmployeeSelection.setEnabled(false);

        // 3. Remove the structure from all reporting structures.
        removeEmployeeSelectionFromAllStructures(employeeSelectionRule);

        // 4. Remove the actual EmployeeSelection
        localEmployeeSelections.remove(employeeSelectionRule);

        // 5. Refresh display
        initCustomComponents();

        // 6. Select first item
        if (lstExistingEmployeeSelections.getModel().getSize() > 0) {
            lstExistingEmployeeSelections.setSelectedIndex(0);
        }
    }

    /**
     * Adds a new employeeselection rule to the configuration
     */
    private void editEmployeeSelectionRule(EmployeeSelectionRule employeeSelectionRule) {
        // 1. Enable Formula Editor
        setEnabledEmployeeSelectionEditor(true);

        // 2. Load rule into editor
        editorPane.setText(employeeSelectionRule.getRule());
        txtEmployeeSelectionName.setText(employeeSelectionRule.getName());

        // 3. Fire the task that formats the Syntax Highlighting
        FormulaFormatter fmt = new FormulaFormatter(editorPane);
        fmt.run();

        // 4. Position the caret at the beginning of the formula
        editorPane.setCaretPosition(0);
    }

    /**
     * Removes an EmployeeSelection from an existing Reporting Structure
     */
    private void deleteSelectionFromReportingStructure(ReportingStructure reportingStructure, EmployeeSelection employeeSelection) {

        // 1. Remove the structure from all reporting structures.
        removeEmployeeSelectionFromReportingStructure(reportingStructure, employeeSelection);

        // 2. Refresh display
        refreshTreeViewDisplay();
    }

    /**
     * Removes the given reportingstructure from the configuration
     */
    private void deleteReportingStructure(ReportingStructure reportingStructure) {
        // 1. Remove from list of available reporting structures
        localReportingStructures.remove(reportingStructure);

        // 2. Remove from WebProcessDefinitions
        for (int i = 0; i
                < localWebProcessDefinitions.size(); i++) {
            if (((WebProcessDefinition) localWebProcessDefinitions.get(i)).getReportingStructure() == reportingStructure) {
                ((WebProcessDefinition) localWebProcessDefinitions.get(i)).setReportingStructure(null);
            }
        }

        // 3. Refresh display of reporting structures
        initCustomComponents();

        // Select newly created structure
        if (lstExistingReportingStructures.getModel().getSize() > 0) {
            lstExistingReportingStructures.setSelectedIndex(0);
        }

    }

    /**
     * Quick way to make sure all the components are enabled/disabled
     */
    private void setEnabledEmployeeSelectionEditor(boolean flag) {
        btnCancelEmployeeSelectionFormula.setEnabled(flag);
        btnApplyEmployeeSelectionFormula.setEnabled(flag);
        panEmployeeSelectionList.setVisible(!flag);
        panRightTop.setVisible(flag);
        editorPane.setEnabled(flag);
        tabPane.setEnabled(!flag);
        if (!flag) {
            splMain.setDividerLocation(200);
        }
    }

    /**
     * Updates the screen with the selected WebProcess Definition properties
     */
    private void displayWebProcessDefinition() {
        // Get Currently selected WebProcessDefinition
        WebProcessDefinition wpd = (WebProcessDefinition) lstExistingWebProcessDefinitions.getSelectedValue();
        if ((wpd != null) && (processPropertiesChanged)) {
            JOptionPane.showMessageDialog(this, "You've made changes to a WebProcess without applying it.  These changes have been discarded",
                    "Web Process Changes", JOptionPane.WARNING_MESSAGE);
        }

        if (wpd != null) {
            edtProcessName.setText(wpd.getName());
            edtProcessDescription.setText(wpd.getDescription());
            edtProcessAge.setText(new Integer(wpd.getMaxProcessAge()).toString());
            edtStageAge.setText(new Integer(wpd.getMaxStageAge()).toString());
            edtAvailableFromDayOfMonth.setText(new Integer(wpd.getActiveFromDayOfMonth()).toString());
            edtAvailableToDayOfMonth.setText(new Integer(wpd.getActiveToDayOfMonth()).toString());
            cbbReportingStructure.getModel().setSelectedItem(wpd.getReportingStructure());
            cbSupervisorProcess.setSelected(wpd.getCannotInstantiateSelf());
            cbNotifyActivation.setSelected(wpd.isNotifyOfPendingActivation());
            cbNotifyDeactivation.setSelected(wpd.isNotifyOfPendingDeactivation());
        }
        processPropertiesChanged = false;
    }

    /**
     * This method is the central point of control for all User Actions.
     */
    private void setupScreensAndButtons() {
        // Edit/Delete Employee Selection IF one is selected
        btnEditEmployeeSelection.setEnabled(lstExistingEmployeeSelections.getSelectedIndex() >= 0);
        btnDeleteEmployeeSelection.setEnabled(lstExistingEmployeeSelections.getSelectedIndex() >= 0);
        btnCopyEmployeeSelection.setEnabled(lstExistingEmployeeSelections.getSelectedIndex() >= 0);

        // Delete Reporting Structures IF one is selected
        btnDeleteReportingStructure.setEnabled(lstExistingReportingStructures.getSelectedIndex() >= 0);
        // Reset visibility of the Employee Selection Panel (Might have been hidden by the editing of formulas)
        btnEditReportingStructure.setEnabled(lstExistingReportingStructures.getSelectedIndex() >= 0);
        // Reset visibility of the Employee Selection Panel (Might have been hidden by the editing of formulas)
        panEmployeeSelectionList.setVisible(true);

        setEnabledEmployeeSelectionEditor(false);

        // Update the editor with the selected Selection's info
        if (lstExistingEmployeeSelections.getSelectedValue() != null) {
            EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
            editorPane.setText(((EmployeeSelectionRule) ex.getEmployeeSelection()).getRule());
            txtEmployeeSelectionName.setText(((EmployeeSelectionRule) ex.getEmployeeSelection()).getName());
        }
        editorPaneKeyReleased(null);

    }

    // Total Refresh of all screens / lists / treeviews
    private void refreshScreenComponents() {

        // 2. Refresh Reporting Structure Combobox in the Web Processes Tab
        refreshReportingStructureCombobox();

        // 3.Display the properties of the WebProcessDefinition
        displayWebProcessDefinition();

        // 4. Refresh the display of the currenlty selected reporting structure
        refreshTreeViewDisplay();

        // 5. Update selected formula in Formula Editor
        if (lstExistingEmployeeSelections.getSelectedValue() != null) {
            EmployeeSelectionRuleExtended ex = (EmployeeSelectionRuleExtended) lstExistingEmployeeSelections.getSelectedValue();
            editorPane.setText(((EmployeeSelectionRule) ex.getEmployeeSelection()).getRule());
            txtEmployeeSelectionName.setText(((EmployeeSelectionRule) ex.getEmployeeSelection()).getName());
        }
        editorPaneKeyReleased(null);
    }

    /**
     * Returns the unformatted text in the JTextPane
     */
    private String getTextpaneText(JTextPane pane) {
        Document document = pane.getDocument();
        try {
            String docText = (document.getText(1, document.getLength()));
            return docText;
        } catch (BadLocationException ble) {
            System.err.println("Bad Location. Exception:" + ble);
        }
        return "";
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
    }

    /**
     * User performed a Drag-n-Drop action
     */
    @Override
    public void drop(DropTargetDropEvent e) {
        try {
            // The DataFlavor is defining the type of data being transferred.
            DataFlavor stringFlavor = DataFlavor.stringFlavor;
            Transferable tr = e.getTransferable();

            // Is this a managable drag-and-drop?

            if (e.isDataFlavorSupported(stringFlavor)) {

                // Get the EmployeeSelectionName that is being dragged
                String droppedSelectionCaption = (String) tr.getTransferData(stringFlavor);
                String employeeSelectionName = droppedSelectionCaption;
                if (droppedSelectionCaption.indexOf('[') > 0) {
                    employeeSelectionName = droppedSelectionCaption.substring(0, droppedSelectionCaption.indexOf('[') - 1);
                }
                e.acceptDrop(DnDConstants.ACTION_MOVE);

                // Get EmployeeSelection that is being dragged
                EmployeeSelection fromSelection = getEmployeeSelection(employeeSelectionName);

                // Get EmployeeSelection closes to where we released the mouse
                int x = new Double(e.getLocation().getX()).intValue();

                int y = new Double(e.getLocation().getY()).intValue();

                EmployeeSelection toSelection = null;
                TreePath path = treeReportingStructure.getClosestPathForLocation(x, y);

                if (path != null) {
                    Object pathComponent;
                    System.out.println("Path:" + path);
                    pathComponent = path.getLastPathComponent();
                    System.out.println("Object:" + pathComponent + " - " + pathComponent.getClass().getName());

                    toSelection = (EmployeeSelection) ((DefaultMutableTreeNode) pathComponent).getUserObject();
                }

                // If this is a first Drag-n-Drop, there is propably no EmployeeSelection in there
                if (toSelection == null) {
                    setTopNode(((ReportingStructure) lstExistingReportingStructures.getSelectedValue()), fromSelection);
                    setContentsChanged(true);
                } else {
                    if (fromSelection.getName().compareToIgnoreCase(toSelection.getName()) != 0) {
                        // Create a link
                        SelectionLink link = new SelectionLink(fromSelection, toSelection);

                        ((ReportingStructure) lstExistingReportingStructures.getSelectedValue()).addSelectionLink(link);
                        setContentsChanged(true);

                    }
                }

                e.dropComplete(true);

                // Refresh the Reporting Structure by unselecting and reselection the reporting structure
                //treeReportingStructure.setModel(createTreeModel((ReportingStructure)lstExistingReportingStructures.getSelectedValue()));
                refreshTreeViewDisplay();
                initSelectionFormulaTab();
                treeReportingStructure.setSelectionPath(treeReportingStructure.getClosestPathForLocation(x, y));

                // Re-link the Web Process to the changed reporting structure.
                Iterator iter = localWebProcessDefinitions.iterator();
                while (iter.hasNext()) {
                    WebProcessDefinition wpd = (WebProcessDefinition) iter.next();

                    if ((wpd != null) && (lstExistingReportingStructures.getSelectedValue() != null) && (wpd.getReportingStructure() != null)) {
                        if (wpd.getReportingStructure().hashCode() == ((ReportingStructure) lstExistingReportingStructures.getSelectedValue()).hashCode()) {
                            wpd.setReportingStructure((ReportingStructure) lstExistingReportingStructures.getSelectedValue());
                        }
                    }
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();

        } catch (UnsupportedFlavorException ufe) {
            ufe.printStackTrace();

        }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    /**
     * Removes this selection from the list. If this selection is used in
     * Reporting Structures, that link is removed as well.
     */
    public void removeEmployeeSelectionFromAllStructures(EmployeeSelection employeeSelection) {
        for (int i = 0; i
                < localReportingStructures.size(); i++) {
            ReportingStructure structure = (ReportingStructure) localReportingStructures.get(i);
            ArrayList selectionLinks = structure.getSelectionLinks(employeeSelection);
            for (int j = 0; j
                    < selectionLinks.size(); j++) {
                structure.removeSelectionLink((SelectionLink) selectionLinks.get(j));

            }
        }
    }

    /**
     * Removes this selection from the selected reporting structure
     */
    public void removeEmployeeSelectionFromReportingStructure(ReportingStructure reportingStructure, EmployeeSelection employeeSelection) {

        ArrayList selectionLinks = reportingStructure.getSelectionLinks(employeeSelection);
        for (int j = 0; j
                < selectionLinks.size(); j++) {
            reportingStructure.removeSelectionLink((SelectionLink) selectionLinks.get(j));
        }
    }

    /**
     * Setter for property contentsChanged.
     *
     * @param contentsChanged New value of property contentsChanged.
     *
     */
    public void setContentsChanged(boolean contentsChanged) {
        this.contentsChanged = contentsChanged;
        if (contentsChanged) {
            setTitle("* " + title);
        } else {
            setTitle(title);
        }
    }
//    FileContainer fileContainer = FileContainer.getInstance(false,false );
    FileContainer fileContainer = FileContainer.getInstance();
    LinkedList localEmployeeSelections = new LinkedList();
    LinkedList localReportingStructures = new LinkedList();
    LinkedList localWebProcessDefinitions = new LinkedList();
    LinkedList topNodes = new LinkedList();
    boolean contentsChanged = false;
    boolean processPropertiesChanged = false;
    String title = "Peopleware - Web Configuration";
    String exportFileName = "Peopleware_ESS_Setup.html";
    String processExportFileName = "ProcessHistory.csv";
    String version = "2.00";
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnApplyEmployeeSelectionFormula;
    private javax.swing.JButton btnApplyWebProcessChanges;
    private javax.swing.JButton btnCancelEmployeeSelectionFormula;
    private javax.swing.JButton btnCollapse;
    private javax.swing.JButton btnCopyEmployeeSelection;
    private javax.swing.JButton btnDeleteEmployeeSelection;
    private javax.swing.JButton btnDeleteReportingStructure;
    private javax.swing.JButton btnEditEmployeeSelection;
    private javax.swing.JButton btnEditReportingStructure;
    private javax.swing.JButton btnExpand;
    private javax.swing.JButton btnNewEmployeeSelection;
    private javax.swing.JButton btnNewReportingStructure;
    private javax.swing.JButton btnTest;
    private javax.swing.JCheckBox cbNotifyActivation;
    private javax.swing.JCheckBox cbNotifyDeactivation;
    private javax.swing.JCheckBox cbSupervisorProcess;
    private javax.swing.JComboBox cbbCompare;
    private javax.swing.JComboBox cbbFilters;
    private javax.swing.JComboBox cbbReportingStructure;
    private javax.swing.JTextPane editorPane;
    private javax.swing.JTextField edtAvailableFromDayOfMonth;
    private javax.swing.JTextField edtAvailableToDayOfMonth;
    private javax.swing.JTextField edtProcessAge;
    private javax.swing.JTextField edtProcessDescription;
    private javax.swing.JTextField edtProcessName;
    private javax.swing.JTextField edtStageAge;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenuItem exportMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblAvailableFromDayOfMonth;
    private javax.swing.JLabel lblAvailableToDayOfMonth;
    private javax.swing.JLabel lblCompare;
    private javax.swing.JLabel lblCurrentReportingStructure;
    private javax.swing.JLabel lblEditor;
    private javax.swing.JLabel lblFilter;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JLabel lblProcessAge;
    private javax.swing.JLabel lblProcessDescription;
    private javax.swing.JLabel lblProcessName;
    private javax.swing.JLabel lblReportingStructure;
    private javax.swing.JLabel lblSelections;
    private javax.swing.JLabel lblSelections1;
    private javax.swing.JLabel lblSelections2;
    private javax.swing.JLabel lblSpacer;
    private javax.swing.JLabel lblSpacer2;
    private javax.swing.JLabel lblStageAge;
    private javax.swing.JLabel lblValidationProgress;
    private javax.swing.JList lstExistingEmployeeSelections;
    private javax.swing.JList lstExistingEmployeeSelectionsInTreeView;
    private javax.swing.JList lstExistingReportingStructures;
    private javax.swing.JList lstExistingWebProcessDefinitions;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel panEmployeeSelectionEdit;
    private javax.swing.JPanel panEmployeeSelectionList;
    private javax.swing.JPanel panEmployeeSelectionRule;
    private javax.swing.JPanel panFormulaEditor;
    private javax.swing.JPanel panLeftBottom;
    private javax.swing.JPanel panLeftBottom1;
    private javax.swing.JPanel panLeftBottom2;
    private javax.swing.JPanel panOKCANCEL;
    private javax.swing.JPanel panOKCANCEL1;
    private javax.swing.JPanel panProcessProperties;
    private javax.swing.JPanel panRename;
    private javax.swing.JPanel panReportingStructureDetail;
    private javax.swing.JPanel panReportingStructureEdit;
    private javax.swing.JPanel panReportingStructureList;
    private javax.swing.JPanel panReportingStructures;
    private javax.swing.JPanel panRightBottom;
    private javax.swing.JPanel panRightBottom1;
    private javax.swing.JPanel panRightCentre;
    private javax.swing.JPanel panRightCentre1;
    private javax.swing.JPanel panRightTop;
    private javax.swing.JPanel panSelection;
    private javax.swing.JPanel panSelection1;
    private javax.swing.JPanel panSelection2;
    private javax.swing.JPanel panSelectionFormulas;
    private javax.swing.JPanel panValidation;
    private javax.swing.JPanel panWebProcesses;
    private javax.swing.JPanel panWebProcessesList;
    private javax.swing.JProgressBar pbValidationProgress;
    private javax.swing.JMenuItem printMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JScrollPane scrlSelections;
    private javax.swing.JScrollPane scrlSelections1;
    private javax.swing.JScrollPane scrlSelections2;
    private javax.swing.JScrollPane spEditor;
    private javax.swing.JScrollPane spEmployees;
    private javax.swing.JSplitPane splMain;
    private javax.swing.JSplitPane splProcesses;
    private javax.swing.JSplitPane splReportingStructures;
    private javax.swing.JSplitPane splitEditor;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTextArea textEmployees;
    private javax.swing.JTree treeReportingStructure;
    private javax.swing.JTextField txtEmployeeSelectionName;
    // End of variables declaration//GEN-END:variables
}

/**
 * Before one can create a reporting structure, one needs at least one employee
 * selection to which the other employee selections can be linked to.
 */
class TopNode {

    public TopNode(
            ReportingStructure structure, EmployeeSelection selection) {
        this.structure = structure;
        this.selection = selection;
    }
    public ReportingStructure structure;
    public EmployeeSelection selection;
}

class printableEditorPane extends JEditorPane implements
        Printable, Serializable {

    public printableEditorPane() {
        super();
        this.setSize(468 * 2, 648 * 4);  // Expand the viewable size of the page to
        // fit onto the standard A4 pager(s)
    }

    @Override
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);

        RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);
        Dimension d = this.getSize();
        double panelWidth = d.width;
        double panelHeight = d.height;
        double pageWidth = pf.getImageableWidth();
        double pageHeight = pf.getImageableHeight();
        double scale = pageWidth / panelWidth;
        int totalNumPages = (int) Math.ceil(scale * panelHeight
                / pageHeight);

        // Check for empty pages
        if (pageIndex >= totalNumPages) {
            return Printable.NO_SUCH_PAGE;
        }

        g2.translate(pf.getImageableX(), pf.getImageableY());
        g2.translate(0f, -pageIndex * pageHeight);
        g2.scale(scale, scale);
        this.paint(g2);

        return Printable.PAGE_EXISTS;
    }
}
