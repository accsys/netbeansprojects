package za.co.ucs.accsys.webmodule;

import java.util.*;
import za.co.ucs.accsys.peopleware.Employee;

/**
 * <p>
 * Singleton - Using Reflection, it generates a list of employees based on the instance of the
 * EmployeeSelection class.  This is more-or-less based on the
 * Factory Design Pattern
 * </p>
 *
 * @serial file
 */
public class SelectionInterpreter {

    private SelectionInterpreter() {
        internalCache = new LinkedHashMap();
    }

    public static SelectionInterpreter getInstance() {
        if (interpreter == null) {
            interpreter = new SelectionInterpreter();
        }
        return interpreter;
    }

    public void resetInternalCache() {
        internalCache.clear();
    }

    /** Returns a HashSet with the key as companyID + "," + employeeID, and the value as Employee
     *
     * @param employeeSelection
     * @return 
     */
    public HashSet<Employee> generateEmployeeList(EmployeeSelection employeeSelection) {
        return generateEmployeeList(employeeSelection, true);
    }

    /** Returns a HashSet with the key as companyID + "," + employeeID, and the value as Employee
     *
     * @param employeeSelection
     * @param useCache - If True, the interpreter will return previously used values
     * @return 
     */
    public HashSet<Employee> generateEmployeeList(EmployeeSelection employeeSelection, boolean useCache) {

        if (useCache) {
            // do we maybe have the result already in cache?
            HashSet result;

            result = (HashSet) internalCache.get(employeeSelection.hashCode());
            if (result != null) {
                return result;
            }
        }

        // no such luck, get it from the database
        HashSet employees = ((IEmployeeSelection) employeeSelection).getEmployees();
        if (employees != null) {
            // add result to cache
            internalCache.put(employeeSelection.hashCode(), employees);
            return employees;
        }

        return null;
    } // end generateEmployeeList


    private static SelectionInterpreter interpreter = null;
    private static LinkedHashMap internalCache = null;  // Key = employeeSelection.hashCode(), Value = LinkedList(employees)
} // end SelectionInterpreter

