package za.co.ucs.accsys.webmodule;

import java.io.File;
import java.sql.*;
import java.text.DecimalFormat;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;

/**
 * <p>
 * A WebProcess_LeaveRequisition is an extension of WebProcess that specializes
 * in LeaveRequests </p>
 */
public class WebProcess_LeaveRequisition extends WebProcess {

    /**
     * LeaveRequisition requires
     *
     * @param processDefinition - details of reporting structure, etc
     * @param owner - who created/is responsible for this process?
     * @param employee - which employee is requesting leave?
     * @param fromDate - String
     * @param toDate - String
     * @param leaveType - Name in database of leave type that the person wishes
     * to apply for leave
     * @param reason - [optional]
     * @param additionalDetail - [optional]
     * @param incidentID
     * @param attachment
     */
    public WebProcess_LeaveRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee,
            String fromDate, String toDate, String leaveType, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        super(processDefinition, owner, employee);

        this.fromDate = DatabaseObject.formatDate(fromDate);
        this.toDate = DatabaseObject.formatDate(toDate);
        this.halfday = false;
        setup(leaveType, reason, additionalDetail, incidentID, attachment);
        escalateInitialProcess();
    }

    public WebProcess_LeaveRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee,
            String fromDate, boolean halfDay, String leaveType, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        super(processDefinition, owner, employee);
        this.fromDate = DatabaseObject.formatDate(fromDate);
        this.toDate = this.fromDate;
        this.halfday = halfDay;
        setup(leaveType, reason, additionalDetail, incidentID, attachment);
        escalateInitialProcess();
    }

    public WebProcess_LeaveRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee,
            String fromDate, String toDate, int leaveTypeID, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        super(processDefinition, owner, employee);
        this.fromDate = DatabaseObject.formatDate(fromDate);
        this.toDate = DatabaseObject.formatDate(toDate);
        this.halfday = false;
        setup(leaveTypeID, reason, additionalDetail, incidentID, attachment);
        escalateInitialProcess();
    }

    public WebProcess_LeaveRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee,
            String fromDate, boolean halfDay, int leaveTypeID, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        super(processDefinition, owner, employee);
        this.fromDate = DatabaseObject.formatDate(fromDate);
        this.toDate = this.fromDate;
        this.halfday = halfDay;
        setup(leaveTypeID, reason, additionalDetail, incidentID, attachment);
        escalateInitialProcess();
    }

    // Accomodate hours
    public WebProcess_LeaveRequisition(WebProcessDefinition processDefinition, Employee owner, Employee employee,
            String fromDate, int hours, int minutes, int leaveTypeID, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        super(processDefinition, owner, employee);
        this.fromDate = DatabaseObject.formatDate(fromDate);
        this.toDate = DatabaseObject.formatDate(fromDate);
        this.halfday = false;
        this.hours = hours;
        this.minutes = minutes;
        this.useHours = true;
        setup(leaveTypeID, reason, additionalDetail, incidentID, attachment);
        escalateInitialProcess();
    }

    private void setup(String leaveType, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        //System.out.println("private void setup(String leaveType, String reason, String incidentID):" + leaveType + "," + reason + "," + incidentID);
        this.leaveType = leaveType;
        this.leaveTypeID = getLeaveTypeID(leaveType);
        if (reason == null) {
            this.reason = "Initial Request";
        } else {
            this.reason = reason;
        }
        if (additionalDetail == null) {
            this.additionalDetail = "";
        } else {
            this.additionalDetail = additionalDetail;
        }

        if (incidentID != null) {
            this.incidentID = new Integer(incidentID);
            this.reason = getIncidentReason(incidentID);
        }
        // Attachment
        if (attachment == null) {
            this.attachmentLink = "";
        } else {
            // If there is an attachment save it
            try {
                // Get the uploaded file parameters
                String ext = "." + FilenameUtils.getExtension(attachment.getName());
                if (ext.equalsIgnoreCase(".")) {
                    this.attachmentLink = "";
                } else {
                    String newFileName = this.hashCode() + ext;
                    //Need to get apache temp folder
                    String relativeWebPath = "\\temp";
                    String catalinaBase = System.getProperty("catalina.base");
                    String savePath = catalinaBase + relativeWebPath;
                    // Write the file
                    File uploadedFile = new File(savePath + FileContainer.getOSFileSeperator() + newFileName);
                    this.attachmentLink = savePath + FileContainer.getOSFileSeperator() + newFileName;
                    uploadedFile.getParentFile().mkdirs();
                    attachment.write(uploadedFile);//Write to disc
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    private void setup(int leaveTypeID, String reason, String additionalDetail, String incidentID, FileItem attachment) {
        //System.out.println("private void setup(String leaveType, String reason, String incidentID):" + leaveType + "," + reason + "," + incidentID);
        this.leaveTypeID = leaveTypeID;
        this.leaveType = getLeaveType(leaveTypeID);
        if (reason == null) {
            this.reason = "Initial Request";
        } else {
            this.reason = reason;
        }
        if (additionalDetail == null) {
            this.additionalDetail = "";
        } else {
            this.additionalDetail = additionalDetail;
        }

        if (incidentID != null) {
            this.incidentID = new Integer(incidentID);
            this.reason = getIncidentReason(incidentID);
        }
        // Attachment
        if (attachment == null) {
            this.attachmentLink = "";
        } else {
            // If there is an attachment save it
            try {
                // Get the uploaded file parameters
                String ext = "." + FilenameUtils.getExtension(attachment.getName());
                if (ext.equalsIgnoreCase(".")) {
                    this.attachmentLink = "";
                } else {
                    String newFileName = this.hashCode() + ext;
                    //Need to get apache temp folder
                    String relativeWebPath = FileContainer.getOSFileSeperator() + "temp";;
                    String catalinaBase = System.getProperty("catalina.base");
                    String savePath = catalinaBase + relativeWebPath;
                    // Write the file
                    File uploadedFile = new File(savePath + FileContainer.getOSFileSeperator() + newFileName);
                    this.attachmentLink = savePath + FileContainer.getOSFileSeperator() + newFileName;
                    uploadedFile.getParentFile().mkdirs();
                    attachment.write(uploadedFile);//Write to disc
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    private void escalateInitialProcess() {
        // Is this a valid leave request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            this.escalateProcess(et_INITIAL, this.reason);
        }
    }

    private int getLeaveTypeID(String leaveType) {
        // Establish LeaveType ID
        int aLeaveTypeID = -1;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select LEAVE_TYPE_ID from l_leave_type with (nolock) where "
                    + " upper(name)=upper('" + leaveType + "')", con);
            if (rs.next()) {
                aLeaveTypeID = rs.getInt("LEAVE_TYPE_ID");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return aLeaveTypeID;
    }

    private String getLeaveType(int leaveTypeID) {
        // Establish LeaveType
        String aLeaveType = "";
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select NAME from l_leave_type with (nolock) where "
                    + " leave_type_id=" + leaveTypeID, con);
            if (rs.next()) {
                aLeaveType = rs.getString("NAME");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return aLeaveType;
    }

    /**
     * Returns the unit of leave (Days, hours, etc.)
     *
     * @param leaveTypeID
     * @return
     */
    public String getLeaveUnit(int leaveTypeID) {
        String result = "Unknown Unit";
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select fn_L_GetCompiledUnit("
                    + this.getEmployee().getCompany().getCompanyID() + ", "
                    + this.getEmployee().getEmployeeID() + ", " + leaveTypeID + ")", con);
            if (rs.next()) {
                result = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the number of working days within the given date-range
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    public float getRequestedWorkingDays(java.util.Date fromDate, java.util.Date toDate) {
        if ((fromDate == null) || (toDate == null)) {
            //System.out.println("Requested Working Days for:" + this.getProcessName() + " returned 0 due to NULL date.");
            return 0;
        }
        float result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select fn_L_GetRequestedWorkDays("
                    + this.getEmployee().getCompany().getCompanyID() + ", "
                    + this.getEmployee().getEmployeeID() + ", "
                    + "'" + DatabaseObject.formatDate(fromDate) + "', '" + DatabaseObject.formatDate(toDate) + "')", con);
            if (rs.next()) {
                result = rs.getFloat(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the leave type for which leave was requested.
     *
     * @return
     */
    public String getLeaveType() {
        String result = "Unknown";
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select trim(NAME) as NAME from L_LEAVE_TYPE with (nolock) where LEAVE_TYPE_ID=" + this.leaveTypeID, con);
            if (rs.next()) {
                result = rs.getString("NAME");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns the reason found in ll_properties_lookup or l_illness_index for
     * the current incident_id
     *
     * @param incidentID
     * @return
     */
    public String getIncidentReason(String incidentID) {
        if (this.incidentID == null) {
            return "";
        }

        String sqlString = "";
        String result = this.reason;

        if (leaveType.trim().compareToIgnoreCase("Sick") == 0) {
            sqlString = "select description from l_illness_index with (nolock) where illness_id=" + incidentID;
        }
        if ((leaveType.trim().compareToIgnoreCase("Family") == 0)
                || (leaveType.trim().compareToIgnoreCase("Special") == 0)
                || (leaveType.trim().compareToIgnoreCase("Unpaid") == 0)) {
            sqlString = "select incident from ll_properties_lookup with (nolock) where incident_id=" + incidentID;
        }

        if (sqlString.length() == 0) {
            return result;
        }

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL(sqlString, con);
            if (rs.next()) {
                result = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Getter for property fromDate.
     *
     * @return Value of property fromDate.
     */
    public java.util.Date getFromDate() {
        return fromDate;
    }

    /**
     * Getter for property toDate.
     *
     * @return Value of property toDate.
     */
    public java.util.Date getToDate() {
        return toDate;
    }

    /**
     * This method should be implemented by each and every descendent of
     * WebProcess. It returns an instance of ValidationResult which contains
     * both the isValid() property as well as a reason in case of a non-valid
     * result.
     *
     * @return
     */
    @Override
    public ValidationResult validate() {
        if (isActive()) {

            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                // First check to see if it is a valid date (between "2000/01/01" and "2020/01/01"
                if (!DatabaseObject.betweenDates(this.fromDate, DatabaseObject.formatDate("2000/01/01"), DatabaseObject.formatDate("2020/01/01"))) {
                    DatabaseObject.releaseConnection(con);
                    return (new ValidationResult(false, "Invalid FROM date format [" + DatabaseObject.formatDate(this.fromDate) + "].  Please use (yyyy/mm/dd)."));
                }
                // First check to see if it is a valid date (between "2000/01/01" and "2020/01/01"
                if (!DatabaseObject.betweenDates(this.toDate, DatabaseObject.formatDate("2000/01/01"), DatabaseObject.formatDate("2020/01/01"))) {
                    DatabaseObject.releaseConnection(con);
                    return (new ValidationResult(false, "Invalid TO date format [" + DatabaseObject.formatDate(this.toDate) + "].  Please use (yyyy/mm/dd)."));
                }

                // Then we continue by checking weather there are sufficient leave days available.
                ResultSet rs1 = DatabaseObject.openSQL(getSufficientLeaveDaysStatement(), con);
                if (rs1.next()) {
                    int days = rs1.getInt(1);
                    //System.out.println("Days available:" + days);
                    if (days < 0) {
                        rs1.close();
                        DatabaseObject.releaseConnection(con);
                        return (new ValidationResult(false, "Insufficient days available."));
                    }
                }
                rs1.close();
                // Next, we check to see if the employee has not already taken leave during that period
                // BUT 
                // If this is a half-day leave request, allow it if there is already another half-day recorded
                if (isHalfday()) {
                    ResultSet rs2 = DatabaseObject.openSQL(getOverlappingHalfDayLeaveStatement(), con);
                    if (rs2.next()) {
                        if (rs2.getInt(1) > 0) {
                            rs2.close();
                            DatabaseObject.releaseConnection(con);
                            return (new ValidationResult(false, "Leave is already booked over this period."));
                        }
                    }
                    rs2.close();
                } else {
                    ResultSet rs2 = DatabaseObject.openSQL(getOverlappingLeaveStatement(), con);
                    if (rs2.next()) {
                        if (rs2.getInt(1) > 0) {
                            rs2.close();
                            DatabaseObject.releaseConnection(con);
                            return (new ValidationResult(false, "Leave is already booked over this period."));
                        }
                    }
                    rs2.close();
                }

                // Then we continue by checking weather there are sufficient resources available in the resource pool.
                ResultSet rs3 = DatabaseObject.openSQL("select DBA.fn_L_EnoughResources(" + this.getEmployee().getCompany().getCompanyID() + ","
                        + this.getEmployee().getEmployeeID() + ",'" + DatabaseObject.formatDate(this.fromDate) + "','"
                        + DatabaseObject.formatDate(this.toDate) + "')", con);
                if (rs3.next()) {
                    String ans = "" + rs3.getString(1);
                    if (ans.trim().length() > 0) {
                        if (ans.equalsIgnoreCase("N")) {
                            rs3.close();
                            DatabaseObject.releaseConnection(con);
                            return (new ValidationResult(false, "Insufficient resources available in pool."));
                        }
                    }
                }
                rs3.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }

        return (new ValidationResult(
                true, ""));
    }

    @Override
    public String toString() {
//        System.out.println("Leave Process - toString...");
//        System.out.println("getLeaveType() - " + getLeaveType());
//        System.out.println("getFromDate() - " + getFromDate());
//        System.out.println("getToDate() - " + getToDate());
//        System.out.println("leaveTypeID - " + leaveTypeID);
//        System.out.println("getLeaveUnit(this.leaveTypeID.intValue()) - " + getLeaveUnit(this.leaveTypeID.intValue()));

        StringBuilder result = new StringBuilder();
        if (isHalfday()) {
            result.append("Leave Type: ").append(getLeaveType()).append(" [").append(formatDate(getFromDate())).append("] "
                    + "\nHalf Day");
        } else if (useHours) {
            if (getLeaveUnit(this.leaveTypeID.intValue()).equalsIgnoreCase("Hours")) {
                double rslt = (getNbUnits() * getHoursPerDayFromPayroll());
                result.append("Leave Type: ").append(getLeaveType()).append(" [").append(formatDate(getFromDate())).append(" - ").append(formatDate(getToDate())).append("] " + "\n").append(new DecimalFormat("#.##").format(rslt)).append(" ").append(getLeaveUnit(this.leaveTypeID.intValue()));
            } else {
                result.append("Leave Type: ").append(getLeaveType()).append(" [").append(formatDate(getFromDate())).append(" - ").append(formatDate(getToDate())).append("] " + "\n").append(new DecimalFormat("#.##").format(getNbUnits())).append(" ").append(getLeaveUnit(this.leaveTypeID.intValue()));
            }
        } else {
            result.append("Leave Type: ").append(getLeaveType()).append(" [").append(formatDate(getFromDate())).append(" - ").append(formatDate(getToDate())).append("] " + "\n").append(getRequestedWorkingDays(getFromDate(), getToDate())).append(" ").append(getLeaveUnit(this.leaveTypeID.intValue()));
        }
        return result.toString();
    }

    @Override
    public String toHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append("Process Number: [").append(this.hashCode()).append("]<br>");
        result.append("<table class=process_detail><font size='-3'><tr><td>Leave Type:</td><td><i>").append(getLeaveType()).append("</i></td>");
        if (isHalfday()) {
            result.append("<td>[").append(formatDate(getFromDate())).append("]</td></tr>");
            result.append("<tr><td>Duration:</td><td>Half Day</td></tr>");
        } else {
            result.append("<td>[").append(formatDate(getFromDate())).append(" - ").append(formatDate(getToDate())).append("]</td></tr>");
            if (useHours) {
                if (getLeaveUnit(this.leaveTypeID.intValue()).equalsIgnoreCase("Hours")) {
                    double rslt = (getNbUnits() * getHoursPerDayFromPayroll());
                    result.append("<tr><td>Duration:</td><td>").append(new DecimalFormat("#.##").format(rslt));
                    result.append(" - ").append(getLeaveUnit(this.leaveTypeID.intValue())).append("</td></tr>");

                } else {
                    result.append("<tr><td>Duration:</td><td>").append(new DecimalFormat("#.##").format(getNbUnits()));
                    result.append(" - ").append(getLeaveUnit(this.leaveTypeID.intValue())).append("</td></tr>");
                }
            } else {
                result.append("<tr><td>Duration:</td><td>").append(getRequestedWorkingDays(getFromDate(), getToDate()));
                result.append(" - ").append(getLeaveUnit(this.leaveTypeID.intValue())).append("</td></tr>");
            }
        }
        // IF there is an attchment add it to the display
        if (this.attachmentLink != null && !(this.attachmentLink.equalsIgnoreCase(""))) {
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String formLink = baseURL + "Controller.jsp";
            result.append("<tr><td>Attachment:</td><td><a href=\"").append(formLink).append("?fileID=").append(hashCode()).append("\">Download</a></td></tr>");
        }
        // Close the table
        result.append("</font></table>");
        return result.toString();
    }

    /**
     * This statement gets executed when the process is to be written to the
     * database.
     */
    @Override
    String getCommitStatement() {
        // Compile the SQL statement used to validate weather the
        // employee can take leave or not.
        return prepareStatement(getCommitCONSTANTStatement());
    }

    /**
     * Performs a ReplaceAll call without generating exceptions on NULL values
     *
     * @param regex - The regex search string
     * @param replacement - replacement string
     * @param string
     * @return
     */
    private String replaceAll(String regex, String replacement, String string) {
        if (string == null) {
            return ("");
        } else {
            return (string.replaceAll(regex, replacement));
        }

    }

    /**
     * Internal method to perform parameter replacements
     */
    private String prepareStatement(String aStatementWithParameters) {
        String sqlString = aStatementWithParameters;
        sqlString = sqlString.replaceAll(":companyID", getEmployee().getCompany().getCompanyID().toString());
        sqlString = sqlString.replaceAll(":employeeID", getEmployee().getEmployeeID().toString());
        sqlString = sqlString.replaceAll(":leaveTypeID", leaveTypeID.toString());
        sqlString = sqlString.replaceAll(":fromDate", formatDate(fromDate));
        sqlString = sqlString.replaceAll(":toDate", formatDate(toDate));
        sqlString = sqlString.replaceAll(":reason", replaceAll("'", "`", reason));
        sqlString = sqlString.replaceAll(":additionalDetail", replaceAll("'", "`", additionalDetail));
        sqlString = sqlString.replaceAll(":processID", Integer.toString(this.hashCode()));
        if (this.incidentID != null) {
            sqlString = sqlString.replaceAll(":incidentID", this.incidentID.toString());
        } else {
            sqlString = sqlString.replaceAll(":incidentID", "null");
        }
        // Get the reasonID that matched the description
        LeaveInfo leaveInfo = FileContainer.getInstance().getLeaveInfoFromContainer(this.getEmployee());
        String[][] leaveReasons = leaveInfo.getLeaveReasons(leaveTypeID);
        String illnessID = "0";
        if (leaveReasons.length > 0) { // Is there leave Reasons for this leave type
            // Find the ID that matches the reason
            for (int row = 0; row < leaveReasons.length; row++) {
                if (leaveReasons[row][1].equalsIgnoreCase(reason)) {
                    illnessID = leaveReasons[row][0];
                }
            }
            sqlString = sqlString.replaceAll(":illnessID", illnessID);
        } else {
            sqlString = sqlString.replaceAll(":illnessID", "null");
        }
        if (this.attachmentLink != null && this.attachmentLink.length() > 0) {
            sqlString = sqlString.replaceAll(":proofYN", "'Y'");
        } else {
            sqlString = sqlString.replaceAll(":proofYN", "'N'");
        }
        return sqlString;
    }

    /**
     * SQL Statement to be used to check weather the process is still valid.
     * These statements contain replacement characters that will be 'replaced'
     * before being fed to the DatabaseComponent.
     *
     * @COMPANY_ID,
     * @EMPLOYEE_ID, etc.
     */
    private String getSufficientLeaveDaysStatement() {
        if (((leaveType.trim().compareToIgnoreCase("Family") == 0)
                || (leaveType.trim().compareToIgnoreCase("Special") == 0)
                || (leaveType.trim().compareToIgnoreCase("Unpaid") == 0))
                && (incidentID != null)) {
            //System.out.println(prepareStatement(getSufficientIncidentLeaveDaysCONSTANTStatement()));
            return prepareStatement(getSufficientIncidentLeaveDaysCONSTANTStatement());
        } else {
            return prepareStatement(getSufficientLeaveDaysCONSTANTStatement());
        }
    }

    /**
     * SQL Statement to be used to check if the employee is not applying for
     * leave over a period that leave is already booked for him/her. These
     * statements contain replacement characters that will be 'replaced' before
     * being fed to the DatabaseComponent.
     *
     * @COMPANY_ID,
     * @EMPLOYEE_ID, etc.
     */
    private String getOverlappingLeaveStatement() {
        return prepareStatement(getOverlappingLeaveCONSTANTStatement());
    }

    private String getOverlappingHalfDayLeaveStatement() {
        return prepareStatement(getOverlappingHalfDayLeaveCONSTANTStatement());
    }

    /**
     * How many hours does this guy qork per day?
     *
     * @return
     */
    public double getHoursPerDayFromPayroll() {
        double result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select HOURS_PER_DAY as RSLT from P_E_BASIC with (nolock) where COMPANY_ID=" + this.getEmployee().getCompany().getCompanyID()
                    + " and EMPLOYEE_ID=" + this.getEmployee().getEmployeeID(), con);
            if (rs.next()) {
                result = rs.getDouble("RSLT");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        if (!(result > 0)) {
            result = 8;
        }
        return result;
    }

    public double getNbUnits() {
        double result = 0;
        double hoursPerDay = getHoursPerDayFromPayroll();
        result = ((hours + (minutes / 60)) / hoursPerDay);
        return result;
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition
     za.co.ucs.accsys.peopleware.WebProcess_LeaveRequisition:    static final long serialVersionUID = -4699957810633474774L;
    
     */
    static final long serialVersionUID = -4699957810633474774L;
    private final java.util.Date fromDate;
    private final java.util.Date toDate;
    private boolean halfday = false;
    private boolean useHours = false;
    private EmployeeSelection initialEmployeeSelection;
    private String leaveType;
    private Integer leaveTypeID;
    private double hours;
    private double minutes;
    private Integer incidentID;
    private String reason;
    public String additionalDetail;
    private String attachmentLink;

    // We found that staff would apply for leave at a later date, and after it has been approved, 
    //  came back to apply for leave at an earlier date.  This resulted in the ESS/Leave system
    //  to evaluate the leave at the earlier date, ignoring the subsequent approved leave's impact
    //  on leave balances, and authorised the earlier leave too.
    // Solution: When someone applies for leave, instead of looking at the balace at the given date, 
    //  rather look at the balance at the latter of the given date and the already-authorised leave 
    //  stored in the system.  -- Cool, I think :-)
    private void getLatestApprovedLeaveEndDate() {

    }

    public String getAttachmentLink() {
        return attachmentLink;
    }

    public void setAttachmentLink(String attachmentLink) {
        // If there is no attachment make the link an empty string to avoid null errors
        if (attachmentLink == null) {
            attachmentLink = "";
        }
        this.attachmentLink = attachmentLink;
    }

    private String getSufficientLeaveDaysCONSTANTStatement() {
        return ("select fn_L_EstimateBalance(:companyID,:employeeID,:leaveTypeID,"
                + "greater((select coalesce(max(end_date),'1899/01/01') from l_history where company_id=:companyID and employee_id=:employeeID and leave_type_id=:leaveTypeID), ':fromDate')) + "
                //                + "':fromDate') + "
                + "fn_L_GetOverdraft(:companyID,:employeeID, :leaveTypeID) - "
                + "fn_L_GetRequestedWorkDays(:companyID,:employeeID,':fromDate', ':toDate')");
    }
    // Used for Family, Special, Unpaid

    private String getSufficientIncidentLeaveDaysCONSTANTStatement() {
        return ("select fn_L_IncidentBalance(:companyID,:employeeID,:leaveTypeID,:incidentID,"
                + "greater((select coalesce(max(end_date),'1899/01/01') from l_history where company_id=:companyID and employee_id=:employeeID and leave_type_id=:leaveTypeID), ':fromDate')) + "
                + "fn_L_GetOverdraft(:companyID,:employeeID, :leaveTypeID) - "
                + "fn_L_GetRequestedWorkDays(:companyID,:employeeID,':fromDate', ':toDate')");
    }

    private String getOverlappingLeaveCONSTANTStatement() {
        return ("select count(*) from l_history  with (NOLOCK) where company_id=:companyID and employee_id=:employeeID "
                + " and (( ':fromDate' BETWEEN START_DATE and END_DATE) or ( ':toDate' BETWEEN START_DATE and END_DATE) "
                + "      or (':fromDate'<=START_DATE and ':toDate'>=END_DATE)) and "
                + " cancel_date is null and convert_date is null");
    }

    private String getOverlappingHalfDayLeaveCONSTANTStatement() {
        return ("select count(*) from l_history  with (NOLOCK) where company_id=:companyID and employee_id=:employeeID "
                + " and (( ':fromDate' BETWEEN START_DATE and END_DATE) or ( ':toDate' BETWEEN START_DATE and END_DATE) "
                + "      or (':fromDate'<=START_DATE and ':toDate'>=END_DATE)) and "
                + " cancel_date is null and convert_date is null and nb_unit>=1");
    }

    private String getCommitCONSTANTStatement() {
        String nbUnits;
        if (useHours) {
            nbUnits = String.format("%f", getNbUnits());
        } else {
            nbUnits = "fn_L_GetRequestedWorkDays(:companyID,:employeeID,':fromDate',':toDate')";
            if (isHalfday()) {
                nbUnits = "0.5";
            }
        }

        return ("call fn_L_SaveLeaveHistory(:companyID,:employeeID,-1, :leaveTypeID, :incidentID,"
                + ":illnessID, null, null, null, 'Days', 'ESS [:processID]',"
                + "':reason', :proofYN, 'N', null, ':additionalDetail', ':fromDate', ':toDate',null,null, " + nbUnits + ", null,null,null,null);commit;");
    }

    @Override
    public String getProcessName() {
        return "Leave Request";
    }

    /**
     * Getter for property halfday.
     *
     * @return Value of property halfday.
     *
     */
    public boolean isHalfday() {
        return this.halfday;
    }

    /**
     * Setter for property halfday.
     *
     * @param halfday New value of property halfday.
     *
     */
    public void setHalfday(boolean halfday) {
        this.halfday = halfday;
    }
} // end LeaveRequisitionProcess

