package za.co.ucs.accsys.webmodule;

import java.util.*;
import java.util.LinkedList;

/**
 * Every Web Process goes through various stages. The WebProcess object contains
 * one ProcessStage at any one time. The completed ProcessStages gets stored as
 * History
 */
public class WebProcessHistory implements java.io.Serializable {

    /**
     * Returns all the SelectionLink Instances
     */
    public LinkedList getProcessStages() {
        LinkedList result = new LinkedList();
        result.addAll(processStages.values());
        return result;
    }

    /**
     * Adds a SelectionLink instance to this 'container' of links
     */
    public void addProcessStage(WebProcessStage processStage) {
        processStages.put(processStage.getCreationDate(), processStage);
    }

    /**
     * Removes a SelectionLink instance from this 'container' of links
     */
    public void removeProcessStage(WebProcessStage processStage) {
        this.processStages.remove(processStage.getCreationDate());
    }

    /**
     * Returns the first WebProcessStage.
     */
    public WebProcessStage getOriginProcessStage() {
        // Get first process in history
        WebProcessStage creationStage = null;
        if (processStages.size() > 0) {
            creationStage = (WebProcessStage) processStages.get(processStages.firstKey());
        }
        return creationStage;
    }

    /**
     * Returns the last WebProcessStage.
     */
    public WebProcessStage getLastProcessStage() {
        // Get first process in history
        WebProcessStage lastStage = null;
        if (processStages.size() > 0) {
            lastStage = (WebProcessStage) processStages.get(processStages.lastKey());
        }
        return lastStage;
    }

    /**
     * Returns all the processOwners (Employee-class) already used in this
     * process
     */
    public LinkedList getPreviousProcessOwners() {
        LinkedList result = new LinkedList();
        // Key Iterator (java.util.Date)
        Iterator iter = processStages.keySet().iterator();
        while (iter.hasNext()) {
            WebProcessStage stage = (WebProcessStage) processStages.get((java.util.Date) iter.next());
            result.add(stage.getOwner());
        }
        return result;
    }

    /*
     * Returns the first stage of the porcess that was approved
     */
    public WebProcessStage getFirstEscalatedStage() {
        // Original Stage
        Iterator iter = processStages.values().iterator();
        // Get second entry
        WebProcessStage nextStage = null;

        if (iter.hasNext()) {
            iter.next();
            if (iter.hasNext()) {
                nextStage = (WebProcessStage) iter.next();
            }
        }

        return nextStage;
    }

    /**
     * Returns the number of times this process has been approved. Required for
     * escalation purposes.
     */
    public int getNumberOfApprovedStages() {
        int result = 0;
        // Key Iterator (java.util.Date)
        // Start by making a copy of the TreeMap
        Iterator iter = processStages.values().iterator();
        while (iter.hasNext()) {
            WebProcessStage stage = (WebProcessStage) (iter.next());
            if (stage != getOriginProcessStage()) {
                //System.out.println(">>>Checking Process Stages:"+stage+"\nAction="+stage.getStageAction());
                if (stage.getStageAction().compareToIgnoreCase(WebProcessStage.sa_ACCEPT) == 0) {
                    result++;
                }
            }
        }
        return result;
    }

    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcessHistory
     */
    private static final long serialVersionUID = -1172167796462589455L;

    // We want to retain the processHistory in chronological order
    // so let's use a Treemap
    private TreeMap processStages = new TreeMap();

} // end ProcessHistory

