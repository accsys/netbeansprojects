/*
 * Version.java
 *
 * Created on 15 April 2005, 11:41
 */

package za.co.ucs.accsys.webmodule;

/**
 * This class is used to compare required vs. actual versions of the objects in the lwt.jar file
 * @author  lwt
 */
public class ESSVersion {
    
    /** Creates a new instance of Version */
    private ESSVersion() {
    }
    
    public static ESSVersion getInstance(){
        if (essVersion == null){
            essVersion = new ESSVersion();
        } 
        
        return essVersion;
    }
    
    public String getJarVersion(){
        return jarVersion;
    }
    
    private static ESSVersion essVersion = null;
    private String jarVersion = "11.28.04";
    
}
