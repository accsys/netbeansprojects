/*
 * IEmployeeSelection.java
 *
 * Created on May 26, 2004, 12:35 PM
 */

package za.co.ucs.accsys.webmodule;
import java.util.*;
import za.co.ucs.accsys.peopleware.Employee;

/**
 * IEmployeeSelection is an Interface that defines what an Employee Selection class should contain
 * @author  liam
 */
public interface IEmployeeSelection {
    // Loads employees from database / cache
    public HashSet<Employee>  getEmployees();
    
}
