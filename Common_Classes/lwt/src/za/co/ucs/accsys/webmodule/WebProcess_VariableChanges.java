/*
 * WebProcess_VariableXXXX.java
 *
 * Created on 22 April 2005, 11:02
 */
package za.co.ucs.accsys.webmodule;

import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * WebProcess_VariableChanges is a class that will control payroll-driven
 * requests. This is a specialization of WebProcess that knows how to handle
 * variables and the potential changing thereoff.
 *
 *
 * 'indicatorCoode' is the indicator code of the variable that will be handled
 * by the appropriate class. This variable can be different for different
 * employees in different payrolls. Should the employee be linked to more than
 * one variable with the given indicator code, the validate() method will return
 * false, and the process will be terminated immediately.
 *
 * Another feature of this kind of WebProcess is the ability to keep track of
 * other instances of the same process currently in progress. Should the same
 * employee request travel allowance more than once, the manager that approves
 * the process, should know what the current travel allowance value in the
 * database is, as well as any other request for the same process currently in
 * the pipe-line. We cannot assume that all the requests for employee A will be
 * sent to manager B for approval.
 *
 * @author lwt
 */
public abstract class WebProcess_VariableChanges extends za.co.ucs.accsys.webmodule.WebProcess {

    /**
     * Creates a new instance of WebProcess_VariableXXXX
     */
    public WebProcess_VariableChanges(WebProcessDefinition processDefinition, Employee owner, Employee employee, String indicatorCode, float value, String comment, String selectionID) {
        super(processDefinition, owner, employee, selectionID);
        this.value = value;
        this.comment = comment;
        this.indicatorCode = indicatorCode;
        ValidationResult validResult = validate();
        // if everything is OK, escalate
        if (validResult.isValid()) {
            variable = getVariable(this.getEmployee(), indicatorCode);
            // Get the initial employee selection
            if (selectionID.contains("null")) {
                this.initialEmployeeSelection = getCurrentStage().getEmployeeSelection();
            } else {
                this.initialEmployeeSelection = processDefinition.getReportingStructure().getEmployeeSelection(Integer.parseInt(selectionID));
            }
            escalateInitialProcess();
        } else {
            this.cancelProcess(validResult.getInvalidReason());
        }
    }

    /**
     * This static method determines the actual variable that will be used when
     * one creates a WebProcess_VariableChanges instance for this
     * employee/indicatorCode combination. It can be used by the Web interface
     * to create a dynamic title for different employees.
     */
    public static Variable getVariable(Employee employee, String indicatorCode) {
        String rslt = null;
        if (validate(employee, indicatorCode).isValid()) {
            rslt = runQuery("select first variable_id from p_variable where upper(ind_code) like "
                    + "'%" + indicatorCode.toUpperCase() + "%' and variable_id in ("
                    + "  select variable_id from pl_emaster_pvariable "
                    + " where company_id=" + employee.getCompany().getCompanyID()
                    + " and employee_id=" + employee.getEmployeeID()
                    + ")");
            return (new Variable(rslt));
        } else {
            return null;
        }
    }

    /**
     * Returns true if a. Only one variable is linked to this employee with that
     * indicator code, ...
     */
    private static boolean canUtilise(Employee employee, String indicatorCode) {
        return validate(employee, indicatorCode).isValid();
    }

    private void escalateInitialProcess() {
        // Is this a valid leave request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            this.escalateProcess(et_INITIAL, this.comment, this.initialEmployeeSelection);
        }
    }

    @Override
    String getCommitStatement() {
        // Compile the SQL statement used to validate weather the
        // employee can take leave or not.
        return prepareStatement(getCommitCONSTANTStatement());
    }

    /**
     * Internal method to perform parameter replacements
     */
    private String prepareStatement(String aStatementWithParameters) {
        String sqlString = aStatementWithParameters;
        sqlString = sqlString.replaceAll(":companyID", getEmployee().getCompany().getCompanyID().toString());
        sqlString = sqlString.replaceAll(":employeeID", getEmployee().getEmployeeID().toString());
        sqlString = sqlString.replaceAll(":variableID", this.variable.getVariableID());
        sqlString = sqlString.replaceAll(":value", Float.toString(this.value));

        return sqlString;
    }

    @Override
    public String getProcessName() {
        return "Value Logging:" + this.variable.getName();
    }

    @Override
    public String toHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append("Process Number:  [").append(this.hashCode()).append("]<br>");
        result.append("<table class=displaytable_inner>");
        result.append("  <tr>");
        result.append("    <td>Variable: </td><td>").append(this.variable.getName()).append("</td>");
        result.append("  </tr>");
        result.append("  <tr>");
        result.append("    <td>Amount: </td><td>").append(this.value).append("</td>");
        result.append("  </tr>");
        result.append("  <tr>");
        result.append("    <td>Comment: </td><td>").append(this.comment).append("</td>");
        result.append("  </tr>");
        result.append("  <tr>");
        float currentValue = this.variable.getValue(getEmployee(), getOpenPayrollDetailID(getEmployee()), "1");
        result.append("    <td>Current Value: </td><td>").append(Float.toString(currentValue)).append("</td>");
        result.append("  </tr>");
        result.append("  </table>");

        // Let us show the previous requests for the same variable, that are still active
        if (getOtherActiveRequests().size() > 0) {
            result.append("<br>Other requests on this variable that are still active.");
            result.append(getOtherRequestInHTML());
        }

        return result.toString();
    }

    /**
     * Returns an HTML-formatted list of other request by this employee on the
     * same variable, that are still active.
     */
    public String getOtherRequestInHTML() {
        DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();
        StringBuilder result = new StringBuilder();
        // Let us show the previous requests for the same variable, that are still active
        if (getOtherActiveRequests().size() > 0) {
            result.append("<table class=process_detail><font size=\"-1\">");
            result.append("<tr><td>Requested</td><td>Value</td><td>Comment</td><td>Current Owner</td></tr>");
            LinkedList requests = getOtherActiveRequests();
            for (int i = 0; i < requests.size(); i++) {
                WebProcess_VariableChanges webRequest = (WebProcess_VariableChanges) requests.get(i);
                result.append("<tr><td>").append(DatabaseObject.formatDate(webRequest.getCreationDate())).append("</td>");
                result.append("    <td>").append(webRequest.getValue()).append("</td>");
                result.append("    <td>").append(webRequest.getComment()).append("</td>");
                result.append("    <td>").append(webRequest.getCurrentStage().getOwner()).append("</td></tr>");
            }
            result.append("</table>");
        }

        return result.toString();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Submit Variable Value: ").append(this.variable.getDescription());
        result.append("\n\tValue=").append(this.value);
        result.append("\n\tComment=").append(this.comment);
        return result.toString();

    }

    @Override
    public final ValidationResult validate() {
        return validate(this.getEmployee(), this.indicatorCode);
    }

    /* Validation :
     */
    public static ValidationResult validate(Employee employee, String indicatorCode) {
        // Linked to a payroll?
        String nonValidReason = "";
        if (!validate_isEmployeeLinkedToPayroll(employee)) {
            nonValidReason = "Employee not linked to a Payroll.";
            //System.out.println("IndCode:" + indicatorCode + " - " + nonValidReason);
            return (new ValidationResult(false, nonValidReason));
        }

        // Payroll currently in Basic Run and employee not yet closed?
        if (!validate_isEmployeeInBasicRunAndActive(employee)) {
            nonValidReason = "Employee is already closed, or the payroll is not in a Basic Run";
            //System.out.println("IndCode:" + indicatorCode + " - " + nonValidReason);
            return (new ValidationResult(false, nonValidReason));
        }

        // Payroll is not Labour Costing
        if (!validate_payrollNotLabourCosting(employee)) {
            nonValidReason = "Not available on Labour Costing payrolls.";
            //System.out.println("IndCode:" + indicatorCode + " - " + nonValidReason);
            return (new ValidationResult(false, nonValidReason));
        }

        // No variable with indicator code linked to employee
        if (validate_noVariableWithIndicatorCodeLinked(employee, indicatorCode)) {
            nonValidReason = "There are not variables with this Indicator Code assigned to this employee.";
            //System.out.println("IndCode:" + indicatorCode + " - " + nonValidReason);
            return (new ValidationResult(false, nonValidReason));
        }

        // Too many variables with indicator code linked to employee
        if (validate_multipleVariablesWithIndicatorCodeLinked(employee, indicatorCode)) {
            nonValidReason = "There are more than one variable with this Indicator Code assigned to this employee.";
            //System.out.println("IndCode:" + indicatorCode + " - " + nonValidReason);
            return (new ValidationResult(false, nonValidReason));
        }

        return (new ValidationResult(true, "All OK"));
    }

    private static boolean validate_isEmployeeLinkedToPayroll(Employee employee) {
        return (employee.getPayroll() != null);
    }

    private static boolean validate_isEmployeeInBasicRunAndActive(Employee employee) {
        String rslt = runQuery("select count(*) from p_calc_employeelist  with (NOLOCK) "
                + " where company_id=" + employee.getCompany().getCompanyID()
                + " and employee_id=" + employee.getEmployeeID()
                + " and payroll_id=" + employee.getPayroll().getPayrollID()
                + " and payrolldetail_id=fn_P_GetOpenPeriod(company_id, payroll_id) "
                + " and runtype_id=fn_P_GetOpenRunType(company_id, payroll_id, fn_P_GetOpenPeriod(company_id, payroll_id))"
                + " and closed_yn='N' and runtype_id=1");

        return (Integer.valueOf(rslt).intValue() > 0);
    }

    private static boolean validate_payrollNotLabourCosting(Employee employee) {
        String rslt = runQuery("select fn_P_IsLabourCostingRegistered(" + employee.getCompany().getCompanyID()
                + ", " + employee.getPayroll().getPayrollID() + ")");

        return (Integer.valueOf(rslt).intValue() == 0);
    }

    // Is only one variable in this payroll with the given indicator code, linked
    // to this employee?
    // And active for a Basic Run
    private static boolean validate_noVariableWithIndicatorCodeLinked(Employee employee, String indicatorCode) {
        String rslt = runQuery("select count(*) from p_variable  with (NOLOCK) where upper(ind_code) like "
                + " '%" + indicatorCode + "%' and fn_P_VariableActiveForRuntype_YN(variable_id, 1)='Y' "
                + " and fn_P_VariableIsLinked_YN(" + employee.getCompany().getCompanyID() + ", "
                + employee.getEmployeeID() + ", variable_id)='Y'");

        return (Integer.valueOf(rslt).intValue() == 0);
    }

    // Is only one variable in this payroll with the given indicator code, linked
    // to this employee?
    // And active for a Basic Run
    private static boolean validate_multipleVariablesWithIndicatorCodeLinked(Employee employee, String indicatorCode) {
        String rslt = runQuery("select count(*) from p_variable  with (NOLOCK) where upper(ind_code) like "
                + " '%" + indicatorCode + "%' and fn_P_VariableActiveForRuntype_YN(variable_id, 1)='Y' "
                + " and fn_P_VariableIsLinked_YN(" + employee.getCompany().getCompanyID() + ", "
                + employee.getEmployeeID() + ", variable_id)='Y'");

        return (Integer.valueOf(rslt).intValue() > 1);
    }

    /**
     * Returns a LinkedList of WebProcesses currently in the system, that are
     * also requesting updates for the same employee, for the same variable.
     */
    private LinkedList getOtherActiveRequests() {
        TreeMap allRequests;
        LinkedList requestsForThisVariable = new LinkedList();
        if (!FileContainer.isBusyCreatingFileContainer) {
            allRequests = FileContainer.getInstance().getWebProcessesInitiatedBy(this.getEmployee(), true);

            Iterator iter = allRequests.values().iterator();
            while (iter.hasNext()) {
                WebProcess process = (WebProcess) iter.next();
                // Is this also a WebProcess_VariableChanges?
                if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase(this.getProcessDefinition().getProcessClassName()) == 0) {
                    // Is this a request for the same variable?
                    if (WebProcess_VariableChanges.getVariable(this.getEmployee(), indicatorCode) != null) {
                        if (WebProcess_VariableChanges.getVariable(this.getEmployee(), indicatorCode).getVariableID().compareToIgnoreCase(this.variable.getVariableID()) == 0) {
                            // Is this a different request from the one we're currently in?
                            if (process.hashCode() != this.hashCode()) {
                                requestsForThisVariable.add(process);
                            }
                        }
                    }
                }
            }
        }

        return requestsForThisVariable;
    }

    /**
     * Returns the currently stored value of the given variable in the database
     */
    public float getCurrentVariableValueInDB() {
        return this.variable.getValue(this.getEmployee(), getOpenPayrollDetailID(this.getEmployee()), getOpenRunTypeID(this.getEmployee()));
    }

    private String getOpenPayrollDetailID(Employee employee) {
        String rslt = runQuery("select fn_P_GetOpenPeriod(" + employee.getCompany().getCompanyID()
                + ", " + employee.getPayroll().getPayrollID() + ")");

        return (rslt);
    }

    private String getOpenRunTypeID(Employee employee) {
        String rslt = runQuery("select fn_P_GetOpenRunType(" + employee.getCompany().getCompanyID()
                + ", " + employee.getPayroll().getPayrollID() + ", " + getOpenPayrollDetailID(employee) + " )");

        return (rslt);
    }

    /**
     * Internal method to run simple SQL queries
     */
    private static String runQuery(String sql) {
        String result = new String();
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL(sql, con);
            if (rs.next()) {
                result = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    private String getCommitCONSTANTStatement() {
        return ("if (select temp_value from pl_emaster_pvariable where "
                + "company_id=:companyID and employee_id=:employeeID and variable_id=:variableID ) is null "
                + " then "
                + "   update pl_emaster_pvariable set temp_value=:value where "
                + "   company_id=:companyID and employee_id=:employeeID and variable_id=:variableID "
                + " else "
                + "   update pl_emaster_pvariable set temp_value=temp_value+:value where "
                + "   company_id=:companyID and employee_id=:employeeID and variable_id=:variableID; "
                + " end if");
    }

    /**
     * Getter for property value.
     *
     * @return Value of property value.
     *
     */
    public float getValue() {
        return value;
    }

    /**
     * Getter for property comment.
     *
     * @return Value of property comment.
     *
     */
    public java.lang.String getComment() {
        return comment;
    }
    private float value = 0;
    private String comment;
    private String indicatorCode;
    private Variable variable = null;
    private EmployeeSelection initialEmployeeSelection;
}
