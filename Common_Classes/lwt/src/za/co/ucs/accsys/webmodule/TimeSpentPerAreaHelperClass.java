/*
 * This class is used in the WebModule (time_stats.jsp) to display time spent per area over a period of time
 */
package za.co.ucs.accsys.webmodule;

import java.util.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 *
 * @author lterblanche
 */
public class TimeSpentPerAreaHelperClass {

    private Queue<TimeSpentPerArea> TimeSpentPerAreaQueue = new LinkedList<TimeSpentPerArea>();

    public void addTimeSpentPerArea(java.util.Date theDay, String theArea, int minutesSpent, String timeSpentFormatted) {
        TimeSpentPerAreaQueue.add(new TimeSpentPerArea(theDay, theArea, minutesSpent, timeSpentFormatted));
    }

    /*
     * Returns a HashSet with the areas (String)
     */
    public HashSet getAreas() {
        HashSet result = new HashSet();
        Iterator iter = TimeSpentPerAreaQueue.iterator();
        while (iter.hasNext()) {
            TimeSpentPerArea anItem = (TimeSpentPerArea) iter.next();
            result.add(anItem.theArea);
        }
        return result;
    }

    /** Returns the time spent (in minutes) for a given area on a given date
     */
    public int getMinutesSpentPerDayPerArea(String theDay, String theArea) {
        int result = 0;
        Iterator iter = TimeSpentPerAreaQueue.iterator();
        while (iter.hasNext()) {
            TimeSpentPerArea ts = (TimeSpentPerArea) iter.next();
            if ((ts.theArea.equals(theArea)) && (DatabaseObject.formatDate(ts.theDay).equals(theDay))) {
                // FailSafe
                if (ts.minutesSpent > (24 * 60 * 60)) {
                    return -(10);
                } else {
                    return ts.minutesSpent;
                }
            }
        }
        return result;
    }

    /** Returns the time spent (in minutes) for a given area on a given date
     */
    public String getDisplayMinutesSpentPerDayPerArea(String theDay, String theArea) {
        String result = "";
        Iterator iter = TimeSpentPerAreaQueue.iterator();
        while (iter.hasNext()) {
            TimeSpentPerArea ts = (TimeSpentPerArea) iter.next();
            if ((ts.theArea.equals(theArea)) && (DatabaseObject.formatDate(ts.theDay).equals(theDay))) {
                // FailSafe
                if (ts.minutesSpent > (24 * 60 * 60)) {
                    return "Out of Range";
                } else {
                    if (ts.minutesSpent == 0) {
                        return "00:00";
                    } else {
                        return ts.timeSpentFormatted;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns a HashSet (String) of all the unique days
     */
    public HashSet getDays() {
        HashSet result = new HashSet();
        Iterator iter = TimeSpentPerAreaQueue.iterator();
        while (iter.hasNext()) {
            TimeSpentPerArea anItem = (TimeSpentPerArea) iter.next();
            result.add((String) DatabaseObject.formatDate(anItem.theDay));
        }
        return result;
    }

    private class TimeSpentPerArea {

        private TimeSpentPerArea(java.util.Date theDay, String theArea, int minutesSpent, String timeSpentFormatted) {
            this.minutesSpent = minutesSpent;
            this.theArea = theArea;
            this.theDay = theDay;
            this.timeSpentFormatted = timeSpentFormatted;
        }
        private java.util.Date theDay;
        private String theArea;
        private int minutesSpent;
        private String timeSpentFormatted;

        /**
         * @return the theDay
         */
        public java.util.Date getTheDay() {
            return theDay;
        }

        /**
         * @param theDay the theDay to set
         */
        public void setTheDay(java.util.Date theDay) {
            this.theDay = theDay;
        }

        /**
         * @return the theArea
         */
        public String getTheArea() {
            return theArea;
        }

        /**
         * @param theArea the theArea to set
         */
        public void setTheArea(String theArea) {
            this.theArea = theArea;
        }

        /**
         * @return the minutesSpent
         */
        public int getMinutesSpent() {
            return minutesSpent;
        }

        /**
         * @param minutesSpent the minutesSpent to set
         */
        public void setMinutesSpent(int minutesSpent) {
            this.minutesSpent = minutesSpent;
        }

        /**
         * @return the timeSpentFormatted
         */
        public String getTimeSpentFormatted() {
            return timeSpentFormatted;
        }

        /**
         * @param timeSpentFormatted the timeSpentFormatted to set
         */
        public void setTimeSpentFormatted(String timeSpentFormatted) {
            this.timeSpentFormatted = timeSpentFormatted;
        }
    }
}
