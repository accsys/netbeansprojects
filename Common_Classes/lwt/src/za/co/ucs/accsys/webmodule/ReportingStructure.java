/**
 * Java class "Structure.java" generated from Poseidon for UML. Poseidon for UML
 * is developed by <A HREF="http://www.gentleware.com">Gentleware</A>. Generated
 * with <A HREF="http://jakarta.apache.org/velocity/">velocity</A> template
 * engine.
 */
package za.co.ucs.accsys.webmodule;

import java.sql.*;
import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * Reporting Structures are the back-bone of the Web Processes. Whenever a
 * process gets initiated, it needs to use a reporting structure to
 * propagate/escalate the request to the next level, until it reaches the top of
 * the hierarchy from where the transaction gets committed.
 * </p>
 */
public class ReportingStructure implements java.io.Serializable, java.lang.Comparable {

    public ReportingStructure(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Returns all the SelectionLink Instances
     */
    public LinkedList<SelectionLink> getSelectionLinks() {
        return selectionLinks;
    }

    /**
     * Adds a SelectionLink instance to this 'container' of links Remember that
     * an EmployeeSelection can at any one time only report to one other
     * EmployeeSelection. I.E. Many es's can report to one es, but one es can
     * only report to one es. Thus, when adding a selectionLink that contradicts
     * with an existing link, the existing link will be replaced.
     */
    public void addSelectionLink(SelectionLink selectionLink) {
        // Does this link cause a circular reference?
        if (isCircular(selectionLink)) {
            return;
        }
        // If the reportsFrom selection is already reporting to another selection,
        // replace it
        if (getParentSelection(selectionLink.getReportsFrom()) != null) {
            selectionLinks.remove(getSelectionLink(selectionLink.getReportsFrom(), getParentSelection(selectionLink.getReportsFrom())));
        }
        if (!this.selectionLinks.contains(selectionLink)) {
            this.selectionLinks.add(selectionLink);
        }
    }

    @Override
    public int hashCode() {
        int v = name.compareTo("ReportingStructure:");
        return v;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReportingStructure other = (ReportingStructure) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /**
     * Check to see weather, if this link is applied, we would be sitting with a
     * circular link
     */
    private boolean isCircular(SelectionLink selectionLink) {
        EmployeeSelection evaluation = selectionLink.getReportsTo();
        EmployeeSelection newEntry = selectionLink.getReportsFrom();
        // Step through the parents until we'return either at the top, or
        //  we run into the same Employee Selection
        while (getParentSelection(evaluation) != null) {
            if (getParentSelection(evaluation).equals(newEntry)) {
                return true;
            } else {
                evaluation = getParentSelection(evaluation);
            }
        }
        return false;

    }

    /**
     * Returns the selection link between these two Employee Selections
     */
    public SelectionLink getSelectionLink(EmployeeSelection fromEmployeeSelection, EmployeeSelection toEmployeeSelection) {
        for (int i = 0; i < selectionLinks.size(); i++) {
            SelectionLink link = (SelectionLink) selectionLinks.get(i);
            if ((link.getReportsFrom().equals(fromEmployeeSelection))
                    && (link.getReportsTo().equals(toEmployeeSelection))) {
                return (link);
            }
        }
        return null;
    }

    /**
     * Returns all the selection links containing this Employee Selection
     */
    public ArrayList<SelectionLink> getSelectionLinks(EmployeeSelection employeeSelection) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < selectionLinks.size(); i++) {
            SelectionLink link = (SelectionLink) selectionLinks.get(i);
            if ((link.getReportsFrom().equals(employeeSelection))
                    || (link.getReportsTo().equals(employeeSelection))) {
                result.add(link);
            }
        }
        return result;
    }

    /**
     * Removes a SelectionLink instance from this 'container' of links
     */
    public void removeSelectionLink(SelectionLink selectionLink) {
        //System.out.println("Removing selection link:"+selectionLink);
        this.selectionLinks.remove(selectionLink);
    }

    /**
     * Returns a LinkedList with ALL the employees in the database not linked in
     * some form or another in this Reporting Structure
     */
    public ArrayList<Employee> getUnlinkedEmployees() {
        ArrayList<Employee> allEmployeesInEmployeeSelections = getAllEmployeesInEmployeeSelections();
        ArrayList<Employee> allEmployeesInDatabase = getAllEmployeesInDatabase();

        // Traverse through all the employees in the database, taking out those found in
        // employee selections, leaving us with, yip, the unlinked employees
        for (int i = 0; i < allEmployeesInEmployeeSelections.size(); i++) {
            allEmployeesInDatabase.remove(allEmployeesInEmployeeSelections.get(i));
        }
        return allEmployeesInDatabase;
    } // end getUnlinkedEmployees

    /**
     * Create a list of every employee contained in any of the
     * EmployeeSelections of this Structure
     */
    public ArrayList<Employee> getAllEmployeesInEmployeeSelections() {
        HashSet result = new HashSet();

        // Step through all the EmployeeSelections contained in the SelectionLinks
        for (int i = 0; i < selectionLinks.size(); i++) {
            // ** ReportsFrom **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            HashSet reportsFrom = SelectionInterpreter.getInstance().generateEmployeeList(((SelectionLink) selectionLinks.get(i)).getReportsFrom());
            result.addAll(reportsFrom);

            // ** ReportsTo **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            HashSet reportsTo = SelectionInterpreter.getInstance().generateEmployeeList(((SelectionLink) selectionLinks.get(i)).getReportsTo());
            result.addAll(reportsTo);
        }

        LinkedHashMap temp = new LinkedHashMap();
        temp.putAll(temp);
        return new ArrayList(result);
    }

    /**
     * Create a list of every employee contained in any of the
     * EmployeeSelections of this Structure
     * @param employeeSelection
     * @return 
     */
    public ArrayList<Employee> getAllEmployeesInEmployeeSelection(EmployeeSelection employeeSelection) {

        HashSet employeesInSelection = SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection);
        return new ArrayList(employeesInSelection);
    }
    
    @Override
    public int compareTo(Object o) {
        return (this.toString().compareToIgnoreCase(o.toString()));
    }

    /**
     * Creates a list of every employee in the database for the given company
     */
    private ArrayList<Employee> getAllEmployeesInDatabase() {
        ArrayList result = new ArrayList();

        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select COMPANY_ID, EMPLOYEE_ID from e_master where active_yn='Y'", con);
            while (rs.next()) {
                result.add(new Employee(new Company(new Integer(rs.getInt("COMPANY_ID"))), new Integer(rs.getInt("EMPLOYEE_ID"))));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    /**
     * Returns a LinkedList of EmployeeSelections that are 'direct children' of
     * the given EmployeeSelection
     */
    public LinkedList<EmployeeSelection> getChildEmployeeSelections(EmployeeSelection employeeSelection) {
        LinkedList result = new LinkedList();
        Iterator iter = selectionLinks.iterator();
        while (iter.hasNext()) {
            SelectionLink link = (SelectionLink) iter.next();
            boolean same = link.getReportsTo().equals(employeeSelection);
            if (same) {
                EmployeeSelection reportsFrom = link.getReportsFrom();
                result.add(reportsFrom);
            }
        }
        return result;
    }

    /**
     * Returns the LinkedList of EmployeeSelections that are on top of the
     * hierarchy. Preferably, we want only one top level.
     */
    public ArrayList<EmployeeSelection> getTopNodes() {
        HashSet result = new HashSet();
        Iterator iter = selectionLinks.iterator();
        while (iter.hasNext()) {
            SelectionLink link = (SelectionLink) iter.next();
            EmployeeSelection reportsTo = link.getReportsTo();
            if (getLevelsFromTop(reportsTo) == 0) {
                result.add(reportsTo);
            }
        }
        return new ArrayList(result);
    }

    /**
     * Returns all the employees that are directly or indirectly located under
     * the current employee in the reporting structure. Note: This can also
     * include the employees that share the same level than this employee in the
     * reportingStructure
     */
    public LinkedList<Employee> getAllChildEmployees(Employee employee, EmployeeSelection employeeSelection) {
        HashSet result = new HashSet();
        result = getAllChildEmployees(employeeSelection, result, false);
        return (new LinkedList(result));
    } // end getChildEmployees

    private HashSet<Employee> getAllChildEmployees(EmployeeSelection employeeSelection, HashSet interimResult, boolean includeEmployeesInThisSelection) {
        // Employees in this selection
        HashSet employeesInSelection = SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection);
        // If necessary, add it to the total list
        if (includeEmployeesInThisSelection) {
            interimResult.addAll(employeesInSelection);
        }

        // Now, recursively call the selections under this one
        LinkedList childrenEmployeeSelections = getChildSelections(employeeSelection);
        for (int i = 0; i < childrenEmployeeSelections.size(); i++) {
            HashSet childEmployees = getAllChildEmployees((EmployeeSelection) childrenEmployeeSelections.get(i), interimResult, true);
            interimResult.addAll(childEmployees);
        }

        return interimResult;
    }

    /**
     * Returns a LinkedList of all EmployeeSelections that are descendants of
     * the given EmployeeSelection
     */
    public LinkedList getAllChildEmployeeSelections(EmployeeSelection employeeSelection) {
        TreeMap tm = new java.util.TreeMap();
        LinkedList result = new LinkedList();
        Collection c = getAllChildEmployeeSelections(employeeSelection, tm).values();
        Iterator iter = c.iterator();
        while (iter.hasNext()) {
            result.add(iter.next());
        }
        return (result);
    } // end getChildEmployees

    /**
     * Returns a LinkedList of all EmployeeSelections that are descendants of
     * the given EmployeeSelection. USed by the public method to traverse
     * through all the underlying reporting structures.
     */
    private TreeMap getAllChildEmployeeSelections(EmployeeSelection employeeSelection, TreeMap interimResult) {

        // Now, recursively call the selections under this one
        LinkedList childrenEmployeeSelections = getChildSelections(employeeSelection);
        for (int i = 0; i < childrenEmployeeSelections.size(); i++) {
            // Add the selection
            EmployeeSelection es = (EmployeeSelection) childrenEmployeeSelections.get(i);
            interimResult.put(es.getName(), es);
            interimResult.putAll(getAllChildEmployeeSelections(es, interimResult));
        }

        return interimResult;
    }

    /**
     * Depending on the property in the config.ini the method will either return
     * the EMployee-Selection closest to the top of the reporting structure
     * look_for_shortest_route=True or closest to the bottom of the reporting
     * structure look_for_shortest_route=False
     */
    public EmployeeSelection getEmployeeSelection(Employee employee) {
        return (getEmployeeSelection(employee, (WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.WORKFLOW_UseShortestRoute, false))));
    }

    /**
     * Depending on the property useShortestRoute either returns the
     * EMployee-Selection closest to the top of the reporting structure
     * look_for_shortest_route=True or closest to the bottom of the reporting
     * structure look_for_shortest_route=False
     *
     * @param useShortestRoute True - Locates employee highest on the hierarchy,
     * False - locates lowest point of entry
     */
    public EmployeeSelection getEmployeeSelection(Employee employee, boolean useShortestRoute) {
        if (useShortestRoute) {
            return getHighestLevel(employee);
        } else {
            return getLowestLevel(employee);
        }
    }

    /**
     * Returns a linked list of all EmployeeSelection objects in this reporting
     * structure that contains this employee
     */
    public LinkedList<EmployeeSelection> getAllEmployeeSelections(Employee employee) {
        LinkedList<EmployeeSelection> result = new LinkedList<EmployeeSelection>();
        for (int i = 0; i < selectionLinks.size(); i++) {
            EmployeeSelection selectionFrom = ((SelectionLink) selectionLinks.get(i)).getReportsFrom();
            if (isEmployeeInSelection(employee, selectionFrom)) {
                result.add(selectionFrom);
            }
            EmployeeSelection selectionTo = ((SelectionLink) selectionLinks.get(i)).getReportsTo();
            if (isEmployeeInSelection(employee, selectionTo)) {
                result.add(selectionTo);
            }
        }
        return result;
    }

    /**
     * Returns true if the current user is 'above' the selection in the
     * reporting structure
     */
    public boolean isUserParentOfSelection(Employee user, EmployeeSelection selection) {
        return (getAllParentEmployees(selection).contains(user));
    }

    /**
     * Returns the employeeselection with the given HashCode
     */
    public EmployeeSelection getEmployeeSelection(int hashCode) {
        for (int i = 0; i < selectionLinks.size(); i++) {
            EmployeeSelection selection = ((SelectionLink) selectionLinks.get(i)).getReportsFrom();
            if (selection.hashCode() == hashCode) {
                return selection;
            }
        }
        return null;
    }

    /**
     * This method returns the Employee selection that depicts the employee on
     * the highest possible level in the hierarchy. If an employee is located in
     * more than one employee selection, the algorithm will be done in such a
     * way that it returns the EmployeeSelection that has the least number of
     * layers before it reaches the top level
     */
    private EmployeeSelection getHighestLevel(Employee employee) {
        EmployeeSelection result = null;
        int levelsFromTop = 1000;

        // Step through all the EmployeeSelections contained in the SelectionLinks
        for (int i = 0; i < selectionLinks.size(); i++) {
            // ** ReportsFrom **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            EmployeeSelection reportsFrom = ((SelectionLink) selectionLinks.get(i)).getReportsFrom();
            if (isEmployeeInSelection(employee, reportsFrom)) {
                int newLevelsFromTop = getLevelsFromTop(reportsFrom);
                if (levelsFromTop > newLevelsFromTop && newLevelsFromTop > 1) {
                    levelsFromTop = newLevelsFromTop;
                    result = reportsFrom;
                    //System.out.println(employee+" found in 'getHighestLevel' reporting from "+reportsFrom.getName());
                }
            }

            // ** ReportsTo **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            EmployeeSelection reportsTo = ((SelectionLink) selectionLinks.get(i)).getReportsTo();
            if (isEmployeeInSelection(employee, reportsTo)) {
                int newLevelsFromTop2 = getLevelsFromTop(reportsFrom);
                if (levelsFromTop > newLevelsFromTop2 && newLevelsFromTop2 > 1) {
                    levelsFromTop = newLevelsFromTop2;
                    result = reportsTo;
                    //System.out.println(employee+" found in 'getHighestLevel' reporting to "+reportsFrom.getName());
                }
            }
        }
        if (result != null) {
            //System.out.println(employee+"'s highest level:"+result.getName());
        }

        return result;
    }

    /**
     * This method returns the Employee selection that depicts the employee on
     * the lowest possible level in the hierarchy. If an employee is located in
     * more than one employee selection, the algorithm will be done in such a
     * way that it returns the EmployeeSelection that has the largest number of
     * layers before it reaches the top level
     */
    private EmployeeSelection getLowestLevel(Employee employee) {
        EmployeeSelection result = null;
        int levelsFromTop = 0;

        // Step through all the EmployeeSelections contained in the SelectionLinks
        for (int i = 0; i < selectionLinks.size(); i++) {
            // ** ReportsFrom **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            EmployeeSelection reportsFrom = ((SelectionLink) selectionLinks.get(i)).getReportsFrom();
            if (isEmployeeInSelection(employee, reportsFrom)) {
                if (levelsFromTop < getLevelsFromTop(reportsFrom)) {
                    levelsFromTop = getLevelsFromTop(reportsFrom);
                    result = reportsFrom;
                    //System.out.println(employee+" found in 'getLowestLevel' reporting from "+reportsFrom.getName());
                }
            }

            // ** ReportsTo **
            // Using the SelectionInterpreter to extract the employees contained in that selection
            EmployeeSelection reportsTo = ((SelectionLink) selectionLinks.get(i)).getReportsTo();
            if (isEmployeeInSelection(employee, reportsTo)) {
                if (levelsFromTop <= getLevelsFromTop(reportsTo)) {
                    levelsFromTop = getLevelsFromTop(reportsTo);
                    result = reportsTo;
                    //System.out.println(employee+" found in 'getLowestLevel' reporting to "+reportsTo.getName());
                }
            }
        }
        if (result != null) {
            //System.out.println(employee+"'s lowest level:"+result.getName());
        }

        return result;
    }

    public boolean isEmployeeInSelection(Employee employee, EmployeeSelection employeeSelection) {
        // ** ReportsFrom **
        // Using the SelectionInterpreter to extract the employees contained in that selection
        return (SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection).contains(employee));
    }

    /**
     * Returns the number of levels above this employeeSelection
     */
    private int getLevelsFromTop(EmployeeSelection employeeSelection) {
        int result = 0;
        while (getParentSelection(employeeSelection) != null) {
            result++;
            employeeSelection = getParentSelection(employeeSelection);
        }
        return result;
    }

    /**
     * Returns the 'ReportsTo' EmployeeSelection
     */
    public EmployeeSelection getParentSelection(EmployeeSelection employeeSelection) {
        for (Iterator i = selectionLinks.iterator(); i.hasNext();) {
            SelectionLink link = (SelectionLink) i.next();
            EmployeeSelection reportsFrom = link.getReportsFrom();
            if (reportsFrom.equals(employeeSelection)) {
                EmployeeSelection reportsTo = link.getReportsTo();
                return (reportsTo);
            }
        }
        return null;
    }

    /**
     * Returns the 'ReportsFrom' EmployeeSelection
     */
    private LinkedList<EmployeeSelection> getChildSelections(EmployeeSelection employeeSelection) {
        LinkedList result = new LinkedList();
        for (Iterator i = selectionLinks.iterator(); i.hasNext();) {
            SelectionLink link = (SelectionLink) i.next();
            if (link.getReportsTo().equals(employeeSelection)) {
                result.add(link.getReportsFrom());
            }
        }
        return result;
    }

    /**
     * Returns all the employees that are directly or indirectly located above
     * the current employee in the reporting structure
     * @param employeeSelection
     * @return 
     */
    public HashSet getAllParentEmployees(EmployeeSelection employeeSelection) {
        HashSet result = new HashSet();
        // Traverse through the employee's selection's parents and throw all the employees together
        while (getParentSelection(employeeSelection) != null) {
            employeeSelection = getParentSelection(employeeSelection);
            HashSet employees = SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection);
            result.addAll(employees);
        }
        return result;
    } // end getParentEmployees

    /**
     * Returns the employees that are directly located above the current
     * employee in the reporting structure
     */
    public HashSet getParentEmployees(Employee employee, EmployeeSelection employeeSelection) {
        HashSet result = new HashSet();
        EmployeeSelection selection = getParentSelection(employeeSelection);
        if (selection != null) {
            HashSet employees = SelectionInterpreter.getInstance().generateEmployeeList(selection);
            result.addAll(employees);
        }
        return result;
    }

     /**
     * Returns all the employees that are part of the same EmployeeSelection as
     * this employee
     */
    public HashSet getEmployeesOnSameLevel(Employee employee, EmployeeSelection employeeSelection) {
        //EmployeeSelection selection = getEmployeeSelection(employee);
        return (SelectionInterpreter.getInstance().generateEmployeeList(employeeSelection));
    } // end getEmployeesOnSameLevel

    @Override
    public String toString() {
        String result = name;
        return result;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.ReportingStructure
     za.co.ucs.accsys.webmodule.ReportingStructure:    static final long serialVersionUID = -1963427582948727103L;

     */
    private static final long serialVersionUID = -1963427582948727103L;
    private String name;
    private String description;
    public LinkedList selectionLinks = new LinkedList(); // of type SelectionLink
} // end Structure

