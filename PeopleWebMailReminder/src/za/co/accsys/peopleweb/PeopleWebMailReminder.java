/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.peopleweb;

/**
 *
 * @author lterblanche
 */
public class PeopleWebMailReminder {

    /**
     * @param args the command line arguments: -u url [PeopleWeb URL] -a n - [Report on n days since
     * activation] -i n - [Report on n days of inactivity] (One or both of the two should be included) -t
     * -n template_name - [Mandril template name that
     * should be used]
     */
    public static void main(String[] args) {
        boolean canRun = true;

        String argU = ""; // URL of PeopleWeb
        String argN = ""; // MailChimp/Mandril template
        String argI = ""; // Report on Days Of Inactivity
        String argA = ""; // Report on Days Since Activation
        String argFromEmail = "";
        String argFromPerson = "";
        String argMailSubject = "";
        boolean bArgA = false;
        boolean bArgI = false;
        int iI = -1;
        int iA = -1;

        System.out.println("*****************************************************************");
        System.out.println("PeopleWebMailReminder is a daemon that runs once a day.");
        System.out.println("It checks the user accounts on PeopleWeb and generate email notifications");
        System.out.println("  for those users based on certain rules.");
        System.out.println("*****************************************************************");

        if (args.length < 6) {
            System.out.println("PeopleWebMailReminder requires at least 7 parameters:");
            System.out.println(" -u url [PeopleWeb URL]");
            System.out.println(" -n template_name [which Mandril template should be used]");
            System.out.println(" -mf from_person [The name in the FROM address of sent email]");
            System.out.println(" -mm from_email [The Email used in the FROM address of sent email]");
            System.out.println(" -ms mail_subject [The Subject of the email]");
            
            System.out.println(" Other parameters (at least one of the following MUST be provided):");
            System.out.println("   -a n [Report on n days since Activation]");
            System.out.println("   -i n [Report on n days of Inactivity]");
            System.out.println("\n\nExample:java -jar \"PeopleWebMailReminder.jar\" -u \"http://rnd-monster:8080/alpha/\" -n \"peopleweb-4-days\" -a 6 -ms \"PeopleWeb Notification\" -mm \"lterblanche@jhb.accsys.co.za\" -mf \"PeopleWeb Team\"");
            System.out.println("\nRetrieves the list of accounts that were activated 4 days ago, and sending out the MailChimp email (through Mandril) to them, using the \"peopleweb-4-days\" template");
        } else {
            {
                int idx = 0;
                for (String arg : args) {
                    // URL
                    if (arg.equalsIgnoreCase("-u")) {
                        argU = args[idx + 1];
                    }
                    // Template
                    if (arg.equalsIgnoreCase("-n")) {
                        argN = args[idx + 1];
                    }
                    // Days since Activation
                    if (arg.equalsIgnoreCase("-a")) {
                        bArgA = true;
                        argA = args[idx + 1];
                    }
                    // Days of Inactivity
                    if (arg.equalsIgnoreCase("-i")) {
                        bArgI = true;
                        argI = args[idx + 1];
                    }
                    // Mail From Person
                    if (arg.equalsIgnoreCase("-mf")) {
                        argFromPerson = args[idx + 1];
                    }
                    // Mail From Email
                    if (arg.equalsIgnoreCase("-mm")) {
                        argFromEmail = args[idx + 1];
                    }
                    // Mail Subject
                    if (arg.equalsIgnoreCase("-ms")) {
                        argMailSubject = args[idx + 1];
                    }                    

                    idx++;
                }

                // Check the params for validity
                // 
                // Days since Inactivity
                if (bArgI) {
                    try {
                        iI = Integer.parseInt(argI);
                    } catch (NumberFormatException e) {
                        canRun = false;
                        System.out.println("Invalid argument -i: must be an integer (days)");
                    }
                }

                // Days since Inactivity
                if (bArgA) {
                    try {
                        iA = Integer.parseInt(argA);
                    } catch (NumberFormatException e) {
                        canRun = false;
                        System.out.println("Invalid argument -a: must be an integer (days)");
                    }
                }

                // 
                // URL
                //
                if (argU.isEmpty()) {
                    canRun = false;
                    System.out.println("Invalid argument -u: bad/missing URL");
                }

                //
                // Template Name
                //
                if ((argN.isEmpty())) {
                    canRun = false;
                    System.out.println("Invalid argument -n: bad/missing Mandril/MailChimp template name");
                }
                
                //
                // Email Subject
                //
                if ((argMailSubject.isEmpty())) {
                    canRun = false;
                    System.out.println("Invalid argument -ms: bad/missing Email Subject Line");
                }    
                
                //
                // Email From Name
                //
                if ((argFromPerson.isEmpty())) {
                    canRun = false;
                    System.out.println("Invalid argument -mf: bad/missing Email From name");
                }  
                
                //
                // Email Subject
                //
                if ((argFromEmail.isEmpty())) {
                    canRun = false;
                    System.out.println("Invalid argument -mm: bad/missing Email From address Line");
                }                  

                if (canRun) {
                    System.out.println("\n\nStarting PeopleWeb Mail reminder monitor...");
                    System.out.println("\tURL:" + argU);
                    System.out.println("\tMail Template:" + argN);
                    if (bArgA) {
                        System.out.println("\tCheck of days since account activation:" + iA);
                    }
                    if (bArgI) {
                        System.out.println("\tCheck of days of account inactivity:" + iI);
                    }
                    EventMonitor monitor = new EventMonitor();

                    monitor.sendEmailNotifications(argU, argN, iI, iA, argMailSubject, argFromEmail, argFromPerson);
                    //sendTestMail("welcome-to-peopleweb");
                }

            }

        }
    }
    
    public static final String JSP_KEY_DAYS_SINCE_ACTICATION = "daysSinceActivation";
    public static final String JSP_KEY_DAYS_OF_INACTIVITY = "daysOfInactivity";
}
