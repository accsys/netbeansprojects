/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.peopleweb;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lterblanche
 */
public class MailChimpMailer {

    private static void createMailChimpMailer() {
        mailer = new MailChimpMailer();
    }

    public static MailChimpMailer getInstance() {
        if (mailer == null) {
            createMailChimpMailer();
        }
        return mailer;
    }

    /**
     * Sends out (through the Mandrill API) merged emails to the given account
     *
     * @param accounts
     * @param templateName
     * @param subject
     * @param fromMail
     * @param fromName
     * @return
     */
    public boolean sendMail(ArrayList<MailChimpClientAccount> accounts, String templateName, String subject,
            String fromMail, String fromName) {
        MandrillApi mandrillApi = new MandrillApi(MANDRILL_API_KEY);

        // create your message
        MandrillMessage message = new MandrillMessage();
        message.setSubject(subject);
        message.setAutoText(true);
        message.setFromEmail(fromMail);
        message.setFromName(fromName);
        // add recipients
        ArrayList<MandrillMessage.Recipient> recipients = new ArrayList<>();

        // Add list of recipients
        for (MailChimpClientAccount account : accounts) {
            MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(account.getOwnerEmail());
            recipient.setName(account.getOwnerFirstname());
            recipient.setType(MandrillMessage.Recipient.Type.TO);
            recipients.add(recipient);
        }

        //
        // List<MergeVarBucket> 
        // - Used to store the MergeVarBucket of each recipient
        // 
        ArrayList<MandrillMessage.MergeVarBucket> mergeVarBuckets = new ArrayList<>();

        //
        // Clients
        // 
        for (MailChimpClientAccount account : accounts) {
            MandrillMessage.MergeVarBucket bucket = new MandrillMessage.MergeVarBucket();

            MandrillMessage.MergeVar mv1 = new MandrillMessage.MergeVar(MANDRILL_MERGE_FNAME, account.getOwnerFirstname());
            MandrillMessage.MergeVar mv2 = new MandrillMessage.MergeVar(MANDRILL_MERGE_PWACCID, account.getAccountID());
            MandrillMessage.MergeVar mv3 = new MandrillMessage.MergeVar(MANDRILL_MERGE_PWUSERNAME, account.getLoginName());

            bucket.setRcpt(account.getOwnerEmail());
            bucket.setVars(new MandrillMessage.MergeVar[]{mv1, mv2, mv3});

            // Add client 
            mergeVarBuckets.add(bucket);
        }

        message.setTo(recipients);
        // Add MergeVars to message
        message.setMergeVars(mergeVarBuckets);
        message.setPreserveRecipients(false);
        // ... add more message details if you want to!
        // then ... send
        MandrillMessageStatus[] messageStatusReports;
        try {
            messageStatusReports = mandrillApi
                    .messages().sendTemplate(templateName, null, message, false);
            for (MandrillMessageStatus st : messageStatusReports) {
                System.out.println("\nResponse:");
                System.out.println("\t" + st.getEmail());
                System.out.println("\t" + st.getId());
                System.out.println("\t" + st.getStatus());
            }
        } catch (MandrillApiError | IOException ex) {
            Logger.getLogger(MailChimpMailer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    private static MailChimpMailer mailer;
    private static final String MANDRILL_API_KEY = "KORiTNlXBNWpijFNC8HGSQ";
    private static final String MANDRILL_MERGE_PWUSERNAME = "pwusername";
    private static final String MANDRILL_MERGE_FNAME = "fname";
    private static final String MANDRILL_MERGE_PWACCID = "pwaccid";

}
