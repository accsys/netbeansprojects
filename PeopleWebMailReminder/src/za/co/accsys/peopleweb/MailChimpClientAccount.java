/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.peopleweb;

/**
 *
 * @author lterblanche
 */
public class MailChimpClientAccount {

    private String accountID;
    private String companyName;
    private String ownerFirstname;
    private String ownerSurname;
    private String ownerEmail;
    private String loginName;

    /**
     * Default constructor
     */
    public MailChimpClientAccount() {
        loginName = "";
    }
    

    /**
     * Creates an instance of MailChimpClientAccount.  
     * This class is used to send emails through MailChimp/Mandarill
     * @param accountID
     * @param companyName
     * @param ownerFirstname
     * @param ownerSurname
     * @param ownerEmail
     * @param loginName 
     */
    public MailChimpClientAccount(String accountID, String companyName, String ownerFirstname, String ownerSurname, String ownerEmail, String loginName) {
        this.accountID = accountID;
        this.companyName = companyName;
        this.ownerFirstname = ownerFirstname;
        this.ownerSurname = ownerSurname;
        this.ownerEmail = ownerEmail;
        this.loginName = loginName;
    }

    
    public String getAccountID() {
        return accountID;
    }

    
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOwnerFirstname() {
        return ownerFirstname;
    }

    public void setOwnerFirstname(String ownerFirstname) {
        this.ownerFirstname = ownerFirstname;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

}
